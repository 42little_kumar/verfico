<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagecontent extends Model
{
	protected $table = 'pagecontent';
    protected $fillable = ['pagecontent'];
}
