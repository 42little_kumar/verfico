<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollFiles extends Model
{
    
    protected $table = 'payroll_files';

    protected $fillable = [ 'file_data', 'payroll_id' ];


}
