<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    
		protected $table = "payroll";

		protected $appends = [ 

			'uploaded_on',

			'subcontractor_name',

			'subcontractor_email',

			'payroll_file_source'

		];

		/*
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [

		    'title',

		    'description',

		    'payroll_file',

			'author',

			'created_by',
			
			'by_admin',

			'notes',

		    'start_date',

		    'end_date'
		
		];

		/*
	     * @Function Name
	     *
	     *
	     */
	    public function getUploadedOnAttribute()
	    {

			$uploadDate = date( "m-d-Y", strtotime( $this->created_at ) );

	        return $uploadDate;

	    }

	    /*
	     * @Function Name
	     *
	     *
	     */
	    public function getSubcontractorNameAttribute()
	    {

	        $dataObject = User::find( $this->author );

	        return @$dataObject->company_name;

		}

		public function getWorkerName( $sheetName, $ssnId, $parent )
		{

			$ssn = substr($ssnId, -4);

			$lastNameSsn = explode(' ', $sheetName);

        	$lastFourSheetName = strtoupper(substr(end($lastNameSsn), 0, 4));

			$return = true;

			$sub = User::where('id', $parent)->first()->company_name;

			$sub = str_replace(' ', '', $sub);

			$sub = substr($sub, 0, 4);

			$ssn = strtoupper( $sub ).$ssn.$lastFourSheetName;

			if( User::where('ssn_id', $ssn)->where('status',2)->where( 'parent', $parent )->exists() )
			{
				$worker = User::where('ssn_id', 'LIKE', '%' . $ssn . '%')->where('status',2)->where( 'parent', $parent )->first();
				$compare_name = strcasecmp( trim( $sheetName ), trim( $worker->name ) );
				if($worker->ssn_id === $ssn && $compare_name !=0){
					return 2;
				}

				return $worker->name;
			}

			return $return;

		}



		/*
	     * @Function Name
	     *
	     *
	     */
	    public function getSubcontractorEmailAttribute()
	    {

			$dataObject = User::find( $this->author );
			
			if( !empty( $dataObject->payroll_email ) )
			{
				return @$dataObject->email.','.@$dataObject->payroll_email;
			}

	        return @$dataObject->email;

		}

		/*
		 * @Average Salary
		 * 
		 * 
		 */
		public function getAvgSalary( $totalHours, $totalPaid, $gross )
		{
			$h = number_format((float)$totalHours,2);

			if( strpos( $h, '.' ) !== false )
			{
			  
				list( $hr, $mn ) = explode('.', $h);

				if( $mn < 30 )
					$h = (int)$hr;

				if( $mn >= 30 )
					$h = (int)$hr+1;

			} 

			$totalHours = $h;

			if($totalHours <= 40 && $totalHours != 0)
			{

				return (int)$gross/(int)$totalHours;

			}
			
			if( $totalHours > 40 && $totalHours != 0)
			{

				$hoursDiff = $totalHours - 40;

				$overtimeHours = $hoursDiff * 1.5;

				$fullHours = $overtimeHours + 40;

				return $gross/$fullHours;

			}

			return 0;

		}
		
		/*
		 * @Average Salary
		 * 
		 * 
		 */
		public function ifOvertime( $totalHours )
		{

			$h = number_format((float)$totalHours,2);

			if( strpos( $h, '.' ) !== false )
			{
			  
				list( $hr, $mn ) = explode('.', $h);

				if( $mn < 30 )
					$h = (int)$hr;

				if( $mn >= 30 )
					$h = (int)$hr+1;

			} 

			$totalHours = $h;
			
			if( $totalHours > 40 && $totalHours != 0)
			{

				return true;

			}

			return false;

		}

		/*
		 * @Average Salary
		 * 
		 * 
		 */
		public function checkGross( $gross, $netPay )
		{

			if( $netPay > $gross )
				return true;

			return false;

		} 
		
		/*
	     * @Function Name
	     *
	     *
	     */
	    public function getWorkerTotalHoursBySsn( $ssn, $totalExcelLogs, $startDate, $endDate, $author )
	    {
	    	$fullSsn = $ssn;

			$ssn = substr($ssn, -4);

			$worker = User::where('ssn_id', 'LIKE', '%' . $fullSsn . '%')->first();

			$totalHours = 0;

			if( $worker )
			{

				$logsObject = WorkLog::where( 'worker_id', $worker->id )->orderBy( 'id', 'desc' );

				if( !empty( $startDate ) && !empty( $endDate ) )
				{

					$dateStart = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($startDate))->format('M d Y');

					$dateEnd = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($endDate))->format('M d Y');

					$from = \Carbon\Carbon::parse( $dateStart )
                             ->startOfDay()
                             ->toDateTimeString();

					$to = \Carbon\Carbon::parse( $dateEnd )
									->endOfDay()
									->toDateTimeString();

					/* Format date */

					$logsObject->where('sub', $author);
					$logsObject->whereBetween('punch_at', [$from, $to]);

				}

				$logs = $logsObject->get();

				if( $logs ):

				foreach( $logs as $get )
				{
					
					$get->punch_in = substr_replace($get->punch_in, '00', -2);

					$get->punch_out = substr_replace($get->punch_out, '00', -2);

					$start = date("Y-m-d ".$get->punch_in);
					
					if(strtotime($get->punch_out) < strtotime($get->punch_in)){
						
						$end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$get->punch_out) ) + 24*60*60) ;
					} else {
						$end = date("Y-m-d ".$get->punch_out);
					}

					$totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

					$totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

					$totalTime = str_replace( ':', '.', $totalTime);

					$totalHours += (float)$totalTime;

				}

				endif;

			}

			$h = number_format((float)$totalHours,2);

			if( strpos( $h, '.' ) !== false )
			{
			  
				list( $hr, $mn ) = explode('.', $h);

				if( $mn < 60 )
					$h = (int)$hr;

				if( $mn >= 60 )
					$h = (int)$hr+1;

			} 

			$totalHours = $h;
			
			//echo $totalHours;

			//echo $totalExcelLogs;

			//die;

			//return $totalHours;

			/*if( $totalHours <= $totalExcelLogs )
			{

				return 'equal';

			}*/

			$error = false;
	

			$diff = $totalHours - $totalExcelLogs;

	
			if( $diff > 0 )
			{
				$error = true;
			}

			if( !$error )
			{
				return 'equal';
			}

			/*if( $totalHours <= $totalExcelLogs )
			{

				return 'equal';

			}*/

	    }

	    /*
	     * @Function Name
	     *
	     *
	     */
	    public function getPayrollFileSourceAttribute()
	    {

	        $payrollFileUrl = url( '/public/uploads/images/payroll/'.$this->author );

	        $payrollFileUrl = $payrollFileUrl.'/'.$this->payroll_file;

	        $downloadFileRoute = route( 'common.datatable.payroll.download.file', [ $this->payroll_file, $this->author ] );

	        $fileName =strlen($this->payroll_file) > 20 ? substr($this->payroll_file,0,20)."..." : $this->payroll_file;

	        $payrollFileSource = "<a href='$downloadFileRoute' ><i class='fa fa-download'></i> ".$fileName."</a>";

	        return $payrollFileSource;

		}
		
		/*
		 * @Check SSN
		 * 
		 * 
		 */
		public function getSsnStatus( $ssnId, $parent )
		{

			$ssn = substr($ssnId, -4);

			$return = true;

			if( User::where('ssn_id', 'LIKE', '%' . $ssn . '%')->where( 'parent', $parent )->exists() )
			$return = false;

			return $return;

		}

		/*
		 * @Check SSN
		 * 
		 * 
		 */
		public function getSsnStatusIds( $sheetName, $ssnId, $parent )
		{

			$ssn = substr($ssnId, -4);

			//$lastFourSheetName = strtoupper(substr($sheetName, -4));

			$lastNameSsn = explode(' ', $sheetName);

        	$lastFourSheetName = strtoupper(substr(end($lastNameSsn), 0, 4));

			$return = true;

			$sub = User::where('id', $parent)->first()->company_name;

			$sub = str_replace(' ', '', $sub);

			$sub = substr($sub, 0, 4);

			$ssn = strtoupper( $sub ).$ssn.$lastFourSheetName;

			if( User::where('ssn_id',$ssn)->where('status',2)->where( 'parent', $parent )->exists() )
			{
				return User::where('ssn_id',$ssn)->where('status',2)->where( 'parent', $parent )->first()->id;
			}
			
		}

		public function getNotInUsers( $notInIds, $parent, $sd, $ed )
		{

			$lw = [];

			$nius = [];

	        $worklogObject = \App\WorkLog::select('worker_id')->orderBy('punch_at', 'desc');

	 

	            $from = \Carbon\Carbon::parse( $sd )
	                             ->startOfDay()
	                             ->toDateTimeString();

	            $to = \Carbon\Carbon::parse( $ed )
	                             ->endOfDay()
	                             ->toDateTimeString();

	            $worklogObject = $worklogObject->where('sub', $parent)->whereBetween('punch_at', [$from, $to])->get();

	      	

	        foreach ($worklogObject as $get)
	        {
	          array_push($lw, $get->worker_id);
	        }

			$niu = User::whereNotIn('id', $notInIds)->where('status', 2)->where( 'parent', $parent )->get();

			foreach ($niu as $get)
	        {
	          array_push($nius, $get->id);
	        }

	        $fi = array_intersect($lw, $nius);  

	        $fu = User::whereIn('id', $fi)->where('status', 2)->where( 'parent', $parent )->get();

			return $fu;

		}

}
