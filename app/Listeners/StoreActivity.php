<?php

namespace App\Listeners;

use App\UserActivity;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StoreActivity
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        $user = $event->user;

        $type = $event->type;

        $content = $event->content;
        
        UserActivity::create([

            'user_id' => $user->id,

            'type' => $type,

            'content' => $content

        ]);

    }
}
