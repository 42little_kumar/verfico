<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tempdb extends Model
{
    protected $table = 'temp';

    protected $fillable = [

        'project_id', 'job_date','created_by', 'job_time', 'job_2_time', 'emp_data', 'emp_data_2'
    
    ];
}
