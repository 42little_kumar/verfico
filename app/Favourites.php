<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourites extends Model
{
    protected $fillable = ['user_id', 'project_id'];



    /*
	 * @Function Name
	 *
	 *
	 */
	public function projects()
	{
	    return $this->belongsTo( Project::class, 'project_id' );
	}
}
