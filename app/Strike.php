<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\WorkLog;
use App\WorkerType;
use DB;
use App\User;
use App\Document;
use App\UserDevice;

class Strike extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable, SoftDeletes;
    
    protected $table = "safety_strikes";

    protected $appends = ['mediaurl', 'projectname', 'projectnumber', 'workername', 'profilepic','createdname', 'isvideo'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'created_by',

        'worker_id',

        'description',
        
        'strike_date',

        'category',

        'name',

        'media',
    
    ];

    public function getMediaurlAttribute()
    {

        $media = $this->media;

        if( !empty($this->media) )
        return url('/').'/public/uploads/strike/'.$media;

        return null;

    }

    public function getIsvideoAttribute()
    {

        $media = $this->media;

        if( !empty($this->media) )
        {

            if(strtolower(substr($this->media, -3)) == "mp4")
            {

                return 1;

            }

        }

        return 0;

    }

    public function getWorkernameAttribute()
    {

        $id = $this->worker_id;

        $name = '';

        if(\DB::Table('users')->where('id', $id)->exists())
            $name = \DB::Table('users')->where('id', $id)->first()->name;

        return $name;

    }

    public function getProfilepicAttribute()
    {

        $workerId = $this->worker_id;

        $image = @\DB::Table('users')->where('id', $workerId)->first()->image;

        if( !empty( $image ) )
            return url('/').'/uploads/images/users/'.$workerId.'/'.$image;

        return null;

    }

    public function getCreatednameAttribute()
    {

        $workerId = $this->created_by;

        $name = @\DB::Table('users')->where('id', $workerId)->first()->name;

        if( $name )
            return $name;

        return null;

    }

    public function getProjectnameAttribute()
    {

        $projectId = $this->name;

        $projectName = null;

        if(\DB::Table('project')->where('id', $projectId)->exists())
            $projectName = \DB::Table('project')->where('id', $projectId)->first()->job_name;

   
        return $projectName;


    }

    public function getProjectnumberAttribute()
    {

        $projectId = $this->name;

        $projectNumber = null;

        if(\DB::Table('project')->where('id', $projectId)->exists())
            $projectNumber = \DB::Table('project')->where('id', $projectId)->first()->job_number;

   
        return $projectNumber;


    }

}
