<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerType extends Model
{
    
	protected $table = 'worker_types';

	protected $fillable = ['type'];

}
