<?php

namespace App\Mail;

use App\User;
use App\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PasswordReset $reset)
    {

        $this->reset = $reset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('verfico@rockspringcontracting.com')
                    ->subject( env('APP_NAME') . ' - Reset Password')
                    ->markdown('emails.auth.reset-password')
                    ->with('reset', $this->reset);
    }
}
