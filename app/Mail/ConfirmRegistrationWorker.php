<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmRegistrationWorker extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user, $notificationText, $simplePassword, $workerCompanyField )
    {

        $this->user = $user;
        
        $this->title = 'Registration confirmation';
        
        $this->message = $notificationText;

        $this->simple_password = $simplePassword;

        $this->worker_company_field = $workerCompanyField;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('verfico@rockspringcontracting.com')
                    ->subject( env('APP_NAME') . ' - ' . $this->title)
                    ->markdown('emails.confirmWorker')
                    ->with('user', $this->user)
                    ->with('simplePassword', $this->simple_password)
                    ->with('workerCompanyField', $this->worker_company_field)
                    ->with('message', $this->message);
    }
}
