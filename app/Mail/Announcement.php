<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Announcement extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $title, $message)
    {

        $this->user = $user;
        
        $this->title = $title;
        
        $this->message = $message;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('donotreply@pt-times.com')
                    ->subject( env('APP_NAME') . ' - Announcement: ' . $this->title)
                    ->markdown('emails.notification')
                    ->with('user', $this->user)
                    ->with('message', $this->message);
    }
}
