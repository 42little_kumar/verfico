<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PayrollUpdateAlert extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $subcontractorData, $notificationText )
    {

        $this->user = $subcontractorData;
        
        $this->title = 'Payroll Update Alert';
        
        $this->message = $notificationText;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('verfico@rockspringcontracting.com')
                    ->subject( env('APP_NAME') . ' - ' . $this->title)
                    ->markdown('emails.payrollUpdateAlert')
                    ->with('user', $this->user)
                    ->with('message', $this->message);
    }
}
