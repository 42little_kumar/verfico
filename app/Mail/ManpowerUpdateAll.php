<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ManpowerUpdateAll extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $subject, $data, $notificationText )
    {

        $this->data = $data;
        
        $this->title = $subject.' ('.ucfirst(\Auth::user()->name).')';
        
        $this->message = $notificationText;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('verfico@rockspringcontracting.com')
                    ->subject( $this->title)
                    ->markdown('emails.manpowerUpdateAll')
                    ->with('data', $this->data)
                    ->with('message', $this->message);
    }
}
