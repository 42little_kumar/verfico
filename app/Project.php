<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;


class Project extends Model
{

	use SoftDeletes;

	protected $table = "project";

	protected $appends = ['sub_contractor_name', 'formated_start_date', 'formated_completion_date','is_favourite'];



	/*
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [

	    'job_type',

        'job_number',

	    'job_name',

        'superint',

	    'sub_contractor',

        'foremen',

        'general_contractor_name',

	    'job_address',

        'job_description',

	    'qrcode',

        'lat',

        'lng',

        'manual_log',

	    'status',

	    'started_at',

	    'completion_at'
	
	];

	/*
     * @Function Name
     *
     *
     */
    public function getSubContractorNameAttribute()
    {

        $subContName = @User::find( $this->sub_contractor )->first_name;

        return $subContName;

    }



    	/*
     * @Function Name
     *
     *
     */
    public function getIsFavouriteAttribute()
    {

       return  @Favourites::whereProjectId($this->id)->whereUserId(Auth::user()->id)->first()?true:false;

    }

    /*
	 * @Function Name
	 *
	 *
	 */
	public function getlastWorkOnAttribute()
	{
	    return @WorkLog::whereProjectId($this->id)->latest()->first()->punch_at;
	}


    /*
     * @Function Name
     *
     *
     */
    public function getFormatedStartDateAttribute()
    {

        $date = $this->started_at;

        $formatedDate = date( 'm/d/Y', strtotime( $date ) );

        if( empty( $date ) )
            $formatedDate = '';

		return $formatedDate;


    }

    /*
     * @Function Name
     *
     *
     */
    public function getFormatedCompletionDateAttribute()
    {

        $date = $this->completion_at;

        $formatedDate = date( 'm/d/Y', strtotime( $date ) );

        if( empty( $date ) )
            $formatedDate = '';

        return $formatedDate;


    }
	
}
