<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerDocuments extends Model
{
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $table = 'worker_documents';

	protected $fillable = [

	    'author', 'user_id', 'document', 'notes', 'show'
	    
	];

	public function user()
	{

	    return $this->belongsTo('App\User', 'user_id', 'id');

	}
}
