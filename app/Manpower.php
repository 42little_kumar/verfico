<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manpower extends Model
{
    protected $table = 'manpower';

    protected $fillable = [

        'project_id', 'job_date','created_by', 'shifttype', 'shifttype2', 'job_time', 'closed', 'special_notes', 'job_2_time', 'emp_data', 'emp_data_2'
    
    ];
}
