<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\User; 
use App\WorkerLog; 
use App\Strike; 
use DB;
use Carbon\Carbon;

class UserExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        $role_type = $_GET['usertype'];
        $status  =   trim($_GET['status']);
        $company =   @$_GET['company'];
        $title   =   @$_GET['title'];
        $count = User::where('role',$role_type)->count();
        $offset = 0;
        $limit = 100000;
        $arr_to_return = [];

        
        while($count>=0){

            $userObject = User::orderBy('name', 'ASC');
         

            if(!empty( $role_type ) ){
                $userObject->where('role',$role_type);
            }


            if($status == "0" || $status == "2"){
                $userObject->where( 'status', $_GET['status'] );
            }else{
                
            }
          
            if(!empty($company) ){

                $userObject->where( 'parent', $company );
            }   
            if(!empty($title)){
                $userObject->where( 'worker_type', $title );
            }
          
            $users = $userObject->where( 'status', '!=', 1 )->select('id','parent','role','rsc_employee_id', 'name', 'classification', 'email', 'phone_number', 'ssn_id', 'training', 'osha_date', 'cpr_date', 'status', 'worker_type')->get();
           // dd($users);
           
            foreach ($users as $key => $user) {
               

                $user->ssn_id = $user->ssn_id;

                if( !empty( $user->training ) ){
                    $user->training = date('m/d/Y', strtotime( $user->training ));
                }
                $user->last_worked = '';
 
                $lastWorked = DB::table('worker_log')->select('punch_at')->where('worker_id', $user->id)->orderBy('id', 'desc')->first();

                if( $lastWorked ){ 
                  $lastWorked = $lastWorked->punch_at;
                  $user->last_worked = date('m/d/Y', strtotime( $lastWorked ));
                }

                $user->total_strikes = '';
                $user->total_strikes_30 = '';
                $user->last_strike_date = '';
                if( Strike::where( 'worker_id', $user->id )->exists() ){
                    $user->total_strikes = Strike::select('id')->where( 'worker_id', $user->id )->count();
                }
                  
                if( Strike::where( 'worker_id', $user->id )->exists() ){
                    $user->total_strikes_30 = Strike::select('id')->where( 'worker_id', $user->id )->whereDate('created_at', '>', Carbon::now()->subDays(30))->count();
                } 
                if( $user->status == 0 )
                {
                    $user->status = "Blocked";
                }else
                {
                    $user->status = "Active";
                }

                if( Strike::where( 'worker_id', $user->id )->exists() ){
                    $user->last_strike_date = date('m/d/Y', strtotime( Strike::select('strike_date')->where( 'worker_id', $user->id )->orderBy('id', 'desc')->first()->strike_date ) );
                }

                $user->classification = $user->classification;  
                $user->name = $user->name;
              
                $user->osha_date = !empty($user->osha_date) ? date("m/d/Y", strtotime($user->osha_date)) : '';
                $user->cpr_date = !empty($user->cpr_date) ? date("m/d/Y", strtotime($user->cpr_date)) : '';

                $arr_to_return[] = [

                                'RSC Employee Id'=> $user->rsc_employee_id,
                                'Name'=> $user->name,
                                'Classification'=>$user->classification,
                                'Email'=>$user->email,
                                'Phone'=>$user->phone_number,
                                'Company'=>$user->sub_contractor_name,
                                'SSN ID'=>$user->ssn_id,
                                'Title'=>$user->worker_type_name,
                                'Rock Spring Safety Training received on'=>$user->training,
                                'OSHA 30 Training received on'=>$user->osha_date,
                                'CPR Training received on'=>$user->cpr_date,
                                'Last Worked'=>$user->last_worked,
                                'Total Strikes'=>$user->total_strikes,
                                'Total 30 Day Strikes'=>$user->total_strikes_30,
                                'Last Strike Date'=>$user->last_strike_date,
                                'Status'=>$user->status,
                            ];
            }
            $offset = $offset + $limit;
              
            $count = $count - $limit; 
        }

        return collect($arr_to_return);

    }

    public function headings(): array
    {
        return [
            'RSC Employee Id',
            'Name', 
            'Classification',
            'Email',
            'Phone',
            'Company',
            'SSN ID',
            'Title',
            'Rock Spring Safety Training received on',
            'OSHA 30 Training received on',
            'CPR Training received on',
            'Last Worked',
            'Total Strikes',
            'Total 30 Day Strikes',
            'Last Strike Date',
            'Status'

        ];
    }
}
