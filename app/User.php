<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\WorkLog;
use App\WorkerType;
use DB;
use App\User;
use App\Document;
use App\UserDevice;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable, SoftDeletes;
    

    protected $appends = ['sub_contractor_name', 'worker_type_name', 'profile_image'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'username',

        'first_name',

        'last_name',

        'role',
        
        'name',

        'payroll_first_name',

        'payroll_last_name',

        'payroll_email',

        'payroll_phone',

        'phone_number',

        'email',

        'ssn_id',

        'company_name',

        'password',

        'parent',

        'updated_by',

        'doc_upload',

        'worker_type',
        
        'worker_document',

        'notes_training',

        'training',

        'lang',

        'classification',

        'osha_date',

        'cpr_date',

        'other_notes',

        'rsc_employee_id',

        'device_info',
    
    ];

    /*
     * @Function Name
     *
     *
     */
    public function getProfileImageAttribute()
    {

        $destination = asset('uploads/images/users/'. $this->id);

        $imagePath = $destination.'/'.$this->image;

        return $imagePath;

    }

    /*
     * @Function Name
     *
     *
     */
    public function getSubContractorNameAttribute()
    {

        $subContName = 'Not Selected';

        if( $this->parent != 0 )
        {

            $dataObject = @User::find( $this->parent );

            $subContName = @$dataObject->company_name;

        }

        return $subContName;

    }

    /*
     * @Function Name
     *
     *
     */
    public function getWorkerTypeNameAttribute()
    {

        if( is_numeric( $this->worker_type ) )
        {
            
            $workerTypeData = WorkerType::where( 'id', $this->worker_type )->first();

            return @$workerTypeData->type;

        }

        return empty( $this->worker_type ) ? 'Foreman' : $this->worker_type;

    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    
        'password', 'remember_token',
    
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    
        'email_verified_at' => 'datetime',
    
    ];

    public function reset_password()
    {

        return $this->hasOne('App\PasswordReset', 'email', 'email');

    }

    public function social_accounts()
    {
    
        return $this->hasMany('App\SocialAccount', 'user_id', 'id');
    
    }

    public function categories()
    {

        return $this->belongsToMany('App\Category', 'category_user', 'user_id', 'category_id');

    }

    public function sub_contractor_name(){
        return $this->belongsTo('App\User','parent');
    }

    public static function userHasRoles($user_id = null){
        $user = DB::table("model_has_roles")->where('model_id',$user_id)->first();
        if($user->role_id == 5){
            return 'true';
        }else{
            return 'false';
        }    
    }

    public function user_worker_type()
    {

        return $this->hasOne('App\WorkerType','id','worker_type');

    }


    public function worker()
    {

        return $this->belongsTo('App\WorkerType','type','worker_type');

    }

    public function devices()
    {

        return $this->hasMany('App\UserDevice', 'user_id', 'id');

    }

    public function documents()
    {

        return $this->hasMany('App\Document', 'user_id', 'id');

    }

    public function workerDocuments()
    {

        return $this->hasMany('App\WorkerDocuments', 'user_id', 'id');

    }

    public function workerDocumentsCount() {
        return $this->workerDocuments()->where('show', 1);
    }

    public function activities()
    {

        return $this->hasMany('App\UserActivity', 'user_id', 'id');

    }

    public function member_roles()
    {

        return $this->hasMany('App\CheckRoles', 'model_id', 'id');

    }

    public function availability()
    {

        return $this->hasOne('App\TrainerAvailability', 'trainer_id', 'id');

    }

    public function user_bookings()
    {

        return $this->hasMany('App\ProductBooking', 'user_id', 'id');

    }

    public function trainer_bookings()
    {

        return $this->hasMany('App\ProductBooking', 'trainer_id', 'id');

    }

    public function tokens()
    {

        return $this->hasMany('App\UserDevice', 'user_id', 'id');

    }

    /*
     * @Function Name
     *
     *
     */
    public function logs()
    {

        return $this->hasMany('App\WorkLog', 'worker_id', 'id')->latest();

    }

    /*
     * @Function Name
     *
     *
     */
    public function scopeGetLogs( $projectId, $workerId )
    {

        $logData = WorkLog::where( 'project_id', 1 )->where( 'worker_id', 4 )->get();

        return $logData;

    }

}
