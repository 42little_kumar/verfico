<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\CronController;

use Illuminate\Console\Command;

use Illuminate\Http\Request;

class DuplicateLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duplicatelogs:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $cron  = new CronController();
        $cron->checkDuplicateLogs(new Request);
    }
}
