<?php

namespace App\Console;
use Illuminate\Console\Scheduling\Schedule;
// use Illuminate\Support\Facades\DB;
use DB;
use App\User;
use App\UserDevice;
use Carbon\Carbon;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            try{
                $worklogs  = DB::table('worker_log')->whereNull('punch_out')->where('punch_at','<',Carbon::now()->subHours(13))->get();
             if(count($worklogs) > 0):
                    foreach($worklogs as $worklog  ):
                        DB::table('worker_log')->where('id', $worklog->id)->update([
                            'punch_out' =>Carbon::parse($worklog->punch_in)->addHour(8),
                            'out_type'=>4
                            ]);
                    endforeach;
                endif;
            }catch(Exception $e){
                print_r($e->getMessage());
            }
        })->everyMinute();

        $schedule->call('App\Http\Controllers\Api\PunchNotificationCron@sendTimeOutReminder')->everyMinute();

        //$schedule->call('App\Http\Controllers\Admin\ReportNotificationCron@sendReportNotificationCron')->weeklyOn(6, '8:00');

        //$schedule->call('App\Http\Controllers\Admin\ManpowerCron@sendManpowerDayBefore')->everyMinute();
   
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
