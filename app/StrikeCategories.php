<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\WorkLog;
use App\WorkerType;
use DB;
use App\User;
use App\Document;
use App\UserDevice;

class StrikeCategories extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;
    
    protected $table = "strike_category";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'name'
    
    ];

    

}
