<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'user_id', 'type', 'identifier', 'api_token', 'fcm_token'
        
    ];

    public function user()
    {

        return $this->belongsTo('App\User', 'user_id', 'id');

    }

}
