<?php

namespace App\Imports;
use App\User;
use App\PayrollFiles;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PayrollImport implements ToCollection, WithCalculatedFormulas, WithMultipleSheets
{

    private $data; 

    private $payrollFileIds = [];

    public function __construct(array $data = [])
    {
        $this->data = $data; 
    }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    public function collection(Collection $rows)
    {

        $payrollId = $this->data['payroll_id'];

        $excelRows = $rows;

        $skipValues = [ 'Payroll Register', 'Employee', 'Name', '', 'Payroll Expense Charts' ];
        
        foreach( $excelRows as $row )
        {
            
            if( in_array( $row[0], $skipValues ) ) { continue; }

            if( isset( $row[13] ) && $row[13]!=null)
            {

                $payrollData = [];
                
                for( $i = 0; $i <= 13; $i++ )
                {

                    $payrollData[$i] = $row[$i];

                }

                $payrollObject = new PayrollFiles;
                $payrollObject->file_data = json_encode( $payrollData );
                $payrollObject->payroll_id = $payrollId;
                $payrollObject->save();

                array_push( $this->payrollFileIds, $payrollObject->id );

            }
            
        }

    }

    public function getPayrollIds()
    {
        return $this->payrollFileIds;
    }
}