<?php

use App\User;
use App\Manpower;

/*
 * Worker request count
 *
 *
 */
function getRoleTitle( $role )
{
    
    switch ( strtolower( $role ) ){
            case "subcontractor":
                $userTitle = 'Subcontractors';
                break;
            case "foreman":
                $userTitle = 'Foremen';
                break;
            case 'worker':
                $userTitle = 'Workers';
                break;
            case 'superint':
                $userTitle = 'Superintendents';
                break;
            case 'admin':
                $userTitle = 'Admins';
                break;
            case 'payroll':
                $userTitle = 'Providers';
                break;
            case 'safetyo':
                $userTitle = 'Safety Officers';
                break;
            default:
                $userTitle = 'Users';
        }

        return $userTitle;

}

function getScheduleSubsPreview( $input )
{


        $format = 'm-d-Y';

        $allSubs = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);

        $projectIds = \DB::table('project')
        ->where('status', 'active')
        ->where('superint', Auth::user()->id)
        ->pluck('id');

        $schedules = Manpower::where('job_date','=', $c->toDateString())
        ->whereIn('project_id', $projectIds)
        ->get();

        foreach ($schedules as $get) {
          
         

            foreach( json_decode( $get->emp_data )->sub as $g1 )
            {

                array_push($allSubs, $g1);

            }

            if( !empty( $get->emp_data_2 ) )
            {

                foreach( json_decode( $get->emp_data_2 )->sub as $g2 )
                {

                    array_push($allSubs, $g2);

                }

            }

        }
        
        $option = '<option value="">-Select-</option>';
        $option .= '<option value="admin">Admin</option>';

        foreach( array_unique($allSubs) as $g )
        {
            $subName = \DB::table('users')->where('id', $g)->first();

            if( $g != 624 ):

                $option .= '<option value="'.$g.'">'.$subName->company_name.'</option>';

            endif;
        }
        
        return $option;

}

function getScheduleSubsPreviewCurrent( $input, $mid )
{


        $format = 'm-d-Y';

        $allSubs = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);

        $projectIds = \DB::table('project')
        ->where('status', 'active')
        ->where('superint', Auth::user()->id)
        ->pluck('id');

        $schedules = Manpower::where('job_date','=', $c->toDateString())
        ->whereIn('project_id', $projectIds)
        ->get();

        $get = Manpower::find( $mid );

       
          
         

            foreach( json_decode( $get->emp_data )->sub as $g1 )
            {

                array_push($allSubs, $g1);

            }

            if( !empty( $get->emp_data_2 ) )
            {

                foreach( json_decode( $get->emp_data_2 )->sub as $g2 )
                {

                    array_push($allSubs, $g2);

                }

            }

        
        
        $option = '<option value="">-Select-</option>';
        $option .= '<option value="admin">Admin</option>';

        foreach( array_unique($allSubs) as $g )
        {
            $subName = \DB::table('users')->where('id', $g)->first();

            if( $g != 624 ):

                $option .= '<option value="'.$g.'">'.$subName->company_name.'</option>';

            endif;
        }
        
        return $option;

}

function getScheduleCurrentSubs( $input )
{


        $format = 'm-d-Y';

        $allSubs = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);

        $projectIds = \DB::table('project')
        ->where('status', 'active')
        ->where('superint', Auth::user()->id)
        ->pluck('id');

        $schedules = Manpower::where('job_date','=', $c->toDateString())
        ->whereIn('project_id', $projectIds)
        ->get();

        foreach ($schedules as $get) {
          
         

            foreach( json_decode( $get->emp_data )->sub as $g1 )
            {

                array_push($allSubs, $g1);

            }

            if( !empty( $get->emp_data_2 ) )
            {

                foreach( json_decode( $get->emp_data_2 )->sub as $g2 )
                {

                    array_push($allSubs, $g2);

                }

            }

        }
        
        
        
        return array_unique($allSubs);

}

function getScheduleCurrentSubsForCurrent( $input, $id )
{


        $format = 'm-d-Y';

        $allSubs = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);

        $projectIds = \DB::table('project')
        ->where('status', 'active')
        ->where('superint', Auth::user()->id)
        ->pluck('id');

        

      
          
         $get = Manpower::find($id);

            foreach( json_decode( $get->emp_data )->sub as $g1 )
            {

                array_push($allSubs, $g1);

            }

            if( !empty( $get->emp_data_2 ) )
            {

                foreach( json_decode( $get->emp_data_2 )->sub as $g2 )
                {

                    array_push($allSubs, $g2);

                }

            }

        
        
        
        
        return array_unique($allSubs);

}

/*
 * Worker request count
 *
 *
 */
function getWorkerRequestCount()
{

    $requestCount = User::role('worker')->where( 'status', 1 )->count();

    return $requestCount;

}

/*
 * Worker request count
 *
 *
 */
function getIconGuides()
{

    $autoOut = asset('public/images/auto_out.png');

    $byForeman = asset('public/images/by_foreman.png');

    $adminOut = asset('public/images/admin_out.png');

    $manualPunch = asset('public/images/manual_punch.png');

    $html = "<div class='icons-guide-container'>
    <div class='icon_3'><img src='$manualPunch'><span>Manual login/logout by worker</span></div>
    <div class='icon_1'><img src='$autoOut'><span>Worker logout automatically after time out</span></div>
    <div class='icon_2'><img src='$byForeman'><span>Worker login/logout by foreman</span></div>
    <div class='icon_5'><img src='$adminOut'><span>Log edited by administrator</span></div>
    </div>";

    return $html;

}

/*
 * Worker request count
 *
 *
 */
function gmwin( $p, $w, $d, $s )
{

    $uids = [];

    $us =  User::role('worker')->where('parent', $s)->where('worker_type', $w)->get();

    foreach( $us as $u )
    {
        array_push($uids, $u->id);
    }

}

function gmwout( $p, $w, $d, $s )
{

    $r = '-';

   for( $i = $w; $i > 0; $i-- )
      {

        $awids = [];

        
        $wt =DB::table('worker_types')->where('id', $s)->first()->id;

        $aw = User::role('worker')->where('worker_type', $wt)->get();

        if( $aw ):

        foreach ($aw as $g) {
           array_push($awids, $g->id);
        }

        foreach ($awids as $g) {
            
            $dt = DB::table('worker_log')->where('project_id', $p)->where('worker_id', $g)->whereDate('punch_at', $d)->first();

            if( $dt )
            {
                $o = $dt->punch_out;

                return gt( $o );
            }

        }

        endif;

      }


    return $r;

}

function gmin( $p, $w, $d )
{

    $dt = DB::table('worker_log')->where('project_id', $p)->where('worker_id', $w)->whereDate('punch_at', $d)->first();

    if( $dt )
    {
        $i = $dt->punch_in;

        return gt( $i );
    }

    return '-';

}

function gmout( $p, $w, $d )
{

    $dt = DB::table('worker_log')->where('project_id', $p)->where('worker_id', $w)->whereDate('punch_at', $d)->first();

    if( $dt )
    {
        $o = $dt->punch_out;

        return gt( $o );
    }

    return '-';    

}

function ifEmail( $pr, $d )
{

    $input  = $d;
          
    $format = 'm-d-Y';

    $c = \Carbon\Carbon::createFromFormat($format, $input);

    $exist = false;

    $exist = \App\Manpower::where('job_date','=', $c->toDateString())->where('project_id',$pr)->where('is_sechedule_email_sent',1)->exists();

    return $exist;

}

function ifShd( $pr, $d )
{

    $input  = $d;
          
    $format = 'm-d-Y';

    $c = \Carbon\Carbon::createFromFormat($format, $input);

    $exist = false;

    $exist = \App\Manpower::where('job_date','=', $c->toDateString())->where('project_id',$pr)->exists();

    return $exist;

}

function gt($t)
    {

        list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

        $punchInTime = $t;

        $formatedPunchInTime = date( 'H:i:s', strtotime( $punchInTime ) );

        $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $formatedPunchInTime, 'UTC') ->setTimezone($timezone);

        $formatedPunchInTime = date( 'h:i A', strtotime( $formatedPunchInTime ) );

        return $formatedPunchInTime;

    }

/*
 * Get timezone select box
 *
 *
 */
function getTimezoneSelectOptions( $selectedOption )
{

    $options = array("Pacific/Midway,-11:00","America/Adak,-10:00","Etc/GMT+10,-10:00","Pacific/Marquesas,-09:30","Pacific/Gambier,-09:00","America/Anchorage,-09:00","America/Ensenada,-08:00","Etc/GMT+8,-08:00","America/Los_Angeles,-08:00","America/Denver,-07:00","America/Chihuahua,-07:00","America/Dawson_Creek,-07:00","America/Belize,-06:00","America/Cancun,-06:00","Chile/EasterIsland,-06:00","America/Chicago,-06:00","America/New_York,-04:00","America/Havana,-04:00","America/Bogota,-05:00","America/Caracas,-04:30","America/Santiago,-04:00","America/La_Paz,-04:00","Atlantic/Stanley,-04:00","America/Campo_Grande,-04:00","America/Goose_Bay,-04:00","America/Glace_Bay,-04:00","America/St_Johns,-03:30","America/Araguaina,-03:00","America/Montevideo,-03:00","America/Miquelon,-03:00","America/Godthab,-03:00","America/Argentina/Buenos_Aires,-03:00","America/Sao_Paulo,-03:00","America/Noronha,-02:00","Atlantic/Cape_Verde,-01:00","Atlantic/Azores,-01:00","Europe/Belfast,+00:00","Europe/Dublin,+00:00","Europe/Lisbon,+00:00","Europe/London,+00:00","Africa/Abidjan,+00:00","Europe/Amsterdam,+01:00","Europe/Belgrade,+01:00","Europe/Brussels,+01:00","Africa/Algiers,+01:00","Africa/Windhoek,+01:00","Asia/Beirut,+02:00","Africa/Cairo,+02:00","Asia/Gaza,+02:00","Africa/Blantyre,+02:00","Asia/Jerusalem,+02:00","Europe/Minsk,+02:00","Asia/Damascus,+02:00","Europe/Moscow,+03:00","Africa/Addis_Ababa,+03:00","Asia/Tehran,+03:30","Asia/Dubai,+04:00","Asia/Yerevan,+04:00","Asia/Kabul,+04:30","Asia/Yekaterinburg,+05:00","Asia/Tashkent,+05:00","Asia/Kolkata,+05:30","Asia/Katmandu,+05:45","Asia/Dhaka,+06:00","Asia/Novosibirsk,+06:00","Asia/Rangoon,+06:30","Asia/Bangkok,+07:00","Asia/Krasnoyarsk,+07:00","Asia/Hong_Kong,+08:00","Asia/Irkutsk,+08:00","Australia/Perth,+08:00","Australia/Eucla,+08:45","Asia/Tokyo,+09:00","Asia/Seoul,+09:00","Asia/Yakutsk,+09:00","Australia/Adelaide,+09:30","Australia/Darwin,+09:30","Australia/Brisbane,+10:00","Australia/Hobart,+10:00","Asia/Vladivostok,+10:00","Australia/Lord_Howe,+10:30","Etc/GMT-11,+11:00","Asia/Magadan,+11:00","Pacific/Norfolk,+11:30","Asia/Anadyr,+12:00","Pacific/Auckland,+12:00","Etc/GMT-12,+12:00","Pacific/Chatham,+12:45","Pacific/Tongatapu,+13:00","Pacific/Kiritimati,+14:00");

    foreach ($options as $option)
    {
        
        $selected = ( $option == $selectedOption ) ? 'selected=""' : '';

        $optionArray[] = "<option value='$option' $selected>$option</option>";

    }

    return $optionArray;

}

 /*
 * Worker request count
 *
 *
 */
function getCompanyNameFromId( $id )
{

    return User::find( $id )->company_name;

}

/*
 *
 *
 *
 */
function getTimeZonePunchDate( $punchAtDate ){

    list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

    $gmt = explode(':', $timeDifference)[0].' Hours';
    //$gmt = explode(':', $timeDifference)[0].'';

    $date = date("m-d-Y", strtotime( $gmt, strtotime( $punchAtDate ) ) );

    return $date;
    
}

function makeSsnFormat( $excelSsn, $subContractorId )
{
    $ssn = substr($excelSsn, -4);

    $companyName = substr(trim(User::where('id', $subContractorId)->first()->company_name), 0, 4);
    
    return strtoupper($companyName.$ssn);
}

function getPayrollErrors( $payrollId )
{
    $payrollData = \App\Payroll::find( $payrollId );

    if( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->count() > 1 ):

        $count = 0;

        $html = [];

        foreach( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->get() as $get ):

            $rowData = json_decode( $get->file_data );

            $errors =  [];

            $wrongSsn = $payrollData->getSsnStatus( $rowData[1], $payrollData->author );

            if( strcasecmp($rowData[0],$payrollData->getWorkerName( $rowData[0], $rowData[1], $payrollData->author ) ) != 0 && !$wrongSsn ):

                array_push( $errors, "Worker name is incorrect." );                  

            endif;


            //
            if( (!$payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) || $payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) != 'equal') && !$wrongSsn && $rowData[6] != 0 )
            {
                array_push( $errors, "Total hours doesn't match." );
            }      
                                                   
            //
            if( $payrollData->checkGross( $rowData[7], $rowData[13] ) )
            {
                array_push( $errors, 'Gross pay is less than Net pay.' );
            }

            //
            if( ( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ) < 15 ) && !$wrongSsn )
            {
                array_push( $errors, 'The hourly rate is less than $15.' );
            }

            // SSN CASE
            if( $wrongSsn )
            {
                $errors =  [];
                array_push( $errors, "Incorrect SSN or worker not found." );
            }
            
            foreach( $errors as $error )
            {
                $html[$rowData[0]][] = "<span><p>- $error</p></span>";
            }

        endforeach;

        unset( $html['Company Totals'] );
        return $html;

    endif;

}

function getPayrollErrorsContent( $payrollId )
{
    $payrollData = \App\Payroll::find( $payrollId );

    if( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->count() > 1 ):

        $count = 0;

        $html = [];

        foreach( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->get() as $get ):

            $rowData = json_decode( $get->file_data );

            $errors =  [];

            $wrongSsn = $payrollData->getSsnStatus( $rowData[1], $payrollData->author );


            //
            if( (!$payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3]) || $payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3]) != 'equal') && !$wrongSsn && $rowData[6] != 0 )
            {
                array_push( $errors, "Total hours doesn't match." );
            }      
                                                   
            //
            if( $payrollData->checkGross( $rowData[7], $rowData[13] ) )
            {
                array_push( $errors, 'Gross pay is less than Net pay.' );
            }

            //
            if( ( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ) < 15 ) && !$wrongSsn )
            {
                array_push( $errors, 'The hourly rate is less than $15.' );
            }

            // SSN CASE
            if( $wrongSsn )
            {
                $errors =  [];
                array_push( $errors, "Incorrect SSN or worker not found." );
            }
            
            foreach( $errors as $error )
            {
                $html[$rowData[0]][] = "<span><p>- $error</p></span>";
            }

        endforeach;

        unset( $html['Company Totals'] );
        return $html;

    endif;

}

function updateRole($id){
   $user = User::find($id);
    if(!empty($user->worker_type)){
     $user->role = ($user->worker_type == 9) ? "safetyo" : "worker";
 }else{
     $user->role = "foreman"; 
 }
    $user->save(); 
    return true;
    
}
?>