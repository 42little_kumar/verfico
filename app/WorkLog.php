<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\WorkerType;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;

class WorkLog extends Model
{
    
	protected $table = 'worker_log';

	protected $appends = [
		'project_name',
		'project_number',
		'total_punch_time',
		'worker_name',
		'rsc_employee_id',
		'company_name',
		'worker_title',
		'punch_date',
		'punch_in_time',
		'punch_out_time'
	];

	protected $fillable = [

		'worker_id',
		'sub',
		'project_id',
		'punch_in',
		'in_type',
		'out_type',
		'punch_out',
		'punch_at',
		'created_at',
		'device_info_in',
		'device_info_out'

	];

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getProjectNameAttribute()
	{

		return $this->project->job_name;

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getProjectNumberAttribute()
	{

		return $this->project->job_number;

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getWorkerNameAttribute()
	{
		// $routeName = Route::currentRouteName();
		// if($routeName == 'admin.project.logs.all.export'){
		// 	$classification = !empty($this->user->classification) ? ' - '.$this->user->classification : '';
		// 	return @$this->user->first_name.' '.@$this->user->last_name.$classification;
		// }
		return @$this->user->first_name.' '.@$this->user->last_name;

	}
	public function getRscEmployeeIdAttribute()
	{
		
		return @$this->user->rsc_employee_id;

	}
	/*
	 * @Function Name
	 *
	 *
	 */
	public function getClassificationAttribute(){
		 $routeName = Route::currentRouteName();
		if($routeName == 'admin.project.logs.all.export'){
			$classification = !empty($this->user->classification) ? $this->user->classification : '';
			return $classification;
		}

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getPunchDateAttribute()
	{

		$date = getTimeZonePunchDate( $this->punch_at );

        return $date;

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getCompanyNameAttribute()
	{


		$parent = User::where( 'id', @$this->user->parent )->first();

		return @$parent->company_name;

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getWorkerTitleAttribute()
	{

		$workerTypeId = @$this->user->worker_type;

		$typeData = WorkerType::where('id', $workerTypeId)->first();

		return @$typeData->type;

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getPunchInTimeAttribute()
	{

		list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

		$punchInTime = $this->punch_in;

		$formatedPunchInTime = date( 'H:i:s', strtotime( $punchInTime ) );

		$formatedPunchInTime = Carbon::createFromFormat('H:i:s', $formatedPunchInTime, 'UTC') ->setTimezone($timezone);

		$formatedPunchInTime = date( 'h:i A', strtotime( $formatedPunchInTime ) );

		return $formatedPunchInTime;

	}
	
	/*
	 * @Function Name
	 *
	 *
	 */
	public function getPunchOutTimeAttribute()
	{

		list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

		$punchOutTime = $this->punch_out;

		if( empty( $punchOutTime ) )
		return ' ';

		$formatedPunchOutTime = date( 'H:i:s', strtotime( $punchOutTime ) );

		$formatedPunchOutTime = Carbon::createFromFormat('H:i:s', $formatedPunchOutTime, 'UTC') ->setTimezone($timezone);

		$formatedPunchOutTime = date( 'h:i A', strtotime( $formatedPunchOutTime ) );

		return $formatedPunchOutTime;

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function getTotalPunchTimeAttribute()
	{

		$punchIn = substr_replace($this->punch_in, '00', -2);
            $punchOut = substr_replace($this->punch_out, '00', -2);
			
			$start = date("Y-m-d ".$punchIn);
			
			if(strtotime($punchOut) < strtotime($punchIn)){
				
				$end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$punchOut) ) + 24*60*60) ;
			} else {
				$end = date("Y-m-d ".$punchOut);
			}

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

            return $totalTime;

	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function project()
	{
	    return $this->belongsTo( Project::class, 'project_id' )->withTrashed();
	}

	/*
	 * @Function Name
	 *
	 *
	 */
	public function user()
	{
	    return $this->belongsTo( User::class, 'worker_id' )->withTrashed();
	}

}
