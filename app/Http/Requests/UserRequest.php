<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [

                        'image' => 'mimes:jpeg,png,jpg,gif,svg',

                        'first_name' => 'required|string',

                        'last_name' => 'required|string',

                        'phone_number' => 'required|unique:users,phone_number',

                        'parent' => 'required|string',

                        'worker_type' => 'required|string',

                        'password' => 'required|string|min:6|max:15',

                        'device_id' => 'required|string',

                        'device_type' => 'required|string',

                        'device_token' => 'required|string',

                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [

                        'name' => 'required|string',

                        'country_id' => 'required',

                    ];
                }
            default:break;
            }
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {

        $errors = (new ValidationException($validator))->errors();

        foreach ( $errors as $get )
        {
            $response = [

                'success' => false,

                'message'    => $get[0],

            ];

            $code = '404';

            throw new HttpResponseException( response()->json( $response, $code ) );
        }

        /*$errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));*/
    }

    
}
