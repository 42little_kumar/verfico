<?php

namespace App\Http\Requests;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class PunchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'worker_id' => 'required|integer',
                        'project_id' => 'required|integer',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'id' => 'required|integer',
                        'project_id' => 'required|integer'
                    ];
                }
            default:break;
            }
    }



    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        foreach ( $errors as $get )
        {
            $response = [
                'success' => false,
                'message'    => $get[0],
            ];
            $code = '404';
            throw new HttpResponseException( response()->json( $response, $code ) );
        }
    }
}
