<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\Project;
use App\Announcement;
use App\WorkerDocuments;
use App\Document;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use Mail;
use App\Mail\ApproveWorker;
use App\Mail\ConfirmRegistrationMobileWorker;
use App\Mail\ConfirmRegistrationWorker;
use App\WorkerType;
use Maatwebsite\Excel\Facades\Excel;

class AnnouncementController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $worker = Announcement::where('type', 'worker')->first();

        $foreman = Announcement::where('type', 'foreman')->first();

        return view('admin.announcements.index')
        ->with('worker', $worker)
        ->with('foreman', $foreman);

    }

    public function logMerger( Request $request )
    {

        $workers = User::role('worker')->where('status', 2)->get();

        return view('admin.log.merge')
        ->with('workers', $workers);

    }

    public function logMergerSave( Request $request )
    {

        DB::table('worker_log')->where('worker_id', $request->todelete)->update( [ 'worker_id' => $request->tokeep ] );

        User::find( $request->todelete )->delete();

        Session::flash('success', 'Worker has been merged.');

        return redirect()->back();

    }

    public function getMergerLogs( Request $request )
    {

        $mergerLogs = DB::table('worker_log')->where('worker_id', $request->id)->orderBy('id', 'desc')->limit(10)->get();

        $html = '<h5>Latest 10 logs</h5><ul>';

        foreach ($mergerLogs as $g)
        {
            $html .= '<li>'. $this->gt( $g->punch_in ) .' - '. $this->gt( $g->punch_out ) .' <span class="lg-date">('. $this->gtd( $g->punch_at ) .')</span></li>';
        }

        $html .= '</ul>';

        if( count( $mergerLogs ) == 0 )
            $html = 'No Logs';

        return $html;

    }

    public function gtd($t)
    {

        list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

        $punchInTime = $t;

        $formatedPunchInTime = date( 'H:i:s', strtotime( $punchInTime ) );

        $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $formatedPunchInTime, 'UTC') ->setTimezone($timezone);

        $formatedPunchInTime = date( 'd M, Y', strtotime( $t ) );

        return $formatedPunchInTime;

    }

    public function gt($t)
    {

        list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

        $punchInTime = $t;

        $formatedPunchInTime = date( 'H:i:s', strtotime( $punchInTime ) );

        $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $formatedPunchInTime, 'UTC') ->setTimezone($timezone);

        $formatedPunchInTime = date( 'h:i A', strtotime( $formatedPunchInTime ) );

        return $formatedPunchInTime;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save( Request $request )
	{

		$user = Announcement::firstOrNew( array( 'type' => 'foreman' ) );

        $user->type = 'foreman';
        
        $user->show_popup = ( $request->show_popup_foreman ) ? 1 : 0;

		$user->announcement_content = $request->content_foreman;

        $user->save();
        
        //

        $user = Announcement::firstOrNew( array( 'type' => 'worker' ) );

        $user->type = 'worker';
        
        $user->show_popup = ( $request->show_popup_worker ) ? 1 : 0;;

		$user->announcement_content = $request->content_worker;

		$user->save();

		Session::flash('success', 'Announcements has been updated successfully.');

        return redirect()->back();

	}

}