<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Payroll;
use App\PayrollFiles;
use App\Http\Controllers\Api\NotificationController as NotificationController;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use DB;
use Carbon\Carbon;
use Mail;
use App\Mail\ReportCron;

class ReportNotificationCron extends ApiBaseController
{
    function sendReportNotificationCron()
    {

        $payrollFiles = PayrollFiles::select('payroll_id')->distinct('payroll_id')->get();

        $users = User::role('subcontractor')->get();

        foreach( $users as $user )
        {

            $date = \Carbon\Carbon::today()->subDays(14);

            $payrolls = Payroll::where('author', $user->id)->where('end_date', '>=', $date)->get();


            foreach( $payrolls as $payroll )
            {


                $payrollFile = PayrollFiles::where('payroll_id', $payroll->id)->first();

                if( empty( $payrollFile ) )
                {

                    $hasLogs = false;

                    $workers = User::where('parent', $user->id)->where('status', 2)->get();

                    foreach( $workers as $worker )
                    {

                        if( $worker->logs()->count() > 0 )
                        $hasLogs = true;

                    }

                    if( $hasLogs )
                    {
                        
                        $notificationText = "We've noticed that you have not uploaded the Payroll Report from last two weeks.";

                        Mail::to( $user->email )->send( new ReportCron( $user, $notificationText ) );

                    }

                }

                
            }
           

        }

        die;
        
    }

}