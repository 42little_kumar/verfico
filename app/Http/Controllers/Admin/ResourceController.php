<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\Project;
use App\WorkerDocuments;
use App\Document;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use Mail;
use App\Mail\ApproveWorker;
use App\Mail\ConfirmRegistrationMobileWorker;
use App\Mail\ConfirmRegistrationWorker;
use App\WorkerType;
use App\Resource;
use App\Pagecontent;
use Maatwebsite\Excel\Facades\Excel;

class ResourceController extends Controller
{

    public function fileDelete( $resourceId, $fileField )
    {

        $data = [];

        $resourceObj = Resource::find( $resourceId );

        $data[$fileField] = null;

        $resourceObj->update( $data );

        Session::flash('success', 'File deleted successfully.');

        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
      
        $resouce = Resource::all();

        /* Companies */

        return view('admin.resource.index');
        
    }

    /*
     * @Datatable get
     *
     *
     */
    public function changeStatusWorker( Request $request )
    {

        $workerDocObject = WorkerDocuments::find( $request->id );
        $workerDocObject->show = $request->showVal;
        $workerDocObject->save();
    }

    /*
     * @Datatable get
     *
     *
     */
    public function getResources( Request $request )
    {

        $resource = Resource::all();

        return Datatables::of($resource)
        ->addColumn('name', function($resource)
        {
            $editLink = route('admin.resource.edit', $resource->id);

          return "<a href='$editLink'>".$resource->name."</a>";

        })
        ->addColumn('description', function($resource)
        {

          

          return $resource->description;

        })
        ->addColumn('action', function($resource)
        {

            $viewLink = '';

            $editLink = route('admin.resource.edit', $resource->id);

            $deleteLink = route('admin.resource.remove', $resource->id);;

            $deleteHtml = '';

           
              $deleteHtml = '<hr>
                                   <div>
                                   <a href="' . $deleteLink . '" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
                                      Delete
                                    </a>
                                  </div>';


            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">


                                  <div>
                                     <a href="' . $editLink . '" class="btn btn-sm btn-primary btn-circle lc-id" title="Edit">
                                       Edit
                                    </a>
                                  </div>

                                   '.$deleteHtml.'
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['name', 'description', 'action'] )
        ->make( true );
    }

    /*
     * @Datatable get
     *
     *
     */
    public function workerExport( Request $request, $subId = null )
    {

        if( $subId == null )
        {

            $allWorkers = User::role('worker')
            ->where( 'status', 2 )
            ->get();

            $csvExporter = new \Laracsv\Export();

            $csvExporter->build($allWorkers, ['first_name', 'last_name', 'email', 'created_at'])->download();

        }
        else
        {

            $allWorkers = User::role('worker')
            ->where( 'status', 2 )
            ->where( 'parent', $subId )
            ->get();

            $csvExporter = new \Laracsv\Export();

            $csvExporter->build($allWorkers, ['first_name', 'last_name', 'email', 'created_at'])->download();

        }

    }

     /*
     * @Datatable get
     *
     *
     */
    public function getRequests( Request $request )
    {

        $userType = '';

        $userObject = User::orderBy( 'id', 'DESC' )->where('status', 1);

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

            $userObject->role( $userType );
            
        }

        // Status filter

        if( isset( $request->status ) )
        {

            $userObject->where( 'status', $request->status );

        }

        // Company filter

        if( isset( $request->company ) )
        {

            $userObject->where( 'parent', $request->company );

        }

        // Title filter

        if( isset( $request->title ) )
        {

            $userObject->where( 'worker_type', $request->title );

        }

        $users = $userObject->get();

        return Datatables::of($users)
        ->addColumn('name', function($user)
        {

          $viewLink = route('admin.users.view', [ 'id' => $user->id ]);

          $userName = '<div class="worker_name_td"><a class="index-link" href="'.$viewLink.'?request=1"><p>'.$user->first_name.' '.$user->last_name.'</p></a><p>'.$user->email.'</p></div>';

          return $userName;

        })
        ->addColumn('email', function($user)
        {
            
           

        })
        ->addColumn('company', function($user)
        {
            
            return $user->sub_contractor_name;

        })
        ->addColumn('created_at', function($user)
        {
            
            return \Carbon\Carbon::parse($user->created_at)->tz(str_before(auth()->user()->timezone_manual, ','))->format(("m-d-Y"));

        })
        ->addColumn('worker_type', function($user)
        {
            
           return empty( $user->worker_type_name ) ? '<i>Not Selected</i>' : ucwords( $user->worker_type_name );

        })
        ->addColumn('status', function($user)
        {

            switch ( $user->status ) {
                case 0:
                    $html = '<span class="status_active">Blocked</span>';
                    break;
                case 1:
                    $html = '<span class="status_active">Pending</span>';
                    break;
                default:
                    $html = '<span class="status_active">Active</span>';
                    break;
            }
            
            return $html;
            
        })
        ->addColumn('action', function($user)
        {

            $approveLink = route('admin.users.approve', [ 'id' => $user->id ]);

            $rejectLink = route('admin.users.reject', [ 'id' => $user->id ]);

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $approveLink . '" class="btn btn-sm btn-info btn-circle btn-action-approve" title="Approve">
    
                                           Approve
    
                                      </a>
                                  </div> 

                                  <div>
                                     <a href="' . $rejectLink . '" class="btn btn-sm btn-info btn-circle btn-action-reject" title="Reject">
    
                                           Reject
    
                                      </a>
                                  </div> 
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['name', 'worker_type', 'company', 'email', 'status', 'action'] )
        ->make( true );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function workerRequests( Request $request )
    {

        $userType = 'worker';

        $userTitle = 'Worker';

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

        }
     
        //$users = User::role('user');

        $users = User::paginate( 20 );

        /* Worker Types */

        $titlesList = WorkerType::get();

        /* Worker Types */

        /* Companies */

        $companiesList = User::role('subcontractor')->get();

        /* Companies */

       return view('admin.users.requests', compact( 'users', 'userType', 'userTitle', 'titlesList', 'companiesList' ));

    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request )
    {
        
        return view( 'admin.resource.create' );

    }

    /**
     * Save worker documents.
     *
     * 
     * 
     */
    public function saveWorkerDocuments( $request, $workerId )
    {

        $workerDocument = new WorkerDocuments;

        if( $request->notes_training ):

        foreach( $request->notes_training as $key => $value)
        {

            $documentName = null;

            $isSave = false;

            if( $request->hasFile( 'worker_document' ) || !empty($value) ) {

                $isSave = true;

            }

            $workerDocument = new WorkerDocuments;

            if( @$request->hasFile( 'worker_document' ) ) {

                /* Upload */

                try {

                    $destination = public_path('uploads/documents/'. $workerId);

                    if(!File::isDirectory($destination)) {

                        mkdir($destination, 0777, true);

                        chmod($destination, 0777);

                    }

                    $documentName = 'document-' . time() . '-' . Str::random(15) . '.' . $request->worker_document[$key]->getClientOriginalExtension();

                    $request->worker_document[$key]->move($destination, $documentName);

                } catch(\Exception $e) {

                    $documentName = null;

                }

            } else {

                $documentName = null;

            }

            /* Upload */

            if( $isSave ):

            $workerDocument->user_id = $workerId;
            $workerDocument->author = Auth::user()->id;
            $workerDocument->notes = $value;
            $workerDocument->document = $documentName;
            $workerDocument->show = ( isset( $request->doc_upload[$key] ) ) ? $request->doc_upload[$key] : 0;

            $workerDocument->save();

            endif;

            

        }

        endif;

    }

    public function store(Request $request)
    {

      $rfa = [];

      $data = [];

      if( $request->hasFile('rfile') ) {

          foreach ( $request->rfile as $key => $rfile) {
            
            $destination = public_path('uploads/resources');

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }


             $filen = substr($rfile->getClientOriginalName(), 0, -4);



            $resouceName = $filen. '--'.uniqid().'--.' . $rfile->getClientOriginalExtension();

            $rfile->move($destination, $resouceName);
            
            $rfa[$key] = $resouceName;

            //Document::create( $saveData );

          }

        }



        $data['name'] = $request->resource_name;
        $data['description'] = $request->resource_description;
        $data['file_1'] = @$rfa[0];
        $data['file_2'] = @$rfa[1];
        $data['file_3'] = @$rfa[2];
        $data['file_4'] = @$rfa[3];
        $data['file_5'] = @$rfa[4];

       

       Resource::create( $data );

       Session::flash('success', 'Resource has been created successfully.');
        
        return redirect( route('admin.resource') );

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contentIndex( Request $request )
    {

        $worker = DB::table('pagecontent')->where('type', 'worker')->first();

        $data = DB::table('pagecontent')->first();

        return view('admin.resource.pagecontent')
        ->with('data', $data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contentSave( Request $request )
  {

    DB::table('pagecontent')->where('type', 'blogcontent')->delete();


    $data = [];

    $data['pagecontent'] = $request->pagecontent;;

    Pagecontent::create( $data );
      

       

    Session::flash('success', 'Content has been updated successfully.');

        return redirect()->back();

  }

    public function edit( Request $request, $id )
    {

      $data = Resource::find( $id );

       return view( 'admin.resource.edit', compact( 'data' ) );

    }

    public function update( Request $request )
    {

      $rfa = [];

      $data = [];

      if( $request->hasFile('rfile') ) {

          foreach ( $request->rfile as $key => $rfile) {
            
            $destination = public_path('uploads/resources');

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

             $filen = substr($rfile->getClientOriginalName(), 0, -4);

            $resouceName = $filen. '--'.uniqid().'--.' . $rfile->getClientOriginalExtension();

            $rfile->move($destination, $resouceName);
            
            $rfa[$key] = $resouceName;

            //Document::create( $saveData );

          }

        }

        $data['name'] = $request->resource_name;
        $data['description'] = $request->resource_description;

        //dd( $rfa );

        if( isset( $rfa[0] ) )
        $data['file_1'] = @$rfa[0];

        if( isset( $rfa[1] ) )
        $data['file_2'] = @$rfa[1];

        if( isset( $rfa[2] ) )
        $data['file_3'] = @$rfa[2];

        if( isset( $rfa[3] ) )
        $data['file_4'] = @$rfa[3];

        if( isset( $rfa[4] ) )
        $data['file_5'] = @$rfa[4];

        $robject = Resource::find( $request->id );
        $robject->update( $data );

      

       Session::flash('success', 'Resource has been updated successfully.');

       return redirect()->back();


    }

    public function destroy($id)
    {

      Resource::find($id)->delete();

      Session::flash('success', 'Resource has been deleted successfully.');

      return redirect( route('admin.resource') );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store1(Request $request)
    {

        $parent = 0;

        $validator = Validator::make($request->all(), [

            'first-name' => 'required',

            'password' => 'required',

        ]);


        if($validator->fails()) {

            $request->flash();
        
            if($validator->errors()->first('first-name')) {
        
                Session::flash('error', 'Please enter first name of user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('email')) {
        
                Session::flash('error', 'Email already exists is system.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('phone-number')) {
        
                Session::flash('error', 'Phone number already exists is system.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('password')) {
        
                Session::flash('error', 'Please enter password for the user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('username')) {
        
                Session::flash('error', 'Please enter username of user.');
        
                return redirect()->back();
        
            }
        
        }

        $additional_validator = Validator::make($request->all(), [

            'password' => [ 'min:6', 'max:18' ]

        ]);

        if($additional_validator->fails()) {

            $request->flash();

            if($additional_validator->errors()->first('email')) {
        
                Session::flash('error', 'Please enter valid email address of user.');
        
                return redirect()->back();
        
            }

            if($additional_validator->errors()->first('password')) {
        
                Session::flash('error', 'Password should be of atleast 6 characters and not more than 18 characters.');
        
                return redirect()->back();
        
            }
        
        }

        $userStatus = 2;

        $companyName = NULL;

        $first_name = $request->input('first-name');

        $last_name = $request->input('last-name');

        $worker_type = $request->input('worker_type');

        $email = strtolower($request->input('email'));

        $username = rand();

        $password = Hash::make($request->input('password'));

        $country_code = null;

        $phone_number = null;

        if( $request->filled('company-name') )
        {

            $companyName = $request->input('company-name');

        }

        if($request->filled('phone-number')) {

            $country_code = null;

            $phone_number = preg_replace('/[^A-Za-z0-9]/', '', $request->input('phone-number'));

        }

        $gender = $request->input('gender');

        /* If email and phone number not empty then validate them */

        if( !empty( $email ) )
        {

          if(User::where('email', $email)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Email already linked with another user. Please try again with different information.');

            return redirect()->back();

          }

        }

        if( !empty( $phone_number ) )
        {

          if(User::where('phone_number', $phone_number)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Phone number already linked with another user. Please try again with different information.');

            return redirect()->back();

          }

        }

        /* If email and phone number not empty then validate them */

        if($username && User::where('username', $username)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Username already linked with another user. Please try again with different information.');

            return redirect()->back();

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        if( isset( $request->general_contractor ) && !empty( $request->general_contractor ) )
        {

            if( isset( $request->user_role ) && ( $request->user_role == 'worker' || $request->user_role == 'foreman' ) )
            {
                
                $userRole = $request->input('user_role');

                $parent = $request->general_contractor;

            }

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $emailUserData = [];

        $emailUserData['first_name'] = $request->input('first-name');
        $emailUserData['name'] = 'Name: '.$request->input('first-name').' '.$request->input('last-name').'<br>';
        $emailUserData['email'] = $request->input('email');
        $emailUserData['phone_number'] = '';
        $emailUserData['user_role'] = $request->input('user_role');

        $workerCompanyfieldForEmail = '';

        if( $request->input('user_role') == 'subcontractor')
        {

          $emailUserData['name'] = 'Company Name: '.$request->input('company-name').'<br>';

        }

        if( $request->input('user_role') == 'worker')
        {

          $workerCompanyfieldForEmail = 'Company: '.getCompanyNameFromId($request->input('general_contractor'));

          $workerCompanyfieldForEmail = '<br>'.$workerCompanyfieldForEmail.'<br><br>';

        }

        if( !empty( $phone_number ) )
        {

          $emailUserData['phone_number'] = 'Phone number: '.$phone_number.'<br>';

        }

        //
        Mail::to($email)->send(new ConfirmRegistrationWorker((object)$emailUserData, 'Mail', $request->input('password'), $workerCompanyfieldForEmail));

        $user = User::create([

            'username' => $username,

            'first_name' => $first_name,

            'last_name' => $last_name,

            'payroll_first_name' => $request->payroll_first_name,

            'payroll_last_name' => $request->payroll_last_name,

            'payroll_email' => $request->payroll_email,

            'payroll_phone' => $request->payroll_phone,

            'worker_type' => $worker_type,

            'ssn_id' => ( $request->input('ssn_id') ) ? strtoupper( $request->input('ssn_id_pre').$request->input('ssn_id') ) : null,

            'parent' => $parent,

            'email' => $email,

            'company_name' => $companyName,

            'password' => $password
            
        ]);

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $userRole = 'worker';
    
        if( isset( $request->user_role ) )
        {
            
            $userRole = $request->input('user_role');

        }
       
        $user->assignRole( $userRole );

        if( $userRole == 'worker' )
        {
            $userStatus = 2;
        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $user->phone_number = $phone_number;
        
        $user->country_code = $country_code;

        $user->gender = $gender;

        $user->status = $userStatus;

        $user->email_verified_at = Carbon::now();

        $user->phone_number_verified_at = $phone_number ? Carbon::now() : null;

        if($request->hasFile('image')) {

            $format_validator = Validator::make($request->all(), [

                'image' => 'mimes:jpeg,png,jpg,gif,svg'
    
            ]);
    
            if($format_validator->fails()) {

                $request->flash();
    
                Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');
    
                return redirect()->back();
    
            }

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('image');

			$image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);
            
            $user->image = $image_name;

        }


        $this->saveWorkerDocuments( $request, $user->id );

        /* Document upload */

        $subContId = $user->id;

        if( $request->hasFile('document') ) {

          foreach ( $request->document as $document) {
            
            $destination = public_path('uploads/documents/'. $subContId);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $documentName = 'document-' . time() . '-' . Str::random(15) . '.' . $document->getClientOriginalExtension();

            $document->move($destination, $documentName);
            
            $saveData['user_id'] = $subContId;
            $saveData['document'] = $documentName;

            Document::create( $saveData );

          }

        }

        /* Document upload */

        if($request->input('cropped-image')){

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $request->input('cropped-image-type');

            $imageInfo = $request->input('cropped-image'); 

            $image = fopen($imageInfo, 'r');
            
            file_put_contents($destination.'/'.$image_name, $image);

            fclose($image); 

            $user->image = $image_name;
        }


        $user->save();

        

        Session::flash('success', 'New user has been created successfully.');

        Session::flash('status-user', $user->id);

        $redirectQuery = 'userType='.$userRole;

        return redirect()->route('admin.users', $redirectQuery);

    }

    public function getWorkerOrderDetails( Request $request )
    {

        $cl = 'id';
        $od = 'desc';
        $st = '>';
        $id = $request->user_id;
        $pm = 'id';

        if( isset( $request->od ) )
        $od = $request->od;

        $worker = User::find( $request->userId );

        if( isset( $request->od ) && $request->od == 'asc' )
        $st = '<';

        if( $request->cl == 0 )
        {
            $cl = 'first_name';
            $pm = $worker->first_name;
        }
        

        if( $request->cl == 2 )
        {
            $cl = 'phone_number';
            $pm = $worker->phone_number;
        }
        

        if( $request->cl == 3 )
        {
            $cl = 'company_name';
            $pm = $worker->company_name;
        }
        

        if( $request->cl == 6 )
        {
            $cl = 'created_at';
            $pm = $worker->created_at;
        }

        $prst = '>';
        $nxtOd = 'desc';

        if( $st == '>' )
        {
            $prst = '<';
            $nxtOd = 'asc';
        }
        
        
   
        $next = User::role('worker')->orderBy($cl, $nxtOd)->where($cl, $st, $pm)->first();

        $previous = User::role('worker')->orderBy($cl, $od)->where($cl, $prst, $pm)->first();

        echo $next->first_name;

        echo '<br>';

        echo $previous->first_name;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $user = User::with('roles')->where('id', $id)->firstOrFail();

        $logs = $user->logs()->latest()->get();

        $roles = json_decode( $user->roles );

        $userRole = @$roles[0]->name;
		
		    $previous = null;
		
        $next = null;
        
        if(  isset( $_GET["dir"] ) ):
            
        
		if(@$_GET["from"]=="workers"){
			
			if(@$_GET["column"]==2){
				

				 
                    $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('phone_number',@$_GET["dir"])->orderBy('users.first_name','desc');

                    if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                    {

                        $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('phone_number',@$_GET["dir"])->orderBy('users.first_name','desc');

                    }
                    
                    if( @$_GET["status"] != 'null' )
                    {
                        $usersobj->where( 'status', @$_GET["status"] );
                    }

                    if( @$_GET["company"] != 'null' )
                    {
                        $usersobj->where( 'parent', @$_GET["company"] );
                    }

                    if( @$_GET["title"] != 'null' )
                    {

                        $usersobj->where( 'worker_type', @$_GET["title"] );
                    }
                    
                    $users = $usersobj->get();

					$index = null;
					$i=0;
					foreach($users as $user){
						if($user->id == $id){
							$index = $i;
							break;
						}
						$i++;
					}
					if(isset($users[$i+1])){
						$next = $users[$i+1];
					}
					if(isset($users[$i-1])){
						$previous = $users[$i-1];
					}
					
					
				
			}
			if(@$_GET["column"]==0){
				

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"]);
				    
				            if( $userRole == 'Subcontractor' )
                    {

                        $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('company_name',@$_GET["dir"]);

                    }

                    if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                    {


                        $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('first_name',@$_GET["dir"]);

                    }
                    
                    
                    if( @$_GET["status"] != 'null' )
                    {
                        $usersobj->where( 'status', @$_GET["status"] );
                    }

                    if( @$_GET["company"] != 'null' )
                    {
                        $usersobj->where( 'parent', @$_GET["company"] );
                    }

                    if( @$_GET["title"] != 'null' )
                    {

                        $usersobj->where( 'worker_type', @$_GET["title"] );
                    }

                    $users = $usersobj->get();
					
					$index = null;
					$i=0;
					foreach($users as $user){
						if($user->id == $id){
							$index = $i;
							break;
						}
						$i++;
					}
					if(isset($users[$i+1])){
						$next = $users[$i+1];
					}
					if(isset($users[$i-1])){
						$previous = $users[$i-1];
					}
					
					
				
            }
            
            if(@$_GET["column"]==6){
				
				
				 
                $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('created_at',@$_GET["dir"]);

                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                    {

                        $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('created_at',@$_GET["dir"]);

                    }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }

                $users = $usersobj->get();
                
                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }

        
        if(@$_GET["column"]==3){
				

           $usersobj = User::role($userRole)->where('status', '2')->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"])->where('id', 'parent');



           if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }

            if( @$_GET["company"] != 'null' )
            {
                $usersobj->where( 'parent', @$_GET["company"] );
            }

            if( @$_GET["title"] != 'null' )
            {

                $usersobj->where( 'worker_type', @$_GET["title"] );
            }

            $users = $usersobj->get();
            
            $index = null;
            $i=0;
            foreach($users as $user){
                if($user->id == $id){
                    $index = $i;
                    break;
                }
                $i++;
            }
            if(isset($users[$i+1])){
                $next = $users[$i+1];
            }
            if(isset($users[$i-1])){
                $previous = $users[$i-1];
            }
            
            
        
    }

        }
        
        endif;
		
		//print_r($previous->id);
		//print_r($next->id);
		//die;

        return view( 'admin.users.view', compact( 'user', 'logs', 'userRole','previous','next' ) );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit1($id)
    {

        if( !Auth::user()->hasRole('Superadmin') && isset( $_REQUEST['userType'] ) && ( $_REQUEST['userType'] == 'admin' || $_REQUEST['userType'] == 'superadmin' ) )
        {

          return abort(404);

        }

        $fieldPrefix = '';

        $subContractors = User::role('subcontractor')->get();

        $workerTypes = WorkerType::get();
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::with('roles')->where('id', $id)->firstOrFail();

        if( $user->hasRole('Subcontractor') )
        {

            $fieldPrefix = 'Contact ';

        }

        $roles = json_decode( $user->roles );

        $userRole = $roles[0]->name;
        
        //$countries = CountryCode::orderBy('name', 'ASC')->get();

        if( isset( $_GET["dir"] ) ):
		
            if(@$_GET["column"]==2){
				

				 
                $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('phone_number',@$_GET["dir"]);

                if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                {

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('phone_number',@$_GET["dir"]);

                }
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }
                
                $users = $usersobj->get();

                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        if(@$_GET["column"]==0){
            
            
             
                $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"]);

                if( $userRole == 'Subcontractor' )
                {

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('company_name',@$_GET["dir"]);

                }

                if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                {

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('first_name',@$_GET["dir"]);

                }
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }

                $users = $usersobj->get();
                
                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        
        if(@$_GET["column"]==6){
            
            
             
            $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('created_at',@$_GET["dir"]);

            if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
            {

                $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('created_at',@$_GET["dir"]);

            }

            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }

            if( @$_GET["company"] != 'null' )
            {
                $usersobj->where( 'parent', @$_GET["company"] );
            }

            if( @$_GET["title"] != 'null' )
            {

                $usersobj->where( 'worker_type', @$_GET["title"] );
            }

            $users = $usersobj->get();
            
            $index = null;
            $i=0;
            foreach($users as $user){
                if($user->id == $id){
                    $index = $i;
                    break;
                }
                $i++;
            }
            if(isset($users[$i+1])){
                $next = $users[$i+1];
            }
            if(isset($users[$i-1])){
                $previous = $users[$i-1];
            }
            
            
        
    }

    
    if(@$_GET["column"]==3){
            

        $usersobj = User::role($userRole)->where('status', '2')->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"])->where('id', 'parent');


       if( @$_GET["status"] != 'null' )
        {
            $usersobj->where( 'status', @$_GET["status"] );
        }

        if( @$_GET["company"] != 'null' )
        {
            $usersobj->where( 'parent', @$_GET["company"] );
        }

        if( @$_GET["title"] != 'null' )
        {

            $usersobj->where( 'worker_type', @$_GET["title"] );
        }

        $users = $usersobj->get();
        
        $index = null;
        $i=0;
        foreach($users as $user){
            if($user->id == $id){
                $index = $i;
                break;
            }
            $i++;
        }
        if(isset($users[$i+1])){
            $next = $users[$i+1];
        }
        if(isset($users[$i-1])){
            $previous = $users[$i-1];
        }
        
        
    
}
            
        endif;

        return view( 'admin.users.edit', compact( 'user', 'userRole', 'subContractors', 'workerTypes', 'fieldPrefix', 'previous','next' ) );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update1(Request $request, $id)
    {

        $parent = 0;

       // $user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::where('id', $id)->firstOrFail();

        $validator = Validator::make($request->all(), [

            'first-name' => 'required',

        ]);

        if($request->filled('phone-number') || !empty( $request->input('email') ))
        {

          $phone_number = preg_replace('/[^A-Za-z0-9]/', '', $request->input('phone-number'));

          $data['phone-number'] = $phone_number;

          if( !empty( $request->input('email') ) )
          {

              $data['email'] = $request->input('email');

          }

          $validator = Validator::make($data, [

            'phone-number' => 'unique:users,phone_number,'.$id,

            'email' => 'unique:users,email,'.$id,

          ]);

        }

        if($validator->fails()) {
        
            if($validator->errors()->first('first-name')) {
        
                Session::flash('error', 'Please enter first name of user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('phone-number')) {
        
                Session::flash('error', 'Phone number already linked with another user. Please try again with different information.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('email')) {
        
                Session::flash('error', 'Email already linked with another user. Please try again with different information.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('username')) {
        
                Session::flash('error', 'Please enter username of user.');
        
                return redirect()->back();
        
            }
        
        }

        $first_name = $request->input('first-name');

        $email = $request->input('email');

        $last_name = $request->input('last-name');
        
        $worker_type = $request->input('worker_type');

        if($request->filled('phone-number')) {

            $country_code = null;

            $phone_number = preg_replace('/[^A-Za-z0-9]/', '', $request->input('phone-number'));


        } else {

            $country_code = null;

            $phone_number = null;

        }

        $gender = $request->input('gender');

        /*if($username && User::where('id', '!=', $user->id)->where('username', $username)->withTrashed()->first()) {

            Session::flash('error', 'Username already linked with another user. Please try again with different information.');

            return redirect()->back();

        }*/

        /*-------------------------------------------- Seperator --------------------------------------------*/

        if( isset( $request->general_contractor ) && !empty( $request->general_contractor ) )
        {

            if( isset( $request->user_role ) && ( $request->user_role == 'worker' || $request->user_role == 'foreman' ) )
            {
                
                $userRole = $request->input('user_role');

                $parent = $request->general_contractor;

            }

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $status = $request->input('status');

        //$user->username = $username;
        
        $user->first_name = $first_name;

        $user->email = $email;

        $user->company_name = @$request->input('company-name');
        
        $user->last_name = $last_name;

        $user->payroll_first_name = $request->payroll_first_name;

        $user->payroll_last_name = $request->payroll_last_name;

        $user->payroll_email = $request->payroll_email;

        $user->payroll_phone = $request->payroll_phone;

        $user->doc_upload =  $request->input('doc_upload') ? 1 : 0;

        $user->phone_number = $phone_number;

        $user->ssn_id = ( $request->input('ssn_id') ) ? strtoupper( $request->input('ssn_id_pre').$request->input('ssn_id') ) : null;

        $user->worker_type = $worker_type;

        $user->gender = $gender;

        $user->parent = $parent;

        $user->phone_number = $phone_number;

        $user->country_code = $country_code;
        
        $user->status = $status;

        if($request->hasFile('image')) {

            $format_validator = Validator::make($request->all(), [

                'image' => 'mimes:jpeg,png,jpg,gif,svg'
    
            ]);
    
            if($format_validator->fails()) {
    
                Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');
    
                return redirect()->back();
    
            }

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('image');

			$image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);
            
            $user->image = $image_name;

        }

        if($request->input('cropped-image')){

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $request->input('cropped-image-type');

            $imageInfo = $request->input('cropped-image'); 

            $image = fopen($imageInfo, 'r');
            
            file_put_contents($destination.'/'.$image_name, $image);

            fclose($image); 

            $user->image = $image_name;
        }

        $this->saveWorkerDocuments( $request, $user->id );

        /* Document upload */

        $subContId = $user->id;

        if( $request->hasFile('document') ) {

          foreach ( $request->document as $document) {
            
            $destination = public_path('uploads/documents/'. $subContId);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $documentName = 'document-' . time() . '-' . Str::random(15) . '.' . $document->getClientOriginalExtension();

            $document->move($destination, $documentName);
            
            $saveData['user_id'] = $subContId;
            $saveData['document'] = $documentName;

            Document::create( $saveData );

          }

        }

        /* Document upload */

        /*-------------------------------------------- Seperator --------------------------------------------*/

    
        if( isset( $request->user_role ) )
        {
            $user->roles()->detach();
            
            $userRole = $request->input('user_role');

            $user->assignRole( $userRole );

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $user->save();

        Session::flash('success', 'User information has been updated successfully.');

        Session::flash('status-user', $user->id);

        $redirectQuery = 'userType='.$userRole;

        return redirect()->back();
        //return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy1($id)
    {

        $user = User::where('id', $id)->firstOrFail();

        if( count( $user->logs ) == 0 )
        {

          DB::table('users')->where('id', $id)->delete();

          Session::flash('success', 'User has been deleted successfully.');

          return redirect()->back();

        }

        $user->delete();

        Session::flash('success', 'User has been deleted successfully.');

        return redirect()->back();

    }

    /**
     * Approve worker
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();

        $user = User::find( $id );

        if( $user->status != 1 )
        {

          Session::flash('error', 'This action has already been performed on the request.');
          return redirect( route('admin.workers.requests') );

        }

        $user->status = 2;

        $user->save();

        $userData = User::where('id', $id)->first();

        $emailUserData = [];

        $emailUserData['first_name'] = $userData->first_name;
        $emailUserData['name'] = 'Name: '.$userData->first_name.' '.$userData->last_name.'<br>';
        $emailUserData['email'] = $userData->email;
        $emailUserData['phone_number'] = '';

        $workerCompanyfieldForEmail = 'Company: '.getCompanyNameFromId( $userData->parent );


        if( !empty( $userData->phone_number ) )
        {

          $emailUserData['phone_number'] = 'Phone number: '.$userData->phone_number.'<br>';

        }

        //
        if( !empty( $userData->email ) )
        Mail::to( $userData->email )->send(new ApproveWorker((object)$emailUserData, 'Mail', '<i>Same as you entered.</i>', $workerCompanyfieldForEmail));

        Session::flash('success', 'Worker request has been approved.');

        //return redirect()->route('admin.users');

        return redirect( route('admin.workers.requests') );

    }

    /**
     * Reject worker
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::find($id);

        if( !$user || $user->status != 1 )
        {

          Session::flash('error', 'This action has already been performed on the request.');
          return redirect( route('admin.workers.requests') );

        }

        $user->delete();

        Session::flash('success', 'Worker request has been rejected.');

        //return redirect()->route('admin.users');

        return redirect( route('admin.workers.requests') );

    }

    /* */

    public function deleteDocument( $id )
    {

      $obj = Document::find( $id );

      $obj->delete();

      Session::flash('success', 'Document has been deleted.');

      return redirect()->back();

    }

    /* */

    public function deleteWorkerDocument( $id )
    {

        $obj = WorkerDocuments::find( $id );

      $obj->delete();

      Session::flash('success', 'Document has been deleted.');

      return redirect()->back();

    }

    public function remove($id)
    {

        //$user = User::role('user')->where('id', $id)->firstorFail();
        $user = User::where('id', $id)->firstorFail();

        $user->image = null;

        $user->save();

        Session::flash('success', 'User image has been removed successfully.');

        return redirect()->back();

    }

}
