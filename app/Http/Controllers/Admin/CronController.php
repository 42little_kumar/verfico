<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ForgotPasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\UserDevice;
use App\WorkLog;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use App\Http\Controllers\Api\NotificationController as NotificationController;
use DB;
use Carbon\Carbon;
use Mail;

class CronController extends ApiBaseController
{
    public $successStatus = 200;

    function checkDuplicateLogs(Request $request )
    {
        list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

          $punchIn = Carbon::createFromFormat('Y-m-d H:i:s', date("Y-m-d H:i:s",time()-2*24*60*60),'UTC')->setTimezone($timezone);
         // var_dump($punchIn);

        $logs = WorkLog::where("punch_at",">=",$punchIn->format("Y-m-d"))->get();

        $duplicate_logs = [];

        foreach ($logs as $log) {
            
            $punch_in_date = date('Y-m-d',strtotime($log->punch_at));

            $punch_in_timestamp = strtotime($log->punch_at);

            $punch_out_timestamp =  strtotime($punch_in_date." ".$log->punch_in);

            if($punch_out_timestamp < $punch_in_timestamp){
                $punch_out_timestamp = $punch_out_timestamp+24*60*60;
            }
            //if($punch_out_timestamp - $punch_out_timestamp > 8*24*60*60){

                $punch_date = Carbon::createFromFormat('Y-m-d H:i:s', $log->punch_at,'UTC')->setTimezone($timezone);

                if(!isset($duplicate_logs[$punch_date->format("m/d/Y")][$log->worker_id])){
                    $duplicate_logs[$punch_date->format("m/d/Y")][$log->worker_id] = 1;    
                } else {
                    $duplicate_logs[$punch_date->format("m/d/Y")][$log->worker_id] = $duplicate_logs[$punch_date->format("m/d/Y")][$log->worker_id]+1;
                 
                }    
           // }

        }
        
        $workers = [];

        foreach ($duplicate_logs as $date=>$date_logs) {

            foreach ($date_logs as $worker => $log) {
                
                if($log>1){
                    $user = User::where("id",$worker)->first();

                    if($user){
                        $workers[$date][] = $user->name;
                    }
                }
            }

        }
        //print_r($workers); die;
        $body = "";

        if(sizeof($workers)>0){

            $body = "Hi Admin<br><br>";

            $body .= "The duplicate logs has been found for following:<br/><br/>";

            foreach ($workers as $date => $worker_arr) {
                $body .= "<b>".$date."</b>: ".implode(", ", $worker_arr)."<br/>";
            }
            $body .= "<br/>Regards<br>Rockspring Contracting";
            Mail::send([], [], function($message) use ($body) {
                //,'raman@42works.net',"sonika@42works.net","qa_aakriti@42works.net"
                $message->to(['kate@rockspringcontracting.com'])->subject
                    ('[Rockspring]: Duplicate Logs')->setBody($body, 'text/html');
            });

        }
        /*Mail::send([], [], function($message) use ($body) {
             $message->to('raman@42works.net')->subject
                ('Laravel Basic Testing Mail')->setBody($body);
          });*/
        
        //return true;
    }


}
