<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Payroll;
use App\PayrollFiles;
use App\Http\Controllers\Api\NotificationController as NotificationController;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use DB;
use Carbon\Carbon;
use Mail;
use App\Mail\ManpowerCronEmail;
use App\Mail\ManpowerPreviewEmailsSend;
use App\Mail\ManpowerEveningEmail;
use App\Manpower;
use App\Project;

class ManpowerCron extends ApiBaseController
{
    function sendEmailEvening()
    {

        $projects = Project::where('status', 'active')->get();

        $response = [];

        foreach( $projects as $pr )
        { 
            
            $finalHtml = "";
            $extraworker = "<h5 style='margin:0px; padding:3px 0;'>Extra workers logged</h5>";
            $notlogged = "<h5 style='margin:0px; padding:3px 0;'>Workers who have not logged</h5>";
            $hasExtra = false;
            $hasNotLogged = false;

            $finalHtml .= "<div style='background:#f2f2f2; font-family: Arial, Helvetica, sans-serif; width: 500px; margin: 0 auto; padding: 10px;'>";
            $finalHtml .= "<h2 style='margin: 0px 0 10px; padding: 0px 0 8px; border-bottom: 1px solid #ddd; font-size: 17px; text-transform: uppercase;'>Project: ".$pr->job_name."</h2>"; 



        $url = \URL::to('/').'/api/v1/getschedule?date='.date('m-d-Y').'&project_id='.$pr->id.'&classification=show';


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        $allLogs = json_decode($output);

        if( $allLogs->data ):

        foreach( $allLogs->data as $g )
        {

            if( !isset( $g->occupied ) )
            {
                $hasExtra = true;
                $extraworker .= "<p style='margin:0px; padding:4px 0; font-size:13px;'>";
                $extraworker .= "<span>Worker Name: $g->worker_name ( $g->sub_name )</span>, ";
                $extraworker .= "<span>". empty( $g->punch_in ) ? 'Punch In: - ' : 'Punch In: '.$g->punch_in ."</span>, ";
                $extraworker .= "<span>". empty( $g->punch_out ) ? 'Punch Out: - ' : 'Punch Out: '.$g->punch_out ."</span> ";
                $extraworker .= "</p>";

            }

            if( isset( $g->occupied ) && $g->occupied == false )
            {

                $hasNotLogged = true;
                $notlogged .= "<p style='margin:0px; padding:4px 0; font-size:13px;'>";
                $notlogged .= "<span>Worker Type: $g->worker_type ( $g->sub_name )</span>, ";
                $notlogged .= "<span>Punch In: - </span>, ";
                $notlogged .= "<span>Punch Out: - </span> ";
                $notlogged .= "</p>";

            }

        }

        endif;
        
            if( $hasExtra )
                $finalHtml .= $extraworker;

            if( $hasNotLogged )
                $finalHtml .= $notlogged;

            
            
            $finalHtml .= "</div>";

            if( $hasExtra || $hasNotLogged )
                $response[$pr->superint][] = $finalHtml;

        }

        //dd( $response );

        foreach ($response as $sub => $get)
        {
            
            $data = '';

            foreach ($get as $g) {
                $data .= $g;
            }


           // Send Email

            $dataSub = User::find( $sub );
            $subEmail = $dataSub->email;
            $subName = $dataSub->first_name.' '.$dataSub->last_name;
            //$subEmail = 'thistesting@yopmail.com';

            $notificationText = "Below is the status of your today's manpower schedule ( Superintendent Name: $subName ):";

            $emailPreview = false;

            if( $emailPreview ){

            	echo $data;

            } else{

            	Mail::to( $subEmail )->send( new ManpowerEveningEmail( $data, $notificationText ) );

            	// Mail to superadmin
            	Mail::to( 'katesuperadmin@yopmail.com' )->send( new ManpowerEveningEmail( $data, $notificationText ) );
            	Mail::to( 'ranveer@42works.net' )->send( new ManpowerEveningEmail( $data, $notificationText ) );

            }
            

            

        }
    }

    function sendEmailEveningShowPreview()
    {

        $projects = Project::where('status', 'active')->get();

        $response = [];

        foreach( $projects as $pr )
        { 
            
            $finalHtml = "";
            $extraworker = "<h5 style='margin:0px; padding:3px 0;'>Extra workers logged</h5>";
            $notlogged = "<h5 style='margin:0px; padding:3px 0;'>Workers who have not logged</h5>";
            $hasExtra = false;
            $hasNotLogged = false;

            $finalHtml .= "<div style='background:#f2f2f2; font-family: Arial, Helvetica, sans-serif; width: 500px; margin: 0 auto; padding: 10px;'>";
            $finalHtml .= "<h2 style='margin: 0px 0 10px; padding: 0px 0 8px; border-bottom: 1px solid #ddd; font-size: 17px; text-transform: uppercase;'>Project: ".$pr->job_name."</h2>"; 


        $url = \URL::to('/').'/api/v1/getschedule?date='.date('m-d-Y').'&project_id='.$pr->id;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        $allLogs = json_decode($output);

        dd($allLogs);

        if( $allLogs->data ):

        foreach( $allLogs->data as $g )
        {

            if( !isset( $g->occupied ) )
            {
                $hasExtra = true;
                $extraworker .= "<p style='margin:0px; padding:4px 0; font-size:13px;'>";
                $extraworker .= "<span>Worker Name: $g->worker_name ( $g->sub_name )</span>, ";
                $extraworker .= "<span>". empty( $g->punch_in ) ? 'Punch In: - ' : 'Punch In: '.$g->punch_in ."</span>, ";
                $extraworker .= "<span>". empty( $g->punch_out ) ? 'Punch Out: -' : 'Punch Out: '.$g->punch_out ."</span> ";
                $extraworker .= "</p>";

            }

            if( isset( $g->occupied ) && $g->occupied == false )
            {

                $hasNotLogged = true;
                $notlogged .= "<p style='margin:0px; padding:4px 0; font-size:13px;'>";
                $notlogged .= "<span>Worker Type: $g->worker_type ( $g->sub_name )</span>, ";
                $notlogged .= "<span>Punch In - </span>, ";
                $notlogged .= "<span>Punch Out - </span> ";
                $notlogged .= "</p>";

            }

        }

        endif;
        
            if( $hasExtra )
                $finalHtml .= $extraworker;

            if( $hasNotLogged )
                $finalHtml .= $notlogged;

            
            
            $finalHtml .= "</div>";

            if( $hasExtra || $hasNotLogged )
                $response[$pr->superint][] = $finalHtml;

        }

        //dd( $response );

        foreach ($response as $sub => $get)
        {
            
            $data = '';

            foreach ($get as $g) {
                $data .= $g;
            }


           // Send Email

            $dataSub = User::find( $sub );
            $subEmail = $dataSub->email;
            $subName = $dataSub->first_name.' '.$dataSub->last_name;
            //$subEmail = 'thistesting@yopmail.com';

            $notificationText = "Below is the status of your today's manpower schedule ( Superintendent Name: $subName ):";

            $emailPreview = true;

            if( $emailPreview ){

            	echo $data;

            } else{

            	//Mail::to( $subEmail )->send( new ManpowerEveningEmail( $data, $notificationText ) );

            	// Mail to superadmin
            	//Mail::to( 'katesuperadmin@yopmail.com' )->send( new ManpowerEveningEmail( $data, $notificationText ) );
            	//Mail::to( 'ranveer@42works.net' )->send( new ManpowerEveningEmail( $data, $notificationText ) );

            }
            

            

        }
    }

    function generatePreview( Request $request )
    {



        $subId = $request->subid;

        if( $subId == 'admin' ){

        	$this->previewAdmin( $request->date );

        	die;

        }

        $input  = $request->date;
          
        $format = 'm-d-Y';

        $emailData = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);
        $datec = $c->toDateString();

        $allProjectsIds = \DB::table('project')->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');

        if( isset( $request->tosingle ) )
        {
            $allProjectsIds = \DB::table('project')->where('id', $request->pr)->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');

        }

        

        $scheduleData = Manpower::where('job_date', $c->toDateString())->whereIn('project_id', $allProjectsIds)->get();

        if( $scheduleData ):



            foreach( $scheduleData as $schedule )
            {

                //

                $dp = [];
                $dp2 = [];

                // Shift 1

                $decodedData = json_decode( $schedule->emp_data );
                $decodedData2 = json_decode( $schedule->emp_data_2 );

                $rowArray = [];

                //print_r( $schedule->emp_data );

                if( !empty( $schedule->emp_data ) ):

                foreach ($decodedData->sub as $k => $g) {

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData->a_workertype_name;
                        $awtn = (array)@$decodedData->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp[$sid] ) ) {

                                $awt = $awt[$sid][$dp[$sid]];
                                $awtn = $awtn[$sid][$dp[$sid]];
                                $dp[$sid] = $dp[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }             
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '1';
                        $tempData['project'] = $schedule->project_id;
                        $tempData['time'] = $schedule->job_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData->jobtype[$k];
                        $tempData['note'] = @$decodedData->note[$k];

                    }

                    

                    array_push($rowArray, $tempData);

                }

                endif;

                // Shift 2

                if( !empty( $schedule->emp_data_2 ) ):

                foreach ($decodedData2->sub as $k => $g) {

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData2->a_workertype_name;
                        $awtn = (array)@$decodedData2->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp2[$sid] ) ) {

                                $awt = $awt[$sid][$dp2[$sid]];
                                $awtn = $awtn[$sid][$dp2[$sid]];
                                $dp2[$sid] = $dp2[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp2[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }             
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '2';
                        $tempData['project'] = $schedule->project_id;
                        $tempData['time'] = $schedule->job_2_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData2->jobtype[$k];
                        $tempData['note'] = @$decodedData2->note[$k];

                    }

                    array_push($rowArray, $tempData);
                    

                }

                endif;

                $emailData[ $schedule->project_id ] = array_filter( $rowArray );


            }

        endif;

        //$response = '<p>Hello,</p>Here is the manpower schedule details with which you are linked to:<br><br>Date: '. date("j F, Y", strtotime($schedule->job_date)) .'<br><br>';
        $response = '<p>Hello,</p>Here is your company’s manpower schedule details for '.date('l', strtotime($schedule->job_date)).', '.date("j F, Y", strtotime($schedule->job_date)).':<br>';

        //print_r( array_filter( $emailData ) );

        foreach( array_filter( $emailData ) as $k => $v )
        {

            $getProject = \DB::table('project')->where('id', $k)->first();

            $getaddress = json_decode( $getProject->job_address );
            $street2 = null;
            if(!empty($getaddress->street_2)){
            $street2 = $getaddress->street_2.',  ';
            }
            // dd($street2);

            $jobaddressmade = $getaddress->street_1.', '. $street2 . $getaddress->city.', '.$getaddress->state.', '.$getaddress->zipcode;
            // dd($jobaddressmade);
            $response .= '<div>'; 
            $response .= '<h5 ><b>'. ucfirst( $getProject->job_name ) .' ('.ucfirst( $getProject->job_number ).')<br><span style="font-size: 10px">'.$jobaddressmade.'</span></b></h5>'; 

            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            foreach ($v as $g) {


                /* -------- Check Emtpy -------- */

                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];
                $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;

                /* -------- Check Emtpy -------- */


                if( $g["shift"] == 1 ){
                    $responseInternal1 .= '<p style="margin-bottom:5px;">'; 
                
                    $responseInternal1 .= '<span><b><i></i></b> ('. $workerNumber .')</span> ';
                    $responseInternal1 .= '<span><b><i></i></b> '. $workerType .' for</span> '; 
                     
                    $responseInternal1 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                    $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal1 .= '</p>';
                }
                else{

                    $responseInternal2 .= '<p style="margin-bottom:5px;">'; 
                
                    $responseInternal2 .= '<span><b><i></i></b> ('. $workerNumber .')</span> ';
                    $responseInternal2 .= '<span><b><i></i></b> '. $workerType .' for</span> '; 
                     
                    $responseInternal2 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                    $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal2 .= '</p>';

                }
                 

            }

            if( reset($v)["closed"]  )
            {
                $response .= '<i>('.reset($v)["special_notes"].')</i><br></br>';
            }

            $response .= '<b>Job Starts at:  '. str_replace(" : ", ":", reset($v)["time"] ) .' ('.reset($v)["shifttype"].' Hour)</b><br>'.$responseInternal1.' </br>';

            if( !empty( $responseInternal2 ) ){
                $response .= '<b>2nd shift starts at: ( '. str_replace(" : ", ":", reset($v)["time2"] ) .' ) ('.reset($v)["shifttype2"].' Hour)</b><br>'.$responseInternal2.'';
            }
            

            $response .= '</div>'; 
        }
        $response .= 'Regards,<br>'.ucfirst( Auth::user()->name );

        echo $response;

    }

    function previewAdmin( $input, $type = null )
    {


    	$currentSubs = getScheduleCurrentSubs( $input );

        $response2 = '';

    	foreach( $currentSubs as $subId ):
          
        $format = 'm-d-Y';

        $emailData = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);
        $datec = $c->toDateString();

        $allProjectsIds = \DB::table('project')->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');

        if( isset( $_REQUEST['tosingle'] ) )
        {
            $allProjectsIds = \DB::table('project')->where('id', $_REQUEST['pr'])->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');
            
        }

        $scheduleData = Manpower::where('job_date', $c->toDateString())->whereIn('project_id', $allProjectsIds)->get();

        if( $scheduleData ):



            foreach( $scheduleData as $schedule )
            {

                //

                $dp = [];
                $dp2 = [];

                // Shift 1

                $decodedData = json_decode( $schedule->emp_data );
                $decodedData2 = json_decode( $schedule->emp_data_2 );

                $rowArray = [];

                //print_r( $schedule->emp_data );

                if( !empty( $schedule->emp_data ) ):

                foreach ($decodedData->sub as $k => $g) {

                    //if( $g != 624 ):

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData->a_workertype_name;
                        $awtn = (array)@$decodedData->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */

                        if( $g != 624 ):
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp[$sid] ) ) {

                                $awt = $awt[$sid][$dp[$sid]];
                                $awtn = $awtn[$sid][$dp[$sid]];
                                $dp[$sid] = $dp[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }      
                        endif;       
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '1';
                        $tempData['sub'] = $g;
                        $tempData['project'] = $schedule->project_id;
                        $tempData['worker'] =  @$decodedData->worker[$k];
                        $tempData['time'] = $schedule->job_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData->jobtype[$k];
                        $tempData['note'] = @$decodedData->note[$k];

                    }

                    

                    array_push($rowArray, $tempData);

                //endif;

                }

                endif;

                // Shift 2

                if( !empty( $schedule->emp_data_2 ) ):

                foreach ($decodedData2->sub as $k => $g) {

                    //if( $g != 624 ):

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData2->a_workertype_name;
                        $awtn = (array)@$decodedData2->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */
                        if( $g != 624 ):
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp2[$sid] ) ) {

                                $awt = $awt[$sid][$dp2[$sid]];
                                $awtn = $awtn[$sid][$dp2[$sid]];
                                $dp2[$sid] = $dp2[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp2[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }
                        endif;         
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '2';
                        $tempData['sub'] = $g;
                        $tempData['project'] = $schedule->project_id;
                        $tempData['worker'] =  @$decodedData2->worker[$k];
                        $tempData['time'] = $schedule->job_2_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData2->jobtype[$k];
                        $tempData['note'] = @$decodedData2->note[$k];

                    }

                    array_push($rowArray, $tempData);

                //endif;
                    

                }

                endif;

                $emailData[ $schedule->project_id ] = array_filter( $rowArray );


            }

        endif;

        //if( $subId != 624 ):

        $response = '<h2>Subcontractor Name:'. User::find( $subId )->company_name.'</h2>';

        $responseP[$subId] = array_filter( $emailData );

        //print_r( array_filter( $emailData ) );

        foreach( array_filter( $emailData ) as $k => $v )
        {

            $getProject = \DB::table('project')->where('id', $k)->first();

            $response .= '<div>'; 
            $response .= '<h5><b>'. ucfirst( $getProject->job_name ) .'</b></h5>'; 

            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            foreach ($v as $g) {

                /* -------- Check Emtpy -------- */

                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];

                if( $subId != 624 ):

                    if( empty( $g["worker_type"] ) || !isset( $g["worker_type"] ) )
                    {
                        $workerType = 'N/A';
                    }
                    else
                    {
                        $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
                    }
                

                else:

                $workerName = $g['worker'];


                endif;

                /* -------- Check Emtpy -------- */

                if( $g["shift"] == 1 ){
                    $responseInternal1 .= '<p>';

                    if( $subId != 624 ):

                        $responseInternal1 .= '<span><b><i>Worker Type:</i></b> '. $workerType .'</span>, ';
                        $responseInternal1 .= '<span><b><i>Worker Number:</i></b> '. $workerNumber .'</span>, ';  

                    endif;
                    
                    $responseInternal1 .= '<span><b><i>Job Type:</i></b> '. $workType .'</span>, '; 
                    $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal1 .= '</p>';
                }
                else{

                    $responseInternal2 .= '<p>'; 

                    if( $subId != 624 ):

                        $responseInternal2 .= '<span><b><i>Worker Type:</i></b> '. $workerType .'</span>, '; 
                        $responseInternal2 .= '<span><b><i>Worker Number:</i></b> '. $workerNumber .'</span>, '; 

                    endif;

                    $responseInternal2 .= '<span><b><i>Job Type:</i></b> '. $workType .'</span>, ';
                    $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal2 .= '</p>';

                }
                 

            }


            $response .= '<b>First Shift: ( '. str_replace(" : ", ":", reset($v)["time"] ) .' ) </b><br>'.$responseInternal1.' </br>';

            if( !empty( $responseInternal2 ) ){
                $response .= '<b>Second Shift: ( '. str_replace(" : ", ":", reset($v)["time2"] ) .' ) </b><br>'.$responseInternal2.' </br>';
            }
            

            $response .= '</div>'; 
                

        }


        $notificationText = '';

        $response2 .= $response;

        //endif;

    endforeach;




    $projectP = [];

foreach( $responseP as $v )
{
    
    foreach( $v as $projectId => $projectData )
    {

        foreach( $projectData as $g )
        {
            array_push( $projectP, $g );
        }

        

    }

}

foreach( $projectP as $k => $v )
{
    
    $newProjectP[ $v['project'] ][] = $projectP[ $k ];    

}

    /* ----------------------- Final Preview ------------------------- */

$fresponse = '';


        $format = 'm-d-Y';

        $emailData = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);
        $datec = $c->toDateString();

$fresponse = '<p style="font-size:15px; line-height:22px;">Hello,<br/> Here is the manpower schedule details for '.date('l', strtotime($datec)).', '.date("j F, Y", strtotime($datec)).':</p><br>';


foreach( $newProjectP as $p => $k )
{

    
    $getaddress = json_decode( \DB::table('project')->where('id', $p)->first()->job_address );
    $street2 = null;
    if(!empty($getaddress->street_2)){
        $street2 = $getaddress->street_2.',  ';
    }
    $jobaddressmade = $getaddress->street_1.', '.$street2.$getaddress->city.', '.$getaddress->state.', '.$getaddress->zipcode;

    $heading = '<h2 style="color: #000;text-transform: uppercase;font-size: 15px;font-weight: bold;  min-height:30px; background: #f3f3f3; padding: 8px; margin-bottom:10px ">Project Name: '. \DB::table('project')->where('id', $p)->first()->job_name.' ('.\DB::table('project')->where('id', $p)->first()->job_number.')<br><span style="font-size: 10px; margin:4px 0; display:block;">'.$jobaddressmade.' </span></h2>';



            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            $rockshift1 = '';
            $rockshift2 = '';
            $responseInternal1 .= '<b>Subcontractors</b>';

            foreach ($k as $g) {

                /* -------- Check Emtpy -------- */

                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];
                $workerName = $g['worker'];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];
                //$workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;

                if( $subId != 624 ):

                if( empty( $g["worker_type"] ) || !isset( $g["worker_type"] ) )
                    {
                        $workerType = 'N/A';
                    }
                    else
                    {
                        $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
                    }

                else:

                    //$workerName = $g['worker'];

                endif;

                /* -------- Check Emtpy -------- */

                if( $g["shift"] == 1 ){
                     
                
                     

                    if( $g["sub"] != 624 ):


                        $responseInternal1 .= '<p style="font-size:13px; margin-bottom:5px;">';
                        $responseInternal1 .= '<span><b><i></i></b> '. User::find( $g["sub"] )->company_name .'</span>: ';
                        $responseInternal1 .= '<span><b><i></i></b> ('. $workerNumber .') </span> ';
                        $responseInternal1 .= '<span><b><i></i></b> '. $workerType .'</span> for '; 
                        $responseInternal1 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                        $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 
                        $responseInternal1 .= '</p>';
                         
                    // dd($responseInternal1);
                    endif;

                    if( $g["sub"] == 624 ):


                        $workerName = @\App\User::where('id', $workerName)->first();
                        $classification = !empty($workerName->classification) ? ' - '.$workerName->classification : '';
                        $rockshift1 .= "<p style='font-size:13px; margin-bottom:5px;'>".ucwords($workerName['name']).$classification ." for " .$workType."</p>";


                        //$responseInternal1 .= '<span><b><i>Worker Name:</i></b> '. $workerName .'</span>, '; 

                    endif;
                    
                    
                }
                else{

                    
                
                     

                    if( $g["sub"] != 624 ):
                     
                        $responseInternal2 .= '<p style="font-size:13px; margin-bottom:5px;">';
                        $responseInternal2 .= '<span><b><i></i></b> '. User::find( $g["sub"] )->company_name .'</span>: ';
                        $responseInternal2 .= '<span><b><i></i></b> ('. $workerNumber .') </span>, ';
                        $responseInternal2 .= '<span><b><i></i></b> '. $workerType .'</span> for ';
                        $responseInternal2 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                        $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> ';
                        $responseInternal2 .= '</p>'; 
                         

                    endif;

                    if( $g["sub"] == 624 ):


                        $workerName = @\App\User::where('id', $workerName)->first();
                        $classification = !empty($workerName->classification) ? ' - '.$workerName->classification : '';
                        $rockshift2 .= "<p style='font-size:13px; margin-bottom:5px;'>".ucwords($workerName['name']).$classification ." for " .$workType."</p>"; 


                    endif;

                    

                }

            }

            $r1 = '';
            $r2 = '';

            if( !empty( $rockshift1 ) )
                $r1 = "<b>Rock Spring</b>".$rockshift1;

            if( !empty( $rockshift2 ) )
                $r2 = "<b>Rock Spring</b>".$rockshift2;

            $fresponse .= $heading;

            if( reset($k)["closed"]  )
            {
                $fresponse .= '<i>('.reset($k)["special_notes"].')</i><br><br>';
            // dd($fresponse);
            }

            $fresponse .= '<b style="font-size: 14px;display: inline-block; margin: 0 0 5px;">Job Starts at: '. str_replace(" : ", ":", reset($k)["time"] ) .'  ('.reset($k)["shifttype"].' Hour)</b><br>'.$r1.$responseInternal1.' </br>';
            
            
            if( !empty( $responseInternal2 ) || !empty( $rockshift2 ) ){
                $fresponse .= '<b>2nd shift starts at: ( '. str_replace(" : ", ":", reset($k)["time2"] ) .' ) ('.reset($k)["shifttype2"].' Hour)</b><br>'.$r2.$responseInternal2.' </br>';
            }
            $fresponse .='<br><br>' ;


}

$fresponse .= '<p style="font-size:15px;">Regards,<br>'.ucfirst( Auth::user()->name ).' </p>';

if( $type == 'email' )
{
    return $fresponse;
}
echo $fresponse;
    




    /* ----------------------- Final Preview ------------------------- */

    }

    function sendPreviewEmails()
    {



    	$input = $_REQUEST['d'];

       

        if( isset( $_REQUEST['tosingle'] ) )
        {

            $currentSubs = getScheduleCurrentSubsForCurrent($input, $_REQUEST['tosingle']);
            
        }
        else
        {

            
            $currentSubs = getScheduleCurrentSubs( $input );
        }
  
        $this->sendPreviewEmailsForman( $input );
     
        $this->sendPreviewAdminEmails( $input );
    	 // dd('$input');

    	foreach( $currentSubs as $subId ):
          
        $format = 'm-d-Y';

        $emailData = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);
        $datec = $c->toDateString();

        $allProjectsIds = \DB::table('project')->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');

        if( isset( $_REQUEST['tosingle'] ) )
        {
            $allProjectsIds = \DB::table('project')->where('id', $_REQUEST['pr'])->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');
            
        }

        $scheduleData = Manpower::where('job_date', $c->toDateString())->whereIn('project_id', $allProjectsIds)->get();

        if( $scheduleData ):



            foreach( $scheduleData as $schedule )
            {

                //

                $dp = [];
                $dp2 = [];

                // Shift 1

                $decodedData = json_decode( $schedule->emp_data );
                $decodedData2 = json_decode( $schedule->emp_data_2 );

                $rowArray = [];

                //print_r( $schedule->emp_data );

                if( !empty( $schedule->emp_data ) ):

                foreach ($decodedData->sub as $k => $g) {

                    if( $subId != 624 ):

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData->a_workertype_name;
                        $awtn = (array)@$decodedData->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp[$sid] ) ) {

                                $awt = $awt[$sid][$dp[$sid]];
                                $awtn = $awtn[$sid][$dp[$sid]];
                                $dp[$sid] = $dp[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }             
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '1';
                        $tempData['project'] = $schedule->project_id;
                        $tempData['time'] = $schedule->job_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData->jobtype[$k];
                        $tempData['note'] = @$decodedData->note[$k];

                    }

                    

                    array_push($rowArray, $tempData);

                endif;

                }

                endif;

                // Shift 2

                if( !empty( $schedule->emp_data_2 ) ):

                foreach ($decodedData2->sub as $k => $g) {

                    if( $subId != 624 ):

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData2->a_workertype_name;
                        $awtn = (array)@$decodedData2->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp2[$sid] ) ) {

                                $awt = $awt[$sid][$dp2[$sid]];
                                $awtn = $awtn[$sid][$dp2[$sid]];
                                $dp2[$sid] = $dp2[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp2[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }             
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '2';
                        $tempData['project'] = $schedule->project_id;
                        $tempData['time'] = $schedule->job_2_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData2->jobtype[$k];
                        $tempData['note'] = @$decodedData2->note[$k];

                    }

                    array_push($rowArray, $tempData);
                    
                    endif;
                }

                endif;

                $emailData[ $schedule->project_id ] = array_filter( $rowArray );


            }

        endif;

        $response = '<p>Hello,</p>Here is your company’s manpower schedule details for '.date('l', strtotime($schedule->job_date)).', '.date("j F, Y", strtotime($schedule->job_date)).':<br><br>';

        //print_r( array_filter( $emailData ) );

        foreach( array_filter( $emailData ) as $k => $v )
        {

            $getProject = \DB::table('project')->where('id', $k)->first();

            $getaddress = json_decode( $getProject->job_address );
            $street2 = null;
            if(!empty($getaddress->street_2)){
            $street2 = $getaddress->street_2.',  ';
            }
            $jobaddressmade = $getaddress->street_1.', '.$street2.$getaddress->city.', '.$getaddress->state.', '.$getaddress->zipcode;

            $response .= '<div>'; 
            $response .= '<h5><b>'. ucfirst( $getProject->job_name ) .' ('.ucfirst( $getProject->job_number ).')<br><span style="font-size: 10px">'.$jobaddressmade.'</span></b></h5>'; 

            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            foreach ($v as $g) {

                /* -------- Check Emtpy -------- */

                $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];

                /* -------- Check Emtpy -------- */

                if( $g["shift"] == 1 ){
                    $responseInternal1 .= '<p>'; 
                
                    $responseInternal1 .= '<span><b><i></i></b> ('. $workerNumber .')</span> ';
                    $responseInternal1 .= '<span><b><i></i></b> '. $workerType .' for</span> '; 
                     
                    $responseInternal1 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                    $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal1 .= '</p>';
                }
                else{

                    $responseInternal2 .= '<p>'; 
                
                    $responseInternal2 .= '<span><b><i></i></b> ('. $workerNumber .')</span> ';
                    $responseInternal2 .= '<span><b><i></i></b> '. $workerType .' for</span> '; 
                     
                    $responseInternal2 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                    $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal2 .= '</p>';

                }
                 

            }

             if( reset($v)["closed"]  )
            {
                $response .= '<i>('.reset($v)["special_notes"].')</i><br></br>';
            }


            $response .= '<b>Job Starts at:  '. str_replace(" : ", ":", reset($v)["time"] ) .' ('.reset($v)["shifttype"].' Hour)</b><br>'.$responseInternal1.' </br>';

            if( !empty( $responseInternal2 ) ){
                $response .= '<b>2nd shift starts at: ( '. str_replace(" : ", ":", reset($v)["time2"] ) .' ) ('.reset($v)["shifttype2"].' Hour)</b><br>'.$responseInternal2.' </br>';
            }
            

            $response .= '</div><br><br>'; 
                

        }

        $response .= '<br>Regards,<br>'.ucfirst( Auth::user()->name );

        $notificationText = '';

        Mail::to( User::find( $subId )->email )->send( new ManpowerPreviewEmailsSend( $response, $notificationText ) );

    endforeach;

    $format = 'm-d-Y';

        
 /* -------------------- */

 $prids = \DB::table('project')->where('superint', Auth::user()->id)->pluck('id');

 
$c = \Carbon\Carbon::createFromFormat($format, $input);
$datec = $c->toDateString();

    
    if( isset( $_REQUEST['tosingle'] ) )
    {
        $allMan = Manpower::where('id', $_REQUEST['tosingle'])->update(["is_sechedule_email_sent"=>1]);
    }
    else
    {
        $allMan = Manpower::where('job_date', $datec)->whereIn('project_id', $prids)->update(["is_sechedule_email_sent"=>1]);
    }
    

    

    /* -------------------- */

    \Session::flash('success', 'Schedule emails have been sent.');

        return redirect()->back();

    }

    /* Foreman email send */

    function sendPreviewEmailsForman( $input )
    {

      $currentSubs = getScheduleCurrentSubs( $input );

        $response2 = '';

        foreach( $currentSubs as $subId ):
          
        $format = 'm-d-Y';

        $emailData = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);
        $datec = $c->toDateString();

        $allProjectsIds = \DB::table('project')->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');

        if( isset( $_REQUEST['tosingle'] ) )
        {
            $allProjectsIds = \DB::table('project')->where('id', $_REQUEST['pr'])->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');
            
        }

        $scheduleData = Manpower::where('job_date', $c->toDateString())->whereIn('project_id', $allProjectsIds)->get();

        if( $scheduleData ):



            foreach( $scheduleData as $schedule )
            {

                //

                $dp = [];
                $dp2 = [];

                // Shift 1

                $decodedData = json_decode( $schedule->emp_data );
                $decodedData2 = json_decode( $schedule->emp_data_2 );

                $rowArray = [];

                //print_r( $schedule->emp_data );

                if( !empty( $schedule->emp_data ) ):

                foreach ($decodedData->sub as $k => $g) {

                    //if( $g != 624 ):

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData->a_workertype_name;
                        $awtn = (array)@$decodedData->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */

                        if( $g != 624 ):
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp[$sid] ) ) {

                                $awt = $awt[$sid][$dp[$sid]];
                                $awtn = $awtn[$sid][$dp[$sid]];
                                $dp[$sid] = $dp[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }      
                        endif;       
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '1';
                        $tempData['sub'] = $g;
                        $tempData['project'] = $schedule->project_id;
                        $tempData['worker'] =  @$decodedData->worker[$k];
                        $tempData['time'] = $schedule->job_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData->jobtype[$k];
                        $tempData['note'] = @$decodedData->note[$k];

                    }

                    

                    array_push($rowArray, $tempData);

                //endif;

                }

                endif;

                // Shift 2

                if( !empty( $schedule->emp_data_2 ) ):

                foreach ($decodedData2->sub as $k => $g) {

                    //if( $g != 624 ):

                    $tempData = [];
                        
                    if( $g == $subId )
                    {

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData2->a_workertype_name;
                        $awtn = (array)@$decodedData2->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */
                        if( $g != 624 ):
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp2[$sid] ) ) {

                                $awt = $awt[$sid][$dp2[$sid]];
                                $awtn = $awtn[$sid][$dp2[$sid]];
                                $dp2[$sid] = $dp2[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp2[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }
                        endif;         
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '2';
                        $tempData['sub'] = $g;
                        $tempData['project'] = $schedule->project_id;
                        $tempData['worker'] =  @$decodedData2->worker[$k];
                        $tempData['time'] = $schedule->job_2_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['closed'] = $schedule->closed;
                        $tempData['special_notes'] = $schedule->special_notes;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData2->jobtype[$k];
                        $tempData['note'] = @$decodedData2->note[$k];

                    }

                    array_push($rowArray, $tempData);

                //endif;
                    

                }

                endif;

                $emailData[ $schedule->project_id ] = array_filter( $rowArray );


            }

        endif;

        //if( $subId != 624 ):

        $response = '<h2>Subcontractor Name:'. User::find( $subId )->company_name.'</h2>';

        $responseP[$subId] = array_filter( $emailData );

        //print_r( array_filter( $emailData ) );

        foreach( array_filter( $emailData ) as $k => $v )
        {

            $getProject = \DB::table('project')->where('id', $k)->first();

            $response .= '<div>'; 
            $response .= '<h5><b>'. ucfirst( $getProject->job_name ) .'</b></h5>'; 

            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            foreach ($v as $g) {

                /* -------- Check Emtpy -------- */

                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];

                if( $subId != 624 ):

                    if( empty( $g["worker_type"] ) || !isset( $g["worker_type"] ) )
                    {
                        $workerType = 'N/A';
                    }
                    else
                    {
                        $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
                    }
                

                else:

                $workerName = $g['worker'];


                endif;

                /* -------- Check Emtpy -------- */

                if( $g["shift"] == 1 ){
                    $responseInternal1 .= '<p>';

                    if( $subId != 624 ):

                        $responseInternal1 .= '<span><b><i>Worker Type:</i></b> '. $workerType .'</span>, ';
                        $responseInternal1 .= '<span><b><i>Worker Number:</i></b> '. $workerNumber .'</span>, ';  

                    endif;
                    
                    $responseInternal1 .= '<span><b><i>Job Type:</i></b> '. $workType .'</span>, '; 
                    $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal1 .= '</p>';
                }
                else{

                    $responseInternal2 .= '<p>'; 

                    if( $subId != 624 ):

                        $responseInternal2 .= '<span><b><i>Worker Type:</i></b> '. $workerType .'</span>, '; 
                        $responseInternal2 .= '<span><b><i>Worker Number:</i></b> '. $workerNumber .'</span>, '; 

                    endif;

                    $responseInternal2 .= '<span><b><i>Job Type:</i></b> '. $workType .'</span>, ';
                    $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 

                    $responseInternal2 .= '</p>';

                }
                 

            }


            $response .= '<b>First Shift: ( '. str_replace(" : ", ":", reset($v)["time"] ) .' ) </b><br>'.$responseInternal1.' </br>';

            if( !empty( $responseInternal2 ) ){
                $response .= '<b>Second Shift: ( '. str_replace(" : ", ":", reset($v)["time2"] ) .' ) </b><br>'.$responseInternal2.' </br>';
            }
            

            $response .= '</div>'; 
                

        }


        $notificationText = '';

        $response2 .= $response;

        //endif;

    endforeach;



   

    $projectP = [];

foreach( $responseP as $v )
{
    
    foreach( $v as $projectId => $projectData )
    {

        foreach( $projectData as $g )
        {
            array_push( $projectP, $g );
        }

        

    }

}

foreach( $projectP as $k => $v )
{
    
    $newProjectP[ $v['project'] ][] = $projectP[ $k ];    

}

    /* ----------------------- Final Preview ------------------------- */

$fresponse = '';


        $format = 'm-d-Y';

        $emailData = [];

        $c = \Carbon\Carbon::createFromFormat($format, $input);
        $datec = $c->toDateString();

$fresponse = '<p>Hello,</p>Here is the manpower schedule details for '.date('l', strtotime($datec)).', '.date("j F, Y", strtotime($datec)).':<br><br>';


 $forRes = [];

 //dd( $newProjectP );

$p6 = [];

 foreach ($newProjectP as $p1 => $p2) {
     
    $p3 = \DB::table('project')->where('id', $p1)->first()->foremen;

    $p4 = [];

    if( !empty( $p3 ) )
    {

        $p4 = explode(',', $p3);

    }

    foreach( $p4 as $p5 )
    {

        $p6[$p5][] = $newProjectP[$p1];

    }

 }

/* ----------------------------------------- */

$fresponse = '';

foreach( $p6 as $fid1 => $pp1 )
{

    $fresponse = '<p style="font-size: 15px; line-height: 22px">Hello, <br/> Here is the manpower schedule details for '.date('l', strtotime($datec)).', '.date("j F, Y", strtotime($datec)).':</p><br>';

    foreach( $pp1 as $pd1 )
    {

       $getaddress = json_decode(\DB::table('project')->where('id', $pd1[0]['project'])->first()->job_address);
        $street2 = null;
            if(!empty($getaddress->street_2)){
            $street2 = $getaddress->street_2.',  ';
            }  

    $jobaddressmade = $getaddress->street_1.', '.$street2.$getaddress->city.', '.$getaddress->state.', '.$getaddress->zipcode;

        $heading = '<h2 style="color: #000; text-transform: uppercase;font-size: 15px;font-weight: bold; min-height: 30px; background: #f3f3f3; padding: 8px; margin-bottom: 10px;">Project Name: '. \DB::table('project')->where('id', $pd1[0]['project'])->first()->job_name.' ('.\DB::table('project')->where('id', $pd1[0]['project'])->first()->job_number.')<br><span style="font-size: 10px;  margin: 4px 0; display:block;">'.$jobaddressmade.' </span></h2>';


  
            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            $rockshift1 = '';
            $rockshift2 = '';

            foreach ($pd1 as $g) {

                /* -------- Check Emtpy -------- */

                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];
                $workerName = $g['worker'];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];
                //$workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
 
                if( $subId != 624 ):

                    if( empty( $g["worker_type"] ) || !isset( $g["worker_type"] ) )
                    {
                        $workerType = 'N/A';
                    }
                    else
                    {
                        $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
                    }

                else:

                    //$workerName = $g['worker'];

                endif;

                /* -------- Check Emtpy -------- */

                if( $g["shift"] == 1 ){
                     
                
                     

                    if( $g["sub"] != 624 ):

                        $responseInternal1 .= '<p style="font-size: 15px; margin-bottom: 5px;">';
                        $responseInternal1 .= '<span><b><i></i></b> '. User::find( $g["sub"] )->company_name .'</span>: ';
                        $responseInternal1 .= '<span><b><i></i></b> ('. $workerNumber .') </span> ';
                        $responseInternal1 .= '<span><b><i></i></b> '. $workerType .'</span> for '; 
                        $responseInternal1 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                        $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 
                        $responseInternal1 .= '</p>';
                         
                    
                    endif;

                    if( $g["sub"] == 624 ):


                        $workerName = @\App\User::where('id', $workerName)->first();
                       
                        $classification = !empty($workerName->classification) ? ' - '.$workerName->classification : '';
                        $rockshift1 .= "<p style='font-size:13px; margin-bottom:5px;'>".ucwords($workerName['name']).$classification ." for " .$workType."</p>";


                        //$responseInternal1 .= '<span><b><i>Worker Name:</i></b> '. $workerName .'</span>, '; 

                    endif;
                
                }
                else{

                    
                
                     

                    if( $g["sub"] != 624 ):

                        $responseInternal2 .= '<p style="font-size:15px; margin-bottom: 5px;">';
                        $responseInternal2 .= '<span><b><i></i></b> '. User::find( $g["sub"] )->company_name .'</span>: ';
                        $responseInternal2 .= '<span><b><i></i></b> ('. $workerNumber .') </span>, ';
                        $responseInternal2 .= '<span><b><i></i></b> '. $workerType .'</span> for ';
                        $responseInternal2 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                        $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> ';
                        $responseInternal2 .= '</p>'; 
                         

                    endif;

                    if( $g["sub"] == 624 ):


                        $workerName = @\App\User::where('id', $workerName)->first();
                        $classification = !empty($workerName->classification) ? ' - '.$workerName->classification : '';
                        $rockshift2 .= "<p style='font-size:13px; margin-bottom:5px;'>".ucwords($workerName['name']).$classification ." for " .$workType."</p>";


                    endif;

                    

                }
 
            }
    
            $r1 = '';
            $r2 = '';

            if( !empty( $rockshift1 ) )
                $r1 = "<b>Rock Spring</b>".$rockshift1;

            if( !empty( $rockshift2 ) )
                $r2 = "<b>Rock Spring</b>".$rockshift2;

            $fresponse .= $heading;

             if( reset($pd1)["closed"]  )
            {
                $fresponse .= '<i>('.reset($pd1)["special_notes"].')</i><br></br>';
            }
          
            $fresponse .= '<b style="font-size: 14px; margin: 0 0 5px;">Job Starts at: '. str_replace(" : ", ":", reset($pd1)["time"] ) .'  </b><br>'.$r1.$responseInternal1.' </br>';
            
            if( !empty( $responseInternal2 ) || !empty( $rockshift2 ) ){
                $fresponse .= '<b>2nd shift starts at: ( '. str_replace(" : ", ":", reset($pd1)["time2"] ) .' ) </b><br>'.$r2.$responseInternal2.' </br>';

                
            }

    }

    
    $fresponse .= '<p style="font-size:15px; line-height:22px;">Regards,<br>'.ucfirst( Auth::user()->name ).'</p>';

    if( \DB::Table('users')->where('id', $fid1)->exists() )
    {
    	$foremanEmail = \DB::Table('users')->where('id', $fid1)->first()->email;

    	$notificationText = '';

    	Mail::to( $foremanEmail )->send( new ManpowerPreviewEmailsSend( $fresponse, $notificationText ) );
    }

    
    

}



foreach( $newProjectP as $p => $k )
{

    $af = \DB::table('project')->where('id', $p)->first()->foremen;

   
    $tempforres = [];
   

    if( !empty( $af ) )
    {

        $afArray = explode(',', $af);

    }

    if( !empty( $afArray ) )
    {

        foreach ($afArray as $fid) {

            
            
            $heading = '<h2>Project Name: '. \DB::table('project')->where('id', $p)->first()->job_name.' ('.\DB::table('project')->where('id', $p)->first()->job_number.')</h2>';



            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            $rockshift1 = '';
            $rockshift2 = '';
            $responseInternal1 .= '<b>Subcontractors</b>';
            foreach ($k as $g) {

                /* -------- Check Emtpy -------- */

                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];
                $workerName = $g['worker'];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];
                //$workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;

                if( $subId != 624 ):

                if( empty( $g["worker_type"] ) || !isset( $g["worker_type"] ) )
                    {
                        $workerType = 'N/A';
                    }
                    else
                    {
                        $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
                    }

                else:

                    //$workerName = $g['worker'];

                endif;

                /* -------- Check Emtpy -------- */

                if( $g["shift"] == 1 ){
                     
                
                     

                    if( $g["sub"] != 624 ):

                        $responseInternal1 .= '<p>';
                        $responseInternal1 .= '<span><b><i></i></b> '. User::find( $g["sub"] )->company_name .'</span>: ';
                        $responseInternal1 .= '<span><b><i></i></b> ('. $workerNumber .') </span> ';
                        $responseInternal1 .= '<span><b><i></i></b> '. $workerType .'</span> for '; 
                        $responseInternal1 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                        $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> '; 
                        $responseInternal1 .= '</p>';
                         
                    
                    endif;

                    if( $g["sub"] == 624 ):

                        $workerName = ucwords(@\App\User::where('id', $workerName)->first()->name);
                        $rockshift1 .= "<p>$workerName for $workType</p>";

                        //$responseInternal1 .= '<span><b><i>Worker Name:</i></b> '. $workerName .'</span>, '; 

                    endif;
                    
                    
                }
                else{

                    
                
                     

                    if( $g["sub"] != 624 ):

                        $responseInternal2 .= '<p>';
                        $responseInternal2 .= '<span><b><i></i></b> '. User::find( $g["sub"] )->company_name .'</span>: ';
                        $responseInternal2 .= '<span><b><i></i></b> ('. $workerNumber .') </span>, ';
                        $responseInternal2 .= '<span><b><i></i></b> '. $workerType .'</span> for ';
                        $responseInternal2 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                        $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> ';
                        $responseInternal2 .= '</p>'; 
                         

                    endif;

                    if( $g["sub"] == 624 ):

                        $workerName = ucwords(@\App\User::where('id', $workerName)->first()->name);
                        $rockshift2 .= "<p>$workerName for $workType</p>"; 

                    endif;

                    

                }

            }

            $r1 = '';
            $r2 = '';

            if( !empty( $rockshift1 ) )
                $r1 = "<b>Rock Spring</b>".$rockshift1;

            if( !empty( $rockshift2 ) )
                $r2 = "<b>Rock Spring</b>".$rockshift2;

            $fresponse .= $heading;
            $fresponse .= '<b>Job Starts at: '. str_replace(" : ", ":", reset($k)["time"] ) .'  ('.reset($k)["shifttype"].' Hour)</b><br>'.$r1.$responseInternal1.' </br>';
            
            if( !empty( $responseInternal2 ) || !empty( $rockshift2 ) ){
                $fresponse .= '<b>2nd shift starts at: ( '. str_replace(" : ", ":", reset($k)["time2"] ) .' ) ('.reset($k)["shifttype2"].' Hour)</b><br>'.$r2.$responseInternal2.' </br>';

                
            }

            //$tempforres[$fid] = $fresponse;

              //  array_push($forRes, $tempforres);

                $forRes[] = $fresponse;


        }

    }
    
    



}

$fresponse .= '<br>Regards,<br>'.ucfirst( Auth::user()->name ).'';

//dd($forRes);

//echo $fresponse;


    /* ----------------------- Final Preview ------------------------- */


    }

    /* Foreman email send */

    function sendPreviewAdminEmails( $input )
    {

    	$fresponse = $this->previewAdmin( $input, 'email' );
  
        $notificationText = '';

        Mail::to( config('app.adminemail') )->send( new ManpowerPreviewEmailsSend( $fresponse, $notificationText ) );

        $allAdmins = User::role('Admin')->get();
        $allSafetyOfficers = User::role('Safetyo')->get();

        foreach ($allAdmins as $g) {
            Mail::to( $g->email )->send( new ManpowerPreviewEmailsSend( $fresponse, $notificationText ) );
        }

        foreach ($allSafetyOfficers as $g) {
            Mail::to( $g->email )->send( new ManpowerPreviewEmailsSend( $fresponse, $notificationText ) );
        }

        Mail::to( Auth::user()->email )->send( new ManpowerPreviewEmailsSend( $fresponse, $notificationText ) );

    }

    function sendManpowerDayBefore()
    {
        $notificationText = "This is a reminder that the manpower with which you are linked is scheduled.";

        $data = [];

        $users = [];

        $input  = $_REQUEST['d'];
        $pr  = $_REQUEST['pr'];
          
        $format = 'm-d-Y';

        $c = \Carbon\Carbon::createFromFormat($format, $input);

        

        $key = Manpower::where('job_date','=', $c->toDateString())->where('project_id',$pr)->first();


            $date=date_create( $key->job_date );
            date_sub($date,date_interval_create_from_date_string("1 days"));
            $ldd = date_format($date,"Y-m-d");
       

            $d1 = strtotime($ldd); 
            $d2 = strtotime(date("Y-m-d")); 

            $shiftOne = [];
            $shiftSecond= [];
              
            $s = true;

            // Compare the timestamp date  
            if ( $s )
            //if ( $d1 == $d2 )
            {

                $shiftOne = [];
            $shiftSecond= [];
            $finalData = [];
                        
                $mds1 = json_decode( $key->emp_data );
                $mds2 = json_decode( $key->emp_data_2 );

                foreach( $mds1->sub as $k => $g )
                {
                    array_push($users, $mds1->sub[$k]);
                    array_push($users, $mds1->worker[$k]);
                }

                if( isset($mds2->sub) ):

                foreach( $mds2->sub as $k => $g )
                {
                    array_push($users, $mds2->sub[$k]);
                    array_push($users, $mds2->worker[$k]);
                }

            endif;

                $dp = [];
                $dp2 = [];

                $rockworker1 = [];
                $rockworker2 = [];

                foreach( $mds1->sub as $k => $v )
                {

                    /* Rockspring */

                    if( $mds1->sub[$k] == 624 )
                    {
                        $tempArray = [];

                        $subName = \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name;

                        $subName = str_replace(' ', '_', $subName);

                        $workerData = \DB::table('users')->where('id', $mds1->worker[$k])->first();

                        array_push($rockworker1, $mds1->worker[$k]);

                        $workerName = $workerData->first_name.'_'.$workerData->last_name;

                        $workerName = str_replace(' ', '_', $workerName);


                        $classification = !empty($workerData->classification) ? ' - '.$workerData->classification : '';

                        $tempArray[$subName]['worker'] = $workerName.$classification;

                        $tempArray[$subName]['work_type'] = $mds1->jobtype[$k];
                        //$tempArray[$subName]['note'] = $mds1->note[$k];
                        $tempArray[$subName]['note'] = empty( $mds1->note[$k] ) ? '-' : $mds1->note[$k];

                        array_push($shiftOne, $tempArray);
                    }

                    /* Rockspring */
                   
                    /* If other */

                    else
                    {
                        $tempArray = [];
                        $awt = (array)$mds1->a_workertype_name;
                          $awtn = (array)$mds1->a_workertype_number;
                          $sid = $mds1->sub[$k];

                          

                          if( count( $awt[$sid] ) > 1 )
                          {

                             if( isset( $dp[$sid] ) )
                            {
                              $awt = $awt[$sid][$dp[$sid]];
                              $awtn = $awtn[$sid][$dp[$sid]];

                              $dp[$sid] = $dp[$sid]+1;
                            }
                            else
                            {
                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];

                              $dp[$sid] = 1;

                            }

                          }
                          else
                          {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                          }

                          if( $awt == 'null' )
                            {
                                $workerType = '-';
                            }
                            else
                            {
                                $workerType = \DB::table('worker_types')->where('id', $awt)->first()->type;
                            }

                          $subName = \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name;

                          $subName = str_replace(' ', '_', $subName);

                        $tempArray[$subName]['worker_type'] = $workerType;
                        $tempArray[$subName]['worker_number'] = empty( $awtn ) ? '-' : $awtn;
                        $tempArray[$subName]['work_type'] = $mds1->jobtype[$k];
                        $tempArray[$subName]['note'] = empty( $mds1->note[$k] ) ? '-' : $mds1->note[$k];

                        array_push($shiftOne, $tempArray);

                    }

                    /* If other */

                }

                // Shift 2

                if( isset($mds2->sub) ):

                foreach( $mds2->sub as $k => $v )
                {

                    /* Rockspring */

                    if( $mds2->sub[$k] == 624 )
                    {
                        $tempArray = [];

                        $subName = \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name;

                        $subName = str_replace(' ', '_', $subName);

                        $workerData = \DB::table('users')->where('id', $mds2->worker[$k])->first();

                        array_push($rockworker2, $mds2->worker[$k]);

                        $workerName = $workerData->first_name.'_'.$workerData->last_name;

                        $workerName = str_replace(' ', '_', $workerName);


                        $classification = !empty($workerData->classification) ? ' - '.$workerData->classification : '';

                        $tempArray[$subName]['worker'] = $workerName.$classification;;

                        $tempArray[$subName]['work_type'] = $mds2->jobtype[$k];
                        //$tempArray[$subName]['note'] = $mds2->note[$k];
                        $tempArray[$subName]['note'] = empty( $mds2->note[$k] ) ? '-' : $mds2->note[$k];

                        array_push($shiftSecond, $tempArray);
                    }

                    /* Rockspring */
                   
                    /* If other */

                    else
                    {
                        $tempArray = [];
                        $awt = (array)$mds2->a_workertype_name;
                          $awtn = (array)$mds2->a_workertype_number;
                          $sid = $mds2->sub[$k];

                          

                          if( count( $awt[$sid] ) > 1 )
                          {

                             if( isset( $dp2[$sid] ) )
                            {
                              $awt = $awt[$sid][$dp2[$sid]];
                              $awtn = $awtn[$sid][$dp2[$sid]];

                              $dp2[$sid] = $dp2[$sid]+1;
                            }
                            else
                            {
                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];

                              $dp2[$sid] = 1;

                            }

                          }
                          else
                          {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                          }

                          if( $awt == 'null' )
                            {
                                $workerType = '-';
                            }
                            else
                            {
                                $workerType = \DB::table('worker_types')->where('id', $awt)->first()->type;
                            }

                          $subName = \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name;

                          $subName = str_replace(' ', '_', $subName);

                        $tempArray[$subName]['worker_type'] = $workerType;
                        $tempArray[$subName]['worker_number'] = empty( $awtn ) ? '-' : $awtn;
                        $tempArray[$subName]['work_type'] = $mds2->jobtype[$k];
                        $tempArray[$subName]['note'] = empty( $mds2->note[$k] ) ? '-' : $mds2->note[$k];

                        array_push($shiftSecond, $tempArray);

                    }

                    /* If other */

                }

                $finalData[1] = $shiftSecond;

                endif;

                $finalData[0] = $shiftOne;
                $finalData['project_name'] = \DB::table('project')->where('id', $key->project_id)->first()->job_name;
                $finalData['date'] = date( 'm-d-Y', strtotime($key->job_date) );
                $finalData['shift_1_time'] = $key->job_time;
                $finalData['shift_2_time'] = $key->job_2_time;
                

                //$this->se( $finalData, $notificationText, 'thistest42@yopmail.com' );

              

                foreach( \App\User::whereIn('id', array_unique( $users ) )->get() as $g )
                {


                    $finalData['contractor'] = $g->company_name;
                    $finalData['userid'] = false;

                    if( $g->hasRole('Worker') )
                        {
                            $finalData['userid1'] = false;
                            $finalData['userid2'] = false;
                            
                            $finalData['parent'] = \App\User::where('id', $g->parent )->first()->company_name;

                            if (in_array($g->id, $rockworker1))
                            {
                                $finalData['userid1'] = true;
                            }

                            if (in_array($g->id, $rockworker2))
                            {
                                $finalData['userid2'] = true;
                            }
                            
                        }
            
                    //echo $g->email;
                    //$this->se( $data, $notificationText, $g->email );
                    $this->se( $finalData, $notificationText, $g->email );
                    //$this->se( $finalData, $notificationText, '42testingemail@yopmail.com' );
                }

                \DB::table('manpower')->where( 'id', $key->id )->update( ['notification' => 1] );

            }

        

        // Return redirect back 

        Manpower::where('job_date','=', $c->toDateString())->where('project_id',$pr)->update( ['is_sechedule_email_sent' => 1] );

        \Session::flash('success', 'Schedule emails have been sent.');

        return redirect()->back();

    }

    function sendManpowerDayBeforeToAll()
    {

        //$notificationText = "This is a reminder that the manpower with which you are linked is scheduled for tomorrow.";

        $notificationText = "This is a reminder for the manpower schedule.";

        $data = [];

        $users = [];

        $input  = $_REQUEST['d'];
        $pr  = $_REQUEST['pr'];
          
        $format = 'm-d-Y';

        $c = \Carbon\Carbon::createFromFormat($format, $input);

        $userProejctIds = DB::table('project')->select('id')->where('superint', Auth::user()->id)->pluck('id'); 

        //$key = Manpower::where('job_date','=', $c->toDateString())->where('project_id',$pr)->first();

        foreach (Manpower::where('job_date','=', $c->toDateString())->whereIn('project_id', $userProejctIds)->get() as $key) {

            echo 'here';

        }

    }

    function haveData( $email, $pid, $input )
    {

        $id = \DB::table('users')->where('email', $email)->first()->id;

        $format = 'm-d-Y';

        $c = \Carbon\Carbon::createFromFormat($format, $input);

        $mdata = Manpower::where('job_date','=', $c->toDateString())->where('project_id',$pid)->first();

        $users = [];

        if( $mdata ):

        $mds1 = json_decode( $mdata->emp_data );
                $mds2 = json_decode( $mdata->emp_data_2 );

                foreach( $mds1->sub as $k => $g )
                {
                    array_push($users, $mds1->sub[$k]);
                    array_push($users, $mds1->worker[$k]);
                }

                if( isset($mds2->sub) ):

                foreach( $mds2->sub as $k => $g )
                {
                    array_push($users, $mds2->sub[$k]);
                    array_push($users, $mds2->worker[$k]);
                }

            endif;

            endif;

            $farray = array_filter( array_unique($users) );

               if (in_array($id, $farray))
               {
                return true;
               }
    }

    function se( $data, $notificationText, $e )
    {

        Mail::to( $e )->send( new ManpowerCronEmail( $data, $notificationText ) );

    }

    function sendReportNotificationCron()
    {

        $payrollFiles = PayrollFiles::select('payroll_id')->distinct('payroll_id')->get();

        $users = User::role('subcontractor')->get();

        foreach( $users as $user )
        {

            $date = \Carbon\Carbon::today()->subDays(14);

            $payrolls = Payroll::where('author', $user->id)->where('end_date', '>=', $date)->get();


            foreach( $payrolls as $payroll )
            {


                $payrollFile = PayrollFiles::where('payroll_id', $payroll->id)->first();

                if( empty( $payrollFile ) )
                {

                    $hasLogs = false;

                    $workers = User::where('parent', $user->id)->where('status', 2)->get();

                    foreach( $workers as $worker )
                    {

                        if( $worker->logs()->count() > 0 )
                        $hasLogs = true;

                    }

                    if( $hasLogs )
                    {
                        
                        $notificationText = "We've noticed that you have not uploaded the Payroll Report from last two weeks.";

                        Mail::to( $user->email )->send( new ReportCron( $user, $notificationText ) );

                    }

                }

                
            }
           

        }

        die;
        
    }

}