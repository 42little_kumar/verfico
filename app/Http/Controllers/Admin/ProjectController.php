<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\Project;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use QrCode;
use Fpdf;
use URL;
use Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $allStatus = array(

            'active' => 'Active',
            'disabled' => 'Disabled',

        );

        return view('admin.project.index', compact( 'allStatus' ));
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function favindex( Request $request )
    {

        $allStatus = array(

            'active' => 'Active',
            'disabled' => 'Disabled',

        );

        return view('admin.project.favindex', compact( 'allStatus' ));
        
    }

    /*
     * @Datatable get
     *
     *
     */
    public function getProjects( Request $request )
    {


        $projectObject = Project::orderBy('id', 'desc');

        if( isset( $request->status ) )
          $projectObject->where('status', $request->status);

        $project = $projectObject->get();

        return Datatables::of( $project )
        ->addColumn( 'jobnumber', function( $project )
        {

          if( !empty( $project->job_number ) )
          {
            $viewLink = route('admin.project.view', [ 'id' => $project->id ]);

            return "<a class='index-link' href='$viewLink'>$project->job_number</a>";
          }
          else
          {
            $viewLink = route('admin.project.view', [ 'id' => $project->id ]);
            
            return "<a class='index-link' href='$viewLink'><i>(Service Job)</i></a>";
          }

            

        })
        ->addColumn( 'jobname', function( $project )
        {
            
            return $project->job_name;

        })
        ->addColumn( 'status', function( $project )
        {
            
            return ucfirst( str_replace('_', ' ', $project->status) );

        })
         ->addColumn( 'manuallogs', function( $project )
        {
            
            $checked = '';

            if( $project->manual_log )
              $checked = 'checked';

            $html = '<div class="" style="margin-left: 10px;">


                                <label class="switch">

                                  <input type="checkbox" class="manual-log" project_id="'.$project->id.'"" value="1" '.$checked.' name="manual_log">

                                  <span class="slider round"></span>

                                </label>

                        </div>';

            return $html;

        })
        ->addColumn( 'address', function( $project )
        {

            return $project->job_address;
            
        })
        ->addColumn( 'favorite', function( $project )
        {

          $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star-o' aria-hidden='true'></i></a>";

          if( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->where('payroll_id', $project->id)->exists() )
          {
            $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star'></a>"; 
          }

            return $fl;
            
        })
        ->addColumn( 'action', function( $project )
        {

            $viewLink = route('admin.project.view', [ 'id' => $project->id ]);

            $editLink = route('admin.project.edit', [ 'id' => $project->id ]);

            $deleteLink = route('admin.project.delete', [ 'id' => $project->id ]);

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
    
                                          View
    
                                      </a>
                                  </div> 

                                  <div>
                                     <a href="' . $editLink . '" class="btn btn-sm btn-primary btn-circle" title="Edit">
                                       Edit
                                    </a>
                                  </div>

                                   <hr>
                                   <div>
                                   <a href="' . $deleteLink . '" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
                                      Delete
                                    </a>
                                  </div>
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['jobnumber', 'jobname', 'address','favorite', 'action', 'manuallogs'] )
        ->make( true );
    }

    /*
     * @Datatable get
     *
     *
     */
    public function getFavProjects( Request $request )
    {


        $favIds = [];

        foreach( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->get() as $get )
        {
            array_push($favIds, $get->payroll_id);
        }

        $projectObject = Project::orderBy('id', 'desc')->whereIn( 'id', $favIds );

        if( isset( $request->status ) )
          $projectObject->where('status', $request->status);

        $project = $projectObject->get();

        return Datatables::of( $project )
        ->addColumn( 'jobnumber', function( $project )
        {

            $viewLink = route('admin.project.view', [ 'id' => $project->id ]);

            return "<a class='index-link' href='$viewLink'>$project->job_number</a>";

        })
        ->addColumn( 'jobname', function( $project )
        {
            
            return $project->job_name;

        })
        ->addColumn( 'status', function( $project )
        {
            
            return ucfirst( str_replace('_', ' ', $project->status) );

        })
         ->addColumn( 'manuallogs', function( $project )
        {
            
            $checked = '';

            if( $project->manual_log )
              $checked = 'checked';

            $html = '<div class="" style="margin-left: 10px;">


                                <label class="switch">

                                  <input type="checkbox" class="manual-log" project_id="'.$project->id.'"" value="1" '.$checked.' name="manual_log">

                                  <span class="slider round"></span>

                                </label>

                        </div>';

            return $html;

        })
        ->addColumn( 'address', function( $project )
        {

            return $project->job_address;
            
        })
        ->addColumn( 'favorite', function( $project )
        {

          $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star-o' aria-hidden='true'></i></a>";

          if( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->where('payroll_id', $project->id)->exists() )
          {
            $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star'></a>"; 
          }

            return $fl;
            
        })
        ->addColumn( 'action', function( $project )
        {

            $viewLink = route('admin.project.view', [ 'id' => $project->id ]);

            $editLink = route('admin.project.edit', [ 'id' => $project->id ]);

            $deleteLink = route('admin.project.delete', [ 'id' => $project->id ]);

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
    
                                          View
    
                                      </a>
                                  </div> 

                                  <div>
                                     <a href="' . $editLink . '" class="btn btn-sm btn-primary btn-circle" title="Edit">
                                       Edit
                                    </a>
                                  </div>

                                   <hr>
                                   <div>
                                   <a href="' . $deleteLink . '" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
                                      Delete
                                    </a>
                                  </div>
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['jobnumber', 'jobname', 'address','favorite', 'action', 'manuallogs'] )
        ->make( true );
    }

    /*
     * @Print QR
     *
     *
     */
    public function printQR( Request $request )
    {

      $address = json_decode( $request->projectAddress );

      $street1 = $address->street_1;

      $street2 = $address->street_2;

      $city = $address->city;

      $state = $address->state;

      $zipcode = $address->zipcode;
      
      $address =  "$street1, $street2, $city, $state, $zipcode";

      $logoImage = 'data://text/plain;base64,iVBORw0KGgoAAAANSUhEUgAAAX0AAABmCAMAAAAQwbGPAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAFdQTFRF////4NfsuKPUmXvBhGG1ZTmiWyyccEaorZbO1svnzL3g6uXzwrDajm67o4nHzL7g1srmhGC0rZXNwbDa9fL5mHvBZTiib0aot6LUelSvoojHjm27eVOuZE4EwQAAB5BJREFUeJztm+2WqjwMhWHkQ2UckS9Ruf/rfIWmDNCkSY+6zlnvyvNvRkLb3XS3tBBFiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoivLvEX/tkjTLsjzZH+Bfx0wSWHyfkiTL0iT5OYtKyraUq5/L5U+XbXDpRHtvBlTIhXmS7L+L4MquqUUhTBnFT768Ot1NtTrx6jf7TWDMhkTlse3miK6ty2Z9y7K6wo/XHrldUfbXjKbHC43LFr389s3UtijvHRo5UXEh137TPPfqnbmw7cuyPFaPMTQ5Fz9ZxqRGsx9HS/aonoFVNUx32bHpNJY4WPHR3mpan5DP3438Q7miepB6TNRQ5OX+rG91t1VI2ApDcdmirNoURpZmQwZG+ij6miS8LIZsbWuGDuOZ4xjY9XMBxTTA0z1X4KJ2D+Jnr/jWSRxbigef+tF1LVhhBMxSdrxCv63/OYWTpR1NCOZMS5pkyvuNzn3Hq7+f5FtljsnpHVPkCDSoJX7OEW0XHHH1o6bzqd9u07UXyh9j6k/3I0uDEM48blM7nAESd4z6pte26hnPSNjxZhPxRtz8+ZNPkpJQ/zkoPOpXjlnAbHzj6ourHw2e0oiQFU1OudMkPz34Ta8hxnERNWdOfjw5an/q0+oXlJeNuOqb2npNboKQ8ugpTaC+0bBDtTr6bO25HhoDEe2KTmY+TedqMfNgTI9UPxo8vYaoD/acM5WlpLzSpQnUN4udI/7j3aN+RaeM56ft7al2P43n6o2l1b+HqR91In+mpPSUxqtPN2LEM4WZOWXAfzSO7l8uRaNJkF3fc71HV7zwTBeY+mA9TG0pKRu6NFZ9Y/p0yRXZMTdffptGJp6CDQ/yume9/BOHP20oMPUfIuMX2EhwSOXL4Gg0AKJ9MGMSChWidJo3DdxBX9Ir0VXoO9Q3/setyz+hfs51+4Nonwkk5/tBmPzmOlfo1r/cjCj1PT4w8X7neSHEu+YzV+BrgZgZrbCKlj5CbodQ4RuPBlz9mhkMmPq5dxhbECnTFzvMdLt3cYGXcGfyBSzF89wD4IvOin9Ax9WvwtWHROI8zJWyeXG4NJnfP2hy5s5g/MRz7AIjx3aA5cQDyAJc/Uu4+ieR8SBSHl9Uv5Zm6BYQ1+MOnWg4z3daZ/qRnXMJ9QsuhV31a2EGulImL6rP+QdJyY5W2VQW2X2v9fz8YJ99CPVvweqD+Pw2sCNl/epELZZoS8UmjHD3ZLbdZR0KyUrSUb85fDHbopGr/l4qPkh5sJx3vHCM+qnQHhwq1rJasamZflo6jWDOJc8XQ9Qv7KmcxHqxwl5TP2N+J7mzteb7x+KuetOs48NeUT/Z7/fmCPtJV0mO4v4l9S9S9SXLqc1509gddz7KqL88WexbqfoL7oJT6AlosaW9/j31eV+BZgpkhEvT+e9EMOfis27cydS/jPqZVdlJUMEJR6rqr6nP+4rceew2v7X6QjZi0DVPGeD7wmNXiyvV4y+r78nsAPVhJNknszt52rACf9rynHUsqlXZgsYxJ3J9TMryPeqHr3ngLNrTVJiXJTpudkRT5lgFwNVvA9RvhlWvMyBSvqj+sGp2AG982vq92PhNLRwwb9jniX0nm1ver34bkKArYIPIcxZ6CTE1sODJA27CsYirXzA2vn7a6gNSBGlPHL4tuqQPMOc1A+dZ6SKdecyic5xGYsEWz8RbTlfMsVYq6e6QbJKFCDdXEbgdIhgc/EaDoZ5VaKVu9Rb14X06ybLz/epDzv3BtHtkRg1MDNJHmXnR2Qjn3HedLJbiLPmA+mA90jXvAu97aHZsyLWx75bU4vHiVT+mbuLsccKjHp8mH1AfDMJ3/trgA4M5k2RelXCw75bc+GMVwKt+Qs0d7v6+dNn5AfXtqscjU0LMnL8TJUIcbAtm/sulc65f/Zpcxrnqmxfv+C2RT6gPhksnf0Wldw1egdIGuf7IvGUpDfKo36TkHin9JiG37P6E+tZBKJ+s6YnVeRn7F5P6YbMJrGG5VxlmPOqf6AGEvdPQiqz/I+rbN7nwsmOPe8CDOjY0pvfcpBYCQBqIu4xWv/ckMqa+3XHwTzifUR/KRru+TK+eKpkHdcS0dkFJbJnuJp1zrfqI8z27kT6cQddi8Njjlb8MVx9C/C0C+bMv55ea+KbKEuNvik/iux9jcExpKTkPWFzutq3Zee9yQ/us5+WHoRmyJSYLsfIn6+uKEyP+/JXQqtLN6Q9sZ7oZ5WMoDZwL3s4HKL44HA7fPylto9E8wzqKwK5USn69aL5ykLwb6YYwedjYLylvX3PrxzP7gf+Ub6p2vrDZb/MZnbySv7TyNWrxBeKjUE/LxX6+ZL9uWmM/78x/zkijm/PNBubfskEdElLOn7+OXzyfkjGyk230DqbSh0NTHM67SZRWnMErYuGcy34tjRqP+7V0Rdxz2263ALaKoSH1ZX11V0mNe/62FAIf4ccFwEW2xdOUDGjNC+eyVY7ExP+fuAWwVQwPKeoH9EB3qYK2/Bsb2V3a8LOCRQ1CHs/+n/yxAuEbpYqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIqiKIryf+c/4emMbpvXalkAAAAASUVORK5CYII=';

      $qrImage = 'data://text/plain;base64,'. $request->imageURL;

      $info = getimagesize( $qrImage );

      Fpdf::AddPage();

      Fpdf::SetFont( 'Arial', 'B', 16 );

      Fpdf::Image( $logoImage, 72,10,60,15,'png' );

      Fpdf::Cell( 0, 60, $request->projectNumber.' '.$request->projectName, 0, 0, 'C' );

      Fpdf::Ln();

      Fpdf::SetFont("Arial","",10);

      Fpdf::Cell( 0, -45, $address, 0, 0, 'C' );

      Fpdf::Ln();

      Fpdf::Image( $qrImage, 30,60,150,150,'png' );

      Fpdf::SetY(-15);

      Fpdf::SetFont("Arial","",10);

      Fpdf::Cell(0,-10,'Copyright © Rockspring Contracting LLC 2020',0,0,'C');

      Fpdf::Output();

      die;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subContractors = User::role('subcontractor')->get();

        $data = compact(

            'subContractors'

        );
        
        return view('admin.project.create', $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $subContractors = User::role('subcontractor')->get();
     
        $projectData = Project::where('id', $id)->firstOrFail();

        $decodedAddress = json_decode( $projectData->job_address );

        $address = $decodedAddress;

        $previous = null;
    
        $next = null;
        
        if(  isset( $_GET["dir"] ) ):
            
        
    if(@$_GET["from"]=="workers"){
      
      if(@$_GET["column"]==1){
        

         
            $usersobj = Project::orderBy('job_name',@$_GET["dir"]);

            
            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }
            
            $users = $usersobj->get();

          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
      }
      if(@$_GET["column"]==0){
        

          $usersobj = Project::orderBy('job_number',@$_GET["dir"]);

            
            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }
            
            $users = $usersobj->get();

          $users = $usersobj->get();
          
          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
            }
            

        }
        
        endif;

        return view( 'admin.project.edit', compact( 'projectData', 'subContractors', 'address', 'previous', 'next' ) );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        /* Manpower */

        $mid = 213;

  
      $d = date('m-d-Y');

      if( isset( $_REQUEST['d'] ) )
      {
        $d = $_REQUEST['d'];
      }


          
      $format = 'm-d-Y';


      $cd = \Carbon\Carbon::createFromFormat($format, $d);

        $p = Project::all();
        $s = User::role('Subcontractor')->get();
        $w = User::role('Worker')->get();
        $f = User::role('Foreman')->get();
        $w = $w->merge($f);
        $md = \App\Manpower::whereDate('job_date','=', $cd->toDateString())->where('project_id', $id)->first();

        $mid = @$md->id;

        /* Manpower */

        $address = 'N/A';
        
        $projectData = Project::where('id', $id)->firstOrFail();

        if( !empty( $projectData->job_address ) )
        {

          $decodedAddress = json_decode( $projectData->job_address );

          $decodedAddressArray = (array)$decodedAddress;

          $decodedAddressArray = array_filter( $decodedAddressArray );

          $address = implode(', ', $decodedAddressArray );

        }

        $projectData->job_address_json = $projectData->job_address;

        $projectData->job_address = $address;

        $previous = null;
    
        $next = null;
        
        if(  isset( $_GET["dir"] ) ):
            
        
    if(@$_GET["from"]=="workers"){
      
      if(@$_GET["column"]==1){
        

         
            $usersobj = Project::orderBy('job_name',@$_GET["dir"]);

            
            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }
            
            $users = $usersobj->get();

          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
      }
      if(@$_GET["column"]==0){
        

          $usersobj = Project::orderBy('job_number',@$_GET["dir"]);

            
            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }
            
            $users = $usersobj->get();

          $users = $usersobj->get();
          
          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
            }
            

        }
        
        endif;

        return view( 'admin.project.view', compact( 'projectData', 'previous', 'next' ) )->with('p', $p)
        ->with('w', $w)
        ->with('md', $md)
        ->with('mid', $mid)
        ->with('s', $s);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function isJson( $string )
    {
        
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'job_number' => 'required|unique:project',

            'job_name' => 'required',

            'status' => 'required',

        ]);

        

        if($validator->fails()) {

            $request->flash();

            if( $request->job_type != 'service' )
            {
              if($validator->errors()->first('job_number')) {
        
                Session::flash('error', 'Please verify job number. It should not be empty or duplicate.');
        
                return redirect()->back();
        
              }

                if($validator->errors()->first('general_contractor')) {
          
                  Session::flash('error', 'Please enter general contractor.');
          
                  return redirect()->back();
          
              }

              if($validator->errors()->first('job_description')) {
        
                Session::flash('error', 'Please enter job description.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('status')) {
        
                Session::flash('error', 'Please select the status.');
        
                return redirect()->back();
        
            }

             if($validator->errors()->first('started_at')) {
        
                Session::flash('error', 'Please enter start date.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('completion_at')) {
        
                Session::flash('error', 'Please enter completion date.');
        
                return redirect()->back();
        
            }

              

            }
        
            

            if($validator->errors()->first('job_name')) {
        
                Session::flash('error', 'Please enter job name.');
        
                return redirect()->back();
        
            }

            

             

           
        
        }

        if( $request->job_type != 'service' )
        {
          $reqStatus = $request->input('status');
          $reqLog = $request->input('manual_log');
          
        }
        else
        {
          $reqStatus = 'active';
          $reqLog = 1;
        }

        $startDate = null;

        $completionDate = null;

        $jobNumber = $request->input('job_number');

        $jobName = $request->input('job_name');

        $subContractor = $request->input('sub_contractor');

        $generalContractorName = $request->input('general_contractor_name');

        $jobDescription = $request->input('job_description');

        $status = $reqStatus;

        $manualLog = $reqLog;

        $superint = $request->input('superint');

        /* Address */

        $jobAddress = json_encode( $request->input('address') );

        $locationAddress = implode(',', $request->input('address'));

        /* Address */

        if( !empty( $request->input('started_at') ) )
        {

          $startDate = date( "Y-m-d H:i:s", strtotime( $request->input('started_at') ) );

        }

        if( !empty( $request->input('completion_at') ) )
        {

          $completionDate = date( "Y-m-d H:i:s", strtotime( $request->input('completion_at') ) );
          
        }

        $qrCode = 'QrCode';

        $foremenVals = '';

        if( !empty( $request->foremen ) ){

          $foremenVals = implode(',', $request->foremen );

        }

        $project = Project::create([

            'job_type' => $request->job_type,

            'job_number' => $jobNumber,

            'job_name' => $jobName,

            'superint' => $superint,

            'sub_contractor' => $subContractor,

            'foremen' => $foremenVals,

            'general_contractor_name' => $generalContractorName,

            'qrcode' => $qrCode,

            'job_address' => $jobAddress,

            'job_description' => $jobDescription,

            'status' => $status,

            'manual_log' => $manualLog,

            'started_at' => $startDate,

            'completion_at' => $completionDate
            
        ]);

        $geoData = $this->getLatlang( $locationAddress );

        /*-------------------------------------------- Seperate --------------------------------------------*/

        $baseUrl = url('/');

        $QrDataArray = [];

        $QrDataArray['id'] = $project->id;

        $QrDataArray['job_name'] = $project->job_name;
        $QrDataArray['job_number'] = $project->job_number;
        $QrDataArray['lat'] = $geoData->lat;
        $QrDataArray['lng'] = $geoData->lng;
        $QrDataArray['radious'] = 3200;

        $QrDataJson = json_encode( $QrDataArray );

        $qrdata = base64_encode( QrCode::format('png')->size(265)->generate( $QrDataJson ) );

        $qrUserData = array(

          'lat' => $geoData->lat,

          'lng' => $geoData->lng,

          'qrcode' => $qrdata

        );

        Project::where( 'id', $project->id )
        ->update( $qrUserData );

        /*-------------------------------------------- Seperate --------------------------------------------*/

        Session::flash('success', 'New project has been created successfully.');

        return redirect()->route('admin.projects');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


       $validator = Validator::make($request->all(), [

           'job_number' => 'required|unique:project,job_number,'.$id,

           'job_name' => 'required',

           'status' => 'required',

       ]);

       if($validator->fails()) {

           $request->flash();

           if( $request->job_type != 'service' )
            {
              if($validator->errors()->first('job_number')) {
       
               Session::flash('error', 'Please verify job number. It should not be empty or duplicate.');
       
               return redirect()->back();
       
           }

           if($validator->errors()->first('general_contractor')) {
       
               Session::flash('error', 'Please enter general contractor.');
       
               return redirect()->back();
       
           }

           if($validator->errors()->first('job_address')) {
       
               Session::flash('error', 'Please enter job address.');
       
               return redirect()->back();
       
           }

           if($validator->errors()->first('job_description')) {
       
               Session::flash('error', 'Please enter job description.');
       
               return redirect()->back();
       
           }

            }
       
           

           if($validator->errors()->first('job_name')) {
       
               Session::flash('error', 'Please enter job name.');
       
               return redirect()->back();
       
           }

           

           
       
       }

       if( $request->job_type != 'service' )
        {
          $reqStatus = $request->input('status');
          $reqLog = $request->input('manual_log');

          $jobNumber = $request->input('job_number');
          
        }
        else
        {
          $reqStatus = 'active';
          $reqLog = 1;

          $jobNumber = NULL;

        }

   
        $startDate = null;

        $completionDate = null;

        $jobNumber = $jobNumber;

        $jobName = $request->input('job_name');

        $superint = $request->input('superint');

        $subContractor = $request->input('sub_contractor');

        $manualLog = $reqLog;

        $generalContractorName = $request->input('general_contractor_name');

        /* Address */

        $jobAddress = json_encode( $request->input('address') );

        $locationAddress = implode(',', $request->input('address'));

        /* Address */

        $jobDescription = $request->input('job_description');

        $status = $request->input('status');

        if( !empty( $request->input('started_at') ) )
        {

          $startDate = date( "Y-m-d H:i:s", strtotime( $request->input('started_at') ) );

        }

        if( !empty( $request->input('completion_at') ) )
        {

          $completionDate = date( "Y-m-d H:i:s", strtotime( $request->input('completion_at') ) );

        }

        $foremenVals = '';

        if( !empty( $request->foremen ) ){

          $foremenVals = implode(',', $request->foremen );

        }

      $geoData = $this->getLatlang( $locationAddress );

       $projectData = array(

           'job_type' => $request->job_type,

           'job_number' => $jobNumber,

           'job_name' => $jobName,

           'superint' => $superint,

           'sub_contractor' => $subContractor,

           'foremen' => $foremenVals,

           'general_contractor_name' => $generalContractorName,

           'job_address' => $jobAddress,

           'lat' => $geoData->lat,

           'lng' => $geoData->lng,

           'manual_log' => $manualLog,

           'job_description' => $jobDescription,

           'status' => $status,

           'started_at' => $startDate,

           'completion_at' => $completionDate

       );

       $projectObject = Project::find( $id );

       /*-------------------------------------------- Seperate --------------------------------------------*/

        $baseUrl = url('/');

        $QrDataArray = [];

        $QrDataArray['id'] = $id;

        $QrDataArray['job_name'] = $jobName;
        $QrDataArray['job_number'] = $jobNumber;
        $QrDataArray['lat'] = $geoData->lat;
        $QrDataArray['lng'] = $geoData->lng;
        $QrDataArray['radious'] = $projectObject->radious;

        $QrDataJson = json_encode( $QrDataArray );

        $qrdata = base64_encode( QrCode::format('png')->size(265)->generate( $QrDataJson ) );

        $projectData['qrcode'] = $qrdata;

        /*-------------------------------------------- Seperate --------------------------------------------*/

       $projectObject->update( $projectData );

       Session::flash('success', 'Project has been updated successfully.');

       return redirect()->route('admin.project.edit', $projectObject->id);

    }

    /* Get Lat Lang */
    public function getLatlang( $address )
    {
        $url = "http://www.mapquestapi.com/geocoding/v1/address?key=MBqM8eGsxJWKZ6HQQOGS7O6F0ipelmOS&location=".urlencode( $address );


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $data = curl_exec($ch);

        curl_close($ch);

        $latLng = json_decode($data)->results[0]->locations[0]->latLng; //$latLng->lat, $latLng->lng;

        return $latLng;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $project = Project::where('id', $id)->firstOrFail();

        $project->delete();

        Session::flash('success', 'Project has been deleted successfully.');

        return redirect()->back();

    }
}
