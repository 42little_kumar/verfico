<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\WorkLog;
use App\Project;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use QrCode;


class LogController extends Controller
{
    
	/*
	 * @Function Name
	 *
	 *
	 */
	public function getUserLogs( Request $request )
	{

		$logs = User::find( $request->userId )->logs;

        //$logs = WorkLog::where( 'worker_id', $request->userId )->get();

     
		return Datatables::of( $logs )
        ->addColumn('project_name', function( $logs )
        {

           return @$logs->project->job_name;

        })
        ->addColumn('project_number', function( $logs )
        {

           return @$logs->project->job_number;

        })
        ->addColumn('punch_in', function( $logs )
        {
            $autoOut = asset('public/images/auto_out.png');

            $adminOut = asset('public/images/admin_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-in-time'> <input class='punch-input' value='$logs->punch_in_time' type='text'> </div>";

            switch ( $logs->in_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon in-image' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon in-image' src='$byForeman'>";
                    break;
                case '5':
                    $imgSrc = "<img width='20' class='log-icon in-image' src='$adminOut'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }
            
            $punchTime = "<div style='width: 97px;' class='punch-in-time'>".$logs->punch_in_time."</div><span class='imgplacein'>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;

        })
        ->addColumn('punch_out', function( $logs )
        {

            $autoOut = asset('public/images/auto_out.png');

            $adminOut = asset('public/images/admin_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');

        	if( empty( $logs->punch_out ) )
        	{

        		return "<span class='status_active'>Punched In</span>";

        	}

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-out-time'> <input class='punch-input' value='$logs->punch_out_time' type='text'> </div>";

            switch ( $logs->out_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$byForeman'>";
                    break;
                case '4':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$autoOut'>";
                    break;
                case '5':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$adminOut'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }
            
            $punchTime = "<div style='width: 97px;' class='punch-out-time'>".$logs->punch_out_time."</div><span class='imgplaceout'>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;
            
        })
         ->addColumn( 'total_hours', function( $logs )
        {

            if( empty( $logs->punch_out ) )
            {

                return "<div style='text-align: center;'>N/A</div>";

            }

            //$start  = new Carbon( $logs->punch_in );
                            
            //$end    = new Carbon( $logs->punch_out );

            $logs->punch_in = substr_replace($logs->punch_in, '00', -2);
            $logs->punch_out = substr_replace($logs->punch_out, '00', -2);

            $start = date("Y-m-d ".$logs->punch_in);
            
            if(strtotime($logs->punch_out) < strtotime($logs->punch_in)){
                
                $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logs->punch_out) ) + 24*60*60) ;
            } else {
                $end = date("Y-m-d ".$logs->punch_out);
            }

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

            $totalTimeInt = (float)str_replace(':','.',$totalTime);

            $overTime = '';

            if( $totalTimeInt > 8 )
            {
                $overTime = "<span><img style='width: 20px;' title='Overtime' class='log-icon' src='".asset('public/images/time.png')."'></span>";
            }

            return "<div class='total-hours-class' style='text-align: center;'>".$totalTime."</div>";

            return "<div style='text-align: center;'>".$start->diff($end)->format('%H:%I')."</div>";

        })
        ->addColumn('subcontractor', function( $logs )
        {

            if( empty($logs->sub) )
            return $logs->user->sub_contractor_name;

            return User::find($logs->sub)->company_name;
            
        })
        ->addColumn('date', function( $logs )
        {

            $date = getTimeZonePunchDate( $logs->punch_at );

            $editDataElement = "<div style='display: none; width: 115px;' class='edit-date'> <input class='date-input' value='$date' type='text'> </div>";

            $dateText = "<div style='width: 115px;' class='date-text'>".$date."</div>";

            return $dateText.$editDataElement;

            
        })
        ->addColumn('action', function( $logs )
        {

            $viewLink = '';

            $editLink = '';

            $deleteLink = route('admin.log.delete', [ 'id' => $logs->id ]);

          
                $editButton = '<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-circle btn-action-edit" title="Edit">
    
                            <i class="fas fa-pencil-alt"></i>
    
                        </a>';

            $html = '<div class="table-action-container">

                        <!--<a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
    
                            <i class="far fa-eye"></i>
    
                        </a>-->

                        '.$editButton.'

                        <a style="display: none;" href="javascript:void(0)" log-id="' . $logs->id . '" class="btn btn-sm btn-success btn-circle btn-action-update" title="Save">
    
                            <i class="fas fa-check"></i>
    
                        </a>

                        <a href="' . $deleteLink . '" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
        
                            <i class="fas fa-trash"></i>
    
                        </a>

                    </div>';

            return $html;

        })
        ->rawColumns( ['project_name', 'punch_in', 'punch_out', 'total_hours', 'date', 'action'] )
        ->make( true );

	}

    /*
     * @Export project logs
     *
     *
     */
    public function exportProjectLogs( Request $request )
    {
        $worklogObject = Worklog::orderBy('punch_at', 'desc');


        $fields = ['project_number' => 'Project Number','project_name' => 'Project Name', 'rsc_employee_id' => 'RSC ID', 'worker_name' => 'Worker Name','classification' => 'Classification', 'company_name' => 'Company Name', 'worker_title' => 'Worker Title', 'punch_date' => 'Date', 'punch_in_time' => 'Punch In', 'punch_out_time' => 'Punch Out', 'total_punch_time' => 'Total Hrs'];


        if( isset( $request->date_from ) && isset( $request->date_to ) )
        {

            $from = Carbon::parse( $request->date_from )
                             ->startOfDay()
                             ->toDateTimeString();

            $to = Carbon::parse( $request->date_to )
                             ->endOfDay()
                             ->toDateTimeString();

            $worklogObject = $worklogObject->whereBetween('punch_at', [$from, $to]);

        }

        if( isset( $request->sub_id ) )
        {

            $subUsers = User::where( 'parent', $request->sub_id )->pluck('id')->toArray();

            $worklogObject = $worklogObject->whereIn('worker_id', $subUsers);


            $fields = ['project_number' => 'Project Number','project_name' => 'Project Name', 'rsc_employee_id' => 'RSC ID', 'worker_name' => 'Worker Name','classification' => 'Classification', 'worker_title' => 'Worker Title', 'punch_date' => 'Date', 'punch_in_time' => 'Punch In', 'punch_out_time' => 'Punch Out', 'total_punch_time' => 'Total Hrs'];


        }

        $data = $worklogObject->get();

        $csvExporter = new \Laracsv\Export();

        $csvExporter->build($data, $fields)->download();

    }


     /*
     * @Export project logs
     *
     *
     */
    public function workerProjectLogs( Request $request, $projectId = null )
    {

            $data = WorkLog::where( 'project_id', $projectId )->get();

            $csvExporter = new \Laracsv\Export();

            $csvExporter->build($data, ['project_name', 'worker_name', 'company_name', 'worker_title', 'punch_in', 'punch_out', 'created_at'])->download();

    }

    public function projectLogStatusUpdate( Request $request )
    {

        $log = NULL;

        if( $request->manual_log == 1 )
            $log = 1;

        $project = Project::find( $request->project_id );

        $project->update( [ 'manual_log' => $log ] );

    }

    /*
     * @Project logs
     *
     *
     */
    public function getProjectLogs( Request $request )
    {
        //$logs = User::find( $request->userId )->logs;

        $logsObject = WorkLog::where( 'project_id', $request->projectId );

        if( isset( $request->log_date ) )
        {
                
            list( $startDate, $endDate ) = explode( '-', $request->log_date );

            /* Format date */

            $from = Carbon::parse( $startDate )
                             ->startOfDay()
                             ->toDateTimeString();

            $to = Carbon::parse( $endDate )
                             ->endOfDay()   
                             ->toDateTimeString();

            /* Format date */

            $logsObject->whereBetween('punch_at', [$from, $to])->get();

        }

        $logs = $logsObject->orderBy('id', 'desc')->get();

        return Datatables::of( $logs )
        ->addColumn('worker_name', function( $logs )

        { 
            $classification = !empty($logs->user->classification) ? ' - '.$logs->user->classification : '';      

           return @$logs->user->first_name.' '.@$logs->user->last_name.$classification;

        })
        ->addColumn('worker_name_without_classification', function( $logs )
        {
           return @$logs->user->first_name.' '.@$logs->user->last_name;

        })
        ->addColumn('classification', function( $logs )
        {
          $classification = !empty($logs->user->classification) ? $logs->user->classification : '';      
           return $classification;

        })
        ->addColumn('subcontractor', function( $logs )
        {

            //return $logs->user->sub_contractor_name;

            if( empty($logs->sub) )
            return @$logs->user->sub_contractor_name;

            return @User::find($logs->sub)->company_name;
            
        })
        ->addColumn('title', function( $logs )
        {

            return ucwords( @$logs->user->worker_type_name );
            
        })
        ->addColumn('punch_in', function( $logs )
        {

            $autoOut = asset('public/images/auto_out.png');

            $adminOut = asset('public/images/admin_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');

            switch ( @$logs->in_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon in-image' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon in-image' src='$byForeman'>";
                    break;
                case '5':
                    $imgSrc = "<img width='20' class='log-icon in-image' src='$adminOut'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-in-time'> <input class='punch-input' value='$logs->punch_in_time' type='text'> </div>";
            
            $punchTime = "<div style='width: 97px;' class='punch-in-time'>".@$logs->punch_in_time."</div><span class='imgplacein'>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;

        })
        ->addColumn('punch_out', function( $logs )
        {

            $autoOut = asset('public/images/auto_out.png');

            $adminOut = asset('public/images/admin_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');

            if( empty( @$logs->punch_out ) )
            {

                return "<span class='status_active'>Punched In</span>";

            }

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-out-time'> <input class='punch-input' value='$logs->punch_out_time' type='text'> </div>";

            switch ( @$logs->out_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$byForeman'>";
                    break;
                case '4':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$autoOut'>";
                    break;
                case '5':
                    $imgSrc = "<img width='20' class='log-icon out-image' src='$adminOut'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }
            
            $punchTime = "<div style='width: 97px;' class='punch-out-time'>".@$logs->punch_out_time."</div><span class='imgplaceout'>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;
            
        })
        ->addColumn( 'total_hours', function( $logs )
        {

            if( empty( $logs->punch_out ) )
            {

                return "<div style='text-align: center;'>N/A</div>";

            }

            //$start  = new Carbon( $logs->punch_in );
                            
            //$end    = new Carbon( $logs->punch_out );

            $logs->punch_in = substr_replace(@$logs->punch_in, '00', -2);
            $logs->punch_out = substr_replace(@$logs->punch_out, '00', -2);
			
			$start = date("Y-m-d ".@$logs->punch_in);
			
			if(strtotime(@$logs->punch_out) < strtotime(@$logs->punch_in)){
				
				$end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".@$logs->punch_out) ) + 24*60*60) ;
			} else {
				$end = date("Y-m-d ".@$logs->punch_out);
			}

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

            $totalTimeInt = (float)str_replace(':','.',$totalTime);

            $overTime = '';

            if( $totalTimeInt > 8 )
            {
                $overTime = "<span><img style='width: 20px;' title='Overtime' class='log-icon' src='".asset('public/images/time.png')."'></span>";
            }
            
			return "<div class='total-hours-class' style='text-align: center;'>".$totalTime."</div>";

            return "<div style='text-align: center;'>".$start->diff($end)->format('%H:%I')."</div>";

        })
        ->addColumn('date', function( $logs )
        {

            $date = getTimeZonePunchDate( @$logs->punch_at );

            $editDataElement = "<div style='display: none; width: 115px;' class='edit-date'> <input class='date-input' value='$date' type='text'> </div>";

            $dateText = "<div style='width: 115px;' class='date-text'>".$date."</div>";

            return $dateText.$editDataElement;

            
        })
        ->addColumn('action', function( $logs )
        {

            $viewLink = '';

            $editLink = '';

            $deleteLink = route('admin.log.delete', [ 'id' => @$logs->id ]);;

            $html = '<div class="table-action-container">

                        <!--<a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
    
                            <i class="far fa-eye"></i>
    
                        </a>-->

                        <a href="javascript:void(0)" class="btn btn-sm btn-primary btn-circle btn-action-edit" title="Edit">
    
                            <i class="fas fa-pencil-alt"></i>
    
                        </a>

                        <a style="display: none;" href="javascript:void(0)" log-id="' . @$logs->id . '" class="btn btn-sm btn-success btn-circle btn-action-update" title="Save">
    
                            <i class="fas fa-check"></i>
    
                        </a>

                        <a href="' . $deleteLink . '" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
        
                            <i class="fas fa-trash"></i>
    
                        </a>

                    </div>';

            return $html;

        })
        ->rawColumns( ['project_name', 'punch_in', 'punch_out', 'total_hours', 'date', 'action'] )
        ->make( true );

    }

    /*
     * @Creat log
     *
     *
     */
    public function create( $userId )
    {

        $allProjects = Project::all();

        $data = compact(

            'allProjects',

            'userId'

        );

        return view( 'admin.worklog.create', $data );

    }

    /*
     * @Save log
     *
     *
     */
    public function save( Request $request )
    {

        $data = [];

        $data['punch_in'] = $request->punch_in;

        $data['punch_out'] = $request->punch_out;

        $data['project_id'] = $request->project_id;

        $data['worker_id'] = $request->user_id;

        WorkLog::create( $data );

        Session::flash('success', 'New log created successfully.');

        return redirect( route( 'admin.users.view', $request->user_id ) );

    }

    /*
     * @Function Name
     *
     *
     */
    public function update( Request $request )
    {

        try
        {
            
            $logId = $request->logId;

            $punchIn = $request->punchIn;

            $punchOut = $request->punchOut;

            $dateVal = date( 'Y-m-d H:i:s', strtotime($request->dateVal) );


            $logUpdateArray = [];

            list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

            //$logUpdateArray['punch_in'] = date( "H:i:s", strtotime( $punchIn ) );

            //$logUpdateArray['punch_out'] = date( "H:i:s", strtotime( $punchOut ) );

            $punchIn = date( 'H:i:s', strtotime( $punchIn ) );

            $punchOut = date( 'H:i:s', strtotime( $punchOut ) );

            $punchIn = Carbon::createFromFormat('H:i:s', $punchIn, $timezone) ->setTimezone('UTC');

            $punchOut =  Carbon::createFromFormat('H:i:s', $punchOut, $timezone) ->setTimezone('UTC');

            $logUpdateArray['punch_in'] = date( 'H:i:s', strtotime( $punchIn ) );

            $logUpdateArray['punch_out'] =  date( 'H:i:s', strtotime( $punchOut ) );

     
            //$logUpdateArray['created_at'] = $dateVal;

            $logObject = WorkLog::find( $logId );

            if( $request->punchIn != $request->oldin )
                $logUpdateArray['in_type'] =  5;

            if( $request->punchOut != $request->oldout )
                $logUpdateArray['out_type'] =  5;

          

            if( empty( $logObject->punch_out ) )
                unset( $logUpdateArray['punch_out'] );

            $logObject->update( $logUpdateArray );

            /* */

            $logAfterUpdate = WorkLog::find( $logId );

            $logAfterUpdate->punch_in = substr_replace($logAfterUpdate->punch_in, '00', -2);
            $logAfterUpdate->punch_out = substr_replace($logAfterUpdate->punch_out, '00', -2);

            $start = date("Y-m-d ".$logAfterUpdate->punch_in);
            
            if(strtotime($logAfterUpdate->punch_out) < strtotime($logAfterUpdate->punch_in)){
                
                $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logAfterUpdate->punch_out) ) + 24*60*60) ;
            } else {
                $end = date("Y-m-d ".$logAfterUpdate->punch_out);
            }

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

            /* */

            $response['status'] = true;

            $response['dateVal'] = $request->dateVal;

            $response['data'] = $logObject;

            $response['total_time'] = $totalTime;

            $response['message'] = 'Log updated.';

            return json_encode( $response );

        }
        catch (Exception $e)
        {
            
            $response['status'] = false;

            $response['message'] = 'Some error.';

            return json_encode( $response );

        }

        

    }

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = WorkLog::where('id', $id)->firstOrFail();

        $user->delete();

        Session::flash('success', 'Log deleted successfully.');

        return \Redirect::to(\URL::previous() . "#logs");


    }

}
