<?php

namespace App\Http\Controllers\Admin;

use Session;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $roles = Role::where('slug', '!=', 'administrator')->where('slug', '!=', 'trainer')->where('slug', '!=', 'user');

        if(request()->query('search')) {

            $string = '%' . request()->query('search') . '%';

            $roles = $roles->where('name', 'LIKE', $string);

        }

        if(request()->query('limit') && is_numeric(request()->query('limit'))) {

            $limit = request()->query('limit');

        } else {

            $limit = 10;

        }

        if(request()->query('sort-by')){
        
            $sort_by = explode('-', request()->query('sort-by'));
        
            if(isset($sort_by[0]) && $sort_by[0] == 'name') {

                $field = $sort_by[0];

            } else {

                $field = 'name';

            }

            if(isset($sort_by[1]) && ($sort_by[1] == 'ASC' || $sort_by[1] == 'DESC')) {

                $order = $sort_by[1];

            } else {

                $order = 'ASC';

            }

            $roles = $roles->orderBy($field, $order)->paginate($limit);
           
        } else {
        
            $roles = $roles->orderBy('updated_at', 'DESC')->paginate($limit);
        
        }
        
        return view('admin.roles.index', compact('roles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [

            'name' => 'required'

        ]);

        if($validator->fails()) {

            $request->flash();
        
            if($validator->errors()->first('name')) {

                Session::flash('open-modal', 'true');
        
                Session::flash('error', 'Please enter name of role.');
        
                return redirect()->back();
        
            }
        
        }

        $name = $request->input('name');
        
        $slug = Str::slug($name);

        if(Role::where('slug', $slug)->first()) {

            $request->flash();

            Session::flash('open-modal', 'true');
        
            Session::flash('error', 'Role already exists. Please try again with different information.');
    
            return redirect()->back();

        }

        $role = Role::create([
                
            'slug' => $slug,

            'name' => $name

        ]);

        Session::flash('success', 'Role created successfully.');

        Session::flash('status-role', $role->id);
        
        return redirect()->route('admin.staff.roles');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $role = Role::where('slug', '!=', 'administrator')->where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->where('id', $id)->firstOrfail();

        return view('admin.roles.edit', compact('role'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $role = Role::where('slug', '!=', 'administrator')->where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->where('id', $id)->firstOrfail();
        
        $validator = Validator::make($request->all(), [

            'name' => 'required'

        ]);

        if($validator->fails()) {
        
            if($validator->errors()->first('name')) {
        
                Session::flash('error', 'Please enter name of role.');
        
                return redirect()->back();
        
            }
        
        }

        $name = $request->input('name');
        
        $slug = Str::slug($name);

        if(Role::where('id', '!=', $id)->where('slug', $slug)->first()) {
        
            Session::flash('error', 'Role already exists. Please try again with different information.');
    
            return redirect()->back();

        }

        $role->name = $name;

        $role->slug = $slug;

        $role->save();

        Session::flash('success', 'Role updated successfully.');

        Session::flash('status-role', $role->id);
        
        return redirect()->route('admin.staff.roles');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $role = Role::where('slug', '!=', 'administrator')->where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->where('id', $id)->firstOrfail();

        $roles = Role::where('slug', '!=', 'administrator')->where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->get();

        if($roles->count() > 1) {

            $role->delete();

            Session::flash('success', 'Role deleted successfully.');

        } else {

            Session::flash('error', 'Atleast one role is required.');

        }
        
        return redirect()->route('admin.staff.roles');

    }
}
