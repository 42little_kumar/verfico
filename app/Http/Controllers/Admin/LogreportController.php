<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\WorkLog;
use App\Payroll;
use App\Project;
use App\Strike;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use QrCode;
use Maatwebsite\Excel\Facades\Excel;


class LogreportController extends Controller
{

    public function exportStrike( $userId )
    {


         if( \DB::table('safety_strikes')->where('worker_id', $userId)->exists() ): 

            $strikes = \App\Strike::where('worker_id', $userId)->get(); 

            $csvExporter = new \Laracsv\Export();

        $csvExporter->beforeEach(function ($strikes) {

            /* Job */

            $job = 'N/A';

            if( empty( $strikes->name ) ):

            $job = 'N/A';

          else:

            if( is_numeric( $strikes->name ) ):

              if( \App\Project::where('id', $strikes->name)->exists() ):

                $job = \App\Project::where('id', $strikes->name)->first()->job_name;

              else:

              $job = $strikes->name. 'Invalid Project Id';

              endif;

            else:

              $job = $strikes->name;

            endif;

          endif;

          /* Job */

          /* Worker */

          $worker = 'N/A';

          if( \DB::table('users')->where('id', $strikes->worker_id)->exists() ):

            $worker = \DB::table('users')->where('id', $strikes->worker_id)->first()->name;

          else:

            $worker = 'N/A';

          endif;

          /* Worker */

          /* Category */

          $category = 'N/A';

          if( !empty( $strikes->category ) ):
                                                             
                $category = $strikes->category;
             
              else:

              $category = 'N/A';
              
        endif;

          /* Category */

          /* Issued by */

          $issuedBy = 'N/A';

                                  if(\DB::Table('users')->where('id', $strikes->created_by)->exists())
                                  {
                                    $issuedBy = \DB::Table('users')->where('id', $strikes->created_by)->first()->name;
                                  }  

          /* Issued by */

  $strikes->date = date("m-d-Y", strtotime($strikes->strike_date));
  $strikes->job = $job;
  $strikes->worker = $worker;
  $strikes->category = $category;
  $strikes->issuedby = $issuedBy;
    

});

        $csvExporter->build($strikes, ['date'=>'Strike Date', 'job'=>'Job', 'worker'=> 'Worker', 'category'=> 'Category', 'description' => 'Violation Description', 'issuedby' => 'Issued By'])->download();

    endif;

    }
    
	/*
	 * @Function Name
	 *
	 *
	 */
	public function logreports( Request $request )
	{

		$subcontractors = User::role('subcontractor')->get();
        $projects = DB::table('project')->where('deleted_at', null)->where('status', 'active')->get();


        $data = compact(

            'subcontractors',
            'projects'

        );

        return view( 'admin.reports.select', $data );

	}

    /*
     * @Function
     *
     *
     */
    public function generate( Request $request )
    {

        $subcontractors = User::role('subcontractor')->get();
        $projects = DB::table('project')->where('deleted_at', null)->where('status', 'active')->get();

        $subid = @$request->sub_id;
        $jobid = @$request->job_id;
        $started_at = @$request->started_at;

        $data = compact(

            'subcontractors',
            'subid',
            'projects',
            'started_at',
            'jobid'

        );

        if( $request->reporttype == 'sub' )
        {
            return view( 'admin.reports.sub', $data );
        }
        
        return view( 'admin.reports.job', $data );

    }

    /*
     * @Datatable get
     *
     *
     */
    public function getReport( Request $request )
    {

        $payrollObject = Project::orderBy('id', 'desc')->where('status', 'active');

        if( Auth::user()->hasRole('Payroll') )
        {
            $payrollObject->where( 'created_by', Auth::user()->id );
        }

        if( isset( $request->subContractorId ) )
        {

            //$payrollObject->where( 'author', $request->subContractorId );

        }

        if( isset( $request->payroll_date ) )
        {
                
            //list( $startDate, $endDate ) = explode( '-', $request->payroll_date );

            /*$from = Carbon::parse( $startDate )
                             ->startOfDay()
                             ->toDateTimeString();

            $to = Carbon::parse( $endDate )
                             ->endOfDay()
                             ->toDateTimeString();

            $payrollObject->whereBetween('created_at', [$from, $to])->get();

            */

            $payrollObject->where('title', $request->payroll_date);


        }
        
        $payrolls = $payrollObject->get();

        
        

        $dt =  Datatables::of( $payrolls );
        $dt->addColumn( 'job_name', function( $project )
        {

            
               return $project->job_name;

        });
        
        foreach( \DB::table('worker_types')->get() as $g ):


        $dt->addColumn( strtolower(str_replace(' ', '', $g->type)), function( $project ) use ($g, $request)
        {

            $workerObject = User::where('worker_type', $g->id)->where('status', 2);

            /* Subcontractor  */

            if( isset( $request->subContractorId ) )
            {

                $workerObject->where('parent', $request->subContractorId);

            }

            /* Subcontractor */

            

            $users = $workerObject->pluck('id');

            $allLogFetch = Worklog::whereIn('worker_id', $users)->where('project_id', $project->id);

            /* Date */

            if( isset( $request->log_date ) )
            {
                    
                list( $startDate, $endDate ) = explode( '-', $request->log_date );

                /* Format date */

                $from = Carbon::parse( $startDate )
                                 ->startOfDay()
                                 ->toDateTimeString();

                $to = Carbon::parse( $endDate )
                                 ->endOfDay()
                                 ->toDateTimeString();

                /* Format date */

                $allLogFetch->whereBetween('punch_at', [$from, $to])->get();

            }

            /* Date */

            $allLogs = $allLogFetch->get();

            //$totalTime = 0;
            //$finalTime = '';

            $finalTime = '';
            $totaltime2 = 0;

            $sum = strtotime('00:00');

            foreach( $allLogs as $logs )
            {

              

                    $logs->punch_in = substr_replace($logs->punch_in, '00', -2);

                    $logs->punch_out = substr_replace($logs->punch_out, '00', -2);

                    $start = date("Y-m-d ".$logs->punch_in);
                    
                    if(strtotime($logs->punch_out) < strtotime($logs->punch_in)){
                        
                        $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logs->punch_out) ) + 24*60*60) ;
                    } else {
                        $end = date("Y-m-d ".$logs->punch_out);
                    }

                    $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

                    $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

                    //$finalTime .= ','.$totalTime;

                    
                    // Converting the time into seconds 
    $timeinsec = strtotime($totalTime) - $sum; 
      
    // Sum the time with previous value 
    $totaltime2 = $totaltime2 + $timeinsec; 
               

                //$finalTime = date("H:i", strtotime($totalTime)  + strtotime($finalTime)) ;

            }

            $h = intval($totaltime2 / 3600); 
  
$totaltime2 = $totaltime2 - ($h * 3600); 
  
// Minutes is obtained by dividing 
// remaining total time with 60 
$m = intval($totaltime2 / 60); 
  


            return "$h:$m";

        });

        endforeach;

        $return = $dt->make( true );

        return $return;
    }

    /*
     * @Datatable get
     *
     *
     */
     function getJobReport( Request $request )
    {

        $payrollObject = User::role('subcontractor')->where('status', 2);

        if( Auth::user()->hasRole('Payroll') )
        {
            $payrollObject->where( 'created_by', Auth::user()->id );
        }

        if( isset( $request->subContractorId ) )
        {

            //$payrollObject->where( 'author', $request->subContractorId );

        }

        if( isset( $request->payroll_date ) )
        {
                
            //list( $startDate, $endDate ) = explode( '-', $request->payroll_date );

            /*$from = Carbon::parse( $startDate )
                             ->startOfDay()
                             ->toDateTimeString();

            $to = Carbon::parse( $endDate )
                             ->endOfDay()
                             ->toDateTimeString();

            $payrollObject->whereBetween('created_at', [$from, $to])->get();

            */

            $payrollObject->where('title', $request->payroll_date);


        }
        
        $payrolls = $payrollObject->get();

        
        

        $dt =  Datatables::of( $payrolls );
        $dt->addColumn( 'sub_name', function( $project )
        {

            
               return $project->company_name;

        });
        
        foreach( \DB::table('worker_types')->get() as $g ):


        $dt->addColumn( strtolower(str_replace(' ', '', $g->type)), function( $project ) use ($g, $request)
        {

            $workerObject = User::where('worker_type', $g->id)->where('parent', $project->id)->where('status', 2);

            

            

            $users = $workerObject->pluck('id');

            $allLogFetch = Worklog::whereIn('worker_id', $users)->where('project_id', $request->jobid);

          

            /* Date */

            if( isset( $request->log_date ) )
            {
                    
                list( $startDate, $endDate ) = explode( '-', $request->log_date );

                /* Format date */

                $from = Carbon::parse( $startDate )
                                 ->startOfDay()
                                 ->toDateTimeString();

                $to = Carbon::parse( $endDate )
                                 ->endOfDay()
                                 ->toDateTimeString();

                /* Format date */

                $allLogFetch->whereBetween('punch_at', [$from, $to])->get();

            }

            /* Date */

            $allLogs = $allLogFetch->get();

            //$totalTime = 0;
            //$finalTime = '';

            $finalTime = '';
            $totaltime2 = 0;

            $sum = strtotime('00:00');

            foreach( $allLogs as $logs )
            {

              

                    $logs->punch_in = substr_replace($logs->punch_in, '00', -2);

                    $logs->punch_out = substr_replace($logs->punch_out, '00', -2);

                    $start = date("Y-m-d ".$logs->punch_in);
                    
                    if(strtotime($logs->punch_out) < strtotime($logs->punch_in)){
                        
                        $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logs->punch_out) ) + 24*60*60) ;
                    } else {
                        $end = date("Y-m-d ".$logs->punch_out);
                    }

                    $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

                    $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

                    //$finalTime .= ','.$totalTime;

                    
                    // Converting the time into seconds 
    $timeinsec = strtotime($totalTime) - $sum; 
      
    // Sum the time with previous value 
    $totaltime2 = $totaltime2 + $timeinsec; 
               

                //$finalTime = date("H:i", strtotime($totalTime)  + strtotime($finalTime)) ;

            }

            $h = intval($totaltime2 / 3600); 
  
$totaltime2 = $totaltime2 - ($h * 3600); 
  
// Minutes is obtained by dividing 
// remaining total time with 60 
$m = intval($totaltime2 / 60); 
  
            
          
            return "$h:$m";

        });

        endforeach;

        $dt->addColumn( 'total', function( $project ) use ( $request)
        {

            $ss = 1;
            foreach( \DB::table('worker_types')->get() as $g ):

            $workerObject = User::where('worker_type', $g->id)->where('parent', $project->id)->where('status', 2);

            $users = $workerObject->pluck('id');

            $allLogFetch = Worklog::whereIn('worker_id', $users)->where('project_id', $request->jobid);

          
            /* Date */

            if( isset( $request->log_date ) )
            {
                    
                list( $startDate, $endDate ) = explode( '-', $request->log_date );

                /* Format date */

                $from = Carbon::parse( $startDate )
                                 ->startOfDay()
                                 ->toDateTimeString();

                $to = Carbon::parse( $endDate )
                                 ->endOfDay()
                                 ->toDateTimeString();

                /* Format date */

                $allLogFetch->whereBetween('punch_at', [$from, $to])->get();

            }

            /* Date */

            $allLogs = $allLogFetch->get();

            //$totalTime = 0;
            //$finalTime = '';

            $finalTime = '';
            $totaltime2 = 0;

            $sum = strtotime('00:00');

            foreach( $allLogs as $logs )
            {

              

                    $logs->punch_in = substr_replace($logs->punch_in, '00', -2);

                    $logs->punch_out = substr_replace($logs->punch_out, '00', -2);

                    $start = date("Y-m-d ".$logs->punch_in);
                    
                    if(strtotime($logs->punch_out) < strtotime($logs->punch_in)){
                        
                        $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logs->punch_out) ) + 24*60*60) ;
                    } else {
                        $end = date("Y-m-d ".$logs->punch_out);
                    }

                    $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

                    $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

                    //$finalTime .= ','.$totalTime;

                    
                    // Converting the time into seconds 
    $timeinsec = strtotime($totalTime) - $sum; 
      
    // Sum the time with previous value 
    $totaltime2 = $totaltime2 + $timeinsec; 
               

                //$finalTime = date("H:i", strtotime($totalTime)  + strtotime($finalTime)) ;

            }

            $h = intval($totaltime2 / 3600); 
  
$totaltime2 = $totaltime2 - ($h * 3600); 
  
// Minutes is obtained by dividing 
// remaining total time with 60 
$m = intval($totaltime2 / 60); 
  
            
          
        $timarray[] = "$h:$m";

       
        endforeach;

        return  $this->AddPlayTime($timarray);;

        });

        $return = $dt->make( true );

        return $return;
    }

    public function AddPlayTime($times) {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }
    
        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;
    
        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }

    /*
     * @Function
     *
     *
     */
    public function strikeDelete($id)
    {

        Strike::find( $id )->delete();

        return redirect()->back();

    }

    /*
     * @Function
     *
     *
     */
    public function edit( $id, $workerId )
    {

        $strikeData = Strike::find( $id );

        $data = compact(

            'strikeData',
            'workerId'
      

        );

        return view('admin.strike.edit', $data);

    }

    /*
     * @Function
     *
     *
     */
    public function update( $id, Request $request )
    {

        $strikeObject = Strike::find( $id );

        $strikeData = [];

        $strikeData['description'] = $request->description;
        $strikeData['category'] = $request->category;
        $strikeData['name'] = $request->project;

        $strikeObject->update( $strikeData );

        Session::flash('success', 'Strike data updated successfully.');

        return redirect()->back();

    }



}



function getHours( $projectId, $typeId )
{

    $users = User::where('worker_type', $typeId)->where('status', 2)->lists('id')->toArray();


    $allLogs = Worklog::whereIn('worker_id', $users)->get();

    foreach( $allLogs as $logs )
    {

            $logs->punch_in = substr_replace($logs->punch_in, '00', -2);
            $logs->punch_out = substr_replace($logs->punch_out, '00', -2);

            $start = date("Y-m-d ".$logs->punch_in);
            
            if(strtotime($logs->punch_out) < strtotime($logs->punch_in)){
                
                $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logs->punch_out) ) + 24*60*60) ;
            } else {
                $end = date("Y-m-d ".$logs->punch_out);
            }

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

            $totalTime =+ $totalTime;

    }

    return $totalTime;

            


}






