<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\Project;
use App\WorkerDocuments;
use App\Document;
use App\Strike;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use Mail;
use App\Mail\ApproveWorker;
use App\Mail\ConfirmRegistrationMobileWorker;
use App\Mail\ConfirmRegistrationWorker;
use App\WorkerType;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        if( !Auth::user()->hasRole('Superadmin') && isset( $_REQUEST['userType'] ) && ( $_REQUEST['userType'] == 'admin' || $_REQUEST['userType'] == 'superadmin' ) )
        {

          return abort(404);

        }

        $userType = '';

        $userTitle = '';

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

        }

        switch ( $request->userType ){
            case "subcontractor":
                $userTitle = 'Subcontractors';
                break;
            case "foreman":
                $userTitle = 'Foremen';
                break;
            case 'worker':
                $userTitle = 'Workers';
                break;
            case 'payroll':
                $userTitle = 'Payroll';
                break;
            case 'superint':
                $userTitle = 'Superintendents';
                break;
            case 'safetyo':
                $userTitle = 'Safety Officers';
                break;
            default:
                $userTitle = 'Admins';
        }
     
        //$users = User::role('user');

        $users = User::paginate( 20 );

        /* Worker Types */

        $titlesList = WorkerType::orderBy('type', 'asc')->get();

        /* Worker Types */

        /* Companies */

        $companiesList = User::role('subcontractor')->orderBy('company_name', 'asc')->get();

        /* Companies */

       return view('admin.users.index', compact( 'users', 'userType', 'userTitle', 'titlesList', 'companiesList' ));
        
    }

    public function downloadExportWorkers( Request $request )
    {
      $userObject = User::orderBy( 'name', 'ASC' );

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

            $userObject->role( $userType );
            
        }

        // Status filter

        if( isset( $request->status ) )
        {

            $userObject->where( 'status', $request->status );

        }

        // Company filter

        if( isset( $request->company ) )
        {

            $userObject->where( 'parent', $request->company );

        }

        // Title filter

        if( isset( $request->title ) )
        {

            $userObject->where( 'worker_type', $request->title );

        }

        $users = $userObject
        ->where( 'status', '!=', 1 )
        ->get();

        $csvExporter = new \Laracsv\Export();

        $csvExporter->beforeEach(function ($user) {

    //$user->ssn_id = substr($user->ssn_id, -4);
    $user->ssn_id = $user->ssn_id;

    if( !empty( $user->training ) )
    $user->training = date('m/d/Y', strtotime( $user->training ));

    $user->last_worked = '';

    $lastWorked = DB::table('worker_log')->where('worker_id', $user->id)->orderBy('id', 'desc')->first();

    if( $lastWorked )
    { 
      $lastWorked = $lastWorked->punch_at;

      $user->last_worked = date('m/d/Y', strtotime( $lastWorked ));
    }

      $user->total_strikes = '';
      $user->total_strikes_30 = '';
      $user->last_strike_date = '';

      //

      if( Strike::where( 'worker_id', $user->id )->exists() )
        $user->total_strikes = Strike::where( 'worker_id', $user->id )->count();

      //

      if( Strike::where( 'worker_id', $user->id )->exists() ) 
        $user->total_strikes_30 = Strike::where( 'worker_id', $user->id )->whereDate('created_at', '>', Carbon::now()->subDays(30))->count();

      //


        if( $user->status == 0 )
        {
            $user->status = "Blocked";
        }else
        {
            $user->status = "Active";
        }
        

      if( Strike::where( 'worker_id', $user->id )->exists() )
        $user->last_strike_date = date('m/d/Y', strtotime( Strike::where( 'worker_id', $user->id )->orderBy('id', 'desc')->first()->strike_date ) );

});

        $csvExporter->build($users, ['name' => 'Name', 'email' => 'Email', 'phone_number' => 'Phone', 'sub_contractor_name' => 'Company', 'ssn_id' => 'SSN ID', 'worker_type_name' => 'Title', 'training' => 'Rock Spring Safety Training received on', 'last_worked' => 'Last Worked', 'total_strikes' => 'Total Strikes', 'total_strikes_30' => 'Total 30 Day Strikes', 'last_strike_date' => 'Last Strike Date', 'status' => 'Status'])->download();

    }

    /*
     * @Datatable get
     *
     *
     */
    public function changeStatusWorker( Request $request )
    {

        $workerDocObject = WorkerDocuments::find( $request->id );
        $workerDocObject->show = $request->showVal;
        $workerDocObject->save();
    }

    /*
     * @Datatable get
     *
     *
     */
    public function getUsers( Request $request )
    {
      $allusers = \DB::table('users')->get();




        $userType = '';

        $userObject = User::orderBy( 'id', 'DESC' );

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

            $userObject->role( $userType );
            
        }

        // Status filter

        if( isset( $request->status ) )
        {

            $userObject->where( 'status', $request->status );

        }

        // Company filter

        if( isset( $request->company ) )
        {

            $userObject->where( 'parent', $request->company );

        }

        // Title filter

        if( isset( $request->title ) )
        {

            $userObject->where( 'worker_type', $request->title );

        }

        $users = $userObject
        ->where( 'status', '!=', 1 )
//->select('id','name','email','company_name','ssn_id','created_at','phone_number','status')
        ->get();

        return Datatables::of($users)
        ->addColumn('id', function($user)
        {

          return $user->id;

        })
        ->addColumn('name', function($user)
        {

          if( $user->hasRole('Subcontractor') )
            return $user->company_name;

          return $user->name;

          $viewLink = route('admin.users.view', [ 'id' => $user->id ]);

          $userName = '<div class="worker_name_td lc-id"><a class="index-link" href="'.$viewLink.'"><p>'.$user->first_name.' '.$user->last_name.'</p></a></div>';

          if( $user->hasRole('Subcontractor') )
            $userName = '<div class="worker_name_td lc-id"><a class="index-link" href="'.$viewLink.'"><p>'.$user->company_name.'</p></a></div>';

          return '<div class="worker_name_td lc-id"><a class="index-link" href="'.$viewLink.'"><p>'.$user->name.'</p></a></div>';

        })
        ->addColumn('email', function($user)
        {
            
            $hiddenid = "<input type='hidden' value='".$user->id."' name='hiddenid'>";

           return $user->email == null ? $hiddenid.'N/A' : $hiddenid.$user->email;

        })
        ->addColumn('company', function($user)
        {
            
            return $user->sub_contractor_name;

        })
        ->addColumn('ssn', function($user)
        {
            
            return $user->ssn_id;

        })
        ->addColumn('phone_number', function($user)
        {
            
            return !empty( $user->phone_number ) ? $user->phone_number : '';

        })
        ->addColumn('created_at', function($user)
        {
            
            return \Carbon\Carbon::parse($user->created_at)->tz(str_before(auth()->user()->timezone_manual, ','))->format(("m-d-Y"));

        })
        ->addColumn('worker_type', function($user)
        {
            
           return empty( $user->worker_type_name ) ? '<i>Not Selected</i>' : ucwords( $user->worker_type_name );

        })
        ->addColumn('status', function($user)
        {

            switch ( $user->status ) {
                case 0:
                    $html = '<span class="status_active action-block">Blocked</span>';
                    break;
                case 1:
                    $html = '<span class="status_active action-pending">Pending</span>';
                    break;
                default:
                    $html = '<span class="status_active action-active">Active</span>';
                    break;
            }
            
            return $html;
            
        })
        ->addColumn('action', function($user)
        {

            $viewLink = route('admin.users.view', [ 'id' => $user->id ]);

            $editLink = route('admin.users.edit', [ 'id' => $user->id ]);

            $deleteLink = route('admin.users.delete', [ 'id' => $user->id ]);

            $deleteHtml = '';

            if( !$user->hasRole('Subcontractor') && $user->id != 1 )
            {

              $deleteHtml = '<hr>
                                   <div>
                                   <a href="' . $deleteLink . '" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
                                      Delete
                                    </a>
                                  </div>';

            }

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle lc-id" title="View">
    
                                           View
    
                                      </a>
                                  </div> 

                                  <div>
                                     <a href="' . $editLink . '" class="btn btn-sm btn-primary btn-circle lc-id" title="Edit">
                                       Edit
                                    </a>
                                  </div>

                                   '.$deleteHtml.'
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['id', 'name', 'worker_type', 'email', 'status', 'action'] )
        ->make( true );
    }

    /*
     * @Datatable get
     *
     *
     */
    public function workerExport( Request $request, $subId = null )
    {

        if( $subId == null )
        {

            $allWorkers = User::role('worker')
            ->where( 'status', 2 )
            ->get();

            $csvExporter = new \Laracsv\Export();

            $csvExporter->build($allWorkers, ['first_name', 'last_name', 'email', 'created_at'])->download();

        }
        else
        {

            $allWorkers = User::role('worker')
            ->where( 'status', 2 )
            ->where( 'parent', $subId )
            ->get();

            $csvExporter = new \Laracsv\Export();

            $csvExporter->build($allWorkers, ['first_name', 'last_name', 'email', 'created_at'])->download();

        }

    }

     /*
     * @Datatable get
     *
     *
     */
    public function getRequests( Request $request )
    {

        $userType = '';

        $userObject = User::orderBy( 'id', 'DESC' )->where('status', 1);

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

            $userObject->role( $userType );
            
        }

        // Status filter

        if( isset( $request->status ) )
        {

            $userObject->where( 'status', $request->status );

        }

        // Company filter

        if( isset( $request->company ) )
        {

            $userObject->where( 'parent', $request->company );

        }

        // Title filter

        if( isset( $request->title ) )
        {

            $userObject->where( 'worker_type', $request->title );

        }

        $users = $userObject->get();

        return Datatables::of($users)
        ->addColumn('name', function($user)
        {

          $viewLink = route('admin.users.view', [ 'id' => $user->id ]);

          $userName = '<div class="worker_name_td"><a class="index-link" href="'.$viewLink.'?request=1"><p>'.$user->first_name.' '.$user->last_name.'</p></a><p>'.$user->email.'</p></div>';

          return $userName;

        })
        ->addColumn('email', function($user)
        {
            
           

        })
        ->addColumn('company', function($user)
        {
            
            return $user->sub_contractor_name;

        })
        ->addColumn('created_at', function($user)
        {
            
            return \Carbon\Carbon::parse($user->created_at)->tz(str_before(auth()->user()->timezone_manual, ','))->format(("m-d-Y"));

        })
        ->addColumn('worker_type', function($user)
        {
            
           return empty( $user->worker_type_name ) ? '<i>Not Selected</i>' : ucwords( $user->worker_type_name );

        })
        ->addColumn('status', function($user)
        {

            switch ( $user->status ) {
                case 0:
                    $html = '<span class="status_active">Blocked</span>';
                    break;
                case 1:
                    $html = '<span class="status_active">Pending</span>';
                    break;
                default:
                    $html = '<span class="status_active">Active</span>';
                    break;
            }
            
            return $html;
            
        })
        ->addColumn('action', function($user)
        {

            $approveLink = route('admin.users.approve', [ 'id' => $user->id ]);

            $rejectLink = route('admin.users.reject', [ 'id' => $user->id ]);

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $approveLink . '" class="btn btn-sm btn-info btn-circle btn-action-approve" title="Approve">
    
                                           Approve
    
                                      </a>
                                  </div> 

                                  <div>
                                     <a href="' . $rejectLink . '" class="btn btn-sm btn-info btn-circle btn-action-reject" title="Reject">
    
                                           Reject
    
                                      </a>
                                  </div> 
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['name', 'worker_type', 'company', 'email', 'status', 'action'] )
        ->make( true );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function workerRequests( Request $request )
    {

        $userType = 'worker';

        $userTitle = 'Worker';

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

        }
     
        //$users = User::role('user');

        $users = User::paginate( 20 );

        /* Worker Types */

        $titlesList = WorkerType::get();

        /* Worker Types */

        /* Companies */

        $companiesList = User::role('subcontractor')->get();

        /* Companies */

       return view('admin.users.requests', compact( 'users', 'userType', 'userTitle', 'titlesList', 'companiesList' ));

    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Request $request )
    {

        if( !Auth::user()->hasRole('Superadmin') && isset( $_REQUEST['userType'] ) && ( $_REQUEST['userType'] == 'admin' || $_REQUEST['userType'] == 'superadmin' ) )
        {

          return abort(404);

        }



        $fieldPrefix = '';

        $subContractors = User::role('subcontractor')->get();

        $workerTypes = WorkerType::get();

        $userType = $request->userType;

        if( $userType == 'subcontractor' )
        {

            $fieldPrefix = 'Contact ';

        }

        $data = compact(

            'subContractors',

            'userType',

            'workerTypes',

            'fieldPrefix'

        );
        
        return view( 'admin.users.create', $data );

    }

    /**
     * Save worker documents.
     *
     * 
     * 
     */
    public function saveWorkerDocuments( $request, $workerId )
    {

        $workerDocument = new WorkerDocuments;

        if( $request->notes_training ):

        foreach( $request->notes_training as $key => $value)
        {

            $documentName = null;

            $isSave = false;

            if( $request->hasFile( 'worker_document' ) || !empty($value) ) {

                $isSave = true;

            }

            $workerDocument = new WorkerDocuments;

            if( @$request->hasFile( 'worker_document' ) ) {

                /* Upload */

                try {

                    $destination = public_path('uploads/documents/'. $workerId);

                    if(!File::isDirectory($destination)) {

                        mkdir($destination, 0777, true);

                        chmod($destination, 0777);

                    }

                    $filen = substr($request->worker_document[$key]->getClientOriginalName(), 0, -4);

            $documentName = $filen.'.'.$request->worker_document[$key]->getClientOriginalExtension();


                    //$documentName = 'document-' . time() . '-' . Str::random(15) . '.' . $request->worker_document[$key]->getClientOriginalExtension();

                    $request->worker_document[$key]->move($destination, $documentName);

                } catch(\Exception $e) {

                    $documentName = null;

                }

            } else {

                $documentName = null;

            }

            /* Upload */

            if( $isSave ):

            $workerDocument->user_id = $workerId;
            $workerDocument->author = Auth::user()->id;
            $workerDocument->notes = $value;
            $workerDocument->document = $documentName;
            $workerDocument->show = ( isset( $request->doc_upload[$key] ) ) ? $request->doc_upload[$key] : 0;

            $workerDocument->save();

            endif;

            

        }

        endif;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parent = 0;

        $validator = Validator::make($request->all(), [

            'first-name' => 'required',

            'password' => 'required',

        ]);


        if($validator->fails()) {

            $request->flash();
        
            if($validator->errors()->first('first-name')) {
        
                Session::flash('error', 'Please enter first name of user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('email')) {
        
                Session::flash('error', 'Email already exists is system.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('phone-number')) {
        
                Session::flash('error', 'Phone number already exists is system.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('password')) {
        
                Session::flash('error', 'Please enter password for the user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('username')) {
        
                Session::flash('error', 'Please enter username of user.');
        
                return redirect()->back();
        
            }
        
        }

        $additional_validator = Validator::make($request->all(), [

            'password' => [ 'min:6', 'max:18' ]

        ]);

        if($additional_validator->fails()) {

            $request->flash();

            if($additional_validator->errors()->first('email')) {
        
                Session::flash('error', 'Please enter valid email address of user.');
        
                return redirect()->back();
        
            }

            if($additional_validator->errors()->first('password')) {
        
                Session::flash('error', 'Password should be of atleast 6 characters and not more than 18 characters.');
        
                return redirect()->back();
        
            }
        
        }

        $userStatus = 2;

        $companyName = NULL;

        $first_name = $request->input('first-name');

        $last_name = $request->input('last-name');

        $worker_type = $request->input('worker_type');

        $email = strtolower($request->input('email'));

        $username = rand();

        $password = Hash::make($request->input('password'));

        $country_code = null;

        $phone_number = null;

        $training = null;

        if( $request->filled('company-name') )
        {

            $companyName = $request->input('company-name');

        }

        if( $request->filled('training') )
        {

            $training = $request->input('training');

            $training = date('Y-m-d H:i:s', strtotime($training));

        }

        if($request->filled('phone-number')) {

            $country_code = null;

            $phone_number = preg_replace('/[^A-Za-z0-9]/', '', $request->input('phone-number'));

        }

        $gender = $request->input('gender');

        /* If email and phone number not empty then validate them */

        if( !empty( $email ) )
        {

          if(User::where('email', $email)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Email already linked with another user. Please try again with different information.');

            return redirect()->back();

          }

        }

        if( !empty( $phone_number ) )
        {

          if(User::where('phone_number', $phone_number)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Phone number already linked with another user. Please try again with different information.');

            return redirect()->back();

          }

        }

        /* If email and phone number not empty then validate them */

        if($username && User::where('username', $username)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Username already linked with another user. Please try again with different information.');

            return redirect()->back();

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        if( isset( $request->general_contractor ) && !empty( $request->general_contractor ) )
        {

            if( isset( $request->user_role ) && ( $request->user_role == 'worker'  || $request->user_role == 'safetyo' ) )
            {
                
                $userRole = $request->input('user_role');

                $parent = $request->general_contractor;

            }

            

        }

        if( isset( $request->user_role ) && ( $request->user_role == 'foreman' ) )
            {
                
                $userRole = $request->input('user_role');

                $parent = '624';

            }


        /*-------------------------------------------- Seperator --------------------------------------------*/

        $emailUserData = [];

        $emailUserData['first_name'] = $request->input('first-name');
        $emailUserData['name'] = 'Name: '.$request->input('first-name').' '.$request->input('last-name').'<br>';
        $emailUserData['email'] = $request->input('email');
        $emailUserData['phone_number'] = '';
        $emailUserData['user_role'] = $request->input('user_role');

        $workerCompanyfieldForEmail = '';

        if( $request->input('user_role') == 'subcontractor')
        {

          $emailUserData['name'] = 'Company Name: '.$request->input('company-name').'<br>';

        }

        if( $request->input('user_role') == 'worker')
        {

          $workerCompanyfieldForEmail = 'Company: '.getCompanyNameFromId($request->input('general_contractor'));

          $workerCompanyfieldForEmail = '<br>'.$workerCompanyfieldForEmail.'<br><br>';

        }

        if( !empty( $phone_number ) )
        {

          $emailUserData['phone_number'] = 'Phone number: '.$phone_number.'<br>';

        }

        //
        if(!empty($email))
        {
          
        Mail::to($email)->send(new ConfirmRegistrationWorker((object)$emailUserData, 'Mail', $request->input('password'), $workerCompanyfieldForEmail));
        }

        $finalSsd = ( $request->input('ssn_id') ) ? strtoupper( $request->input('ssn_id_pre').$request->input('ssn_id') ) : null;

        $lastNameSsn = explode(' ', $last_name);

        $finalSsd .= strtoupper(substr(end($lastNameSsn), 0, 4));

        $user = User::create([

            'username' => $username,

            'first_name' => $first_name,

            'last_name' => $last_name,

            'training' => $training,

            'name' => $first_name.' '.$last_name,

            'payroll_first_name' => $request->payroll_first_name,

            'payroll_last_name' => $request->payroll_last_name,

            'payroll_email' => $request->payroll_email,

            'payroll_phone' => $request->payroll_phone,

            'worker_type' => $worker_type,

            'ssn_id' => $finalSsd,

            'parent' => $parent,

            'email' => $email,

            'company_name' => $companyName,

            'password' => $password
            
        ]);

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $userRole = 'worker';
    
        if( isset( $request->user_role ) )
        {
            
            $userRole = $request->input('user_role');

        }
       
        $user->assignRole( $userRole );

        if( $userRole == 'worker' )
        {
            $userStatus = 2;
        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $user->phone_number = $phone_number;
        
        $user->country_code = $country_code;

        $user->gender = $gender;

        $user->status = $userStatus;

        $user->email_verified_at = Carbon::now();

        $user->phone_number_verified_at = $phone_number ? Carbon::now() : null;

        if($request->hasFile('image')) {

            $format_validator = Validator::make($request->all(), [

                'image' => 'mimes:jpeg,png,jpg,gif,svg'
    
            ]);
    
            if($format_validator->fails()) {

                $request->flash();
    
                Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');
    
                return redirect()->back();
    
            }

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('image');

      $image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);
            
            $user->image = $image_name;

        }


        $this->saveWorkerDocuments( $request, $user->id );

        /* Document upload */

        $subContId = $user->id;

        if( $request->hasFile('document') ) {

          foreach ( $request->document as $document) {
            
            $destination = public_path('uploads/documents/'. $subContId);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $documentName = 'document-' . time() . '-' . Str::random(15) . '.' . $document->getClientOriginalExtension();

            $document->move($destination, $documentName);
            
            $saveData['user_id'] = $subContId;
            $saveData['document'] = $documentName;

            Document::create( $saveData );

          }

        }

        /* Document upload */

        if($request->input('cropped-image')){

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $request->input('cropped-image-type');

            $imageInfo = $request->input('cropped-image'); 

            $image = fopen($imageInfo, 'r');
            
            file_put_contents($destination.'/'.$image_name, $image);

            fclose($image); 

            $user->image = $image_name;
        }


        $user->save();

        /* Assing projects */

        if( isset( $request->si_project ) )
        {

          foreach( $request->si_project as $sip )
          {
            Project::where('id', $sip)->update( [ 'superint' => $user->id ] );
          }

        }

        /* Assing projects */

        Session::flash('success', 'New user has been created successfully.');

        Session::flash('status-user', $user->id);

        $redirectQuery = 'userType='.$userRole;

        return redirect()->route('admin.users', $redirectQuery);

    }

    public function getWorkerOrderDetails( Request $request )
    {

        $cl = 'id';
        $od = 'desc';
        $st = '>';
        $id = $request->user_id;
        $pm = 'id';

        if( isset( $request->od ) )
        $od = $request->od;

        $worker = User::find( $request->userId );

        if( isset( $request->od ) && $request->od == 'asc' )
        $st = '<';

        if( $request->cl == 0 )
        {
            $cl = 'first_name';
            $pm = $worker->first_name;
        }
        

        if( $request->cl == 2 )
        {
            $cl = 'phone_number';
            $pm = $worker->phone_number;
        }
        

        if( $request->cl == 3 )
        {
            $cl = 'company_name';
            $pm = $worker->company_name;
        }
        

        if( $request->cl == 6 )
        {
            $cl = 'created_at';
            $pm = $worker->created_at;
        }

        $prst = '>';
        $nxtOd = 'desc';

        if( $st == '>' )
        {
            $prst = '<';
            $nxtOd = 'asc';
        }
        
        
   
        $next = User::role('worker')->orderBy($cl, $nxtOd)->where($cl, $st, $pm)->first();

        $previous = User::role('worker')->orderBy($cl, $od)->where($cl, $prst, $pm)->first();

        echo $next->first_name;

        echo '<br>';

        echo $previous->first_name;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $user = User::with('roles')->where('id', $id)->firstOrFail();

        $logs = $user->logs()->latest()->get();

        $roles = json_decode( $user->roles );

        $userRole = @$roles[0]->name;
    
        $previous = null;
    
        $next = null;
        
        if(  isset( $_GET["dir"] ) ):
            
        
    if(@$_GET["from"]=="workers"){
      
      if(@$_GET["column"]==2){
        

         
                    $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('phone_number',@$_GET["dir"])->orderBy('users.first_name','desc');

                    if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                    {

                        $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('phone_number',@$_GET["dir"])->orderBy('users.first_name','desc');

                    }
                    
                    if( @$_GET["status"] != 'null' )
                    {
                        $usersobj->where( 'status', @$_GET["status"] );
                    }

                    if( @$_GET["company"] != 'null' )
                    {
                        $usersobj->where( 'parent', @$_GET["company"] );
                    }

                    if( @$_GET["title"] != 'null' )
                    {

                        $usersobj->where( 'worker_type', @$_GET["title"] );
                    }
                    
                    $users = $usersobj->get();

          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
      }
      if(@$_GET["column"]==0){
        

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"]);
            
                    if( $userRole == 'Subcontractor' )
                    {

                        $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('company_name',@$_GET["dir"]);

                    }

                    if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                    {


                        $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('first_name',@$_GET["dir"]);

                    }
                    
                    
                    if( @$_GET["status"] != 'null' )
                    {
                        $usersobj->where( 'status', @$_GET["status"] );
                    }

                    if( @$_GET["company"] != 'null' )
                    {
                        $usersobj->where( 'parent', @$_GET["company"] );
                    }

                    if( @$_GET["title"] != 'null' )
                    {

                        $usersobj->where( 'worker_type', @$_GET["title"] );
                    }

                    $users = $usersobj->get();
          
          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
            }
            
            if(@$_GET["column"]==6){
        
        
         
                $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('created_at',@$_GET["dir"]);

                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                    {

                        $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('created_at',@$_GET["dir"]);

                    }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }

                $users = $usersobj->get();
                
                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }

        
        if(@$_GET["column"]==3){
        

           $usersobj = User::role($userRole)->where('status', '2')->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"])->where('id', 'parent');



           if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }

            if( @$_GET["company"] != 'null' )
            {
                $usersobj->where( 'parent', @$_GET["company"] );
            }

            if( @$_GET["title"] != 'null' )
            {

                $usersobj->where( 'worker_type', @$_GET["title"] );
            }

            $users = $usersobj->get();
            
            $index = null;
            $i=0;
            foreach($users as $user){
                if($user->id == $id){
                    $index = $i;
                    break;
                }
                $i++;
            }
            if(isset($users[$i+1])){
                $next = $users[$i+1];
            }
            if(isset($users[$i-1])){
                $previous = $users[$i-1];
            }
            
            
        
    }

        }
        
        endif;
    
    //print_r($previous->id);
    //print_r($next->id);
    //die;

        return view( 'admin.users.view', compact( 'user', 'logs', 'userRole','previous','next' ) );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if( !Auth::user()->hasRole('Superadmin') && isset( $_REQUEST['userType'] ) && ( $_REQUEST['userType'] == 'admin' || $_REQUEST['userType'] == 'superadmin' ) )
        {

          return abort(404);

        }

        $fieldPrefix = '';

        $subContractors = User::role('subcontractor')->get();

        $workerTypes = WorkerType::get();
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::with('roles')->where('id', $id)->firstOrFail();

        if( $user->hasRole('Subcontractor') )
        {

            $fieldPrefix = 'Contact ';

        }

        $roles = json_decode( $user->roles );

        $userRole = $roles[0]->name;
        
        //$countries = CountryCode::orderBy('name', 'ASC')->get();

        if( isset( $_GET["dir"] ) ):
    
            if(@$_GET["column"]==2){
        

         
                $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('phone_number',@$_GET["dir"]);

                if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                {

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('phone_number',@$_GET["dir"]);

                }
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }
                
                $users = $usersobj->get();

                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        if(@$_GET["column"]==0){
            
            
             
                $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"]);

                if( $userRole == 'Subcontractor' )
                {

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('company_name',@$_GET["dir"]);

                }

                if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
                {

                    $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('first_name',@$_GET["dir"]);

                }
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }

                $users = $usersobj->get();
                
                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        
        if(@$_GET["column"]==6){
            
            
             
            $usersobj = User::role($userRole)->whereNull('deleted_at')->orderBy('created_at',@$_GET["dir"]);

            if( isset($_GET["wr"]) || @$_GET["wr"] != '' )
            {

                $usersobj = User::role($userRole)->whereNull('deleted_at')->where('status', 1)->orderBy('created_at',@$_GET["dir"]);

            }

            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }

            if( @$_GET["company"] != 'null' )
            {
                $usersobj->where( 'parent', @$_GET["company"] );
            }

            if( @$_GET["title"] != 'null' )
            {

                $usersobj->where( 'worker_type', @$_GET["title"] );
            }

            $users = $usersobj->get();
            
            $index = null;
            $i=0;
            foreach($users as $user){
                if($user->id == $id){
                    $index = $i;
                    break;
                }
                $i++;
            }
            if(isset($users[$i+1])){
                $next = $users[$i+1];
            }
            if(isset($users[$i-1])){
                $previous = $users[$i-1];
            }
            
            
        
    }

    
    if(@$_GET["column"]==3){
            

        $usersobj = User::role($userRole)->where('status', '2')->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"])->where('id', 'parent');


       if( @$_GET["status"] != 'null' )
        {
            $usersobj->where( 'status', @$_GET["status"] );
        }

        if( @$_GET["company"] != 'null' )
        {
            $usersobj->where( 'parent', @$_GET["company"] );
        }

        if( @$_GET["title"] != 'null' )
        {

            $usersobj->where( 'worker_type', @$_GET["title"] );
        }

        $users = $usersobj->get();
        
        $index = null;
        $i=0;
        foreach($users as $user){
            if($user->id == $id){
                $index = $i;
                break;
            }
            $i++;
        }
        if(isset($users[$i+1])){
            $next = $users[$i+1];
        }
        if(isset($users[$i-1])){
            $previous = $users[$i-1];
        }
        
        
    
}
            
        endif;

        return view( 'admin.users.edit', compact( 'user', 'userRole', 'subContractors', 'workerTypes', 'fieldPrefix', 'previous','next' ) );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $parent = 0;

       // $user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::where('id', $id)->firstOrFail();

        $validator = Validator::make($request->all(), [

            'first-name' => 'required',

        ]);

        if($request->filled('phone-number') || !empty( $request->input('email') ))
        {

          $phone_number = preg_replace('/[^A-Za-z0-9]/', '', $request->input('phone-number'));

          $data['phone-number'] = $phone_number;

          if( !empty( $request->input('email') ) )
          {

              $data['email'] = $request->input('email');

          }

          $validator = Validator::make($data, [

            'phone-number' => 'unique:users,phone_number,'.$id,

            'email' => 'unique:users,email,'.$id,

          ]);

        }

        if($validator->fails()) {
        
            if($validator->errors()->first('first-name')) {
        
                Session::flash('error', 'Please enter first name of user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('phone-number')) {
        
                Session::flash('error', 'Phone number already linked with another user. Please try again with different information.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('email')) {
        
                Session::flash('error', 'Email already linked with another user. Please try again with different information.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('username')) {
        
                Session::flash('error', 'Please enter username of user.');
        
                return redirect()->back();
        
            }
        
        }

        $first_name = $request->input('first-name');

        $email = $request->input('email');

        $last_name = $request->input('last-name');
        
        $worker_type = $request->input('worker_type');

        $training = null;

        if( $request->filled('training') )
        {

            $training = $request->input('training');

            $training = date('Y-m-d H:i:s', strtotime($training));

        }

        if($request->filled('phone-number')) {

            $country_code = null;

            $phone_number = preg_replace('/[^A-Za-z0-9]/', '', $request->input('phone-number'));


        } else {

            $country_code = null;

            $phone_number = null;

        }

        $gender = $request->input('gender');

        /*if($username && User::where('id', '!=', $user->id)->where('username', $username)->withTrashed()->first()) {

            Session::flash('error', 'Username already linked with another user. Please try again with different information.');

            return redirect()->back();

        }*/

        /*-------------------------------------------- Seperator --------------------------------------------*/

        if( isset( $request->general_contractor ) && !empty( $request->general_contractor ) )
        {

            if( isset( $request->user_role ) && ( $request->user_role == 'worker' || $request->user_role == 'safetyo' )   )
            {
                
                $userRole = $request->input('user_role');

                $parent = $request->general_contractor;

            }

        }

        if( isset( $request->user_role ) && ( $request->user_role == 'foreman' ) )
            {
                
                $userRole = $request->input('user_role');

                $parent = '624';

            }

        $finalSsd = ( $request->input('ssn_id') ) ? strtoupper( $request->input('ssn_id_pre').$request->input('ssn_id') ) : null;


        $lastNameSsn = explode(' ', $last_name);

        $finalSsd .= strtoupper(substr(end($lastNameSsn), 0, 4));

     

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $status = $request->input('status');

        //$user->username = $username;
        
        $user->first_name = $first_name;

        $user->email = $email;

        $user->company_name = @$request->input('company-name');
        
        $user->last_name = $last_name;

        $user->training = $training;

        $user->name = $first_name.' '.$last_name;

        $user->payroll_first_name = $request->payroll_first_name;

        $user->payroll_last_name = $request->payroll_last_name;

        $user->payroll_email = $request->payroll_email;

        $user->payroll_phone = $request->payroll_phone;

        $user->doc_upload =  $request->input('doc_upload') ? 1 : 0;

        $user->phone_number = $phone_number;

        $user->ssn_id = $finalSsd;

        $user->worker_type = $worker_type;

        $user->gender = $gender;

        $user->updated_by = Auth::user()->id;

        $user->parent = $parent;

        $user->phone_number = $phone_number;

        $user->country_code = $country_code;
        
        $user->status = $status;

        if($request->hasFile('image')) {

            $format_validator = Validator::make($request->all(), [

                'image' => 'mimes:jpeg,png,jpg,gif,svg'
    
            ]);
    
            if($format_validator->fails()) {
    
                Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');
    
                return redirect()->back();
    
            }

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('image');

      $image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);
            
            $user->image = $image_name;

        }

        if($request->input('cropped-image')){

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $request->input('cropped-image-type');

            $imageInfo = $request->input('cropped-image'); 

            $image = fopen($imageInfo, 'r');
            
            file_put_contents($destination.'/'.$image_name, $image);

            fclose($image); 

            $user->image = $image_name;
        }

        $this->saveWorkerDocuments( $request, $user->id );

        /* Document upload */

        $subContId = $user->id;

        if( $request->hasFile('document') ) {

          foreach ( $request->document as $document) {
            
            $destination = public_path('uploads/documents/'. $subContId);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $filen = substr($document->getClientOriginalName(), 0, -4);
            $documentName = $filen.'.'.$document->getClientOriginalExtension();

            //$documentName = 'document-' . time() . '-' . Str::random(15) . '.' . $document->getClientOriginalExtension();

            $document->move($destination, $documentName);
            
            $saveData['user_id'] = $subContId;
            $saveData['document'] = $documentName;

            Document::create( $saveData );

          }

        }

        /* Document upload */

        /*-------------------------------------------- Seperator --------------------------------------------*/

    
        if( isset( $request->user_role ) )
        {
            $user->roles()->detach();
            
            $userRole = $request->input('user_role');

            $user->assignRole( $userRole );

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $user->save();



        if( isset( $request->si_project ) )
        {

          Project::where('superint', $id)->update( [ 'superint' => NULL ] );

          foreach( $request->si_project as $sip )
          {
            Project::where('id', $sip)->update( [ 'superint' => $id ] );
          }

        }

        Session::flash('success', 'User information has been updated successfully.');

        Session::flash('status-user', $user->id);

        $redirectQuery = 'userType='.$userRole;
        

        if( isset($request->goback) )
        {
              return redirect(route('admin.users.view').'/'.$user['id'].'?column=0&dir=asc&from=workers&title=null&status=null&company=null');
        }


        return redirect()->back();
        //return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::where('id', $id)->firstOrFail();

        if( count( $user->logs ) == 0 )
        {

          DB::table('users')->where('id', $id)->delete();

          Session::flash('success', 'User has been deleted successfully.');

          return redirect()->back();

        }
        else
        {

          Session::flash('error', 'Worker has active logs so could not be deleted.');

        }

        //$user->delete();

        

        return redirect()->back();

    }

    /**
     * Approve worker
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();

        $user = User::find( $id );

        if( $user->status != 1 )
        {

          Session::flash('error', 'This action has already been performed on the request.');
          return redirect( route('admin.workers.requests') );

        }

        $user->status = 2;

        $user->save();

        $userData = User::where('id', $id)->first();

        $emailUserData = [];

        $emailUserData['first_name'] = $userData->first_name;
        $emailUserData['name'] = 'Name: '.$userData->first_name.' '.$userData->last_name.'<br>';
        $emailUserData['email'] = $userData->email;
        $emailUserData['phone_number'] = '';

        $workerCompanyfieldForEmail = 'Company: '.getCompanyNameFromId( $userData->parent );


        if( !empty( $userData->phone_number ) )
        {

          $emailUserData['phone_number'] = 'Phone number: '.$userData->phone_number.'<br>';

        }

        //
        if( !empty( $userData->email ) )
        Mail::to( $userData->email )->send(new ApproveWorker((object)$emailUserData, 'Mail', '<i>Same as you entered.</i>', $workerCompanyfieldForEmail));

        Session::flash('success', 'Worker request has been approved.');

        //return redirect()->route('admin.users');

        return redirect( route('admin.workers.requests') );

    }

    /**
     * Reject worker
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::find($id);

        if( !$user || $user->status != 1 )
        {

          Session::flash('error', 'This action has already been performed on the request.');
          return redirect( route('admin.workers.requests') );

        }

        $user->delete();

        Session::flash('success', 'Worker request has been rejected.');

        //return redirect()->route('admin.users');

        return redirect( route('admin.workers.requests') );

    }

    /* */

    public function deleteDocument( $id )
    {

      $obj = Document::find( $id );

      $obj->delete();

      Session::flash('success', 'Document has been deleted.');

      return redirect()->back();

    }

    /* */

    public function deleteWorkerDocument( $id )
    {

        $obj = WorkerDocuments::find( $id );

      $obj->delete();

      Session::flash('success', 'Document has been deleted.');

      return redirect()->back();

    }

    public function remove($id)
    {

        //$user = User::role('user')->where('id', $id)->firstorFail();
        $user = User::where('id', $id)->firstorFail();

        $user->image = null;

        $user->save();

        Session::flash('success', 'User image has been removed successfully.');

        return redirect()->back();

    }

}
