<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\User;
use App\WorkerType;
use Fpdf;
use Auth;

class DashboardController extends Controller
{
    
    public function index()
    {

    	$totalProjects = Project::count();

        $totalActiveProjects = Project::where( 'status', 'active' )->count();

    	$totalWorkers = User::role('worker')->where( 'status', '!=', 1 )->count();

    	$totalAdmins = User::role('admin')->count();

    	$totalSubContractors = User::role('subcontractor')->count();

    	$totalForemans = User::role('foreman')->count();

        $userType = 'worker';

        $userTitle = 'Worker';

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

        }
        
        $users = User::paginate( 20 );

        $titlesList = WorkerType::get();

        $companiesList = User::role('subcontractor')->get();

    	$data = compact(

    		'totalProjects',

            'totalActiveProjects',

    		'totalWorkers',

    		'totalAdmins',

    		'totalSubContractors',

    		'totalForemans',

            'users',

            'userType',

            'userTitle',

            'titlesList',

            'companiesList'

    	);

        if( Auth::user()->hasRole('Payroll') )
        {
            return( redirect( route('admin.payrolls') ) );
        }
        elseif(  Auth::user()->hasRole('Superint') )
        {
            return( redirect( route('manpower') ) );
        }
        elseif(  Auth::user()->hasRole('Subcontractor') )
        {
            return( redirect( route('subcontractor.dashboard') ) );
        }
        else
        {
            return view( 'admin.dashboard.index', $data );
        }

        

    }

}
