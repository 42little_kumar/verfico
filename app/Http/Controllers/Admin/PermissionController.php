<?php

namespace App\Http\Controllers\Admin;

use Session;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug = false)
    {
        
        $roles = Role::where('slug', '!=', 'administrator')->where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->orderBy('name', 'asc')->get();

        if($slug) {

            $role = $roles->where('slug', $slug)->first();

        }
        
        if(!isset($role) || !$role) {

            $role = $roles->first();

        }

        $permissions = Permission::all();

        $data = [];

        foreach($permissions as $p) {

            $array = explode(' ', $p->name);

            $permission = $array[0];

            $section = $array[1];

            $data[$section][] = $permission;

        }

        $permissions = collect($data)->sortKeys();

        return view('admin.permissions.index', compact('roles', 'role', 'permissions'));


    }

    public function permissions($role, $section, $permission)
    {

        $role = Role::where('slug', $role)->firstOrFail();

        $section_permission = $permission . ' ' . $section;

        $permission = Permission::where('name', $section_permission)->firstOrFail();

        $permissions = Permission::where('name', 'LIKE', '%' . $section . '%')->get();

        foreach($permissions as $p) {

            $role->revokePermissionTo($p);

        }

        $role->givePermissionTo($permission);

        Session::flash('success', $role->name . ' permissions updated successfully.');

        return redirect()->back();

    }

}
