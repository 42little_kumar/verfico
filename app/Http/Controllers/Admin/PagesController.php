<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\Project;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use App\WorkerType;
use App\Pages;
use Maatwebsite\Excel\Facades\Excel;

class PagesController extends Controller
{
    
	public function index()
	{
		$page = Pages::first();

		$data = compact(

			'page'

		);

	    return view( 'admin.pages.billboard.index', $data );
	}

	/* */

	public function update( Request $request )
	{

		$user = Pages::firstOrNew( array( 'name' => 'billboard' ) );

		$user->name = $request->name ;

		$user->content = $request->content ;

		$user->save();

		Session::flash('success', 'Billboard has been updated successfully.');

        return redirect()->back();

	}

}
