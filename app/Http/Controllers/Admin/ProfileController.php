<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $fieldPrefix = '';

        $user = Auth::user();

        $userType = $user->getRoleNames();

        $userTypeName = $userType[0];

        if( $userTypeName == 'Subcontractor' )
        {

            $fieldPrefix = 'Contract ';

        }

        return view('admin.profile.index', compact( 'user', 'userTypeName', 'fieldPrefix' ));
    }

    /**
     * Update profile image.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'first-name'=> 'required'

        ]);

        if($validator->fails()) {

            Session::flash('error', 'Please enter your first name.');

            return redirect()->back();

        }

        $validator = Validator::make($request->all(), [

            'email'=> 'required'

        ]);

        if($validator->fails()) {

            Session::flash('error', 'Please enter your email.');

            return redirect()->back();

        }

        $limit_validator = Validator::make($request->all(), [

            'first-name' => 'max:25',

            'last-name' => [ 'nullable', 'max:25' ]

        ]);

        if($limit_validator->fails()) {

            if($validator->errors()->first('first-name')) {
        
                Session::flash('error', 'First name can contain maximum of 25 characters.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('last-name')) {
        
                Session::flash('error', 'Last name can contain maximum of 25 characters.');
        
                return redirect()->back();
        
            }

        }

        $user = Auth::user();

          if($request->input('cropped-image')){

            $format_validator = Validator::make($request->all(), [

                'cropped-image-type' => 'in:jpeg,png,jpg,gif,svg'
    
            ]);
    
            if($format_validator->fails()) {
    
                Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');
    
                return redirect()->back();
    
            }


            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $request->input('cropped-image-type');

            $imageInfo = $request->input('cropped-image'); 

            $image = fopen($imageInfo, 'r');
            
            file_put_contents($destination.'/'.$image_name, $image);

            fclose($image); 

            $user->image = $image_name;
        }


        $user->first_name = $request->input('first-name');

        $user->last_name = $request->input('last-name');

        $user->phone_number = $request->input('phone_number');

        $user->email = $request->input('email');

        $user->timezone_manual = @$request->input('timezone_manual');

        $user->gender = $request->input('gender');

        $user->save();

        Session::flash('success', 'Profile information has been updated successfully.');

        return redirect()->route('admin.profile');
        
    }

    /**
     * Update profile image.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function image(Request $request)
    {

        $input_validator = Validator::make($request->all(), [

            'image' => 'required'

        ]);

        if($input_validator->fails()) {

            Session::flash('error', 'Please select an image to upload.');

            return redirect()->back();

        }

        $image_validator = Validator::make($request->all(), [

            'image' => 'image'

        ]);

        if($image_validator->fails()) {

            Session::flash('error', 'Please select a valid image to upload.');

            return redirect()->back();

        }

        $format_validator = Validator::make($request->all(), [

            'image' => 'mimes:jpeg,png,jpg,gif,svg'

        ]);

        if($format_validator->fails()) {

            Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');

            return redirect()->back();

        }

        $user = Auth::user();

        if ($request->hasFile('image')) {

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('image');

			$image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);
            
            $user->image = $image_name;

            $user->save();

            Session::flash('success', 'Profile image has been updated successfully.');

            return redirect()->route('admin.profile');

        }

        abort(404);

    }

    public function password(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'old-password' => 'required',

            'new-password' => 'required',

            'confirm-new-password' => 'required'

        ]);

        if($validator->fails()) {

            if($validator->errors()->first('old-password')) {

                Session::flash('error', 'Please enter account old password.');

                return redirect()->back();

            }

            if($validator->errors()->first('new-password')) {

                Session::flash('error', 'Please enter new password.');

                return redirect()->back();

            }

            if($validator->errors()->first('confirm-new-password')) {

                Session::flash('error', 'Please enter confirm new password.');

                return redirect()->back();

            }

        }

        $password_validator = Validator::make($request->all(), [

            'confirm-new-password' => 'same:new-password'

        ]);

        if($password_validator->fails()) {

            Session::flash('error', 'New password and Confirm new password do not match.');

            return redirect()->back();

        }

        $user = Auth::user();

        if(!Hash::check($request->input('old-password'), $user->password)) {

            Session::flash('error', 'Old password do not match account password.');

            return redirect()->back();

        }

        if(Hash::check($request->input('new-password'), $user->password)) {

            Session::flash('error', 'New password cannot be same as the old password.');

            return redirect()->back();

        }

        $user->password = Hash::make($request->input('new-password'));

        $user->save();

        Session::flash('success', 'Account password has been updated sucessfully.');

        return redirect()->back();

    }

    public function userPasswordChange(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'new-password' => 'required',

            'confirm-new-password' => 'required'

        ]);

        if($validator->fails()) {


            if($validator->errors()->first('new-password')) {

                Session::flash('error', 'Please enter new password.');

                return redirect()->back();

            }

            if($validator->errors()->first('confirm-new-password')) {

                Session::flash('error', 'Please enter confirm new password.');

                return redirect()->back();

            }

        }

        $password_validator = Validator::make($request->all(), [

            'confirm-new-password' => 'same:new-password'

        ]);

        if($password_validator->fails()) {

            Session::flash('error', 'New password and Confirm new password do not match.');

            return redirect()->back();

        }

        $user = User::find( $request->user_id );

        $user->password = Hash::make($request->input('new-password'));

        $user->save();

        Session::flash('success', 'Password has been updated successfully.');

        return redirect()->back();

    }

}
