<?php

namespace App\Http\Controllers\Admin;

use File;
use Session;
use App\User;
use App\Project;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use App\WorkerType;
use Maatwebsite\Excel\Facades\Excel;

class WorkerController extends Controller
{

	/**
	 * Display a listing of the worker types.
	 *
	 * 
	 */
	public function index( Request $request )
	{

	   return view('admin.workertypes.index');
	    
	}

    public function createParentOfLogs()
    {

        $logs = \DB::table('worker_log')->get();

        foreach ($logs as $g) {
            
            if( \DB::Table('users')->where('id', $g->worker_id)->exists() )
            {
                $data = array( 'sub' => \DB::Table('users')->where('id', $g->worker_id)->first()->parent );

            \DB::table('worker_log')->where('id', $g->id)->update( $data );
            }

            

        }

    }

	 /**
	 * Get worker types.
	 *
	 * 
	 */
	public function getData( Request $request )
	{

	   //$logs = User::find( $request->userId )->logs;

	   $data = WorkerType::orderBy('id', 'DESC')->get();


	   return Datatables::of( $data )
	   ->addColumn('type', function( $data )
	   {

	   	$editElement = "<div style='display: none;' class='edit-type'> <input style='width: 110px;' class='type-input' value='$data->type' type='text'> </div>";

		$text = "<div class='type-text'>".$data->type."</div>";

            return $text.$editElement;

	   })
	   ->addColumn('action', function( $data )
	   {

	       $editLink = '';

	       $deleteLink = route('admin.worker.type.delete', $data->id);

	       $html = '<div class="table-action-container">

	       				<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-circle btn-action-edit" title="Edit">
    
                            <i class="fas fa-pencil-alt"></i>
    
                        </a>

	                   <a style="display: none;" href="javascript:void(0)" element-id="' . $data->id . '" class="btn btn-sm btn-success btn-circle btn-action-update" title="Save">
	   
	                       <i class="fas fa-check"></i>
	   
	                   </a>

                       <a href="javascript:void(0)" style="display: none;" class="btn btn-sm btn-danger btn-circle btn-action-close" title="Cancel">
       
                           <i class="fas fa-times" style="font-size: 21px;"></i>
       
                       </a>

	                   <a href="' . $deleteLink . '" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
	   
	                       <i class="fas fa-trash"></i>
	   
	                   </a>

	               </div>';

	       return $html;

	   })
	   ->rawColumns( ['type', 'action'] )
	   ->make( true );
	    
	}

	/*
     * @Function Name
     *
     *
     */
    public function update( Request $request )
    {

        try
        {

            $workerTypeId = $request->id;

            $validator = Validator::make($request->all(), [

                'type' => "unique:worker_types,type,$workerTypeId,id",

            ]);

            if($validator->fails())
            {

                if($validator->errors()->first('type')) {
            
                    $response['success'] = false;

                    $response['message'] = 'This type name already exists.';

                    return json_encode( $response );
                    
                }

            }
            
            $obj = new WorkerType();

			$obj->exists = true;

            $request = $request->except('_token');

            foreach($request as  $key => $field):

            	$obj->$key = $field;

            endforeach;

            $obj->save();

            $response['success'] = true;

            $response['data'] = $obj;

            $response['message'] = 'Type updated.';

            

            return json_encode( $response );

        }
        catch (Exception $e)
        {
            
            $response['success'] = false;

            $response['message'] = 'Some error.';

            return json_encode( $response );

        }

    }

    /*
     * @Function Name
     *
     *
     */
    public function add( Request $request )
    {

        $validator = Validator::make($request->all(), [

            'type_name' => "unique:worker_types,type",

        ]);

        if($validator->fails())
        {

            if($validator->errors()->first('type_name')) {
        
                $response['success'] = false;

                Session::flash('error', 'This type name already exists.');

                return redirect()->back();
                
            }

        }

    	$data['type'] = $request->type_name;

    	WorkerType::create( $data );

    	Session::flash('success', 'Worker type created successfully.');

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $object = WorkerType::where('id', $id)->firstOrFail();

        $object->delete();

        Session::flash('success', 'Type has been deleted successfully.');

        return redirect()->back();

    }
}