<?php

namespace App\Http\Controllers\Subcontractor;

use File;
use Session;
use App\User;
use App\Project;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use QrCode;
use Auth;
use App\WorkLog;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $allStatus = array(

            'active' => 'Active',
            'disabled' => 'Disabled',

        );

        return view('subcontractor.project.index', compact( 'allStatus' ));
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function favindex( Request $request )
    {

        $allStatus = array(

            'active' => 'Active',
            'disabled' => 'Disabled',

        );

        return view('subcontractor.project.favindex', compact( 'allStatus' ));
        
    }

    /*
     * @Datatable get
     *
     *
     */
    public function getProjects( Request $request )
    {

        $subContId = Auth::user()->id;

        $subUsers = User::where( 'parent', $subContId )->pluck('id')->toArray();

        $projectsIds = WorkLog::whereIn('worker_id', $subUsers)->pluck('project_id')->toArray();

        $projectObject = Project::whereIn( 'id', $projectsIds );

        if( isset( $request->status ) )
          $projectObject->where('status', $request->status);

        $project = $projectObject->get();

        return Datatables::of( $project )
        ->addColumn( 'jobnumber', function( $project )
        {

            $viewLink = route('subcontractor.project.view', [ 'id' => $project->id ]);

           	return "<a class='index-link' href='$viewLink'>$project->job_number</a>";

        })
        ->addColumn( 'jobname', function( $project )
        {
            
            return $project->job_name;

        })
        ->addColumn( 'status', function( $project )
        {
            
            return ucfirst( str_replace('_', ' ', $project->status) );

        })
        ->addColumn( 'address', function( $project )
        {

            return $project->job_address;
            
        })
        ->addColumn( 'favorite', function( $project )
        {

          $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star-o' aria-hidden='true'></i></a>";

          if( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->where('payroll_id', $project->id)->exists() )
          {
            $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star'></a>"; 
          }

            return $fl;
            
        })
        ->addColumn( 'action', function( $project )
        {

            $viewLink = route('subcontractor.project.view', [ 'id' => $project->id ]);

            $editLink = '';

            $deleteLink = '';

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
    
                                          View
    
                                      </a>
                                  </div> 

                                  
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['jobnumber', 'jobname', 'address', 'favorite', 'action'] )
        ->make( true );
    }

    /*
     * @Datatable get
     *
     *
     */
    public function getProjectsFav( Request $request )
    {

        $favIds = [];

        foreach( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->get() as $get )
        {
            array_push($favIds, $get->payroll_id);
        }

        $subContId = Auth::user()->id;

        $subUsers = User::where( 'parent', $subContId )->pluck('id')->toArray();

        $projectsIds = WorkLog::whereIn('worker_id', $subUsers)->pluck('project_id')->toArray();

        $projectObject = Project::whereIn( 'id', $favIds );

        if( isset( $request->status ) )
          $projectObject->where('status', $request->status);

        $project = $projectObject->get();

        return Datatables::of( $project )
        ->addColumn( 'jobnumber', function( $project )
        {

            $viewLink = route('subcontractor.project.view', [ 'id' => $project->id ]);

            return "<a class='index-link' href='$viewLink'>$project->job_number</a>";

        })
        ->addColumn( 'jobname', function( $project )
        {
            
            return $project->job_name;

        })
        ->addColumn( 'status', function( $project )
        {
            
            return ucfirst( str_replace('_', ' ', $project->status) );

        })
        ->addColumn( 'address', function( $project )
        {

            return $project->job_address;
            
        })
        ->addColumn( 'favorite', function( $project )
        {

          $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star-o' aria-hidden='true'></i></a>";

          if( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->where('payroll_id', $project->id)->exists() )
          {
            $fl = "<a href='".route('admin.payrolls.fav', [$project->id, Auth::user()->id])."'><i class='fa fa-star'></a>"; 
          }

            return $fl;
            
        })
        ->addColumn( 'action', function( $project )
        {

            $viewLink = route('subcontractor.project.view', [ 'id' => $project->id ]);

            $editLink = '';

            $deleteLink = '';

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
    
                                          View
    
                                      </a>
                                  </div> 

                                  
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['jobnumber', 'jobname', 'address', 'favorite', 'action'] )
        ->make( true );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $subContId = Auth::user()->id;

        $data = compact(

            'subContId'

        );
        
        return view('subcontractor.project.create', $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $projectData = Project::where('id', $id)->firstOrFail();

        $subContId = Auth::user()->id;

        $decodedAddress = json_decode( $projectData->job_address );

        $address = $decodedAddress;

        return view( 'subcontractor.project.edit', compact( 'projectData', 'subContId', 'address' ) );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $projectData = Project::where('id', $id)->firstOrFail();

        if( !empty( $projectData->job_address ) )
        {

          $decodedAddress = json_decode( $projectData->job_address );

          $decodedAddressArray = (array)$decodedAddress;

          $decodedAddressArray = array_filter( $decodedAddressArray );

          $address = implode(', ', $decodedAddressArray );

        }

        $projectData->job_address_json = $projectData->job_address;

        $projectData->job_address = $address;

        $previous = null;
    
        $next = null;
        
        if(  isset( $_GET["dir"] ) ):
            
        
    if(@$_GET["from"]=="workers"){
      
      if(@$_GET["column"]==1){

        $subContId = Auth::user()->id;

        $subUsers = User::where( 'parent', $subContId )->pluck('id')->toArray();

        $projectsIds = WorkLog::whereIn('worker_id', $subUsers)->pluck('project_id')->toArray();

        $usersobj = Project::whereIn( 'id', $projectsIds )->orderBy('job_name',@$_GET["dir"]);
        


            
            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }
            
            $users = $usersobj->get();

          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
      }
      if(@$_GET["column"]==0){
        
        $subContId = Auth::user()->id;

        $subUsers = User::where( 'parent', $subContId )->pluck('id')->toArray();

        $projectsIds = WorkLog::whereIn('worker_id', $subUsers)->pluck('project_id')->toArray();

        $usersobj = Project::whereIn( 'id', $projectsIds )->orderBy('job_number',@$_GET["dir"]);
            
            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }
            
            $users = $usersobj->get();

          $users = $usersobj->get();
          
          $index = null;
          $i=0;
          foreach($users as $user){
            if($user->id == $id){
              $index = $i;
              break;
            }
            $i++;
          }
          if(isset($users[$i+1])){
            $next = $users[$i+1];
          }
          if(isset($users[$i-1])){
            $previous = $users[$i-1];
          }
          
          
        
            }
            

        }
        
        endif;

        return view( 'subcontractor.project.view', compact( 'projectData', 'previous', 'next' ) );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'job_number' => 'required|unique:project',

            'job_name' => 'required',
            
            'sub_contractor' => 'required',

        ]);

        if($validator->fails()) {

            $request->flash();
        
            if($validator->errors()->first('job_number')) {
        
                Session::flash('error', 'Please verify job number. It should not be empty or duplicate.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('job_name')) {
        
                Session::flash('error', 'Please enter job name.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('sub_contractor')) {
        
                Session::flash('error', 'Please enter Subcontractor.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('job_address')) {
        
                Session::flash('error', 'Please enter job address.');
        
                return redirect()->back();
        
            }
        
        }

        $jobNumber = $request->input('job_number');

        $jobName = $request->input('job_name');

        $subContractor = $request->input('sub_contractor');

        $generalContractorName = $request->input('general_contractor_name');

        /* Address */

        $jobAddress = json_encode( $request->input('address') );

        /* Address */

        $qrCode = 'QrCode';

        $project = Project::create([

            'job_number' => $jobNumber,

            'job_name' => $jobName,

            'sub_contractor' => $subContractor,

            'general_contractor_name' => $generalContractorName,

            'qrcode' => $qrCode,

            'job_address' => $jobAddress
            
        ]);

        /*-------------------------------------------- Seperate --------------------------------------------*/

        $baseUrl = url('/');

        $QrDataArray = [];

        $QrDataArray['project_id'] = $project->id;

        $QrDataArray['project_name'] = $project->job_name;

        $QrDataJson = json_encode( $QrDataArray );

        $qrdata = base64_encode( QrCode::format('png')->size(265)->generate( $QrDataJson ) );

        $qrUserData = array(

          'qrcode' => $qrdata

        );

        Project::where( 'id', $project->id )
        ->update( $qrUserData );

        /*-------------------------------------------- Seperate --------------------------------------------*/

        Session::flash('success', 'New project created successfully.');

        return redirect()->route('subcontractor.projects');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
       $validator = Validator::make($request->all(), [

           'job_number' => 'required|unique:project,job_number,'.$id,

           'job_name' => 'required',
           
           'sub_contractor' => 'required',

       ]);

       if($validator->fails()) {

           $request->flash();
       
           if($validator->errors()->first('job_number')) {
       
               Session::flash('error', 'Please verify job number. It should not be empty or duplicate.');
       
               return redirect()->back();
       
           }

           if($validator->errors()->first('job_name')) {
       
               Session::flash('error', 'Please enter job name.');
       
               return redirect()->back();
       
           }

           if($validator->errors()->first('sub_contractor')) {
       
               Session::flash('error', 'Please enter Subcontractor.');
       
               return redirect()->back();
       
           }

           if($validator->errors()->first('job_address')) {
       
               Session::flash('error', 'Please enter job address.');
       
               return redirect()->back();
       
           }
       
       }

       $jobNumber = $request->input('job_number');

       $jobName = $request->input('job_name');

       $subContractor = $request->input('sub_contractor');

       $generalContractorName = $request->input('general_contractor_name');

       /* Address */

        $jobAddress = json_encode( $request->input('address') );

        /* Address */ 

       $projectData = array(

           'job_number' => $jobNumber,

           'job_name' => $jobName,

           'sub_contractor' => $subContractor,

           'general_contractor_name' => $generalContractorName,

           'job_address' => $jobAddress

       );

       $projectObject = Project::find( $id );

       $projectObject->update( $projectData );

       Session::flash('success', 'Project updated successfully.');

       return redirect()->route('subcontractor.projects');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $project = Project::where('id', $id)->firstOrFail();

        $project->delete();

        Session::flash('success', 'Project deleted successfully.');

        return redirect()->back();

    }
}
