<?php

namespace App\Http\Controllers\Subcontractor;

use File;
use Session;
use App\User;
use App\WorkLog;
use App\Project;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use QrCode;
use Auth;


class LogController extends Controller
{
    
	/*
	 * @Function Name
	 *
	 *
	 */
	public function getUserLogs( Request $request )
	{

		//$logs = User::find( $request->userId )->logs;

        $logs = WorkLog::where( 'worker_id', $request->userId )->where('sub', Auth::user()->id)->get();

     
		return Datatables::of( $logs )
        ->addColumn('project_name', function( $logs )
        {

           return $logs->project->job_name;

        })
        ->addColumn('project_number', function( $logs )
        {

           return $logs->project->job_number;

        })
        ->addColumn('punch_in', function( $logs )
        {

            $autoOut = asset('public/images/auto_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');
            
            switch ( $logs->in_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon' src='$byForeman'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-in-time'> <input class='punch-input' value='$logs->punch_in' type='text'> </div>";
            
            $punchTime = "<div style='width: 97px;' class='punch-in-time'>".$logs->punch_in_time."</div><span>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;

        })
        ->addColumn('punch_out', function( $logs )
        {

            $autoOut = asset('public/images/auto_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');

        	if( empty( $logs->punch_out ) )
        	{

        		return "<span class='status_active'>Punched In</span>";

        	}

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-out-time'> <input class='punch-input' value='$logs->punch_out' type='text'> </div>";

            switch ( $logs->out_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon' src='$byForeman'>";
                    break;
                case '4':
                    $imgSrc = "<img width='20' class='log-icon' src='$autoOut'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }
            
            $punchTime = "<div style='width: 97px;' class='punch-out-time'>".$logs->punch_out_time."</div><span>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;
            
        })
        ->addColumn( 'total_hours', function( $logs )
        {

            if( empty( $logs->punch_out ) )
            {

                return "<div style='text-align: center;'>N/A</div>";

            }

            //$start  = new Carbon( $logs->punch_in );
                            
            //$end    = new Carbon( $logs->punch_out );

            $logs->punch_in = substr_replace($logs->punch_in, '00', -2);
            $logs->punch_out = substr_replace($logs->punch_out, '00', -2);

            $start = date("Y-m-d ".$logs->punch_in);
            
            if(strtotime($logs->punch_out) < strtotime($logs->punch_in)){
                
                $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logs->punch_out) ) + 24*60*60) ;
            } else {
                $end = date("Y-m-d ".$logs->punch_out);
            }

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

            $totalTimeInt = (float)str_replace(':','.',$totalTime);

            $overTime = '';

            if( $totalTimeInt > 8 )
            {
                $overTime = "<span><img style='width: 20px;' title='Overtime' class='log-icon' src='".asset('public/images/time.png')."'></span>";
            }

            return "<div class='total-hours-class' style='text-align: center;'>".$totalTime."</div>";

            return "<div style='text-align: center;'>".$start->diff($end)->format('%H:%I')."</div>";

        })
        ->addColumn('date', function( $logs )
        {

           return getTimeZonePunchDate( $logs->punch_at );

        })
        ->rawColumns( ['project_name','project_number', 'punch_in', 'punch_out', 'total_hours', 'action'] )
        ->make( true );

	}

    /*
     * @Function Name
     *
     *
     */
    public function getProjectLogs( Request $request )
    {

        //$logs = User::find( $request->userId )->logs;

        $subContractorId = Auth::user()->id;

        $logsObject = WorkLog::where( 'project_id', $request->projectId )
        ->leftJoin('users', 'worker_log.worker_id', '=', 'users.id')
        ->where( 'users.parent', $subContractorId )
        ->where( 'sub', $subContractorId );

        if( isset( $request->log_date ) )
        {
                
            list( $startDate, $endDate ) = explode( '-', $request->log_date );

            /* Format date */

            $from = Carbon::parse( $startDate )
                             ->startOfDay()
                             ->toDateTimeString();

            $to = Carbon::parse( $endDate )
                             ->endOfDay()
                             ->toDateTimeString();

            /* Format date */

            $logsObject->whereBetween('worker_log.punch_at', [$from, $to])->get();

        }

        $logs = $logsObject->orderBy('users.updated_at', 'desc')->get();

     
        return Datatables::of( $logs )
        ->addColumn('worker_name', function( $logs )
        {

           return $logs->user->first_name.' '.$logs->user->last_name;

        })
        ->addColumn('subcontractor', function( $logs )
        {

            return $logs->user->sub_contractor_name;
            
        })
        ->addColumn('title', function( $logs )
        {

            return ucwords( $logs->user->worker_type_name );
            
        })
        ->addColumn('punch_in', function( $logs )
        {

            $autoOut = asset('public/images/auto_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-in-time'> <input class='punch-input' value='$logs->punch_in_time' type='text'> </div>";

            switch ( $logs->in_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon' src='$byForeman'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-in-time'> <input class='punch-input' value='$logs->punch_in' type='text'> </div>";
            
            $punchTime = "<div style='width: 97px;' class='punch-in-time'>".$logs->punch_in_time."</div><span>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;

        })
        ->addColumn('punch_out', function( $logs )
        {

            $autoOut = asset('public/images/auto_out.png');

            $byForeman = asset('public/images/by_foreman.png');

            $manualPunch = asset('public/images/manual_punch.png');

            if( empty( $logs->punch_out ) )
            {

                return "<span class='status_active'>Punched In</span>";

            }

            $editPunchTimeElement = "<div style='display: none; width: 97px;' class='edit-punch-out-time'> <input class='punch-input' value='$logs->punch_out' type='text'> </div>";

            switch ( $logs->out_type ) {
                case '2':
                    $imgSrc = "<img width='20' class='log-icon' src='$manualPunch'>";
                    break;
                case '3':
                    $imgSrc = "<img width='20' class='log-icon' src='$byForeman'>";
                    break;
                case '4':
                    $imgSrc = "<img width='20' class='log-icon' src='$autoOut'>";
                    break;
                default:
                    $imgSrc = "";
                    break;
            }
            
            $punchTime = "<div style='width: 97px;' class='punch-out-time'>".$logs->punch_out_time."</div><span>$imgSrc</span>";

            return $punchTime.$editPunchTimeElement;
            
        })
        ->addColumn( 'total_hours', function( $logs )
        {

            if( empty( $logs->punch_out ) )
            {

                return "<div style='text-align: center;'>N/A</div>";

            }

            //$start  = new Carbon( $logs->punch_in );
                            
            //$end    = new Carbon( $logs->punch_out );

            $logs->punch_in = substr_replace($logs->punch_in, '00', -2);
            $logs->punch_out = substr_replace($logs->punch_out, '00', -2);

            $start = date("Y-m-d ".$logs->punch_in);
            
            if(strtotime($logs->punch_out) < strtotime($logs->punch_in)){
                
                $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logs->punch_out) ) + 24*60*60) ;
            } else {
                $end = date("Y-m-d ".$logs->punch_out);
            }

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            $totalTime = date('H:i',strtotime('+1 minutes',strtotime($totalTime)));

            $totalTimeInt = (float)str_replace(':','.',$totalTime);

            $overTime = '';

            if( $totalTimeInt > 8 )
            {
                $overTime = "<span><img style='width: 20px;' title='Overtime' class='log-icon' src='".asset('public/images/time.png')."'></span>";
            }

            return "<div class='total-hours-class' style='text-align: center;'>".$totalTime."</div>";

            return "<div style='text-align: center;'>".$start->diff($end)->format('%H:%I')."</div>";

        })
        ->addColumn('date', function( $logs )
        {

            return getTimeZonePunchDate( $logs->punch_at );
            
        })
        ->rawColumns( ['worker_name', 'punch_in', 'punch_out', 'total_hours', 'action'] )
        ->make( true );

    }

    /*
     * @Creat log
     *
     *
     */
    public function create( $userId )
    {

        $allProjects = Project::all();

        $data = compact(

            'allProjects',

            'userId'

        );

        return view( 'admin.worklog.create', $data );

    }

    /*
     * @Save log
     *
     *
     */
    public function save( Request $request )
    {

        print_r( $request->all() );

    }

    /*
     * @Function Name
     *
     *
     */
    public function update( Request $request )
    {

        try
        {
            
            $logId = $request->logId;

            $punchIn = $request->punchIn;

            $punchOut = $request->punchOut;

            $logUpdateArray = [];

            $logUpdateArray['punch_in'] = date( "H:i:s", strtotime( $punchIn ) );;

            $logUpdateArray['punch_out'] = date( "H:i:s", strtotime( $punchOut ) );;

            $logObject = WorkLog::find( $logId );

            $logObject->update( $logUpdateArray );

            /* */

            $logAfterUpdate = WorkLog::find( $logId );

            $logAfterUpdate->punch_in = substr_replace($logAfterUpdate->punch_in, '00', -2);
            $logAfterUpdate->punch_out = substr_replace($logAfterUpdate->punch_out, '00', -2);

            $start = date("Y-m-d ".$logAfterUpdate->punch_in);
            
            if(strtotime($logAfterUpdate->punch_out) < strtotime($logAfterUpdate->punch_in)){
                
                $end = date("Y-m-d H:i:s", strtotime(date("Y-m-d ".$logAfterUpdate->punch_out) ) + 24*60*60) ;
            } else {
                $end = date("Y-m-d ".$logAfterUpdate->punch_out);
            }

            $totalTime = date("H:i", strtotime("Y-m-d") + strtotime($end) - strtotime($start));

            /* */

            $response['status'] = true;

            $response['data'] = $logObject;

            $response['total_time'] = $totalTime;

            $response['message'] = 'Log updated.';

            return json_encode( $response );

        }
        catch (Exception $e)
        {
            
            $response['status'] = false;

            $response['message'] = 'Some error.';

            return json_encode( $response );

        }

        

    }

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = WorkLog::where('id', $id)->firstOrFail();

        $user->delete();

        Session::flash('success', 'Log deleted successfully.');

        return redirect()->back();

    }

}
