<?php

namespace App\Http\Controllers\Subcontractor;

use File;
use Session;
use App\User;
use App\Payroll;
use App\PayrollFiles;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use QrCode;
use Auth;
use Mail;
use App\Imports\PayrollImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\PayrollNotification;

class PayrollsController extends Controller
{
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request )
	{

	    return view('subcontractor.payrolls.index');
	    
	}

	/*
	 * @Datatable get
	 *
	 *
	 */
	public function getPayrolls( Request $request )
	{

		$subContId = Auth::user()->id;

	    $payrollObject = Payroll::where( 'author', $subContId );

	    if( isset( $request->payroll_date ) )
		{

			$payrollObject->where('title', $request->payroll_date);

		}

		$payrolls = $payrollObject->get();

	    return Datatables::of( $payrolls )
	    ->addColumn( 'start_date', function( $payrolls )
	    {

	    	$viewLink = route('admin.payroll.view', [ 'id' => $payrolls->id ]);

			   //return "<a class='index-link' href='$viewLink'>".ucwords( $payrolls->title )."</a>";

			   $title = ucwords( $payrolls->title );

			   $endDate = date( "m-d-Y", strtotime( $payrolls->start_date ) );
			   
			   return $endDate;

		})
		->addColumn( 'end_date', function( $payrolls )
	    {

	    	$viewLink = route('subcontractor.payroll.view', [ 'id' => $payrolls->id ]);

			   //return "<a class='index-link' href='$viewLink'>".ucwords( $payrolls->title )."</a>";

			   $title = ucwords( $payrolls->title );

			   $endDate = date( "m-d-Y", strtotime( $payrolls->end_date ) );
			   
			   return "<a class='index-link' href='$viewLink'>".$endDate."</a>";

	    })
	    ->addColumn( 'payroll_file', function( $payrolls )
	    {
	      
	        return $payrolls->payroll_file_source;

	    })
	    ->addColumn( 'uploaded_on', function( $payrolls )
	    {

	        return $payrolls->uploaded_on;
	        
		})
		->addColumn( 'status', function( $payrolls )
	    {

	        return $payrolls->status ? '<span class="green">Approved</span>' : '<span class="red">Pending Review</span>';
	        
	    })
	    ->addColumn( 'action', function( $payrolls )
	    {

	        $viewLink = route('subcontractor.payroll.view', [ 'id' => $payrolls->id ]);

			$deleteLink = route('admin.payroll.delete', [ 'id' => $payrolls->id ]);

			$deletePopUp = "return confirm('Are you sure you want to delete?')";
			
			$deleteHtml = '';

			//$deleteLink = route('subcontractor.payroll.delete', [ 'id' => $payrolls->id ]);
			
			if( $payrolls->status == 0 )
			{

				$deleteHtml = '<div>
				<a href="' . $deleteLink . '" onclick="'.$deletePopUp.'" class="btn btn-sm btn-primary btn-circle" title="Edit">
				Delete
			</a>
			</div>';
				
			}

	        $html = '<div class="drill-type">

	                     <div class="company_edit">
	                        <div class="dropdown">

	                           <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
	                           </button>

	                           <div class="dropdown-menu">

	                              <div>
	                                 <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
	
	                                      View
	
	                                  </a>
	                              </div> 

									'.$deleteHtml.'
	                           </div>
	                        </div>
	                     </div>
	                     <div class="clearfix"></div>
	                  </div>';

	        return $html;

	    })
	    ->rawColumns( ['title', 'payroll_file', 'uploaded_on', 'status', 'action', 'end_date'] )
	    ->make( true );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function downloadFiles( $file_source, $author )
	{

		$payrollFileUrl = base_path( '/public/uploads/images/payroll/'.$author );

	    $payrollFileUrl = $payrollFileUrl.'/'.$file_source;


   
        $filepath = urldecode($payrollFileUrl);;

        // Process download
        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            die();
        } else {
            http_response_code(404);
	        die();
        }
    

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{

	    $subContId = Auth::user()->id;

	    $data = compact(

	        'subContId'

	    );
	    
	    return view('subcontractor.payrolls.create', $data);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$subContId = Auth::user()->id;

		$data['payroll_id'] = null;
		$import = new PayrollImport($data);
		Excel::import($import, $request->file('payroll_file'));

	    $validator = Validator::make($request->all(), [

	        'title' => 'required',
	        
	        'payroll_file' => 'required',

	    ]);

	    $extensions = array("csv","pdf","xlsx","xls","xlm","xla","xlc","xlt","xlw");

	    $result = array( $request->file('payroll_file')->getClientOriginalExtension() );

		if( !in_array( $result[0], $extensions ) )
		{

			Session::flash('error', 'Please upload valid payroll file (Excel format or PDF).');
			
			return redirect()->back();

		}

	    if($validator->fails()) {

	        $request->flash();
	    
	        if($validator->errors()->first('title')) {
	    
	            Session::flash('error', 'Please enter payroll title.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('description')) {
	    
	            Session::flash('error', 'Please enter payroll description.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('payroll_file')) {
	    
	            Session::flash('error', 'Please upload payroll file.');
	    
	            return redirect()->back();
	    
	        }
	    
		}
		
		/*if( Payroll::where( 'title', $request->title)->where('author', $subContId)->exists() )
		{
			Session::flash('error', 'Payroll already exists for this dates.');
	    
	        return redirect()->back();
		}*/

		if( Payroll::where( 'title', $request->title)->where('author', $subContId)->exists() )
		{
			$payroll = Payroll::where( 'title', $request->title)->where('author', $subContId);

			PayrollFiles::where('payroll_id', $payroll->first()->id)->delete();

			$payroll->delete();
		}

	    /*-------------------------------------------- Seperator --------------------------------------------*/

	    $title = $request->input('title');

	    $description = $request->input('description');

	    $startDate = $request->input('start_date');

	    $endDate = $request->input('end_date');

	    /*-------------------------------------------- Seperator --------------------------------------------*/

	    /*-------------------------------------------- Payroll file upload --------------------------------------------*/

	    $destination = public_path('uploads/images/payroll/'. $subContId);

        if(!File::isDirectory($destination)) {

            mkdir($destination, 0777, true);

            chmod($destination, 0777);

        }

        $image = $request->file('payroll_file');

		$image_name = 'payroll-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

        $image->move($destination, $image_name);

	    /*-------------------------------------------- Payroll file upload --------------------------------------------*/

	    $mc = 0;

	    $twoTab = 0;

	    $tc = count(PayrollFiles::whereIn('id', $import->getPayrollIds())->get());

	    foreach( PayrollFiles::whereIn('id', $import->getPayrollIds())->get() as $fdata )
	    {
	    	$dbsd = '';
	    	$dbed = '';

	    	$rsd = $request->input('start_date');

	    	$rsd = date( "M d, Y", strtotime( $rsd ) );

	    	$red = $request->input('end_date');

	    	$red = date( "M d, Y", strtotime( $red ) );

	    	if( empty( json_decode( $fdata['file_data'] )[3] ) )
	    	{
	    		$twoTab++;
	    	}

	    	$dbsd = @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(json_decode( $fdata['file_data'] )[2]))->format('M d, Y');
	    	$dbed = @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(json_decode( $fdata['file_data'] )[3]))->format('M d, Y');

	    	echo '<br>';

	    	if( $dbsd == $rsd && $dbed == $red )
	    	{
	    		$mc++;
	    	}
	    }

	    /*if( $twoTab >= 2 )
	    {
	    	Session::flash('error', 'Please upload the excel file with only one tab.');
	    
	        return redirect()->back();
	    }*/

	    if( ( $tc - 1 ) != $mc )
	    {
	    	Session::flash('error', 'One or more work logs you are submitting are outside the week you selected. Please check the CSV file or change the week you selected.');
	    
	        return redirect()->back();
	    }


	    $payroll = Payroll::create([

	        'title' => $title,

	        'description' => $description,

	        'payroll_file' => $image_name,

	        'start_date' => date('Y-m-d H:i:s',  strtotime( $startDate ) ),

	        'end_date' => date('Y-m-d H:i:s', strtotime( $endDate ) ),

	        'author' => $subContId
	        
		]);
		
		PayrollFiles::whereIn('id', $import->getPayrollIds())->update(['payroll_id' => $payroll->id]);

	    Session::flash('success', 'New payroll created successfully.');

	    return redirect()->route('subcontractor.payrolls');

	}

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $payrollData = Payroll::where('id', $id)->firstOrFail();

        $previous = null;
		
        $next = null;

        if(  isset( $_GET["dir"] ) ):
            
        
		if(@$_GET["from"]=="workers"){
			
			if(@$_GET["column"]==1){
				

				 
                    $usersobj = Payroll::where( 'author', Auth::user()->id )->orderBy('start_date',@$_GET["dir"]);

                  
                    
                  if( @$_GET["date"] != 'null' )
                    {
                        $usersobj->where('title', @$_GET["date"]);
                    }
                    
                    $users = $usersobj->get();

					$index = null;
					$i=0;
					foreach($users as $user){
						if($user->id == $id){
							$index = $i;
							break;
						}
						$i++;
					}
					if(isset($users[$i+1])){
						$next = $users[$i+1];
					}
					if(isset($users[$i-1])){
						$previous = $users[$i-1];
					}
					
					
				
			}
			if(@$_GET["column"]==0){
				

                    $usersobj = Payroll::where( 'author', Auth::user()->id )->orderBy('end_date',@$_GET["dir"]);
				    


                    if( @$_GET["date"] != 'null' )
                    {
                        $usersobj->where('title', @$_GET["date"]);
                    }

                    
                    $users = $usersobj->get();
					
					$index = null;
					$i=0;
					foreach($users as $user){
						if($user->id == $id){
							$index = $i;
							break;
						}
						$i++;
					}
					if(isset($users[$i+1])){
						$next = $users[$i+1];
					}
					if(isset($users[$i-1])){
						$previous = $users[$i-1];
					}
					
					
				
            }
            
            if(@$_GET["column"]==2){
				
				

                $usersobj = Payroll::where( 'author', Auth::user()->id )->orderBy('created_at',@$_GET["dir"]);


                if( @$_GET["date"] != 'null' )
                {
                    $usersobj->where('title', @$_GET["date"]);
                }

                $users = $usersobj->get();
                
                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }

        
        

        }
        
        endif;

        return view( 'subcontractor.payrolls.view', compact( 'payrollData', 'previous', 'next' ) );

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    
	    $project = Payroll::where('id', $id)->firstOrFail();

	    $project->delete();

	    Session::flash('success', 'Payroll deleted successfully.');

	    return redirect()->back();

	}

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
     
		$payrollData = Payroll::where('id', $id)->firstOrFail();
		
		if( $payrollData->status )
			return redirect()->back();

        $subContId = Auth::user()->id;

        return view( 'subcontractor.payrolls.edit', compact( 'payrollData', 'subContId' ) );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

		if($request->hasFile('payroll_file')):

		$data['payroll_id'] = $id;
		PayrollFiles::where( 'payroll_id', $id )->delete();
		$data = Excel::import(new PayrollImport($data), $request->file('payroll_file'));

		endif;

    	$subContId = Auth::user()->id;
        
       	$validator = Validator::make($request->all(), [

	        'title' => 'required',

	    ]);

       	if($validator->fails()) {

           $request->flash();
       
           if($validator->errors()->first('title')) {
	    
	            Session::flash('error', 'Please enter week end date.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('description')) {
	    
	            Session::flash('error', 'Please enter payroll description.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('payroll_file')) {
	    
	            Session::flash('error', 'Please upload valid payroll file (CSV or PDF).');
	    
	            return redirect()->back();
	    
			}
			
		   }
		   
		    if( Payroll::where( 'title', $request->title)->where('id', '!=', $id)->where('author', $subContId)->exists() )
			{
				Session::flash('error', 'Payroll already exists for this dates.');
			
				return redirect()->back();
			}

       	/*-------------------------------------------- Seperator --------------------------------------------*/

	    $title = $request->input('title');

	    $description = $request->input('description');

	    $startDate = $request->input('start_date');

	    $endDate = $request->input('end_date');

	    /*-------------------------------------------- Seperator --------------------------------------------*/

       	$payrollData = array(

       	    'title' => $title,

       	    'description' => $description,

       	    'author' => $subContId,

       	    'start_date' => date('Y-m-d H:i:s',  strtotime( $startDate ) ),

	        'end_date' => date('Y-m-d H:i:s', strtotime( $endDate ) ),

       	);

       	/*-------------------------------------------- Image validation --------------------------------------------*/

        if($request->hasFile('payroll_file')) {

            $format_validator = Validator::make($request->all(), [

                'image' => 'mimes:csv,pdf'
    
            ]);
    
            if($format_validator->fails()) {
    
                Session::flash('error', 'Please select image with format CSV, PDF');
    
                return redirect()->back();
    
            }

            $destination = public_path('uploads/images/payroll/'. $subContId);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('payroll_file');

			$image_name = 'payroll-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);

            $payrollData['payroll_file'] = $image_name;

        }

       	/*-------------------------------------------- Image validation --------------------------------------------*/


       $payrollObject = Payroll::find( $id );

       $payrollObject->update( $payrollData );

       Session::flash('success', 'Payroll has been updated successfully.');

       return redirect()->route('subcontractor.payrolls');

    }

}
