<?php

namespace App\Http\Controllers\Subcontractor;

use File;
use Session;
use App\User;
use App\Payroll;
use App\PayrollFiles;
use App\Manpower;
use App\Project;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use QrCode;
use Auth;
use Mail;
use App\Imports\PayrollImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\PayrollNotification;

class ManpowerController extends Controller
{
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request )
	{

	    return view('subcontractor.manpower.index');
	    
	}

	public function logs( $id )
    {
      $p = Project::all();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->get();
      $md = Manpower::find( $id );

      return view('subcontractor.manpower.view')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', $id)
      ->with('s', $s);

    }

    public function logsd()
    {

      $mids = [];

      $r = Manpower::all();

      $d = $_REQUEST['d'];

      $input  = $d;
          
      $format = 'm-d-Y';

      $c = \Carbon\Carbon::createFromFormat($format, $input);
      $cd = \Carbon\Carbon::createFromFormat($format, date('m-d-Y'));


      foreach( $r as $md )
        {

          $mds1 = json_decode( $md->emp_data );

          if( isset( $mds1->sub ) ){
            foreach( $mds1->sub as $k => $g )
            {

              if( Auth::user()->id == $g )
              {

                array_push($mids, $md->id);

              }

            }
          }

          


          if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
                
              foreach( $mds2->sub as $k => $g )
              {

                if( Auth::user()->id == $g )
                {

                  array_push($mids, $md->id);

                }

              }

            }

        }

        $mids = array_unique( $mids );

        $resource = Manpower::whereIn( 'id', array_unique($mids) )->whereDate('job_date','=', $c->toDateString())->get();

       


      $id = 201;

      $p = Project::all();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->get();
      $md = Manpower::find( $id );

      
      return view('subcontractor.manpower.view')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', @$id)
      ->with('l', $resource)
      ->with('s', $s);

    }

    public function logsp( $id )
    {
      $p = Project::all();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->get();
      $md = Manpower::find( $id );

      return view('admin.manpower.view')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', $id)
      ->with('s', $s);

    }

    public function get( Request $request )
    {
        $r = Manpower::all();

        $mids = [];

        foreach( $r as $md )
        {

          $mds1 = json_decode( $md->emp_data );

          foreach( $mds1->sub as $k => $g )
          {

            if( Auth::user()->id == $g )
            {

              array_push($mids, $md->id);

            }

          }


          if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
                
              foreach( $mds2->sub as $k => $g )
              {

                if( Auth::user()->id == $g )
                {

                  array_push($mids, $md->id);

                }

              }

            }

        }

        $resource = Manpower::whereIn( 'id', array_unique($mids) )->get();


        return Datatables::of($resource)
        ->addColumn('project', function($resource)
        {

          return "<a href='".route('subcontractor.manpower.view', $resource->id)."'>".Project::find( $resource->project_id )->job_name."</a>";

        })
        ->addColumn('job_date', function($resource)
        {

          

          return date( 'm-d-Y', strtotime( $resource->job_date ) );

        })
        ->addColumn('action', function($resource)
        {

            $editLink = route('manpower.edit', $resource->id);

            $viewLink = route('subcontractor.manpower.view', $resource->id);

            $deleteLink = route('manpower.delete', $resource->id);

            $deleteHtml = '';


              $editHtml = '<hr>
                                   <div>
                                   <a href="' . $editLink . '" class="btn btn-sm btn-danger btn-circle btn-action-edit" title="Edit">
                                      Edit
                                    </a>
                                  </div>';

              $viewHtml = '<hr>
                                   <div>
                                   <a href="' . $viewLink . '" class="btn btn-sm btn-danger btn-circle btn-action-edit" title="View">
                                      View
                                    </a>
                                  </div>';
           
              $deleteHtml = '<hr>
                                   <div>
                                   <a href="' . $deleteLink . '" class="btn btn-sm btn-circle btn-action-delete" title="Delete">
                                      Delete
                                    </a>
                                  </div>';

              


            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                    '.$viewHtml.'
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['project', 'job_date', 'action'] )
        ->make( true );
    }

    public function getp( $id, Request $request )
    {
        $r = Manpower::all();

        $mids = [];

        foreach( $r as $md )
        {

          if( $md->project_id == $id )
          array_push($mids, $md->project_id);

        }

        $resource = Manpower::whereIn( 'project_id', $mids )->get();


        return Datatables::of($resource)
        ->addColumn('project', function($resource)
        {

          return "<a href='".route('superadmin.manpower.view', $resource->id)."'>".Project::find( $resource->project_id )->job_name."</a>";

        })
        ->addColumn('job_date', function($resource)
        {

          

          return date( 'm-d-Y', strtotime( $resource->job_date ) );

        })
        ->addColumn('action', function($resource)
        {

            $editLink = route('manpower.edit', $resource->id);

            $viewLink = route('subcontractor.manpower.view', $resource->id);

            $deleteLink = route('manpower.delete', $resource->id);

            $deleteHtml = '';


              $editHtml = '<hr>
                                   <div>
                                   <a href="' . $editLink . '" class="btn btn-sm btn-danger btn-circle btn-action-edit" title="Edit">
                                      Edit
                                    </a>
                                  </div>';

              $viewHtml = '<hr>
                                   <div>
                                   <a href="' . $viewLink . '" class="btn btn-sm btn-danger btn-circle btn-action-edit" title="View">
                                      View
                                    </a>
                                  </div>';
           
              $deleteHtml = '<hr>
                                   <div>
                                   <a href="' . $deleteLink . '" class="btn btn-sm btn-circle btn-action-delete" title="Delete">
                                      Delete
                                    </a>
                                  </div>';

              


            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                    '.$viewHtml.'
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['project', 'job_date', 'action'] )
        ->make( true );
    }

    public function create( Request $request )
    {

        $p = Project::all();
        $s = User::role('Subcontractor')->get();
        $w = User::role('Worker')->get();

        return view('superint.manpower.create')
        ->with('p', $p)
        ->with('w', $w)
        ->with('s', $s);

    }

    public function copyPrevious()
    {

      DB::table('temp')->truncate();;

      $ld = Manpower::orderBy('id', 'desc')->first()->toArray();

      $id = Tempdb::create( $ld );

      return redirect( route('manpower.copy.edit', $id) );

    }

    public function edit( $id )
    {

      $p = Project::all();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->get();
      $md = Manpower::find( $id );

      return view('superint.manpower.edit')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', $id)
      ->with('s', $s);

    }

    public function copyEdit( $id )
    {

      $p = Project::all();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->get();
      $md = DB::table('temp')->find( $id );

      return view('superint.manpower.copyedit')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', $id)
      ->with('s', $s);

    }

    public function store( Request $request )
    {
      

        $data = [];

        $data['project_id'] = $request->project_id;
        $data['job_date'] = date( 'Y-m-d H:i:s', strtotime( $request->job_date ) );
        $data['job_time'] = $request->job_time;
        $data['emp_data'] = json_encode($request->shift1);

        if( isset( $request->isshift ) )
          $data['emp_data_2'] = json_encode($request->shift2);

        Manpower::create( $data );

        Session::flash('success', 'Schedule has been added successfully.');

        return redirect( route('manpower') );

    }

    public function update( Request $request )
    {

      $data = [];

        $data['project_id'] = $request->project_id;
        $data['job_date'] = date( 'Y-m-d H:i:s', strtotime( $request->job_date ) );
        $data['job_time'] = $request->job_time;
        $data['emp_data'] = json_encode($request->shift1);

        $data['emp_data_2'] = NULL;

        if( isset( $request->isshift ) )
          $data['emp_data_2'] = json_encode($request->shift2);

        Manpower::find( $request->id )->update( $data );

        Session::flash('success', 'Schedule has been updated successfully.');

        return redirect( route('manpower') );

    }

    public function destroy( $id )
    {

        Manpower::find( $id )->delete();

        Session::flash('success', 'Schedule has been deleted successfully.');

        return redirect( route('manpower') );

    }

}
