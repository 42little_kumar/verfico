<?php

namespace App\Http\Controllers\subcontractor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\User;
use App\WorkerType;
use App\Payroll;
use App\WorkLog;
use App\Pages;
use Fpdf;
use Auth;

class DashboardController extends Controller
{
    
    public function index()
    {

        $subContId = Auth::user()->id;

        $subUsers = User::where( 'parent', $subContId )->pluck('id')->toArray();

        $projectsIds = WorkLog::whereIn('worker_id', $subUsers)->pluck('project_id')->toArray();

        $totalProjects = Project::whereIn( 'id', $projectsIds )->count();

    	$totalActiveProjects = Project::whereIn( 'id', $projectsIds )->where( 'status', 'active' )->count();

    	$totalWorkers = User::role('worker')->where( 'parent', $subContId )->where( 'status', '!=', 1 )->count();

        $totalPayroll = Payroll::where( 'author', Auth::user()->id )->count();

        $billboard = @Pages::where( 'name', 'billboard' )->first()->content;

        $userType = 'worker';

        $userTitle = 'Worker';

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

        }
        
        $users = User::paginate( 20 );

        $titlesList = WorkerType::get();

        $companiesList = User::role('subcontractor')->get();

    	$data = compact(

    		'totalProjects',

            'totalActiveProjects',

    		'totalWorkers',

            'users',

            'userType',

            'userTitle',

            'titlesList',

            'companiesList',

            'totalPayroll',

            'billboard'

    	);

        return view( 'subcontractor.dashboard.index', $data );

    }

}