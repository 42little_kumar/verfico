<?php

namespace App\Http\Controllers\Subcontractor;

use File;
use Session;
use App\User;
use App\Project;
use App\WorkerType;
use App\Strike;
use App\WorkerDocuments;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $users = User::role('worker')->paginate( 20 );

        $titlesList = WorkerType::get();

        $subId = Auth::user()->id;

        return view('subcontractor.workers.index', compact( 'users', 'titlesList', 'subId' ));
        
    }

    /*
     * @Datatable get
     *
     *
     */
    public function getUsers( Request $request )
    {

        $userType = '';

        $subContId = Auth::user()->id;

        $userObject = User::where( 'parent', $subContId )->where('role', 'worker');

        if( isset( $request->userType ) && !empty( $request->userType ) )
        {

            $userType = $request->userType;

            $users = User::where('role', $userType);

            $users = $users->get();
            
        }

        // Status filter

        if( isset( $request->status ) )
        {

            $userObject->where( 'status', $request->status );

        }

        // Title filter

        if( isset( $request->title ) )
        {

            $userObject->where( 'worker_type', $request->title );

        }

        $users = $userObject
        ->where( 'status', '!=', 1 )
        ->get();

        return Datatables::of($users)
        ->addColumn('id', function($user)
        {

          return $user->id;

        })
        ->addColumn('name', function($user)
        {

            $viewLink = route('subcontractor.workers.view', [ 'id' => $user->id ]);

            $userName = '<div class="worker_name_td lc-id"><p><a class="index-link" href="'.$viewLink.'">'.$user->first_name.' '.$user->last_name.'</a></p></div>';

            return $userName;

        })
        ->addColumn('email', function($user)
        {
            
           return $user->email;

        })
        ->addColumn('ssn', function($user)
        {
            
            return substr($user->ssn_id, -4);

        })
        ->addColumn('worker_type', function($user)
        {

          return empty( $user->worker_type_name ) ? '<i>Not Selected</i>' : ucwords( $user->worker_type_name );

        })
        ->addColumn('phone_number', function($user)
        {
            
            return !empty( $user->phone_number ) ? $user->phone_number : '';

        })
        ->addColumn('created_at', function($user)
        {
            
            return \Carbon\Carbon::parse($user->created_at)->tz(str_before(User::role('superadmin')->first()->timezone_manual, ','))->format(("m-d-Y"));

        })
        ->addColumn('total_strike', function($user)
        {
            
            $total_strikes = '';

            if( Strike::where( 'worker_id', $user->id )->exists() )
            $total_strikes = Strike::where( 'worker_id', $user->id )->count();

            return $total_strikes;

        })
        ->addColumn('total_strike_30', function($user)
        {
            
            $total_strikes_30 = '';

            if( Strike::where( 'worker_id', $user->id )->exists() ) 
            $total_strikes_30 = Strike::where( 'worker_id', $user->id )->whereDate('created_at', '>', Carbon::now()->subDays(30))->count();

            return $total_strikes_30;

        })
        ->addColumn('last_strike_date', function($user)
        {
            
            $last_strike_date = '';

            if( Strike::where( 'worker_id', $user->id )->exists() )
            $last_strike_date = date('m/d/Y', strtotime( Strike::where( 'worker_id', $user->id )->orderBy('id', 'desc')->first()->strike_date ) );

            return $last_strike_date;

        })
        ->addColumn('safety_training', function($user)
        {

            $return = '';

            if( !empty( $user->training ) )
            $return = date('m/d/Y', strtotime( $user->training ));

            return $return;

        })
        ->addColumn('last_worked', function($user)
        {

            $return = '';
            
            $lastWorked = DB::table('worker_log')->where('worker_id', $user->id)->orderBy('id', 'desc')->first();

            if( $lastWorked )
            { 
                $lastWorked = $lastWorked->punch_at;

                $return = date('m/d/Y', strtotime( $lastWorked ));
            }

            return $return;

        })
        ->addColumn('status', function($user)
        {

            switch ( $user->status ) {
                case 0:
                    $html = '<span class="status_active action-block">Blocked</span>';
                    break;
                case 1:
                    $html = '<span class="status_active action-pending">Pending</span>';
                    break;
                default:
                    $html = '<span class="status_active action-active">Active</span>';
                    break;
            }

            return $html;
            
        })
        ->addColumn('action', function($user)
        {

            $viewLink = route('subcontractor.workers.view', [ 'id' => $user->id ]);

            $deleteLink = route('subcontractor.workers.delete', [ 'id' => $user->id ]);

            $editLink = route('subcontractor.workers.edit', [ 'id' => $user->id ]);

            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                  <div>
                                     <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle lc-id" title="View">
    
                                           View
    
                                      </a>
                                  </div> 

                                   <div>
                                     <a href="' . $editLink . '" class="btn btn-sm btn-primary btn-circle lc-id" title="Edit">
                                       Edit
                                    </a>
                                  </div>
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['id', 'name', 'email', 'worker_type', 'status', 'action', 'last_worked', 'safety_training'] )
        ->make( true );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $subContId = Auth::user()->id;

        $workerTypes = WorkerType::get();

        $data = compact( 

            'subContId',

            'workerTypes'

         );

        return view( 'subcontractor.workers.create', $data );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $parent = 0;

        $validator = Validator::make($request->all(), [

            'first-name' => 'required',

            'email' => 'required',

            'password' => 'required',

        ]);

        if($validator->fails()) {

            $request->flash();
        
            if($validator->errors()->first('first-name')) {
        
                Session::flash('error', 'Please enter first name of user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('email')) {
        
                Session::flash('error', 'Please enter email of user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('password')) {
        
                Session::flash('error', 'Please enter password for the user.');
        
                return redirect()->back();
        
            }

            if($validator->errors()->first('username')) {
        
                Session::flash('error', 'Please enter username of user.');
        
                return redirect()->back();
        
            }
        
        }

        $additional_validator = Validator::make($request->all(), [

            'email' => 'email',

            'password' => [ 'min:8', 'max:18' ]

        ]);

        if($additional_validator->fails()) {

            $request->flash();

            if($additional_validator->errors()->first('email')) {
        
                Session::flash('error', 'Please enter valid email address of user.');
        
                return redirect()->back();
        
            }

            if($additional_validator->errors()->first('password')) {
        
                Session::flash('error', 'Password should be of atleast 8 characters and not more than 18 characters.');
        
                return redirect()->back();
        
            }
        
        }

        $first_name = $request->input('first-name');

        $last_name = $request->input('last-name');

        $worker_type = $request->input('worker_type');

        $email = strtolower($request->input('email'));

        $username = rand();

        $password = Hash::make($request->input('password'));

        $phone_number = null;

        $country_code = null;

        if( $request->filled('phone-number')) {

            $country_code = null;

            $phone_number = preg_replace('/[^A-Za-z0-9]/', '', $request->input('phone-number'));

        }

        $gender = $request->input('gender');

        if(User::where('email', $email)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Email already linked with another user. Please try again with different information.');

            return redirect()->back();

        }

        if($username && User::where('username', $username)->withTrashed()->first()) {

            $request->flash();

            Session::flash('error', 'Username already linked with another user. Please try again with different information.');

            return redirect()->back();

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        if( isset( $request->general_contractor ) && !empty( $request->general_contractor ) )
        {

            if( isset( $request->user_role ) && $request->user_role == 'worker' )
            {
                
                $userRole = $request->input('user_role');

                $parent = $request->general_contractor;

            }

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $user = User::create([

            'username' => $username,

            'first_name' => $first_name,

            'last_name' => $last_name,

            'worker_type' => $worker_type,

            'parent' => $parent,

            'email' => $email,

            'password' => $password
            
        ]);

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $userRole = 'worker';
    
        if( isset( $request->user_role ) )
        {
            
            $userRole = $request->input('user_role');

        }
       
        $user->assignRole( $userRole );

        /*-------------------------------------------- Seperator --------------------------------------------*/

        $user->phone_number = $phone_number;
        
        $user->country_code = $country_code;

        $user->gender = $gender;

        $user->email_verified_at = Carbon::now();

        $user->phone_number_verified_at = $phone_number ? Carbon::now() : null;

        if($request->hasFile('image')) {

            $format_validator = Validator::make($request->all(), [

                'image' => 'mimes:jpeg,png,jpg,gif,svg'
    
            ]);
    
            if($format_validator->fails()) {

                $request->flash();
    
                Session::flash('error', 'Please select image with format JPEG, JPG, GIF, SVG or PNG to upload');
    
                return redirect()->back();
    
            }

            $destination = public_path('uploads/images/users/'. $user->id);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('image');

			$image_name = 'profile-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);
            
            $user->image = $image_name;

        }

        $user->save();

        Session::flash('success', 'New user has been created successfully.');

        Session::flash('status-user', $user->id);

        $redirectQuery = 'userType='.$userRole;

        return redirect()->route('subcontractor.workers', $redirectQuery);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showDocuments()
    {

        $subId = Auth::user()->id;

        $user = User::find( $subId );

        $documents = $user->documents;

        return view( 'subcontractor.documents.index', compact( 'documents', 'subId' ) );    

    }

    /**
     * Save worker documents.
     *
     * 
     * 
     */
    public function saveWorkerDocuments( $request, $workerId )
    {

        $workerDocument = new WorkerDocuments;

        foreach( $request->notes_training as $key => $value)
        {

            $documentName = null;

            $workerDocument = new WorkerDocuments;

            if( @$request->hasFile( 'worker_document' ) ):

            /* Upload */

            $destination = public_path('uploads/documents/'. $workerId);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $documentName = 'document-' . time() . '-' . Str::random(15) . '.' . @$request->worker_document[$key]->getClientOriginalExtension();

            @$request->worker_document[$key]->move($destination, $documentName);

            /* Upload */

            $workerDocument->user_id = $workerId;
            $workerDocument->author = Auth::user()->id;
            $workerDocument->notes = $value;
            $workerDocument->document = $documentName;
            $workerDocument->show = ( isset( $request->doc_upload[$key] ) ) ? $request->doc_upload[$key] : 0;

            $workerDocument->save();

            endif;

            

        }

    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $user = User::role('worker')->with('roles')->where('id', $id)->firstOrFail();

        $logs = $user->logs()->latest()->get();

        $roles = json_decode( $user->roles );

        $userRole = $roles[0]->name;

        if( $userRole == 'Worker' && isset( $_GET["dir"] ) ):
            
		
            if(@$_GET["column"]==2){
				

				 
                $usersobj = User::role('worker')->whereNull('deleted_at')->where( 'parent', Auth::user()->id )->orderBy('phone_number',@$_GET["dir"])->orderBy('users.first_name','desc');
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }
                
                $users = $usersobj->get();

          

                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        if(@$_GET["column"]==0){
            
                
             
                $usersobj = User::role('worker')->whereNull('deleted_at')->where( 'parent', Auth::user()->id )->orderBy('first_name',@$_GET["dir"]);
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }

                $users = $usersobj->get();
                
                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        
        if(@$_GET["column"]==6){
            
            
             
            $usersobj = User::role('worker')->whereNull('deleted_at')->where( 'parent', Auth::user()->id )->orderBy('created_at',@$_GET["dir"]);

            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }

            if( @$_GET["company"] != 'null' )
            {
                $usersobj->where( 'parent', @$_GET["company"] );
            }

            if( @$_GET["title"] != 'null' )
            {

                $usersobj->where( 'worker_type', @$_GET["title"] );
            }

            $users = $usersobj->get();
            
            $index = null;
            $i=0;
            foreach($users as $user){
                if($user->id == $id){
                    $index = $i;
                    break;
                }
                $i++;
            }
            if(isset($users[$i+1])){
                $next = $users[$i+1];
            }
            if(isset($users[$i-1])){
                $previous = $users[$i-1];
            }
            
            
        
    }

    
    if(@$_GET["column"]==3){
            

       $usersobj = User::role('worker')->where('status', '2')->where( 'parent', Auth::user()->id )->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"])->where('id', 'parent');

       if( @$_GET["status"] != 'null' )
        {
            $usersobj->where( 'status', @$_GET["status"] );
        }

        if( @$_GET["company"] != 'null' )
        {
            $usersobj->where( 'parent', @$_GET["company"] );
        }

        if( @$_GET["title"] != 'null' )
        {

            $usersobj->where( 'worker_type', @$_GET["title"] );
        }

        $users = $usersobj->get();
        
        $index = null;
        $i=0;
        foreach($users as $user){
            if($user->id == $id){
                $index = $i;
                break;
            }
            $i++;
        }
        if(isset($users[$i+1])){
            $next = $users[$i+1];
        }
        if(isset($users[$i-1])){
            $previous = $users[$i-1];
        }
        
        
    
}
            
        endif;
        

        return view( 'subcontractor.workers.view', compact( 'user', 'logs', 'userRole' ) )->with('previous1', @$previous)->with('next1', @$next);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $subContractors = User::role('subcontractor')->get();

        $workerTypes = WorkerType::get();
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::with('roles')->where('id', $id)->firstOrFail();

        $roles = json_decode( $user->roles );

        $userRole = $roles[0]->name;
        
        //$countries = CountryCode::orderBy('name', 'ASC')->get();

        if( $userRole == 'Worker' && isset( $_GET["dir"] ) ):
		
            if(@$_GET["column"]==2){
				

				 
                $usersobj = User::role('worker')->whereNull('deleted_at')->where( 'parent', Auth::user()->id )->orderBy('phone_number',@$_GET["dir"]);
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }
                
                $users = $usersobj->get();

                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        if(@$_GET["column"]==0){
            
            
             
                $usersobj = User::role('worker')->whereNull('deleted_at')->where( 'parent', Auth::user()->id )->orderBy('first_name',@$_GET["dir"]);
                
                if( @$_GET["status"] != 'null' )
                {
                    $usersobj->where( 'status', @$_GET["status"] );
                }

                if( @$_GET["company"] != 'null' )
                {
                    $usersobj->where( 'parent', @$_GET["company"] );
                }

                if( @$_GET["title"] != 'null' )
                {

                    $usersobj->where( 'worker_type', @$_GET["title"] );
                }

                $users = $usersobj->get();
                
                $index = null;
                $i=0;
                foreach($users as $user){
                    if($user->id == $id){
                        $index = $i;
                        break;
                    }
                    $i++;
                }
                if(isset($users[$i+1])){
                    $next = $users[$i+1];
                }
                if(isset($users[$i-1])){
                    $previous = $users[$i-1];
                }
                
                
            
        }
        
        if(@$_GET["column"]==6){
            
            
             
            $usersobj = User::role('worker')->whereNull('deleted_at')->where( 'parent', Auth::user()->id )->orderBy('created_at',@$_GET["dir"]);

            if( @$_GET["status"] != 'null' )
            {
                $usersobj->where( 'status', @$_GET["status"] );
            }

            if( @$_GET["company"] != 'null' )
            {
                $usersobj->where( 'parent', @$_GET["company"] );
            }

            if( @$_GET["title"] != 'null' )
            {

                $usersobj->where( 'worker_type', @$_GET["title"] );
            }

            $users = $usersobj->get();
            
            $index = null;
            $i=0;
            foreach($users as $user){
                if($user->id == $id){
                    $index = $i;
                    break;
                }
                $i++;
            }
            if(isset($users[$i+1])){
                $next = $users[$i+1];
            }
            if(isset($users[$i-1])){
                $previous = $users[$i-1];
            }
            
            
        
    }

    
    if(@$_GET["column"]==3){
            

       $usersobj = User::role('worker')->where('status', '2')->where( 'parent', Auth::user()->id )->whereNull('deleted_at')->orderBy('first_name',@$_GET["dir"])->where('id', 'parent');

       if( @$_GET["status"] != 'null' )
        {
            $usersobj->where( 'status', @$_GET["status"] );
        }

        if( @$_GET["company"] != 'null' )
        {
            $usersobj->where( 'parent', @$_GET["company"] );
        }

        if( @$_GET["title"] != 'null' )
        {

            $usersobj->where( 'worker_type', @$_GET["title"] );
        }

        $users = $usersobj->get();
        
        $index = null;
        $i=0;
        foreach($users as $user){
            if($user->id == $id){
                $index = $i;
                break;
            }
            $i++;
        }
        if(isset($users[$i+1])){
            $next = $users[$i+1];
        }
        if(isset($users[$i-1])){
            $previous = $users[$i-1];
        }
        
        
    
}
            
        endif;

        return view( 'subcontractor.workers.edit', compact( 'user', 'userRole', 'subContractors', 'workerTypes','previous','next' ) );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // $user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::where('id', $id)->firstOrFail();

        $training = null;

        if( $request->filled('training') )
        {

            $training = $request->input('training');

            $training = date('Y-m-d H:i:s', strtotime($training));

        }

        $status = $request->input('status');

        $worker_type = $request->input('worker_type');

        $user->first_name = $request->input('first-name');

        $user->last_name = $request->input('last-name');

        $user->updated_by = Auth::user()->id;

        $user->training = $training;

        $finalSsd = $request->input('ssn_id_pre').$request->input('ssn_id');

        $lastNameSsn = explode(' ', $request->input('last-name'));

        $finalSsd .= strtoupper(substr(end($lastNameSsn), 0, 4));

        $finalSsd = substr($finalSsd,0,12); 
        
        $user->ssn_id = $finalSsd;

        $user->name = $request->input('first-name').' '.$request->input('last-name');

        $user->worker_type = $worker_type;

        $user->role = ($worker_type == 9) ? "safetyo" : "worker";
        
        $user->status = $status;

        

        /* Worker document */

        $this->saveWorkerDocuments( $request, $user->id );

        /* Worker document */

        $user->save();

        Session::flash('success', 'User information has been updated successfully.');

        Session::flash('status-user', $user->id);

        return redirect()->back();
        //return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //$user = User::role('user')->where('id', $id)->firstOrFail();
        $user = User::where('id', $id)->firstOrFail();

        $user->delete();

        Session::flash('success', 'Worker has been deleted successfully.');

        //return redirect()->route('admin.users');

        return redirect()->back();

    }

    public function remove($id)
    {

        //$user = User::role('user')->where('id', $id)->firstorFail();
        $user = User::where('id', $id)->firstorFail();

        $user->image = null;

        $user->save();

        Session::flash('success', 'User image has been removed successfully.');

        return redirect()->back();

    }

}
