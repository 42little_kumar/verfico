<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\PunchRequest;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use DB;
use App\WorkLog;
use App\Project;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use Carbon\Carbon;
use Illuminate\Support\Arr;


class PunchController extends ApiBaseController
{
	/**
     * Display a listing of worker log in/out time.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/project/punch",
     *     tags={"Punch In & Out"},
     *     summary="listing log in/out for workers.",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="worker log in/out fetched successfully."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function index()
    {
        try{
            $log = WorkLog::whereWorkerId(Auth::user()->id)->orderBy('punch_at', 'DESC')->whereBetween('created_at',[Carbon::now()->subDays(6),Carbon::now()])->with('project')->get();

            $userData = User::find( Auth::user()->id );

            $data['punches'] = $log;

            $data['is_deleted'] = 0;

            $data['is_blocked'] = 0;

            $message = trans('messages.PUNCH_LIST');

            if( $userData->deleted_at != NULL )
            {
                $data['is_deleted'] = 1;
                $message = trans('messages.IS_DELETED');
            }
            

            if( $userData->status == 0 )
            {
                $data['is_blocked'] = 1;
                $message = trans('messages.IS_BLOCKED');
            }

            return $this->success($data , $message );
        }catch(Excetion $e){
            return $this->error( [], trans('messages.EXECUTION_ERROR') );
        }
    }

    /* ---------------------------------------------------------------- */

    /**
     * Display a listing of worker log in/out time.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/getscheduleforadmin",
     *     tags={"Punch In & Out"},
     *     summary="listing log in/out for workers.",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="project_id",
     *         in="query",
     *         description="Project ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="date",
     *         in="query",
     *         description="m-d-y (7-30-2020)",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Get schedule of this day."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function getscheduleforadmin(Request $request){

      try{

        $date  = $request->date;
        
        $project_id  = $request->project_id;

        $format = 'm-d-Y';

        $requested_date = \Carbon\Carbon::createFromFormat($format, $date);
        
        $current_date = \Carbon\Carbon::createFromFormat($format, date('m-d-Y'));

        $manpower = \App\Manpower::where( 'job_date','=', $requested_date->toDateString() )
          ->where( 'project_id', $project_id )
          ->first();

        $data = [];

        $schedule_data = [];
    
    $start_time = null;
    
    $end_time = null;

        if($manpower){

          $emp_data = json_decode($manpower->emp_data, true);

          $emp_data_2 = json_decode($manpower->emp_data_2, true);

          $shifts = [];

          $shifts[] = $emp_data;

          $shifts[] = $emp_data_2;

          $start_time = null;

          $end_time = null;

          $start_time = str_replace(" : ", ":", $manpower->job_time);


          if($manpower->job_2_time!='' && $manpower->job_2_time!=null){
            $end_time = str_replace(" : ", ":", $manpower->job_2_time);  

            
          }
      //echo $end_time; die; 
      //print_r($shifts); die;
          foreach ($shifts as $shift_index => $shift) {
            
            $subcontractor_index = [];
      
      if(!isset($shift["sub"])){
        break;
      }
                        
            foreach ($shift['sub'] as $index => $sub_contractor) {

              if(!isset($subcontractor_index[$sub_contractor])){
                $subcontractor_index[$sub_contractor] = 0;
              } else {
                $subcontractor_index[$sub_contractor]++;
              }
              if($sub_contractor==624){

                $worker = $shift['worker'][$index];  
                $jobType = $shift['jobtype'][$index]; 

                $job_type = $shift['jobtype'][$index];
  
                $workers = User::orderBy('id', 'desc')->where('id', $worker)->get();

                foreach ($workers as $worker) {
                  
                  $log = [];

                  $subContractor = User::role('subcontractor')->where('id', $sub_contractor)->first();

                  if($shift_index==0){
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_time);  
                  } else {
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_2_time);
                  }
                  $log['shift_no'] = $shift_index + 1;
                  $log['sub_id'] = $sub_contractor;
                  $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
                  $log['sub_code'] = strtoupper( substr(str_replace(' ', '', $subContractor->company_name), 0, 4) );                     
                  $log['worker_id'] = $worker->id;
                  if(!empty($request->classification)){
                    $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                    $log['worker_name'] = $worker->first_name.' '.$worker->last_name.$classification;
                  }else{
                    $log['worker_name'] = $worker->first_name.' '.$worker->last_name;
                  }

                  $worker_type = \DB::table('worker_types')->where('id', $worker->worker_type)->first();
                  $log['worker_type'] = @$worker_type->type;
                  $log['work_type'] = $jobType;
                  $log['worker_type_id'] = @$worker_type->id; 
                  $log['is_rock'] = 'true';
                  $log['punch_id'] = null;
                  $log['punch_at'] = null;
                  $log['punch_in'] = null;
                  $log['punch_out'] = null;
                  $log['in_type'] = null;
                  $log['out_type'] = null;
                  $log['occupied'] = false;

                  $data[] = $log;
                }  

              } else {
                
                $worker = $shift['worker'][$index]; 
                $jobType = $shift['jobtype'][$index]; 
                
                $worker_type_name = @$shift['a_workertype_name'][$sub_contractor][$subcontractor_index[$sub_contractor]];

                $worker_type_number = @$shift['a_workertype_number'][$sub_contractor][$subcontractor_index[$sub_contractor]];

                $worker_type_count = intval($worker_type_number);

                $worker_type  = $worker_type_name;

                $worker_type_or = $worker_type;

                $job_type = $shift['jobtype'][$index];

                for($i=0; $i < $worker_type_count; $i++){
                  $log = [];

                  $subContractor = User::role('subcontractor')->where('id', $sub_contractor)->first();

                  if($shift_index==0){
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_time);  
                  } else {
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_2_time);
                  }
                  
                  $log['shift_no'] = $shift_index + 1;
                  $log['sub_id'] = $sub_contractor;
                  $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
                  $log['sub_code'] = strtoupper( substr(str_replace(' ', '', $subContractor->company_name), 0, 4) );                     
                  $log['worker_id'] = null;
                  $log['worker_name'] = null;
                  //echo $worker_type."==";
                  $worker_type_obj = \DB::table('worker_types')->where('id', $worker_type)->first();
                  $log['worker_type'] = @$worker_type_obj->type; 
                  $log['worker_type_id'] = @$worker_type_obj->id;  
                  $log['work_type'] = $jobType;
                  $log['is_rock'] = 'false';
                  $log['punch_id'] = null;
                  $log['punch_at'] = null;
                  $log['punch_in'] = null;
                  $log['punch_out'] = null;
                  $log['in_type'] = null;
                  $log['out_type'] = null;
                  $log['occupied'] = false;

                  if( @$worker_type_obj->type != null && @$worker_type_obj->id != null )
                  $data[] = $log;
                }
              }
            }
          }  
          
        }
        $schedule_data = $data;

        //$new_data = [];

        $worker_log = \DB::table('worker_log')->where('project_id', $project_id)->whereDate('punch_at',"<=",  date("Y-m-d",strtotime($requested_date->format("Y-m-d")) + 24*60*60)  )->whereDate('punch_at',">=",  date("Y-m-d",strtotime($requested_date->format("Y-m-d"))-24*60*60) )->orderBy("punch_at","asc")->get();


        //$worker_log = \DB::table('worker_log')->where('project_id1', $project_id)->whereRaw(" DATE_FORMAT('%Y-%m-%d', CONVERT_TZ(punch_at,'+00:00','-04:00'))='".date("Y-m-d",strtotime($manpower->job_date))."'" )->orderBy("punch_at","asc")->get();
        
        //prin
        //print_r($worker_log);

        //echo $manpower->job_date;

        $new_worker_log = [];

        foreach ($worker_log as $log) {
          
          list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

          $punchIn = Carbon::createFromFormat('Y-m-d H:i:s',$log->punch_at, "UTC" ) ->setTimezone($timezone);

          if($punchIn->format("Y-m-d") == str_replace(" 00:00:00", "", $requested_date->format("Y-m-d"))){
            $new_worker_log[] = $log;
          }
        }
        $worker_log = $new_worker_log;

        //print_r($worker_log);
        $shift_logs = [];

        foreach ($worker_log as $punchlog) {

          list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

          $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $punchlog->punch_in, "UTC")->setTimezone($timezone);

          $punch_in = date( 'H:i:s', strtotime( $formatedPunchInTime ) );

          if( $punchlog->punch_out!=null){
            $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $punchlog->punch_out, "UTC")->setTimezone($timezone);

            $punch_out = date( 'H:i:s', strtotime( $formatedPunchInTime ) );
          } else {
            $punch_out = null;
          }
          /*if($punchlog->worker_id==836){
            echo $punch_in."==";
            echo strtotime($punch_in);
            echo "==";
            echo strtotime($end_time);
          }*/
          if($end_time==null){
            $shift_logs[$punchlog->worker_id]["shift1"][] = $punchlog;
            $shift_logs[$punchlog->worker_id]["shift1_done"] = false;
          }
          else if($end_time!=null && ( strtotime($punch_in) >= strtotime($end_time) || ($punch_out!=null && strtotime($punch_in) >= strtotime($end_time) - 30*60 &&  strtotime($punch_out) > strtotime($end_time) ) )){
            //echo 111;
            $shift_logs[$punchlog->worker_id]["shift2"][] = $punchlog;
            $shift_logs[$punchlog->worker_id]["shift2_done"] = false;

          } else {
            //echo 11112323;
            $shift_logs[$punchlog->worker_id]["shift1"][] = $punchlog;
            $shift_logs[$punchlog->worker_id]["shift1_done"] = false;
          }
          
        }
        //print_r($shift_logs);
        $data = [];

        
        for ($i=0 ; $i<sizeof($schedule_data); $i++) {
          
          

          

           
          foreach ($shift_logs as $worker_key => $shifts) {

            $worker = User::orderBy('id', 'desc')->where('id', $worker_key)->first();  
                            
            $worker_type = @\DB::table('worker_types')->where('id', $worker->worker_type)->first();

            $subcontractor = @User::where("id", $worker->parent)->first();
            
            $punchlog_matched = false;
            
            $schedule = $schedule_data[$i];

            if(@$worker->parent == $schedule["sub_id"]){



              $first_shift =   $schedule["shift_no"] == 1 ? (isset($shifts["shift1"]) ? $shifts["shift1"][0] : null) : (isset($shifts["shift2"]) ? $shifts["shift2"][0] : null); 

              $other_shifts =   $schedule["shift_no"] == 1 ? (isset($shifts["shift1"]) ? $shifts["shift1"] : null) : (isset($shifts["shift2"]) ? $shifts["shift2"] : null); 

              $shift_done_key = $schedule["shift_no"] == 1 ? 'shift1_done' : 'shift2_done'; 
              
              if($first_shift==null){
                continue;
              }
              if( $schedule["is_rock"]=='true'){
                
                if($first_shift->worker_id == $schedule['worker_id']){

                  if($schedule_data[$i]["occupied"]==false && !$shift_logs[$worker_key][$shift_done_key]){

                    $shift_logs[$worker_key][$shift_done_key] = true;

                    $schedule_data[$i]["occupied"] = true;

                    $schedule_data[$i]["occupied"] = true;

                    $punchlog_matched = true;

                    $temp_data = $schedule_data[$i];
                    $temp_data['punch_id'] = $first_shift->id;
                    $temp_data['punch_at'] = $first_shift->punch_at;
                    $temp_data['punch_in'] = $this->gt($first_shift->punch_in);
                    $temp_data['punch_out'] = $this->gt($first_shift->punch_out);
                    $temp_data['in_type'] = $first_shift->in_type;
                    $temp_data['out_type'] = $first_shift->out_type;
                    
                    $data[] = $temp_data;

                    for($m=1; $m<sizeof($other_shifts);$m++){

                      $punchlog = $other_shifts[$m];
                      $temp_data = $schedule_data[$i];
                      $temp_data['punch_id'] = $punchlog->id;
                      $temp_data['punch_at'] = $punchlog->punch_at;
                      $temp_data['punch_in'] = $this->gt($punchlog->punch_in);
                      $temp_data['punch_out'] = $this->gt($punchlog->punch_out);
                      $temp_data['in_type'] = $punchlog->in_type;
                      $temp_data['out_type'] = $punchlog->out_type;
                      
                      $data[] = $temp_data;
                    }
                  }

                }





              } else {

                if($worker_type->id == $schedule['worker_type_id']){
                 
                  if($schedule_data[$i]["occupied"]==false && !$shift_logs[$worker_key][$shift_done_key]){

                    $shift_logs[$worker_key][$shift_done_key] = true;



                    $schedule_data[$i]["occupied"] = true;
                    $punchlog_matched = true;
                    $temp_data = $schedule_data[$i];
                    $temp_data['worker_id'] = $punchlog->worker_id;
                    if(!empty($request->classification)){
                      $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                      $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name.$classification;
                    }else{
                      $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name;
                    }

                          
                    $temp_data['punch_id'] = $first_shift->id;
                    $temp_data['punch_at'] = $first_shift->punch_at;
                    $temp_data['punch_in'] = $this->gt($first_shift->punch_in);
                    $temp_data['punch_out'] = $this->gt($first_shift->punch_out);
                    $temp_data['in_type'] = $first_shift->in_type;
                    $temp_data['out_type'] = $first_shift->out_type;
                    


                    $data[] = $temp_data;

                    

                    //print_r($data);

                    for($m=1; $m<sizeof($other_shifts);$m++){

                      $punchlog = $other_shifts[$m];

                      
                      $temp_data = $schedule_data[$i];
                      $temp_data['worker_id'] = $punchlog->worker_id;
                      if(!empty($request->classification)){
                        $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                        $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name.$classification;
                      }else{
                        $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name;
                      }

                      $temp_data['work_type'] = 'hereworktype2';
                          
                      $temp_data['punch_id'] = $punchlog->id;
                      $temp_data['punch_at'] = $punchlog->punch_at;
                      $temp_data['punch_in'] = $this->gt($punchlog->punch_in);
                      $temp_data['punch_out'] = $this->gt($punchlog->punch_out);
                      $temp_data['in_type'] = $punchlog->in_type;
                      $temp_data['out_type'] = $punchlog->out_type;
                      
                      $data[] = $temp_data;
                    }
                  }

                }

              }


            }
          }
          
        }  
        foreach ($schedule_data as $schedule) {
          if(!$schedule["occupied"]){
            $data[] = $schedule;
          }    
        }  
        foreach ($shift_logs as $worker_key => $shifts) {
          
          $worker = User::orderBy('id', 'desc')->where('id', $worker_key)->first();  
                            
          $worker_type = @\DB::table('worker_types')->where('id', $worker->worker_type)->first();

          $subcontractor = @User::where("id", $worker->parent)->first();
            
//print_r($shifts);

//print_r($shifts["shift1_done"]);

          if(isset($shifts["shift1_done"]) && !$shifts["shift1_done"]){
            
            foreach ($shifts["shift1"] as $shift) {
              
              //print_r($shift);
              $log = [];
              $log['shift_time'] = "";
              $log['shift_no'] = "";
              $log['sub_id'] = @$subcontractor->id;
              $log['sub_name'] = @explode(' ', $subcontractor->company_name)[0];
              $log['sub_code'] = strtoupper( substr(str_replace(' ', '', @$subcontractor->company_name), 0, 4) );                     
              if(!empty($request->classification)){
                $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name.$classification;
              }else{
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name;
              }

              $log['worker_id'] = @$worker->id;
              $worker_type_obj = \DB::table('worker_types')->where('id', $worker->worker_type)->first();
              $log['worker_type'] = @$worker_type_obj->type;  
              $log['work_type'] = '';  

              $log['is_rock'] = 'false';
              if(@$subcontractor->id==624){
                $log['is_rock'] = 'true';
              }
              
              $log['punch_id'] = $shift->id;
              $log['punch_at'] = $shift->punch_at;
              $log['punch_in'] = $this->gt($shift->punch_in);
              $log['punch_out'] = $this->gt($shift->punch_out);
              $log['in_type'] = $shift->in_type;
              $log['out_type'] = $shift->out_type;

              $data[] = $log;
            }
          }
          if(isset($shifts["shift2_done"]) && !$shifts["shift2_done"]){
            foreach ($shifts["shift2"] as $shift) {
              
              $subcontractor = User::where("id", $worker->parent)->first();
            
              $log = [];
              $log['shift_time'] = "";
              $log['shift_no'] = "";
              $log['sub_id'] = @$subcontractor->id;
              $log['sub_name'] = @explode(' ', $subcontractor->company_name)[0];
              $log['sub_code'] = strtoupper( substr(str_replace(' ', '', @$subcontractor->company_name), 0, 4) );                     
              if(!empty($request->classification)){
                $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name.$classification;
              }else{
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name;
              }

              $log['worker_id'] = @$worker->id;
              $worker_type_obj = \DB::table('worker_types')->where('id', $worker->worker_type)->first();
              $log['worker_type'] = @$worker_type_obj->type;  
              $log['work_type'] = '';  
              $log['is_rock'] = 'false';
              if(@$subcontractor->id==624){
                $log['is_rock'] = 'true';
              }
              
              $log['punch_id'] = $shift->id;
              $log['punch_at'] = $shift->punch_at;
              $log['punch_in'] = $this->gt($shift->punch_in);
              $log['punch_out'] = $this->gt($shift->punch_out);
              $log['in_type'] = $shift->in_type;
              $log['out_type'] = $shift->out_type;

              $data[] = $log;
            }
          }
        } 
        usort($data, $this->sortByDateAdmin('punch_at'));

        $not_null_records = [];

        $null_records = [];

        foreach ($data as $record) {
          if($record['punch_in']==null){
            $null_records[] = $record;
          } else{
            $not_null_records[] = $record;
          }
        }

        usort($not_null_records, $this->sortByDateAdmin('punch_at'));

        $data = array_merge($not_null_records,$null_records);
        
        return $this->success($data,"Manpower schedule");
         
      } catch (Exception $e){
        return $this->error( [], trans('messages.EXECUTION_ERROR') );
      }
    }

    /* ---------------------------------------------------------------- */

    /**
     * Display a listing of worker log in/out time.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/getschedule",
     *     tags={"Punch In & Out"},
     *     summary="listing log in/out for workers.",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="project_id",
     *         in="query",
     *         description="Project ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="date",
     *         in="query",
     *         description="m-d-y (7-30-2020)",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Get schedule of this day."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function getschedule(Request $request){

      try{

        $date  = $request->date;
        
        $project_id  = $request->project_id;

        $format = 'm-d-Y';

        $requested_date = \Carbon\Carbon::createFromFormat($format, $date);
        
        $current_date = \Carbon\Carbon::createFromFormat($format, date('m-d-Y'));

        $manpower = \App\Manpower::where( 'job_date','=', $requested_date->toDateString() )
          ->where( 'project_id', $project_id )
          ->first();

        $data = [];

        $schedule_data = [];
		
		$start_time = null;
		
		$end_time = null;

        if($manpower){

          $emp_data = json_decode($manpower->emp_data, true);

          $emp_data_2 = json_decode($manpower->emp_data_2, true);

          $shifts = [];

          $shifts[] = $emp_data;

          $shifts[] = $emp_data_2;

          $start_time = null;

          $end_time = null;

          $start_time = str_replace(" : ", ":", $manpower->job_time);


          if($manpower->job_2_time!='' && $manpower->job_2_time!=null){
            $end_time = str_replace(" : ", ":", $manpower->job_2_time);  

            
          }
		  //echo $end_time; die; 
		  //print_r($shifts); die;
          foreach ($shifts as $shift_index => $shift) {
            
            $subcontractor_index = [];
			
			if(!isset($shift["sub"])){
				break;
			}
                        
            foreach ($shift['sub'] as $index => $sub_contractor) {

              if(!isset($subcontractor_index[$sub_contractor])){
                $subcontractor_index[$sub_contractor] = 0;
              } else {
                $subcontractor_index[$sub_contractor]++;
              }
              if($sub_contractor==624){

                if(!isset($shift['worker'][$index])){
                  continue;
                }
                $worker = $shift['worker'][$index];  

                $job_type = $shift['jobtype'][$index];
  
                $workers = User::role('worker')->orderBy('id', 'desc')->where('id', $worker)->get();

                foreach ($workers as $worker) {
                  
                  $log = [];

                  $subContractor = User::role('subcontractor')->where('id', $sub_contractor)->first();

                  if($shift_index==0){
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_time);  
                  } else {
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_2_time);
                  }
                  $log['shift_no'] = $shift_index + 1;
                  $log['sub_id'] = $sub_contractor;
                  $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
                  $log['sub_code'] = strtoupper( substr(str_replace(' ', '', $subContractor->company_name), 0, 4) );                     
                  $log['worker_id'] = $worker->id;
                  if(!empty($request->classification)){
                    $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                    $log['worker_name'] = $worker->first_name.' '.$worker->last_name.$classification;
                  }else{
                    $log['worker_name'] = $worker->first_name.' '.$worker->last_name;
                  }

                  $worker_type = \DB::table('worker_types')->where('id', $worker->worker_type)->first();
                  $log['worker_type'] = $worker_type->type;
                  $log['worker_type_id'] = $worker_type->id; 
                  $log['is_rock'] = 'true';
                  $log['punch_id'] = null;
                  $log['punch_at'] = null;
                  $log['punch_in'] = null;
                  $log['punch_out'] = null;
                  $log['device_info_in'] = null;
                  $log['device_info_out'] = null;
                  $log['in_type'] = null;
                  $log['out_type'] = null;
                  $log['occupied'] = false;

                  $data[] = $log;
                }  

              } else {
                
                if(!isset($shift['worker'][$index])){
                  continue;
                }
                $worker = $shift['worker'][$index]; 
                
                $worker_type_name = @$shift['a_workertype_name'][$sub_contractor][$subcontractor_index[$sub_contractor]];

                $worker_type_number = @$shift['a_workertype_number'][$sub_contractor][$subcontractor_index[$sub_contractor]];

                $worker_type_count = intval($worker_type_number);

                $worker_type  = $worker_type_name;

                $worker_type_or = $worker_type;

                $job_type = $shift['jobtype'][$index];

                for($i=0; $i < $worker_type_count; $i++){
                  $log = [];

                  $subContractor = User::role('subcontractor')->where('id', $sub_contractor)->first();

                  if($shift_index==0){
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_time);  
                  } else {
                    $log['shift_time'] = str_replace(' : ', ':', $manpower->job_2_time);
                  }
                  
                  $log['shift_no'] = $shift_index + 1;
                  $log['sub_id'] = $sub_contractor;
                  $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
                  $log['sub_code'] = strtoupper( substr(str_replace(' ', '', $subContractor->company_name), 0, 4) );                     
                  $log['worker_id'] = null;
                  $log['worker_name'] = null;
                  //echo $worker_type."==";
                  $worker_type_obj = \DB::table('worker_types')->where('id', $worker_type)->first();
                  $log['worker_type'] = @$worker_type_obj->type; 
                  $log['worker_type_id'] = @$worker_type_obj->id;  
                  $log['is_rock'] = 'false';
                  $log['punch_id'] = null;
                  $log['punch_at'] = null;
                  $log['punch_in'] = null;
                  $log['punch_out'] = null;
                  $log['in_type'] = null;
                  $log['out_type'] = null;
                  $log['occupied'] = false;
                  $log['device_info_in'] = null;
                  $log['device_info_out'] = null;

                  if( @$worker_type_obj->type != null && @$worker_type_obj->id != null )
                  $data[] = $log;
                }
              }
            }
          }  
          
        }
        $schedule_data = $data;

        //$new_data = [];

        $worker_log = \DB::table('worker_log')->where('project_id', $project_id)->whereDate('punch_at',"<=",  date("Y-m-d",strtotime($requested_date->format("Y-m-d")) + 24*60*60)  )->whereDate('punch_at',">=",  date("Y-m-d",strtotime($requested_date->format("Y-m-d"))-24*60*60) )->orderBy("punch_at","asc")->get();


        //$worker_log = \DB::table('worker_log')->where('project_id1', $project_id)->whereRaw(" DATE_FORMAT('%Y-%m-%d', CONVERT_TZ(punch_at,'+00:00','-04:00'))='".date("Y-m-d",strtotime($manpower->job_date))."'" )->orderBy("punch_at","asc")->get();
        
        //prin
        //print_r($worker_log);

        //echo $manpower->job_date;

        $new_worker_log = [];

        foreach ($worker_log as $log) {
          
          list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

          $punchIn = Carbon::createFromFormat('Y-m-d H:i:s',$log->punch_at, "UTC" ) ->setTimezone($timezone);

          if($punchIn->format("Y-m-d") == str_replace(" 00:00:00", "", $requested_date->format("Y-m-d"))){
            $new_worker_log[] = $log;
          }
        }
        $worker_log = $new_worker_log;

        //print_r($worker_log);
        $shift_logs = [];

        foreach ($worker_log as $punchlog) {

          list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

          $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $punchlog->punch_in, "UTC")->setTimezone($timezone);

          $punch_in = date( 'H:i:s', strtotime( $formatedPunchInTime ) );

          if( $punchlog->punch_out!=null){
            $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $punchlog->punch_out, "UTC")->setTimezone($timezone);

            $punch_out = date( 'H:i:s', strtotime( $formatedPunchInTime ) );
          } else {
            $punch_out = null;
          }
          /*if($punchlog->worker_id==836){
            echo $punch_in."==";
            echo strtotime($punch_in);
            echo "==";
            echo strtotime($end_time);
          }*/
          if($end_time==null){
            $shift_logs[$punchlog->worker_id]["shift1"][] = $punchlog;
            $shift_logs[$punchlog->worker_id]["shift1_done"] = false;
          }
          else if($end_time!=null && ( strtotime($punch_in) >= strtotime($end_time) || ($punch_out!=null && strtotime($punch_in) >= strtotime($end_time) - 30*60 &&  strtotime($punch_out) > strtotime($end_time) ) )){
            //echo 111;
            $shift_logs[$punchlog->worker_id]["shift2"][] = $punchlog;
            $shift_logs[$punchlog->worker_id]["shift2_done"] = false;

          } else {
            //echo 11112323;
            $shift_logs[$punchlog->worker_id]["shift1"][] = $punchlog;
            $shift_logs[$punchlog->worker_id]["shift1_done"] = false;
          }
          
        }
        //print_r($shift_logs);
        $data = [];

        
        for ($i=0 ; $i<sizeof($schedule_data); $i++) {
          
          

          

           
          foreach ($shift_logs as $worker_key => $shifts) {

            $worker = User::role('worker')->orderBy('id', 'desc')->where('id', $worker_key)->first();  
                            
            $worker_type = @\DB::table('worker_types')->where('id', $worker->worker_type)->first();

            $subcontractor = @User::where("id", $worker->parent)->first();
            
            $punchlog_matched = false;
            
            $schedule = $schedule_data[$i];

            if(@$worker->parent == $schedule["sub_id"]){



              $first_shift =   $schedule["shift_no"] == 1 ? (isset($shifts["shift1"]) ? $shifts["shift1"][0] : null) : (isset($shifts["shift2"]) ? $shifts["shift2"][0] : null); 

              $other_shifts =   $schedule["shift_no"] == 1 ? (isset($shifts["shift1"]) ? $shifts["shift1"] : null) : (isset($shifts["shift2"]) ? $shifts["shift2"] : null); 

              $shift_done_key = $schedule["shift_no"] == 1 ? 'shift1_done' : 'shift2_done'; 
              
              if($first_shift==null){
                continue;
              }
              if( $schedule["is_rock"]=='true'){
                
                if($first_shift->worker_id == $schedule['worker_id']){

                  if($schedule_data[$i]["occupied"]==false && !$shift_logs[$worker_key][$shift_done_key]){

                    $shift_logs[$worker_key][$shift_done_key] = true;

                    $schedule_data[$i]["occupied"] = true;

                    $schedule_data[$i]["occupied"] = true;

                    $punchlog_matched = true;

                    $temp_data = $schedule_data[$i];
                    $temp_data['punch_id'] = $first_shift->id;
                    $temp_data['punch_at'] = $first_shift->punch_at;
                    $temp_data['punch_in'] = $this->gt($first_shift->punch_in);
                    $temp_data['punch_out'] = $this->gt($first_shift->punch_out);
                    $temp_data['in_type'] = $first_shift->in_type;
                    $temp_data['out_type'] = $first_shift->out_type;
                    $temp_data['device_info_in'] = $first_shift->device_info_in;
                    $temp_data['device_info_out'] =  $first_shift->device_info_out;
                    
                    $data[] = $temp_data;

                    for($m=1; $m<sizeof($other_shifts);$m++){

                      $punchlog = $other_shifts[$m];
                      $temp_data = $schedule_data[$i];
                      $temp_data['punch_id'] = $punchlog->id;
                      $temp_data['punch_at'] = $punchlog->punch_at;
                      $temp_data['punch_in'] = $this->gt($punchlog->punch_in);
                      $temp_data['punch_out'] = $this->gt($punchlog->punch_out);
                      $temp_data['in_type'] = $punchlog->in_type;
                      $temp_data['out_type'] = $punchlog->out_type;
                      $temp_data['device_info_in'] = $punchlog->device_info_in;
                      $temp_data['device_info_out'] =  $punchlog->device_info_out;

                      $data[] = $temp_data;
                    }
                  }

                }





              } else {

                if($worker_type->id == $schedule['worker_type_id']){
                 
                  if($schedule_data[$i]["occupied"]==false && !$shift_logs[$worker_key][$shift_done_key]){

                    $shift_logs[$worker_key][$shift_done_key] = true;



                    $schedule_data[$i]["occupied"] = true;
                    $punchlog_matched = true;
                    $temp_data = $schedule_data[$i];
                    $temp_data['worker_id'] = $punchlog->worker_id;
                    if(!empty($request->classification)){
                    $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                    $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name.$classification;
                  }else{
                    $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name;
                  }

                          
                    $temp_data['punch_id'] = $first_shift->id;
                    $temp_data['punch_at'] = $first_shift->punch_at;
                    $temp_data['punch_in'] = $this->gt($first_shift->punch_in);
                    $temp_data['punch_out'] = $this->gt($first_shift->punch_out);
                    $temp_data['in_type'] = $first_shift->in_type;
                    $temp_data['out_type'] = $first_shift->out_type;
                    $temp_data['device_info_in'] = $first_shift->device_info_in;
                    $temp_data['device_info_out'] =  $first_shift->device_info_out;


                    $data[] = $temp_data;

                    

                    //print_r($data);

                    for($m=1; $m<sizeof($other_shifts);$m++){

                      $punchlog = $other_shifts[$m];

                      
                      $temp_data = $schedule_data[$i];
                      $temp_data['worker_id'] = $punchlog->worker_id;
                      if(!empty($request->classification)){
                        $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                        $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name.$classification;
                      }else{
                        $temp_data['worker_name'] = $worker->first_name.' '.$worker->last_name;
                      }

                          
                      $temp_data['punch_id'] = $punchlog->id;
                      $temp_data['punch_at'] = $punchlog->punch_at;
                      $temp_data['punch_in'] = $this->gt($punchlog->punch_in);
                      $temp_data['punch_out'] = $this->gt($punchlog->punch_out);
                      $temp_data['in_type'] = $punchlog->in_type;
                      $temp_data['out_type'] = $punchlog->out_type;
                      $temp_data['device_info_in'] = $punchlog->device_info_in;
                      $temp_data['device_info_out'] =  $punchlog->device_info_out;
                      $data[] = $temp_data;
                    }
                  }

                }

              }


            }
          }
          
        }  
        foreach ($schedule_data as $schedule) {
          if(!$schedule["occupied"]){
            $data[] = $schedule;
          }    
        }  
        foreach ($shift_logs as $worker_key => $shifts) {
          
          $worker = User::orderBy('id', 'desc')->where('id', $worker_key)->first();  
                            
          $worker_type = @\DB::table('worker_types')->where('id', $worker->worker_type)->first();

          $subcontractor = @User::where("id", $worker->parent)->first();
            
//print_r($shifts);

//print_r($shifts["shift1_done"]);

          if(isset($shifts["shift1_done"]) && !$shifts["shift1_done"]){
            
            foreach ($shifts["shift1"] as $shift) {
              
              //print_r($shift);
              $log = [];
              $log['shift_time'] = "";
              $log['shift_no'] = "";
              $log['sub_id'] = @$subcontractor->id;
              $log['sub_name'] = @explode(' ', $subcontractor->company_name)[0];
              $log['sub_code'] = empty( $subcontractor->id ) ? 'FOREMAN' : strtoupper( substr(str_replace(' ', '', @$subcontractor->company_name), 0, 4) );                     
              if(!empty($request->classification)){
                $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name.$classification;
              }else{
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name;
              }

              $log['worker_id'] = @$worker->id;
              $worker_type_obj = \DB::table('worker_types')->where('id', $worker->worker_type)->first();
              $log['worker_type'] = @$worker_type_obj->type;  
              $log['is_rock'] = 'false';
              if(@$subcontractor->id==624){
                $log['is_rock'] = 'true';
              }
              
              $log['punch_id'] = $shift->id;
              $log['punch_at'] = $shift->punch_at;
              $log['punch_in'] = $this->gt($shift->punch_in);
              $log['punch_out'] = $this->gt($shift->punch_out);
              $log['in_type'] = $shift->in_type;
              $log['out_type'] = $shift->out_type;
              $log['device_info_in'] = $shift->device_info_in;
              $log['device_info_out'] =  $shift->device_info_out;

              $data[] = $log;
            }
          }
          if(isset($shifts["shift2_done"]) && !$shifts["shift2_done"]){
            foreach ($shifts["shift2"] as $shift) {
              
              $subcontractor = User::where("id", $worker->parent)->first();
            
              $log = [];
              $log['shift_time'] = "";
              $log['shift_no'] = "";
              $log['sub_id'] = @$subcontractor->id;
              $log['sub_name'] = @explode(' ', $subcontractor->company_name)[0];
              $log['sub_code'] = empty( $subcontractor->id ) ? 'FOREMAN' : strtoupper( substr(str_replace(' ', '', @$subcontractor->company_name), 0, 4) );                     
              if(!empty($request->classification)){
                $classification = !empty($worker->classification) ? ' - '.$worker->classification : '';
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name.$classification;
              }else{
                $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name;
              }

              $log['worker_id'] = @$worker->id;
              $worker_type_obj = \DB::table('worker_types')->where('id', $worker->worker_type)->first();
              $log['worker_type'] = @$worker_type_obj->type;  
              $log['is_rock'] = 'false';
              if(@$subcontractor->id==624){
                $log['is_rock'] = 'true';
              }
              
              $log['punch_id'] = $shift->id;
              $log['punch_at'] = $shift->punch_at;
              $log['punch_in'] = $this->gt($shift->punch_in);
              $log['punch_out'] = $this->gt($shift->punch_out);
              $log['in_type'] = $shift->in_type;
              $log['out_type'] = $shift->out_type;
              $log['device_info_in'] = $shift->device_info_in;
              $log['device_info_out'] =  $shift->device_info_out;

              $data[] = $log;
            }
          }
        } 
        usort($data, $this->sortByDate('punch_at'));

        $not_null_records = [];

        $null_records = [];

        foreach ($data as $record) {
          if($record['punch_in']==null){
            $null_records[] = $record;
          } else{
            $not_null_records[] = $record;
          }
        }

        usort($not_null_records, $this->sortByDate('punch_at'));

        $data = array_merge($not_null_records,$null_records);
        
        return $this->success($data,"Manpower schedule");
         
      } catch (Exception $e){
        return $this->error( [], trans('messages.EXECUTION_ERROR') );
      }
    }
    public function getscheduleYest(Request $request){
      try{

        $date  = $request->date;
        
        $project_id  = $request->project_id;

        $format = 'm-d-Y';

        $requested_date = \Carbon\Carbon::createFromFormat($format, $date);
        
        $current_date = \Carbon\Carbon::createFromFormat($format, date('m-d-Y'));

        $manpower = \App\Manpower::where( 'job_date','=', $requested_date->toDateString() )
          ->where( 'project_id', $project_id )
          ->first();

        $data = [];

        $schedule_data = [];

        if($manpower){

          $emp_data = json_decode($manpower->emp_data, true);

          $emp_data_2 = json_decode($manpower->emp_data_2, true);

          $shifts = [];

          $shifts[] = $emp_data;

          $shifts[] = $emp_data_2;

          $start_time = null;

          $end_time = null;


          $start_time = str_replace(" : ", ":", $manpower->job_time);

          if($manpower->job_2_time!='' && $manpower->job_2_time!=null){
              $end_time = str_replace(" : ", ":", $manpower->job_2_time);  
            }

         // print_r($start_time);

         // print_r($end_time);


          foreach ($shifts as $shift_index => $shift) {
            
            $subcontractor_index = [];
                        
            foreach ($shift['sub'] as $index => $sub_contractor) {

              if(!isset($subcontractor_index[$sub_contractor])){
                $subcontractor_index[$sub_contractor] = 0;
              } else {
                $subcontractor_index[$sub_contractor]++;
              }
              
              $subLog = [];


              if($sub_contractor==624){

                $worker = $shift['worker'][$index];  

                $job_type = $shift['jobtype'][$index];
  
                $workers = User::role('worker')->orderBy('id', 'desc')->where('id', $worker)->get();  

                foreach($workers as $worker){
                  $worker_log = \DB::table('worker_log')->where('project_id', $project_id)->where('worker_id', $worker->id)->whereDate('punch_at',  $manpower->job_date)->orderBy("punch_at","asc")->get();
                  
                  //print_r($worker_log);

                  foreach ($worker_log as $punchlog) {
                   
                    if($shift_index == 0){

                        if($end_time!=null && (strtotime($punchlog->punch_in) > strtotime($end_time)  ) ){
                          break;
                        }
                      }
                      if($shift_index == 1){

                        if($end_time!=null && (strtotime($punchlog->punch_in) <= strtotime($end_time) ) ){
                          continue;
                        }
                      }
                          
                    $log = [];

                    $subContractor = User::role('subcontractor')->where('id', $sub_contractor)->first();

                    if($shift_index==0){
                      $log['shift_time'] = str_replace(' : ', ':', $manpower->job_time);  
                    } else {
                      $log['shift_time'] = str_replace(' : ', ':', $manpower->job_2_time);
                    }
                    $log['shift_no'] = $shift_index + 1;
                    $log['sub_id'] = @$sub_contractor;
                    $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
                    $log['sub_code'] = strtoupper( substr(str_replace(' ', '', @$subContractor->company_name), 0, 4) );                     
                    $log['worker_id'] = @$worker->id;
                    $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name;
                    $worker_type = \DB::table('worker_types')->where('id', @$worker->worker_type)->first();
                      $log['worker_type'] = @$worker_type->type;
                    $log['is_rock'] = 'false';
                    $log['punch_id'] = $punchlog->id;
                    $log['punch_at'] = $punchlog->punch_at;
                    $log['punch_in'] = $this->gt($punchlog->punch_in);
                    $log['punch_out'] = $this->gt($punchlog->punch_out);
                    $log['in_type'] = $punchlog->in_type;
                    $log['out_type'] = $punchlog->out_type;

                    $data[] = $log;

                    $subLog[] = $log;
                  }
                }


              } else {
                
                //print_r($shift);

                

                $worker = $shift['worker'][$index]; 

                //foreach ($shift['a_workertype_name'][$sub_contractor] as $key => $worker_type_name) {
                  
                  $worker_type_name = @$shift['a_workertype_name'][$sub_contractor][$subcontractor_index[$sub_contractor]];

                  $worker_type_number = @$shift['a_workertype_number'][$sub_contractor][$subcontractor_index[$sub_contractor]];

                  

                  $worker_type_count = $worker_type_number;

                  $worker_type  = $worker_type_name;

                  //echo "sub==".$sub_contractor."-===";

                   $worker_type_or = $worker_type;

                  //echo "<br/>";

                  $job_type = $shift['jobtype'][$index];

                  $worker_ids = [];

                  $workers = User::role('worker')->orderBy('id', 'desc')->where('parent', $sub_contractor)->where('worker_type', $worker_type)->get();  


                  $worker_ids = [];

                  foreach ($workers as $worker) {
                    $worker_ids[] = $worker->id;
                  }

                    
                  $workersArray = [];

                  foreach ($workers as $worker) {
                    
                    $worker_log = \DB::table('worker_log')->where('project_id', $project_id)->where('worker_id', $worker->id)->whereDate('punch_at', $manpower->job_date)->orderBy("punch_at","asc")->get();
                    
                    
                    foreach ($worker_log as $log) {
                      
                      if(!in_array($log->worker_id, $workersArray)){
                        
                        $workersArray[] = $log->worker_id;
                      }

                    }
                  }
                  //array_splice($workersArray, $worker_type_count);

                  foreach($workersArray as $worker_id){
                    
                    $worker = User::role('worker')->orderBy('id', 'desc')->where('id', $worker_id)->first();  

                    $worker_log = \DB::table('worker_log')->where('project_id', $project_id)->where('worker_id', $worker_id)->whereDate('punch_at', $manpower->job_date)->orderBy("punch_at","asc")->get();
                    
                    
                    
                    foreach ($worker_log as $punchlog) {
                     
                      /*if($shift_index == 0){

                        if($end_time!=null && (strtotime($punchlog->punch_in) > strtotime($end_time) ) ){
                          break;
                        }
                      }
                      if($shift_index == 1){

                        if($end_time!=null && (strtotime($punchlog->punch_in) <= strtotime($end_time)  ) ){
                          continue;
                        }
                      }*/
                      if($shift_index == 0){

                        if($end_time!=null && ( strtotime($punchlog->punch_in) > strtotime($end_time) || (strtotime($punchlog->punch_in) > strtotime($end_time) - 30*60 &&  strtotime($punchlog->punch_out) > strtotime($end_time) ) )){
                          break;
                        }
                      }
                      if($shift_index == 1){

                        if($end_time!=null && (strtotime($punchlog->punch_in) < strtotime($end_time)  || !(strtotime($punchlog->punch_in) > strtotime($end_time) - 30*60 &&  strtotime($punchlog->punch_out) > strtotime($end_time) ) )){
                          continue;
                        }
                      }

                      $log = [];

                      $subContractor = User::role('subcontractor')->where('id', $sub_contractor)->first();

                      if($shift_index==0){
                        $log['shift_time'] = str_replace(' : ', ':', $manpower->job_time);  
                      } else {
                        $log['shift_time'] = str_replace(' : ', ':', $manpower->job_2_time);
                      }
                      
                      $log['shift_no'] = $shift_index + 1;
                      $log['sub_id'] = @$sub_contractor;
                      $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
                      $log['sub_code'] = strtoupper( substr(str_replace(' ', '', @$subContractor->company_name), 0, 4) );                     
                      $log['worker_id'] = @$worker->id;
                      $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name;
                      
                       $worker_type = \DB::table('worker_types')->where('id', @$worker->worker_type)->first();
                      $log['worker_type'] = @$worker_type->type;  
                      $log['is_rock'] = 'false';
                      $log['punch_id'] = $punchlog->id;
                      $log['punch_at'] = $punchlog->punch_at;
                      $log['punch_in'] = $this->gt($punchlog->punch_in);
                      $log['punch_out'] = $this->gt($punchlog->punch_out);
                      $log['in_type'] = $punchlog->in_type;
                      $log['out_type'] = $punchlog->out_type;

                      $data[] = $log;

                      $subLog[] = $log;
                    }  
                  }

                  
            

                //}
              }
              //print_r($subLog);

              if(sizeof($subLog)==0){
                $log = [];

                $subContractor = User::role('subcontractor')->where('id', $sub_contractor)->first();


                if($shift_index==0){
                  $log['shift_time'] = str_replace(' : ', ':', $manpower->job_time);  
                } else {
                  $log['shift_time'] = str_replace(' : ', ':', $manpower->job_2_time);
                }
                $log['shift_no'] = $shift_index + 1;
                $log['sub_id'] = $sub_contractor;
                $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
                $log['sub_code'] = strtoupper( substr(str_replace(' ', '', $subContractor->company_name), 0, 4) );                     
                $log['worker_id'] = null;
                $log['worker_name'] = null;

                $log['worker_type'] = NULL;                 
                if($subContractor->id==624){

                  $log['is_rock'] = 'true';  
                  
                  $worker = $shift['worker'][$index];  

                  
                  $worker = User::role('worker')->orderBy('id', 'desc')->where('id', $worker)->first();

                   //$worker_type = \DB::table('worker_types')->where('id', $worker->worker_type)->first();
  

                  $log['worker_id'] = $worker->id;

                 // $log['worker_type'] = $worker_type->type;

                  $log['worker_name'] =  $worker->first_name.' '.$worker->last_name;

                } else {
                  //echo  $worker_type;
                  $worker_type = \DB::table('worker_types')->where('id', $worker_type_or)->first();

                  if($worker_type){
                    $log['worker_type'] = $worker_type->type;
                  }
                  
                  $log['is_rock'] = 'false';
                }
                $log['punch_id'] = null;
                $log['punch_at'] = null;
                $log['punch_in'] = null;
                $log['punch_out'] = null;
                $log['in_type'] = null;
                $log['out_type'] = null;

                $schedule_data[] = $log;
              }

            }

          }
          
        }
        $log_id = [];

        foreach ($data as $log) {
          if($log['punch_id']!='' && $log['punch_id']!=null){
            $log_id[] = $log['punch_id'];
          }
        }
        $worker_log = \DB::table('worker_log')->where('project_id', $project_id)->whereNotIn("id",$log_id)->whereDate('punch_at',  $requested_date)->orderBy("punch_at","asc")->get();
       // print_r($log_id);          

        foreach ($worker_log as $punchlog) {



          $worker = User::role('worker')->orderBy('id', 'desc')->where('id', $punchlog->worker_id)->first();  

                
          $log = [];

          $subContractor = User::where("id", $worker->parent )->first();
          //print_r($subContractor); die;
          $log['shift_time'] = "";
          $log['shift_no'] = "";
          $log['sub_id'] = @$subContractor->id;
          $log['sub_name'] = @explode(' ', $subContractor->company_name)[0];
          $log['sub_code'] = strtoupper( substr(str_replace(' ', '', @$subContractor->company_name), 0, 4) );                     
          $log['worker_id'] = @$worker->id;
          $log['worker_name'] = @$worker->first_name.' '.@$worker->last_name;

         $worker_type = \DB::table('worker_types')->where('id', @$worker->worker_type)->first();

          $log['worker_type'] = $worker_type->type;

          if(@$subContractor->id==624){
            $log['is_rock'] = 'true';  
          } else {
            $log['is_rock'] = 'false';
          }
          
          $log['punch_id'] = $punchlog->id;
          $log['punch_at'] = $punchlog->punch_at;
          $log['punch_in'] = $this->gt($punchlog->punch_in);
          $log['punch_out'] = $this->gt($punchlog->punch_out);
          $log['in_type'] = $punchlog->in_type;
          $log['out_type'] = $punchlog->out_type;

          $data[] = $log;
        }
        if(sizeof($schedule_data)>0){
          $data = array_merge($data,$schedule_data);
        }
        //array_unique($data, SORT_REGULAR);

        return $this->success($data,"Manpower schedule");
         

      } catch (Exception $e){
        return $this->error( [], trans('messages.EXECUTION_ERROR') );
      }
    }
    public function getScheduleOld( Request $request )
    {
        try{

        	/* -------------------------------------------- */

        	$i  = $request->date;
        	$pr  = $request->project_id;

        	$mdr = [];
        	$mdc = [];

      		$format = 'm-d-Y';

      		$c = \Carbon\Carbon::createFromFormat($format, $i);
      		$cd = \Carbon\Carbon::createFromFormat($format, date('m-d-Y'));

      		$md = \App\Manpower::where( 'job_date','=', $c->toDateString() )
      		->where( 'project_id', $pr )
      		->first();

         
          
      		if( $md )
      		{

      			/* Common data */

      			$mdc['project_id'] = $md->project_id;
      			$mdc['job_date'] = $md->job_date;
            $s1logs = [];
            $alllogs = [];

      			/* Common data */

      			/* Repeat shifts 1 data */

      			//print_r( json_decode($md->emp_data) );

      			$s1 = json_decode($md->emp_data);

                $wlp = [];
                $wlp2 = [];

      			foreach( $s1->sub as $k => $v )
      			{
      				$sn = \DB::table('users')->where('id', $v)->first()->company_name;

      				/* If rockspring */
      				
      				if( $v == 624 )
      				{

                        $pi = '';
                        $po = '';
                        $pid = '';
                        $pat = '';
                        $pit = '';
                        $pot = '';
                        /* Get time */

                        $dtd = \DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $s1->worker[$k])->whereDate('punch_at', $md->job_date)->get();

                        if( count( $dtd ) == 0 )
                        {
                          $dtd[0] = 1;
                        }

                        foreach( $dtd as $dt )
                        {

                          if( $dt )
                        {
                            $pid = @$dt->id;
                            $alllogs[] = @$dt->worker_id;
                            $pat = @$dt->punch_at;
                            $pit = @$dt->in_type;
                            $pot = @$dt->out_type;
                            $i = @$dt->punch_in;
                            $o = @$dt->punch_out;

                            $pi = $this->gt( $i );
                            $po = $this->gt( $o );
                        }

                        

                        /* Get time */

                $ta['shift_time'] = str_replace(' : ', ':', $md->job_time);
                $ta['shift_no'] = '1';
                $ta['sub_id'] = $v;
                $ta['sub_name'] = @explode(' ', $sn)[0];
                $ta['sub_code'] = strtoupper( substr(str_replace(' ', '', $sn), 0, 4) );
                $ta['worker_id'] = $s1->worker[$k];
                $ta['worker_name'] = \DB::table('users')->where('id', $s1->worker[$k])->first()->first_name.' '.\DB::table('users')->where('id', $s1->worker[$k])->first()->last_name;
                $ta['worker_type'] = \DB::table('worker_types')->where('id', \DB::table('users')->where('id', $s1->worker[$k])->first()->worker_type)->first()->type;;
                $ta['is_rock'] = 'true';
                $ta['punch_id'] = $pid;
                $ta['punch_at'] = $pat;
                $ta['punch_in'] = $pi;
                $ta['punch_out'] = $po;
                $ta['in_type'] = $pit;
                $ta['out_type'] = $pot;

                array_push($mdr, $ta);

                        }

                        
      				}

      				/* If rockspring */

      				/* If other */

      				else
      				{
                  

                        $wtnm = (array)$s1->a_workertype_number;
                        $wtnme = (array)$s1->a_workertype_name;

                        $pi = '';
                        $po = '';
                        $pid = '';
                        $pat = '';
                        $pit = '';
                        $pot = '';

                        if( isset( $wlp[$v] ) )
                        {
                            $cw = $wlp[$v];
                            $wlp[$v] = $wlp[$v]+1;
                        }
                        else
                        {
                            $cw = 0;
                            $wlp[$v] = 1;
                        }

                        for( $il = 0; $il < $wtnm[$v][$cw]; $il++ )
                        {

                            /* Get time */

                        if( $il == 0)
                        {
                            $uwid = User::role('worker')->orderBy('id', 'desc')->where('parent', $v)->where('worker_type', $wtnme[$v][$cw])->first();
                            if($uwid)
                                $uwid = $uwid->id;
                        }
                        else
                        {
                            $uwid = User::role('worker')->orderBy('id', 'desc')->where('parent', $v)->where('worker_type', $wtnme[$v][$cw])->skip($il)->first();
                            if($uwid)
                                $uwid = $uwid->id;
                        }

                        
                    
                        $dtd = \DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $uwid)->whereDate('punch_at', $md->job_date)->get();



                        if( count( $dtd ) == 0 )
                        {
                          $dtd[0] = 1;
                        }

                        foreach( $dtd as $dt )
                        {

                          if( $dt )
                        {
                            $s1logs[] = @$dt->id;
                            $alllogs[] = @$dt->worker_id;
                            $pid = @$dt->id;
                            $pat = @$dt->punch_at;
                            $pit = @$dt->in_type;
                            $pot = @$dt->out_type;
                            $i = @$dt->punch_in;
                            $o = @$dt->punch_out;

                            $pi = $this->gt( $i );
                            $po = $this->gt( $o );
                        }

                        /* Get time */

                            $ta['shift_time'] = str_replace(' : ', ':', $md->job_time);
                            $ta['shift_no'] =  strtotime($i) > strtotime($md->job_2_time) ? '2' : '1';
                            $ta['sub_id'] = $v;
                            $ta['sub_name'] = @explode(' ', $sn)[0];
                            $ta['sub_code'] = strtoupper( substr(str_replace(' ', '', $sn), 0, 4) );
                            $ta['worker_id'] = ( !empty( $dt->punch_in ) ) ? $uwid : '';
                            $ta['worker_name'] = ( !empty( $dt->punch_in ) ) ? \DB::table('users')->where('id', $uwid)->first()->first_name.' '.\DB::table('users')->where('id', $uwid)->first()->last_name : '';
                            $ta['worker_type'] = \DB::table('worker_types')->where('id', $wtnme[$v][$cw])->first()->type;
                            $ta['is_rock'] = 'false';
                            $ta['punch_id'] = $pid;
                            $ta['punch_at'] = $pat;
                            $ta['punch_in'] = $pi;
                            $ta['punch_out'] = $po;
                            $ta['in_type'] = $pit;
                            $ta['out_type'] = $pot;

                            array_push($mdr, $ta);

                        }

                        
                        }

      				}

      				/* If other */

      			}


      			/* Repeat shifts 1 data */

                /* Repeat shifts 2 data */

                //print_r( json_decode($md->emp_data) );

                $s1 = json_decode($md->emp_data_2);

                if( isset( $s1->sub ) ):

                $wlp = [];
                $wlp2 = [];

                foreach( $s1->sub as $k => $v )
                {
                  $pi = '';
                        $po = '';
                        $pid = '';
                        $pat = '';
                        $pit = '';
                        $pot = '';

                    $sn = \DB::table('users')->where('id', $v)->first()->company_name;

                    /* Get time */

                        

                        

                        /* Get time */

                    /* If rockspring */
                    
                    if( $v == 624 )
                    {
                      $dtd = \DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $s1->worker[$k])->whereDate('punch_at', $md->job_date)->get();

                      if( count( $dtd ) == 0 )
                        {
                          $dtd[0] = 1;
                        }

                        foreach( $dtd as $dt )
                        {

                        if( $dt )
                        {
                            $alllogs[] = @$dt->worker_id;
                            $pid = @$dt->id;
                            $pat = @$dt->punch_at;
                            $pit = @$dt->in_type;
                            $pot = @$dt->out_type;
                            $i = @$dt->punch_in;
                            $o = @$dt->punch_out;

                            $pi = $this->gt( $i );
                            $po = $this->gt( $o );
                        }
                        $ta['shift_time'] = str_replace(' : ', ':', $md->job_2_time);
                        $ta['shift_no'] = '2';
                        $ta['sub_id'] = $v;
                        $ta['sub_name'] = @explode(' ', $sn)[0];
                        $ta['sub_code'] = strtoupper( substr(str_replace(' ', '', $sn), 0, 4) );
                        $ta['worker_id'] = $s1->worker[$k];
                        $ta['worker_name'] = \DB::table('users')->where('id', $s1->worker[$k])->first()->first_name.' '.\DB::table('users')->where('id', $s1->worker[$k])->first()->last_name;
                        $ta['worker_type'] = \DB::table('worker_types')->where('id', \DB::table('users')->where('id', $s1->worker[$k])->first()->worker_type)->first()->type;;
                        $ta['is_rock'] = 'true';
                        $ta['punch_id'] = $pid;
                        $ta['punch_at'] = $pat;
                        $ta['punch_in'] = $pi;
                        $ta['punch_out'] = $po;
                        $ta['in_type'] = $pit;
                        $ta['out_type'] = $pot;

                        array_push($mdr, $ta);
                      }
                    }

                    /* If rockspring */

                    /* If other */

                    else
                    {
                        
                        $wtnm = (array)$s1->a_workertype_number;
                        $wtnme = (array)$s1->a_workertype_name;

                        $pi = '';
                        $po = '';
                        $pid = '';
                        $pat = '';
                        $pit = '';
                        $pot = '';

                        if( isset( $wlp[$v] ) )
                        {
                            $cw = $wlp[$v];
                            $wlp[$v] = $wlp[$v]+1;
                        }
                        else
                        {
                            $cw = 0;
                            $wlp[$v] = 1;
                        }

                        $excids = [];

                        for( $il = 0; $il < $wtnm[$v][$cw]; $il++ )
                        {

                            /* Get time */

                        if( $il == 0)
                        {

                            $uwid = User::role('worker')->orderBy('id', 'desc')->where('parent', $v)->where('worker_type', $wtnme[$v][$cw])->first();
                            if($uwid)
                                $uwid = $uwid->id;
                        }
                        else
                        {

                            $uwid = User::role('worker')->orderBy('id', 'desc')->where('parent', $v)->where('worker_type', $wtnme[$v][$cw])->skip($il)->first();
                            if($uwid)
                                $uwid = $uwid->id;
                        }

                    
                        $dtd = \DB::table('worker_log')->whereNotIn('id', $s1logs)->where('project_id', $md->project_id)->where('worker_id', $uwid)->whereDate('punch_at', $md->job_date)->get();

                        if( count( $dtd ) == 0 )
                        {
                          $dtd[0] = 1;
                        }

                        foreach( $dtd as $dt )
                        {

                          if( $dt )
                          {
                              $pid = @$dt->id;
                              $alllogs[] = @$dt->worker_id;
                              $pat = @$dt->punch_at;
                              $pit = @$dt->in_type;
                              $pot = @$dt->out_type;
                              $i = @$dt->punch_in;
                              $o = @$dt->punch_out;

                              $pi = $this->gt( $i );
                              $po = $this->gt( $o );
                          }

                        /* Get time */

                            $ta['shift_time'] = str_replace(' : ', ':', $md->job_time);
                            $ta['shift_no'] = '2';
                            $ta['sub_id'] = $v;
                            $ta['sub_name'] = @explode(' ', $sn)[0];
                            $ta['sub_code'] = strtoupper( substr(str_replace(' ', '', $sn), 0, 4) );
                            $ta['worker_id'] = ( !empty( $dt->punch_in ) ) ? $uwid : '';
                            $ta['worker_name'] = ( !empty( $dt->punch_in ) ) ? \DB::table('users')->where('id', $uwid)->first()->first_name.' '.\DB::table('users')->where('id', $uwid)->first()->last_name : '';
                            $ta['worker_type'] = \DB::table('worker_types')->where('id', $wtnme[$v][$cw])->first()->type;
                            $ta['is_rock'] = 'false';
                            $ta['punch_id'] = $pid;
                            $ta['punch_at'] = $pat;
                            $ta['punch_in'] = $pi;
                            $ta['punch_out'] = $po;
                            $ta['in_type'] = $pit;
                            $ta['out_type'] = $pot;

                            array_push($mdr, $ta);

                        }

                        
                        }
                    }

                    /* If other */

                }

                endif;

                /* Repeat shifts 2 data */

      			//$md->emp_data = json_decode( $md->emp_data );

      		}


          $thirdpw = \DB::table('worker_log')->whereNotIn('worker_id', array_filter($alllogs))->where('project_id', $md->project_id)->whereDate('punch_at', $md->job_date)->get();

          if( $md ):

          foreach( $thirdpw as $dt ):

          if( $dt )
                          {
                              $pid = @$dt->id;
                              $pat = @$dt->punch_at;
                              $pit = @$dt->in_type;
                              $pot = @$dt->out_type;
                              $i = @$dt->punch_in;
                              $o = @$dt->punch_out;

                              $pi = $this->gt( $i );
                              $po = $this->gt( $o );
                          }

                        /* Get time */

                            $ta['shift_time'] = str_replace(' : ', ':', $md->job_time);
                            $ta['shift_no'] = '1';
                            $ta['sub_id'] = \DB::table('users')->where('id', \DB::table('users')->where('id', $dt->worker_id)->first()->parent)->first()->id;
                            $ta['sub_name'] = \DB::table('users')->where('id', \DB::table('users')->where('id', $dt->worker_id)->first()->parent)->first()->company_name;
                            $ta['sub_code'] = strtoupper( substr(str_replace(' ', '', explode(' ', \DB::table('users')->where('id', \DB::table('users')->where('id', $dt->worker_id)->first()->parent)->first()->company_name)[0]), 0, 4) );
                            $ta['worker_id'] = $dt->worker_id;
                            $ta['worker_name'] = \DB::table('users')->where('id', $dt->worker_id)->first()->first_name.' '.\DB::table('users')->where('id', $dt->worker_id)->first()->last_name;
                            $ta['worker_type'] = \DB::table('worker_types')->where('id', \DB::table('users')->where('id', $dt->worker_id)->first()->worker_type)->first()->type;
                            $ta['is_rock'] = 'false';
                            $ta['punch_id'] = $pid;
                            $ta['punch_at'] = $pat;
                            $ta['punch_in'] = $pi;
                            $ta['punch_out'] = $po;
                            $ta['in_type'] = $pit;
                            $ta['out_type'] = $pot;

                            array_push($mdr, $ta);

          endforeach;

        endif;

      

        	/* -------------------------------------------- */

          $mdr2 = [];
          $mdr3 = [];
          $mdr4 = [];
          $mdr5 = [];
          foreach( $mdr as $mdr1 )
          {

            if( !empty( $mdr1['punch_at'] ) )
            {
              $mdr2[] = $mdr1;
            }
            else
            {
              $mdr5[] = $mdr1;
            }
            
          }


            
        	$d = $md;
        	$m = 'Schedule Fetched';

 
          usort($mdr2, $this->sortByDate('punch_at'));


            return $this->success(array_merge($mdr2, $mdr5) , $m );

        }
        catch(Excetion $e)
        {
            return $this->error( [], trans('messages.EXECUTION_ERROR') );
        }
    }

    function sortByDate($key)
    {
        return function ($a, $b) use ($key) {
            $t1 = strtotime($a[$key]);
            $t2 = strtotime($b[$key]);
            return $t1-$t2;
        };

    }


      function sortByDateAdmin($key)
    {
        return function ($a, $b) use ($key) {
            $t1 = strtotime($a[$key]);
            $t2 = strtotime($b[$key]);
            return $t2-$t1;
        };

    }  

    public function gt($t)
    {

        list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

        $punchInTime = $t;

        $formatedPunchInTime = date( 'H:i:s', strtotime( $punchInTime ) );

        $formatedPunchInTime = \Carbon\Carbon::createFromFormat('H:i:s', $formatedPunchInTime, 'UTC') ->setTimezone($timezone);

        $formatedPunchInTime = date( 'H:i:s', strtotime( $formatedPunchInTime ) );

        return $t;

    }

    /**
     * Worker can log in/out
     *
     * @return \Illuminate\Http\PunchRequest
     * @OA\Post(
     *     path="/v1/project/punch_in",
     *     tags={"Punch In & Out"},
     *     summary="Punch in for workers.",
     *     operationId="create",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="project_id",
     *         in="query",
     *         description="Project ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="in_type",
     *         in="query",
     *         description="Integer 1:scan,2 manually,3: by foreman ",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="worker_id",
     *         in="query",
     *         description="Worker ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Punch in marked successfully."
     *     )
     * )
     */
    public function create(PunchRequest $request)
    {

        try
        {

        	$workerPreId = Auth::user()->id;

        	$alreadyPunched = false;

        	//
        	if( WorkLog::where('worker_id', $workerPreId)->whereNull('punch_out')->whereNotNull('punch_in')->exists() )
        	{
        		$alreadyPunched = true;
        	}

        	//
        	if( $alreadyPunched )
        	{
        		return $this->error( [], 'User already punched in'  );
        	}
            $notFound = true;

            if( Project::where( 'id', $request->project_id )->exists() )
                $notFound = false;

            if( !$notFound )
            {

                $proObj = Project::withTrashed()->where( 'id', $request->project_id )->first();
            
                if( $proObj->deleted_at == NULL )
                    $notFound = false;  

            }

            if( $notFound )
                return $this->error( [],trans('messages.PROJECT_DELETED')  );

            $dateTime = Carbon::now();
            $time = Carbon::now()->toTimeString();
            $workLogData['worker_id'] = $request->worker_id;
            $workLogData['sub'] = \DB::Table('users')->where('id', $request->worker_id)->first()->parent;
            $workLogData['project_id'] = $request->project_id;
            $workLogData['punch_in'] = Carbon::now()->toTimeString();
            $workLogData['in_type'] = $request->in_type;
            $workLogData['out_type'] = 0;
            $workLogData['punch_at'] = Carbon::now();
            $workLogData['punch_out'] = null;
            if( $request->device_info_in){
              $workLogData['device_info_in'] = $request->device_info_in;
            }
            if( $request->device_info_out){
              $workLogData['device_info_out'] = $request->device_info_out;
            }
            //$workLogData['device_info'] = $request->device_info

            $wordkerStatus =   WorkLog::create( $workLogData ); // save worker log 

            $userData = User::find( Auth::user()->id );

            $log = WorkLog::with('project')->findOrFail($wordkerStatus->id);

            $data['punches'] = $log;

            $data['is_deleted'] = 0;

            $data['is_blocked'] = 0;

            $message = trans('messages.PUNCH_SUCCESS');

            if( $userData->deleted_at != NULL )
            {
                $data['is_deleted'] = 1;
                $message = trans('messages.IS_DELETED');
            }
            

            if( $userData->status == 0 )
            {
                $data['is_blocked'] = 1;
                $message = trans('messages.IS_BLOCKED');
            }
            
            return $this->success( $data,  $message );
        } 
        catch (Exception $e)
        {
            return $this->error( [],trans('messages.EXECUTION_ERROR')  );

        }
    }

    /**
     * Store a newly offline punch resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @OA\Post(
     *     path="/v1/project/punch_offline",
     *     tags={"Punch In & Out"},
     *     summary="Sync offline punches in  & out for workers.",
     *     operationId="store",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="offline",
     *         in="query",
     *         description="Offline object [{'project_id':1,'worker_id':4, 'punch_in':'08:58:39','punch_out':'08:58:39', 'in_type':1,'out_type':1},{'project_id':2,'worker_id':4, 'punch_in':'08:58:39','punch_out':'08:58:39','in_type':1,'out_type':1}]",
     *         required=true,
     *         @OA\Schema(
     *             type="object"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Punch in & out marked successfully."
     *     )
     * )
     */
    public function store(Request $request)
    {
        try
        {   
	           $i=0;
            foreach($request->all() as $ofline):

            $projectExists = false;
	
		
            if( Project::where( 'id', $ofline['project_id'] )->exists() )

                $projectExists = true;

            if( $projectExists ):
	             

                if(isset($ofline['id'])){
                    $worklog = new WorkLog();
                    $worklog->exists = true;
                    $worklog->sub = \DB::Table('users')->where('id', $ofline['worker_id'])->first()->parent;
	
	               unset($ofline['punch_out_date']);

                      $j=0;
                    foreach($ofline as  $key => $field):
                        $worklog->$key = $field;
                    endforeach;

                    $worklog->save();

                    /* 8 hour logout */
	
                       $worklogs  = DB::table('worker_log')->where('id', $ofline['id'])->where('punch_at','<',Carbon::now()->subHours(13))->get();
	

                    if(count($worklogs)):
                           foreach($worklogs as $worklog  ):
                               DB::table('worker_log')->where('id', $worklog->id)->update([
                                   'punch_out' =>Carbon::parse($worklog->punch_in)->addHour(8)
                                   ]);
                           endforeach;
                       endif;

                    /* 8 hour logout */

                
                }else
                {

                	$workerPreId = Auth::user()->id;


		        	$alreadyPunched = false;

		        	//
		        	if( WorkLog::where('worker_id', $workerPreId)->whereNull('punch_out')->whereNotNull('punch_in')->exists() )
		        	{
				
		        		$alreadyPunched = true;
		        	}
                                                             
		        	//
		        	if( $alreadyPunched )
		        	{
		        		return $this->error( [], 'User already punched in'  );
		        	}
                	else
                	{
                		$ofline['sub'] = \DB::Table('users')->where('id', $ofline['worker_id'])->first()->parent;
                  	
                		if(!empty($ofline['punch_out_date'])){
                	        $add_offline_work_logs = new WorkLog;
                     	    $add_offline_work_logs->worker_id   = $ofline['worker_id'];
                            $add_offline_work_logs->sub         = $ofline['sub'];
                            $add_offline_work_logs->project_id  = $ofline['project_id'];
                            $add_offline_work_logs->in_type     = $ofline['in_type'];
                            $add_offline_work_logs->out_type    = 2;
                            $add_offline_work_logs->punch_at    = $ofline['punch_at'];
                            $add_offline_work_logs->punch_in    = $ofline['punch_in'];

                            if( $request->device_info_in){
                              $add_offline_work_logs->device_info_in = $request->device_info_in;
                            }
                            if( $request->device_info_out){
                              $add_offline_work_logs->device_info_out = $request->device_info_out;
                            }

                            
                            // *********************** Old Logic ********************

                            // $start = Carbon::parse($ofline['punch_at']);
                            // $end = Carbon::parse($ofline['punch_out_date']);
                            // $totalDuration = $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S');

                            // $total_duration = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $totalDuration);
                            // sscanf($total_duration, "%d:%d:%d", $hours, $minutes, $seconds);
                            // $total_duration_seconds = $hours * 3600 + $minutes * 60 + $seconds;

                            // $time_avail ="08:00:00";
                            // $total_duration = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time_avail);
                            // sscanf($time_avail, "%d:%d:%d", $hours, $minutes, $seconds);
                            // $time_avail_seconds = $hours * 3600 + $minutes * 60 + $seconds;
                            // if($time_avail_seconds >= $total_duration_seconds ){
                            //     $add_offline_work_logs->punch_out   = $ofline['punch_out'];
                            // }else{
                            //     $add_offline_work_logs->punch_out   = Carbon::parse($ofline['punch_in'])->addHour(8);
                            // }

                            // *********************** Old Logic ********************



                          // *********************** New Logic ********************

                            $start          = Carbon::parse($ofline['punch_at']);
                            $end            = Carbon::parse($ofline['punch_out_date']);
                            $totalDuration  = $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S');

                            if(date('H:i:s',strtotime($totalDuration)) < date('H:i:s',strtotime("13:00:00"))){
                                $add_offline_work_logs->punch_out   = $ofline['punch_out'];
                            }else{
                                $add_offline_work_logs->punch_out   = Carbon::parse($ofline['punch_in'])->addHour(8);
                            }
                          
                          // *********************** New Logic ********************

                           
                             $add_offline_work_logs->save();
                        }else{
                  		    WorkLog::create($ofline);
                        }


                	}
                  
                }
                  

            endif;

            endforeach;

            return $this->success(true, trans('messages.OFFLINE_MARKED') );
        } 
        catch (Exception $e)
        { 
            return $this->error( [], trans('messages.EXECUTION_ERROR') );

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update worker status and attandence nby foreman.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
      * @OA\Post(
     *     path="/v1/project/update_out_in",
     *     tags={"Foreman Update OUT/IN"},
     *     summary=" Foreman can manage worker IN/OUT punch.",
     *     operationId="updateWorkerStatus",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Punch in marked id",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="punch_in",
     *         in="query",
     *         description="Punch in (UTC Time HH:MM:ss)",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="punch_at",
     *         in="query",
     *         description="Punch at value",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="worker_id",
     *         in="query",
     *         description="Worker Id",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="project_id",
     *         in="query",
     *         description="Project Id",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="punch_out",
     *         in="query",
     *         description="punch_out (UTC Time HH:MM:ss)",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="in_type",
     *         in="query",
     *         description="1:scan,2 manually,3: by foreman ",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="out_type",
     *         in="query",
     *         description="1:scan,2 manually,3: by foreman,4 Auto out ",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Record successfully update."
     *     )
     * )
     */
    public function updateWorkerStatus(Request $request){
        try{
            $worklog = new WorkLog();

            $existStatus = true;

            if( empty( $request->id )  )
            $existStatus = false;

        	if( !$existStatus )
        	{

        		if( WorkLog::where('worker_id', $request->worker_id)->whereNull('punch_out')->whereNotNull('punch_in')->exists() )
	        	{
	        		return $this->error( [], 'Worker already punched in.');
	        	}

        	}

            $worklog->exists = $existStatus;
            foreach($request->all() as  $key => $field):

                    $worklog->$key = $field;
            endforeach;
            $worklog->save();

            
            $data = WorkLog::where('id', $worklog->id)->orderBy('id','desc')->with('user')->first();

            return $this->success( $data, trans('messages.PUNCH_UPDATE'));
        }catch(Exception $e){
            return $this->error( [], trans('messages.EXECUTION_ERROR'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
      * @OA\Post(
     *     path="/v1/project/punch_out",
     *     tags={"Punch In & Out"},
     *     summary=" Punch out for workers.",
     *     operationId="update",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Punch in marked id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="in_type",
     *         in="query",
     *         description="1:scan,2 manually,3: by foreman ",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="out_type",
     *         in="query",
     *         description="1:scan,2 manually,3: by foreman,4 Auto out ",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Punch out marked successfully."
     *     )
     * )
     */
    public function update(Request $request)
    {
        try{

          $log =  WorkLog::with('project')->findOrFail($request->id);

          if($log && $log->punch_out == null){
            $worklog =  new WorkLog();
            $worklog->exists = true;
            $worklog->id = $request->id;
            $worklog->out_type = $request->out_type;
            $worklog->punch_out = Carbon::now()->toTimeString();
            if($request->device_info_in){
              $worklog->device_info_in = $request->device_info_in;  
            }

            if($request->device_info_out){
              $worklog->device_info_out = $request->device_info_out;  
            }
            $worklog->save();
          }
            return $this->success(WorkLog::with('project')->findOrFail($request->id),trans('messages.RECORD_UPDATE') );
        }catch(Exception $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
        * @OA\Delete(
     *     path="/v1/project/punch/{id}",
     *     tags={"delete"},
     *     summary=" Foreman can delete  by punch id.",
     *     operationId="destroy",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Punch in marked id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Record successfully removed."
     *     )
     * )
     */
    public function destroy($id)
    {
        try{
          $checkIfExists = WorkLog::where('id', $id)->count();
          if($checkIfExists == 0){
            return $this->error( [],"Seems puch is already deleted");
          }
            WorkLog::find($id)->delete();
            return $this->success( [],trans('messages.RECORD_DELETE'));
        }catch(Exception $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR'));
        }
    }


   /**
     * Punch status the specified resource from storage.
     * @return \Illuminate\Http\Response
       * @OA\get(
     *     path="/v1/project/punch/status",
     *     tags={"Punch In & Out"},
     *     summary="Last punch status for workers.",
     *     operationId="create",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Fetch last punch marked status."
     *     )
     * )
     */
    public function status(Request $request){
        try{
            $lastLog =  WorkLog::whereWorkerId(Auth::user()->id)->orderBy('id', 'desc')->with('project')->first();
            if($lastLog)
            $lastLog->isPunchOut =  $lastLog && empty($lastLog->punch_out)?false:true;
            return $this->success( $lastLog,trans('messages.PUNCH_STATUS') );
        }catch(Exception $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR'));
        }
    }
}
