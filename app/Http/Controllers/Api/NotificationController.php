<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ForgotPasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\UserDevice;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use File;
use Hash;
use Illuminate\Support\Str;
use Mail;
use App\Mail\ResetPasswordRequest;

class NotificationController extends ApiBaseController
{

	public function sendAndroidNotification( $usersId = '',$message = '' ,$messageJson = '', $title = '' )
    { 

		try {

			/*-------------------------------------------- Set Parameters --------------------------------------------*/

			
			if( empty( $title ) )
			{

				$title = "Verfico";

			}
			
			$notification = array(

				'title' => $title, 

				'body' => $messageJson['message'], 

				'text' => $messageJson['message'],

				'message' => $messageJson['message'],

				'type' => $messageJson['type'],

				'badge' => '1'

			);

			$arrayToSend = array(

				'registration_ids' => $usersId, 

				'data' => $notification,

				'notification' => $notification,

				'message' => $messageJson['message'],

				"priority" => "high"
				

			);


			$headers = array
				(
					'Authorization: key=AAAAMmWH7XI:APA91bFlA3wGKYvAbTU40VJHcH0N77n-NJBRXamaa5Ft4GYCLU290QOsD_Od8WmFOPn9f8cC0fLT8l-p5iFBSjctbSKwhKTVY2tUQUx01H29JYdIBt_VMqokmIqO1__l-IxEtOmWFw-_',
					'Content-Type: application/json'
				);

			/*-------------------------------------------- Set Parameters --------------------------------------------*/

			/*-------------------------------------------- Send notification --------------------------------------------*/
			
			$ch = curl_init();

			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );

			curl_setopt( $ch,CURLOPT_POST, true );

			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );

			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );

			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $arrayToSend ) );

			$result = curl_exec($ch );
			
			curl_close( $ch );

			/*-------------------------------------------- Send notification --------------------------------------------*/
			
			if ($result === FALSE)
			{

				die('FCM Send Error: ' . curl_error($ch));

			}

			$json = json_decode( $result, true );

			if( $json['success'] > 0 )
			{

				return 1;
			}
			else
			{

				return 0;

			}
        
        }
		catch (\Exception $e )
		{
		
			return 0;

		}
    }

}
