<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Http\Controllers\Api\NotificationController as NotificationController;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use DB;
use Carbon\Carbon;

class TestController extends ApiBaseController
{
    function testNotification()
    {
        /* Notification test */

            $msgTag = [];

           $msgJson = array
             (
                'message'      => 'This is a test notification with type (admin).',
                'icon'    => '',
                'type' => 'admin'
             );

            $notificationData = new NotificationController();

            $deviceTokens[] = 'ctpa6Ww13YU:APA91bH373jDK76vzNg3s3SUqVeygkFgXp8jlqftOyu0J7SnxJGwUHzXtUn_ZogoJ0-Dc32qr18o2odh4mUEpIIIWu3lh2wUdNfByksRVH7KMvpulumoY5pSjNpwyrfRMztRRCUbXfkL';

            $notificationSend = $notificationData->sendAndroidNotification( $deviceTokens, $msgTag, $msgJson );

            /* Notification test */

            if( $notificationSend )
            {
                echo 'sent';
            }
    }

}