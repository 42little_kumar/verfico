<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use App\User;
use App\WorkerType;
use Auth;
use File;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;
class ProfileController extends ApiBaseController
{

    /** 
     * Profile detail API 
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/user/profile",
     *     tags={"User"},
     *     summary="Get user profile.",
    
        * @OA\SecurityScheme(
        *     type="apiKey",
        *     in="header",
        *     securityScheme="api_key",
        *     name="Authorization"
        * ),
        *
     *     @OA\Response(
     *         response=200,
     *         description="User profile fetched successfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function index()
    {
        
       try{
       	$user =  User::with('sub_contractor_name')->findOrFail(Auth::user()->id);

        if( !empty( $user->ssn_id ) )
		  $user->ssn_id = $ssnPre = substr($user->ssn_id, 4, 8);

       	$user->image = '/uploads/images/users/'.$user->id.'/'.$user->image;

       	$user->worker = WorkerType::whereType($user->worker_type)->first();
            return  $this->success($user, trans('messages.FETCH') );
       }
       catch ( Exception $e )
       {
            return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);

       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ProfileRequest  $request
     * @return \Illuminate\Http\Response
     * Update Profile Api 
     * 
     *  @OA\Post(
     *     path="/v1/user/profile",
     *     tags={"User"},
     *     summary="Profile update.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="image",
     *         in="query",
     *         description="User Image",
     *         @OA\Schema(
     *             type="file"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="first_name",
     *         in="query",
     *         description="First Name",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="last_name",
     *         in="query",
     *         description="Last Name",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="Email Address",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="bio",
     *         in="query",
     *         description="Bio",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *       @OA\Parameter(
     *         name="worker_type",
     *         in="query",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="gender",
     *         in="query",
     *         description="Gender",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="ssn_id",
     *         in="query",
     *         description="3453",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="worker",
     *         in="query",
     *         description="1 = worker, 0 = foreman",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="phone_number",
     *         in="query",
     *         description="Mobile Number",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="parent",
     *         in="query",
     *         description="Company Id",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User succussfully update."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid parameters values."
     *     )
     * )
     */ 
    public function update(Request $request)
    {
        try{
            $request['id'] = Auth::user()->id;
            $user = new User();
            $user->exists = true;

            /* Phone validation */

            $userId = Auth::user()->id;

            $validator = Validator::make($request->all(), [

                "phone_number" => "unique:users,phone_number,$userId,id",

                "email" => "unique:users,email,$userId,id",

            ]);

            /* Phone validation */
            $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 

            if( !empty( $request->email ) )
            {
                if( !preg_match($regex, $request->email) )
                {
                    return $this->error( [], trans('messages.UNCORRECT_EMAIL_FORMAT')  );
                }
                
            }

            if( isset( $request->phone_number ) ):

                $string = $request->phone_number;
                $phoneNumber = preg_replace("/[^0-9\.]/", '', $string);

                if ( strlen($phoneNumber) < 10 || strlen($phoneNumber)>10 ) {
                    return $this->error( [], trans('messages.UNCORRECT_PHONE_NUMBER_FORMAT')  );
                }

                $request->phone_number = $phoneNumber;

            endif;

            if($validator->fails())
            {

                if($validator->errors()->first('phone_number')) {
            
                    return $this->error( [], 'Mobile number already taken.' );
            
                }

                if($validator->errors()->first('email') && !empty( $request->email ) ) {
            
                    return $this->error( [], 'Email already taken.' );
            
                }

            }

            /* Phone validation */

            foreach($request->all() as  $key => $field):
                //if(Auth::user()->hasRole("Worker") && $key != "phone_number")
                    $user->$key = $field;
                //if(Auth::user()->hasRole("Foreman") && $key != "email")
                    //$user->$key = $field;
            endforeach;

            /* SSN ID */

            if( $request->parent && $request->ssn_id ):

            $companyData = User::find( $request->parent );

            $companyName = $companyData->company_name;

            $companyName = preg_replace('/\s+/', '', $companyName);

            $ssnPre = substr($companyName, 0, 4);

            $finalSsd = strtoupper( $ssnPre.$request->ssn_id );

            $lastNameSsn = explode(' ', $request->last_name);

            $finalSsd .= strtoupper(substr(end($lastNameSsn), 0, 4));

            $finalSsd = substr($finalSsd,0,12);
            
            $user->ssn_id = $finalSsd;

        	endif;

            /* SSN ID */

            unset($user->worker);
            if($request->has('image'))
            {
                $image = $request->file('image');
                $imageName = $image->getClientOriginalName();

                $destination = public_path('uploads/images/users/'. Auth::user()->id);

                if(!File::isDirectory($destination)) {

                    mkdir($destination, 0777, true);

                    chmod($destination, 0777);

                }

                $imageName = Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

                //request()->image->move(public_path('uploads/images/users/'), $imageName);

                $image->move($destination, $imageName);

                $user->image = $imageName;
           }
            $user->save();
            $user = User::with('sub_contractor_name')->findOrFail(Auth::user()->id);
            $user->worker = WorkerType::whereType($user->worker_type)->first();
            return  $this->success( $user, trans('messages.RECORD_UPDATE'));
       }
       catch ( Exception $e )
       {
            return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);

       }
            
    }

    /** 
     * Language updated 
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/user/update/language",
     *     tags={"User"},
     *     summary="Update language.",
    
        * @OA\SecurityScheme(
        *     type="apiKey",
        *     in="header",
        *     securityScheme="api_key",
        *     name="Authorization"
        * ),
        *
     *     @OA\Response(
     *         response=200,
     *         description="User profile fetched successfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function updateLanguage( Request $request )
    {

        try
        {

            $userId = Auth::user()->id;

            $user = new User();

            $user->exists = true;

            $user->id = $userId;

            $user->lang = $request->header('X-localization');

            $user->save();
            
            return  $this->success( $user, 'Updated');

        }
        catch ( Exception $e )
        {

             return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
