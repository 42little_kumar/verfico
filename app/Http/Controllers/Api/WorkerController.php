<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use DB;
use App\WorkLog;
use App\Announcement;
use App\WorkerType;
use App\Project;
use Carbon\Carbon;
use Validator;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Mail;
use App\Mail\ApproveWorker;
use DateTime;
use DateTimeZone;

class WorkerController extends ApiBaseController
{



     /**
     * Display a listing of Project logs.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/project/logs/{id}",
     *     tags={"Punch In & Out Worker"},
     *     summary="{id}: project id, listing of project log dates",
     *     operationId="logDates",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Projects logs fetched successfully."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function logDates($id)
    {
        try{
            $data = DB::table('worker_log')
                ->select(DB::Raw('DATE(worker_log.punch_at) day'))
                ->whereProjectId($id)
                ->whereDate('punch_at', '>=', Carbon::now()->subDays(15))
                ->whereDate('punch_at', '<=', Carbon::now())
                ->groupBy('day')
                ->orderBy('day','desc')
                ->pluck('day')->toArray();

            //$data['dates'] = $datesArray;
            //$data['qr_code'] = Project::find( $id )->qrcode;

            return $this->success(array_filter( $data ) ,trans('messages.WORKER_LIST') );
        }catch(Excetion $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }

    /**
     * Display a listing of worker log in/out time.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/project/worker/{id}",
     *     tags={"Punch In & Out Worker"},
     *     summary="{id}: project id, listing log in/out for workers of a project. like: 'v1/project/worker/4?date=2020-03-24'",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="worker log in/out fetched successfully."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function index($id,Request $request)
    {
        try{

            $dataToSend = [];

            $dateEST =  $request->date; // 2020-05-04

            $startEstDate = $dateEST.' 00:00:00';

            //$endEstDate = Carbon::parse( $startEstDate )->addHours(24);

            $startUtcDate = $this->convertDateTime( $startEstDate );

            $endUtcDate = Carbon::parse( $startUtcDate )->addHours(24);

            //$endUtcDate = $this->convertDateTime( $endEstDate );

            $data = WorkLog::whereProjectId($id)

            //->whereDate('punch_at','=',Carbon::parse($logData)->toDateString())
            ->whereDate('punch_at','>=',Carbon::parse($startUtcDate)->toDateString())
            ->whereDate('punch_at','<=',Carbon::parse($endUtcDate)->toDateString())
            ->orderBy('punch_at','desc')
            ->with('user')->get();

            foreach( $data as $get )
            {

                $format = "d_m_y";
              
                $punchAt = explode(' ', $this->convertUtcToTimezone($get->punch_at))[0];

                if( $punchAt == $dateEST )
                    array_push($dataToSend, $get);


                

            }

            return $this->success($dataToSend ,trans('messages.WORKER_LIST') );
        }catch(Excetion $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }

    public function convertDateTime( $date )
    {

        list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

        $date = new \DateTime( $date, new DateTimeZone($timezone) );

        $date->setTimezone(new \DateTimeZone('UTC'));

        return $date->format('Y-m-d H:i:s');

    }

     public function convertUtcToTimezone( $date )
    {

        list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

        $date = new \DateTime( $date, new DateTimeZone('UTC') );

        $date->setTimezone(new \DateTimeZone($timezone));

        return $date->format('Y-m-d H:i:s');

    }

    /** 
     * Company API
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/workers/type/get",
     *     tags={"Worker"},
     *     summary="Get list of companies.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Worker type fetched successfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    
    function getWorkers( Request $request )
    {

        try
        {
            
            $data = [];

            $data = WorkerType::select('id', 'type as name')->get();

            $message = 'Worker type fetched successfully.';

            return $this->success( $data, $message );

        }
        catch ( Exception $e )
        {
            
            $data = [];

            $error = 'Some error!';

        }

     }

      /** 
     * Filter Worker API
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/worker",
     *     tags={"Worker"},
     *     summary="Filter worker list by its first and last name.? kewords=test",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Listing of workers."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
     public function filterWorkers(Request $request){
        try
        {
            $keyword = $request->input('keywords');

            $data = User::role(['worker', 'foreman', 'safetyo'])
            ->where('status', 2);
            
               $data =  $data->whereRaw("concat(first_name, ' ', last_name) like '%$keyword%' ");
            
            if(!empty($request->input('sub_id'))){
                $sub_id = $request->input('sub_id');

                $data = $data->where('parent', $sub_id);
            }
            if(!empty($request->input('worker_type'))){
                
                $worker_type = $request->input('worker_type');
                $getWorkerTypeId = WorkerType::select('id')->where('type', $worker_type)->first();
                if(!empty($getWorkerTypeId->id)){

                   $data = $data->where('worker_type', $getWorkerTypeId->id); 
                }
            }
           $result = $data->get();



            return $this->success( $result, trans('messages.WORKER_LIST') );
        }catch ( Exception $e ){
            return $this->error( [], trans('messages.EXECUTION_ERROR') );
        }
    }

       /** 
     * Pending for approval
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/worker/pending",
     *     tags={"Worker"},
     *     summary="Pending for approval",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="E.g. 1",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Listing of pending worker request."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function pendingApproval(Request $request){
        try
        {
            $page = $request->has('page') ? $request->get('page') : 1;
            $limit = $request->has('limit') ? $request->get('limit') : 25;

            $data = User::whereStatus(1)->role('worker')->limit($limit)->offset(($page - 1) * $limit)->paginate(25);
            return $this->success( $data,trans('messages.WORKER_LIST') );
        }catch ( Exception $e ){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }

    /** 
     * Get workers request count
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/workers/request/count",
     *     tags={"Worker"},
     *     summary="Count of the workers request",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Workers request count."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function getWorkersRequestCount(Request $request){
        try
        {

            $data['count'] = User::role('worker')->where('status', 1)->count();

            return $this->success( $data, 'Workers request count.' );

        }catch ( Exception $e ){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }
       /** 
     *  Approve
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Post(
     *     path="/v1/worker/update",
     *     tags={"Worker"},
     *     summary="Approval/Edit/Reject Worker Account",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="worker id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description=" status 0:disable,1:pending,2:approval,3:reject",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="image",
     *         in="query",
     *         description="User Image",
     *         @OA\Schema(
     *             type="file"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="first_name",
     *         in="query",
     *         description="First Name",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="last_name",
     *         in="query",
     *         description="Last Name",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="Email Address",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="bio",
     *         in="query",
     *         description="Bio",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *       @OA\Parameter(
     *         name="worker_type",
     *         in="query",
     *         description="ID",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="gender",
     *         in="query",
     *         description="Gender",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="phone_number",
     *         in="query",
     *         description="Mobile Number",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="ssn_id",
     *         in="query",
     *         description="3453",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="parent",
     *         in="query",
     *         description="Company Id",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Worker successfully approved."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function update(Request $request){
        try
        {
            $user = new User();
            $user->exists = true;

            $userId = $request->id;

            if( User::find( $request->id )->status != 1 )
                return $this->error( [], trans('messages.ACTION_ALREADY_PERFORMED') );

            $validator = Validator::make($request->all(), [

                "phone_number" => "unique:users,phone_number,$userId,id",

                "email" => "unique:users,email,$userId,id",

            ]);

            $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 

            if( !empty( $request->email ) )
            {
                if( !preg_match($regex, $request->email) )
                {
                    return $this->error( [], trans('messages.UNCORRECT_EMAIL_FORMAT')  );
                }
                
            }

            if( isset( $request->phone_number ) ):

                $string = $request->phone_number;
                $phoneNumber = preg_replace("/[^0-9\.]/", '', $string);

                if ( strlen($phoneNumber) < 10 || strlen($phoneNumber)>10 ) {
                    return $this->error( [], trans('messages.UNCORRECT_PHONE_NUMBER_FORMAT')  );
                }

                $request->phone_number = $phoneNumber;

            endif;

            if($validator->fails())
            {

                if($validator->errors()->first('phone_number')) {
            
                    return $this->error( [], 'Mobile number already taken.' );
            
                }

                if($validator->errors()->first('email') && !empty( $request->email )) {
            
                    return $this->error( [], 'Email already taken.' );
            
                }

            }

            foreach($request->all() as  $key => $field):
                if($field)
                    $user->$key = $field;
            endforeach;
            if($request->has('image'))
            {
                $image = $request->file('image');
                $imageName = $image->getClientOriginalName();

                $destination = public_path('uploads/images/users/'. $request->id);

                if(!File::isDirectory($destination)) {

                    mkdir($destination, 0777, true);

                    chmod($destination, 0777);

                }

                $imageName = $request->id . time() . '.' . $image->getClientOriginalExtension();

                $image->move($destination, $imageName);

                $user->image = $imageName;
           }

           /* SSN ID */

            if( $request->parent ):

            if( empty( $request->ssn_id ) )
            {

                $user->ssn_id = null;

            }
            else
            {

                $companyData = User::find( $request->parent );

                $companyName = $companyData->company_name;

                $companyName = preg_replace('/\s+/', '', $companyName);

                $ssnPre = substr($companyName, 0, 4);

                $finalSsd = strtoupper( $ssnPre.$request->ssn_id );

                $lastNameSsn = explode(' ', $request->last_name);

                $finalSsd .= strtoupper(substr(end($lastNameSsn), 0, 4));

                $finalSsd = substr($finalSsd,0,12);

                $user->ssn_id = $finalSsd;
                
            }
            

            endif;

            /* SSN ID */

            $user->save();

            if( $request->status == '3' )
                return $this->success( [], trans('messages.ACCOUNT_REJECTED') );

            // Trigger approve worker email
            $this->triggerApproveWorkerEmail( $user->id );

            return $this->success( [], trans('messages.ACCOUNT_APPROVED') );
        }catch ( Exception $e ){
            return $this->error( [], trans('messages.EXECUTION_ERROR') );
        }
    }

    /*
     *
     *
     *
     */
    public function triggerApproveWorkerEmail( $id )
    {

        $userData = User::where('id', $id)->first();

        $emailUserData = [];

        $emailUserData['first_name'] = $userData->first_name;
        $emailUserData['name'] = 'Name: '.$userData->first_name.' '.$userData->last_name.'<br>';
        $emailUserData['email'] = $userData->email;
        $emailUserData['phone_number'] = '';

        $workerCompanyfieldForEmail = 'Company: '.getCompanyNameFromId( $userData->parent );


        if( !empty( $userData->phone_number ) )
        {

          $emailUserData['phone_number'] = 'Phone number: '.$userData->phone_number.'<br>';

        }

        //
        if( !empty( $userData->email ) )
        Mail::to( $userData->email )->send(new ApproveWorker((object)$emailUserData, 'Mail', '<i>Same as you registered.</i>', $workerCompanyfieldForEmail));

    }

    /** 
     * Get workers request count
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/annoucements",
     *     tags={"Worker"},
     *     summary="Count of the workers request",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Workers request count."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function getAnnce( Request $request )
    {

        if( Auth::user()->hasRole('Foreman') )
        {
            $role = 'foreman';
        }

        if( Auth::user()->hasRole('Worker') )
        {
            $role = 'worker';
        }

        if( Auth::user()->hasRole('Safetyo') )
        {
            $role = 'worker';
        }
        
        $data = Announcement::where( 'type', $role )->first();

        $response['show_popup'] = $data->show_popup;

        $response['announcement'] = $data->announcement_content;

        return $this->success( $response, 'Annoucements fetched successfully!' ); 

    }
}

