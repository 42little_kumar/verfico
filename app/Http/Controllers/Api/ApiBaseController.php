<?php

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Verfico API documentation",
 *      description="Verfico API endpoints",
 *      @OA\Contact(
 *          email="gursimrat@42works.net"
 *      )
 * )
 */

/**
 *  @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Rockspring server"
 *  )
 */

namespace App\Http\Controllers\Api;

use FCM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiBaseController extends Controller
{

	/*
	 * @Function Name
	 *
	 *
	 */
    public function success($result, $message)
    {
    $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    /*
     * @Function Name
     *
     *
     */
    public function error($error, $errorMessages = [], $code = 404)
    {
    /*$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);*/

        $response = [

                'success' => false,

                'data'    => $error,

                'message' => $errorMessages,

            ];


        return response()->json( $response, $code );
    }

    /*
     * @Function Name
     *
     *
     */
    public function send_notificaton($device, $data)
    {

        /*$optionBuilder = new OptionsBuilder();

        $optionBuilder->setTimeToLive(60*20);

        if($device->type == "ios") {

            $notificationBuilder = new PayloadNotificationBuilder($data['title']);

            $notificationBuilder->setBody($data['message'])->setSound('default');


        } else {

            $notificationBuilder = new PayloadNotificationBuilder();

        }

        $dataBuilder = new PayloadDataBuilder();

        $data = [

            'title' => $data['title'],

            'message' => $data['message'],

            'type' => $data['type'],

            'id' => $data['id']

        ];
        
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();

        $notification = $notificationBuilder->build();

        $notification_data = $dataBuilder->build();

        return FCM::sendTo($device->fcm_token, $option, $notification, $notification_data);*/

    }

}
