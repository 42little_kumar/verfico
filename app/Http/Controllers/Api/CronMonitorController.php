<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ForgotPasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\UserDevice;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use File;
use Hash;
use Illuminate\Support\Str;
use Mail;
use Validator;
use App\Mail\ResetPasswordRequest;
use App\Http\Controllers\Api\NotificationController as NotificationController;
use DB;
use App\PasswordReset;
use App\Mail\ResetPassword;
use App\Mail\ResetPasswordAdmin;
use App\Mail\ConfirmRegistrationMobileWorker;

class CronMonitorController extends ApiBaseController
{

/** 
     * Get timezone
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/cron/monitor",
     *     tags={"Cron Monitor"},
     *     summary="Cron Monitor.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *         description="Set X-localization in header for Language en OR sp",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=""
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
     public function cronMonitor(){

        try{

            DB::table('cron_monitor')->insert(
    			[
    				'cron_name' => 'Auto log out worker cron.',
    				'status' => 1,
    			]
			);

            $data = [];
            $message = 'Cron Monitored';

            return $this->success($data , $message );

        }
        catch ( Exception $e ){

             return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);

        }

     }

}