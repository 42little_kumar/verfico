<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;

class CompanyController extends ApiBaseController
{

    /** 
     * Company API
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/companies/get",
     *     tags={"Company"},
     *     summary="Get list of companies.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Company list fetched successfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */

    function getCompanies( Request $request )
    {

        try
        {

            $subContractors = User::role('subcontractor')->select('id', 'company_name as name')->get();

            $data = $subContractors;
            return $this->success( $data, trans('messages.FETCH') );

        }
        catch ( Exception $e )
        {
            return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);
        }

     }
}
