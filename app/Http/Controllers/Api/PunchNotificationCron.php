<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Http\Controllers\Api\NotificationController as NotificationController;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use DB;
use Carbon\Carbon;

class PunchNotificationCron extends ApiBaseController
{
    function sendTimeOutReminder()
    {
        /* Notification test */

            $msgTag = [];

           $msgJson = array
             (
                'message'      => 'Please remember to check out before leaving the job site.',
                'icon'    => '',
                'type' => 'admin'
             );

            $notificationData = new NotificationController();

            $worklogs  = DB::table('worker_log')->wherePunchOut(null)->where('punch_in','<',Carbon::now()->subHours(7))->where('is_notified', 0)->get();

            if(count($worklogs)):
                foreach($worklogs as $worklog  ):

                    $deviceTokens = [];
                    
                    $workerData = User::find( $worklog->worker_id );

                    foreach( $workerData->tokens as $userDevices )
                    {
                        array_push( $deviceTokens, $userDevices->fcm_token );
                    }

                    $msgJson['message'] = 'Please remember to check out before leaving the job site.';

                    if( $workerData->lang == 'es' )
                      $msgJson['message'] = 'Por favor recuerde revisar antes de salir del lugar de trabajo.';


                    $notificationSend = $notificationData->sendAndroidNotification( $deviceTokens, $msgTag, $msgJson );

                    /* Update notification status */

                    if( $notificationSend ):

                    DB::table('worker_log')
                    ->where('id', $worklog->id)
                    ->update(['is_notified' => 1]);

                    endif;

                    /* Update notification status */

                endforeach;
            endif;

            /* Notification test */
    }

}