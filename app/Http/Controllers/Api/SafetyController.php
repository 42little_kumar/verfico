<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use DB;
use App\WorkLog;
use App\Announcement;
use App\WorkerType;
use App\Strike;
use App\StrikeCategories;
use App\Project;
use Carbon\Carbon;
use Validator;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Mail;
use App\Mail\ApproveWorker;
use DateTime;
use DateTimeZone;

class SafetyController extends ApiBaseController
{



     /**
     * Display a listing of Project logs.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/safety/strike/list",
     *     tags={"Safety Strike"},
     *     summary="Listing For The Safety Strike",
     *     @OA\Parameter(
     *         name="keywords",
     *         in="query",
     *         description="Keyword",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
      *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="page=2",
     *         required=false,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Safety Strike Listing Fetched Successfully."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function list( Request $request )
    {
        try{

        	$userIds = [];
        	$projectIds = [];

        	//

        	if( !empty( $request->keywords ) )
        	{
        		$userIds = User::where('name','like',  '%' . $request->keywords . '%')->pluck('id')->toArray();;
        	}

           	//

        	if( !empty( $request->keywords ) )
        	{
        		$projectIds = Project::where('job_name','like',  '%' . $request->keywords . '%')
        		->orWhere('job_number','like',  '%' . $request->keywords . '%')
        		->pluck('id')->toArray();;
        	}

           	//
            //dd($userIds);
         

           $strikeDataObj = Strike::whereYear('strike_date', Carbon::now()->year)
                        ->whereMonth('strike_date', Carbon::now()->month);

            if( !empty( $projectIds ) && !empty( $userIds ) )
            {


             
                $strikeDataObj->whereIn('created_by', $userIds)->orWhereIn('worker_id', $userIds);

                $strikeDataObj->orWhereIn('name', $projectIds);
                
            }
            else
            {

                if( !empty( $projectIds ) ){

                $strikeDataObj->whereIn('name', $projectIds);
                }

                if( !empty( $userIds ) ){
                    $strikeDataObj->whereIn('created_by', $userIds)->orWhereIn('worker_id', $userIds);
                }

            }
        
            //

            $strikeData = $strikeDataObj->orderBy('id', 'desc')->paginate(50);

            //

            if( empty( $projectIds ) && empty( $userIds ) && !empty( $request->keywords ) )
            {
                $strikeData = [];

                $strikeData = (object) $strikeData;
            }

            //

            return $this->success( $strikeData ,trans('messages.SAFETY_VIEW') );
        }catch(Excetion $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }

    /**
     * Display a listing of Strike Category.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/safety/strike/category/list",
     *     tags={"Safety Strike"},
     *     summary="Listing For The Safety Strike Categories",
    *     @OA\Response(
    *         response=200,
    *         description="Safety Strike Category Listing Fetched Successfully."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function getCategories()
    {
        try{
           
           $strikeData = StrikeCategories::get();

            return $this->success( $strikeData ,'Strike category list fetched successfully.' );
        }catch(Excetion $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }

    /**
     * Display a listing of Project logs.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/safety/strike/worker/list",
     *     tags={"Safety Strike"},
     *     summary="Listing For The Workers",
    *     @OA\Response(
    *         response=200,
    *         description="Worker Listing Fetched Successfully."
    *     ),
     *     @OA\Parameter(
     *         name="keywords",
     *         in="query",
     *         description="Name of worker",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function workersList( Request $request )
    {
        try{

            
           
           $workers = User::role(['Worker','Safetyo'])->where('status', 2)->where('name','like',  '%' . $request->keywords . '%')->get();

            return $this->success( $workers ,'Worker list fetched successfully.' );
        }catch(Excetion $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }

    /**
     * Delete safety strike.
     *
     * @return \Illuminate\Http\Response
     * @OA\Post(
     *     path="/v1/safety/strike/delete",
     *     tags={"Safety Strike"},
     *     summary="Delete safety strike",
    *     @OA\Response(
    *         response=200,
    *         description="Delete safety strike."
    *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Id of safety strike",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function destroy( Request $request )
    {
        try{

            
           
           Strike::find( $request->id )->delete();

            return $this->success( [] ,'Safety strike deleted successfully.' );
        }catch(Excetion $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );
        }
    }

 
       /** 
     *  Create
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Post(
     *     path="/v1/safety/strike/create",
     *     tags={"Safety Strike"},
     *     summary="Create Safety Strike Request",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Send project ID in this parameter it will not accept the string value.",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="worker_id",
     *         in="query",
     *         description="worker id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="created_id",
     *         in="query",
     *         description="Created By Id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="strike_date",
     *         in="query",
     *         description="Strike Date (dd-mm-yyyy e.g. 23-11-2020)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
      *     @OA\Parameter(
     *         name="category",
     *         in="query",
     *         description="Name of the strike category",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Description",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="media",
     *         in="query",
     *         description="photos/video",
     *         @OA\Schema(
     *             type="file"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Safety Strike Request Created Successfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function create(Request $request){
        try
        {
          
            $strike = [];

            $strike['created_by'] = $request->created_id;
            $strike['worker_id'] = $request->worker_id;
            $strike['name'] = $request->name;
            $strike['category'] = $request->category;
            $strike['description'] = $request->description;
            $strike['strike_date'] = date( 'Y-m-d H:i:s', strtotime($request->strike_date) );


            /* Image */

            if($request->has('media'))
            {
                $image = $request->file('media');
                $imageName = $image->getClientOriginalName();

 

                $destination = public_path('uploads/strike/');

                if(!File::isDirectory($destination)) {

                    mkdir($destination, 0777, true);

                    chmod($destination, 0777);

                }

                $imageName = 'strike_' . time() . '.' . $image->getClientOriginalExtension();

                //request()->image->move(public_path('uploads/images/users/'), $imageName);

                $image->move($destination, $imageName);

           
            }
            /* Image */

            //$strike['media'] = $imageName;

            Strike::create( $strike );

            return $this->success( [], trans('messages.SAFETY_CREATED') );
        }catch ( Exception $e ){
            return $this->error( [], trans('messages.EXECUTION_ERROR') );
        }
    }

     /** 
     *  Edit
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Post(
     *     path="/v1/safety/strike/update",
     *     tags={"Safety Strike"},
     *     summary="Create Safety Strike Request",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Id of the safety strike",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Name",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="worker_id",
     *         in="query",
     *         description="worker id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="created_id",
     *         in="query",
     *         description="Created By Id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="strike_date",
     *         in="query",
     *         description="Strike Date (dd-mm-yyyy e.g. 23-11-2020)",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
      *     @OA\Parameter(
     *         name="category",
     *         in="query",
     *         description="Name of the strike category",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Description",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="media",
     *         in="query",
     *         description="photos/video",
     *         @OA\Schema(
     *             type="file"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Safety Strike Request Created Successfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description=""
     *     )
     * )
     */
    public function update(Request $request){
        try
        {
          
            $strike = [];

            if(isset( $request->created_id ))
            $strike['created_by'] = $request->created_id;

            if(isset( $request->worker_id ))
            $strike['worker_id'] = $request->worker_id;

            if(isset( $request->name ))
            $strike['name'] = $request->name;

            if(isset( $request->category ))
            $strike['category'] = $request->category;

            if(isset( $request->description ))
            $strike['description'] = $request->description;

            if( isset( $request->strike_date) )
            $strike['strike_date'] = date( 'Y-m-d H:i:s', strtotime($request->strike_date) );


            /* Image */

            if($request->has('media'))
            {
                $image = $request->file('media');
                $imageName = $image->getClientOriginalName();

 

                $destination = public_path('uploads/strike/');

                if(!File::isDirectory($destination)) {

                    mkdir($destination, 0777, true);

                    chmod($destination, 0777);

                }

                $imageName = 'strike_' . time() . '.' . $image->getClientOriginalExtension();

                //request()->image->move(public_path('uploads/images/users/'), $imageName);

                $image->move($destination, $imageName);

           
            }
            /* Image */

            //$strike['media'] = $imageName;

            $strikeObject = Strike::find( $request->id );

            //

         
            //

            $strikeObject->update( $strike );

            $strikeObject = Strike::find( $request->id );

            return $this->success( $strikeObject, 'Safety strike updated successfully.' );
        }catch ( Exception $e ){
            return $this->error( [], trans('messages.EXECUTION_ERROR') );
        }
    }

   
}

