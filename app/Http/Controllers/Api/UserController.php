<?php

namespace App\Http\Controllers\Api;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ForgotPasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\UserDevice;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use File;
use Hash;
use Illuminate\Support\Str;
use Mail;
use Validator;
use App\Mail\ResetPasswordRequest;
use App\Http\Controllers\Api\NotificationController as NotificationController;
use DB;
use App\PasswordReset;
use App\Mail\ResetPassword;
use App\Mail\ResetPasswordAdmin;
use App\Mail\ConfirmRegistrationMobileWorker;
use App\Mail\NewRegistrationMobileWorkerNotificationToAdmin;

class UserController extends ApiBaseController
{
    public $successStatus = 200;

     /** 
     * Login API 
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Post(
     *     path="/v1/user/login",
     *     tags={"User"},
     *     summary="User login request.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *         description="Set X-localization in header for Language en OR sp",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="User email",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="User password",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="worker",
     *         in="query",
     *         description="1 = worker, 0 = foreman",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="device_id",
     *         in="query",
     *         description="Device Id",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="device_type",
     *         in="query",
     *         description="Device type",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="device_token",
     *         in="query",
     *         description="Device token",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User has been succussfully logged in."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid email/password."
     *     )
     * )
     */

    function login( LoginRequest $request )
    {

        /*-------------------------------------------- Seperator --------------------------------------------*/
       $user =  User::whereEmail($request->email)->orWhere('phone_number',$request->email)->first();

        /*Auth::loginUsingId($user->id);

       $user = Auth::user(); 
            $data['token'] =  $user->createToken('Verfico')->accessToken; 
            $data['user_type'] = 1;
            $data['users'] = $user;
            return $this->success( $data, trans('messages.LOGIN_SUCCESS') );*/

      if(!$user)
        return $this->error( [],trans('messages.LOGIN_VALID'));

        /* Save user device */

        UserDevice::where('api_token', '=', $request->device_id)->delete();

        $deviceData = [];

        $deviceData['type'] = $request->device_type;

        $deviceData['user_id'] = $user->id;

        $deviceData['api_token'] = $request->device_id;

        $deviceData['fcm_token'] = $request->device_token;

        UserDevice::create( $deviceData );

        /* Check is email or number */

        if (is_numeric(request('email')))
        {

            $field = 'phone_number';

        }
        else
        {

            $field = 'email';

        }

        /* Check is email or number */

        /* Save user device */

        if( $user )
        {

            if( $user->hasRole("Subcontractor") )
            {
                return $this->error( [], trans('messages.UNAUTH_ROLE') );
            }

            if( $user->hasRole("Admin") )
            {
                return $this->error( [], trans('messages.UNAUTH_ROLE') );
            }

        }

        if( $user && $user->hasRole("Safetyo") )
        {
            if($user->status == 3)
                return $this->error( [], trans('messages.UNAUTH') );
            if($user->status == 1)
                return $this->error( [],trans('messages.APPROVE'));
            if($user->status == 0)
                return $this->error( [],trans('messages.BLOCK_ACCOUNT'));

            if( Auth::attempt( [$field => request('email'), 'password' => request('password')] ) )
            { 
                $user = Auth::user(); 
                $user->device_info = $request->device_info;
                $user->save();
                $data['token'] =  $user->createToken('Verfico')->accessToken; 
                $data['user_type'] = 2;
                $data['users'] = $user;
                return $this->success( $data, trans('messages.LOGIN_SUCCESS') );
            }  
            else
            { 
                return $this->error( [], trans('messages.UNAUTH') );

            } 

        }


       if( $user && $user->hasRole("Worker") )
        {
            if($user->status == 3)
                return $this->error( [], trans('messages.UNAUTH') );
            if($user->status == 1)
                return $this->error( [],trans('messages.APPROVE'));
            if($user->status == 0)
                return $this->error( [],trans('messages.BLOCK_ACCOUNT'));

            if( Auth::attempt( [$field => request('email'), 'password' => request('password')] ) )
            { 
                $user = Auth::user(); 
                $user->device_info = $request->device_info;
                $user->save();
                $data['token'] =  $user->createToken('Verfico')-> accessToken; 
                $data['user_type'] = 0;
                $data['users'] = $user;
                return $this->success( $data, trans('messages.LOGIN_SUCCESS') );
            }  
            else
            { 
                return $this->error( [], trans('messages.UNAUTH') );

            } 

        }

        /*-------------------------------------------- Seperator --------------------------------------------*/

        if( Auth::attempt( [$field => request('email'), 'password' => request('password')] ) )
        { 
            $user = Auth::user(); 
            $user->device_info = $request->device_info;
            $user->save();
            $data['token'] =  $user->createToken('Verfico')-> accessToken; 
            $data['user_type'] = 1;
            $data['users'] = $user;
            return $this->success( $data, trans('messages.LOGIN_SUCCESS') );

        } 
        else
        { 
            return $this->error( [], trans('messages.UNAUTH') );

        } 

        /*-------------------------------------------- Seperator --------------------------------------------*/

     }

     /** 
     * Check user
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/user/check",
     *     tags={"User"},
     *     summary="Check User.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *         description="Set X-localization in header for Language en OR sp",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="user_id",
     *         in="query",
     *         description="User Id",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User has been succussfully logged in."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid email/password."
     *     )
     * )
     */
     public function checkUser( Request $request )
     {

        try
        {

            $userData = User::withTrashed()->where( 'id', $request->user_id )->first();

            $message = trans('messages.USER_EXISTS');

             $data['is_deleted'] = 0;
             $data['is_blocked'] = 0;

            if( $userData->deleted_at != NULL )
            {
                $data['is_deleted'] = 1;
                $message = trans('messages.IS_DELETED');
            }
            

            if( $userData->status == 0 )
            {
                $data['is_blocked'] = 1;
                $message = trans('messages.IS_BLOCKED');
            }

            return $this->success($data , $message );

        }
        catch ( Exception $e )
        {

             return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);

        }

     }

     /** 
     * Get timezone
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Get(
     *     path="/v1/user/get/timezone",
     *     tags={"User"},
     *     summary="Check User.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *         description="Set X-localization in header for Language en OR sp",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User has been succussfully logged in."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid email/password."
     *     )
     * )
     */
     public function getTimezone( Request $request )
     {

        try
        {

            list( $timezone, $timeDifference ) = explode(',', User::find(1)->timezone_manual);

            $data['timezone'] = $timezone;
            $data['time_difference'] = $timeDifference;

            $message = trans('Timezone fetched successfully.');

            return $this->success($data , $message );

        }
        catch ( Exception $e )
        {

             return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);

        }

     }

    /** 
     * Forgot password request API 
     * 
     * @return \Illuminate\Http\Response 
     * @OA\Post(
     *     path="/v1/user/forgot_password",
     *     tags={"User"},
     *     summary="Forgot password.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *         description="Set X-localization in header for Language en OR sp",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="User email",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="worker",
     *         in="query",
     *         description="1 = worker, 0 = foreman",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User has been succussfully logged in."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid email/password."
     *     )
     * )
     */

    function forgotPassword( ForgotPasswordRequest $request )
    {

       try
       {
            
            // Get email
            $email = $request->email;

            // Get email from phone number
            if (is_numeric($email))
            {
                $field = 'phone_number';

                $user = User::where( 'phone_number', $request->email )->first();

                if( !$user ) {

                    return $this->error( [], 'Account not found against the provided phone number.' );

                }

                $inputVal = $request->email;

                $email = $user->email;
            
            }
            else
            {
                $field = 'email';

                $user = User::where( 'email', $request->email )->first();

                if( !$user ) {

                    return $this->error( [], 'Account not found against the provided email.' );

                }

                $inputVal = $request->email;

                $email = $user->email;

            }

            if( empty( $email ) )
            {
                $superadmin = User::where('username', 'superadmin')->first();

                Mail::to( @$superadmin->email )->send(new ResetPasswordAdmin($user));

                return $this->success( [], trans('messages.TO_ADMIN_SENT') );

            }

            if($user->reset_password) {

                PasswordReset::where('email', $email)->update([ 'token' => Str::random(40) ]);

            } else {

                PasswordReset::create([

                    'email' => $email,

                    'token' => Str::random(40)

                ]);

            }

            $reset = PasswordReset::where('email', $email)->first();

            Mail::to($user)->send(new ResetPassword($reset));
            
            return $this->success( [], trans('messages.CHECK_RESET_EMAIL') );

       }
       catch ( Exception $e )
       {
            return $this->error( [], trans('messages.EXECUTION_ERROR') );

       }

    }

    /** 
     * Register Api 
     * 
     * @return \Illuminate\Http\Response 
     *  @OA\Post(
     *     path="/v1/user/change-password",
     *     tags={"User"},
     *     summary="Change password.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *         description="Set X-localization in header for Language en OR sp",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description="password",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
   
     *     @OA\Response(
     *         response=200,
     *         description="Password successfully changed."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid parameters values."
     *     )
     * )
     */ 
    public function changePassword(Request $request){

        try{
            $user = User::findOrFail(Auth::user()->id);
            $user->fill([
                'password' => Hash::make($request->password)
                ])->save();
            return $this->success( [], trans('messages.CHANGE_PASS_SUCCESS') );
            
        }catch(Exception $e){
            return $this->error( [],trans('messages.EXECUTION_ERROR') );

        }

    }

    /** 
     * Register Api 
     * 
     * @return \Illuminate\Http\Response 
     *  @OA\Post(
     *     path="/v1/user/register",
     *     tags={"User"},
     *     summary="Register user request.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="X-localization",
     *         in="header",
     *         description="Set X-localization in header for Language en OR sp",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="username",
     *         in="query",
     *         description="User name (Unique)",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="image",
     *         in="query",
     *         description="User Image",
     *         required=true,
     *         @OA\Schema(
     *             type="file"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="first_name",
     *         in="query",
     *         description="First Name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="last_name",
     *         in="query",
     *         description="Last Name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="email",
     *         in="query",
     *         description="Email Address",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="phone_number",
     *         in="query",
     *         description="Mobile Number",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="ssn_id",
     *         in="query",
     *         description="3453",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="parent",
     *         in="query",
     *         description="Company Id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="worker_type",
     *         in="query",
     *         description="ID",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="query",
     *         description=" Password",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="device_id",
     *         in="query",
     *         description="Device Id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="device_type",
     *         in="query",
     *         description="Device Type",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="device_token",
     *         in="query",
     *         description="Device Token",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User registered succussfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid parameters values."
     *     )
     * )
     */ 
    public function register(UserRequest $request) 
    { 
        try{
            $input = $request->all(); 
            $input['password'] = bcrypt($input['password']); 

            /* Phone validation */

            $validator = Validator::make($request->all(), [

                "email" => "unique:users,email",

            ]);

            if($validator->fails())
            {

                if($validator->errors()->first('email') && !empty( $request->email ) ) {
            
                    return $this->error( [], 'Email has already been taken.' );
            
                }

            }

            /* Phone validation */
            $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 

            if( !empty( $request->email ) )
            {
                if( preg_match($regex, $request->email) )
                {
                    Mail::to($request->email)->send(new ConfirmRegistrationMobileWorker($request, 'Mail'));
                }
                else
                {
                    return $this->error( [], trans('messages.UNCORRECT_EMAIL_FORMAT')  );
                }
                
            }
            
            if( isset( $input['phone_number'] ) ):

                $string = $input['phone_number'];
                $phoneNumber = preg_replace("/[^0-9\.]/", '', $string);

                if ( strlen($phoneNumber) < 10 || strlen($phoneNumber)>10 ) {
                    return $this->error( [], trans('messages.UNCORRECT_PHONE_NUMBER_FORMAT')  );
                }

                $input['phone_number'] = $phoneNumber;

            endif;

            $input['name'] = $input['first_name'].' '.$input['last_name'];
            $input['role'] = 'worker';
            $user = User::create($input);

            

            /*-------------------------------------------- Seperator --------------------------------------------*/
             if($request->file('image'))
             {
                $image = $request->file('image');
                $imageName = $image->getClientOriginalName();

                $destination = public_path('uploads/images/users/'. $user->id);

                if(!File::isDirectory($destination)) {

                    mkdir($destination, 0777, true);

                    chmod($destination, 0777);

                }

                $imageName = $user->id . time() . '.' . $image->getClientOriginalExtension();
                
                $image->move($destination, $imageName);

                $user->image = $imageName;

            }

            /* SSN ID */

            if( $request->parent && $request->ssn_id ):

            $companyData = User::find( $request->parent );

            $companyName = $companyData->company_name;

            $companyNameOrig = $companyName;

            $companyName = preg_replace('/\s+/', '', $companyName);

            $ssnPre = substr($companyName, 0, 4);

            //$user->ssn_id = strtoupper( $ssnPre.$request->ssn_id );

            $finalSsd = strtoupper( $ssnPre.$request->ssn_id );

            $lastNameSsn = explode(' ', $request->last_name);

            $finalSsd .= strtoupper(substr(end($lastNameSsn), 0, 4));
            
            $finalSsd = substr($finalSsd,0,12);

            $user->ssn_id = $finalSsd;

            endif;

            /* SSN ID */


            

            $user->save();
            $userRole = 'worker';
            $user->assignRole( $userRole );

            //$this->notificationAllForemen( $user );

            /*-------------------------------------------- Seperator --------------------------------------------*/
            $data['token'] =  $user->createToken('Verfico')-> accessToken; 
            $data['user'] =  $user;

            if($companyNameOrig=="Rock Spring Contracting"){
                //,"nitesh@42works.net","sonika@42works.net","raman@42works.net"
            Mail::to([config('app.adminemail')])->send(new NewRegistrationMobileWorkerNotificationToAdmin($request, 'Mail'));
        }
            return $this->success( $data, trans('messages.REGISTER_SUCCESS') );
        }
        catch (Exception $e)
        {
            return $this->error( $data, trans('messages.EXECUTION_ERROR')  );

        }

    }

    /*
    *
    *
    *
    */
    public function notificationAllForemen( $user )
    {

        /* Notification test */

       $msgTag = [];

       $msgJson = array
         (
            'message'      => 'A new worker has registered. Please approve or reject the same..',
            'icon'    => '',
            'type' => 'request'
         );

        $notificationData = new NotificationController();

        $formen = User::role('foreman')->get();

        

        foreach ($formen as $forman)
        {

            $deviceTokens = [];
           
            foreach( $forman->tokens as $userDevices )
            {
                array_push( $deviceTokens, $userDevices->fcm_token );
            }

            $notificationSend = $notificationData->sendAndroidNotification( $deviceTokens, $msgTag, $msgJson );

        }

        /* Notification test */

    }

}
