<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use App\Project;

class ProjectController extends ApiBaseController
{
    /**
     * Display a listing of worker log in/out time.
     *
     * @return \Illuminate\Http\Response
     * @OA\Post(
     *     path="/v1/project",
     *     tags={"Project"},
     *     summary="listing of projects. you can filter by passing params in query string ?keywords=sdd&page=1 ",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="List of project lists."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="some query execution errors."
    *     )
    * )
    */
    public function index(Request $request)
    {
        try{    

            return $this->success(
                Project::select('id','job_number','job_name')
                ->where(function ($query) use( $request ){
                    $query->where('status', 'active')
                          ->where('job_name','like',  '%' . $request->keywords . '%');
                })
                ->orWhere(function ($query) use( $request ){
                    $query->where('status', 'active')
                          ->where('job_number','like',  '%' . $request->keywords . '%');
                })
                ->orderBy('id', 'DESC')
                ->paginate(100000),trans('messages.PROJECT_LIST'));
        } 
        catch (Exception $e)
        {
            return $this->error( [], trans('messages.EXECUTION_ERROR') );

        }
    }

    /**
     * Display a listing of worker log in/out time.
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/project/qrcode/get",
     *     tags={"Project"},
     *     summary="Get QRcode",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="project_id",
     *         in="query",
     *         description="48",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="QRcode"
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="some query execution errors."
    *     )
    * )
    */
    public function getQRcode(Request $request)
    {
        try{    

            $qrcode = Project::find( $request->project_id )->qrcode;

            $data['qrcode'] = $qrcode;

            return  $this->success( $data, 'QRcode fetched.');
            
        } 
        catch (Exception $e)
        {
            return $this->error( [], trans('messages.EXECUTION_ERROR') );

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
