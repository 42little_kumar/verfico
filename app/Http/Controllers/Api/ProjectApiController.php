<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\WorkLog;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use Carbon\Carbon;
use App\Project;
class ProjectApiController extends ApiBaseController
{
    
     /**
     * Display a listing of project with keywords (Filter).
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/project/filter",
     *     tags={"Punch In & Out"},
     *     summary="Filter project with project name .",
     *     operationId="projectFilter",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="keywords",
     *         in="query",
     *         description="Keywords",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="fetch records successfully."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function projectFilter(Request $request){
        try{
            $projects = Project::orderBy('id', 'DESC')
            ->where( 'manual_log', 1 )
            ->where('status','active')
            ->get();
            return $this->success( $projects, "Filter records" );
        }catch(Exception $e){
            return $this->error( [], $e->getMessage());
        }
    }

    /**
     * Display a listing of project with keywords (Filter).
     *
     * @return \Illuminate\Http\Response
     * @OA\Get(
     *     path="/v1/project/safety-filter",
     *     tags={"Punch In & Out"},
     *     summary="Filter project with project name .",
     *     operationId="safetyProjectFilter",
     *     @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="keywords",
     *         in="query",
     *         description="Keywords",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
    *     @OA\Response(
    *         response=200,
    *         description="fetch records successfully."
    *     ),
    *     @OA\Response(
    *         response=401,
    *         description="Enter valid parameters."
    *     )
    * )
    */
    public function safetyProjectFilter(Request $request){
        try{
            $projects = Project::orderBy('id', 'DESC')
            ->where('status','active')
            ->get();
            return $this->success( $projects, "Filter records" );
        }catch(Exception $e){
            return $this->error( [], $e->getMessage());
        }
    }

    public function projectPunch( Request $request )
    {

        try
        {
        
            $workLogData = [];

            $projectId = $request->project_id;

            $workerId = $request->worker_id;

            $dateTime = Carbon::now();

            $time = $dateTime->toTimeString();

            $workLogData['worker_id'] = $workerId;

            $workLogData['project_id'] = $projectId;

            $punchData = WorkLog::where( 'project_id', $projectId )
            ->where( 'worker_id', $workerId )
            ->latest()
            ->first();

            if( $punchData && empty( $punchData->punch_out ) )
            {

                $punchData->punch_out = $time;

                $punchData->update();

                $message = 'Worker has been punched out.';

            }
            else
            {

                $workLogData['punch_in'] = $time;

                $workLogData['punch_out'] = null;

                $message = 'Worker has been punched in.';

                WorkLog::create( $workLogData );
            }

  
            return $this->success( [], $message );

        } 
        catch (Exception $e)
        {

            $message = 'There is some error while executing your query.';

            return $this->error( [], $message );

        }

    }

}
