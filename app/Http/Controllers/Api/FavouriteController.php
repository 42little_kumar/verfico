<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiBaseController as ApiBaseController;
use App\Http\Requests\FavouriteRequest;
use App\Favourites;
use App\Project;
use Auth;
class FavouriteController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
      *  @OA\Get(
     *     path="/v1/project/favourite",
     *     tags={"Add favourite/unfavourite"},
     *     summary="List of all favourite projects. you can filter by passing params in query string ?keywords=sdd&page=1",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="List of favourite projects."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Some query execution error."
     *     )
     * )
     */ 
    public function index(Request $request)
    {
        try{
           $keywords =  $request->input('keywords');
            $fav = Project::select('id','job_number','job_name')->whereIn('id',Favourites::whereUserId(Auth::user()->id)->pluck('project_id')->toArray())->where(function($query) use ($keywords){
                $query->where( 'job_name','like',$keywords. '%');
                $query->orwhere( 'job_number','like',$keywords. '%');
            })->orderBy('id', 'DESC')->paginate(100000);
            return  $this->success( $fav,trans('messages.PROJECT_LIST') );
       }
       catch ( Exception $e )
       {
            return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);
       } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  App\Http\FavouriteRequest  $request
     * @return \Illuminate\Http\Response
      *  @OA\Post(
     *     path="/v1/project/favourite",
     *     tags={"Add favourite/unfavourite"},
     *     summary="foreman can favourite/unfavourite.",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Bearer <token>",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="project_id",
     *         in="query",
     *         description="Project Id",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Add favourite/unfavourite succussfully."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Please enter valid parameters values."
     *     )
     * )
     */ 
    public function create(FavouriteRequest $request)
    {
        try{
            if($favourite = Favourites::whereUserId(Auth::user()->id)->whereProjectId($request->project_id)->first()){
                $favourite->delete();
                return  $this->success( [],trans('messages.RECORD_UPDATE') );
            }else
             $fav =  Favourites::create(['user_id'=>Auth::user()->id, 'project_id'=>$request->project_id]);
            return  $this->success( $fav, trans('messages.RECORD_UPDATE') );
       }
       catch ( Exception $e )
       {
            return $this->error(['error'=>trans('messages.EXECUTION_ERROR')],401);
       }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
