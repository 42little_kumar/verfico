<?php

namespace App\Http\Controllers\Superint;

use File;
use Session;
use App\User;
use App\Project;
use App\WorkerType;
use App\Manpower;
use App\Tempdb;
use App\WorkerDocuments;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Mail;
use App\Mail\ManpowerCronEmail;
use App\Mail\ManpowerDeleteEmail;
use App\Mail\ManpowerUpdateAll;
use Auth;

class ManpowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

    	//return view('superint.manpower.index');

      $p = Project::where('superint', Auth::user()->id)->where('status', 'active')->orderBy('job_name', 'asc')->orderBy('job_name', 'asc')->first();

      if( !$p )
      {
        return view('superint.manpower.noproject');
      }

      $pr = $p->id;

      if( isset( $_REQUEST['pr'] ) )
      {
        $pr = $_REQUEST['pr'];
      }

    	return redirect( route('manpower.create').'?d='.date('m-d-Y').'&pr='.$pr );
        
    }

    public function logs( $id )
    {
      $p = Project::where('superint', Auth::user()->id)->where('status', 'active')->orderBy('job_name', 'asc')->get();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->where('parent', '624')->where('status', 2)->get();
      $md = Manpower::find( $id );

      return view('superint.manpower.logs')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', $id)
      ->with('s', $s);

    }

    public function logsd()
    {



      $id = 187;

      $d = $_REQUEST['d'];

      $input  = $d;
          
      $format = 'm-d-Y';

      $c = \Carbon\Carbon::createFromFormat($format, $input);
      $cd = \Carbon\Carbon::createFromFormat($format, date('m-d-Y'));



      if($c->greaterThanOrEqualTo($cd)){

        return redirect( route('manpower.create').'?d='.$c->format('m-d-Y')."&pr=".$_REQUEST['pr'] );

      }


      $p = Project::where('superint', Auth::user()->id)->where('status', 'active')->orderBy('job_name', 'asc')->get();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->where('parent', '624')->where('status', 2)->get();
      $md = Manpower::where('job_date','=', $c->toDateString())->where('project_id',$_REQUEST['pr'])->first();

 

      return view('superint.manpower.logs')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', @$md->id)
      ->with('s', $s);

    }

    public function clear($id,$d,$pr)
    {

      Manpower::find($id)->delete();

      Session::flash('success', 'Schedule has been cleared.');

       return \Redirect::to(route('manpower.create') . "?d=".$d."&pr=".$pr);


    }

    public function get( Request $request )
    {
        $resource = Manpower::all();

        return Datatables::of($resource)
        ->addColumn('project', function($resource)
        {

          return Project::find( $resource->project_id )->job_name;

        })
        ->addColumn('job_date', function($resource)
        {

          

          return date( 'm-d-Y', strtotime( $resource->job_date ) );

        })
        ->addColumn('action', function($resource)
        {

            $editLink = route('manpower.edit', $resource->id);;


            $deleteLink = route('manpower.delete', $resource->id);

            $deleteHtml = '';


              $editHtml = '<hr>
                                   <div>
                                   <a href="' . $editLink . '" class="btn btn-sm btn-danger btn-circle btn-action-edit" title="Edit">
                                      Edit
                                    </a>
                                  </div>';
           
              $deleteHtml = '<hr>
                                   <div>
                                   <a href="' . $deleteLink . '" class="btn btn-sm btn-circle btn-action-delete" title="Delete">
                                      Delete
                                    </a>
                                  </div>';

              


            $html = '<div class="drill-type">

                         <div class="company_edit">
                            <div class="dropdown">

                               <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                               </button>

                               <div class="dropdown-menu">

                                    '.$editHtml.'

                                   '.$deleteHtml.'
                                  
                               </div>
                            </div>
                         </div>
                         <div class="clearfix"></div>
                      </div>';

            return $html;

        })
        ->rawColumns( ['project', 'job_date', 'action'] )
        ->make( true );
    }

    public function create( Request $request )
    {

      $d = $_REQUEST['d'];

      $input  = $d;
          
      $format = 'm-d-Y';

      $c = \Carbon\Carbon::createFromFormat($format, $input);



      $cd = \Carbon\Carbon::createFromFormat($format, date('m-d-Y'));



      if($c->lessThan($cd)){

        return redirect( route('manpower.dl')."?d=".$_REQUEST['d']."&pr=".$_REQUEST['pr'] );

      }


      $md = Manpower::where('job_date','=', $c->toDateString())->where('project_id',$_REQUEST['pr'])->first();

      if( $md ){

        $pr = '';

        if( isset( $_REQUEST['pr'] ) )
        {
            $pr = "&pr=".$_REQUEST['pr'];
        }

        return redirect( route('manpower.edit', $md->id)."?d=".$_REQUEST['d'].$pr );

      }

        $p = Project::where('superint', Auth::user()->id)->where('status', 'active')->orderBy('job_name', 'asc')->get();
        $s = User::role('Subcontractor')->get();
        $w = User::role(['Worker','Safetyo'])->where('parent', '624')->where('status', 2)->get();
        $f = User::role('Foreman')->where('status', 2)->get();
        $w = $w->merge($f);

        return view('superint.manpower.create')
        ->with('p', $p)
        ->with('w', $w)
        ->with('s', $s);

    }

    public function copyPrevious( Request $request )
    {
      $input  = $request->d;
          
      $format = 'm-d-Y';

      $c = \Carbon\Carbon::createFromFormat($format, $input);

      

      DB::table('temp')->truncate();

      $id = 0;


      foreach( $request->project_ids as $g )
      {

        $ld = Manpower::orderBy('id', 'desc')->where('project_id', $g)->first();

        if( $ld )
        {

          $ld = $ld->toArray();

          $ld['job_date'] = $c->toDateString();

          //echo $ld['job_date'];



          $id = Manpower::create( $ld );

        }

      }

      return redirect( route('manpower.create').'?d='.$_REQUEST['d'].'&pr='.$request->project_ids[0] );

      //return redirect( route('manpower.copy.edit', $id)."?d=".$_REQUEST['d']."&pr=".$_REQUEST['pr'] );

    }

    public function copyPreviousByDate( Request $request )
    {

 

      DB::table('temp')->truncate();

      $getDate = $_REQUEST['date'];

      $id = 0;

      /* Date copy from */

      $input  = $getDate;
          
      $format = 'm-d-Y';

      $c = \Carbon\Carbon::createFromFormat($format, $input);

      /* Date copy from */

      /* Date copy to */

      $cpt_input  = $_REQUEST['d'];
          
      $cpt_format = 'm-d-Y';

      $cpt_c = \Carbon\Carbon::createFromFormat($cpt_format, $cpt_input);

      /* Date copy to */

      /* Save project wise */

      foreach( $request->project_ids as $g )
      {

        $ld = Manpower::orderBy('id', 'desc')->where('job_date', '=' ,$c->toDateString())->where('project_id', $g)->first();

        if( $ld )
        {

          $ld = $ld->toArray();

          $ld['job_date'] = $cpt_c->toDateString();

          $id = Manpower::create( $ld );

        }

      }

      /* Save project wise */


      //$ld = Manpower::orderBy('id', 'desc')->where('project_id', $_REQUEST['pr'])->where('job_date', '=' ,$c->toDateString())->first();

      //if( empty( $ld ) )
      //{
        //return redirect( route('manpower.create').'?d='.$_REQUEST['d'].'&pr='.$_REQUEST['pr'] );
      //}
      //else
      //{
          //if( $ld )
          //{
            //$ld = $ld->toArray();
          

          //$id = Tempdb::create( $ld );

        //}

      return redirect( route('manpower.create').'?d='.$_REQUEST['d'].'&pr='.$request->project_ids[0] );

        //return redirect( route('manpower.copy.edit', $id)."?d=".$_REQUEST['d']."&pr=".$_REQUEST['pr'] );
      //}

      



      

    }

    public function edit( $id )
    {


      $p = Project::where('superint', Auth::user()->id)->where('status', 'active')->orderBy('job_name', 'asc')->get();
      $s = User::role('Subcontractor')->get();
      $w = User::role(['Worker','Safetyo'])->where('parent', '624')->where('status', 2)->get();
      $f = User::role('Foreman')->where('status', 2)->get();
      $w = $w->merge($f);
      

      if( Manpower::where('id', $id)->exists() )
      {
        $md = Manpower::find( $id );

      return view('superint.manpower.edit')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', $id)
      ->with('s', $s);
      }
      else
      {

        $p = Project::where('superint', Auth::user()->id)->where('status', 'active')->orderBy('job_name', 'asc')->orderBy('job_name', 'asc')->first();

      if( !$p )
      {
        return view('superint.manpower.noproject');
      }

      $pr = $p->id;

      if( isset( $_REQUEST['pr'] ) )
      {
        $pr = $_REQUEST['pr'];
      }

      return redirect( route('manpower.create').'?d='.date('m-d-Y').'&pr='.$pr );

    
      }
      

    }

    public function copyEdit( $id )
    {

      $p = Project::where('superint', Auth::user()->id)->where('status', 'active')->orderBy('job_name', 'asc')->get();
      $s = User::role('Subcontractor')->get();
      $w = User::role('Worker')->where('parent', '624')->where('status', 2)->get();
      $md = DB::table('temp')->find( $id );

      return view('superint.manpower.copyedit')
      ->with('p', $p)
      ->with('w', $w)
      ->with('md', $md)
      ->with('mid', $id)
      ->with('s', $s);

    }

    public function store( Request $request )
    {
    

        $data = [];

        $data['project_id'] = $request->project_id;
        $data['job_date'] = date( 'Y-m-d H:i:s', strtotime( $request->job_date ) );
        $data['job_time'] = $request->job_time;
        $data['job_2_time'] = $request->job_2_time;
        $data['shifttype'] = $request->shifttype;
        $data['shifttype2'] = $request->shifttype2;
        $data['emp_data'] = json_encode($request->shift1);

        $data['special_notes'] = null;

        if( isset( $request->closed ) )
        {
          $data['special_notes'] = $request->special_notes;
          $data['closed'] = 1;
        }

        if( isset( $request->isshift ) )
          $data['emp_data_2'] = json_encode($request->shift2);

        $id = Manpower::create( $data );

        Session::flash('success', 'New schedule has been added successfully.');

        return redirect( route('manpower.edit', $id)."?d=".$_REQUEST['d']."&pr=".$_REQUEST['pr'] );

    }

    public function update_BACKUP( Request $request )
    {



      //dd( $request->all() );

      $data = [];

        $data['project_id'] = $request->project_id;
        $data['job_date'] = date( 'Y-m-d H:i:s', strtotime( $request->job_date ) );
        $data['job_time'] = $request->job_time;
        $data['job_2_time'] = $request->job_2_time;
        $data['emp_data'] = json_encode($request->shift1);

        $data['emp_data_2'] = NULL;

        if( isset( $request->isshift ) )
          $data['emp_data_2'] = json_encode($request->shift2);

        Manpower::find( $request->id )->update( $data );

       if( isset( $request->emailusers ) )
      {


        $alteredUsersIds = array_unique( $request->emailusers );

        foreach ($alteredUsersIds as $k => $v) {
          

          if( $v == 'undefined' )
            unset( $alteredUsersIds[$k] );

        if( $v == '624' )
            unset( $alteredUsersIds[$k] );

          if( $v == 'null' )
            unset( $alteredUsersIds[$k] );

        }

        //dd( $alteredUsersIds );

        $this->sendUpdateEmail( $alteredUsersIds, $request->id );


      }

      if( isset( $request->emaildels ) )
      {


        $delIds = array_unique( $request->emaildels );

        foreach ($delIds as $k => $v) {
          
          if( $v == 'undefined' )
            unset( $delIds[$k] );

        	if( $v == '624:1' )
            unset( $delIds[$k] );

        	if( $v == '624:2' )
            unset( $delIds[$k] );

          if( $v == 'undefined:1' )
            unset( $delIds[$k] );

          if( $v == 'undefined:2' )
            unset( $delIds[$k] );

          if( $v == 'null' )
            unset( $delIds[$k] );

          if( $v == 'null:1' )
            unset( $delIds[$k] );

          if( $v == 'null:2' )
            unset( $delIds[$k] );

        }

        //dd( $delIds );

        //$this->sendDeletedEmail( $delIds, $request->id );


      }

      if( isset( $request->emailusers ) )
      {


        $alteredUsersIds = array_unique( $request->emailusers );

        foreach ($alteredUsersIds as $k => $v) {
          

          if( $v == 'undefined' )
            unset( $alteredUsersIds[$k] );

          if( $v == 'null' )
            unset( $alteredUsersIds[$k] );

        }

        //dd( $alteredUsersIds );

        $this->sendUpdateEmail( $alteredUsersIds, $request->id );


      }

      if( isset( $request->allUpdate ) )
      {
        $this->sendUpdateEmailAll( $request->id );
      }

        Session::flash('success', 'Schedule has been updated successfully.');

        return redirect()->back();

    }

    public function update( Request $request )
    {



      //dd( $request->all() );

      $mobj = Manpower::find( $request->id );

      $data = [];

        $data['project_id'] = $request->project_id;
        $data['job_date'] = date( 'Y-m-d H:i:s', strtotime( $request->job_date ) );
        $data['job_time'] = $request->job_time;
        $data['job_2_time'] = $request->job_2_time;
        $data['shifttype'] = $request->shifttype;
        $data['shifttype2'] = $request->shifttype2;
        $data['emp_data'] = json_encode($request->shift1);

        $data['closed'] = 0;
        $data['special_notes'] = null;

        if( isset( $request->closed ) )
        {
          $data['special_notes'] = $request->special_notes;
          $data['closed'] = 1;
        }

        $data['emp_data_2'] = NULL;

        if( isset( $request->isshift ) )
          $data['emp_data_2'] = json_encode($request->shift2);

        $mobj->update( $data );

       if( isset( $request->emailusers ) && $mobj->is_sechedule_email_sent == 1 )
      {


        $alteredUsersIds = array_unique( $request->emailusers );

        foreach ($alteredUsersIds as $k => $v) {
          

          if( $v == 'undefined' )
            unset( $alteredUsersIds[$k] );

        if( $v == '624' )
            unset( $alteredUsersIds[$k] );

          if( $v == 'null' )
            unset( $alteredUsersIds[$k] );

        }

        //dd( $alteredUsersIds );

        $this->sendUpdateEmail( $alteredUsersIds, $request->id );


      }

      if( isset( $request->emaildels ) && $mobj->is_sechedule_email_sent == 1 )
      {


        $delIds = array_unique( $request->emaildels );

        foreach ($delIds as $k => $v) {
          
          if( $v == 'undefined' )
            unset( $delIds[$k] );

          if( $v == '624:1' )
            unset( $delIds[$k] );

          if( $v == '624:2' )
            unset( $delIds[$k] );

          if( $v == 'undefined:1' )
            unset( $delIds[$k] );

          if( $v == 'undefined:2' )
            unset( $delIds[$k] );

          if( $v == 'null' )
            unset( $delIds[$k] );

          if( $v == 'null:1' )
            unset( $delIds[$k] );

          if( $v == 'null:2' )
            unset( $delIds[$k] );

        }

        //dd( $delIds );

        $this->sendDeletedEmail( $delIds, $request->id );


      }

      if( isset( $request->emailusers ) && $mobj->is_sechedule_email_sent == 1 )
      {


        $alteredUsersIds = array_unique( $request->emailusers );

        foreach ($alteredUsersIds as $k => $v) {
          

          if( $v == 'undefined' )
            unset( $alteredUsersIds[$k] );

          if( $v == 'null' )
            unset( $alteredUsersIds[$k] );

        }

        //dd( $alteredUsersIds );

        $emailIds = $alteredUsersIds;

        $this->sendUpdateEmailAll( $request->id, $emailIds  );


      }

      if( isset( $request->allUpdate ) && $mobj->is_sechedule_email_sent == 1 )
      {
        $this->sendUpdateEmailAll( $request->id );
      }

        Session::flash('success', 'Schedule has been updated successfully.');

        return redirect()->back();

    }

    function sendDeletedEmail( $a_emails, $jid )
    {
        $notificationText = "This is a reminder that you have been removed from the manpower schedule with which you were linked to";

        $data = [];

        $users = [];

        $key = \App\Manpower::find( $jid );

        $data['project_name'] = \DB::table('project')->where('id', $key->project_id)->first()->job_name;
        $data['date'] = date( 'm-d-Y', strtotime($key->job_date) );
        

        foreach ($a_emails as $v)
        {
          
          list( $userId, $shift ) = explode(':', $v);

          if( $shift == '1' )
          {
            $data['shift'] = 'First';
            $data['shift_time'] = $key->job_time;
          }
          else
          {
            $data['shift'] = 'Second';
            $data['shift_time'] = $key->job_2_time;
          }

          
          
          $data['regards'] = '<br>Regards,<br>'.ucfirst( Auth::user()->name );
          Mail::to( User::find( $userId )->email )->send( new ManpowerDeleteEmail( $data, $notificationText ) );

        }



    }

    function sendUpdateEmailAll( $jid, $emailsIds = null )
    {
      
        $notificationText = "This is a reminder that the manpower with which you are linked has been updated.";

        //$currentSubs = getScheduleCurrentSubs( $input );

        $response2 = '';

      $subId = Auth::user()->id;
          
        $format = 'm-d-Y';

        $emailData = [];

        //$c = \Carbon\Carbon::createFromFormat($format, $input);
        //$datec = $c->toDateString();

        $allProjectsIds = \DB::table('project')->where('status', 'active')->where('superint', Auth::user()->id)->pluck('id');

        $schedule = Manpower::find($jid);

      

        if( $schedule ):



           
                //

                $dp = [];
                $dp2 = [];

                // Shift 1

                $decodedData = json_decode( $schedule->emp_data );
                $decodedData2 = json_decode( $schedule->emp_data_2 );

                $rowArray = [];


                if( !empty( $schedule->emp_data ) ):

                foreach ($decodedData->sub as $k => $g) {

                  

                    $tempData = [];
                        
                 

                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData->a_workertype_name;
                        $awtn = (array)@$decodedData->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */

                        if( $g != 624 ):
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp[$sid] ) ) {

                                $awt = $awt[$sid][$dp[$sid]];
                                $awtn = $awtn[$sid][$dp[$sid]];
                                $dp[$sid] = $dp[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }      
                        endif;       
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '1';
                        $tempData['sub'] = $g;
                        $tempData['project'] = $schedule->project_id;
                        $tempData['worker'] =  @$decodedData->worker[$k];
                        $tempData['time'] = $schedule->job_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData->jobtype[$k];
                        $tempData['note'] = @$decodedData->note[$k];


                    

                    array_push($rowArray, $tempData);

               

                }

                endif;


                // Shift 2

                if( !empty( $schedule->emp_data_2 ) ):

                foreach ($decodedData2->sub as $k => $g) {

                    

                    $tempData = [];
                        
                   
                        /* -------------------------------------------------- */
                        $awt = (array)@$decodedData2->a_workertype_name;
                        $awtn = (array)@$decodedData2->a_workertype_number;
                        $sid = $g;
                        /* -------------------------------------------------- */
                        
                        /* -------------------------------------------------- */
                      if( $g != 624 ):
                        if( count( $awt[$sid] ) > 1 ) {

                            if( isset( $dp2[$sid] ) ) {

                                $awt = $awt[$sid][$dp2[$sid]];
                                $awtn = $awtn[$sid][$dp2[$sid]];
                                $dp2[$sid] = $dp2[$sid]+1;

                            }
                            else {

                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];
                              $dp2[$sid] = 1;

                            }

                        }
                        else {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                        }
                          endif;     
                        /* -------------------------------------------------- */

                        $tempData['shift'] = '2';
                        $tempData['sub'] = $g;
                        $tempData['project'] = $schedule->project_id;
                        $tempData['worker'] =  @$decodedData2->worker[$k];
                        $tempData['time'] = $schedule->job_2_time;
                        $tempData['time2'] = $schedule->job_2_time;
                        $tempData['shifttype'] = $schedule->shifttype;
                        $tempData['shifttype2'] = $schedule->shifttype2;
                        $tempData['date'] = $schedule->job_date;
                        $tempData['worker_type'] = $awt;
                        $tempData['worker_number'] = $awtn;
                        $tempData['work_type'] = @$decodedData2->jobtype[$k];
                        $tempData['note'] = @$decodedData2->note[$k];


                    array_push($rowArray, $tempData);

              
                    

                }

                endif;

                $emailData[] = array_filter( $rowArray );


            

        endif;

        //if( $subId != 624 ):

        $response = '<p>Hello,</p>The following manpower update has been made for '.date('l', strtotime($schedule->job_date)).', '.date("j F, Y", strtotime($schedule->job_date)).':<br><br>Project Name: '.ucwords( \DB::table('project')->where('id', $schedule->project_id)->first()->job_name ).'<br><br>';

        //print_r( array_filter( $emailData ) );

        foreach( array_filter( $emailData ) as $k => $v )
        {

            $getProject = \DB::table('project')->where('id', $k)->first();

            $response .= '<div>'; 
            

            $responseInternal = '';
            $responseInternal1 = '';
            $responseInternal2 = '';

            foreach ($v as $g) {

                /* -------- Check Emtpy -------- */

                $workerType = ( $g["worker_type"] == 'null' ) ? 'N/A' : @\DB::table('worker_types')->where('id', $g["worker_type"])->first()->type;
                $workerNumber = ( empty( $g["worker_number"] ) ) ? 'N/A' : $g["worker_number"];
                $workType = ( empty( $g["work_type"] ) ) ? 'N/A' : $g["work_type"];
                $worker = $g["worker"];
                $note = ( empty( $g["note"] ) ) ? 'N/A' : $g["note"];

                /* -------- Check Emtpy -------- */

                if( $g["shift"] == 1 ){
                    $responseInternal1 .= '<p>'; 

                    if( $g['sub'] == 624 ):
                      $responseInternal1 .= '<span><b><i></i></b>(Rockspring)</span> '; 
                      $worker = ucwords(@\App\User::where('id', $worker)->first()->name);
                      $responseInternal1 .= '<span><b><i></i></b> '. $worker .' for</span> '; 
                    endif;
                
                if( $g['sub'] != 624 ):
                    $responseInternal1 .= '<span><b><i></i></b> ('. $workerNumber .')</span> ';
                    $responseInternal1 .= '<span><b><i></i></b> '. $workerType .' for</span> '; 
                  endif;
                     
                    $responseInternal1 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                    $responseInternal1 .= '<span><b><i>Note:</i></b> '. $note .'</span> ('. ucfirst( User::find($g['sub'])->company_name ).')'; 

                    $responseInternal1 .= '</p>';
                }
                else{

                    $responseInternal2 .= '<p>'; 

                     if( $g['sub'] == 624 ):
                      $responseInternal2 .= '<span><b><i></i></b>(Rockspring)</span> ';
                      $worker = ucwords(@\App\User::where('id', $worker)->first()->name);
                      $responseInternal2 .= '<span><b><i></i></b> '. $worker .' for</span> '; 
                    endif;
                
                if( $g['sub'] != 624 ):
                    $responseInternal2 .= '<span><b><i></i></b> ('. $workerNumber .')</span> ';
                    $responseInternal2 .= '<span><b><i></i></b> '. $workerType .' for</span> '; 
                  endif;
                     
                    $responseInternal2 .= '<span><b><i></i></b> '. $workType .'</span>, '; 
                    $responseInternal2 .= '<span><b><i>Note:</i></b> '. $note .'</span> ('. ucfirst( User::find($g['sub'])->company_name ).')'; 

                    $responseInternal2 .= '</p>';

                }
                 

            }


            $response .= '<b>Job Starts at:  '. str_replace(" : ", ":", reset($v)["time"] ) .' ('.reset($v)["shifttype"].' Hour)</b><br>'.$responseInternal1.' </br>';

            if( !empty( $responseInternal2 ) ){
                $response .= '<b>2nd shift starts at: ( '. str_replace(" : ", ":", reset($v)["time2"] ) .' ) ('.reset($v)["shifttype"].' Hour)</b><br>'.$responseInternal2.' </br>';
            }
            

            $response .= '</div>'; 
                

        }

        $response .= '<br>Regards,<br>'.ucfirst( Auth::user()->name );

        $notificationText = '';

        $subject = 'Manpower Update: '.ucwords( \DB::table('project')->where('id', $schedule->project_id)->first()->job_name ).', '.date("j F, Y", strtotime($schedule->job_date));

        if( $emailsIds != null )
        {

          //$users = array_intersect( array_unique( $users ), $emailsIds );


                foreach( \App\User::whereIn('id', array_unique( $emailsIds ) )->get() as $g )
                {

                  //Mail::to( $g->email )->send( new ManpowerUpdateAll( $response, $notificationText ) );
                  //Mail::to( 'kate@yopmail.com' )->send( new ManpowerUpdateAll( $response, $notificationText ) );
                }

                $e = Auth::user()->email;
                Mail::to( $e )->send( new ManpowerUpdateAll( $subject, $response, $notificationText ) );
                Mail::to( config('app.adminemail') )->send( new ManpowerUpdateAll( $subject, $response, $notificationText ) );

                $allAdmins = User::role('Admin')->get();

                foreach ($allAdmins as $g) {
                    Mail::to( $g->email )->send( new ManpowerUpdateAll( $subject, $response, $notificationText ) );
                }

                //
                $projectId = \DB::table('manpower')->where('id', $jid)->first()->project_id;

                $formen = \DB::table('project')->where('id', $projectId)->first()->foremen;

                if( !empty( $formen ) )
                {

                  $formenArray = explode(',', $formen);

                  foreach ($formenArray as $g) {
                   
                    $e = \DB::Table('users')->where('id', $g)->first()->email;
                    Mail::to( $e )->send( new ManpowerUpdateAll( $subject, $response, $notificationText ) );

                  }

                }

        }
        else
        {
          $e = Auth::user()->email;

        Mail::to( $e )->send( new ManpowerUpdateAll( $subject, $response, $notificationText ) );
        }

        

        

    }



    function sendUpdateEmail( $a_emails, $jid )
    {
        $notificationText = "This is a reminder that the manpower with which you are linked has been updated.";

        $data = [];

        $users = [];

        $key = \App\Manpower::find( $jid );

        


           
       


            $shiftOne = [];
            $shiftSecond= [];
              



                $shiftOne = [];
            $shiftSecond= [];
            $finalData = [];
                        
                $mds1 = json_decode( $key->emp_data );
                $mds2 = json_decode( $key->emp_data_2 );

                foreach( $mds1->sub as $k => $g )
                {
                    array_push($users, $mds1->sub[$k]);
                    array_push($users, $mds1->worker[$k]);
                }

                if( isset($mds2->sub) ):

                foreach( $mds2->sub as $k => $g )
                {
                    array_push($users, $mds2->sub[$k]);
                    array_push($users, $mds2->worker[$k]);
                }

            endif;

                $dp = [];
                $dp2 = [];

                $rockworker1 = [];
                $rockworker2 = [];

                foreach( $mds1->sub as $k => $v )
                {

                    /* Rockspring */

                    if( $mds1->sub[$k] == 624 )
                    {
                        $tempArray = [];

                        $subName = \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name;

                        $subName = str_replace(' ', '_', $subName);

                        $workerData = \DB::table('users')->where('id', $mds1->worker[$k])->first();

                        array_push($rockworker1, $mds1->worker[$k]);

                        $workerName = $workerData->first_name.'_'.$workerData->last_name;

                        $workerName = str_replace(' ', '_', $workerName);

                        $tempArray[$subName]['worker'] = $workerName;
                        $tempArray[$subName]['work_type'] = $mds1->jobtype[$k];
                        $tempArray[$subName]['note'] = $mds1->note[$k];

                        array_push($shiftOne, $tempArray);
                    }

                    /* Rockspring */
                   
                    /* If other */

                    else
                    {
                        $tempArray = [];
                        $awt = (array)$mds1->a_workertype_name;
                          $awtn = (array)$mds1->a_workertype_number;
                          $sid = $mds1->sub[$k];

                          

                          if( count( $awt[$sid] ) > 1 )
                          {

                             if( isset( $dp[$sid] ) )
                            {
                              $awt = $awt[$sid][$dp[$sid]];
                              $awtn = $awtn[$sid][$dp[$sid]];

                              $dp[$sid] = $dp[$sid]+1;
                            }
                            else
                            {
                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];

                              $dp[$sid] = 1;

                            }

                          }
                          else
                          {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                          }

                          if( $awt == 'null' )
                            {
                                $workerType = '-';
                            }
                            else
                            {
                                $workerType = \DB::table('worker_types')->where('id', $awt)->first()->type;
                            }

                          $subName = \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name;

                          $subName = str_replace(' ', '_', $subName);

                        $tempArray[$subName]['worker_type'] = $workerType;
                        $tempArray[$subName]['worker_number'] = empty( $awtn ) ? '-' : $awtn;
                        $tempArray[$subName]['work_type'] = $mds1->jobtype[$k];
                        $tempArray[$subName]['note'] = empty( $mds1->note[$k] ) ? '-' : $mds1->note[$k];

                        array_push($shiftOne, $tempArray);

                    }

                    /* If other */

                }

                // Shift 2

                if( isset($mds2->sub) ):

                foreach( $mds2->sub as $k => $v )
                {

                    /* Rockspring */

                    if( $mds2->sub[$k] == 624 )
                    {
                        $tempArray = [];

                        $subName = \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name;

                        $subName = str_replace(' ', '_', $subName);

                        $workerData = \DB::table('users')->where('id', $mds2->worker[$k])->first();

                        array_push($rockworker2, $mds2->worker[$k]);

                        $workerName = $workerData->first_name.'_'.$workerData->last_name;

                        $workerName = str_replace(' ', '_', $workerName);

                        $tempArray[$subName]['worker'] = $workerName;
                        $tempArray[$subName]['work_type'] = $mds2->jobtype[$k];
                        $tempArray[$subName]['note'] = $mds2->note[$k];

                        array_push($shiftSecond, $tempArray);
                    }

                    /* Rockspring */
                   
                    /* If other */

                    else
                    {
                        $tempArray = [];
                        $awt = (array)$mds2->a_workertype_name;
                          $awtn = (array)$mds2->a_workertype_number;
                          $sid = $mds2->sub[$k];

                          

                          if( count( $awt[$sid] ) > 1 )
                          {

                             if( isset( $dp2[$sid] ) )
                            {
                              $awt = $awt[$sid][$dp2[$sid]];
                              $awtn = $awtn[$sid][$dp2[$sid]];

                              $dp2[$sid] = $dp2[$sid]+1;
                            }
                            else
                            {
                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];

                              $dp2[$sid] = 1;

                            }

                          }
                          else
                          {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                          }

                          if( $awt == 'null' )
                            {
                                $workerType = '-';
                            }
                            else
                            {
                                $workerType = \DB::table('worker_types')->where('id', $awt)->first()->type;
                            }

                          $subName = \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name;

                          $subName = str_replace(' ', '_', $subName);

                        $tempArray[$subName]['worker_type'] = $workerType;
                        $tempArray[$subName]['worker_number'] = empty( $awtn ) ? '-' : $awtn;
                        $tempArray[$subName]['work_type'] = $mds2->jobtype[$k];
                        $tempArray[$subName]['note'] = empty( $mds2->note[$k] ) ? '-' : $mds2->note[$k];

                        array_push($shiftSecond, $tempArray);

                    }

                    /* If other */

                }

                $finalData[1] = $shiftSecond;

                endif;

                $finalData[0] = $shiftOne;
                $finalData['project_name'] = \DB::table('project')->where('id', $key->project_id)->first()->job_name;
                $finalData['date'] = date( 'm-d-Y', strtotime($key->job_date) );
                $finalData['shift_1_time'] = $key->job_time;
                $finalData['shift_2_time'] = $key->job_2_time;
                

                //$this->se( $finalData, $notificationText, 'thistest42@yopmail.com' );

              	
                

                $users = array_intersect( array_unique( $users ), $a_emails );


                foreach( \App\User::whereIn('id', array_unique( $users ) )->get() as $g )
                {


                    $finalData['contractor'] = $g->company_name;
                    $finalData['userid'] = false;

                    if( $g->hasRole('Worker') )
                        {
                            $finalData['userid1'] = false;
                            $finalData['userid2'] = false;
                            
                            $finalData['parent'] = \App\User::where('id', $g->parent )->first()->company_name;

                            if (in_array($g->id, $rockworker1))
                            {
                                $finalData['userid1'] = true;
                            }

                            if (in_array($g->id, $rockworker2))
                            {
                                $finalData['userid2'] = true;
                            }
                            
                        }
            
                    //echo $g->email;
                    //$this->se( $data, $notificationText, $g->email );
                    $this->se( $finalData, $notificationText, $g->email );
                    //$this->se( $finalData, $notificationText, \DB::table('users')->where('id', Auth::user()->id)->first()->email );
                    //$this->se( $finalData, $notificationText, '42testingemail@yopmail.com' );
                }

                //\DB::table('manpower')->where( 'id', $key->id )->update( ['notification' => 1] );

            

        

        // Return redirect back 

        //\Session::flash('success', 'Schedule emails have been sent.');

        //return redirect()->back();

    }

    function sendUpdateEmailAll_backup($jid)
    {
        $notificationText = "This is a reminder that the manpower with which you are linked has been updated.";

        $data = [];

        $users = [];

        $key = \App\Manpower::find( $jid );

        


           
       


            $shiftOne = [];
            $shiftSecond= [];
              



                $shiftOne = [];
            $shiftSecond= [];
            $finalData = [];
                        
                $mds1 = json_decode( $key->emp_data );
                $mds2 = json_decode( $key->emp_data_2 );

                foreach( $mds1->sub as $k => $g )
                {
                    array_push($users, $mds1->sub[$k]);
                    array_push($users, $mds1->worker[$k]);
                }

                if( isset($mds2->sub) ):

                foreach( $mds2->sub as $k => $g )
                {
                    array_push($users, $mds2->sub[$k]);
                    array_push($users, $mds2->worker[$k]);
                }

            endif;

                $dp = [];
                $dp2 = [];

                $rockworker1 = [];
                $rockworker2 = [];

                foreach( $mds1->sub as $k => $v )
                {

                    /* Rockspring */

                    if( $mds1->sub[$k] == 624 )
                    {
                        $tempArray = [];

                        $subName = \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name;

                        $subName = str_replace(' ', '_', $subName);

                        $workerData = \DB::table('users')->where('id', $mds1->worker[$k])->first();

                        array_push($rockworker1, $mds1->worker[$k]);

                        $workerName = $workerData->first_name.'_'.$workerData->last_name;

                        $workerName = str_replace(' ', '_', $workerName);

                        $tempArray[$subName]['worker'] = $workerName;
                        $tempArray[$subName]['work_type'] = $mds1->jobtype[$k];
                        $tempArray[$subName]['note'] = $mds1->note[$k];

                        array_push($shiftOne, $tempArray);
                    }

                    /* Rockspring */
                   
                    /* If other */

                    else
                    {
                        $tempArray = [];
                        $awt = (array)$mds1->a_workertype_name;
                          $awtn = (array)$mds1->a_workertype_number;
                          $sid = $mds1->sub[$k];

                          

                          if( count( $awt[$sid] ) > 1 )
                          {

                             if( isset( $dp[$sid] ) )
                            {
                              $awt = $awt[$sid][$dp[$sid]];
                              $awtn = $awtn[$sid][$dp[$sid]];

                              $dp[$sid] = $dp[$sid]+1;
                            }
                            else
                            {
                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];

                              $dp[$sid] = 1;

                            }

                          }
                          else
                          {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                          }

                          if( $awt == 'null' )
                            {
                                $workerType = '-';
                            }
                            else
                            {
                                $workerType = \DB::table('worker_types')->where('id', $awt)->first()->type;
                            }

                          $subName = \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name;

                          $subName = str_replace(' ', '_', $subName);

                        $tempArray[$subName]['worker_type'] = $workerType;
                        $tempArray[$subName]['worker_number'] = empty( $awtn ) ? '-' : $awtn;
                        $tempArray[$subName]['work_type'] = $mds1->jobtype[$k];
                        $tempArray[$subName]['note'] = empty( $mds1->note[$k] ) ? '-' : $mds1->note[$k];

                        array_push($shiftOne, $tempArray);

                    }

                    /* If other */

                }

                // Shift 2

                if( isset($mds2->sub) ):

                foreach( $mds2->sub as $k => $v )
                {

                    /* Rockspring */

                    if( $mds2->sub[$k] == 624 )
                    {
                        $tempArray = [];

                        $subName = \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name;

                        $subName = str_replace(' ', '_', $subName);

                        $workerData = \DB::table('users')->where('id', $mds2->worker[$k])->first();

                        array_push($rockworker2, $mds2->worker[$k]);

                        $workerName = $workerData->first_name.'_'.$workerData->last_name;

                        $workerName = str_replace(' ', '_', $workerName);

                        $tempArray[$subName]['worker'] = $workerName;
                        $tempArray[$subName]['work_type'] = $mds2->jobtype[$k];
                        $tempArray[$subName]['note'] = $mds2->note[$k];

                        array_push($shiftSecond, $tempArray);
                    }

                    /* Rockspring */
                   
                    /* If other */

                    else
                    {
                        $tempArray = [];
                        $awt = (array)$mds2->a_workertype_name;
                          $awtn = (array)$mds2->a_workertype_number;
                          $sid = $mds2->sub[$k];

                          

                          if( count( $awt[$sid] ) > 1 )
                          {

                             if( isset( $dp2[$sid] ) )
                            {
                              $awt = $awt[$sid][$dp2[$sid]];
                              $awtn = $awtn[$sid][$dp2[$sid]];

                              $dp2[$sid] = $dp2[$sid]+1;
                            }
                            else
                            {
                              $awt = $awt[$sid][0];
                              $awtn = $awtn[$sid][0];

                              $dp2[$sid] = 1;

                            }

                          }
                          else
                          {

                            $awt = $awt[$sid][0];
                            $awtn = $awtn[$sid][0];

                          }

                          if( $awt == 'null' )
                            {
                                $workerType = '-';
                            }
                            else
                            {
                                $workerType = \DB::table('worker_types')->where('id', $awt)->first()->type;
                            }

                          $subName = \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name;

                          $subName = str_replace(' ', '_', $subName);

                        $tempArray[$subName]['worker_type'] = $workerType;
                        $tempArray[$subName]['worker_number'] = empty( $awtn ) ? '-' : $awtn;
                        $tempArray[$subName]['work_type'] = $mds2->jobtype[$k];
                        $tempArray[$subName]['note'] = empty( $mds2->note[$k] ) ? '-' : $mds2->note[$k];

                        array_push($shiftSecond, $tempArray);

                    }

                    /* If other */

                }

                $finalData[1] = $shiftSecond;

                endif;

                $finalData[0] = $shiftOne;
                $finalData['project_name'] = \DB::table('project')->where('id', $key->project_id)->first()->job_name;
                $finalData['date'] = date( 'm-d-Y', strtotime($key->job_date) );
                $finalData['shift_1_time'] = $key->job_time;
                $finalData['shift_2_time'] = $key->job_2_time;
                

                //$this->se( $finalData, $notificationText, 'thistest42@yopmail.com' );

                
                

                //$users = array_intersect( array_unique( $users ), $a_emails );


                foreach( \App\User::whereIn('id', array_unique( $users ) )->get() as $g )
                {


                    $finalData['contractor'] = $g->company_name;
                    $finalData['userid'] = false;

                    if( $g->hasRole('Worker') )
                        {
                            $finalData['userid1'] = false;
                            $finalData['userid2'] = false;
                            
                            $finalData['parent'] = \App\User::where('id', $g->parent )->first()->company_name;

                            if (in_array($g->id, $rockworker1))
                            {
                                $finalData['userid1'] = true;
                            }

                            if (in_array($g->id, $rockworker2))
                            {
                                $finalData['userid2'] = true;
                            }
                            
                        }
            
                    //echo $g->email;
                    //$this->se( $data, $notificationText, $g->email );
                    $this->se( $finalData, $notificationText, $g->email );
                    //$this->se( $finalData, $notificationText, '42testingemail@yopmail.com' );
                }

                //\DB::table('manpower')->where( 'id', $key->id )->update( ['notification' => 1] );

            

        

        // Return redirect back 

        //\Session::flash('success', 'Schedule emails have been sent.');

        //return redirect()->back();

    }

    function se( $data, $notificationText, $e )
    {

        $data['regards'] = '<br>Regards,<br>'.ucfirst( Auth::user()->name );
        Mail::to( $e )->send( new ManpowerCronEmail( $data, $notificationText ) );

    }


    public function destroy( $id )
    {

        Manpower::find( $id )->delete();

        Session::flash('success', 'Schedule has been deleted successfully.');

        return redirect( route('manpower') );

    }

}
