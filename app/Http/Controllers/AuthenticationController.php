<?php

namespace App\Http\Controllers;

use File;
use Session;
use App\User;
use Socialite;
use Carbon\Carbon;
use App\SocialAccount;
use App\PasswordReset;
use App\Events\Activity;
use App\Mail\VerifyEmail;
use App\Mail\ResetPassword;
use App\Mail\ResetPasswordAdmin;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;

class AuthenticationController extends Controller
{
    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    private function findUsername()
    {
    
        $login = request()->input('login');

        if(is_numeric($login)) // phone number is valid
        {
            $field = 'phone_number';
            //$field = 'username';
        }
        else
        {
            $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        }

        return $field;
    
    }

    private function redirect_authenticated_user(User $user)
    {
        $roles = Role::where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->get();
    
        if($user->hasRole('Superadmin') || $user->hasRole('Admin') ) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('admin.dashboard');
            
        } elseif($user->hasRole('Subcontractor')) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('subcontractor.dashboard');

        } elseif($user->hasRole('Payroll')) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('admin.payrolls');
    
        } elseif($user->hasRole('Superint')) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('manpower');
    
        }
        elseif($user->hasRole('Worker')) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('f.resources');
    
        }
        elseif($user->hasRole('Foreman')) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('f.resources');
    
        }
        elseif($user->hasRole('Safetyo')) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('f.resources');
    
        } 
        elseif($user->hasRole('User')) {

            event(new Activity($user, 'login', 'Account Login.'));
    
            return redirect()->route('home');
    
        }
    
    }

    public function register()
    {

        return view('auth.register');

    }

    public function registeration(Request $request)
    {

        dd($request->all());

        $this->validate($request, [

            'username' => [ 'required', 'min:6', 'max:75', 'alpha_num', 'unique:users' ],

            'first-name' => ['required', 'string', 'max:75'],
            
            'email' => ['required', 'email', 'max:191', 'unique:users'],
            
            'password' => ['required', 'min:8', 'max:50'],

            'password-confirmation' => ['required', 'same:password'],
        
        ],[

            'username.required' => 'Please enter your username.',

            'username.min' => 'Username should be of atleast 6 characters.',

            'username.max' => 'Username should not be more than 75 characters.',

            'username.alpha_num' => 'Username may only contain letters and numbers. No special characters allowed.',

            'username.unique' => 'Username already registered. Please try a different username.',

            'first-name.required' => 'Please enter your first name.',

            'first-name.string' => 'First name must be string.',

            'first-name.max' => 'First name should not be more than 75 characters.',

            'email.required' => 'Please enter your email address.',

            'email.email' => 'Please enter a valid email address.',

            'email.max' => 'Email address should not be more than 191 characters.',

            'email.unique' => 'Email address already registered. Please try a different email address.',

            'password.required' => 'Please enter password.',

            'password.min' => 'Password should be of atleast 8 characters.',
            
            'password.max' => 'Password should not be more than 50 characters.',

            'password-confirmation.required' => 'Please enter confirm password.',

            'password-confirmation.same' => 'Password and confirm password should be same.',

        ]);

        $role = Role::where('slug', 'regular')->firstOrFail();

        $user = User::create([

            'role_id' => $role->id,

            'username' => strtolower($request->input('username')),

            'first_name' => $request->input('first-name'),

            'last_name' => $request->input('last-name'),

            'email' => $request->input('email'),

            'password' => Hash::make($request->input('password'))

        ]);

        Mail::to($user)->send(new VerifyEmail($user));

        Session::flash('success', 'Account registeration successful. Kinldy check your mailbox, we have sent an email containing instructions to activate your account before login.');

        return redirect()->route('register');

    }

    public function verify()
    {

        if(request()->query('email') && request()->query('token')) {

            $email = request()->query('email');

            $token = request()->query('token');

            $user = User::where('email', $email)->whereNull('email_verified_at')->firstOrFail();

            if($token == $user->password) {

                $user->email_verified_at = Carbon::now();

                $user->save();

                event(new Activity($user, 'email-verification', 'Email address verified.'));

                Session::flash('success', 'Your account is activated successfully. Kindly login.');

                return redirect()->route('login');

            }

        }

        abort(404);

    }

    public function resend_verify_email()
    {

        if(request()->query('email')) {

            $email = request()->query('email');

            $user = User::where('email', $email)->whereNull('email_verified_at')->firstOrFail();

            Mail::to($user)->send(new VerifyEmail($user));

            event(new Activity($user, 'request', 'Account activation email.'));

            Session::flash('success', 'Kindly check your mailbox, we have sent an email containing instructions to activate your account before login.');

            return redirect()->route('login');

        }

        abort(404);

    }
    
    public function login()
    {

        return view('auth.login');

    }

    public function authenticate(Request $request)
    {

        $this->validate($request, [

            'login' => 'required',

            'password' => 'required'

        ],[

            'login.required' => 'Please enter email address',

            'password.required' => 'Please enter account password'

        ]);

        $credentials = [

            $this->findUsername() => trim($request->input('login')),

            'password' => $request->input('password'),

        ];

        if(!$user = User::where(key($credentials), current($credentials))->first()) {

            $request->flash();

            Session::flash('error', 'Please enter valid email address.');

            return redirect()->back();

        }

        /*if(!$user->email_verified_at) {

            $request->flash();

            Session::flash('error', "Kindly activate your account before login. Check your mailbox for instruction. If you haven't recieved any email <a href='" . route('verify.email.resend', [ 'email' => $user->email ]) . "'>click here</a>");

            return redirect()->back();

        }*/

        if( $user->status == 0 ) {

            $request->flash();

            Session::flash('error', 'Your account is suspended. Please contact the administrator for further details.');

            return redirect()->back();

        }

        $roles = $user->getRoleNames();
        //print_r($roles); die;

        $exist = false;

        foreach ($roles as $role) {
            
            if($role=="Worker" || $role=="Safetyo" || $role=="Foreman"){
                $exist = true;
            }
        }
        /*if($exist){

            $request->flash();

            Session::flash('error', 'You are not allowed to login to the CMS. Please login from the app if you are a Worker, Foreman or Safety Officer.');

            return redirect()->back();
            die;
        }*/
        /*

        if($user->status == 1) {


            $request->flash();

            Session::flash('error', 'Your account is under review. Please contact the administrator for further details.');

            return redirect()->back();

        }*/

        $remember = $request->filled('remember') ? true : false;

        if (Auth::attempt($credentials, $remember)) {

            $user = Auth::user();

            if($request->filled('timezone')) {
            
                $user->timezone = $request->input('timezone');
            
                $user->save();
    
            }

            return $this->redirect_authenticated_user($user);
        
        } else {

            $request->flash();

            Session::flash('error', 'Incorrect Email address or Password combination.');

            return redirect()->back();

        }

    }

    public function social_authentication($provider)
    {
        if($provider == 'google' || $provider == 'facebook') {

            return Socialite::driver($provider)->redirect();
            
        }

        return redirect()->back();

    }

    public function social_authentication_callback($provider)
    {
    
        if($provider == 'google' || $provider == 'facebook') {
    
            try {
    
                $response = Socialite::driver($provider)->user();
    
                $id = $response->getId();
    
                $name = $response->getName();
    
                $email = $response->getEmail();
    
                if($user = User::where('email', $email)->first()) {
    
                    if(!$user->email_verified_at) {
    
                        $user->email_verified_at = Carbon::now();
    
                    }
    
                    if($social_account = $user->social_accounts()->where('provider', $provider)->first()) {
    
                        if($social_account->provider_user_id != $id) {
    
                            $social_account->provider_user_id = $id;
    
                            $social_account->save();
    
                        }
    
                    } else {
    
                        $social_account = SocialAccount::create([
    
                            'user_id' => $user->id,
    
                            'provider' => $provider,
    
                            'provider_user_id' => $id
    
                        ]);

                    }
                
                } else {
                
                    $role = Role::where('slug', 'regular')->firstOrFail();
                
                    $user = User::create([

                        'role_id' => $role->id,

                        'first_name' => Str::before($name, ' '),

                        'email' => $email,

                        'password' => Hash::make($id)

                    ]);

                    $user->email_verified_at = Carbon::now();

                    if($response->getAvatar()) {
                        
                        $image_content = file_get_contents($response->getAvatar());
                        
                        $image_name = 'profile-' . time() . '-' . Str::random(15) . '.jpeg';
                        
                        $image_path = public_path('uploads/images/users/'. $user->id . '/');
                        
                        if (!File::isDirectory($image_path)) {
                        
                            mkdir($image_path, 0777, true);
            
                            chmod($image_path, 0777);
                        
                        }
                        
                        file_put_contents($image_path . $image_name, $image_content);
                        
                        $user->image = $image_name;
                    
                    }
                    
                    $user->save();
                    
                    $social_account = SocialAccount::create([
                    
                        'user_id' => $user->id,
                    
                        'provider' => $provider,
                    
                        'provider_user_id' => $id
                    
                    ]);
                
                }

                Auth::login($user);

                return $this->redirect_authenticated_user(Auth::user());

            } catch(\Exception $e) {

                return redirect()->back();

            }
            
        }

        return redirect()->back();

    }

    public function forgot_password()
    {

        return view('auth.forgot');

    }

    public function forgot_password_mail(Request $request)
    {
        // Get email
        $email = $request->input('email');

        // Get email from phone number
        if (is_numeric($email))
        {
            $field = 'phone_number';

            $user = User::where( 'phone_number', request('email') )->first();

            if( !$user ) {

                Session::flash('error', 'Account not found against the provided phone number.');

                return redirect()->back();

            }

            $inputVal = request('email');

            $email = $user->email;
   
        }
        else
        {
            $field = 'email';

            $user = User::where( 'email', request('email') )->first();

            if( !$user ) {

                Session::flash('error', 'Account not found against the provided email.');

                return redirect()->back();

            }

            $inputVal = request('email');

            $email = $user->email;

        }

        if( empty( $email ) )
        {

            Mail::to('superadmin@yopmail.com')->send(new ResetPasswordAdmin($user));

            Session::flash('success', 'Password reset request has been sent to the admin.');

            return redirect()->back();

        }

        if($user->reset_password) {

            PasswordReset::where('email', $email)->update([ 'token' => Str::random(40) ]);

        } else {

            PasswordReset::create([

                'email' => $email,

                'token' => Str::random(40)

            ]);

        }

        $reset = PasswordReset::where('email', $email)->first();

        Mail::to($user)->send(new ResetPassword($reset));

        event(new Activity($user, 'request', 'Account password reset request.'));

        Session::flash('success', 'Kindly check your mailbox, we have sent an email containing instructions to reset your account password.');

        return redirect()->back();

    }

    public function reset_password( Request $request )
    {

        if(request()->query('token')) {

            $token = PasswordReset::where('token', request()->query('token'))->firstOrFail();

            return view('auth.reset', compact('token'));

        }

        abort(404);

    }

    public function reset_password_mail(Request $request)
    {

        Auth::logout();

        

        $validator = Validator::make($request->all(), [

            'token' => [ 'required' ],

            'email' => [ 'required', 'email' ],

            'password' => [ 'required', 'min:6' ],

            'confirm-password' => [ 'required', 'same:password' ]

        ]);

        if($validator->fails()) {

            $request->flash();
        
            if($validator->errors()->first('email')) {
        
                Session::flash('error', 'Please enter email of user.');
        
                return redirect()->back();
        
            }

        }

        $emailExists = PasswordReset::where('email', $request->input('email'))->where('token', $request->input('token'))->exists();

        if( !$emailExists )
        {

            echo 'here';

            Session::flash('error', 'Email not found or token expired.');
            
            return redirect()->back();

        }
        
        $reset = PasswordReset::where('email', $request->input('email'))->where('token', $request->input('token'))->first();

        $user = $reset->user;

        $user->password = Hash::make($request->input('password'));

        $user->save();

        PasswordReset::where('email', $reset->email)->where('token', $reset->token)->delete();

        Session::flash('success', 'Password reset successful. Kindly login with new credentials.');

        event(new Activity($user, 'request', 'Account password changed.'));

        $roles = $user->getRoleNames();
        //print_r($roles); die;

        $exist = false;

        foreach ($roles as $role) {
            
            if($role=="Worker" || $role=="Safetyo" || $role=="Foreman"){
                $exist = true;
            }
        }
        /*if($exist){
            Session::flash('success', 'Password reset successful. Kindly login with new credentials from the app.');
            return redirect()->route('login_app');
        }*/

        return redirect()->route('login');

    }

    public function logout()
    {

        Auth::logout();

        return redirect()->route('login');

    }
    public function loginApp()
    {

        return view('auth.login_app');

    }

}