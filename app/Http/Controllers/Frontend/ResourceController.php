<?php

namespace App\Http\Controllers\Frontend;

use File;
use Session;
use App\User;
use App\Project;
use App\Document;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use Mail;
use App\Mail\ApproveWorker;
use App\Mail\ConfirmRegistrationMobileWorker;
use App\Mail\ConfirmRegistrationWorker;
use App\WorkerType;
use Maatwebsite\Excel\Facades\Excel;

class ResourceController extends Controller
{


    public function __construct()
    {
       
        
    }

    /*
     * @Contactus page
     *
     *
     */
    public function resource( Request $request )
    {

        if($this->usercheckmod()){
            $r = DB::table('resource')->get();

        return view('frontend.pages.resources.index')->with( 'r', $r );

        } else {

        return view('frontend.pages.resources.404_view');

        }

        
    }

    /**
     * @Privacy page.
     *
     * 
     */
    public function view( $id )
    {

        $this->usercheck();

        $r = DB::table('resource')->where('id', $id)->first();


        return view('frontend.pages.resources.view')->with( 'r', $r );
        
    }

   public function usercheck()
   {
        if( Auth::user()->id == 624 || Auth::user()->id == 1 || Auth::user()->parent == 624 )
        return true;

        return redirect( abort(404) );
   }
   public function usercheckmod()
   {
        if( Auth::user()->id == 624 || Auth::user()->id == 1 || Auth::user()->parent == 624 )
        { return true; }

        return false;
   }

}
