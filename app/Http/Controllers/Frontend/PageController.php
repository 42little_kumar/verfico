<?php

namespace App\Http\Controllers\Frontend;

use File;
use Session;
use App\User;
use App\Project;
use App\Document;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use Auth;
use Mail;
use App\Mail\ApproveWorker;
use App\Mail\ConfirmRegistrationMobileWorker;
use App\Mail\ConfirmRegistrationWorker;
use App\WorkerType;
use Maatwebsite\Excel\Facades\Excel;

class PageController extends Controller
{

    /**
     * @Privacy page.
     *
     * 
     */
    public function privacy( Request $request )
    {

      return view('frontend.pages.privacy');
        
    }

    /*
     * @Contactus page
     *
     *
     */
    public function contactUs( Request $request )
    {

      return view('frontend.pages.contact_us');

    }

}
