<?php

namespace App\Http\Middleware;

use Closure;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::check()) {

            $user = Auth::user();

            $roles = $user->getRoleNames();
        
            $exist = false;

            foreach ($roles as $role) {
                
                if($role=="Worker" || $role=="Safetyo" || $role=="Foreman"){
                    $exist = true;
                }
            }
            if($exist){
                return redirect()->route('f.resources');
            }

            $roles = Role::where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->get();

            if($user && $user->hasAnyRole($roles)) {

                return redirect()->route('admin.dashboard');
    
            } else{

                return redirect()->route('home');

            }
        
        }

        return $next($request);
    
    }

}
