<?php

namespace App\Http\Middleware;

use Closure;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();

        $roles = Role::where('slug', '!=', 'trainer')->where('slug', '!=', 'user')->get();

        if($user && $user->hasAnyRole($roles)) {

            return $next($request);

        }
        $roles = $user->getRoleNames();
        
        $exist = false;

        foreach ($roles as $role) {
            
            if($role=="Worker" || $role=="Safetyo" || $role=="Foreman"){
                $exist = true;
            }
        }
        if($exist){
            return redirect()->route('f.resources');
        }
        return redirect()->route('home');
        
    }
}
