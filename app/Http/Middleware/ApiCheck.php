<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class ApiCheck
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $content_type = $request->header('content-type');

        if($content_type == "application/json" || $content_type == "application/x-www-form-urlencoded" || Str::contains($content_type, 'multipart/form-data')) {

            return $next($request);

        }

        return response()->json([ 'status' => 'error', 'message' => 'Invalid headers for the request.' ], 400);

    }

}
