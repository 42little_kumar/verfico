<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\UserDevice;

class ApiTokenCheck
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    
        if(!$request->header('x-api-token')) {

            return response()->json([ 'code' => 400, 'status' => false, 'message' => 'No API token provided for the request.' ], 400);

        }

        $token = $request->header('x-api-token');

        $device = UserDevice::where('api_token', $token)->first();

        if(!$device) {

            return response()->json([ 'code' => 400, 'status' => false, 'message' => 'Invalid API token.' ], 400);

        }

        $user = User::where('id', $device->user_id)->withTrashed()->first();

        if(!$user) {

            return response()->json([ 'code' => 400, 'status' => false, 'message' => 'Invalid API token.' ], 400);

        }

        if($user->trashed()) {

            return response()->json([ 'code' => 400, 'status' => false, 'message' => 'Invalid API token.' ], 400);

        }

        if(!$user->status) {

            return response()->json([ 'code' => 400, 'status' => false, 'message' => 'Invalid API token.' ], 400);

        }

        return $next($request);

    }

}
