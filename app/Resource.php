<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = "resource";

	protected $fillable = ['name', 'description', 'file_1', 'file_2', 'file_3', 'file_4', 'file_5'];

}
