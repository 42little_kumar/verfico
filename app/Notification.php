<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'receiver_id', 'sender_id', 'type', 'data'
    
    ];

    public function sender()
    {

        return $this->belongsTo('App\User', 'sender_id', 'id');

    }

    public function receiver()
    {

        return $this->belongsTo('App\User', 'receiver_id', 'id');

    }

}
