<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('job_type')->nullable();
            $table->string('job_number')->nullable();
            $table->string('job_name')->nullable();
            $table->integer('superint')->nullable();
            $table->string('sub_contractor')->nullable();
            $table->string('foremen')->nullable();
            $table->string('general_contractor_name')->nullable();
            $table->text('qrcode')->nullable();
            $table->text('job_address')->nullable();
            $table->text('job_description')->nullable();
            $table->string('lat')->nullable();
            $table->integer('is_sechedule_email_sent')->default(0);
            $table->string('lng')->nullable();
            $table->integer('manual_log')->default(1);
            $table->timestamp('started_at')->nullable();
            $table->timestamp('completion_at')->nullable();
            $table->string('status')->nullable();
            $table->integer('radious')->default(100);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
