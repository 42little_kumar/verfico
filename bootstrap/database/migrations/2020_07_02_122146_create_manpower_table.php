<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManpowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manpower', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamp('job_date')->nullable();
            $table->timestamp('job_time')->nullable();
            $table->timestamp('job_2_time')->nullable();
            $table->string('shifttype')->nullable();
            $table->string('shifttype2')->nullable();
            $table->text('emp_data')->nullable();
            
            $table->text('emp_data_2')->nullable();
            $table->integer('is_sechedule_email_sent')->default(0);
            $table->integer('closed')->default(0);
            $table->text('special_notes')->nullable();
            $table->integer('notification')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manpower');
    }
}
