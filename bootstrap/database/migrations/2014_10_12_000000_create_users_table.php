<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->nullable()->unique();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('name')->nullable();
            $table->string('payroll_first_name')->nullable();
            $table->string('payroll_last_name')->nullable();
            $table->string('payroll_email')->nullable();
            $table->string('payroll_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('ssn_id')->nullable();
            $table->string('company_name')->nullable();
            $table->string('country_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('parent')->default(0);
            $table->integer('doc_upload')->default(0);
            $table->string('worker_type')->nullable();
            $table->string('worker_document')->nullable();
            $table->text('notes_training')->nullable();
            $table->string('gender')->nullable();
            $table->string('image')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->text('bio')->nullable();
            $table->string('timezone')->default('UTC');
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('training')->nullable();
            $table->timestamp('phone_number_verified_at')->nullable();
            $table->unsignedTinyInteger('status')->default(1);
            $table->string('lang')->default('en');
            $table->string('timezone_manual')->default('America/New_York,-05:00');
            $table->interger('updated_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
