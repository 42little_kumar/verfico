<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->integer('project_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamp('job_date')->nullable();
            $table->timestamp('job_time')->nullable();
            $table->timestamp('job_2_time')->nullable();
            $table->text('emp_data')->nullable();
            $table->text('emp_data_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp');
    }
}
