<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $settings = [
            
            'application-name' => env('APP_NAME'),

            'mail-driver' => env('MAIL_DRIVER'),
            
            'mail-host' => env('MAIL_HOST'),
            
            'mail-port' => env('MAIL_PORT'),
            
            'mail-username' => null,
            
            'mail-password' => null,
            
            'mail-encryption' => env('MAIL_ENCRYPTION'),

            'currency' => 'usd',
            
            'paypal-mode' => 'sandbox',
            
            'paypal-client-id' => null,
            
            'paypal-secret' => null,
            
            'stripe-key' => null,
            
            'stripe-secret' => null
        
        ];

        foreach($settings as $setting => $value) {

            Setting::create([

                'name' => $setting,

                'value' => $value

            ]);

        }

    }
}
