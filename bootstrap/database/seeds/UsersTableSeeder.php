<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $roles = Role::all();

        foreach($roles as $role) {

            $parent = 0;

            if( $role->slug == 'worker' )
            {

                $parent = 5;

            }
        
            $user = User::create([
            
                'username' => $role->slug,
            
                'first_name' => $role->name,

                'parent' => $parent,

                'status' => 2,

                'gender' => 'male',

                'worker_type' => 'laborer',
            
                'email' => $role->slug . '@yopmail.com',
            
                'password' => Hash::make('demodemo')
            
            ]);

            $user->assignRole($role->slug);
            
            $user->email_verified_at = Carbon::now();
            
            $user->save();

        }

    }
}
