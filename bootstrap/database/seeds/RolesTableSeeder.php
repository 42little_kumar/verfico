<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [

            'superadmin', 'admin', 'foreman', 'worker', 'subcontractor', 'payroll', 'superint', 'safetyo'

        ];

        foreach($roles as $role) {

            Role::create([
                
                'slug' => Str::slug($role, '-'),

                'name' => Str::title($role)

            ]);

        }
    }
}
