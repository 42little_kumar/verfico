<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $sections = [ 'trainer-categories', 'permissions', 'surveys', 'trainers', 'users', 'roles', 'members', 'session-categories', 'sessions', 'package-categories', 'packages', 'coupons', 'notifications', 'products' ];

        $permissions = [ 'no-access', 'read', 'write' ];

        foreach($sections as $section) {

            foreach($permissions as $p) {

                $permission = $p . ' ' . $section;

                Permission::create([
                    
                    'name' => $permission
                    
                ]);

            }

        }

    }
}
