<?php

use Illuminate\Database\Seeder;
use App\WorkerType;

class WorkerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$types = [

    	    'Finisher', 'Mechanic', 'Laborer'

    	];

    	foreach( $types as $type )
    	{

    	    WorkerType::create([
    	        
    	        'type' => $type

    	    ]);

    	}

    }
}
