@extends('admin.layouts.app')

@section('title', 'Documents')

@section('content')

@section('styles')

    <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>

    <style type="text/css">
      
      .tox-notifications-container
      {
        display: none !important;
      }

      i.fa.fa-file-pdf {
          color: #0f3459;
          font-size: 60px;
      }

    </style>

@endsection


<div class="d-sm-flex align-items-center justify-content-between mb-4">

    <h1 class="h3 mb-0 text-gray-800">Documents</h1>

</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

        <div class="card-body shadow mb-4">

          <div class="row">

            @if( count( $documents ) > 0  )
              
             @foreach( $documents as $get )

             <div class="col-md-2 text-center pdf-column">

              

              <a href="{{ asset('public/uploads/documents') }}/{{ $subId }}/{{ $get->document }}" target="_blank"><p><i class="fa fa-file-pdf"></i></p>{{ strlen($get->document) > 20 ? substr($get->document,0,20)."..." : $get->document }}</a>

             </div>

             @endforeach

             @else

             <p style="margin: 0 auto;">No documents found!</p>

             @endif

          </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>

<script type="text/javascript">

    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {

        initialize_database();

    });

</script>


@endsection