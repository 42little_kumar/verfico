@extends('admin.layouts.app')

@section('title', 'Manpower Schedule')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('common/wickedpicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }
    div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
  right:187px;  
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}

.wickedpicker {
    z-index: 9999;
}
footer{
display:none;	
}

    </style>

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection

@section('content')




   <div class="row subcon-project-schedule-wrap" style="margin:0px;">
      
	<div class="d-sm-flex align-items-center justify-content-between mb-4 col-lg-12">         
		<h1 class="h3 mb-0 text-gray-800">Manpower Schedule</h1>    
     </div>
	  <div class="clearfix"></div>
	<?php

			  	$dt = date('m-d-Y');

			  	if( isset( $_GET['d'] ) )
			  	{
			  		$dt = $_GET['d'];
			  	}

			  	$timestamp = strtotime($dt);

			  	$input  = $dt;

				$format = 'm-d-Y';

				$c = \Carbon\Carbon::createFromFormat($format, $input);

			  	$d = $c->format('l');
			  	$da = $c->format('d');
			  	$m = $c->format('F');
			  	$y = $c->format('Y');


			  	$dtv = \Carbon\Carbon::createFromFormat($format, $dt)->format('m/d/Y');

			  	?>
   

       <div class="col-md-12">
	    
               <h2 class="project-name-wrap" style="display:none;">
                @foreach( $p as $g )

                @if( @$md->project_id == $g->id )

                Project Name: {{ $g->job_name }}

                @endif

                @endforeach
              </h2>
			  
			<div class="manpower-next-pre-wrap"> 
			<div class="row manpower-top-btn-wrap">
			<div class="col-3">
			 <div class="manpower-next-pre-button-wrap">
                <a href="{{ route('subcontractor.manpower.view.d') }}?d={{$c->subDays(1)->format('m-d-Y')}}" class="btn">Previous</a>
			  </div>
			</div>
			<div class="col-6">
			  <div class="manpower-date-wrap ">
			    <strong>{{ $d }}</strong>
				<p>{{ $da }} {{ $m }}, {{ $y }}</p>
				<input class="form-control job-date" name="job_date" type="text" id="datetype" placeholder="Select date" value="{{ $dtv }}" autocomplete="off" style="background:white;"/ />	
				</div>
			  </div>
			  <div class="col-3">
			   <div class="manpower-next-pre-button-wrap" style="text-align:right;">
                <a href="{{ route('subcontractor.manpower.view.d') }}?d={{$c->addDays(2)->format('m-d-Y')}}" class="btn">Next</a>
			  </div>
			  </div>
			</div>
			</div>
	   </div>
	   
       <div class="clearfix"></div>
	   @if( count( $l ) == 0 )

	   <p class="col-lg-12 no-data-found">No manpower schedule found</p>

	   @endif

     

	   @foreach( $l as $md )

	   <div class="manpower-project-repeat-wrapper col-lg-12">

	          
      <?php $nmc_r_1 = 0; ?>
     <?php $nmc_s_1 = 0; ?>

	   @php

              $mds1 = json_decode( $md->emp_data );
           

            @endphp


<?php

 $date_now = date("m-d-Y"); // this format is string comparable

 $todayDateGreater = false;

if ( $date_now  >  @$_REQUEST['d'] ) {
    
  $todayDateGreater = true;

}


?>

      <!-- -------------------- Logs ---------------- -->

<div class="card card-body shadow col-lg-12" @if( !$todayDateGreater ) style='display: none;' @endif> 
        <div class="man-power-project-name">
            <h3>Project Name: {{ \DB::table('project')->where('id', $md->project_id)->first()->job_name }}</h3>
          <!--<span class="s1time{{ $md->id }}">Job Opens: {{ str_replace( ' : ', ':', $md->job_time ) }}</span>-->
      </div>
         
         <div class="manpower-view-table-holder">


          <div>
          <ul class="log-list-flex">   
          <li>
            <b>WORKERS</b>
            <b>WORK TYPE</b>
            <b>IN TIME</b>
            <b>OUT TIME</b>
            
          </li>
          </ul>
       </div>

        <?php

        $url = URL::to('/').'/api/v1/getscheduleforadmin?date='.$_REQUEST["d"].'&project_id='.$md->project_id;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        $allLogs = json_decode($output);


        ?>
        
        <div>

          

            @foreach( $allLogs->data as $g )

            @if( $g->sub_id == Auth::user()->id )

            <ul class='log-list-flex'>

            <li @if( !isset( $g->occupied ) ) class="loggreen" @endif @if( empty( $g->punch_in ) && empty( $g->punch_out ) ) class="logred" @endif>
            <div class='log-worker-name'>
            <span>@if( !empty( $g->worker_name ) ) {{ ucfirst( $g->worker_name ) }} @else {{ ucfirst( $g->worker_type ) }} @endif</span>
            </div>

            <div>
            <span class='in-time-text'> {{ $g->work_type }} </span>
            </div>  

            <div>
            <span class='in-time-text'>@if( !empty( $g->punch_in ) ) {{ gt( $g->punch_in ) }} @else - @endif</span>
            </div>
            <div>
            <span class='out-time-text'>@if( !empty( $g->punch_out ) ) {{ gt( $g->punch_out ) }} @else - @endif</span>
            </div>  
            </li>

            </ul>  

            @endif

            @endforeach

        </div>

           

  
</div>
</div>

<!-- -------------------- Logs ---------------- -->

		 <div class="card card-body shadow col-lg-12" @if( $todayDateGreater ) style='display: none;' @endif> 
		    <div class="man-power-project-name">
				    <h3>Project Name: {{ \DB::table('project')->where('id', $md->project_id)->first()->job_name }}</h3>
					<span class="s1time{{ $md->id }}">Job Opens: {{ str_replace( ' : ', ':', $md->job_time ) }} ( {{ $md->shifttype }} Hours )</span>
			</div>
				 
         <div class="manpower-view-table-holder">
            <ul class="view-power-list-wrap"   @if(count($mds1->sub) == 0 && Auth::user()->id == 624 ) style='display:none; margin:0px 0 0; padding-bottom:0px;' @endif>
			  <li style="border:0px;">
                    @if( Auth::user()->id != 624 )
          <b class="work-name-td">Worker Title</b>
          @else
<b class="work-name-td">Worker Name</b>

          @endif
			   
				
			    @if( Auth::user()->id != 624 )
          <b class="work-no-td">No. of Workers</b>
          <b class="work-type-td">Work Type</b>
          @else
          <b class="work-type-td">Work Type</b>
          @endif
			    <b class="work-notes-td">Notes</b>
			  </li>
			</ul>
           

  
            <?php

            $mds2 = [];

            if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
             

            }

            ?>

            <?php

            $dp = [];
            $dp2 = [];

            ?>

  
		<ul class="view-power-list-wrap" @if(count($mds1->sub) == 0 && Auth::user()->id == 624 ) style='display:none;' @endif>
	       
          
          @foreach( $mds1->sub as $k => $g )

          <li>

          @if( $mds1->sub[$k] == Auth::user()->id )

          <?php $nmc_s_1++; ?>
             

            @if( $mds1->sub[$k] == 624 )

              <?php $nmc_r_1++; ?>

                <!-- Rosa case shift 1 start -->
                    
                <span class="work-name-td"> 
                @foreach( $w as $g )

                @if( $mds1->worker[$k] == $g->id )

                {{ $g->first_name }} {{ $g->last_name }}

                @endif

                @endforeach
                </span> 
				
                <!-- Rosa case shift 1 end -->
     

            @else
				
           
			
		    <span class="work-name-td">
            

              @php

              $awt = (array)$mds1->a_workertype_name;
              $awtn = (array)$mds1->a_workertype_number;
              $sid = $mds1->sub[$k];

              @endphp

              <?php

              if( count( $awt[$sid] ) > 1 )
              {

                 if( isset( $dp[$sid] ) )
                {
                  $awt = $awt[$sid][$dp[$sid]];
                  $awtn = $awtn[$sid][$dp[$sid]];

                  $dp[$sid] = $dp[$sid]+1;
                }
                else
                {
                  $awt = $awt[$sid][0];
                  $awtn = $awtn[$sid][0];

                  $dp[$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[$sid][0];
                $awtn = $awtn[$sid][0];

              }

              ?>

             
       {!! ( $awt == 'null' ) ? '-</span>' : '' !!}   
      
      @foreach( \DB::table('worker_types')->get() as $g )



      @if( $awt == $g->id )

      <!-- shift 1 type start -->

      

      {{ $g->type }}


      <!-- shift 1 type start -->

      </span>

      @endif
    
	
      @endforeach
	    
	    


              <!-- shift 1 no start -->
             <span class="work-no-td">
             {{ empty( $awtn ) ? '-' : $awtn }}
            </span> 
              <!-- shift 1 no end -->

            @endif
			
			   
              
                @foreach( \DB::table('work_types')->get() as $g )



                <!--@if( $mds1->jobtype[$k] == $g->type )

   
                <span class="work-type-td">  
                {{ $g->type }}
               </span>


                @endif-->

                    @endforeach

                    <span class="work-type-td">  
                {{ empty( $mds1->jobtype[$k] ) ? '-' : $mds1->jobtype[$k] }}
               </span>
                
	

             <span class="work-notes-td">
               <!-- shift 1 note start -->

               {{ empty( $mds1->note[$k] ) ? '-' : $mds1->note[$k] }}

               <!-- shift 1 note end -->
            
            </span>   
            @if( $k != 0 )
            
            @endif


            @endif

            </li>



          @endforeach


        @if( $nmc_r_1 == 0 && Auth::user()->id == 624)

         <p class="no-data-found">No manpower schedule found in shift 1</p>

         <style>
            .s1time{{ $md->id }}
            {
              display: none !important;
            }
           </style>

        @endif

        @if( $nmc_s_1 == 0 && Auth::user()->id != 624)

        <p class="no-data-found">No manpower schedule found in shift 1</p>

        <style>
            .s1time{{ $md->id }}
            {
              display: none !important;
            }
           </style>

        @endif

      </ul>
  
<?php $nmc_r_2 = 0; ?>
<?php $nmc_s_2 = 0; ?>

 
</div>

  <!--shift2 code--->
   
  <!--shift2 code--->




<div class="clearfix"></div>
<div @if( empty( $md->emp_data_2 ) ) style="display: none;" @endif class="project-sift-inner-repeat project-sift-2-wrap"> 

 <h4 class="project-sift-2">2nd Shift</h4>
 
    <div class="manpower-view-table-holder">
	  <div class="man-power-project-name s2time{{$md->id}}" style="margin-bottom:0px;">
        <span>Job Opens: {{ str_replace( ' : ', ':', $md->job_2_time ) }} ( {{ $md->shifttype2 }} Hours )</span>
	  </div>
       <ul class="view-power-list-wrap shift-table-head-wrap" style="margin:12px 0 0; padding-bottom:0px;">
			  <li style="border:0px;">
			    @if( Auth::user()->id != 624 )
          <b class="work-name-td">Worker Title</b>
          @else
<b class="work-name-td">Worker Name</b>

          @endif
				@if( Auth::user()->id != 624 )
          <b class="work-no-td">No. of Workers</b>
          <b class="work-type-td">Work Type</b>
          @else
          <b class="work-type-td">Work Type</b>
          @endif
			    <b class="work-notes-td">Notes</b>
			  </li>
			</ul>

      @if( !empty( $mds2 ) )
			
<ul class="view-power-list-wrap" >

        
           

              @foreach( $mds2->sub as $k => $g )

              

              <li>

              @if( $mds2->sub[$k] == Auth::user()->id )
              
               <?php $nmc_s_2++ ?>
            
            
            @if( $mds2->sub[$k] == 624 )

            <?php $nmc_r_2++ ?>
              
                 <span class="work-name-td">  
                @foreach( $w as $g )

                @if( $mds2->worker[$k] == $g->id )

                <!-- shift 2 name start -->

                {{ $g->first_name }} {{ $g->last_name }}

                <!-- shift 2 name end -->


                @endif

                  

                @endforeach
               </span>
            
            @else

          
            <span class="work-name-td">
              @php

              $awt = (array)$mds2->a_workertype_name;
              $awtn = (array)$mds2->a_workertype_number;
              $sid = $mds2->sub[$k];

              @endphp

              <?php

              if( count( $awt[$sid] ) > 1 )
              {

                 if( isset( $dp2[$sid] ) )
                {
                  $awt = $awt[$sid][$dp2[$sid]];
                  $awtn = $awtn[$sid][$dp2[$sid]];

                  $dp2[$sid] = $dp2[$sid]+1;
                }
                else
                {
                  $awt = $awt[$sid][0];
                  $awtn = $awtn[$sid][0];

                  $dp2[$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[$sid][0];
                $awtn = $awtn[$sid][0];

              }

              ?>

              {!! ( $awt == 'null' ) ? '-</span>' : '' !!}

      @foreach( \DB::table('worker_types')->get() as $g )

      @if( $awt == $g->id )

      {{ $g->type }}

      </span>
      @endif
      
	  
      @endforeach

      <!-- shift 2 no start -->
	    <span class="work-no-td">
	       {{ empty( $awtn ) ? '-' : $awtn }}
        </span>
          <!-- shift 2 no end -->
              


            @endif
			
              

                <!--@foreach( \DB::table('work_types')->get() as $g )

                 @if( $mds2->jobtype[$k] == $g->type )


                  <span class="work-type-td"> 
                 {{ $g->type }}
                 </span>
             

                 @endif


                    @endforeach-->

                    <span class="work-type-td"> 
                 {{ empty( $mds2->jobtype[$k] ) ? '-' : $mds2->jobtype[$k] }}
                 </span>
              


           
            <!-- shift 2 note start -->
            <span class="work-notes-td">
            {{ empty( $mds2->note[$k] ) ? '-' : $mds2->note[$k] }}
            </span>
            <!-- shift 2 note end --> 
         
 
          
            @if( $k != 0 )
            
            @endif


            @endif

          </li>

          @endforeach

          @if( $nmc_r_2 == 0 && Auth::user()->id == 624)

           <p class="no-data-found">No manpower schedule found in shift 2</p>

           <style>
            .s2time{{$md->id}}
            {
              display: none;
            }
           </style>

           @endif 

           @if( $nmc_s_2 == 0 && Auth::user()->id != 624)

           <p class="no-data-found">No manpower schedule found in shift 2</p>

          <style>
            .s2time{{$md->id}}
            {
              display: none;
            }
           </style>

           @endif 

            @else
            <p class="no-data-found">No manpower schedule found in shift 2</p>

            <style>
            .s2time{{$md->id}}
            {
              display: none;
            }
           </style>
            
          </ul>

           
          


 @endif

  
</div>
</div>



<div class="clearfix"></div>



</div>

<div class="clearfix"></div>
</div>



@endforeach


@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>

<script src="{{ asset('js/jquery.timepicker.min.js') }}"></script>

<script src="{{ asset('common/wickedpicker.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

	$('.job-date').datepicker({

    onSelect: function (dateText, inst) {

         var d = $(this).datepicker('getDate').getDate();
         var m = $(this).datepicker('getDate').getMonth()+1;
         var y = $(this).datepicker('getDate').getFullYear();

         location.href =  "{{ route('subcontractor.manpower.view.d') }}?d="+m+"-"+d+"-"+y;


      }
    });

  $("#datetype").on("keypress", function(e){
   
        e.preventDefault();
    
});

var currentSub = '';
var currentSub2 = '';
var arraySub = [];

$(document).on('change', '.s1-s', function(event){


    
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
      //alert( $(this).val() ); 
      currentSub = $(this).val();

      }
    }
    
  

})

$(document).on('change', '.s2-s', function(event){


    
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
      //alert( $(this).val() ); 
      currentSub2 = $(this).val();

      }
    }
    
  

})



</script>

@endsection