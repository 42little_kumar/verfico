@extends('admin.layouts.app')

@section('title', 'All Schedules')

@section('styles')
  
    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">


    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
   
   <style>
    #datatable_wrapper {
    overflow-x: visible !important;
     }
     .dropdown-menu hr:first-child{
	  display:none;	  
	 }
	#datatable_processing{
	 display:none !important;	
	}
   </style>

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">
        
        Manpower Schedules

    </h1>

</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body worker_page_wrapper">

            
                <table class="table table-bordered table-hover" id="datatable">
  
                    <thead>
    
                        <tr>

                            <!--<th scope="col" class="worker-ids">Id</th>-->

                            <th scope="col">Project</th>
        
                            <th scope="col">Schedule Date</th>
    
                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>


@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this manpower?')) {
        
            event.preventDefault();

        }

    });


    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_users_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

          "order": [[ 1, 'desc' ]],

            //stateSave: true,


            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No resources found!',

            searchPlaceholder: "Search...",
              sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,


            "ajax": {

              "url": "{{ route('subcontractor.manpower.get') }}",

              "data": function ( d ) {

               d.userType = ''
              
            }

            },

            columns: [

               // { data: "id", name: "id", className: "workers-ids-value", searchable: false, orderable: false },

                { data: "project", name: "project", searchable: true, orderable: true },
                
                { data: "job_date", name: "job_date", searchable: true, orderable: true },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

    });

</script>

@endsection