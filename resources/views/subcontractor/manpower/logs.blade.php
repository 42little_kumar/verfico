@extends('admin.layouts.app')

@section('title', 'Edit')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('common/wickedpicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }
		div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
	right:187px;	
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}


    </style>

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection

@section('content')

<div class="manprowe-page-wrap">
<div class="row">

    <div class="col-lg-12">
       <div class="d-sm-flex align-items-center justify-content-between mb-4 manpower-top-btn-wrap">
	    <h1 class="h3 mb-0 text-gray-800">Manpower Logs</h1> 
      <?php

                      $m = \DB::table('manpower')->where('id', '<', $mid)->orderBy('id','desc')->first();
                      $m1 = \DB::table('manpower')->where('id', '>', $mid)->orderBy('id','asc')->first();

                      $pid = @$m->id;
                      $nid = @$m1->id;
                  ?>
                 <div class="manpower-next-pre-button-wrap">
                  @if( isset( $m->id ) )
                <!--<a  href="{{ route('manpower.logs', $pid) }}"><button class="btn copy-btn">Previous</button></a>-->
                @else
                <!--<a class="" href="javacript:void(0)"><button class="btn copy-btn">Previous</button></a>-->
                @endif

                @if( isset( $m1->id ) )
                <!--<a href="{{ route('manpower.logs', $nid) }}"><button class="btn copy-btn">Next</button></a>-->
                @else
                <!--<a href="{{ route('manpower.create') }}"><button class="btn copy-btn">Next</button></a>-->
                @endif
             </div>
        </div>
      <form action="{{ route('manpower.update'), $mid }}" method="post">

        @csrf

        <input type="hidden" value="{{ $mid }}" name='id'>

        <div class="card shadow mb-4">
            <div class="card-body worker_page_wrapper">
			
                  <div class="manpower-filter-wrap" style="padding:0px; background:none; border-radius:0px;">
                    <div class="row">
						<div class="col-md-3">

						
						<div class="col-md-3">
						 
						</div>
					</div>   
                    
					
					
					
                          
                    <!-- Status filter -->

                    <!--select id="status-filter">

                        <option disabled selected>-Select Status-</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 2 ) selected  @endif value="2">Active</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 0 ) selected  @endif value="0">Blocked</option>

                    </select-->

                    <!-- Status filter -->

                    <!-- Title filter -->

                    <!-- Title filter -->

                    <!-- Reset filter -->

                    @if(
                    (
                        isset( $_GET['status'] ) ||
                        isset( $_GET['title'] )
                    )
                    )

                    <a style="display:none;" href="{{ route('subcontractor.workers') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

                    @endif

                    <!-- Reset filter -->

                </div>
				
            <div class="step-one-hidden" style="display: none;">
                  <select name="shift1[sub][]" class="form-control get-subcontractors">
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>

            <div class="step-one-hidden-2" style="display: none;">
                  <select name="shift2[sub][]" class="form-control get-subcontractors-2">
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>


            <div class="step-one-hidden gww" style="display:none;">
                  <select name="shift1[worker][]" class="form-control get-workers">
      
                @foreach( $w as $g )

                  <option value="{{ $g->id }}">{{ $g->first_name }}</option>

                @endforeach
              </select> 
            </div>

            <div class="step-one-hidden gww2" style="display:none;">
                  <select name="shift2[worker][]" class="form-control get-workers">
      
                @foreach( $w as $g )

                  <option value="{{ $g->id }}">{{ $g->first_name }}</option>

                @endforeach
              </select> 
            </div>

            @php

              $mds1 = json_decode( $md->emp_data );
              //array_shift( $mds1->sub );
              //array_shift( $mds1->worker );

            @endphp

  
            <?php

            $mds2 = [];

            if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
              //array_shift( $mds2->sub );
              //array_shift( $mds2->worker );

            }

            ?>



				<div class="manpower-text-body-wrap" style="margin:0px; padding:0px; background:#fff; border-radius:0px;">
				   <div class="step-one two rw">

            <span class="project_name">
              <p><b>Project</b>: {{ \DB::table('project')->where('id', $md->project_id)->first()->job_name }}</p>
              <p><b>Time</b>: {{ $md->job_time }}</p>
              <p><b>Date</b>: {{ date( 'm-d-Y', strtotime( $md->job_date ) ) }}</p>
            </span>

			    <ul class="log-list-flex">   
					<li>
					  <b>WORKERS</b>
					  <b>IN TIME</b>
					  <b>OUT TIME</b>
					  
					</li>
			    </ul>
				</div>

				<?php $commonR = []; ?>
        <?php $commonR1 = []; ?>

				@foreach( $mds1->sub as $k => $g )

					     @if( Auth::user()->id == $mds1->sub[$k] )

                @if( $mds1->sub[$k] == 624 )

                <?php

                $ldh = '';

                if( DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $mds1->worker[$k])->whereDate('punch_at', $md->job_date)->count() == 0 )
                {

                  $crh = "<div class='step-one two rw'>
       

        
                <ul class='log-list-flex'>   
                <li>
            <div class='log-worker-name'>
             <span>".\DB::table('users')->where('id', $mds1->worker[$k])->first()->first_name ." ". \DB::table('users')->where('id', $mds1->worker[$k])->first()->last_name."</span>
                  </div>
               
                  <div>
                  <span class='in-time-text'>".gmin( $md->project_id, $mds1->worker[$k], $md->job_date )."</span>
                  </div>
          <div>
                  <span class='out-time-text'>".gmout( $md->project_id, $mds1->worker[$k], $md->job_date )."</span>
                  </div>  
                </li>
                </ul>  
              </div>";

              array_push($commonR, $crh);

                }
                else
                {
                  foreach(\DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $mds1->worker[$k])->whereDate('punch_at', $md->job_date)->get() as $ld)
                {
                  $ldh .= "<div class='step-one two rw'>
        
              <ul class='log-list-flex'>   
              <li>
          <div class='log-worker-name'>
           <span>".\DB::table('users')->where('id', $mds1->worker[$k])->first()->first_name ." ". \DB::table('users')->where('id', $mds1->worker[$k])->first()->last_name."</span>
                </div>
             
                <div>
                <span class='in-time-text'>".gt( @$ld->punch_in )."</span>
                </div>
        <div>
                <span class='out-time-text'>".( empty( $ld->punch_out ) ? 'Pending' : gt( @$ld->punch_out ))."</span>
                </div>  
              </li>
              </ul>  
            </div>";
                }


            array_push($commonR, $ldh);
                }

                

                ?>

                @endif

                @endif

                @endforeach

                <?php

                sort($commonR);

                foreach( $commonR as $g )
                {
                	echo $g;
                }

                ?>

					
					@foreach( $mds1->sub as $k => $g )

          @if( Auth::user()->id == $mds1->sub[$k] )

             @if( $mds1->sub[$k] == 624 )

            @else

            <div class="step-one two rw">

              @php

              $awt = (array)$mds1->a_workertype_name;
              $awtn = (array)$mds1->a_workertype_number;
              $sid = $mds1->sub[$k];

              @endphp

              <?php

              $wlin = [];
              $wlout = [];
              $c = 0;

              for( $i = $awtn["$sid"]; $i > 0; $i-- )
              {

                $us =  \App\User::role('worker')->where('parent', $mds1->sub[$k])->where('worker_type', $awt["$sid"])->get();

                foreach( $us as $g )
                {

                  $dt = DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $g->id)->whereDate('punch_at', $md->job_date)->first();

                  if( $dt )
                  {
                      array_push($wlin, '<li>'+$dt->punch_in+'</li>');
                      array_push($wlout, '<li>'+$dt->punch_out+'</li>');
                  }

                }

                ?>

                 <ul class="log-list-flex"> 
				<li>
				<div class="log-worker-name">
				<span>{{ explode(' ', \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name)[0] }} {{ \DB::table('worker_types')->where('id', $awt["$sid"])->first()->type }}</span>
                </div>
				
				<div>
				<span class="in-time-text">- {{ @$wlin[$c] }}</span>
                </div>
				<div>
                <span class="out-time-text">- {{ @$wlout[$c] }}</span>
                </div>
                </li>
				</ul>

                <?php

                $c++;

              }

              ?>

            </div>

            @endif

            @endif

          @endforeach

          <?php $sc = false; ?>

          @foreach( $mds2->sub as $k => $g )

              @if( Auth::user()->id == $mds2->sub[$k] )

              <?php $sc = true; ?>

              @endif

          @endforeach

          @if( $sc )

          <label style="background: #f2f2f2;
color: black;
font-weight: bold;
width: 100%;
padding-left: 15px;">Shift 2</label>

          @endif

          @if( !empty( $mds2 ) )

          

          @foreach( $mds2->sub as $k => $g )

              @if( Auth::user()->id == $mds2->sub[$k] )

                @if( $mds2->sub[$k] == 624 )

                <?php

                $ldh = '';

                if( DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $mds2->worker[$k])->whereDate('punch_at', $md->job_date)->count() == 0 )
                {
                  $crh = "<div class='step-one two rw'>
       

        
                <ul class='log-list-flex'>   
                <li>
            <div class='log-worker-name'>
             <span>".\DB::table('users')->where('id', $mds2->worker[$k])->first()->first_name ." ". \DB::table('users')->where('id', $mds2->worker[$k])->first()->last_name."</span>
                  </div>
               
                  <div>
                  <span class='in-time-text'>".gmin( $md->project_id, $mds2->worker[$k], $md->job_date )."</span>
                  </div>
          <div>
                  <span class='out-time-text'>".gmout( $md->project_id, $mds2->worker[$k], $md->job_date )."</span>
                  </div>  
                </li>
                </ul>  
              </div>";

              array_push($commonR1, $crh);
                }
                else
                {
                  foreach(\DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $mds2->worker[$k])->whereDate('punch_at', $md->job_date)->get() as $ld)
                {
                  $ldh .= "<div class='step-one two rw'>
        
              <ul class='log-list-flex'>   
              <li>
          <div class='log-worker-name'>
           <span>".\DB::table('users')->where('id', $mds2->worker[$k])->first()->first_name ." ". \DB::table('users')->where('id', $mds2->worker[$k])->first()->last_name."</span>
                </div>
             
                <div>
                <span class='in-time-text'>".gt( @$ld->punch_in )."</span>
                </div>
        <div>
                <span class='out-time-text'>".( empty( $ld->punch_out ) ? 'Pending' : gt( @$ld->punch_out ))."</span>
                </div>  
              </li>
              </ul>  
            </div>";
                }

            array_push($commonR1, $ldh);
                }

                

                ?>

                @endif

                @endif

                @endforeach

                <?php

                sort($commonR1);

                foreach( $commonR1 as $g )
                {
                  echo $g;
                }

                ?>

          @foreach( $mds2->sub as $k => $g )

          @if( Auth::user()->id == $mds2->sub[$k] )

                @if( $mds2->sub[$k] == 624 )


            @else

            <div class="step-one two rw">

              @php

              $awt = (array)$mds2->a_workertype_name;
              $awtn = (array)$mds2->a_workertype_number;
              $sid = $mds2->sub[$k];

              @endphp

              <?php

              $wlin = [];
              $wlout = [];
              $c = 0;

              for( $i = $awtn["$sid"]; $i > 0; $i-- )
              {

                $us =  \App\User::role('worker')->where('parent', $mds2->sub[$k])->where('worker_type', $awt["$sid"])->get();

                foreach( $us as $g )
                {

                  $dt = DB::table('worker_log')->where('project_id', $md->project_id)->where('worker_id', $g->id)->whereDate('punch_at', $md->job_date)->first();

                  if( $dt )
                  {
                      array_push($wlin, '<li>'+$dt->punch_in+'</li>');
                      array_push($wlout, '<li>'+$dt->punch_out+'</li>');
                  }

                }

                ?>
                <ul class="log-list-flex">  
                <li>
				<div class="log-worker-name"> 
				 <span>{{ explode(' ', \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name)[0] }}  {{ \DB::table('worker_types')->where('id', $awt["$sid"])->first()->type }}</span>
				</div>
               
                <div>
                <span class="in-time-text">- {{ @$wlin[$c] }}</span>
                </div>
				
				<div>
                <span class="out-time-text">- {{ @$wlout[$c] }}</span>
                </div>
                </li>
                </ul>
                <?php

                $c++;

              }

              ?>

            </div>

            @endif

            @endif

          @endforeach

          @endif

        </div>

        

    </div>

</div>


</form>
</div>
</div>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script src="{{ asset('common/wickedpicker.min.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

var wrd = $('.gww').html()
var wrd2 = $('.gww2').html()

  $('.sc').click(function(){

    var t = $(this)

    if( t.is(':checked') == true )
    {

      $('.second-shift').show()
      $('.s2jobfield').attr('required','required')

    }
    else
    {
      $('.second-shift').hide()
      $('.s2jobfield').removeAttr('required')
    }
  })



 $(document).on('change','.get-subcontractors', function(event){
    
    event.stopPropagation();
    var t = $(this);

    var sv = t.val();

    if( sv == 624 )
    {
      
      t.closest('.power-row-repeat').find('.rw').html(wrd);
    }
    else
    {
      t.closest('.power-row-repeat').find('.rw').html('');

      var s1 = "<input type='hidden' name='shift1[worker][]' value='2222222'><input class='form-control' placeholder='Worker Type' type='text' name='shift1[a_workertype_name]["+sv+"]'>";
      var s2 = "<input class='form-control' placeholder='No.' type='text' name='shift1[a_workertype_number]["+sv+"]'>";

      t.closest('.power-row-repeat').find('.rw').html(s1+s2);


    }


  })

 $(document).on('change','.get-subcontractors-2', function(event){
    
    event.stopPropagation();
    var t = $(this);

    var sv = t.val();

    if( sv == 624 )
    {
      
      t.closest('.power-row-repeat').find('.rw').html(wrd2);
    }
    else
    {
      t.closest('.power-row-repeat').find('.rw').html('');

      var s1 = "<input type='hidden' name='shift2[worker][]' value='2222222'><input class='form-control' placeholder='Worker Type' type='text' name='shift2[a_workertype_name]["+sv+"]'>";
      var s2 = "<input class='form-control' placeholder='No.' type='text' name='shift2[a_workertype_number]["+sv+"]'>";

      t.closest('.power-row-repeat').find('.rw').html(s1+s2);


    }


  })

var s1 = $('.step-one-hidden').html();
  $(document).on('click','.add-power-row-wrap-button', function()
  {

    

    $(this).closest('.manpower-text-body-wrap').append('<div class="power-row-repeat"> <div class="step-one"> '+s1+' </div> <div class="step-one two rw"> <select data-live-search="true" class="form-control" required="required"> </select> </div> <div class="step-one for-label-wrap"> for </div> <div class="step-one four"> <select class="form-control" name="shift1[jobtype][]"> <option>Cleaning</option><option>Washing</option><option>Decorating</option> </select> </div> <div class="step-one five"> <input class="form-control" type="text" name="shift1[note][]" placeholder="Notes" /> </div> <div class="step-one add-power-row-wrap add-power-row-wrap-button"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a> </div> <div class="step-one add-power-row-wrap delete-wrap"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div> </div>')
  })

  $(document).on('click','.delete-wrap', function()
  {
    $(this).parent().remove();
  })


var ss1 = $('.step-one-hidden-2').html();
$(document).on('click','.add-power-row-wrap-button-2', function()
  {

    

    $(this).closest('.manpower-text-body-wrap').append('<div class="power-row-repeat"> <div class="step-one"> '+ss1+' </div> <div class="step-one two rw"> <select data-live-search="true" class="form-control" required="required">  </select> </div> <div class="step-one for-label-wrap"> for </div> <div class="step-one four"> <select class="form-control" name="shift2[jobtype][]"> <option>Cleaning</option><option>Washing</option><option>Decorating</option> </select> </div> <div class="step-one five"> <input class="form-control" type="text" name="shift2[note][]" placeholder="Notes" /> </div> <div class="step-one add-power-row-wrap add-power-row-wrap-button-2"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a> </div> <div class="step-one add-power-row-wrap delete-wrap-2"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div> </div>')
  })


  $(document).on('click','.delete-wrap-2', function()
  {
    $(this).parent().remove();
  })


  $('.job-date').datepicker();


    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this user?')) {
        
            event.preventDefault();

        }

    });


    serialize = function(obj) {
	  var str = [];
	  for (var p in obj)
		if (obj.hasOwnProperty(p)) {
		  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	  return str.join("&");
	}
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

	$(document).on("click","a.index-link", function(event){
        
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='Edit']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='View']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});


    var table;

    $(document).on('click', '.lc-id', function(event) {

        var lcstids = [];

        $(".workers-ids-value").each(function() {
            
            lcstids.push( $(this).text() );

        });

        lcstids.shift();

        localStorage.workerIds = JSON.stringify(lcstids);

    });

    $(document).on('click', '.lc-id', function(event) {

var lcstids = [];

$(".workers-ids-value").each(function() {
    
    lcstids.push( $(this).text() );

});

lcstids.shift();

localStorage.workerIds = JSON.stringify(lcstids);

});

    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_sub_workers_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

            stateSave: true,

            dom: 'lBfrtip',

            buttons: [
                {
                    extend: 'csvHtml5',
                    text: 'Export Workers',
                    exportOptions: {
                       modifier : {
             page : 'all', // 'all', 'current'
         },
                        columns: [1, 2, 3, 4, 5]
                    }
                }
            ],
        
            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No workers found!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,

            "ajax": {

              "url": "{{ route('subcontractor.datatable.workers.get') }}",

              "data": function ( d ) {

               d.status = "{{ @$_REQUEST['status'] }}"
               d.title = "{{ @$_REQUEST['title'] }}"
              
            }

            },
            
            columns: [

                { data: "name", name: "first_name", searchable: true, orderable: true },

                { data: "email", name: "email", searchable: true, orderable: false },

                { data: "phone_number", name: "phone_number", searchable: false, orderable: true },

                { data: "worker_type", name: "worker_type", searchable: false, orderable: false },
                
                { data: "ssn", name: "ssn", visible: false, orderable: true },

                { data: "created_at", name: "created_at", searchable: false, orderable: true }, 
                
                { data: "status", name: "status", searchable: false, orderable: false },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_sub_workers_pageLength", len);

        });

        function getQueryVariable(url, variable)
        {

          var query = url.substring(1);

          var vars = query.split('&');

          for (var i=0; i<vars.length; i++)
          {

              var pair = vars[i].split('=');

              if (pair[0] == variable)
              {

                return pair[1];

              }
            
          }

             return false;

        }

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#status-filter').change(function()
        {
             var statusVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&status="+statusVal;

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#title-filter').change(function()
        {
             var titleVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&title="+titleVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

    });

</script>

@endsection