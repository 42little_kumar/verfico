@extends('admin.layouts.app')

@section('title', 'Workers')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }
		div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
	right:187px;	
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}
table tr td:nth-child(6) {
    display: none;
}
table tr th:nth-child(6) {
    display: none;
}
table tr td:nth-child(7) {
    display: none;
}
table tr th:nth-child(7) {
    display: none;
}

table tr td:nth-child(8) {
    display: none;
}
table tr th:nth-child(8) {
    display: none;
}

table tr td:nth-child(9) {
    display: none;
}
table tr th:nth-child(9) {
    display: none;
}

table tr td:nth-child(10) {
    display: none;
}
table tr th:nth-child(10) {
    display: none;
}

    </style>

    <link href="{{ asset('css/jquery-ui.css') }}" rel="Stylesheet" type="text/css" />

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Workers</h1>

   
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body worker_page_wrapper">




                  <div class="custom_table_filter">

                    <!-- Status filter -->

                    <select id="status-filter">

                        <option disabled selected>-Select Status-</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 2 ) selected  @endif value="2">Active</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 0 ) selected  @endif value="0">Blocked</option>

                    </select>

                    <!-- Status filter -->

                    <!-- Title filter -->

                    <select id="title-filter">

                        <option disabled selected>-Select Title-</option>

                        @foreach( $titlesList as $title )

                          <?php $titleId = $title->id; ?>

                          <option @if( isset( $_REQUEST['title'] ) && @$_REQUEST['title'] == $titleId ) selected  @endif value="{{ $titleId }}">{{ $title->type }}</option>

                        @endforeach

                    </select>

                    <!-- Title filter -->

                    <!-- Reset filter -->

                    @if(
                    (
                        isset( $_GET['status'] ) ||
                        isset( $_GET['title'] )
                    )
                    )

                    <a href="{{ route('subcontractor.workers') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

                    @endif

                    <!-- Reset filter -->

                </div>
            
                <table class="table table-bordered table-hover" id="datatable">
  
                    <thead>
    
                        <tr>


                            <th scope="col">Name</th>

                            <th scope="col">Email</th>

                            <th scope="col">Phone</th>

                            <th scope="col">Title</th>

                            <th scope="col">SSN ID</th>

                           <th scope="col">Registered On</th>

                           <th scope="col">Rock Spring Safety Training received on</th>

                           <th scope="col">Last Worked</th>

                           <th scope="col">Total Strikes</th>

                           <th scope="col">Total 30 Day Strikes</th>

                           <th scope="col">Last Strike Date</th>
                            
                            <th style="text-align: center;" scope="col">Status</th>

                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script src="{{ asset('js/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
  
  <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.flash.min.js') }}"></script>
      
  <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
  
  <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.print.min.js') }}"></script>

  <script src="{{ asset('js/jquery-ui.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/datetime-moment.js') }}"></script>


<script type="text/javascript">



    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this user?')) {
        
            event.preventDefault();

        }

    });


    serialize = function(obj) {
	  var str = [];
	  for (var p in obj)
		if (obj.hasOwnProperty(p)) {
		  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	  return str.join("&");
	}
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

	$(document).on("click","a.index-link", function(event){
        
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='Edit']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='View']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});


    var table;

    $(document).on('click', '.lc-id', function(event) {

        var lcstids = [];

        $(".workers-ids-value").each(function() {
            
            lcstids.push( $(this).text() );

        });

        lcstids.shift();

        localStorage.workerIds = JSON.stringify(lcstids);

    });

    $(document).on('click', '.lc-id', function(event) {

var lcstids = [];

$(".workers-ids-value").each(function() {
    
    lcstids.push( $(this).text() );

});

lcstids.shift();

localStorage.workerIds = JSON.stringify(lcstids);

});

    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_sub_workers_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

            stateSave: true,

            dom: 'lBfrtip',

            buttons: [
                {
                    extend: 'csvHtml5',
                    text: 'Export Workers',
                    exportOptions: {
                       modifier : {
             page : 'all', // 'all', 'current'
         },
                        columns: [0,1, 2, 3, 4, 5, 6, 7, 8, 9 ,10]
                    }
                }
            ],
        
            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No workers found!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,

            "ajax": {

              "url": "{{ route('subcontractor.datatable.workers.get') }}",

              "data": function ( d ) {

               d.status = "{{ @$_REQUEST['status'] }}"
               d.title = "{{ @$_REQUEST['title'] }}"
              
            }

            },
            
            columns: [

                { data: "name", name: "first_name", searchable: true, orderable: true },

                { data: "email", name: "email", searchable: true, orderable: false },

                { data: "phone_number", name: "phone_number", searchable: false, orderable: true },

                { data: "worker_type", name: "worker_type", searchable: false, orderable: false },
                
                { data: "ssn", name: "ssn", visible: false, orderable: true },

                { data: "created_at", name: "created_at", searchable: false, orderable: true }, 

                { data: "safety_training", name: "safety_training", searchable: false, orderable: true },

                { data: "last_worked", name: "last_worked", searchable: false, orderable: true }, 

                { data: "total_strike", name: "last_worked", searchable: false, orderable: true }, 

                { data: "total_strike_30", name: "last_worked", searchable: false, orderable: true }, 

                { data: "last_strike_date", name: "last_worked", searchable: false, orderable: true }, 
                
                { data: "status", name: "status", searchable: false, orderable: false },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_sub_workers_pageLength", len);

        });

        function getQueryVariable(url, variable)
        {

          var query = url.substring(1);

          var vars = query.split('&');

          for (var i=0; i<vars.length; i++)
          {

              var pair = vars[i].split('=');

              if (pair[0] == variable)
              {

                return pair[1];

              }
            
          }

             return false;

        }

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#status-filter').change(function()
        {
             var statusVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&status="+statusVal;

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#title-filter').change(function()
        {
             var titleVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&title="+titleVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

    });

</script>

@endsection