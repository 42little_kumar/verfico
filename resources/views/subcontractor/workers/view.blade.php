@extends('admin.layouts.app')
@section('title', $user->first_name .' '. $user->last_name )
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><span class="project_name_view_heading">User Information <em><a href="{{ route('subcontractor.workers.edit', [ 'id' => $user->id ]) }}@if( !empty( @$_GET['from'] ) )?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?>@endif"><i class="fas fa-pen"></i></a></em></span> </h1>
    <a href="{{ route( 'subcontractor.workers' ) }}" class="back-link">< Back to Workers</a>
</div>
<div class="row justify-content-center trainer_view_wrapper">
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">

        <? /*@php

$next = \App\User::role('worker')->where('status', 2)->where('id', '<', $user->id)->orderBy('id','desc')->first();

$previous = \App\User::role('worker')->where('status', 2)->where('id', '>', $user->id)->orderBy('id','asc')->first();

@endphp */ ?>

<?php
$previous = $previous1;

$next = $next1;
?>

@if( $user->hasRole('Worker') )

        <div class="next-prev-navigation">

                <div class="prev" >

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('subcontractor.workers.view', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('subcontractor.workers.view', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls" style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>

            @endif
  
            <div class="card-body">
                <div class="row">
                   <div class="col-md-3 text-center">
                      <div id="profile-image" class="profile-img" style="background:url({{ asset('uploads/images/users/' . $user->id . '/' . $user->image) }}); background-size:cover; background-position:center;"> 
                        @if($user->image)
                        <img style="display:none;" src=""  alt="profile-image" class="img-profile" >
                        @else
                        <img style="display:none;" src="{{ asset('images/elements/user.png') }}" id="profile-image" alt="profile-image" class="img-profile" >
                        @endif
                      </div>
                    </div>
                    <div class="col-md-9">
                      <div class="user-bio-section">
                        <div class="bio-inner-section">
                        <div class="form-group">
                            <label> <i class="far fa-user"></i> Name  </label>
                            <p class="businesses_view_text user-profile-name">{{ $user->first_name }} {{ $user->last_name }}</p>
                        </div>
                        <div class="form-group">
                          <label><i class="far fa-user"></i> Subcontractor </label>
                          <p class="businesses_view_text sub-contrsct-name"><span style="font-weight:normal;"><b>Company:</b> {{ $user->sub_contractor_name }}</span> 

                            @if( !empty( $user->updated_by ) )

                            <?php 

                            $updateBy = \App\User::find( $user->updated_by );

                            ?>

                             <i class="stamp"> <b>Last edited on:</b>

                              {{ date( 'M d, Y', strtotime( $user->updated_at ) ) }} by 

                              @if( $updateBy->hasRole('Subcontractor') )

                              {{ $updateBy->company_name }}

                              @else

                              {{ $updateBy->name }}

                              @endif

                             </i>
                             <div class="clearfix"></div>
                            @endif

                          </p>
                        </div>
                        <div class="bio-section" style="margin-top:6px;">
                          <div class="row">
                         
                          <div class="col-md-6">
                            <div class="form-group">
                              <label> <i class="far fa-envelope"></i> Email </label>
                              <p class="businesses_view_text"><span><i class="far fa-envelope"></i> </span><?php echo empty( $user->email ) ? 'N/A' : $user->email; ?></p>
                              </div>
                            <div class="form-group">
                              <label> <i class="fas fa-mobile-alt"></i> Phone Number </label>
                                @php

                                  $phone_number = ($user->country_code ? $user->country_code . '-' : '') . $user->phone_number;

                                @endphp
                              <p class="businesses_view_text"><span> <i style="transform: rotate(95deg);" class="fa fa-phone" aria-hidden="true"></i> </span> {{ str_replace('-', '', $phone_number) != '' ? $phone_number : 'Not available'}}</p>
                            </div>

                            @if($user->ssn_id)
                            <div class="form-group">
                            <label> <i class="fas fa-mobile-alt"></i> SSN ID </label>
                              
                            <p class="businesses_view_text"><span> <i class="fa fa-id-card" aria-hidden="true"></i> </span> {{ $user->ssn_id }}</p>
                            </div>
                            @endif
							
							<div class="form-group">
                              @if( $user->hasRole('Worker') )
                                <label> <i class="far fa-user"></i> Worker Type </label>
                                <p class="businesses_view_text"> <span><i class="far fa-user"></i></span> {{ ucwords( $user->worker_type_name ) }} </p>
                                @endif
                            </div>

                            @if( $user->hasRole('Worker') )

                          <div class="form-group">
                              
                                <label> <i class="far fa-user"></i> Registered On </label>
                                <p class="businesses_view_text"> <span><i class="fas fa-user-clock"></i></span> {{ \Carbon\Carbon::parse($user->created_at)->tz(str_before(\App\User::role('superadmin')->first()->timezone_manual, ','))->format('jS F Y, h:i A') }} </p>
                                
                            </div>

                          @endif

                          </div>
                          
                         
                          </div>                      
                        </div>
                      </div>
                      </div>
                    </div>
					
					<div class="col-md-12">
					 
					  
					  <div class="user_view_wrap">
					   <div class="currently_up_title">Currently Uploaded</div>
					   
					   
					    <div class="form-group more-pdf worker_doc_view_wrap">
					     @if( count( $user->workerDocuments ) > 0 )

               @php

                        $count = 0;

                        @endphp

                              @foreach( $user->workerDocuments as $get )

                              

                              @if( $get->author == Auth::user()->id || $get->show )


                              @php

                        $count = 1;

                        @endphp
                              

                                <div class="document-view row">

                                  <div class="col-md-6 form-group">
                                    <label class="doc_label">Notes/Training</label>
                                   <span>
                                        
                                        <span class="">{{ empty( $get->notes ) ? 'N/A' : $get->notes }}</span>
                              

                                   </span>
                                   
                                </div>

                                <div class="col-md-6 form-group">
                                   <label class="doc_label">Document</label>  
                                   <span>
                                        @if($get->document)
                                        <span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a target="_blank" href="{{ asset('public/uploads/documents') }}/{{ $user->id }}/{{ $get->document }}">{{ strlen($get->document) > 20 ? substr($get->document,0,20)."..." : $get->document }}</a>
                                        @else
                                        N/A
                                        @endif
                                   </span>
                                   
                                </div>

                                </div>

                                @endif

                                @endforeach

                                @if( $count == 0 )

                        <p class="no-worker-documents" style="margin: 0 auto; padding-top: 10px;">

                            No documents uploaded

                        </p>


                        @endif
  
                              @else

                                <p class="no-worker-documents" style="margin: 0 auto; padding-top: 10px;">

                                  No documents uploaded

                                </p>

                              @endif
					  </div>
					</div>
					</div>

                    @if( $user->doc_upload )
                    
                    <div class="col-lg-12" style="display:none;">
                        <div class="worker-note">
                           @if( $user->hasRole('Worker') )
                          <div class="form-group">
                              <p class="businesses_view_text"> <span>Notes/Training: </span>
                              @if( empty( $user->notes_training ) ) N/A @else {{ $user->notes_training }} @endif
                              </p>
                          </div>
                      </div>
                      </div>

                    @endif
                  
                    @endif
                </div>
    
            </div>
        </div>
    </div>
    @if( $user->hasRole('Worker') )
    <div class="col-lg-12">
        <h6 class="logs_heading">Logs</h6>
        <div class="card shadow mb-4">
            <div class="card-body">
                <style>
          .project_view_table th{
              position:relative;
          }
          .project_view_table th.sorting_asc, th.sorting_desc, .project_view_table th.sorting_asc, th.sorting_desc, th.sorting, th.sorting{
            cursor:pointer;  
          }
          
          .project_view_table th.sorting_asc:after, th.sorting_desc:after, .project_view_table th.sorting_asc:before, th.sorting_desc:before, th.sorting:before, th.sorting:after{
            position: absolute;
          bottom: 0.9em;
          display: block;
          opacity: 0.3;  
                  
          }
          
          th.sorting_asc:before{
          right: 1em;
            content: "\2191"; 
 cursor:pointer;            
          }
          th.sorting:before{
            right: 1em;
            content: "\2191";  
            cursor:pointer;
          }
          th.sorting_asc:after{
                    right: 0.5em;
                    content: "\2193"; 
 cursor:pointer;                    
          }
          th.sorting:after{
            right: 0.5em;
                    content: "\2193";
           cursor:pointer;                  
          }
          th.sorting_desc:before{
               right: 1em;
             content: "\2191";
              cursor:pointer;
          }
          
          th.sorting_desc:after{
                right: 0.5em;
               content: "\2193"; 
 cursor:pointer;               
          }
          
        </style>
            
                <table class="table table-bordered table-hover project_view_table" id="datatable">
  
                    <thead>
    
                        <tr>
                            <th scope="col">Project Name</th>
                            <th scope="col">Project #</th>
                            <th scope="col">Date</th>
                            <th scope="col" style="text-align: center;" >In</th>
                            <th scope="col" style="text-align: center;" >Out</th>
                            <th scope="col" style="text-align:center;">Total Hrs</th>

                        </tr>
  
                    </thead>
                    <tbody></tbody>
                </table>
                {!! getIconGuides() !!}
            </div>
        </div>
    </div>
    @endif

    @if( $user->hasRole('Worker') )

     <div class="col-lg-12">
        <h6 class="logs_heading safety-strikes-head">Safety Strikes
          @if( \DB::table('safety_strikes')->whereNull('deleted_at')->where('worker_id', $user->id)->exists() )
          <span><a class="btn export-btn" href="{{ route('strike.export', $user->id) }}">Export</a></span>
          @endif
        </h6>
        <div class="card shadow mb-4">
            <div class="card-body">

         
             <div class="form-group more-pdf strike-wrap worker_doc_view_wrap strike-table-wrapper">
                              <?php 

                              $subworkers = null;

                              if( $user->hasRole('Subcontractor') )
                              {

                                  $subworkers = \App\User::where('parent', $user->id)->where('status', 2)->count();

                             
                              }


                              if( \DB::table('safety_strikes')->where('worker_id', $user->id)->exists() ): 


                                ?>

                              <?php
                              if( $user->hasRole('Subcontractor') )
                              {

                                  $subworkers = \App\User::where('parent', $user->id)->where('status', 2)->pluck('id')->toArray();

                                  $strikes = \App\Strike::whereIn('worker_id', $subworkers)->get();
                              }
                              else
                              {
                                  $strikes = \App\Strike::where('worker_id', $user->id)->get();
                              }
                              

                              ?>

                              @if( count( $strikes ) == 0 )
                            <p class="no-worker-documents">

                                  No safety strikes

                                </p>
                            @else

                          <table>

                            <th>Date</th>
                            <th>Job</th>
                            <th>Worker</th>
                            <th>Category</th>
                            <th>Violation Description</th>
                            <!--<th>Media</th>-->
                            <th>Issued By</th>
                            <th>Action</th>
                            

                            @foreach( $strikes as $g )

                              <tr>

                                <td style="width:150px;">{{ date("m-d-Y", strtotime($g->strike_date)) }}</td>
                                
                                <td style="width:200px;">

                                   @if( empty( $g->name ) )

                                    N/A

                                  @else

                                    @if( is_numeric( $g->name ) )

                                      @if( \App\Project::where('id', $g->name)->exists() )

                                        {{ \App\Project::where('id', $g->name)->first()->job_name }}

                                      @else

                                      ({{ $g->name }}) Invalid Project Id

                                      @endif

                                    @else

                                      {{ $g->name }}

                                    @endif

                                  @endif

                                </td>

                                <td style="width:200px;">
                                    
                                  @if( \DB::table('users')->where('id', $g->worker_id)->exists() )

                                    {{ \DB::table('users')->where('id', $g->worker_id)->first()->name }}

                                  @else

                                    N/A

                                  @endif

                                </td>

                                <td style="width:150px;">
                                   @if( !empty( $g->category ) )
                                                   
                                                      {{ $g->category }}
                                                   
                                                    @else

                                                    N/A
                                                    
                                                    @endif
                                  </td>

                                <td style="width:300px;">{{ $g->description }}</td>

                                <td style="width:150px;">
                                   

                                  <?php

                                  $issuedBy = 'N/A';

                                  if(\DB::Table('users')->where('id', $g->created_by)->exists())
                                  {
                                    $issuedBy = \DB::Table('users')->where('id', $g->created_by)->first()->name;
                                  }  

                                  echo $issuedBy;

                                  ?>

                                  </td>
                                <!--<td style="width:210px;">

                                  @if( !empty( $g->media ) && $g->isvideo )
                                  <video style="border-radius:5px;"  width="150" controls>
                                    <source src="<?php echo url('/'); ?>/public/uploads/strike/{{$g->media}}" type="video/mp4">
                                  </video>
                                  @endif

                                  @if( !empty( $g->media ) && !$g->isvideo )
                                 
                                    <img width="150" style="border-radius:5px;" src="<?php echo url('/'); ?>/public/uploads/strike/{{$g->media}}">
                                 
                                  @endif

                                  @if( empty( $g->media ) )
                                    <div class="no-media-text">No media uploaded</div>
                                  @endif

                                </td>-->
                
                                <td style="width:150px;">
                                  
                                  <a style="text-transform: none;" onclick="return confirm('Are you sure you want to delete this strike?')" href="{{ route('admin.strike.delete', $g->id) }}"><i  style="color: red;" class="fa fa-trash"></i></a>
                                  <a style="text-transform: none;" href="{{ route('admin.strike.edit', [$g->id, $user->id]) }}"><i class="fa fa-edit"></i></a>

                                </td>

                              </tr>

                            @endforeach

                            

                          </table>

                          @endif
  
                              <?php else: ?>

                                <p class="no-worker-documents">

                                  No safety strikes

                                </p>

                              <?php endif; ?>

                            </div>
            </div>
          </div>

          @endif

</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

var ls = localStorage['workerIds'];

if( ls )
{

var lsa = $.parseJSON(ls);

console.log( lsa );

var val = "{{ $user->id }}";

var next = lsa.indexOf(val)+1;

var previous = lsa.indexOf(val)-1;


if(lsa[previous]  )
{

  $('.prev-ls a').attr('href', "{{ route('subcontractor.workers.view') }}/"+lsa[previous])


}

if(lsa[next]  )
{
  $('.next-ls a').attr('href', "{{ route('subcontractor.workers.view') }}/"+lsa[next])
}
}
else
{
  $('.prev-ls').hide();
  $('.next-ls').hide();

  $('.prev').show();
  $('.next').show();
}



    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-delete', function(event) {
        if(!confirm('Are you sure you want to delete this log?')) {
        
            event.preventDefault();
        }
    });
    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-edit', function(event) {
        var thisRow = $(this).parent().parent().parent();
        console.log( thisRow );
        /*-------------------------------------------- Seperated --------------------------------------------*/
        $('.btn-action-edit').show();
        $('.btn-action-update').hide();
        $( this ).hide();
        thisRow.find('.btn-action-update').show();
        $('.edit-punch-in-time').slideUp('fast');
        $('.edit-punch-out-time').slideUp('fast');
        thisRow.find('.edit-punch-in-time').slideDown('fast');
        thisRow.find('.edit-punch-out-time').slideDown('fast');
        /*-------------------------------------------- Seperated --------------------------------------------*/
    })
    /*
     * @Function Name
     *
     *
     */
     $(document).on('click', '.btn-action-update', function(event) {
        var thisRow = $(this).parent().parent().parent();
        var logId = $(this).attr('log-id');
        var punchIn = thisRow.find('.edit-punch-in-time input').val();
        var punchOut = thisRow.find('.edit-punch-out-time input').val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        /*-------------------------------------------- Seperated --------------------------------------------*/
        $.ajax({
            type:'POST',
            url:'{{ route("admin.log.update") }}',
            data: { 
                _token: CSRF_TOKEN,
                logId: logId, 
                punchIn: punchIn,
                punchOut: punchOut 
            },
            success:function( data )
            {
                var response = JSON.parse( data );
                var returnedPunchIn = response.data.punch_in_time;
                var returnedPunchOut = response.data.punch_out_time;
                thisRow.find('.punch-in-time').text( returnedPunchIn );
                thisRow.find('.total-hours-class').text( response.total_time );
                thisRow.find('.punch-out-time').text( returnedPunchOut );
                thisRow.find('.edit-punch-in-time').slideUp('fast');
                thisRow.find('.edit-punch-out-time').slideUp('fast');
                $('.btn-action-edit').show();
                $('.btn-action-update').hide();
            }
        })
     });
    /*
     * @Function Name
     *
     *
     */
    function initialize_database() {

      $.fn.dataTable.moment('MM-DD-YYYY');
      $.fn.dataTable.moment('h:mm a');
      
        $('#datatable').DataTable({
          "order": [[ 2, 'desc' ], [ 3, 'desc' ]],
            "bLengthChange": true,
            language: {
              sEmptyTable: "No data available.",
            sLengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Search...",
             sProcessing: "<i class='fa fa-circle-o-notch fa-spin'></i>",
             },
            processing: true,
            serverSide: false,
            "ajax": {
              "url": "{{ route('subcontractor.datatable.logs.get') }}",
              "data": function ( d ) {
               d.userId = "{{ $user->id }}"
              
            }
            },
            columns: [
                { data: "project_name", name: "project_name", searchable: true, orderable: true },
                { data: "project_number", name: "project_number", searchable: true, orderable: true },
                { data: "date", name: "date", searchable: false, orderable: true },
                
                { data: "punch_in", name: "punch_in", searchable: true, orderable: true },
                
                { data: "punch_out", name: "punch_out", searchable: false, orderable: true },
                { data: "total_hours", name: "total_hours", searchable: false, orderable: true },
            ],
        });
    }
    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {
        initialize_database();
    });
</script>
@endsection