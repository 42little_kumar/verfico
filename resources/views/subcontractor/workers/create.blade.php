@extends('admin.layouts.app')

@section('title', 'New Worker')

@section('styles')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">New User</h1>

    <a href="{{ route('subcontractor.workers') }}" class="back-link">< Back to Users</a>
    
</div>

<form action="{{ route('subcontractor.workers.create') }}" id="create-user" method="POST" enctype="multipart/form-data">
            
    @csrf

    <div class="row">

        <div class="col-lg-4">

            <div class="card shadow mb-4">
        
                <div class="card-header py-3">
            
                    <h6 class="m-0 font-weight-bold text-primary">Image</h6>
            
                </div>
            
                <div class="card-body text-center" id="update-profile-image">

                    <img src="{{ asset('images/elements/user.png') }}" id="profile-image" alt="profile-image" class="img-profile rounded-circle" style="width: 150px; height: 150px;">

                    <hr>

                    <input type="file" name="image" accept="image/*" style="display:none">

                    <button id="change-profile-image" type="button" class="btn btn-info btn-icon-split">

                        <span class="icon text-white-50">
                        
                            <i class="far fa-image"></i>
                        
                        </span>
                        
                        <span class="text">Upload</span>
                    
                    </button>

                    <button id="remove-profile-preview" style="display:none" type="button" class="btn btn-danger btn-icon-split">

                        <span class="icon text-white-50">
                        
                            <i class="fas fa-times"></i>
                        
                        </span>
                        
                        <span class="text">Remove</span>
                    
                    </button>

                </div>
            
            </div>
            
        </div>

        <div class="col-lg-8">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>First Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="first-name" placeholder="First Name" value="{{ old('first-name') }}" maxlength="50" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Last Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="last-name" placeholder="Last Name" value="{{ old('last-name') }}" maxlength="50" required="">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Email<span class="text-danger">*</span></label>

                                <input type="email" class="form-control" name="email" placeholder="Email Address" autocomplete="off" value="{{ old('email') }}" required>

                                <span id="error-email" class="error-message text-danger"></span>

                            </div>

                        </div>

                        {{--<div class="col-md-6">

                            <div class="form-group">

                                <label>Username<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" maxlength="25" required>

                                <span id="error-username" class="error-message text-danger"></span>

                            </div>

                        </div>--}}

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Phone Number</label>

                                <div class="country_code"> 

                                    <input type="text" class="form-control" name="phone-number" placeholder="Phone Number" value="{{ old('phone-number') }}">
                            
                                </div> 
                                
                                <span id="error-phone-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Password<span class="text-danger">*</span></label>

                                <input type="password" pattern=".{6,}" title="6 characters minimum" class="form-control" name="password" placeholder="Password" required>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <!-- <div class="col-md-6">

                            <div class="form-group">

                                <label>Gender</label>

                                <select name="gender" class="form-control">$subContId

                                    <option value="">Select</option>

                                    <option value="male" @if(old('gender') && old('gender') == 'male') selected @endif>Male</option>

                                    <option value="female" @if(old('gender') && old('gender') == 'female') selected @endif>Female</option>

                                    <option value="others" @if(old('gender') && old('gender') == 'others') selected @endif>Others</option>

                                </select>

                            </div>

                        </div> -->

                        <div class="col-md-6">

                            <div class="form-group worker-type-field">

                                <label>Worker Type</label>

                                <select name="worker_type" class="form-control" required="">
                                    
                                    <option value="">Select</option>

                                    <option value="finisher">Finisher</option>

                                    <option value="mechanic">Mechanic</option>

                                    <option value="laborer">Laborer</option>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                       <div class="col-md-6">

                            <div class="form-group subcontractor-field" style="display: none;" >

                                <input type="hidden" name="general_contractor" value="{{ $subContId }}">

                                <input type="hidden" name="user_role" value="worker">

                            </div>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Create</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">

$(document).ready(function() {

     /*
      * @Function Name
      *
      *
      */
    $('#change-profile-image').on('click', function(event) {

        $('#update-profile-image input[name="image"]').click();

        event.preventDefault();

    });

    $('#update-profile-image input[name="image"]').on('change', function(event) {

        previewImage(this, '#profile-image');

        $('#remove-profile-preview').show();

    });

    $('#remove-profile-preview').on('click', function(event) {

        $('#update-profile-image input[name="image"]').val('');

        $('#profile-image').attr('src', '{{ asset("images/elements/user.png") }}');

        $(this).hide();

        event.preventDefault();

    });

    $('input[name="phone-number"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });

    $('#create-user').on('submit', function(event) {

        var error = false;

        $('.form-group .error-message').html('');

        var name = $('input[name="first-name"]').val();
        
        var email = $('input[name="email"]').val().toLowerCase();

        var phone_number = $('input[name="phone-number"]').val();
        
        var username = $('input[name="username"]').val();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter first name of user.');

        }

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('First name should contain atleast one alphabetic character.');
            
        }

        if(!email.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) {

            error = true;
            
            $('#error-email').html('Enter a valid email address of user.');
            
        }

        if(phone_number != '' && phone_number.replace(/[^0-9]/g, "").length != 10) {

            error = true;
            
            $('#error-phone-number').html('Please enter a valid phone number for user.');
            
        }

        if(username == '') {

            error = true;

            $('#error-username').html('Please enter username of user.');

        }

        if(username != '' && /\s/.test(username)) {

            error = true;
            
            $('#error-username').html('Username cannot contain spaces.');

        }

        if(username != '' && !/[a-zA-Z]/.test(username)) {

            error = true;

            $('#error-username').html('Username should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();

        }

    });

});

</script>

@endsection