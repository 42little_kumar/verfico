@extends('admin.layouts.app')

@section('title', 'Edit : ' . $user->first_name .' '. $user->last_name)

@section('content')

@section('styles')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">

<link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">
<style>
 .current-uploaded-document .title {
    background: #fff;
    color: #000;
    margin-bottom: 18px;
    padding: 0 10px;
    margin-top: 9px;
    font-size: 18px;
    font-weight: bold;
    border-bottom: 1px solid #ddd;
    padding: 10px 0 9px;
}
i.fa.fa-times.delete-document {
    background: red;
    color: white;
    padding: 2px 5px;
    border-radius: 50%;
}
.col-md-12.additional-notedoc-element {
    margin: 20px 0 0;
    padding: 0 25px;
    display: flex;
    align-items: center;
    position: relative;
}
.flex_wrap{
 display:flex;
 align-items:center; 
}
.col-md-12.additional-notedoc-element .switch{
 display:none;	
}
.col-md-12.additional-notedoc-element textarea.form-control, .small_textarea textarea.form-control {
    height: 47px !important;
}
.col-md-12.add-more a {
    display: block;
    align-items: center;
    justify-content: center;
    padding: 11px;
    background: #103359;
    font-size: 15px;
    text-transform: uppercase;
    font-weight: 600;
    border-radius: 50px;
    text-align: center;
    color: #fff !important;
}
.col-md-12.add-more {
    padding: 0 25px;
    margin-bottom: 15px !important;
}
.btn.btn-primary{
    width: auto;
    display: table;
    margin: 28px auto;	
}
a.document-close {
    position: absolute;
    right: 82px;
    color: #ff0000;
    border: 1.2px solid #ff0000;
    height: 30px;
    width: 30px;
    border-radius: 50%;
    text-align: center;
    padding: 3px 0;
}
.worker-container .col-md-3{
     -webkit-box-flex: 0;
    -ms-flex: 0 0 40%;
    flex: 0 0 40%;
    max-width: 40%;	
}
span#ssn-id {
    background: #f2f2f2;
    padding: 11px 12px;
    color: #000 !important;
    font-weight: bold;
    border-radius: 5px;
    margin: 0 6px 0 0;
    font-size: 14px;
}

</style>
@endsection

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Edit : {{ $user->first_name }} {{ $user->last_name }}</h1>

    <a href="{{ route('subcontractor.workers') }}" class="back-link">< Back to {{ getRoleTitle( $user->getRoleNames()[0] ) }}</a>
    
</div>

<form action="{{ route('subcontractor.workers.update', [ 'id' => $user->id ]) }}" id="edit-user" method="POST" enctype="multipart/form-data">
            
    @csrf

    <div class="row">

        

        <div class="col-lg-12">

       <?php /*@php

            $next = \App\User::role('worker')->where('status', 2)->where('parent', Auth::user()->id)->where('id', '<', $user->id)->orderBy('id','desc')->first();

            $previous = \App\User::role('worker')->where('status', 2)->where('parent', Auth::user()->id)->where('id', '>', $user->id)->orderBy('id','asc')->first();
            
            @endphp*/ ?>



        @if( $user->hasRole('Worker') )

        <div class="next-prev-navigation">

                <div class="prev" >

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('subcontractor.workers.edit', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('subcontractor.workers.edit', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls" style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>

            @endif

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
                
                </div>

                <div class="card-body">

                    


                    <div class="row">

                        
                        <div class="col-md-6">

                            <div class="form-group">

                                <label>First Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="first-name" placeholder="First Name" value="{{ $user->first_name }}" maxlength="50" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Last Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="last-name" placeholder="Last Name" value="{{ $user->last_name }}" maxlength="50" required="">

                            </div>

                        </div>
                        

                         <div class="col-md-6">

                            <div class="form-group">

                                <label>Status<span class="text-danger">*</span></label>

                                <select name="status" class="form-control">
                                
                                    <option value="2" @if($user->status == 2) selected @endif>Active</option>
                                    
                                    <option value="0" @if($user->status == 0) selected @endif>Blocked</option>

                                </select>

                            </div>

                        </div>

                       <div class="col-md-6">

                            <div class="form-group worker-type-field" @if( !$user->hasRole('Worker') ) style="display: none;" @endif >

                                <label>Worker Type<span class="text-danger">*</span></label>

                                <?php $workerTypeSaved = $user->worker_type; ?>

                                <select name="worker_type" class="form-control" @if( $user->hasRole('Worker') ) required @endif>
                                    
                                    <option value="">Select</option>

                                    @foreach( $workerTypes as $workerType )

                                    <?php

                                    $workerTypeId = $workerType->id;

                                    ?>

                                     <option value="{{ $workerTypeId }}" @if( $workerTypeId == $workerTypeSaved ) selected @endif>{{ $workerType->type }}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>Rock Spring Safety Training received on:</label>

                                @if( !empty( $user->training ) )
                                  
                                 <input type="text" class="form-control training" name="training" placeholder="" value="{{ date('m/d/Y', strtotime($user->training)) }}" autocomplete="off">

                                 @else

                                 <input type="text" class="form-control training" name="training" placeholder="" autocomplete="off">

                                 @endif
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>SSN ID</label>
                                  
                                 <div class="flex_wrap">
                                

                                <?php

                                $companyName = Auth::user()->company_name;
                                $companyName = str_replace(' ', '', $companyName);
                                $companyName = strtoupper(  substr($companyName, 0, 4)  );

                                ?>

                                <span style="color: blue;" id='ssn-id'>{{ $companyName }}</span>

                                <div class="flex_wrap">

                                <input type="hidden" id="ssn-id-pre" value="{{ $companyName }}" name="ssn_id_pre">

                                <input type="text" class="form-control" id="" maxlength="4" value="{{ substr( $user->ssn_id, 4, 4) }}" name="ssn_id" placeholder="SSN ID">
                                 </div>
                             </div>
                            </div>

                        </div>


                    </div>

                    @if( $user->doc_upload )

                    <!--<div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Notes/Training</label>

                                <textarea class="form-control" name="notes_training" maxlength="1000">{{ $user->notes_training }}</textarea>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Worker document (PDF)

                                @if( $user->worker_document )

                                <p>

                                    <span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a target="_blank" href="{{ asset('public/uploads/images/worker_documents') }}/{{ $user->id }}/{{ $user->worker_document }}">{{ strlen($user->worker_document) > 20 ? substr($user->worker_document,0,20)."..." : $user->worker_document }} </a>

                                    <a href="{{ route('admin.worker.document.delete', $user->id ) }}" class="delete-document" onclick="return confirm('Are you sure you want to delete this document?');"><i class="fa fa-times delete-document"></i></a>


                                </p>

                                @endif

                                </label>

                                <input type="file" class="form-control" id="file_upload" name="worker_document">

                            </div>

                        </div>

                    </div>-->

                    @endif

                    <div class="row current-uploaded-document">

                        <div class="col-md-12">

                            <div class="title">Currently Uploaded</div>

                        </div>
						
						<div class="updated_doc_wrp" style="width:100%;">
						<div class="row">
						  <div class="col-md-6">
						    <label class="doc_label">Notes/Training</label>
						  </div>
						  <div class="col-md-6">
						    <label class="doc_label">Documents</label>
						  </div>
						  
						</div>
						</div>

                    @if( count( $user->workerDocuments ) > 0 )

         
                    
						

                        <?php $count = 1; ?> 

                        @php

                        $count = 0;

                        @endphp

                        @foreach( $user->workerDocuments as $get )

                        

                        @if( $get->author == Auth::user()->id || $get->show )

                        @php

                        $count = 1;

                        @endphp

                            <div class="col-md-12 additional-notedoc-element document_updated_text_wrap">

                                <div class="col-md-6 form-group">
                                   <label class="doc_label label_for_mobile">Notes/Training</label>
                                   <span>
                                        
                                        <span class="">{{ empty( $get->notes ) ? 'N/A' : $get->notes }}</span>
                              

                                   </span>
                                   
                                </div>

                                <div class="col-md-4 form-group">
                                   <label class="doc_label label_for_mobile">Document</label>
                                   <span>
                                        
                                        <span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a target="_blank" href="{{ asset('public/uploads/documents') }}/{{ $user->id }}/{{ $get->document }}">{{ strlen($get->document) > 20 ? substr($get->document,0,20)."..." : $get->document }}</a>

                                        

                                   </span>
                                   
                                </div>
								
								<div class="col-md-2">
								  @if( $get->author == Auth::user()->id )

                                        <a style="color: #ff0000;text-decoration: underline;" href="{{ route('admin.worker.document.delete', $get->id ) }}" class="delete-document" onclick="return confirm('Are you sure you want to delete this record?');">Delete</a>

                                        @endif
								</div>


                              
                                        
                                       <label class="switch" style="display:none;">

                                        <input type="checkbox" class="manual-log" value="1"  name="doc_upload[]">

                                        <span class="slider round" style="width: 50px;"></span>

                                        </label>
                                   
                                
                                

                            </div>

                            <?php $count++; ?>

                            @endif

                        @endforeach

                        @if( $count == 0 )

                        <p class="no-worker-documents" style="margin: 0 auto; padding-top: 10px;">

                            No documents uploaded

                        </p>


                        @endif

                    
					
					

                    @else

                    <p class="no-worker-documents" style="margin: 0 auto; padding-top: 10px;">

                            No documents uploaded

                        </p>

                    @endif

                    </div>

                    <br/>

                    @if( count( $user->workerDocuments ) < 10 )

                    <!-- Multiple data -->

                    <div class="row worker-container">

                        <div class="col-md-12 add-more" style="margin-bottom: 10px;">

                            <a href="javascript:void(0);" class="add-notedoc"><i class="fa fa-plus"></i>  Add Notes/documents</a>

                        </div>
                        <div class="col-md-12 add_doc_first_row" style="display: flex; align-items: center; padding: 0px 25px;">   
                        <div class="col-md-6 small_textarea">

                        <textarea placeholder="Notes/Training" class="form-control" name="notes_training[]" maxlength="1000"></textarea>

                        </div>

                        <div class="col-md-3">

                            <input title="Document" type="file" class="form-control doc-field" id="" name="worker_document[]">

                        </div>

                        <div class="col-md-3" style="display:none;">

                        <label class="switch" style="display:none;">

                        <input type="checkbox" class="manual-log" value="1"  name="doc_upload[]">

                        <span class="slider round" style="width: 50px;"></span>

                        </label>

                        </div>
						</div>
						<div class="clearfix"></div>

                    </div>

                    <!-- /Multiple data -->
                  
                    @endif

                    

                    

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Update</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>
</div>
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">

var ls = localStorage['workerIds'];

if( ls )
{

var lsa = $.parseJSON(ls);

console.log( lsa );

var val = "{{ $user->id }}";

var next = lsa.indexOf(val)+1;

var previous = lsa.indexOf(val)-1;


if(lsa[previous]  )
{

  $('.prev-ls a').attr('href', "{{ route('subcontractor.workers.view') }}/"+lsa[previous])


}

if(lsa[next]  )
{
  $('.next-ls a').attr('href', "{{ route('subcontractor.workers.view') }}/"+lsa[next])
}
}
else
{
  $('.prev-ls').hide();
  $('.next-ls').hide();

  $('.prev').show();
  $('.next').show();
}

$(document).ready(function() {

    $( ".training" ).datepicker();

    /* Worker add note document */

    $('.add-notedoc').click(function(){

if( $( ".additional-notedoc-element" ).length == 9 )
    return false;

$('.worker-container').append('<div class="col-md-12 additional-notedoc-element"><div class="col-md-6"><textarea placeholder="Notes/Training" class="form-control" id="" name="notes_training[]"></textarea></div><div class="col-md-3"><input title="Document" type="file" class="form-control doc-field" id="" name="worker_document[]"></div><div class="col-md-3"><label class="switch"><input type="checkbox" class="manual-log" value="1"  name="doc_upload[]"><span class="slider round" style="width: 50px;"></span></label></div><a href="javascript:void(0);" class="document-close"><i class="fa fa-times close-document-button"></i></a></div>');

})

 $(document).on("click", ".document-close", function(){

        $(this).parent().remove();

    })

    /* Check each document field for PDF */

    $('.doc-field').click(function()
    {

        if( $(this).val() != '' )
        {

            $(this).val('');
                
        }

    })

    $(document).on("change", ".doc-field", function(){

        var ext = $(this).val().split('.').pop().toLowerCase();

        if( $.inArray(ext, ["pdf"]) == -1 )
        {

            alert('Document should be only PDF!');

            $(this).val('');

            return false;

        }


    })

    /*
     * @Function Name
     *
     *
     */

     $(document).on('change', '#role-select', function(){ 

        $('.subcontractor-field').hide();

        $('.worker-type-field').hide();

        var role = $(this);

        var roleVal = role.val();

        if( roleVal == 'worker' )
        {

            $('.subcontractor-field').show();

            $('.worker-type-field').show();

        }

        if( roleVal == 'foreman' )
        {

            $('.subcontractor-field').show();

        }

     } );

     $('#file_upload').change(function()
    {

        var ext = $('#file_upload').val().split('.').pop().toLowerCase();

        if( $.inArray(ext, ["pdf"]) == -1 )
        {

            alert('Worker document should be only PDF!');

            $('#file_upload').val('');

        }

    })

    /*
     * @Function Name
     *
     *
     */
    $('#change-profile-image').on('click', function(event) {

        $('#update-profile-image input[name="image"]').click();

        event.preventDefault();

    });

    $('#update-profile-image input[name="image"]').on('change', function(event) {

        previewImage(this, '#profile-image');

        $('#remove-profile-preview').show();

    });

    $('#remove-profile-preview').on('click', function(event) {

        $('#update-profile-image input[name="image"]').val('');

        $('#profile-image').attr('src', '{{ asset("images/elements/user.png") }}');

        $(this).hide();

        event.preventDefault();

    });

    $('input[name="phone-number"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });

    $('#edit-user').on('submit', function(event) {

        var error = false;

        $('.form-group .error-message').html('');

        var name = $('input[name="first-name"]').val();
        
        var email = $('input[name="email"]').val().toLowerCase();

        var phone_number = $('input[name="phone-number"]').val();
        
        var username = $('input[name="username"]').val();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter first name of user.');

        }

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('First name should contain atleast one alphabetic character.');
            
        }

        if(!email.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) {

            error = true;
            
            $('#error-email').html('Enter a valid email address of user.');
            
        }

        if(phone_number != '' && phone_number.replace(/[^0-9]/g, "").length != 10) {

            error = true;
            
            $('#error-phone-number').html('Please enter a valid phone number for user.');
            
        }

        if(username == '') {

            error = true;

            $('#error-username').html('Please enter username of user.');

        }

        if(username != '' && /\s/.test(username)) {

            error = true;
            
            $('#error-username').html('Username cannot contain spaces.');

        }

        if(username != '' && !/[a-zA-Z]/.test(username)) {

            error = true;

            $('#error-username').html('Username should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();

        }

    });

    $('.btn-remove-image').on('click', function(event) {

        if(!confirm('Are you sure you want to remove the image?')) {

            event.preventDefault();

        }

    });

});

</script>

@endsection