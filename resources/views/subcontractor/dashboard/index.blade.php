@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    
</div>

<div class="row">


        <div class="col-md-4 col-lg-3">

        <a href="{{ route('subcontractor.projects') }}">

        	<div class="dashboard-block" style="border-color:#55efc4;">
              <div class="dashboard_block_inner" style="background:#55efc4;">
			    <div class="dashboard-block-value"> {{ $totalProjects }} </div>
        		@if( $totalProjects == 1 )

              <label> Total Project </label>

              @else

              <label> Total Projects </label>

              @endif
               </div>
        	</div>

        </a>

        </div>

        <div class="col-md-4 col-lg-3">
          
          <a href="{{ route('subcontractor.projects') }}">

          <div class="dashboard-block" style="border-color:#55efc4;">
              <div class="dashboard_block_inner" style="background:#55efc4;">
          <div class="dashboard-block-value"> {{ $totalActiveProjects }} </div>
            @if( $totalActiveProjects == 1 )

                          <label> Total Active Project </label>

                          @else

                          <label> Total Active Projects </label>

                          @endif
               </div>
          </div>

        </a>

        </div>
		
        <div class="col-md-4 col-lg-3">

          <a href="{{ route('subcontractor.workers') }}">
        	
        	<div class="dashboard-block" style="border-color:#ff7675;">
              <div class="dashboard_block_inner" style="background:#ff7675;">
			    <div class="dashboard-block-value"> {{ $totalWorkers }} </div>
        		@if( $totalWorkers == 1 )

              <label> Total Worker </label>

              @else

              <label> Total Workers </label>

              @endif
              </div>
        	</div>

        </a>

        </div>

        <div class="col-md-4 col-lg-3">

          <a href="{{ route('subcontractor.payrolls') }}">
        	
 			<div class="dashboard-block" style="border-color:#fdcb6e;">
              <div class="dashboard_block_inner" style="background:#fdcb6e;">
			    <div class="dashboard-block-value"> {{ $totalPayroll }} </div>
 				@if( $totalPayroll == 1 )

              <label> Total Payroll </label>

              @else

              <label> Total Payrolls </label>

              @endif
              </div>
 			</div>

    </a>

        </div>

 		
</div>

@if( $billboard )

<div class="row">
<div class="col-md-12">

  <div class="billboard-points-col">

      <div class="billboard-inner">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
                  
          <h1 class="h3 mb-0 text-gray-800">Education Billboard</h1>
          
      </div>

      
  <div class="col-md-12">

    {!! $billboard !!}

  </div>
</div>
</div>
</div>
</div>

@endif

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

    

</script>

@endsection