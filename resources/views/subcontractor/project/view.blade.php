@extends('admin.layouts.app')
@section('title', 'Project: ' . $projectData->job_number)
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
@endsection
@section('content')
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<div class="col-lg-12">
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800"><span class="project_name_view_heading">Project: {{ $projectData->job_number }} <em></em>

        <?php 

          if( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->where('payroll_id', $projectData->id)->exists() )
          {
            $fl = "<a href='".route('admin.payrolls.fav', [$projectData->id, Auth::user()->id])."'><i class='fa fa-star'></i></a>"; 
          }
          else
          {
            $fl = "<a href='".route('admin.payrolls.fav', [$projectData->id, Auth::user()->id])."'><i class='fa fa-star-o' aria-hidden='true'></i></a>";
          }

        ?>

        {!! $fl !!}

      </span></h1>
      <a href="{{ route( 'subcontractor.projects' ) }}" class="back-link">< Back to Projects</a>
  </div>
</div>
<div class="row justify-content-center trainer_view_wrapper">
    <div class="col-lg-12 mb-4">
       <div class="col-lg-12">

        <div class="next-prev-navigation">

                <div class="prev">

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('subcontractor.project.view', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('subcontractor.project.view', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls"  style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Information</h6>
            </div>
  
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>
                                
                                <i class="far fa-user"></i> Job Name
                            
                            </label>
                            <p class="businesses_view_text">{{ $projectData->job_name }}</p>
                        </div>
                        <div class="form-group">
                            <label>
                                
                                <i class="far fa-user"></i> General Contractor
                            
                            </label>
                            <p class="businesses_view_text">{{ $projectData->general_contractor_name }}</p>
                        </div>
                        <div class="form-group">
                            <label>
                                
                                <i class="fas fa-map-marker-alt"></i> Job Address
                            
                            </label>
                            <p class="businesses_view_text">{{ $projectData->job_address }}</p>
                        </div>
                        <div class="form-group">

                            <label>
                                
                                <i class="fas fa-eye"></i> Status
                            
                            </label>

                            <p class="businesses_view_text">{{ ucfirst( str_replace( '_', ' ', $projectData->status ) ) }}</p>

                        </div>
                    </div>
                    {{--<div class="col-md-3" style="text-align : center;align-items: center;display: flex;flex-direction: column;">
                        <div class="bar_code_holder">
                       <!-- Show The Qrcode -->
                        @if( !empty( $projectData->qrcode ) || $projectData->qrcode != null )
                            <img src="data:image/png;base64, {{ $projectData->qrcode  }} ">
                        @endif
                        <!-- /Show The Qrcode -->
                          <form action="{{ route('admin.project.print.qr', 'Job Number: '.$projectData->job_number) }}" target="_blank" method="post">
                            {{ csrf_field() }}
                             <input type="hidden" value="{{ $projectData->qrcode }}" name="imageURL">
                            <input type="hidden" value="{{ $projectData->job_name }}" name="projectName">
                            <input type="hidden" value="{{ $projectData->job_number }}" name="projectNumber">
                            <input type="hidden" value="{{ $projectData->job_address_json }}" name="projectAddress">
                            <input style="background: none; border: 0px; font-weight: bold;" type="submit" value="Print">
                        </form>
                        <!-- Print QR -->
             </div>
                       
                        <!-- Print QR -->
                        
                   
                    </div>
                    
                </div>--}}
    
            </div>
        </div>
      </div>
    </div>
    <!-------------------------------------------- Seperator -------------------------------------------->
             <style>
          .project_view_table th{
        position:relative;
      }
      .project_view_table th.sorting_asc, th.sorting_desc, .project_view_table th.sorting_asc, th.sorting_desc, th.sorting, th.sorting{
      cursor:pointer;  
      }
      
          .project_view_table th.sorting_asc:after, th.sorting_desc:after, .project_view_table th.sorting_asc:before, th.sorting_desc:before, th.sorting:before, th.sorting:after{
            position: absolute;
          bottom: 0.9em;
          display: block;
          opacity: 0.3;  
              
          }
      
          th.sorting_asc:before{
          right: 1em;
            content: "\2191"; 
 cursor:pointer;      
          }
      th.sorting:before{
      right: 1em;
            content: "\2191";  
      cursor:pointer;
      }
          th.sorting_asc:after{
                    right: 0.5em;
                    content: "\2193"; 
 cursor:pointer;          
          }
      th.sorting:after{
      right: 0.5em;
                    content: "\2193";
           cursor:pointer;          
      }
      th.sorting_desc:before{
               right: 1em;
             content: "\2191";
        cursor:pointer;
      }
      
      th.sorting_desc:after{
          right: 0.5em;
               content: "\2193"; 
 cursor:pointer;         
      }
      .project_view_table .punch-in-time, .project_view_table .punch-out-time{
       display:table;
             margin:0 auto;
            width:100%;
            text-align:center;      
      }
      .worker_page_wrapper .dataTables_length {
    position: absolute;
    right: 131px;
}
div#datatable_info {
    float: left;
    margin-top: 7px;
}
      
        </style>
    <div class="col-lg-12">
        <h6 class="logs_heading">Logs</h6>
        <div class="card shadow mb-4 ">
            <div class="card-body worker_page_wrapper">
                  <div class="custom_table_filter">
                 
                  <!-- Date filter -->
                  <input id="date-filter" type="text" value="{{ @$_REQUEST['date'] }}" name="started_at" required="" class="form-control datepicker_one" autocomplete="off">
                  <!-- Date filter -->
                  <!-- Reset filter -->
                  @if(
                  (
                      isset( $_GET['date'] )
                  )
                  )
                  <a href="{{ route('subcontractor.project.view', $projectData->id) }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>
                  @endif
                  <!-- Reset filter -->
                </div>
            
                <table class="table table-bordered table-hover project_view_table" id="datatable">
    
                    <thead>
    
                        <tr>
                            <th scope="col">Worker Name</th>
                            <th scope="col">Company</th>
                            <th scope="col">Title</th>
                            <th scope="col">Date</th>
        
                            <th scope="col" style="text-align:center">In</th>
                            <th scope="col" style="text-align:center">Out</th>
                            <th scope="col" style="text-align:center;">Total Hrs</th>
        
                        </tr>
    
                    </thead>
                    <tbody></tbody>
                </table>
                {!! getIconGuides() !!}
            </div>
        </div>
    </div>
    <!-------------------------------------------- Seperator -------------------------------------------->
</div>
@endsection
@section('scripts')
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>
<script src="{{ asset('js/daterangepicker.js') }}"></script>    
<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

  <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script>
    /*
     * @Function Name
     *
     *
     */
    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');
      $.fn.dataTable.moment('h:mm a');

        $('#datatable').DataTable({
          "order": [[ 3, 'desc' ], [ 4, 'desc' ]],
dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csvHtml5',
          text: 'Export',
          exportOptions: {
             modifier : {
             page : 'all', // 'all', 'current'
         },
            columns: [0,1, 2,3,4,5,6]
          }
                }
      ],
            "bLengthChange": true,
            language: {
            sEmptyTable: "No data available.",
            sLengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Search...",
             sProcessing: "<i class='fa fa-circle-o-notch fa-spin'></i>",
             },
            processing: true,
            serverSide: false,
            "ajax": {
              "url": "{{ route('subcontractor.datatable.projects.logs.get') }}",
              "data": function ( d ) {
               d.projectId = "{{ $projectData->id }}",
               d.log_date = "{{ @$_REQUEST['date'] }}"
              
            }
            },
            columns: [
                { data: "worker_name", name: "worker_name", searchable: true, orderable: true },
                { data: "subcontractor", name: "subcontractor", searchable: false, orderable: true },
                { data: "title", name: "title", searchable: false, orderable: false },
                { data: "date", name: "date", searchable: false, orderable: true },
                
                { data: "punch_in", name: "punch_in", searchable: true, orderable: true },
                
                { data: "punch_out", name: "punch_out", searchable: false, orderable: true },
                { data: "total_hours", name: "total_hours", searchable: false, orderable: true },
            ],
            
        });
    }
    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {
        initialize_database();
        /*
         * @Function Name
         *
         *
         */
        $( ".datepicker_one" ).daterangepicker();
        /*
        * @Date Filter
        *
        *
        */
        $('#date-filter').change(function()
        {
             var dateVal = $( this ).val();
             var url = window.location.href;
             var a = url.indexOf("?");
             var b =  url.substring(a);
             var c = url.replace(b,"");
             url = c;
             url += '?date='+dateVal;
             window.location.href = url;
        })
    });
</script>
@endsection