@extends('admin.layouts.app')

@section('title', 'New Project')

@section('styles')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">New Project</h1>

    <a href="{{ route('subcontractor.projects') }}" class="back-link">< Back to Projects</a>
    
</div>

<form action="{{ route('subcontractor.projects.create') }}" id="create-project" method="POST" enctype="multipart/form-data">
            
    @csrf

    <div class="row">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Project Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Job Number<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="job_number" placeholder="Job Number" value="{{ old('job_number') }}" maxlength="50" required>

                                <span id="error-job-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Job Name</label>

                                <input type="text" class="form-control" name="job_name" placeholder="Job Name" value="{{ old('job_name') }}" maxlength="50">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12" style="display: none;">

                            <div class="form-group">

                                <input type="hidden" value="{{ $subContId }}" name="sub_contractor">

                                <span id="error-general-contractor" class="error-message text-danger"></span>

                            </div>

                        </div>

                         <div class="col-md-12">

                            <div class="form-group">

                                <label>General Contractor Name<span class="text-danger">*</span></label>

                                <input type="text" value="{{ old('general_contractor_name') }}" placeholder="John Watson" class="form-control" name="general_contractor_name" required="">

                                <span id="error-general-contractor-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <!-- Address -->

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Street 1<span class="text-danger">*</span></label>

                                <textarea class="form-control" name="address[street_1]" placeholder="Address 1" required></textarea>

                                <span id="error-job-address" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Street 2<span class="text-danger">*</span></label>

                                <textarea class="form-control" name="address[street_2]" placeholder="Address 1" required></textarea>

                                <span id="error-job-address" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>City<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="address[city]" placeholder="City"  maxlength="50" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>State<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="address[state]" placeholder="State"  maxlength="50" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Zipcode<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="address[zipcode]" placeholder="Zipcode"  maxlength="50" required>

                            </div>

                        </div>

                        <!-- Address -->

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Create Project</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">

$(document).ready(function() {

    $('#create-project').on('submit', function(event) {

  

    });

});

</script>

@endsection