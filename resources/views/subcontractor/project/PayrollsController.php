<?php

namespace App\Http\Controllers\Subcontractor;

use File;
use Session;
use App\User;
use App\Payroll;
use Validator;
use Carbon\Carbon;
use App\CountryCode;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DataTables;
use DB;
use QrCode;
use Auth;

class PayrollsController extends Controller
{
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request )
	{

	    return view('subcontractor.payrolls.index');
	    
	}

	/*
	 * @Datatable get
	 *
	 *
	 */
	public function getPayrolls( Request $request )
	{

		$subContId = Auth::user()->id;

	    $payrolls = Payroll::where( 'author', $subContId )->get();

	    return Datatables::of( $payrolls )
	    ->addColumn( 'title', function( $payrolls )
	    {

	    	$viewLink = route('subcontractor.payroll.view', [ 'id' => $payrolls->id ]);

	       	return "<a class='index-link' href='$viewLink'>".ucwords( $payrolls->title )."</a>";

	    })
	    ->addColumn( 'payroll_file', function( $payrolls )
	    {
	      
	        return $payrolls->payroll_file_source;

	    })
	    ->addColumn( 'uploaded_on', function( $payrolls )
	    {

	        return $payrolls->uploaded_on;
	        
	    })
	    ->addColumn( 'action', function( $payrolls )
	    {

	        $viewLink = route('subcontractor.payroll.view', [ 'id' => $payrolls->id ]);

	        $editLink = route('subcontractor.payroll.edit', [ 'id' => $payrolls->id ]);

	        //$deleteLink = route('subcontractor.payroll.delete', [ 'id' => $payrolls->id ]);

	        $html = '<div class="drill-type">

	                     <div class="company_edit">
	                        <div class="dropdown">

	                           <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn dropdown-toggle"> <a><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
	                           </button>

	                           <div class="dropdown-menu">

	                              <div>
	                                 <a href="' . $viewLink . '" class="btn btn-sm btn-info btn-circle" title="View">
	
	                                      View
	
	                                  </a>
	                              </div> 

	                              <div>
	                                 <a href="' . $editLink . '" class="btn btn-sm btn-primary btn-circle" title="Edit">
	                                   Edit
	                                </a>
	                              </div>

	                               
	                              
	                           </div>
	                        </div>
	                     </div>
	                     <div class="clearfix"></div>
	                  </div>';

	        return $html;

	    })
	    ->rawColumns( ['title', 'payroll_file', 'uploaded_on', 'action'] )
	    ->make( true );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function downloadFiles( $file_source, $author )
	{

		$payrollFileUrl = base_path( '/public/uploads/images/payroll/'.$author );

	    $payrollFileUrl = $payrollFileUrl.'/'.$file_source;


   
        $filepath = urldecode($payrollFileUrl);;

        // Process download
        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
            die();
        } else {
            http_response_code(404);
	        die();
        }
    

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{

	    $subContId = Auth::user()->id;

	    $data = compact(

	        'subContId'

	    );
	    
	    return view('subcontractor.payrolls.create', $data);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$subContId = Auth::user()->id;

	    $validator = Validator::make($request->all(), [

	        'title' => 'required',
	        
	        'payroll_file' => 'required',

	    ]);

	    if($validator->fails()) {

	        $request->flash();
	    
	        if($validator->errors()->first('title')) {
	    
	            Session::flash('error', 'Please enter payroll title.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('description')) {
	    
	            Session::flash('error', 'Please enter payroll description.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('payroll_file')) {
	    
	            Session::flash('error', 'Please upload valid payroll file (CSV or PDF).');
	    
	            return redirect()->back();
	    
	        }
	    
	    }

	    /*-------------------------------------------- Seperator --------------------------------------------*/

	    $title = $request->input('title');

	    $description = $request->input('description');

	    /*-------------------------------------------- Seperator --------------------------------------------*/

	    /*-------------------------------------------- Payroll file upload --------------------------------------------*/

	    $destination = public_path('uploads/images/payroll/'. $subContId);

        if(!File::isDirectory($destination)) {

            mkdir($destination, 0777, true);

            chmod($destination, 0777);

        }

        $image = $request->file('payroll_file');

		$image_name = 'payroll-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

        $image->move($destination, $image_name);

	    /*-------------------------------------------- Payroll file upload --------------------------------------------*/


	    $payroll = Payroll::create([

	        'title' => $title,

	        'description' => $description,

	        'payroll_file' => $image_name,

	        'author' => $subContId
	        
	    ]);

	    Session::flash('success', 'New payroll created successfully.');

	    return redirect()->route('subcontractor.payrolls');

	}

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $payrollData = Payroll::where('id', $id)->firstOrFail();

        return view( 'subcontractor.payrolls.view', compact( 'payrollData' ) );

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    
	    $project = Payroll::where('id', $id)->firstOrFail();

	    $project->delete();

	    Session::flash('success', 'Payroll deleted successfully.');

	    return redirect()->back();

	}

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
     
        $payrollData = Payroll::where('id', $id)->firstOrFail();

        $subContId = Auth::user()->id;

        return view( 'subcontractor.payrolls.edit', compact( 'payrollData', 'subContId' ) );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    	$subContId = Auth::user()->id;
        
       	$validator = Validator::make($request->all(), [

	        'title' => 'required',

	    ]);

       	if($validator->fails()) {

           $request->flash();
       
           if($validator->errors()->first('title')) {
	    
	            Session::flash('error', 'Please enter payroll title.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('description')) {
	    
	            Session::flash('error', 'Please enter payroll description.');
	    
	            return redirect()->back();
	    
	        }

	        if($validator->errors()->first('payroll_file')) {
	    
	            Session::flash('error', 'Please upload valid payroll file (CSV or PDF).');
	    
	            return redirect()->back();
	    
	        }
       
       	}

       	/*-------------------------------------------- Seperator --------------------------------------------*/

	    $title = $request->input('title');

	    $description = $request->input('description');

	    /*-------------------------------------------- Seperator --------------------------------------------*/

       	$payrollData = array(

       	    'title' => $title,

       	    'description' => $description,

       	    'author' => $subContId

       	);

       	/*-------------------------------------------- Image validation --------------------------------------------*/

        if($request->hasFile('payroll_file')) {

            $format_validator = Validator::make($request->all(), [

                'image' => 'mimes:csv,pdf'
    
            ]);
    
            if($format_validator->fails()) {
    
                Session::flash('error', 'Please select image with format CSV, PDF');
    
                return redirect()->back();
    
            }

            $destination = public_path('uploads/images/payroll/'. $subContId);

            if(!File::isDirectory($destination)) {

                mkdir($destination, 0777, true);

                chmod($destination, 0777);

            }

            $image = $request->file('payroll_file');

			$image_name = 'payroll-' . time() . '-' . Str::random(15) . '.' . $image->getClientOriginalExtension();

            $image->move($destination, $image_name);

            $payrollData['payroll_file'] = $image_name;

        }

       	/*-------------------------------------------- Image validation --------------------------------------------*/


       $payrollObject = Payroll::find( $id );

       $payrollObject->update( $payrollData );

       Session::flash('success', 'Payroll updated successfully.');

       return redirect()->route('subcontractor.payrolls');

    }

}
