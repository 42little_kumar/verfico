@extends('admin.layouts.app')

@section('title', 'Payroll: ' . $payrollData->title)


@section('styles')
<link href="{{ asset('css/OverlayScrollbars.min.css') }}" rel="stylesheet"> 
<style>

tbody tr:last-child {
   font-weight:bold; color:black;
   background: white !important;
   color: inherit !important;
}

.tox-notifications-container {
    display: none !important;
}

.approve-button {
   background: #509c50;
   color: white;
   padding: 9px 10px;
   position: absolute;
   z-index: 99;
   right: 35px;
   margin-top: 8px;
   color: white !important;
   border-radius: 5px;
   min-width: 150px;
   text-align: center;
}

.payroll-excel-report-table-wrapper  tr td{
background:#fff;	
}

.payroll-excel-report-table-wrapper .yellow-row td
{
  background: yellow;
}

td.sheet-error {
   background: red !important;
   color: white !important;
}

.approve-button.disabled {
   background: #cac9d4 !important;
   cursor: not-allowed;
   color: white !important;
}

.approved {
   background: #cac9d4 !important;
   cursor: not-allowed;
   color: white !important;
   padding: 9px 10px;
   position: absolute;
   z-index: 99;
   right: 36px;
   margin-top: 8px;
   border-radius: 5px;
   min-width: 150px;
   text-align: center;
}

.payroll-excel-report-table-wrapper tr.logs-not-equal {
   background: #f17575;
   color: white;
}

.payroll-excel-report-table-wrapper tr td.less-hours, td.less-gross, .payroll-excel-report-table-wrapper tr td.less-rate {
   background: red !important;
   color: white;
}

.fa-times-circle {
   color: red;
}

ul {
   list-style: none;
   padding: 0px;
   margin: 0px;
   font-size: 10px;
   color: red;
}
ul li{
font-size:13px;	
}
a.report-alert {
   position: absolute;
   z-index: 999;
   right: 190px;
   margin-top: 16px;
}


			label{ text-align: left }
			button[type="submit"] {
				float: right;
				background: #404094;
				border: 0px;
				border-radius: 4px;
				width: 100%;
				color: white;
			}
			
			
/*****/
.payroll-excel-report-table-wrapper {
  position: relative;
  width:100%;
  z-index: 1;
  margin: auto;
  overflow: auto;
}

.payroll-excel-report-table-wrapper table {
  width: 100%;
  margin: auto;
  border-collapse: separate;
  border-spacing: 0;
}



.payroll-excel-report-table-wrapper table th:nth-child(9), .payroll-excel-report-table-wrapper table th:nth-child(10),
.payroll-excel-report-table-wrapper table th:nth-child(11), .payroll-excel-report-table-wrapper table th:nth-child(12),
.payroll-excel-report-table-wrapper table th:nth-child(13),
.payroll-excel-report-table-wrapper table td:nth-child(9), .payroll-excel-report-table-wrapper table td:nth-child(10), .payroll-excel-report-table-wrapper table td:nth-child(11),
.payroll-excel-report-table-wrapper table td:nth-child(12),.payroll-excel-report-table-wrapper table td:nth-child(13){
display: none !important;
}

.payroll-excel-report-data-show table th:nth-child(9), .payroll-excel-report-data-show table th:nth-child(10),
.payroll-excel-report-data-show table th:nth-child(11), .payroll-excel-report-data-show table th:nth-child(12),
.payroll-excel-report-data-show table th:nth-child(13),
.payroll-excel-report-data-show table td:nth-child(9), .payroll-excel-report-data-show table td:nth-child(10), .payroll-excel-report-data-show table td:nth-child(11),
.payroll-excel-report-data-show table td:nth-child(12), .payroll-excel-report-data-show table td:nth-child(13){
 display:table-cell !important;;
}
.payroll-excel-report-data-show table th:nth-child(13){
    width: 127px !important;	
}

.payroll-excel-report-table-wrapper table {
 min-width: 1930px !important;
}

.payroll-excel-report-table-wrapper.payroll-excel-report-data-show table{
min-width:2060px !important;	
}

.table-wrap {
  position: relative;
}

.payroll-excel-report-table-wrapper th,
.payroll-excel-report-table-wrapper td {

}

.payroll-excel-report-table-wrapper thead th {
  position: -webkit-sticky;
  position: sticky;
  top: 0;
}
/* safari and ios need the tfoot itself to be position:sticky also */


.payroll-excel-report-table-wrapper td:first-child, .payroll-excel-report-table-wrapper td:nth-child(2){
  position: -webkit-sticky;
  position: sticky;
  left: 0;
  z-index: 2;
  width:150px;
}
.payroll-excel-report-table-wrapper th:first-child, .payroll-excel-report-table-wrapper th:nth-child(2){
  position: -webkit-sticky;
  position: sticky;
  left: 0;
  z-index: 5;
  width:150px;	
}

.payroll-excel-report-table-wrapper td:nth-child(2), .payroll-excel-report-table-wrapper th:nth-child(2){
 left:149px;
 border-right:1.5px solid #ddd;
}


.show-table-data {
    z-index: 555;
    right: 33px;
    top: 9px;
    transform: rotate(90deg);
    font-size: 17px;
    color: #000;
    cursor: pointer;
	position:absolute;
}
		
</style>
 
@endsection



@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
<h1 class="h3 mb-0 text-gray-800"><span class="project_name_view_heading">Payroll: {{ $payrollData->title }} @if( $payrollData->status == 0 )@endif</span></h1>
    <a href="{{ route( 'subcontractor.payrolls' ) }}" class="back-link">< Back to Payrolls</a>
</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

        <div class="next-prev-navigation">

                <div class="prev">

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('subcontractor.payroll.view', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&subcontractor=<?php echo @$_GET['subcontractor']; ?>&date=<?php echo @$_GET['date']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('subcontractor.payroll.view', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&subcontractor=<?php echo @$_GET['subcontractor']; ?>&date=<?php echo @$_GET['date']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls"  style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Information</h6>
            </div>
  
            <div class="card-body">

                <div class="row">

                    <div class="col-lg-12">
                        <div class="row">
							<div class="form-group col-md-4">
								<label>
									<i class="far fa-user"></i> Week of
								</label>
								<p class="businesses_view_text"> {{ $payrollData->title }} </p>
							</div>
							
                        <div class="form-group col-md-4">

                            <label>
                                
                                <i class="far fa-envelope"></i> Uploaded On
                            
                            </label>

                            <p class="businesses_view_text"> {{ $payrollData->uploaded_on }} </p>

                        </div>

                        <div class="form-group col-md-4">

                            <p class="businesses_view_text"> {!! $payrollData->payroll_file_source !!} </p>

                        </div>

                        <div class="form-group col-md-4">

                            <label>
                                
                                <i class="far fa-user"></i> Payroll Status
                            
                            </label>

                            @if( $payrollData->status )
                            <p class="businesses_view_text"> Approved </p>
                            @else
                            <p class="businesses_view_text"> Pending </p>
                            @endif

                        </div>
						
						  <div class="form-group col-md-8">

                            <label>
                                
                                <i class="far fa-user"></i> Description
                            
                            </label>

                            <p class="businesses_view_text"> {{ $payrollData->description }} </p>

                        </div>
						
	                    </div>
                    </div>
                    
                </div>
    
            </div>

        </div>

        <?php

        $workerExistIds = [];

        foreach( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->get() as $get )
        {

            $rowData = json_decode( $get->file_data );

            $userId = $payrollData->getSsnStatusIds( $rowData[1], $payrollData->author );

            if( $userId )
            {

                array_push($workerExistIds, $userId);

            }   

        }

        $niu = $payrollData->getNotInUsers( $workerExistIds, $payrollData->author, $payrollData->start_date, $payrollData->end_date );

        ?>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Excluded Workers</h6>
            </div>
  
            <div class="card-body">

                <div class="row">

                    <div class="col-lg-12">
                     
                     @if( count($niu) > 0)
                     
                     <ul id="custom-scroll-1" style="max-height:250px;">

                        @foreach( $niu as $u )

                        <li><a href="{{ route('admin.users.view', $u->id) }}">{{ $u->first_name }} {{ $u->last_name }} ({{ $u->email }})</a></li>

                        <hr>

                        @endforeach

                      @else

                      <div style="margin: 0 auto; width: 220px;">No Excluded Workers Found!</div>

                      @endif

                    </ul>
                    
                </div>
    
            </div>

        </div>
		  </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Excel File Data</h6>
            </div>
  
            <div class="card-body">

                <div class="row">

                    <div class="col-md-12">

                        <div id="custom-scroll" class="payroll-excel-report-table-wrapper">
						<table class="table">

                           <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>SSN</th>
                                    <th>Pay Start</th>
                                    <th>Pay End</th>
                                    <th>Chk Date</th>
                                    <th>Chk #</th>
                                    <th>Hours</th>
                                    <th>Gross <div class="show-table-data" id="show-table-data"><i class="fa fa-sort" aria-hidden="true"></i></div></th>
                                    <th>Fed W/H</th>
                                    <th>Soc Sec</th>
                                    <th>Med Care</th>
                                    <th>Med Care Add</th>
                                    <th>State W/H <div class="show-table-data" id="hide-table-data"><i class="fa fa-sort" aria-hidden="true"></i></div></th>
                                    <th>Net Pay</th>
                                    <th>Avg. Sal./Hour (Overtime)</th>
                                    <th>Errors</th>
                                </tr>
                            </thead>

                            

                                

                                    @if( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->count() > 1 )

                                    <tbody>

                                        @foreach( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->get() as $get )

                                        @php

                                        $wrongName = false;

                                        $rowData = json_decode( $get->file_data );

                                        $errors =  [];

                                        $wrongSsn = $payrollData->getSsnStatus( $rowData[1], $payrollData->author );

                                        @endphp

                                        @if( empty( $rowData[1] ) )

                                        @php 

                                        $wrongSsn = true;

                                        @endphp

                                        @endif

                                        @php
                                        $checkUser = $payrollData->getWorkerName( $rowData[0], $rowData[1], $payrollData->author );
                                        @endphp
                                        @if( $checkUser == 1  && !$wrongSsn)

                                        @php 

                                        $wrongName = true;

                                        array_push( $errors, "Worker not found for this subcontractor or Wrong SSN id." );

                                        @endphp

                                        @elseif($checkUser == 2 && !$wrongSsn)
                                         @php 

                                        $wrongName = true;

                                        array_push( $errors, "Worker name is incorrect." );

                                        @endphp

                                        @endif

                                        <tr  @if( $rowData[6] <= 0 ) class="yellow-row" @endif>
                                        
                                        
                                        <td @if( ( empty(  $rowData[0] ) || $wrongName ) && !$wrongSsn  ) class="sheet-error" @endif>{{ $rowData[0] }}</td>
                                        <td style='text-align: center;' @if( empty( $rowData[1] ) || $wrongSsn ) class="sheet-error" @endif>@if( !empty($rowData[1]) ){{ makeSsnFormat( $rowData[1], $payrollData->author ) }}@else - @endif</td>
                                        <td @if( empty( $rowData[2] )  && !$wrongSsn ) class="sheet-error" @endif>@if(!empty($rowData[2])){{ @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rowData[2]))->format('M d Y') }}@endif</td>
                                        <td @if( empty( $rowData[3] )  && !$wrongSsn ) class="sheet-error" @endif>@if(!empty($rowData[3])){{ @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rowData[3]))->format('M d Y') }}@endif</td>
                                        <td @if( empty( $rowData[4] )  && !$wrongSsn ) class="sheet-error" @endif>@if(!empty($rowData[4])){{ @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rowData[4]))->format('M d Y') }}@endif</td>
                                        <td @if( empty( $rowData[5] ) ) class="sheet-error" @endif>{{ $rowData[5] }}</td>

                                        

                                        @if( (!$payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) || $payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) != 'equal') && !$wrongSsn )

                                            @php

                                            array_push( $errors, "Total hours doesn't match." )

                                            @endphp

                                            <td class="less-hours sheet-error" title="Logged hours {{ $payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) }}">{{ $rowData[6] }}</td>

                                        @else

                                            <td @if( empty( $rowData[6] ) && $rowData[6] != 0 ) class="sheet-error" @endif>{{ $rowData[6] }}</td>

                                        @endif

                                        


                                        <td @if( empty( $rowData[7] ) && $rowData[7] != 0 ) class="sheet-error" @endif>{{ $rowData[7] }}</td>
                                        <td>{{ empty( $rowData[8] ) ? 0 : $rowData[8] }}</td>
                                        <td @if( empty( $rowData[9] ) && $rowData[9] != 0 ) class="sheet-error" @endif>{{ $rowData[9] }}</td>
                                        <td @if( empty( $rowData[10] ) && $rowData[10] != 0 ) class="sheet-error" @endif>{{ $rowData[10] }}</td>
                                        <td>{{ empty( $rowData[11] ) ? 0 : $rowData[11] }}</td>
                                        <td @if( empty( $rowData[12] ) && $rowData[12] != 0 ) class="sheet-error" @endif>{{ $rowData[12] }}</td>

                                        @if( $payrollData->checkGross( $rowData[7], $rowData[13] ) )

                                            @php

                                            array_push( $errors, 'Gross pay is less then Net pay.' )

                                            @endphp

                                            <td class="less-gross sheet-error" title="Gross is less than net pay.">
                                                {{$rowData[13]}}
                                            </td>

                                        @else

                                            <td @if( empty( $rowData[13] ) && $rowData[13] != 0 ) class="sheet-error" @endif>
                                                {{$rowData[13]}}
                                            </td>

                                        @endif

                                        @if( ( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ) < 15 ) && !$wrongSsn && round( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ), 2) != 0 )

                                            @php

                                            array_push( $errors, 'Hourly rate is less then $15.' )

                                            @endphp

                                            <td class="less-rate sheet-error" title="Hourly rate is less then $15.">
                                            ${{ round( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ), 2) }}
                                                <span>
                                                    @if( $payrollData->ifOvertime( $rowData[6] ) && !$wrongSsn )
                                                    <img style='width: 20px;' title='Overtime' class='log-icon' src="{{ asset('public/images/time.png') }}">
                                                    @endif
                                                </span>
                                            </td>

                                        @else

                                            <td>
                                            ${{ round( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ), 2) }}
                                                <span>
                                                    @if( $payrollData->ifOvertime( $rowData[6] ) && !$wrongSsn )
                                                    <img style='width: 20px;' title='Overtime' class='log-icon' src="{{ asset('public/images/time.png') }}">
                                                    @endif
                                                </span>
                                            </td>

                                        @endif

                                        <td>

                                            @if( !empty( $errors )  || $wrongSsn )

                                                <span class="fa fa-times-circle"></span>

                                                <ul>

                                                @if( $wrongSsn )
                                                
                                                    @php 

                                                        $errors = [];

                                                        $errors = ['Worker not found for this subcontractor or Wrong SSN id.'];

                                                    @endphp

                                                @endif

                                                    @foreach( $errors as $error )
                                                        
                                                        <li>{{ $error }}</li>

                                                    @endforeach

                                                </ul>

                                            @else

                                                <span class="fa fa-check-circle"></span>

                                            @endif

                                        </td>

                                        </tr>
                                        

                                        @endforeach
                                    
                                    </tbody>

                                    @else

                                    <tfoot>

                                        <tr>

                                        <td colspan="14" class="wrong-format" style="color: red;">No data or wrong format uploaded. Please use well formatted XLS OR XLSX file.</td>

                                        </tr>

                                    </tfoot>

                                    @endif

                        </table>
                        </div>

                    </div>
                    
                </div>
    
            </div>

        </div>

  

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.overlayScrollbars.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/OverlayScrollbars.min.js') }}"></script>

<script>

$(document).ready(function()
{
	$('#show-table-data').click(function(){
	 $('.payroll-excel-report-table-wrapper').addClass('payroll-excel-report-data-show');
     $('#show-table-data').hide();	 
	}); 
	
	$('#hide-table-data').click(function(){
	 $('.payroll-excel-report-table-wrapper').removeClass('payroll-excel-report-data-show');
     $('#show-table-data').show();	 
	});
	
    $('tbody tr:last-child td').removeClass('sheet-error');
    $('tbody tr:last-child td').removeClass('less-hours');
    $('tbody tr:last-child td').removeClass('less-rate');
    $('tbody tr:last-child td:last-child').text('');
    $('tbody tr:last-child td:first-child').next().text('');
    $('tbody tr:last-child td:last-child').prev().text('');

   
})
$('#custom-scroll').overlayScrollbars({
   
	className       : "os-theme-dark",
	scrollbars : {
		clickScrolling : true
	}
		
});
$('#custom-scroll-1').overlayScrollbars({
   
	className       : "os-theme-dark",
	scrollbars : {
		clickScrolling : true
	}
		
});

</script>

@endsection