@extends('admin.layouts.app')

@section('title', 'Payrolls')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }

        .week-ending {
            width: 260px !important;
        }

        .custom_table_filter {
            left: 10px !important;
        }

        .green
        {
            color: green;
        }

        .red
        {
            color: red;
        }
        
		/*table thead th:nth-child(2){
		width: 157px !important;	
		}
		table thead th:nth-child(4){
		width: 128px !important;	
		}
		table thead th:nth-child(5){
		width: 150px !important;	
		}
		
		table thead th:nth-child(6){
		width: 157px !important;	
		}*/
		.payroll_custom_table_wrap td:first-child {
         width:100px !important;
    
     }
    </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Payrolls</h1>

    @if( auth()->user()->hasRole('Subcontractor') )

    <a href="{{ route('subcontractor.payroll.create') }}" class="float-right btn-create">

        Add

    </a>

    @endif
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            <div class="custom_table_filter">
           

            <!-- Date filter -->

            <input id="date-filter" type="text" placeholder="Select Pay Period End Date" value="{{ @$_REQUEST['date'] }}" name="started_at" required="" class="form-control datepicker_one week-ending" autocomplete="off">

            <!-- Date filter -->

            <!-- Reset filter -->

            @if(
            (
                isset( $_GET['date'] )
            )
            )

            <a href="{{ route('subcontractor.payrolls') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

            @endif

            <!-- Reset filter -->
          </div>

            <div class="card-body">
            
                <table class="table table-bordered table-hover payroll_custom_table_wrap" id="datatable">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Week End</th>

                            <th scope="col">Week Start</th>
        
                            <th scope="col">Uploaded On</th>

                            <th scope="col">Status</th>

                            <th scope="col">Payroll Document</th>

                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this payroll?')) {
        
            event.preventDefault();

        }

    });

    /* ----------------------------------- Next/Prev ----------------------------------- */

    serialize = function(obj) {
      var str = [];
      for (var p in obj)
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      return str.join("&");
    }
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

    $(document).on("click","a.index-link", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&date="+$.urlParam('date');
        
        event.preventDefault();
    });
    $(document).on("click","a[title='Edit']", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&date="+$.urlParam('date');
        
        event.preventDefault();
    });
    $(document).on("click","a[title='View']", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&date="+$.urlParam('date');
        
        event.preventDefault();
    });


    var table;

    /* ----------------------------------- Next/Prev ----------------------------------- */

    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_sub_payrolls_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

            stateSave: true,

            "order": [[ 1, 'desc' ] ],

            "bLengthChange": true,

            pageLength: pageLength,

            "bFilter": false,

            "bInfo": false,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No payrolls found!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: true,

            "ajax": {

              "url": "{{ route('subcontractor.datatable.payroll.get') }}",

              "data": function ( d ) {

               d.payroll_date = "{{ @$_REQUEST['date'] }}"
              
            }

            },

            columns: [

                { data: "end_date", name: "end_date", searchable: false, orderable: true },

                { data: "start_date", name: "start_date", searchable: false, orderable: true },

                { data: "uploaded_on", name: "uploaded_on", searchable: false, orderable: true },

                { data: "status", name: "status", searchable: false, orderable: true },
                
                { data: "payroll_file", name: "payroll_file", searchable: false, orderable: false },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_sub_payrolls_pageLength", len);

        });

        /*
         * @Function Name
         *
         *
         */
        $('.week-ending').datepicker({

                dateFormat: 'MM dd, yy' ,

                   

                    onSelect: function (dateText, inst) {

                        var newdate = new Date(dateText);

                        const monthNames = ["January", "February", "March", "April", "May", "June",
                          "July", "August", "September", "October", "November", "December"
                        ];

                        var endDate = newdate.setDate(newdate.getDate() - 6);

                        var dd = newdate.getDate();

                        var mm = monthNames[newdate.getMonth()];

                        var y = newdate.getFullYear();

                        endDate = mm+' '+dd+', '+y;

                        $('.start-date').val(endDate);

                        $('.end-date').val(dateText);

                        $('.week-ending').val( endDate+' to '+dateText );

                        /* ***** */

                        var dateVal = endDate+' to '+dateText;

                        var url = window.location.href;

                        var a = url.indexOf("?");

                        var b =  url.substring(a);

                        var c = url.replace(b,"");

                        url = c;

                        url += '?date='+dateVal;

                        window.location.href = url;

                        /* ***** */
                         
                        }

            })

    });

</script>

@endsection