@extends('admin.layouts.app')

@section('title', 'Landing Page Content')

@section('content')

@section('styles')

    <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>

	<link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">

    <style type="text/css">
    	
    	.tox-notifications-container
    	{
    		display: none !important;
    	}

      .tox.tox-tinymce {
    height: 500px !important;
}

    </style>

@endsection


<div class="d-sm-flex align-items-center justify-content-between mb-4">

    <h1 class="h3 mb-0 text-gray-800">Landing Page Content</h1>

</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

        <div class="card-body shadow mb-4">
  				
  				<div>
  					
  					<form action='{{ route("admin.resource.content.save") }}' method='post' name='notification-form' class='notification-form'>

              @csrf

  						<div class='row'>

  							<!-- ----------- -->

  							<div class="col-md-12">

							  <div class="form-group">

  							       

  							    </div>

  							    <div class="form-group">

  							        <label>Content</label>

  							        <input type="hidden" name="name" value="billboard">

  							        <textarea id="pagecontent" class="form-control pagecontent" maxlength="200" name="pagecontent">{{ @$data->pagecontent }}</textarea>

  							    </div>

  							</div>

							  
  							
  							<!-- ----------- -->

  							<!-- ----------- -->

  							<div class="col-md-12">


  							        <button type="submit" class="btn btn-primary float-right">

				                        <span class="text">Update</span>
				    
				                    </button>


  							</div>
  							
  							<!-- ----------- -->

  						</div>

  					</form>

  				</div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>

<script type="text/javascript">

    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {

        initialize_database();

    });

</script>

<script>
      tinymce.init({
        selector: '.pagecontent',
         plugins: "lists",
  toolbar: "undo redo | styleselect | bold italic | numlist bullist | alignleft aligncenter alignright alignjustify"
      });
    </script>

@endsection