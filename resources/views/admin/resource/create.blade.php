@extends('admin.layouts.app')

@section('title', 'Create')

@section('styles')


<link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">

<style type="text/css">
        
        .tox-notifications-container
        {
            display: none !important;
        }

      .tox.tox-tinymce {
    height: 500px !important;
}

.show-file-name {
        font-size: 12px;
    position: relative;
    top: -6px;
    }
    </style>


@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">

      Create Resource

     </h1>
    
</div>

<form action="{{ route('admin.resource.save') }}" id="create-user" method="POST" enctype="multipart/form-data">
            
    @csrf


    <div class="row">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Resource Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Resource Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="resource_name" placeholder="Resource Name" value="{{ old('resource_name') }}" maxlength="50" required="">

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Resource Description</label>

                                <textarea id="pagecontent" class="form-control pagecontent" name="resource_description" placeholder="Resource Description" ></textarea>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>
                    
					<div class="resource-file-section-wrap">
					<div class="row resource-file-row-wrap">
					  <div class="col-6">
					    <h3 class="resource-file-head">File</h3>
					  </div>
					  <div class="col-6">
					    <!--<button class="resource-file-addbtn btn">Add</button>-->
					  </div>
					</div>
					 
					<div class="resource-files-list-wrap"> 
					<div class="row">
					   <div class="col-md-9 col-8">
                            <div class="form-group">
                                <span class="show-file-name"></span>
                                <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                             
                            </div>
                        </div>
						<div class="col-md-3 col-4">
						 <!--<a class="delete-resource-file-link">Delete</a>-->
						</div>
					</div>
					
					<div class="row">
					   <div class="col-md-9 col-8">
                            <div class="form-group">
                                <span class="show-file-name"></span>
                                <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                                
                            </div>
                        </div>
						<div class="col-md-3 col-4">
						 <!--<a class="delete-resource-file-link">Delete</a>-->
						</div>
					</div>
					
					<div class="row">
					   <div class="col-md-9 col-8">
                            <div class="form-group">
                                <span class="show-file-name"></span>
                                <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                                
                            </div>
                        </div>
						<div class="col-md-3 col-4">
						 <!--<a class="delete-resource-file-link">Delete</a>-->
						</div>
					</div>
					
					<div class="row">
					   <div class="col-md-9 col-8">
                            <div class="form-group">
                               <span class="show-file-name"></span>
                                <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                              
                            </div>
                        </div>
						<div class="col-md-3 col-4">
						 <!--<a class="delete-resource-file-link">Delete</a>-->
						</div>
					</div>
					
					<div class="row">
					   <div class="col-md-9 col-8">
                            <div class="form-group">
                                <span class="show-file-name"></span>
                                <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                               
                            </div>
                        </div>
						<div class="col-md-3 col-4">
						 <!--<a class="delete-resource-file-link">Delete</a>-->
						</div>
					</div>
					
					</div>
					
                    <!--div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" name="rfile[]">
                            </div>
                        </div>
                    </div-->
					
					</div>

                    <button type="submit" class="btn btn-primary float-right" style="margin-top:15px;">

                        <span class="text">Create</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>

<script type="text/javascript">

$(document).ready(function() { 
            $('input[type="file"]').change(function(e) { 

                var fileName = $(this).val();

                if( fileName )
                {
                     var uFileName = e.target.files[0].name; 
               $(this).parent().parent().find('.show-file-name').text(uFileName)
                }
               else
               {
                $(this).parent().parent().find('.show-file-name').text('')
               }
  
            }); 
        }); 

</script>

<script>
      tinymce.init({
        selector: '.pagecontent',
         plugins: "lists",
  toolbar: "undo redo | styleselect | bold italic | numlist bullist | alignleft aligncenter alignright alignjustify"
      });
    </script>



@endsection