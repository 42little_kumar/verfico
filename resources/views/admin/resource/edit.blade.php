@extends('admin.layouts.app')

@section('title', 'Edit')

@section('styles')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">

<link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">

<style type="text/css">
        
        .tox-notifications-container
        {
            display: none !important;
        }

      .tox.tox-tinymce {
    height: 500px !important;
}

    .show-file-name {
        font-size: 12px;
    position: relative;
    top: -6px;
    }
    

    </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">

      Create Resource

     </h1>
    
</div>

  <?php

          function getBetween($string, $start = "", $end = ""){
    if (strpos($string, $start)) { // required if $start not exist in $string
        $startCharCount = strpos($string, $start) + strlen($start);
        $firstSubStr = substr($string, $startCharCount, strlen($string));
        $endCharCount = strpos($firstSubStr, $end);
        if ($endCharCount == 0) {
            $endCharCount = strlen($firstSubStr);
        }
        $toreplace = substr($firstSubStr, 0, $endCharCount);

        return str_replace('--'.$toreplace.'--', '', $string);
    } else {
        return $string;
    }
}

          ?>

<form action="{{ route('admin.resource.update') }}" id="create-user" method="POST" enctype="multipart/form-data">
            
    @csrf


    <div class="row">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Resource Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Resource Name<span class="text-danger">*</span></label>

                                <input type="hidden" name="id" value='{{ $data->id }}'>

                                <input type="text" class="form-control" name="resource_name" value="{{ $data->name }}" value="{{ old('resource_name') }}" maxlength="50" required="">

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Resource Description</label>

                                <textarea class="form-control pagecontent" name="resource_description" placeholder="Resource Description">{{ $data->description }}</textarea>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>
					
					<div class="resource-file-section-wrap">
                      <div class="row resource-file-row-wrap">
						  <div class="col-6">
							<h3 class="resource-file-head">Files</h3>
						  </div>
						  <div class="col-6">
							<!--<button class="resource-file-addbtn btn">Add</button>-->
						  </div>
					</div>
					<div class="resource-files-list-wrap"> 
					    <div class="row">

							
							 <div class="col-md-10 col-8">@if( !empty( $data->file_1 ) )<p><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $data->file_1 }}">{{ getBetween($data->file_1,"--","--") }}</a><p>

                                <input style="display: none;" type="file" name="rfile[]">
                                @else 

<div class="form-group">
                                    <span class="show-file-name"></span>
                                    <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                                </div>
                              @endif</div>
							 
							 <div class="col-md-2 col-4">
								@if( !empty( $data->file_1 ) )<a href="{{ route('resource.delete', [$data->id, 'file_1']) }}" onclick="return confirm('Are you sure you want to delete this file?');" class="delete-resource-file-link">Delete</a>@endif
							 </div>

					   </div>
					   
					   <div class="row">
					    
							 <div class="col-md-10 col-8">@if( !empty( $data->file_2 ) )<p><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $data->file_2 }}">{{ getBetween($data->file_2,"--","--") }}</a><p>
                                <input style="display: none;" type="file" name="rfile[]">
                                @else 

                             <div class="form-group">
                                    <span class="show-file-name"></span>
                               <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                                </div> @endif</div>
							 
							 <div class="col-md-2 col-4">
								@if( !empty( $data->file_2 ) )<a href="{{ route('resource.delete', [$data->id, 'file_2']) }}" onclick="return confirm('Are you sure you want to delete this file?');" class="delete-resource-file-link">Delete</a>@endif
							 </div>
					  </div> 

                    <div class="row">
					     	
							 <div class="col-md-10 col-8">@if( !empty( $data->file_3 ) )<p><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $data->file_3 }}">{{ getBetween($data->file_3,"--","--") }}</a><p>
                                <input style="display: none;" type="file" name="rfile[]">
                                @else 

                                <div class="form-group">
                                     <span class="show-file-name"></span>
                                    <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                                </div>
                              @endif</div>
							 <div class="col-md-2 col-4">
								@if( !empty( $data->file_3 ) )<a href="{{ route('resource.delete', [$data->id, 'file_3']) }}" onclick="return confirm('Are you sure you want to delete this file?');" class="delete-resource-file-link">Delete</a>@endif
							 </div>
					  </div> 

                    <div class="row">
					     	
							 <div class="col-md-10 col-8"> @if( !empty( $data->file_4 ) )<p><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $data->file_4 }}">{{ getBetween($data->file_4,"--","--") }}</a><p>
                                <input style="display: none;" type="file" name="rfile[]">
                                @else 
                             <div class="form-group">
                                    <span class="show-file-name"></span>
                                <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                                </div> @endif</div>
							 <div class="col-md-2 col-4">
								@if( !empty( $data->file_4 ) )<a href="{{ route('resource.delete', [$data->id, 'file_4']) }}" onclick="return confirm('Are you sure you want to delete this file?');" class="delete-resource-file-link">Delete</a>@endif
							 </div>
					  </div> 					  

                    <div class="row">
					     	
							  <div class="col-md-10 col-8">@if( !empty( $data->file_5 ) )<p><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $data->file_5 }}">{{ getBetween($data->file_5,"--","--") }}</a><p>
                                <input style="display: none;" type="file" name="rfile[]">
                                @else 
                              <div class="form-group">
                                <span class="show-file-name"></span>
                                    <div class="resource-input-type-file">
                                      Choose file <input type="file" name="rfile[]">
                                    </div>
                                </div> @endif</div>
							 <div class="col-md-2 col-4">
								@if( !empty( $data->file_5 ) )<a href="{{ route('resource.delete', [$data->id, 'file_5']) }}" onclick="return confirm('Are you sure you want to delete this file?');" class="delete-resource-file-link">Delete</a>@endif
							 </div>
					  </div> 
					  
					 
					
					</div>
                    </div>
					
                    <button type="submit" class="btn btn-primary float-right" style="margin-top:15px;">

                        <span class="text">Update</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">

$(document).ready(function() { 
            $('input[type="file"]').change(function(e) { 

                var fileName = $(this).val();

                if( fileName )
                {
                     var uFileName = e.target.files[0].name; 
               $(this).parent().parent().find('.show-file-name').text(uFileName)
                }
               else
               {
                $(this).parent().parent().find('.show-file-name').text('')
               }
  
            }); 
        }); 

</script>

<script>
      tinymce.init({
        selector: '.pagecontent',
         plugins: "lists",
  toolbar: "undo redo | styleselect | bold italic | numlist bullist | alignleft aligncenter alignright alignjustify"
      });
    </script>

@endsection