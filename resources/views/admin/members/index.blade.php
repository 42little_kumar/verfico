@extends('admin.layouts.app')

@section('title', 'Staff Members')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Staff Members</h1>

    @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write members'))

    <a href="{{ route('admin.staff.members.create') }}" class="float-right btn-create">

        <i class="fas fa-plus-circle"></i>

    </a>

    @endif
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            <div class="card-header text-right filter-header-wrap">

                <form method="GET" id="page-filter-form" class="justify-content-between">
                   
                   <div class="fliter_wrap_left">

                        <div class="input-group">
                
                            <select class="selectpicker" name="status">
                    
                                <option value="">-Filter by Status-</option>
        
                                <option value="active" @if(request()->query('status') && request()->query('status') == 'active') selected @endif>Active</option>
        
                                <option value="blocked" @if(request()->query('status') && request()->query('status') == 'blocked') selected @endif>Blocked</option>                        
                    
                            </select>
                
                        </div>

                        <div class="input-group">
                
                            <select class="selectpicker" name="sort-by">
                    
                                <option value="">-Sort By-</option>

                                <option value="name-ASC" @if(request()->query('sort-by') && request()->query('sort-by') == 'name-ASC') selected @endif>Name (Ascending)</option>

                                <option value="name-DESC" @if(request()->query('sort-by') && request()->query('sort-by') == 'name-DESC') selected @endif>Name (Descending)</option>                        
                    
                            </select>
                
                        </div>
                   
                        <div class="input-group">
                           
                            <input type="text" name="search" class="form-control bg-light border-0 small" value="{{ request()->query('search') }}" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                           
                        </div>
               
                        <div class="input-group" style="width:auto;">
                       
                            <div class="input-group-append">
                       
                                <button class="btn btn-primary" type="submit">
                           
                                    <i class="fas fa-search fa-sm"></i> Search

                                </button>
                       
                            </div>


                            @if(request()->query('status') != '' || request()->query('sort-by') != '' || request()->query('search') != '')

                            <div class="reset-group">

                                <a class="btn-filter-reset" href="{{ route('admin.staff.members') }}"><i class="fas fa-sync-alt fa-sm"></i> Reset</a>

                            </div>

                            @endif
                       
                        </div>
               
                    </div>
                  
                    <div class="line_limit_wrap">
                   
                        <div class="input-group" style="width:78px;" title="Select Limit">
                   
                            <select name="limit">
                       
                                <option value="10" @if(request()->query('limit') && request()->query('limit') == 10) selected @endif>10</option>

                                <option value="25" @if(request()->query('limit') && request()->query('limit') == 25) selected @endif>25</option>

                                <option value="50" @if(request()->query('limit') && request()->query('limit') == 50) selected @endif>50</option>

                                <option value="75" @if(request()->query('limit') && request()->query('limit') == 75) selected @endif>75</option>

                                <option value="100" @if(request()->query('limit') && request()->query('limit') == 100) selected @endif>100</option>

                            </select>
               
                        </div>
               
                    </div>

                </form>
            
            </div>
            
            <div class="card-body">

                <table class="table table-bordered table-hover">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Name</th>
        
                            <th scope="col">Email</th>
                            
                            <th scope="col">Role</th>

                            <th scope="col">Status</th>

                            @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write members'))
    
                            <th scope="col" class="text-center">Action</th>

                            @endif
        
                        </tr>
  
                    </thead>
  
                    <tbody>

                        @if($members->isEmpty())

                        <tr>

                            <td colspan="6" class="text-center">No records found.</td>

                        </tr>

                        @else

                            @foreach($members as $member)
    
                            <tr class="@if(Session::has('status-member') && $member->id == Session::get('status-member')) focused_row  @endif">
        
                                <td>

                                    @if(!empty($member->first_name))
                                    
                                        @php
                                        
                                            $full_name = trim($member->first_name . ' ' . $member->last_name);

                                        @endphp

                                        {{ str_limit($full_name, 40) }}

                                    @else
                                    
                                    Not Available
                                    
                                    @endif

                                </td>

                                <td>{{ $member->email }}</td>
                                
                                <td>{{ $member->getRoleNames()->first() ? $member->getRoleNames()->first() : 'N/A' }}</td>
                                
                                <td>
                                    
                                    @if($member->status)

                                        <span class="status_active">Active</span>
                                    
                                    @else
                                    
                                        <span class="status_active status_inactive">Blocked</span>
                                       
                                    @endif

                                </td>

                                @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write members'))
        
                                <td class="text-center">

                                    <div class="table-action">

                                        <a href="{{ route('admin.staff.members.edit', [ 'id' => $member->id ]) }}" class="btn btn-sm btn-primary btn-circle" title="Edit">
                    
                                            <i class="fas fa-pencil-alt"></i>
                    
                                        </a>

                                        <a href="{{ route('admin.staff.members.delete', [ 'id' => $member->id ]) }}" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
                        
                                            <i class="fas fa-trash"></i>
                    
                                        </a>

                                    </div>

                                </td>

                                @endif
        
                            </tr>

                            @endforeach

                        @endif
  
                    </tbody>

                </table>

                @if($members->isNotEmpty())

                    <div class="pagination-wrap">

                        <span class="entries float-right" style="margin-top: 7px">

                            Showing {{ $members->firstItem() }} to {{ $members->lastItem() }} of total {{ $members->total() }} entries

                        </span>

                        @php
                        
                            $paginate = [];

                            if(request()->query('search')) {

                                $paginate['search'] = request()->query('search');

                            }

                            if(request()->query('limit')) {

                                $paginate['limit'] = request()->query('limit');

                            }

                            if(request()->query('status')) {

                                $paginate['status'] = request()->query('status');

                            }
                            if(request()->query('sort-by')) {

                            $paginate['sort-by'] = request()->query('sort-by');

                            }

                        @endphp

                        {{ $members->appends($paginate)->links() }}

                        <div class="clearfix"></div>

                    </div>

                @endif

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

    $(document).ready(function() {

        $('.btn-action-delete').on('click', function(event) {

            if(!confirm('Are you sure you want to delete this member?')) {
            
                event.preventDefault();

            }

        });

    });

</script>

@endsection