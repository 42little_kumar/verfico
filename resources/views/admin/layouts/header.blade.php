<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    
    <ul class="navbar-nav ml-auto">

        @if( Auth::user()->hasRole('Superint') )

        <li class="nav-item dropdown no-arrow">

            <a class="nav-link {{ request()->is('superint/manpower') || request()->is('superint/manpower/*') ? 'active' : '' }}"" href="{{ route('manpower') }}">
            Manpower
            </a>

        </li>

        <li class="nav-item dropdown no-arrow">

            <a class="nav-link {{ request()->is('superint/projects') || request()->is('superint/projects/*') ? 'active' : '' }}"" href="{{ route('superint.projects') }}">
            Projects
            </a>

        </li>

        @endif

        <li class="nav-item dropdown no-arrow">
        
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            
                @if( auth()->user()->hasRole('Subcontractor') )

                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->company_name }}</span>

                @else

                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</span>

                @endif

                

                @php
                
                    if(auth()->user()->image) {

                        $user_image = asset('uploads/images/users/' . auth()->user()->id . '/' . auth()->user()->image);

                    } else {

                        $image = 'user';

                        if(auth()->user()->gender == 'male' || auth()->user()->gender == 'female') {

                            $image .= '-' . auth()->user()->gender;

                        }

                        $image .= '.png';

                        $user_image = asset('images/elements/' . $image);

                    }
                
                @endphp
                
                <img class="img-profile rounded-circle" src="{{ $user_image }}">
              
            </a>
            
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            
                <a class="dropdown-item" href="{{ route('admin.profile') }}">
                
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i> Profile
                
                </a>

                @if(auth()->user()->hasRole('Administrator'))

                <a class="dropdown-item" href="{{ route('admin.settings') }}">
                
                    <i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i> Settings
                
                </a>

                @endif
                
                <div class="dropdown-divider"></div>
                
                <a class="dropdown-item" href="{{ route('logout') }}">
                
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> Logout
                
                </a>
              
            </div>
            
        </li>
    
    </ul>

</nav>