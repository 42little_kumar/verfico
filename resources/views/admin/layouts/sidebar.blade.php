<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="javascript:void(0);">
        
        <div class="sidebar-brand-text mx-3">

            <img src="{{ asset('images/elements/logo-white.png') }}" alt="logo" style="width: 100%">

        </div>
      
    </a>

    <hr class="sidebar-divider my-0">

    

    <hr class="sidebar-divider">

    @if( auth()->user()->hasRole('Payroll') )

        <li class="nav-item {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'active' : '' }}">

                <a class="nav-link {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? '' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-staff-subCon" aria-expanded="true" aria-controls="collapse-staff-subCon">

                <i class="fas fa-users"></i> <span>Payrolls</span>
        
            </a>

                    <div id="collapse-staff-subCon" class="collapse {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'show' : 'show' }}" data-parent="#accordionSidebar-1">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                            

                            <a class="collapse-item {{ request()->is('superadmin/payrolls') || request()->is('superadmin/payrolls/*') ? 'active' : '' }}" href="{{ route('admin.payrolls') }}">All Payrolls</a>

                        </div>
                    
                    </div>
                
                </a>
            
            </li>

    @endif

    @if( auth()->user()->hasRole('Superint') )

        <li style="display:none;" class="nav-item {{ request()->input('userType') == 'subcontractor' || request()->is('superint/manpower') ? 'active' : '' }}">

                <a class="nav-link {{ request()->input('userType') == 'subcontractor' || request()->is('superint/manpower') ? '' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-staff-subCon" aria-expanded="true" aria-controls="collapse-staff-subCon">

                <i class="fas fa-users"></i> <span>Manpower Schedule</span>
        
            </a>

                    <div id="collapse-staff-subCon" class="collapse {{ request()->input('userType') == 'subcontractor' || request()->is('superint/manpower') ? 'show' : 'show' }}" data-parent="#accordionSidebar-1">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                         
                            

                            <a class="collapse-item {{ request()->is('superint/manpower') || request()->is('superint/manpower/*') ? 'active' : '' }}" href="{{ route('manpower') }}">All Schedules</a>

                           

                        </div>
                    
                    </div>
                
                </a>
            
            </li>

    @endif

    @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

        <li class="nav-item {{ request()->is('admin/dashboard') ? 'active' : '' }}">
        
            <a class="nav-link" href="{{ route('admin.dashboard') }}">
            
                <i class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
            
            </a>
          
        </li>

        <div class="sidebar-heading" id="accordionSidebar">Users</div>

        

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ ( request()->is('superadmin/users') || request()->is('superadmin/users/*') ) && request()->input('userType') != 'subcontractor' ? 'active' : '' }}">

                <a class="nav-link {{ ( request()->is('superadmin/users') || request()->is('superadmin/users/*') ) && request()->input('userType') != 'subcontractor' ? '' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-staff" aria-expanded="true" aria-controls="collapse-staff">

                <i class="fas fa-users"></i> <span>Users</span>
        
            </a>

                    <div id="collapse-staff" class="collapse {{ ( request()->is('superadmin/users') || request()->is('superadmin/users/*') ) && request()->input('userType') != 'subcontractor' ? 'show' : 'show' }}" data-parent="#accordionSidebar">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                            @if( auth()->user()->hasRole('Superadmin') )

                            <a class="collapse-item {{ request()->input('userType') == 'admin' ? 'active' : '' }}" href="{{ route('admin.users', 'userType=admin') }}">All Admins</a>
                            
                            @endif

                            <a class="collapse-item {{ request()->input('userType') == 'foreman' ? 'active' : '' }}" href="{{ route('admin.users', 'userType=foreman') }}">All Foremen</a>

                            <a class="collapse-item {{ request()->input('userType') == 'worker' ? 'active' : '' }}" href="{{ route('admin.users', 'userType=worker') }}">All Workers</a>

                            <a class="collapse-item {{ request()->input('userType') == 'safetyo' ? 'active' : '' }}" href="{{ route('admin.users', 'userType=safetyo') }}">All Safety Officers</a>

                            <a class="collapse-item {{ request()->is('superadmin/workers/request') || request()->is('superadmin/workers/request/*') ? 'active' : '' }}" href="{{ route('admin.workers.requests') }}">Worker Requests <span class="request-count">{{ getWorkerRequestCount() }}</span></a>

                            <a class="collapse-item {{ request()->is('superadmin/workers/types') || request()->is('superadmin/workers/types/*') ? 'active' : '' }}" href="{{ route('admin.workers.types') }}">Worker Types </a>

                            <a class="collapse-item {{ request()->is('superadmin/work/types') || request()->is('superadmin/work/types/*') ? 'active' : '' }}" href="{{ route('admin.work.types') }}">Work Types </a>
                    
                        </div>
                    
                    </div>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

        <div class="sidebar-heading" id="accordionSidebar-1">Subcontractors</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'active' : '' }}">

                <a class="nav-link {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? '' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-staff-subCon" aria-expanded="true" aria-controls="collapse-staff-subCon">

                <i class="fas fa-users"></i> <span>Subcontractors</span>
        
            </a>

                    <div id="collapse-staff-subCon" class="collapse {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'show' : 'show' }}" data-parent="#accordionSidebar-1">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                             <a class="collapse-item {{ request()->input('userType') == 'subcontractor' ? 'active' : '' }}" href="{{ route('admin.users', 'userType=subcontractor') }}">All Subcontractors</a>

                            <a class="collapse-item {{ request()->is('superadmin/pages/billboard') || request()->is('superadmin/pages/billboard/*') ? 'active' : '' }}" href="{{ route('admin.pages.billboard') }}">Billboard</a>
                    
                        </div>
                    
                    </div>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

        <div class="sidebar-heading" id="accordionSidebar-1">Reports</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'active' : '' }}">

                <a class="nav-link {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? '' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-staff-subCon" aria-expanded="true" aria-controls="collapse-staff-subCon">

                <i class="fas fa-users"></i> <span>Reports</span>
        
            </a>

                    <div id="collapse-staff-subCon" class="collapse {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'show' : 'show' }}" data-parent="#accordionSidebar-1">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                             <a class="collapse-item {{ request()->is('log/reports/show') || request()->is('log/reports/*') ? 'active' : '' }}" href="{{ route('admin.logreports') }}">All Reports</a>

                        </div>
                    
                    </div>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

        <div class="sidebar-heading" id="accordionSidebar-1">Superintendents</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->input('userType') == 'superint' || request()->is('superadmin/payrolls') ? 'active' : '' }}">

                <a class="nav-link {{ request()->input('userType') == 'superint' || request()->is('superadmin/payrolls') ? '' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-staff-subCon" aria-expanded="true" aria-controls="collapse-staff-subCon">

                <i class="fas fa-users"></i> <span>Superintendents</span>
        
            </a>

                    <div id="collapse-staff-subCon" class="collapse {{ request()->input('userType') == 'superint' || request()->is('superadmin/payrolls') ? 'show' : 'show' }}" data-parent="#accordionSidebar-1">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                             <a class="collapse-item {{ request()->input('userType') == 'superint' ? 'active' : '' }}" href="{{ route('admin.users', 'userType=superint') }}">All Superintendents</a>
                             
                        </div>
                    
                    </div>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

        <div class="sidebar-heading" id="providers-1">Payrolls</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'active' : '' }}">

                <a class="nav-link {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? '' : '' }}" href="#" data-toggle="collapse" data-target="#collapse-staff-providers" aria-expanded="true" aria-controls="collapse-staff-providers">

                <i class="fas fa-users"></i> <span>Payrolls</span>
        
            </a>

                    <div id="collapse-staff-providers" class="collapse {{ request()->input('userType') == 'payroll' || request()->is('superadmin/payrolls') ? 'show' : 'show' }}" data-parent="#providers-1">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                            <a class="collapse-item {{ request()->is('superadmin/payrolls') || request()->is('superadmin/payrolls/*') ? 'active' : '' }}" href="{{ route('admin.payrolls') }}">All Payrolls</a>

                             <a class="collapse-item {{ request()->input('userType') == 'payroll' ? 'active' : '' }}" href="{{ route('admin.users', 'userType=payroll') }}">Payroll Providers</a>

                            

                            <a class="collapse-item {{ request()->is('superadmin/notifications/payroll') || request()->is('superadmin/notifications/payroll/*') ? 'active' : '' }}" href="{{ route('admin.payroll.notifications') }}">Payroll Notifications</a>
                    
                        </div>
                    
                    </div>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

        <div class="sidebar-heading" id="accordionSidebar-resource">Resources</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->is('superadmin/resource') || request()->is('superadmin/projects/*') ? 'active' : '' }}" >

                <a class="nav-link" href="#" aria-controls="collapse-staff-resource" data-target="#collapse-staff-resource" data-toggle="collapse">
                
                    <i class="fas fa-project-diagram"></i> <span>Resources</span>

                </a>

                    <div id="collapse-staff-resource" class="collapse {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/resource') ? 'show' : 'show' }}" data-parent="#accordionSidebar-resource">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                             <a class="collapse-item {{ request()->is('superadmin/resource') || request()->is('superadmin/resource/*') ? 'active' : '' }}" href="{{ route('admin.resource') }}">All Resources</a>

                             <a class="collapse-item {{ request()->is('superadmin/resource-content') || request()->is('superadmin/resource-content/*') ? 'active' : '' }}" href="{{ route('admin.resource.content.create') }}">Landing Page Content</a>
                    
                        </div>

                    
                    </div>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

        <div class="sidebar-heading" id="accordionSidebar-project">Projects</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->is('superadmin/projects') || request()->is('superadmin/projects/*') ? 'active' : '' }}" >

                <a class="nav-link" href="#" aria-controls="collapse-staff-projectblock" data-target="#collapse-staff-projectblock" data-toggle="collapse">
                
                    <i class="fas fa-project-diagram"></i> <span>Projects</span>

                </a>

                    <div id="collapse-staff-projectblock" class="collapse {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'show' : 'show' }}" data-parent="#accordionSidebar-project">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                             <a class="collapse-item {{ request()->input('userType') == 'subcontractor' ? 'active' : '' }}" href="{{ route('admin.projects') }}">All Projects</a>

                            <a class="collapse-item {{ request()->is('superadmin/projects/fav') || request()->is('superadmin/projects/fav/*') ? 'active' : '' }}" href="{{ route('admin.projects.get.fav') }}">Favorite Projects</a>

                       
                    
                        </div>
                    
                    </div>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

        <div class="sidebar-heading">Notifications</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->is('superadmin/announcement') || request()->is('superadmin/announcement/*') ? 'active' : '' }}">

                <a class="nav-link" href="{{ route('admin.announcement.create') }}">
                
                    <i class="fas fa-bell"></i> <span>Announcements</span>
                
                </a>
            
            </li>

        @endif

        <!-- --------------- -->

        <div class="sidebar-heading">Settings</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->is('superadmin/log/merger') || request()->is('superadmin/log/merger/*') ? 'active' : '' }}">

                <a class="nav-link" href="{{ route('admin.log.merger') }}">
                
                    <i class="fas fa-edit"></i> <span>Log Merger</span>
                
                </a>
            
            </li>

        @endif

        <!-- --------------- -->

        <div class="sidebar-heading">Frontend</div>

        @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

            <li class="nav-item {{ request()->is('superadmin/announcement') || request()->is('superadmin/announcement/*') ? 'active' : '' }}">

                <a class="nav-link" target="_blank" href="{{ route('f.resources') }}">
                
                    <i class="fas fa-file"></i> <span>Resources View</span>
                
                </a>
            
            </li>

        @endif

        <hr class="sidebar-divider">

    @endif

    @if( auth()->user()->hasRole('Subcontractor') )

        <li class="nav-item {{ request()->is('subcontractor/dashboard') ? 'active' : '' }}">
        
            <a class="nav-link" href="{{ route('subcontractor.dashboard') }}">
            
                <i class="fas fa-fw fa-tachometer-alt"></i> <span>Dashboard</span>
            
            </a>
          
        </li>

        <div class="sidebar-heading">Workers</div>

        @if( auth()->user()->hasRole('Subcontractor') )

            <li class="nav-item {{ request()->is('subcontractor/workers') || request()->is('subcontractor/workers/*') ? 'active' : '' }}">

                <a class="nav-link" href="{{ route('subcontractor.workers') }}">
                
                    <i class="fas fa-users"></i> <span>All Workers</span>
                
                </a>
            
            </li>

        @endif

        <div class="sidebar-heading" id="accordionSidebar-sub-project">Projects</div>

        @if( auth()->user()->hasRole('Subcontractor') )

            <li class="nav-item {{ request()->is('subcontractor/projects') || request()->is('subcontractor/projects/*') ? 'active' : '' }}">

                <a class="nav-link" href="#" aria-controls="collapse-staff-projectblock" data-target="#collapse-staff-projectblock" data-toggle="collapse">
                
                    <i class="fas fa-project-diagram"></i> <span>All Projects</span>
                
                </a>

                <div id="collapse-staff-projectblock" class="collapse {{ request()->input('userType') == 'subcontractor' || request()->is('superadmin/payrolls') ? 'show' : 'show' }}" data-parent="#accordionSidebar-sub-project">
                    
                        <div class="bg-white py-2 collapse-inner rounded">

                             <a class="collapse-item {{ request()->input('userType') == 'subcontractor' ? 'active' : '' }}" href="{{ route('subcontractor.projects') }}">All Projects</a>

                            <a class="collapse-item {{ request()->is('superadmin/project/fav') || request()->is('superadmin/project/fav/*') ? 'active' : '' }}" href="{{ route('subcontractor.projects.get.fav') }}">Favorite Projects</a>
                    
                        </div>
                    
                    </div>

                </a>
            
            </li>

        @endif

        <div class="sidebar-heading">Manpower</div>

        @if( auth()->user()->hasRole('Subcontractor') )

            <li class="nav-item {{ request()->is('subcontractor/manpower/requests') || request()->is('subcontractor/manpower/requests/*') ? 'active' : '' }}">

                <a class="nav-link" href="{{ route('subcontractor.manpower.view.d') }}?d={{ date('m-d-Y') }}">
                
                    <i class="fas fa-credit-card"></i> <span>All Schedules</span>
                
                </a>
            
            </li>

        @endif

        <div class="sidebar-heading">Payrolls</div>

        @if( auth()->user()->hasRole('Subcontractor') )

            <li class="nav-item {{ request()->is('subcontractor/payrolls') || request()->is('subcontractor/payrolls/*') ? 'active' : '' }}">

                <a class="nav-link" href="{{ route('subcontractor.payrolls') }}">
                
                    <i class="fas fa-credit-card"></i> <span>All Payrolls</span>
                
                </a>
            
            </li>

        @endif

        <div class="sidebar-heading">Documents</div>

        @if( auth()->user()->hasRole('Subcontractor') )

            <li class="nav-item {{ request()->is('subcontractor/documents') || request()->is('subcontractor/documents/*') ? 'active' : '' }}">

                <a class="nav-link" href="{{ route('subcontractor.documents') }}">
                
                    <i class="fas fa-credit-card"></i> <span>All Documents</span>
                
                </a>
            
            </li>

        @endif

        @if( Auth::User()->id == 624 )

        <div class="sidebar-heading">Frontend</div>

        @if( auth()->user()->hasRole('Subcontractor') )

            <li class="nav-item {{ request()->is('superadmin/announcement') || request()->is('superadmin/announcement/*') ? 'active' : '' }}">

                <a class="nav-link" target="_blank" href="{{ route('f.resources') }}">
                
                    <i class="fas fa-file"></i> <span>Resources View</span>
                
                </a>
            
            </li>

        @endif

        @endif

    @endif


    <div class="text-center d-none d-md-inline">
    
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    
    </div>

</ul>