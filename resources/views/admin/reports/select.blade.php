@extends('admin.layouts.app')

@section('title', 'Reports')

@section('styles')

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }

        .week-ending {
            width: 260px !important;
        }

        .green
        {
            color: green;
        }

        .red
        {
            color: red;
        }

        div#datatable_processing {
    color: white;
    background: #0f3459;
}
		
		/*--table thead th:nth-child(2){
		width:187px !important;	
		}
		table thead th:nth-child(4){
		width: 128px !important;	
		}
		table thead th:nth-child(5){
		width: 160px !important;	
		}
		
		table thead th:nth-child(6){
		width:287px !important;	
		}--*/
		.payroll_custom_table_wrap td:first-child {
            width: 100px!important;
        }

    </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Reports</h1>

    
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body worker_page_wrapper log-reports-text-wrapper">
               
              <form action="{{ route('admin.report.generate') }}" method="post" class="reportform">
               <div class="log-report-filter-wrapper">
                 @csrf

            <select name="reporttype" id="type-select">

              <option disabled selected>-Select-</option>

              <option @if( isset( $_REQUEST['q_sub'] ) ) selected  @endif value="sub">Subcontractor</option>
              <option @if( isset( $_REQUEST['q_job'] ) ) selected  @endif value="job">Job #</option>

            </select>
            
              <select  name="sub_id" class="subfield" required="" data-live-search='true'>

                <option value="" disabled selected>-Subcontractor-</option>

                @foreach( $subcontractors as $g )

                  <option @if(isset( $_REQUEST['q_sub'] ) && @$_REQUEST['q_sub'] == $g->id ) selected  @endif value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
                
              </select>

              <select  name="job_id" class="jobfield" required="" data-live-search='true'>

                <option value="" disabled selected>-Job-</option>

                @foreach( $projects as $g )

                  <option @if( isset( $_REQUEST['q_job'] ) && @$_REQUEST['q_job'] == $g->id ) selected  @endif value="{{ $g->id }}">{{ $g->job_name }} @if(!empty($g->job_number))[{{ $g->job_number }}]@endif</option>

                @endforeach
                
              </select>

              <input style="display:none;" id="date-filter" type="text" value="{{ @$_REQUEST['q_date'] }}" name="started_at" required="" class="datepicker_one commonfield" autocomplete="off">

              <input style="display:none;" class="commonfield" type="submit" value="Create Report">
              </div>
            </form>

            </div>

        </div>

    </div>

</div>



@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>tinymce.init({selector:'textarea'});
  
  
  </script>

  

<script type="text/javascript">

  $(document).ready(function() {

    $('select').removeAttr('required','required');

@if( !isset( $_REQUEST['q_sub'] ) )
    $('.subfield').hide();

    @endif

    @if( !isset( $_REQUEST['q_job'] ) )
          $('.jobfield').hide();

          @endif


        $( ".datepicker_one" ).daterangepicker({
          maxDate: new Date()
        });

      });

  /*
        * @Subcontractor Filter
        *
        *
        */

        $('#type-select').change(function(){

          if( $(this).val() == 'sub' )
          {
            $('.subfield').show();
            $('.jobfield').hide();

            $('.subfield').attr('required','required');
            $('.jobfield').removeAttr('required','required');

          }

          if( $(this).val() == 'job' )
          {
            $('.subfield').hide();
            $('.jobfield').show();

            $('.jobfield').attr('required','required');
            $('.subfield').removeAttr('required','required');
          }

          $('.commonfield').show();
          

        })

        @if( isset( $_REQUEST['q_job'] ) )

        
        $('.commonfield').show();

        @endif

        @if( isset( $_REQUEST['q_sub'] ) )

        $('.commonfield').show();

        @endif



        /*$('#type-select').change(function()
        {
             var selectVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?type='+selectVal;

             window.location.href = url;


        })*/

</script>

@endsection