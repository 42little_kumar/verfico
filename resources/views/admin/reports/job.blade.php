@extends('admin.layouts.app')

@section('title', 'Reports')

@section('styles')

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }

        .week-ending {
            width: 260px !important;
        }

        .green
        {
            color: green;
        }

        .red
        {
            color: red;
        }

        div#datatable_processing {
    color: white;
    background: #0f3459;
}
		
		/*--table thead th:nth-child(2){
		width:187px !important;	
		}
		table thead th:nth-child(4){
		width: 128px !important;	
		}
		table thead th:nth-child(5){
		width: 160px !important;	
		}
		
		table thead th:nth-child(6){
		width:287px !important;	
		}--*/
		.payroll_custom_table_wrap td:first-child {
            width: 100px!important;
        }

    </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Reports</h1>

    
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body worker_page_wrapper log-reports-text-wrapper">


            <div class="custom_table_filter">
            <!-- Subcontractor filter -->

            

            <!-- Subcontractor filter -->

            <!-- Date filter -->

            <!--<input id="date-filter" type="text" placeholder="Select Pay Period End Date" value="{{ @$_REQUEST['date'] }}" name="started_at" required="" class="form-control datepicker_one week-ending" autocomplete="off">-->

            <!-- Date filter -->

            <!-- Reset filter -->

            @if(
            (
                isset( $_GET['subcontractor'] ) ||
                isset( $_GET['date'] )
            )
            )

            <a href="{{ route('admin.logreports') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

            @endif
            <div class="log-reports-modify-search-wrap">
            <a href="{{ route('admin.logreports') }}?q_job={{$jobid}}&q_date={{$started_at}}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Modify Search</a>

            
            <span> Job: {{ \App\Project::where('id', $jobid)->first()->job_name }} </span>
           
            <span> Date Range: {{ $started_at }} </span>
            </div>
            <!-- Reset filter -->
          </div>
            
                <table class="table table-bordered table-hover payroll_custom_table_wrap" id="datatable">
  
                    <thead>
    
                        <tr>    


                            <th scope="col">Sub Name</th>

                            <?php foreach( \DB::table('worker_types')->get() as $g ): ?>

                            <th scope="col">{{ $g->type }}</th>

                            <?php endforeach; ?>

                            <th scope="col">Total </th>

        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>



@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>tinymce.init({selector:'textarea'});
  
  
  </script>

  

<script type="text/javascript">

    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this payroll?')) {
        
            event.preventDefault();

        }

    });

    /* ----------------------------------- Next/Prev ----------------------------------- */

    serialize = function(obj) {
      var str = [];
      for (var p in obj)
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      return str.join("&");
    }
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

    $(document).on("click","a.index-link", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&subcontractor="+$.urlParam('subcontractor')+"&date="+$.urlParam('date');
        
        event.preventDefault();
    });
    $(document).on("click","a[title='Edit']", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&subcontractor="+$.urlParam('subcontractor')+"&date="+$.urlParam('date');
        
        event.preventDefault();
    });
    $(document).on("click","a[title='View']", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&subcontractor="+$.urlParam('subcontractor')+"&date="+$.urlParam('date');
        
        event.preventDefault();
    });


    var table;

    /* ----------------------------------- Next/Prev ----------------------------------- */

    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_payrolls_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

            /*@if( isset( $_REQUEST['rowcount'] ) )

            "iDisplayLength": "{{ $_REQUEST['rowcount'] }}",

            @endif

            "rowCallback": function( row, data ) {
    
              var isempty = true;

              <?php foreach( \DB::table('worker_types')->get() as $g ): ?>

                if( data.{{ strtolower(str_replace(' ', '', $g->type)) }} != '0:0')
                {
                  isempty = false;
                }

                <?php endforeach; ?>

                if( isempty == true )
                {
               $(row).hide();
                }
              

  },*/

            stateSave: false,

            "order": [[1,'desc'], [2,'desc'],[3,'desc'], [4,'desc'] ],

            dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csvHtml5',
          text: 'Export',
          exportOptions: {
             modifier : {
             page : 'all', // 'all', 'current'
         },
          }
                }
      ],

            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No payrolls found!',

            searchPlaceholder: "Search...",
             sProcessing: "<span class='total-hours-class'>Calculation of total hours could take some time, Please wait.....<br><i class='fas fa-spinner fa-spin'></i></span>",
             },

            processing: true,

            serverSide: false,

         
            "ajax": {

              "url": "{{ route('admin.report.job.get') }}",

              "data": function ( d ) {

               d.projectData = null,
               d.jobid = "{{ @$jobid }}",
               d.log_date = "{{ @$started_at }}"
              
            }

            },

            columns: [

                { data: "sub_name", name: "job_name", searchable: true, orderable: true },

                <?php foreach( \DB::table('worker_types')->get() as $g ): ?>

                { data: "{{ strtolower(str_replace(' ', '', $g->type)) }}", name: "{{ strtolower(str_replace(' ', '', $g->type)) }}", searchable: true, orderable: true },
                

                <?php endforeach; ?>
                { data: "total", name: "total", searchable: true, orderable: true },

                

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $( ".datepicker_one" ).daterangepicker();

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_payrolls_pageLength", len);

        });

        /*
         * @Function Name
         *
         *
         */
        $('.week-ending').datepicker({

                dateFormat: 'MM dd, yy' ,

                   

                    onSelect: function (dateText, inst) {

                        var newdate = new Date(dateText);

                        const monthNames = ["January", "February", "March", "April", "May", "June",
                          "July", "August", "September", "October", "November", "December"
                        ];

                        var endDate = newdate.setDate(newdate.getDate() - 6);

                        var dd = newdate.getDate();

                        var mm = monthNames[newdate.getMonth()];

                        var y = newdate.getFullYear();

                        endDate = mm+' '+dd+', '+y;

                        $('.start-date').val(endDate);

                        $('.end-date').val(dateText);

                        $('.week-ending').val( endDate+' to '+dateText );

                        /* ***** */

                        var dateVal = endDate+' to '+dateText;

                        var url = window.location.href;

                        var a = url.indexOf("?");

                        var b =  url.substring(a);

                        var c = url.replace(b,"");

                        url = c;

                        url += '?date='+dateVal;

                        window.location.href = url;

                        /* ***** */
                         
                        }

            })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#subcontractor-filter').change(function()
        {
             var selectVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?subcontractor='+selectVal;

             window.location.href = url;


        })

        /*
        * @Date Filter
        *
        *
        */
        $('#date-filter-1').change(function()
        {
             var dateVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?date='+dateVal;

             window.location.href = url;


        })

        $('#date-filter').change(function()
        {
             var dateVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?date='+dateVal;

             window.location.href = url;


        })

    });

</script>

@endsection