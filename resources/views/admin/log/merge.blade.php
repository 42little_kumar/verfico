@extends('admin.layouts.app')

@section('title', 'Log Merger')

@section('content')

@section('styles')

    <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>

	<link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">

    <style type="text/css">
    	.tox-notifications-container
    	{
    		display: none !important;
    	}

      .tox.tox-tinymce {
    height: 500px !important;
}

ul
{
  list-style: none;
  padding: 0;
  margin-top: 10px;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100%;
    margin: 0 0 10px;
}
.tokeeplogs ul, .todeletelogs ul{
    background: #f7f3f3;
    padding: 10px;
    color: #000;
	margin:0px;
}

.tokeeplogs ul li , .todeletelogs ul li{
    display: block;
    /* margin: 5px 0; */
    border-bottom: 1px solid #e2e2e2;
    padding: 8px 0;
}
.tokeeplogs ul li:last-child, .todeletelogs ul li:last-child{
 border:0px;	
}
    </style>

@endsection


<div class="d-sm-flex align-items-center justify-content-between mb-4">

    <h1 class="h3 mb-0 text-gray-800">Log Merger</h1>

</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

        <div class="card-body shadow mb-4" style="background:#fff;">
  				
  			
  					
  					<form action='{{ route("admin.log.merger.save") }}' method='post' name='notification-form' class='notification-form'>

              @csrf

  						<div class='row'>

  							<!-- ----------- -->

  							<div class="col-md-6">

                  <label>Worker to keep</label>

  							  <select name="tokeep" class="tokeep" required="" data-live-search='true' required="">

                    <option value=''>-Please Select-</option>

                    @foreach( $workers as $g )

                      <option value="{{ $g->id }}">{{ $g->first_name }} {{ $g->last_name }}, @if( empty( $g->email ) ) N/A @else {{ $g->email }} @endif ,  @if( empty( $g->phone_number ) ) N/A @else {{ $g->phone_number }} @endif</option>

                    @endforeach
                  
                </select> 

                <div class="tokeeplogs">
                </div>

  							</div>

                <div class="col-md-6">

                  <label>Worker to delete</label>

                  <select name="todelete" class="todelete" required="" data-live-search='true' required="">


                    
                  
                </select> 

                <div class="todeletelogs">
                </div>

                </div>

							 
  							
  							<!-- ----------- -->

  							<!-- ----------- -->
                            
  							<div class="col-md-12">


  							        <button type="submit" class="btn btn-primary float-right activebutton" style="margin-top:15px;">

				                        <span class="text">Merge Logs</span>
				    
				                    </button>

                       


  							</div>
  							
  							<!-- ----------- -->

  						</div>

  					</form>

  			

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>

<script type="text/javascript">

    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {

  

        var tokeep = false;
        var todelete = false;

        $('.tokeep').change(function(event){

          event.stopPropagation();

          $('.todeletelogs').html(''); 

         $.ajax({
           type:'GET',
           url:'{{ route("get.merger.logs") }}',
           data:{id:$(this).val()},
           success:function(data){
              $('.tokeeplogs').html(data); 
           }
        });

        })

        $('.tokeep').change(function(e){

          var $s = $(e.target);
    

          //$('.todelete').find('option').not(':first').remove();

          $('.todelete').find('option').remove();

          var $options = $(".tokeep > option").clone();
          $('select[name="todelete"]').append($options);

          

          var valthis = $(this).val();

          $(".todelete").not($s).find("option[value="+$s.val()+"]").remove();

          $('.todelete').selectpicker('refresh');


        })

        $('.todelete').change(function(event){

          event.stopPropagation();

          $.ajax({
           type:'GET',
           url:'{{ route("get.merger.logs") }}',
           data:{id:$(this).val()},
           success:function(data){
              $('.todeletelogs').html(data); 
           }
        });
          
        })


    });




</script>

<script>
      tinymce.init({
        selector: '.pagecontent',
         plugins: "lists",
  toolbar: "undo redo | styleselect | bold italic | numlist bullist | alignleft aligncenter alignright alignjustify"
      });
    </script>

@endsection