@extends('admin.layouts.app')

@section('title', 'Settings')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Settings</h1>
    
</div>

<div class="row">

    <div class="col-12">

        <ul class="nav nav-pills nav-fill">

            <li class="nav-item">

                <a class="nav-link @if($tab == 'general') active @endif" href="?tab=general">General</a>
  
            </li>
  
            <li class="nav-item">

                <a class="nav-link @if($tab == 'mail') active @endif" href="?tab=mail">Mail</a>

            </li>

            <li class="nav-item">

                <a class="nav-link @if($tab == 'payments') active @endif" href="?tab=payments">Payments</a>

            </li>

        </ul>

    </div>

</div>

<br>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                
                <h6 class="m-0 font-weight-bold text-primary">
                    
                    {{ title_case($tab) }} Settings

                </h6>
            
            </div>

            <div class="card-body">

                <form action="{{ route('admin.settings') }}" method="POST">

                    @csrf

                    @foreach($settings as $setting)

                    <div class="row">

                        <div class="col-md-6">

                            <p style="font-weight: 600">{{ title_case(str_replace('-', ' ', $setting->name)) }}</p>

                        </div>

                        <div class="col-md-6">

                            @if($setting->name == 'paypal-mode')
                            
                            <div class="form-group">

                                <select name="{{ $setting->name }}" class="form-control">

                                    <option value="">-Select-</option>
                                    
                                    <option value="live" @if($setting->value == 'live') selected @endif>Live</option>
                                    
                                    <option value="sandbox" @if($setting->value == 'sandbox') selected @endif>Sandbox</option>

                                </select>

                            </div>

                            @elseif($setting->name == 'currency')

                            <div class="form-group">

                                <select name="{{ $setting->name }}" class="form-control">

                                    <option value="">-Select-</option>
                                    
                                    <option value="usd" @if($setting->value == 'usd') selected @endif>USD</option>

                                </select>

                            </div>

                            @elseif($setting->name == 'mail-driver')

                            <div class="form-group">

                                <select name="{{ $setting->name }}" class="form-control">

                                    <option value="">-Select-</option>
                                    
                                    <option value="smtp" @if($setting->value == 'smtp') selected @endif>SMTP</option>

                                </select>

                            </div>

                            @elseif($setting->name == 'mail-encryption')

                            <div class="form-group">

                                <select name="{{ $setting->name }}" class="form-control">

                                    <option value="">-Select-</option>
                                    
                                    <option value="tls" @if($setting->value == 'tls') selected @endif>TLS</option>

                                </select>

                            </div>

                            @elseif($setting->name == 'mail-password')

                            <div class="form-group">

                                <input type="password" name="{{ $setting->name }}" placeholder="{{ title_case(str_replace('-', ' ', $setting->name)) }}" maxlength="50" class="form-control" autocomplete="off" value="{{ $setting->value }}">

                            </div>

                            @else

                            <div class="form-group">

                                <input type="text" name="{{ $setting->name }}" placeholder="{{ title_case(str_replace('-', ' ', $setting->name)) }}" maxlength="50" class="form-control" autocomplete="off" value="{{ $setting->value }}">

                            </div>

                            @endif

                        </div>

                    </div>

                    @if($setting->name == 'currency' || $setting->name == 'paypal-secret')

                    <br>

                    @endif

                    @endforeach

                    <div class="row">

                        <div class="col-lg-12">

                            <button class="btn btn-primary float-right">Update</button>

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection