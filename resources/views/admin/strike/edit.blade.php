@extends('admin.layouts.app')

@section('title', 'Strike Edit')

@section('styles')

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }

        .week-ending {
            width: 260px !important;
        }

        .green
        {
            color: green;
        }

        .red
        {
            color: red;
        }

        div#datatable_processing {
    color: white;
    background: #0f3459;
}
		
		/*--table thead th:nth-child(2){
		width:187px !important;	
		}
		table thead th:nth-child(4){
		width: 128px !important;	
		}
		table thead th:nth-child(5){
		width: 160px !important;	
		}
		
		table thead th:nth-child(6){
		width:287px !important;	
		}--*/
		.payroll_custom_table_wrap td:first-child {
            width: 100px!important;
        }

    </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Strike Edit</h1>

    <a href="{{ route('admin.users.view', $workerId) }}" class="back-link">< Back</a>
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body worker_page_wrapper log-reports-text-wrapper">

              <form action="{{ route('admin.strike.update', $strikeData->id) }}" method="post" class="reportform">

                @csrf

                <div class="row">

                        

                        <div class="col-md-6">

                            

                            <div class="form-group">

                                <label>Job (Project):  </label>

                                <select class="form-control" name="project">

                                  @foreach( \App\Project::where('deleted_at', null)->where('status', 'active')->get() as $g )

                                  <option @if( $g->id == $strikeData->name ) selected @endif value="{{ $g->id }}">{{ $g->job_name }}</option>

                                  @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                           <div class="form-group">

                                <label>Category:  </label>

                                <select class="form-control" name="category">

                                  @foreach( \DB::Table('strike_category')->get() as $g )

                                  <option @if( $g->name == $strikeData->category ) selected @endif value="{{ $g->name }}">{{ $g->name }}</option>

                                  @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Violation Description:  </label>

                                <textarea class="form-control" name="description">{{ $strikeData->description }}</textarea>

                            </div>

                        </div>

                        

                </div>

                <div class="row">

                  <div class="col-md-12">

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary float-right">

                                <span class="text">Update</span>
            
                            </button>


                        </div>

                    </div>

                </div>

            </form>

            </div>

        </div>

    </div>

</div>



@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
  
  
  </script>

  

<script type="text/javascript">

  $(document).ready(function() {

    $('select').removeAttr('required','required');

@if( !isset( $_REQUEST['q_sub'] ) )
    $('.subfield').hide();

    @endif

    @if( !isset( $_REQUEST['q_job'] ) )
          $('.jobfield').hide();

          @endif


        $( ".datepicker_one" ).daterangepicker();

      });

  /*
        * @Subcontractor Filter
        *
        *
        */

        $('#type-select').change(function(){

          if( $(this).val() == 'sub' )
          {
            $('.subfield').show();
            $('.jobfield').hide();

            $('.subfield').attr('required','required');
            $('.jobfield').removeAttr('required','required');

          }

          if( $(this).val() == 'job' )
          {
            $('.subfield').hide();
            $('.jobfield').show();

            $('.jobfield').attr('required','required');
            $('.subfield').removeAttr('required','required');
          }

          $('.commonfield').show();
          

        })

        @if( isset( $_REQUEST['q_job'] ) )

        
        $('.commonfield').show();

        @endif

        @if( isset( $_REQUEST['q_sub'] ) )

        $('.commonfield').show();

        @endif



        /*$('#type-select').change(function()
        {
             var selectVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?type='+selectVal;

             window.location.href = url;


        })*/

</script>

@endsection