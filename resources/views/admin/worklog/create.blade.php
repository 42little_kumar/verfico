@extends('admin.layouts.app')

@section('title', 'New User')

@section('styles')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">New Log</h1>

    <a href="{{ route('admin.users') }}" class="back-link">< Back to Users</a>
    
</div>

<form action="{{ route('admin.log.save') }}" id="create-log" method="POST" enctype="multipart/form-data">
            
    @csrf

    <div class="row">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Log Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Punch In<span class="text-danger">*</span></label>

                                <input type="text" class="form-control timepicker" name="punch_in" placeholder="12:00" value="{{ old('punch_in') }}" maxlength="50" required>

                                <span id="error-job-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Punch Out</label>

                                <input type="text" class="form-control timepicker" name="punch_out" placeholder="12:00" value="{{ old('punch_out') }}" maxlength="50">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>General Project<span class="text-danger">*</span></label>

                                <select name="project_id">
                                    
                                    @foreach( $allProjects as $project )

                                    <option value="{{ $project->id }}"> {{ $project->job_name }} </option>

                                    @endforeach

                                </select>

                                <input type="hidden" name="user_id" value="{{ $userId }}">

                            </div>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Add Log</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">

$(document).ready(function() {

    /*
     * @Function Name
     *
     *
     */
    $('.timepicker').timepicker();

});

</script>

@endsection