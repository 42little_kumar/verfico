@extends('admin.layouts.app')

@section('title', 'Edit : ' . $user->first_name .' '. $user->last_name)

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Edit : {{ $user->first_name }} {{ $user->last_name }}</h1>

    <a href="{{ route('admin.users') }}?userType={{ $userRole }}" class="back-link">< Back to {{ getRoleTitle( $user->getRoleNames()[0] ) }}</a>
    
</div>

<form action="{{ route('admin.users.edit', [ 'id' => $user->id ]) }}" id="edit-user" method="POST" enctype="multipart/form-data">
            
    @csrf

    <input type="hidden" id="cropped-image" name="cropped-image">

    <input type="hidden" id="cropped-image-type" name="cropped-image-type">

    <div class="row">

        <div class="col-lg-4">

            <div class="card shadow mb-4">
        
                <div class="card-header py-3">
            
                   @if( $user->hasRole('Subcontractor') )

                    <h6 class="m-0 font-weight-bold text-primary">Company Logo</h6>

                    @else

                    <h6 class="m-0 font-weight-bold text-primary">Image</h6>

                    @endif
            
                </div>
            
                <div class="card-body text-center" id="update-profile-image">

                    @if($user->image)

                        <img src="{{ asset('uploads/images/users/' . $user->id . '/' . $user->image) }}" id="profile-image" alt="profile-image" class="img-profile rounded-circle" style="width: 150px; height: 150px;">

                        <hr>

                        <a href="{{ route('admin.users.remove', [ 'id' => $user->id ]) }}" class="btn btn-danger btn-remove-image btn-icon-split">

                            <span class="icon text-white-50">
                            
                                <i class="fas fa-times"></i>
                            
                            </span>
                            
                            <span class="text">Remove</span>
                        
                        </a>

                    @else
                        <div class="dropzone" id="myDropzone">

                            <img src="{{ asset('images/elements/user.png') }}" id="profile-image" alt="profile-image" class="img-profile rounded-circle" style="width: 150px; height: 150px;">

                            <hr>

                            <input type="file" name="image" accept="image/*" style="display:none">

                            <div class="dz-message"></div>
                            
                            <div>Drag and drop your file here.</div>

                        </div>
                        

                        <button id="change-profile-image" type="button" class="btn btn-info btn-icon-split">

                            <span class="icon text-white-50">
                            
                                <i class="far fa-image"></i>
                            
                            </span>
                            
                            <span class="text">Change</span>
                        
                        </button>

                        <button id="remove-profile-preview" style="display:none" type="button" class="btn btn-danger btn-icon-split">

                            <span class="icon text-white-50">
                            
                                <i class="fas fa-times"></i>
                            
                            </span>
                            
                            <span class="text">Remove</span>
                        
                        </button>

                    @endif

                </div>
            
            </div>
            
        </div>

        <div class="col-lg-8">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}First Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="first-name" placeholder="First Name" value="{{ $user->first_name }}" maxlength="50" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}Last Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="last-name" placeholder="Last Name" value="{{ $user->last_name }}" maxlength="50" required="">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}Email<span class="text-danger">*</span></label>

                                <input type="email" class="form-control" name="email" placeholder="Email Address" autocomplete="off" value="{{ $user->email }}">

                                <span id="error-email" class="error-message text-danger"></span>

                            </div>

                        </div>

                        {{--<div class="col-md-6">

                            <div class="form-group">

                                <label>Username<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="username" placeholder="Username" value="{{ $user->username }}" maxlength="25" required>

                                <span id="error-username" class="error-message text-danger"></span>

                            </div>

                        </div>--}}

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}Phone</label>
                                    
                                <div class="country_code"> 
                                
                                   

                                    <input type="text" class="form-control" name="phone-number" placeholder="Phone Number" value="{{ $user->phone_number }}" >
                            
                                </div> 
                            
                                <span id="error-phone-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        

                        <div class="col-md-6" style="display: none;">

                            <div class="form-group">

                                <label>Gender</label>

                                <select name="gender" class="form-control">

                                    <option value="">Select</option>

                                    <option value="male" @if($user->gender == 'male') selected @endif>Male</option>

                                    <option value="female" @if($user->gender == 'female') selected @endif>Female</option>

                                    <option value="others" @if($user->gender == 'others') selected @endif>Others</option>

                                </select>

                            </div>

                        </div>

                         <div class="col-md-6">

                            <div class="form-group">

                                <label>Status</label>

                                <select name="status" class="form-control">
                                
                                    <option value="2" @if($user->status == 2) selected @endif>Active</option>
                                    
                                    <option value="0" @if($user->status == 0) selected @endif>Blocked</option>

                                    <option value="1" @if($user->status == 1) selected @endif>Pending</option>

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Role</label>

                                <select name="user_role" class="form-control" id="role-select" required="">

                                    <option value="" disabled="">Select</option>

                                    <option value="admin" @if($user->hasRole('Admin')) selected @endif>Administrator</option>

                                    <option value="foreman" @if($user->hasRole('Foreman')) selected @endif>Foreman</option>

                                    <option value="subcontractor" @if($user->hasRole('Subcontractor')) selected @endif>Subcontractor</option>

                                    <option value="worker" @if($user->hasRole('Worker')) selected @endif>Worker</option>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                             <div class="form-group subcontractor-field" @if( $user->hasRole('Worker') ) style="display: block;" @else style="display: none;" @endif  >

                                 <label>Company<span class="text-danger">*</span></label>

                                 <select name="general_contractor" class="form-control" @if( $user->hasRole('Worker') ) required @endif>

                                     <option value="">Select</option>

                                     @foreach( $subContractors as $subContractor )                                    

                                     <?php

                                     $subContractorName = $subContractor->id; 

                                     $parent = $user->parent; 

                                     ?>

                                     <option  value="{{ $subContractor->id }}" @if( $subContractorName == $parent ) selected @endif>
                                             
                                             {{ $subContractor->first_name }}

                                     </option>  

                                     @endforeach

                                 </select>

                             </div>

                         </div>

                         <div class="col-md-6">

                            <div class="form-group worker-type-field" @if( !$user->hasRole('Worker') ) style="display: none;" @endif >

                                <label>Worker Type<span class="text-danger">*</span></label>

                                <?php $workerTypeSaved = $user->worker_type; ?>

                                <select name="worker_type" class="form-control" @if( $user->hasRole('Worker') ) required @endif>
                                    
                                    <option value="">Select</option>

                                    @foreach( $workerTypes as $workerType )

                                    <?php

                                    $workerTypeId = $workerType->id;

                                    ?>

                                     <option value="{{ $workerTypeId }}" @if( $workerTypeId == $workerTypeSaved ) selected @endif>{{ $workerType->type }}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                    </div>

                   

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Update</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">



$(document).ready(function() {

    /*
     * @Function Name
     *
     *
     */

     $(document).on('change', '#role-select', function(){ 

        $('.subcontractor-field').hide();

        $('.worker-type-field').hide();

        var role = $(this);

        var roleVal = role.val();

        if( roleVal == 'worker' )
        {

            $('.subcontractor-field').show();

            $('.worker-type-field').show();

        }

        if( roleVal == 'foreman' )
        {

            $('.subcontractor-field').show();

        }

     } );

    /*
     * @Function Name
     *
     *
     */
    $('#change-profile-image').on('click', function(event) {

        // $('#update-profile-image input[name="image"]').click();

        $('#myDropzone').click();

        event.preventDefault();

    });

    // $('#update-profile-image input[name="image"]').on('change', function(event) {

    //     previewImage(this, '#profile-image');

    //     $('#remove-profile-preview').show();

    // });

    $('#remove-profile-preview').on('click', function(event) {
        
        $('#cropped-image').val('');

        $('#cropped-image-type').val('');

        // $('#update-profile-image input[name="image"]').val('');

        $('#profile-image').attr('src', '{{ asset("images/elements/user.png") }}');

        $(this).hide();

        event.preventDefault();

    });

    $('input[name="phone-number"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });

    $('#edit-user').on('submit', function(event) {

        var error = false;

        $('.form-group .error-message').html('');

        var name = $('input[name="first-name"]').val();
        
        var email = $('input[name="email"]').val().toLowerCase();

        var phone_number = $('input[name="phone-number"]').val();
        
        var username = $('input[name="username"]').val();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter first name of user.');

        }

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('First name should contain atleast one alphabetic character.');
            
        }

        if(!email.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) {

            error = true;
            
            $('#error-email').html('Enter a valid email address of user.');
            
        }

        if(phone_number != '' && phone_number.replace(/[^0-9]/g, "").length != 10) {

            error = true;
            
            $('#error-phone-number').html('Please enter a valid phone number for user.');
            
        }

        if(username == '') {

            error = true;

            $('#error-username').html('Please enter username of user.');

        }

        if(username != '' && /\s/.test(username)) {

            error = true;
            
            $('#error-username').html('Username cannot contain spaces.');

        }

        if(username != '' && !/[a-zA-Z]/.test(username)) {

            error = true;

            $('#error-username').html('Username should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();

        }

    });

    $('.btn-remove-image').on('click', function(event) {

        if(!confirm('Are you sure you want to remove the image?')) {

            event.preventDefault();

        }

    });

}); 

var placeholder_img ="{{ asset('images/elements/user.png') }}";

Dropzone.options.myDropzone = {
    url: '/post',
    addRemoveLinks: true,
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png",
    disablePreview: false,

    init: function(file, done) {
        
    this.on("addedfile", function(event) {
        console.log('addedfile')
        if(this.files[0].type != 'image/png' && this.files[0].type != 'image/jpeg' && this.files[0].type != 'image/svg'){
            document.getElementsByClassName('dz-preview')[0].style.display = "none";
            alert("Please upload a image");
            this.removeFile(this.files[0]);
        }
      if (this.files[1]!=null){
        console.log('removeFile')
        this.removeFile(this.files[0]);
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
      }
      
    });
  },
    transformFile: function(file, done) {  
 
     
     
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
        // Create Dropzone reference for use in confirm button click handler
        var myDropZone = this;  
        
        // Create the image editor overlay
        var editor = document.createElement('div');
        editor.style.position = 'fixed';
        editor.style.left = '50px';
        editor.style.right = '50px';
        editor.style.top = '50px';
        editor.style.bottom = '50px';
        editor.style.zIndex = 9999;
        editor.style.width = '500px';
        editor.style.height = '500px';
        editor.className += "profile_image_cropper";
        editor.style.backgroundColor = '#000';
        document.body.appendChild(editor);  
        
        // Create confirm button at the top left of the viewport
        var buttonConfirm = document.createElement('button');
        buttonConfirm.style.position = 'absolute';
        buttonConfirm.style.left = '10px';
        buttonConfirm.style.top = '10px';
        buttonConfirm.style.zIndex = 9999;
        buttonConfirm.textContent = 'Confirm';
        editor.appendChild(buttonConfirm);

        var buttonCancel = document.createElement('button');
        buttonCancel.style.position = 'absolute';
        buttonCancel.style.left = '150px';
        buttonCancel.style.top = '10px';
        buttonCancel.style.zIndex = 9999;
        buttonCancel.textContent = 'Cancel';
        editor.appendChild(buttonCancel);

        buttonCancel.addEventListener('click', function() {  
                            // Remove the editor from the view
            document.body.removeChild(editor);  
            document.getElementById('profile-image').src = placeholder_img;


        });

        
        buttonConfirm.addEventListener('click', function() {    
            
            // Get the canvas with image data from Cropper.js
            var canvas = cropper.getCroppedCanvas({
                width: 256,
                height: 256
            });    

         
           var profile_img = document.getElementById('profile-image');
           profile_img.src = canvas.toDataURL();

           var image1 = document.getElementById('cropped-image');
           var imagetype = document.getElementById('cropped-image-type');
           image1.value=canvas.toDataURL();
           imagetype.value = file.type.substr(6); 

           

          
           
            


            // Turn the canvas into a Blob (file object without a name)
            canvas.toBlob(function(blob) { 


                // Create a new Dropzone file thumbnail
                myDropZone.createThumbnail(blob,
                myDropZone.options.thumbnailWidth,
                myDropZone.options.thumbnailHeight,
                myDropZone.options.thumbnailMethod,
                false, 
                function(dataURL) {
                
                        // Update the Dropzone file thumbnail
                        myDropZone.emit('thumbnail', file, dataURL);          
                        // Return the file to Dropzone
                        done(blob);      
                    });    
                });   
                // Remove the editor from the view
                document.body.removeChild(editor);  
              $('#remove-profile-preview').show();
               


            
            });  
            
            // Create an image node for Cropper.js
            var image = new Image();
            image.src = URL.createObjectURL(file);

            editor.appendChild(image);
    
            // Create Cropper.js
            var cropper = new Cropper(image, { aspectRatio: 1 });
            
            }
    

};

</script>

@endsection