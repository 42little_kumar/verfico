@extends('admin.layouts.app')

@section('title', 'Staff Roles')

@section('content')

@if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write roles'))

    @include('admin.roles.includes.create')

@endif

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Staff Roles</h1>

    @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write roles'))

    <a href="javascript:void(0);" class="float-right btn-create" data-toggle="modal" data-target="#create-role">

        <i class="fas fa-plus-circle"></i>

    </a>

    @endif
    
</div>

<div class="row justify-content-center">

    <div class="col-md-12">

        <div class="card shadow mb-4">

            <div class="card-header text-right filter-header-wrap">

                <form method="GET" id="page-filter-form" class="justify-content-between">
                   
                   <div class="fliter_wrap_left">

                        <div class="input-group">
                
                            <select class="selectpicker" name="sort-by">
                    
                                <option value="">-Sort By-</option>

                                <option value="name-ASC" @if(request()->query('sort-by') && request()->query('sort-by') == 'name-ASC') selected @endif>Name (Ascending)</option>

                                <option value="name-DESC" @if(request()->query('sort-by') && request()->query('sort-by') == 'name-DESC') selected @endif>Name (Descending)</option>                        
                    
                            </select>
                
                        </div>
                   
                        <div class="input-group">
                           
                            <input type="text" name="search" class="form-control bg-light border-0 small" value="{{ request()->query('search') }}" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                           
                        </div>
               
                        <div class="input-group" style="width:auto;">
                       
                            <div class="input-group-append">
                       
                                <button class="btn btn-primary" type="submit">
                           
                                    <i class="fas fa-search fa-sm"></i> Search

                                </button>
                       
                            </div>


                            @if(request()->query('sort-by') != '' || request()->query('search') != '')

                            <div class="reset-group">

                                <a class="btn-filter-reset" href="{{ route('admin.staff.roles') }}"><i class="fas fa-sync-alt fa-sm"></i> Reset</a>

                            </div>

                            @endif
                       
                        </div>
               
                    </div>
                  
                    <div class="line_limit_wrap">
                   
                        <div class="input-group" style="width:78px;" title="Select Limit">
                   
                            <select name="limit">
                       
                                <option value="10" @if(request()->query('limit') && request()->query('limit') == 10) selected @endif>10</option>

                                <option value="25" @if(request()->query('limit') && request()->query('limit') == 25) selected @endif>25</option>

                                <option value="50" @if(request()->query('limit') && request()->query('limit') == 50) selected @endif>50</option>

                                <option value="75" @if(request()->query('limit') && request()->query('limit') == 75) selected @endif>75</option>

                                <option value="100" @if(request()->query('limit') && request()->query('limit') == 100) selected @endif>100</option>

                            </select>
               
                        </div>
               
                    </div>

                </form>
            
            </div>
            
            <div class="card-body">

                <table class="table table-bordered table-hover">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Name</th>
        
                            <th scope="col">No. of users</th>

                            @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write roles'))
    
                            <th scope="col" class="text-center">Action</th>

                            @endif
        
                        </tr>
  
                    </thead>
                    
                    <tbody>

                        @if($roles->isEmpty())

                            <tr>

                                <td colspan="3" class="text-center">No records found.</td>

                            </tr>

                        @else

                            @foreach($roles as $role)

                            <tr class="@if(Session::has('status-role') && $role->id == Session::get('status-role')) focused_row  @endif">

                                <td>{{ $role->name }}</td>

                                <td>{{ $role->users()->count() }}</td>

                                @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write roles'))

                                <td class="text-center">

                                    <div class="table-action">

                                        <a href="{{ route('admin.staff.roles.edit', [ 'id' => $role->id ]) }}" class="btn btn-sm btn-primary btn-circle" title="Edit">
                    
                                            <i class="fas fa-pencil-alt"></i>
                    
                                        </a>

                                        <a href="{{ route('admin.staff.roles.delete', [ 'id' => $role->id ]) }}" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
                        
                                            <i class="fas fa-trash"></i>
                    
                                        </a>

                                    </div>

                                </td>

                                @endif

                            </tr>

                            @endforeach

                        @endif

                    </tbody>

                </table>

                @if($roles->isNotEmpty())

                    <div class="pagination-wrap">

                        <span class="entries float-right" style="margin-top: 7px">

                            Showing {{ $roles->firstItem() }} to {{ $roles->lastItem() }} of total {{ $roles->total() }} entries

                        </span>

                        @php
                        
                            $paginate = [];

                            if(request()->query('search')) {

                                $paginate['search'] = request()->query('search');

                            }

                            if(request()->query('limit')) {

                                $paginate['limit'] = request()->query('limit');

                            }

                            if(request()->query('sort-by')) {

                            $paginate['sort-by'] = request()->query('sort-by');

                            }

                        @endphp

                        {{ $roles->appends($paginate)->links() }}

                        <div class="clearfix"></div>

                    </div>

                @endif

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

$(document).ready(function() {

    $('#create-role form').on('submit', function(event) {

        var error = false;

        $('#create-role .form-group .error-message').html('');

        var name = $('#create-role input[name="name"]').val().trim();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter name of role.');

        }

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('Role name should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();

        }

    });

    $('.btn-action-delete').on('click', function(event) {

        if(!confirm('Are you sure you want to delete this role?')) {

            event.preventDefault();

        }

    });

    @if(Session::has('open-modal'))
    
        $('a[data-target="#create-role"').click();

    @endif

});

</script>

@endsection