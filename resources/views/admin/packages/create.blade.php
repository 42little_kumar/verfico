@extends('admin.layouts.app')

@section('title', 'New Package')

@section('styles')

<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">New Project</h1>

    <a href="" class="back-link">< Back to Project</a>
    
</div>

<div class="row justify-content-center">

    <div class="col-lg-12">

        <form action="{{ route('admin.packages.create') }}" id="create-package" method="POST">

            @csrf

            <div class="card shadow mb-4">

                <div class="card-header">
                
                    <h6 class="m-0 font-weight-bold text-primary">Project Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Create</span>
    
                    </button>

                </div>

            </div>

        </form>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

    

</script>

@endsection