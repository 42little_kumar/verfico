@extends('admin.layouts.app')

@section('title', 'Packages')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }

    </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Packages</h1>

    @if(auth()->user()->hasRole('Superadmin') || auth()->user()->can('write packages'))

    <a href="{{ route('admin.packages.create') }}" class="float-right btn-create">

        <i class="fas fa-plus-circle"></i>

    </a>

    @endif
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            <div class="card-header text-right filter-header-wrap">

                
            
            </div>

            <div class="card-body">
            
                <table class="table table-bordered table-hover" id="datatable">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Project Name</th>
        
                            <th scope="col">Project Name</th>
                
                            <th scope="col" class="text-center">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript">


    $('#datatable').DataTable({

        processing: true,

        serverSide: true,

        searching: false,

        lengthChange: false,

        pageLength: limit,

        order: [],

        ajax: {

            url: "{{ route('admin.packages') }}",

            dataType: 'JSON',

            data: { 'status' : status, 'search' : search }

        },

        columns: [

            { data: "project_name", name: "project_name", searchable: true, orderable: true },
            
            { data: "project_description", name: "description", searchable: false, orderable: false },
            
            { data: "action", name: "action", searchable: false, orderable: false },

        ],

    });



  

</script>

@endsection