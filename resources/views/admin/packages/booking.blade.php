@extends('admin.layouts.app')

@section('title', 'Book Package: ' . $package->title)

@section('styles')

<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800"></h1>
    
    <a href="{{ route('admin.packages') }}" class="back-link">< Back to Packages</a>
    
</div>

<div class="row justify-content-center">

    <div class="col-lg-12">

        <form action="{{ route('admin.packages.booking', [ 'id' => $package->id ]) }}" id="book-package" method="POST">

            @csrf

            <div class="card shadow mb-4">

                <div class="card-header">
                
                    <h6 class="m-0 font-weight-bold text-primary">Book Package: {{ $package->title }}</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        @foreach($sessions as $session)
                        
                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Availability {{ $session->title }}<span class="text-danger">*</span></label>

                                <select name="availability[{{ $session->id }}]" class="form-control session-slots" required>

                                    <option value="">Select availability slot</option>

                                    @foreach($availabilities as $key => $slots)

                                    <optgroup label="{{ $key }}">

                                        @foreach($slots as $slot)
                                        
                                        <option title="{{ $key }} ({{ $slot['interval'] }})" value="{{ $slot['id'] }}" @if($session->booking_details && $session->booking_details->id == $slot['id']) selected @endif>{{ $slot['interval'] }}</option>

                                        @endforeach

                                    </optgroup>

                                    @endforeach

                                </select>

                                <span id="error-availability" class="error-message text-danger"></span>

                            </div>

                        </div>

                        @endforeach

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>User<span class="text-danger">*</span></label>

                                <select name="user" class="form-control" required>

                                    <option value="">Select User</option>

                                    @foreach($users as $user)
                                    
                                    <option title="{{ $user->first_name }} {{ $user->last_name }}" value="{{ $user->id }}" @if($session->users()->where('user_id', $user->id)->first()) selected @endif>{{ $user->first_name }} {{ $user->last_name }} ({{ $user->email }})</option>

                                    @endforeach

                                </select>

                                <span id="error-users" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Book</span>
    
                    </button>

                </div>

            </div>

        </form>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

    $(document).ready(function() {

        $('#book-package').on('submit', function(event) {

            var inputs = $( this ).serializeArray();

            var slots = [];

            var error = false;

            $.each(inputs, function(index, value) {

                if(value.name.includes('availability')) {

                    if(!slots.includes(value.value)) {

                        slots.push(value.value);

                    } else {

                        error = true;

                    }                

                }
                
            });

            if(error) {

                toastr.error("Two or more sessions can not have same time slot.");

                event.preventDefault();

            }

        });

    });

</script>

@endsection