@extends('admin.layouts.app')

@section('title', 'Edit Package: ' . $package->title)

@section('styles')

<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Edit Package: {{ str_limit($package->title, 30) }}</h1>
    
    <a href="{{ route('admin.packages') }}" class="back-link">< Back to Packages</a>
    
</div>

<div class="row justify-content-center">

    <div class="col-lg-12">

        <form action="{{ route('admin.packages.edit', [ 'id' => $package->id ]) }}" id="edit-package" method="POST">

            @csrf

            <div class="card shadow mb-4">

                <div class="card-header">
                
                    <h6 class="m-0 font-weight-bold text-primary">Package Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Title<span class="text-danger">*</span></label>

                                <input type="text" name="title" placeholder="Package title" maxlength="50" class="form-control" value="{{ $package->title }}" autocomplete="off" required>

                                <span id="error-title" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Trainer<span class="text-danger">*</span></label>

                                <select name="trainer" class="form-control" required>

                                    <option value="">Select Trainer</option>

                                    @foreach($trainers as $trainer)

                                    <option value="{{ $trainer->id }}" @if($package->trainer && $package->trainer->id == $trainer->id) selected @endif>{{ $trainer->first_name }} {{ $trainer->last_name }} ({{ $trainer->email }})</option>

                                    @endforeach

                                </select>

                                <span id="error-trainer" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Category<span class="text-danger">*</span></label>

                                <select name="category[]" class="form-control" multiple required>

                                    @foreach($categories as $category)

                                    <option value="{{ $category->slug }}"@if($package->categories()->where('category_id', $category->id)->first()) selected @endif>{{ $category->name }}</option>

                                    @endforeach

                                </select>

                                <span id="error-category" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>No. of Session(s)<span class="text-danger">*</span></label>

                                <input type="number" name="sessions" class="form-control" min="1" placeholder="Number of session(s)" value="{{ $package->quantity }}" autocomplete="off" required>

                                <span id="error-sessions" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Duration<span class="text-danger">*</span></label>

                                <select name="duration" class="form-control" required>

                                    <option value="">Select Duration</option>

                                    <option value="30" @if($package->duration == "30") selected @endif>30 Minutes</option>
                                    
                                    <option value="45" @if($package->duration == "45") selected @endif>45 Minutes</option>
                                    
                                    <option value="60" @if($package->duration == "60") selected @endif>60 Minutes</option>

                                </select>

                                <span id="error-duration" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Price<span class="text-danger">*</span></label>

                                <input type="number" name="price" placeholder="Package price" class="form-control" min="0.01" step="0.01" maxlength="10" value="{{ $package->price }}" autocomplete="off" required>

                                <span id="error-price" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Intensity level</label>

                                <select name="intensity-level" class="form-control">

                                    <option value="">-Select-</option>

                                    <option value="Low" @if($metas->where('name', 'intensity-level')->where('value', 'Low')->first()) selected @endif>Low</option>
                                    
                                    <option value="Moderate" @if($metas->where('name', 'intensity-level')->where('value', 'Moderate')->first()) selected @endif>Moderate</option>
                                    
                                    <option value="High" @if($metas->where('name', 'intensity-level')->where('value', 'High')->first()) selected @endif>High</option>

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Fitness level</label>

                                <select name="fitness-level" class="form-control">

                                    <option value="">-Select-</option>

                                    <option value="Low" @if($metas->where('name', 'fitness-level')->where('value', 'Low')->first()) selected @endif>Low</option>
                                    
                                    <option value="Moderate" @if($metas->where('name', 'fitness-level')->where('value', 'Moderate')->first()) selected @endif>Moderate</option>
                                    
                                    <option value="High" @if($metas->where('name', 'fitness-level')->where('value', 'High')->first()) selected @endif>High</option>

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Gender preferability</label>

                                <select name="gender-preferability" class="form-control">

                                    <option value="">-Select-</option>

                                    <option value="Male" @if($metas->where('name', 'gender-preferability')->where('value', 'Male')->first()) selected @endif>Male</option>
                                    
                                    <option value="Female" @if($metas->where('name', 'gender-preferability')->where('value', 'Female')->first()) selected @endif>Female</option>
                                    
                                    <option value="Both" @if($metas->where('name', 'gender-preferability')->where('value', 'Both')->first()) selected @endif>Both</option>

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Validity (in days)<span class="text-danger">*</span></label>

                                <input type="number" name="validity" placeholder="Package validity" min="1" class="form-control" maxlength="10" value="@if($package->validity){{ $package->validity }}@endif" autocomplete="off" required>

                                <span id="error-validity" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Applicable Coupons</label>

                                <select name="coupon[]" class="form-control" multiple @if($coupons->isEmpty()) disabled @endif>

                                    @foreach($coupons as $coupon)

                                    <option title="{{ $coupon->code }}" value="{{ $coupon->code }}" @if($package->coupons()->where('coupon_id', $coupon->id)->first()) selected @endif>{{ $coupon->code }} (Discount: {{ $coupon->type == 'percentage' ? $coupon->value . '%' : '$' . $coupon->value }})</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Status</label>

                                <select name="status" class="form-control">

                                    <option value="active" @if($package->status) selected @endif>Active</option>
                                    
                                    <option value="inactive" @if(!$package->status) selected @endif>Inactive</option>

                                </select>

                            </div>

                        </div>

                        <div class="col-md-12">
                        
                            <div class="form-group">

                                <label>Description</label>

                                <textarea name="description" placeholder="Package description" rows="5" class="form-control"autocomplete="off">{{ $package->description }}</textarea>

                            </div>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Update</span>
    
                    </button>

                </div>

            </div>

        </form>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

    $(document).ready(function() {

        $('input[name="validity"], input[name="sessions"]').on('keyup', function(event) {

            var value = $(this).val().replace(/\D/g,'');

            if(value.length > 10) {

                $(this).val(value.substr(0, 10));

            } else {

                $(this).val(value);

            }
            
        });

        $('input[name="price"]').on('keyup', function(event) {

            var value = $(this).val();

            var amount = parseAmount(value.trim());

            if (amount) {

                $(this).val(amount);

            } else {

                $(this).val('');

            }
            
        });

        $('#edit-package').on('submit', function(event) {

            var error = false;

            $('.form-group .error-message').html('');

            var title = $('input[name="title"]').val();
            
            var trainer = $('select[name="trainer"] option:selected').val();

            var category = $('select[name="category[]"] option:selected').val();
            
            var duration = $('select[name="duration"] option:selected').val();
            
            var price = $('input[name="price"]').val();
            
            var validity = $('input[name="validity"]').val();

            if(title.trim() == '') {

                error = true;

                $('#error-title').html('Please enter title of package');

            }

            if($.isNumeric(title)) {

                error = true;

                $('#error-title').html('Title should contain atleast one alphabetic character');
                
            }

            if(trainer.trim() == '') {

                error = true;

                $('#error-trainer').html('Please specify trainer of the package');

            }

            if(category.trim() == '') {

                error = true;

                $('#error-category').html('Please select atleast one category for the package');

            }

            if(duration.trim() == '') {

                error = true;

                $('#error-duration').html('Please select duration of the package');

            }

            if(price.trim() == '') {

                error = true;

                $('#error-price').html('Please enter price of package');

            }

            if(!price.match(/^\d+(\.\d{1,2})?$/)) {

                error = true;

                $('#error-price').html('Please enter a valid price of package');

            }

             if(validity.trim() == '') {

                error = true;

                $('#error-validity').html('Please enter validity of package');

            }

            if(!$.isNumeric(validity)) {

                error = true;
                
                $('#error-validity').html('Please enter a valid validity of package');
                
            }

            if(error) {

                event.preventDefault();

            }

        });

    });

</script>

@endsection