@extends('admin.layouts.app')

@section('title', 'Profile')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Profile</h1>
    
</div>

<div class="row">

    <div class="col-lg-4">

        <div class="card shadow mb-4">
        
            <div class="card-header py-3">
            
                <h6 class="m-0 font-weight-bold text-primary">Image</h6>
            
            </div>
            
            <div class="card-body text-center">

                @php
                
                    if(auth()->user()->image) {

                        $user_image = asset('uploads/images/users/' . auth()->user()->id . '/' . auth()->user()->image);

                    } else {

                        $image = 'user';

                        if(auth()->user()->gender == 'male' || auth()->user()->gender == 'female') {

                            $image .= '-' . auth()->user()->gender;

                        }

                        $image .= '.png';

                        $user_image = asset('images/elements/' . $image);

                    }
                
                @endphp

              



                <form action="{{ route('admin.profile.image') }}" method="POST" id="update-profile-image" enctype="multipart/form-data">
            
                    @csrf

   

                    <div class="dropzone" id="myDropzone">

                    <img src="{{ $user_image }}" alt="profile-image" id="profile-image" class="img-profile rounded-circle" style="width: 150px; height: 150px;">

                    <hr>

                    <input type="file" name="image" accept="image/*" style="display:none" required>


                    <div class="dz-message"></div>
                        
                    <div >Drag and drop your file here.</div>

                    </div>

                    <button id="change-profile-image" type="button" class="btn btn-info btn-icon-split">

                        <span class="icon text-white-50">
                        
                            <i class="far fa-image"></i>
                        
                        </span>
                        
                        @if(auth()->user()->image)

                        <span class="text">Change</span>

                        @else

                        <span class="text add-text">Add</span>

                        @endif
                        
                    
                    </button>

                    @if(auth()->user()->image)  

                    <a onclick="if (!confirm('Are you sure to delete user image?')) { return false }" href="{{ route('admin.users.remove', [ 'id' => $user->id ]) }}" class="btn btn-danger btn-remove-image btn-icon-split">

                            <span class="icon text-white-50">
                            
                                <i class="fas fa-times"></i>
                            
                            </span>
                            
                            <span class="text">Remove</span>
                        
                        </a>

                    @endif

                    @if(!auth()->user()->image)  

                    <button id="remove-profile-preview" style="display:none" type="button" class="btn btn-danger btn-icon-split">

                        <span class="icon text-white-50">
                        
                            <i class="fas fa-times"></i>
                        
                        </span>
                        
                        <span class="text">Remove</span>
                    
                    </button>

                    @endif

                </form>
            
            </div>
            
        </div>

    </div>

    <div class="col-lg-8">

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                
                <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
            
            </div>

            <div class="card-body">

                <form class="personal-information" method="POST" id="update-profile" action="{{ route('admin.profile') }}" enctype="multipart/form-data">

                    @csrf

                    <input type="hidden" id="cropped-image" name="cropped-image">

                    <input type="hidden" id="cropped-image-type" name="cropped-image-type">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>First Name</label>

                                <input type="text" class="form-control" name="first-name" placeholder="First Name" value="{{ $user->first_name }}" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Last Name</label>

                                <input type="text" class="form-control" name="last-name" placeholder="Last Name" value="{{ $user->last_name }}">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Phone Number</label>

                                <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" value="{{ $user->phone_number }}">

                                <span id="error-phone-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <!-- <div class="col-md-6">

                            <div class="form-group">

                                <label>Gender</label>

                                <select class="form-control" name="gender">

                                    <option value="">Select</option>

                                    <option value="male" @if($user->gender == 'male') selected @endif>Male</option>
    
                                    <option value="female" @if($user->gender == 'female') selected @endif>Female</option>
            
                                    <option value="others" @if($user->gender == 'others') selected @endif>Others</option>

                                </select>

                            </div>

                        </div> -->

                    <div class="col-md-6">

                        <div class="form-group">

                            <label>Email Address</label>

                            <input type="email" class="form-control" name="email" placeholder="Email Address" value="{{ $user->email }}">

                        </div>

                    </div>

                    @if( $user->hasRole('Superadmin') )

                    <div class="col-md-6">

                        <div class="form-group">

                            <label>Timezone</label>

                            <select class='form-control' name='timezone_manual'>

                            @foreach( getTimezoneSelectOptions( $user->timezone_manual ) as $option )

                            {!! $option !!}

                            @endforeach

                            </select>

                        </div>

                    </div>

                    @endif

                    </div>


                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Update</span>
    
                    </button>

                </form>

            </div>

        </div>

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                
                <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
            
            </div>

            <div class="card-body">

                <form class="change-password" method="POST" action="{{ route('admin.profile.password') }}">

                    @csrf

                    <div class="form-group">

                        <label>Old Password<span class="text-danger">*</span></label>

                        <input type="password" class="form-control" name="old-password" placeholder="Old Password" autocomplete="off" required>

                    </div>

                    <div class="form-group">

                        <label>New Password<span class="text-danger">*</span></label>

                        <input type="password" class="form-control" name="new-password" placeholder="New Password" autocomplete="off" required>

                    </div>

                    <div class="form-group">

                        <label>Confirm New Password<span class="text-danger">*</span></label>

                        <input type="password" class="form-control" name="confirm-new-password" placeholder="Confirm New Password" autocomplete="off" required>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Change Password</span>
    
                    </button>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">

$(document).ready(function() {

    $('#change-profile-image').on('click', function(event) {
        // $('#update-profile-image input[name="image"]').click();
        $('#myDropzone').click();

        // $('#remove-profile-preview').show();

        event.preventDefault();

    });

    /* Phone number masking */

    $('input[name="phone_number"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });

    /* On form submit */

    $('#update-profile').on('submit', function(event) {

        var error = false;

        $('.form-group .error-message').html('');

        var phone_number = $('input[name="phone_number"]').val();

        if(phone_number != '' && phone_number.replace(/[^0-9]/g, "").length != 10) {

            error = true;

            $('#error-phone-number').html('Please enter a valid phone number.');

        }

        if(error) {

            event.preventDefault();

        }

    });

    // $('#update-profile-image input[name="image"]').on('change', function(event) {

    //     previewImage(this, '#profile-image');

    //     $('#update-profile-image button[type="submit"]').show();

    // });

    $('#remove-profile-preview').on('click', function(event) {

        $('.add-text').text('Add');

        // $('#update-profile-image input[name="image"]').val('');
        $('#cropped-image').val('');

        $('#cropped-image-type').val('');

        $('#update-profile-image input[name="image"]').val('');

        $('#profile-image').attr('src', '{{ asset("images/elements/user.png") }}');

        $(this).hide();

        event.preventDefault();

    });

})

Dropzone.options.myDropzone = {
    url: '/post',
    addRemoveLinks: true,
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png",
    disablePreview: false,

    init: function(file, done) {
        
    this.on("addedfile", function(event) {
        console.log('addedfile')
        if(this.files[0].type != 'image/png' && this.files[0].type != 'image/jpeg' && this.files[0].type != 'image/svg'){
            document.getElementsByClassName('dz-preview')[0].style.display = "none";
            alert("Please upload a image");
            this.removeFile(this.files[0]);
        }
      if (this.files[1]!=null){
        console.log('removeFile')
        this.removeFile(this.files[0]);
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
      }
      
    });
  },
    transformFile: function(file, done) {  
 
     
     
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
        // Create Dropzone reference for use in confirm button click handler
        var myDropZone = this;  
        
        // Create the image editor overlay
        var editor = document.createElement('div');
        editor.style.position = 'fixed';
        editor.style.left = '50px';
        editor.style.right = '50px';
        editor.style.top = '50px';
        editor.style.bottom = '50px';
        editor.style.zIndex = 9999;
        editor.style.width = '500px';
        editor.style.height = '500px';
        editor.className += "profile_image_cropper";
        editor.style.backgroundColor = '#000';
        document.body.appendChild(editor);  
        
        // Create confirm button at the top left of the viewport
        var buttonConfirm = document.createElement('button');
        buttonConfirm.style.position = 'absolute';
        buttonConfirm.style.left = '10px';
        buttonConfirm.style.top = '10px';
        buttonConfirm.style.zIndex = 9999;
        buttonConfirm.textContent = 'Confirm';
        editor.appendChild(buttonConfirm);

        var buttonCancel = document.createElement('button');
        buttonCancel.style.position = 'absolute';
        buttonCancel.style.left = '150px';
        buttonCancel.style.top = '10px';
        buttonCancel.style.zIndex = 9999;
        buttonCancel.textContent = 'Cancel';
        editor.appendChild(buttonCancel);

        buttonCancel.addEventListener('click', function() {  
                            // Remove the editor from the view
            document.body.removeChild(editor);  
            document.getElementById('profile-image').src = placeholder_img;


        });

        
        buttonConfirm.addEventListener('click', function() {    
            
            // Get the canvas with image data from Cropper.js
            var canvas = cropper.getCroppedCanvas({
                width: 256,
                height: 256
            });    

         
           var profile_img = document.getElementById('profile-image');
           profile_img.src = canvas.toDataURL();

           var image1 = document.getElementById('cropped-image');
           var imagetype = document.getElementById('cropped-image-type');
           image1.value=canvas.toDataURL();
           imagetype.value = file.type.substr(6); 

           

          
           
            


            // Turn the canvas into a Blob (file object without a name)
          /*  canvas.toBlob(function(blob) { 


                // Create a new Dropzone file thumbnail
                myDropZone.createThumbnail(blob,
                myDropZone.options.thumbnailWidth,
                myDropZone.options.thumbnailHeight,
                myDropZone.options.thumbnailMethod,
                false, 
                function(dataURL) {
                
                        // Update the Dropzone file thumbnail
                        myDropZone.emit('thumbnail', file, dataURL);          
                        // Return the file to Dropzone
                        done(blob);      
                    });    
                }); */  
                // Remove the editor from the view
                document.body.removeChild(editor);  
              $('.add-text').text('Change');
              $('#remove-profile-preview').show();
               
done();

            
            });  
            
            // Create an image node for Cropper.js
            var image = new Image();
            image.src = URL.createObjectURL(file);

            editor.appendChild(image);
    
            // Create Cropper.js
            var cropper = new Cropper(image, { aspectRatio: 1 });
            
            }
    

};


</script>

@endsection