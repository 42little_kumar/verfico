@extends('admin.layouts.app')

@section('title', 'Edit : ' . $user->first_name .' '. $user->last_name)

@section('styles')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">

<style type="text/css">
        
    i.fa.fa-times.close-document-button {
    position: absolute;
    top: 10px;
    right: 29px;
    border: 1px solid;
    padding: 5px 8px;
    border-radius: 50%;
}

.sub-container .col-md-12
{
    margin-bottom: 10px;
}
.flex_wrap{
 display:flex;
 align-items:center; 
}

span#ssn-id {
    background: #f2f2f2;
    padding: 11px 12px;
    color: #000 !important;
    font-weight: bold;
    border-radius: 5px;
    margin: 0 6px 0 0;
    font-size: 14px;
}

.current-uploaded-document .title {
    background: #fff;
    color: #000;
    margin-bottom: 18px;
    padding: 0 10px;
    margin-top: 9px;
    font-size: 18px;
    font-weight: bold;
    border-bottom: 1px solid #ddd;
    padding: 10px 0 9px;
}


.add-more {
    background: #3b7642;
    color: white;
    margin-bottom: 12px;
    padding: 0 10px;
    margin-top: 7px;
}

.add-more a {
    color: white;
}

i.fa.fa-times.delete-document {
    background: red;
    color: white;
    padding: 2px 5px;
    border-radius: 50%;
}

.col-md-12.additional-notedoc-element {
    margin: 20px 0 0;
    padding: 0;
    display: flex;
    align-items: center;
	position:relative;
}

.col-md-12.add-more a{
    display:block;
    align-items: center;
    justify-content: center;
    padding: 11px;
    background: #103359;
    font-size: 15px;
    text-transform: uppercase;
    font-weight: 600;
    border-radius: 50px;
	text-align:center;
}
.col-md-12.add-more a i{
margin-right:3px;	
}
.col-md-12.add-more{
 background:none !important;	
}

a.document-close {
    position: absolute;
    right: -15px;
    top: 9px;
    color: #ff0000;
    font-weight: bold;
    font-size: 18px;
}

.col-md-12.additional-notedoc-element textarea.form-control, .small_textarea textarea.form-control{
    height: 47px !important;
}
p.visible_text {
    font-weight: bold;
    font-size: 11px;
    margin: 0 0 7px;
    width: 100%;
}
span.uploaded-by-text {
    font-size: 12px;
    font-weight: bold;
    display: block;
}
.switch_wrap .switch {
    float: right;
    margin-right: 55px;
}
a.document-close.close_sub_doc {
    top: -3px;
    right: -9px;
    background: #fff;
    padding: 0;
    font-size: 20px;
}
a.document-close.close_sub_doc i.fa.fa-times.close-document-button {
    position: absolute;
    top: 9.2px;
    right: 29px;
}
</style>

<link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Edit : {{ $user->first_name }} {{ $user->last_name }} {{ !empty($user->classification) ? ' - '.$user->classification:'' }}</h1>
    <?php 
    $type = $text = "worker";
    if($user->role == "worker"){
        $type = $text = "worker";
    }elseif($user->role == "foreman"){
        $type = $text = 'foreman';
    }elseif($user->role == "safetyo"){
        $type = 'safetyo';
        $text = 'Safety Officers';
    }

    ?>
    <a href="@if( isset($_GET['wr']) ){{ route('admin.workers.requests') }}@else{{ route('admin.users') }}?userType={{ strtolower( $type ) }}@endif" class="back-link">< @if( isset($_GET['wr']) ) Worker Requests @else Back to {{ ucwords($text) }}@endif</a>
    
</div>

<form action="{{ route('admin.users.edit', [ 'id' => $user->id ]) }}" id="edit-user" method="POST" enctype="multipart/form-data">
            
    @csrf

    <input type="hidden" id="cropped-image" name="cropped-image">

    <input type="hidden" id="cropped-image-type" name="cropped-image-type">

    <div class="row">

        <div class="col-lg-4">

            <div class="card shadow mb-4">
        
                <div class="card-header py-3">
            
                   @if( $user->hasRole('Subcontractor') )

                    <h6 class="m-0 font-weight-bold text-primary">Company Logo</h6>

                    @else

                    <h6 class="m-0 font-weight-bold text-primary">Image</h6>

                    @endif
            
                </div>
            
                <div class="card-body text-center" id="update-profile-image">

                    @if($user->image)
                        <div id="profile-image" class="profile-img" style="background:url({{ asset('uploads/images/users/' . $user->id . '/' . $user->image) }}); background-size:cover; background-position:center;">
					    </div> 
                        
                        <hr>

                        <a href="{{ route('admin.users.remove', [ 'id' => $user->id ]) }}" class="btn btn-danger btn-remove-image btn-icon-split">

                            <span class="icon text-white-50">
                            
                                <i class="fas fa-times"></i>
                            
                            </span>
                            
                            <span class="text">Remove</span>
                        
                        </a>

                    @else
                        <div class="dropzone" id="myDropzone">

                            <img src="{{ asset('images/elements/user.png') }}" id="profile-image" alt="profile-image" class="img-profile rounded-circle" style="width: 150px; height: 150px;">

                            <hr>

                            <input type="file" name="image" accept="image/*" style="display:none">

                            <div class="dz-message"></div>
                            
                            <div>Drag and drop your file here.</div>

                        </div>
                        

                        <button id="change-profile-image" type="button" class="btn btn-info btn-icon-split">

                            <span class="icon text-white-50">
                            
                                <i class="far fa-image"></i>
                            
                            </span>
                            
                            <span class="text add-text">Add</span>
                        
                        </button>

                        <button id="remove-profile-preview" style="display:none" type="button" class="btn btn-danger btn-icon-split">

                            <span class="icon text-white-50">
                            
                                <i class="fas fa-times"></i>
                            
                            </span>
                            
                            <span class="text">Remove</span>
                        
                        </button>

                    @endif

                </div>
            
            </div>
            
        </div>

        <div class="col-lg-8">

        
<?php
            /*@php

            $next = \App\User::role('worker')->where('status', 2)->where('id', '<', $user->id)->orderBy('id','desc')->first();

            $previous = \App\User::role('worker')->where('status', 2)->where('id', '>', $user->id)->orderBy('id','asc')->first();
            
            @endphp*/
			?>


      

            <div class="next-prev-navigation">

                <div class="prev" >

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('admin.users.edit', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?><?php if( isset( $_GET['wr'] ) ): ?>&wr=yes<?php endif; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('admin.users.edit', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?><?php if( isset( $_GET['wr'] ) ): ?>&wr=yes<?php endif; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls" style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>


            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
                
                </div>

                <div class="card-body">

                   @if( $user->hasRole('Subcontractor') )

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Company Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="company-name" placeholder="Company Name" value="{{ $user->company_name }}" maxlength="50" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        </div>

                        <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll First Name</label>

                                <input type="text" class="form-control" name="payroll_first_name" placeholder="Payroll First Name" value="{{ $user->payroll_first_name }}" maxlength="50" >

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll Last Name</label>

                                <input type="text" class="form-control" name="payroll_last_name" placeholder="Payroll Last Name" value="{{ $user->payroll_last_name }}" maxlength="50" >

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll Email</label>

                                <input type="email" class="form-control" name="payroll_email" placeholder="Payroll Email" value="{{ $user->payroll_email }}" maxlength="50" >

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll Phone</label>

                                <input type="text" class="form-control" name="payroll_phone" placeholder="Payroll Phone" value="{{ $user->payroll_phone }}" maxlength="50" >

                            </div>

                        </div>

                    </div>

                    @endif

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}First Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="first-name" placeholder="First Name" value="{{ $user->first_name }}" maxlength="50" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}Last Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="last-name" placeholder="Last Name" value="{{ $user->last_name }}" maxlength="50" required="">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">
 
                                <label>{{ $fieldPrefix }}{{ 'Email' }}@if( !$user->hasRole('Worker') )<span class="text-danger">*</span>@endif</label>

                                <input type="email" class="form-control" name="email" placeholder="Email Address" autocomplete="off" value="{{ $user->email }}"

                                @if( !$user->hasRole('Worker') ) required="" @endif

                                >

                                <span id="error-email" class="error-message text-danger"></span>

                            </div>

                        </div>

                        {{--<div class="col-md-6">

                            <div class="form-group">

                                <label>Username<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="username" placeholder="Username" value="{{ $user->username }}" maxlength="25" required>

                                <span id="error-username" class="error-message text-danger"></span>

                            </div>

                        </div>--}}

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}Phone</label>
                                    
                                <div class="country_code"> 
                                
                                   

                                    <input type="text" class="form-control" name="phone-number" placeholder="Phone Number" value="{{ $user->phone_number }}" >
                            
                                </div> 
                            
                                <span id="error-phone-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                        @if( $user->hasRole('Superint') )

                        <?php $siProjects = \DB::table('project')->where('status', 'active')->where('deleted_at', NULL)->get(); ?>

                        <div class="col-md-12">

                                <div class="form-group">

                                    <label>Projects<span class="text-danger">*</span></label>

                                    <select name="si_project[]" multiple class="form-control" required="">

                                        @foreach( $siProjects as $g )

                                        <option @if( $g->superint == $user->id ) selected @endif value="{{ $g->id }}">{{ $g->job_name }}</option>

                                        @endforeach

                                    </select>
                                    
                                    <span id="error-phone-number" class="error-message text-danger"></span>

                                </div>

                            </div>

                        

                        @endif

                    </div>

                    <div class="row">

                        

                        <div class="col-md-6" style="display: none;">

                            <div class="form-group">

                                <label>Gender</label>

                                <select name="gender" class="form-control">

                                    <option value="">Select</option>

                                    <option value="male" @if($user->gender == 'male') selected @endif>Male</option>

                                    <option value="female" @if($user->gender == 'female') selected @endif>Female</option>

                                    <option value="others" @if($user->gender == 'others') selected @endif>Others</option>

                                </select>

                            </div>

                        </div>

                         <div class="col-md-6">

                            <div class="form-group">

                                <label>Status</label>

                                <select name="status" class="form-control">
                                
                                    <option value="2" @if($user->status == 2) selected @endif>Active</option>
                                    
                                    <option value="0" @if($user->status == 0) selected @endif>Blocked</option>

                                    @if( $user->hasRole('Worker') )

                                    <!--<option value="1" @if($user->status == 1) selected @endif>Pending</option>-->

                                    @endif

                                </select>

                            </div>

                        </div>
                        @if($user->hasRole('Superint'))
                    <div class="col-md-6">

                            <div class="form-group">

                                <label>Classification</label>

                                
                                <select name="classification" class="form-control">

                                    <option value="">Select classfication</option>
                                    <option value="C"  {{ ($user->classification == "C") ? "selected" : '' }}>C</option>
                                    <option value="W" {{ ($user->classification == "W") ? "selected" : '' }}>W</option>
                                    <option value="P" {{ ($user->classification == "P") ? "selected" : '' }}>P</option>
                                    <option value="O" {{ ($user->classification == "O") ? "selected" : '' }}>O</option>                                  

                            </select>
                        </div>

                    </div>
                    @endif
                             @if($user->hasRole('Foreman'))
                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Classification</label>

                                
                                <select name="classification" class="form-control">

                                    <option value="">Select classfication</option>
                                    <option value="C"  {{ ($user->classification == "C") ? "selected" : '' }}>C</option>
                                    <option value="W" {{ ($user->classification == "W") ? "selected" : '' }}>W</option>
                                    <option value="P" {{ ($user->classification == "P") ? "selected" : '' }}>P</option>
                                    <option value="O" {{ ($user->classification == "O") ? "selected" : '' }}>O</option>                                    

                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">

                            <div class="form-group">

                                <label>RSC Employee Id</label>

                                
                                <input type="text" value="{{ $user->rsc_employee_id }}"  autocomplete="off" class="form-control" name="rsc_employee_id" placeholder="" >

                        </div>

                    </div>

                    @endif

                        @if( $user->hasRole('Subcontractor') )

                          <!--<div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>Rock Spring Safety Training received on:</label>

                                @if( !empty( $user->training ) )
                                  
                                 <input type="text" class="form-control training" name="training" placeholder="" value="{{ date('m/d/Y', strtotime($user->training)) }}" >

                                 @else

                                 <input type="text" class="form-control training" name="training" placeholder="" >

                                 @endif
                            </div>

                        </div>-->

                       
                        

                    @endif

                        

                        <div class="col-md-6" style="display: none;">

                            <div class="form-group">

                                <label>Role</label>

                                <select name="user_role" class="form-control" id="role-select" required="">

                                    <option value="" disabled="">Select</option>

                                    <option value="superint" @if($user->hasRole('Superint')) selected @endif>Superint</option>

                                    <option value="payroll" @if($user->hasRole('Payroll')) selected @endif>Payroll</option>

                                    <option value="admin" @if($user->hasRole('Admin')) selected @endif>Administrator</option>

                                    <option value="foreman" @if($user->hasRole('Foreman')) selected @endif>Foreman</option>

                                    <option value="subcontractor" @if($user->hasRole('Subcontractor')) selected @endif>Subcontractor</option>

                                    <option value="safetyo" @if($user->hasRole('Safetyo')) selected @endif>Safety Officer</option>

                                    <option value="worker" @if($user->hasRole('Worker')) selected @endif>Worker</option>

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group worker-type-field" @if( !$user->hasRole(['Worker','Safetyo', 'Foreman']) ) style="display: none;" @endif >

                                <label>Worker Type<span class="text-danger">*</span></label>

                                <?php $workerTypeSaved = $user->worker_type; ?>

                                <select name="worker_type" class="form-control" @if( $user->hasRole('Worker') || $user->hasRole('Safetyo') ) required @endif>
                                    
                                    <option value="">Select</option>

                                    @foreach( $workerTypes as $workerType )

                                    <?php

                                    $workerTypeId = $workerType->id;

                                    ?>

                                     <option value="{{ $workerTypeId }}" @if( $workerTypeId == $workerTypeSaved ) selected @endif>{{ $workerType->type }}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                    </div>

                    @if($user->hasRole('Foreman'))
                    <div class="row">
                        <div class="col-md-12 current-uploaded-document">

                            <div class="col-md-12">

                                <div class="title">Safety Training</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                    <div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>Rock Spring Safety Training received on:</label>

                                @if( !empty( $user->training ) )
                                  
                                 <input type="text" class="form-control training" name="training" placeholder="" value="{{ date('m/d/Y', strtotime($user->training)) }}" >

                                 @else

                                 <input type="text" class="form-control training" name="training" placeholder="" >

                                 @endif
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>OSHA 30 Training received on:</label>

                                @if( !empty( $user->osha_date ) )

                                <input type="text" class="form-control osha_date" name="osha_date" placeholder="" value="{{ date('m/d/Y', strtotime($user->osha_date)) }}" >

                                @else

                                <input type="text" class="form-control osha_date " name="osha_date" placeholder="" >

                                @endif
                            </div>

                        </div>

                    </div>

                    <div class="row">


                        <div class="col-md-6">

                            <div class="form-group">


                                <label>CPR Training received on:</label>

                                @if( !empty( $user->cpr_date ) )

                                <input type="text" class="form-control cpr_date" name="cpr_date" placeholder="" value="{{ date('m/d/Y', strtotime($user->cpr_date)) }}" >

                                @else

                                <input type="text" class="form-control cpr_date" name="cpr_date" placeholder="" >

                                @endif
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>Other</label>

                                @if( !empty( $user->other_notes ) )

                                <input type="text" class="form-control" name="other_notes" placeholder="" value="{{ $user->other_notes }}" >

                                @else

                                <input type="text" class="form-control" name="other_notes" placeholder="" >

                                @endif
                            </div>

                        </div>
                    </div>       

                    
                    @endif


                    <div class="row">

                        <div class="col-md-6" @if( $user->hasRole('Worker') || $user->hasRole('Safetyo') ) style="display: block;" @else style="display: none;" @endif>

                             <div class="form-group subcontractor-field" @if( $user->hasRole('Worker') || $user->hasRole('Safetyo') ) style="display: block;" @else style="display: none;" @endif  >

                                 <label>Company<span class="text-danger">*</span></label>
                                  
								 
								  
   
                                 <select name="general_contractor" class="form-control" id="company-select" @if( $user->hasRole('Worker') || $user->hasRole('Safetyo') ) required @endif>

                                     <option value="">Select</option>

                                     @foreach( $subContractors as $subContractor )                                    

                                     <?php

                                     $subContractorName = $subContractor->id; 

                                     $parent = $user->parent; 

                                     ?>

                                     <option  value="{{ $subContractor->id }}" @if( $subContractorName == $parent ) selected @endif>
                                             
                                             {{ $subContractor->company_name }}

                                     </option>  

                                     @endforeach

                                 </select>
								
								 

                             </div>

                         </div>

                         @if( $user->hasRole('Worker') || $user->hasRole('Safetyo') )
                   

                        <div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>SSN ID</label>
                                  
                                 <div class="flex_wrap">
                                <span style="color: blue;" id='ssn-id'>{{ empty( $user->ssn_id ) ? 'COMP' : substr( $user->ssn_id, 0, 4) }}</span>
                                <input type="hidden" id="ssn-id-pre" value="{{ substr( $user->ssn_id, 0, 4) }}" name="ssn_id_pre">
                                <input type="text" class="form-control" id="" maxlength="4" value="{{ substr( $user->ssn_id, 4, 4) }}" name="ssn_id" placeholder="SSN ID">
                                 </div>
                            </div>



                        </div>

                          @if($user->hasRole('Worker') || $user->hasRole('Safetyo') || $user->hasRole('Superint'))  
                         <div class="col-md-6">

                            <div class="form-group">

                                <label>Classification</label>

                                
                                <select name="classification" class="form-control">

                                    <option value="">Select classfication</option>
                                    <option value="C"  {{ ($user->classification == "C") ? "selected" : '' }}>C</option>
                                    <option value="W" {{ ($user->classification == "W") ? "selected" : '' }}>W</option>
                                    <option value="P" {{ ($user->classification == "P") ? "selected" : '' }}>P</option>
                                    <option value="O" {{ ($user->classification == "O") ? "selected" : '' }}>O</option>                                  

                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">

                                <div class="form-group">

                                    <label>RSC Employee Id</label>

                                    
                                    <input type="text" value="{{ $user->rsc_employee_id }}"  autocomplete="off" class="form-control" name="rsc_employee_id" placeholder="" >

                            </div>

                        </div>
                    @endif

                        

                    @endif
                    
                    </div>

                     @if($user->hasRole('Worker') || $user->hasRole('Safetyo')  || $user->hasRole('Superint')) 
                    <div class="row">
                        <div class="col-md-12 current-uploaded-document">

                            <div class="col-md-12">

                                <div class="title">Safety Training</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                          <div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>Rock Spring Safety Training received on:</label>

                                @if( !empty( $user->training ) )
                                  
                                 <input type="text" class="form-control training" name="training" placeholder="" value="{{ date('m/d/Y', strtotime($user->training)) }}" >

                                 @else

                                 <input type="text" class="form-control training" name="training" placeholder="" >

                                 @endif
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>OSHA 30 Training received on:</label>

                                @if( !empty( $user->osha_date ) )

                                <input type="text" class="form-control osha_date" name="osha_date" placeholder="" value="{{ date('m/d/Y', strtotime($user->osha_date)) }}" >

                                @else

                                <input type="text" class="form-control osha_date " name="osha_date" placeholder="" >

                                @endif
                            </div>

                        </div>

                        
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>CPR Training received on:</label>

                                @if( !empty( $user->cpr_date ) )

                                <input type="text" class="form-control cpr_date" name="cpr_date" placeholder="" value="{{ date('m/d/Y', strtotime($user->cpr_date)) }}" >

                                @else

                                <input type="text" class="form-control cpr_date" name="cpr_date" placeholder="" >

                                @endif
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>Other</label>

                                @if( !empty( $user->other_notes ) )

                                <input type="text" class="form-control" name="other_notes" placeholder="" value="{{ $user->other_notes }}" >

                                @else

                                <input type="text" class="form-control" name="other_notes" placeholder="" >

                                @endif
                            </div>

                        </div>
                    </div>       

                    
                    @endif

                   

                    @if( $user->hasRole('Worker') || $user->hasRole('Safetyo') )

                   
                    @if( count( $user->workerDocuments ) > 0 )

         
                    <div class="current-uploaded-document">

                        <div class="col-md-12">

                            <div class="title">Currently Uploaded</div>

                        </div>
						
						<div class="updated_doc_wrp">
						<div class="row">
						  <div class="col-md-6">
						    <label class="doc_label">Notes/Training</label>
						  </div>
						  <div class="col-md-4">
						    <label class="doc_label">Documents</label>
						  </div>
						  <div class="col-md-2">
						    <label class="doc_label">Visible to Subcontractor?</label>
						  </div>
						</div>
						</div>

                        <?php $count = 1; ?> 

                        @foreach( $user->workerDocuments as $get )

                            <div class="col-md-12 additional-notedoc-element document_updated_text_wrap">

                                <div class="col-md-6 form-group">
                                    <label class="doc_label label_for_mobile">Notes/Training</label>
                                   <span>
                                        
                                        <span class="">{{ empty( $get->notes ) ? 'N/A' : $get->notes }}</span>
                              

                                   </span>
                                   
                                </div>

                                <div class="col-md-4 form-group">
                                    <label class="doc_label label_for_mobile">Document</label>
                                    @if($get->document)
                                   <span>
                                        
                                        <span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a target="_blank" href="{{ asset('public/uploads/documents') }}/{{ $user->id }}/{{ $get->document }}">{{ strlen($get->document) > 20 ? substr($get->document,0,20)."..." : $get->document }}</a>

                                        

                                   </span>

                                   @else 

                                    <span>Not Available</span>

                                    @endif
                                   
                                </div>

                                <div class="col-md-2 form-group">
                                 <label class="doc_label label_for_mobile">Visible to Subcontractor?</label>
                                <span>

                                @if( $get->author == Auth::user()->id )
                                        
                                       <label class="switch">

                                        <input type="checkbox" class="manual-log manual-log-toggle" doc-id="{{ $get->id }}" value="1" @if( $get->show ) checked @endif name="doc_upload[]">

                                        <span class="slider round" style="width: 50px;"></span>

                                        </label>

                                @else

                                    <span class="uploaded-by-text">

                                        Uploaded by Subcontractor

                                    </span>

                                @endif
								
								@if( $get->author == Auth::user()->id )

                                        <a style="color: #ff0000;
    text-decoration: underline;" href="{{ route('admin.worker.document.delete', $get->id ) }}" class="delete-document" onclick="return confirm('Are you sure you want to delete this record?');"><!--i class="fa fa-times delete-document"></i--> Delete</a>

                                        @endif
                                   
                                </div>
						
                                

                            </div>

                            <?php $count++; ?>

                        @endforeach



                    </div>

                    @endif
                     <br/>
                    @if( count( $user->workerDocuments ) < 10 )

                    <!-- Multiple data -->

                    <div class="worker-container" style="">

                        <div class="col-md-12 add-more" style="margin-bottom: 10px;">

                            <a href="javascript:void(0);" class="add-notedoc"><i class="fa fa-plus"></i> Add Notes/documents</a>

                        </div>
                        <div class="col-md-12 add_doc_first_row" style="display:flex; align-items:center; padding:0px;"> 
                        <div class="col-md-6 small_textarea">

                        <textarea placeholder="Notes/Training" class="form-control" name="notes_training[]" maxlength="1000"></textarea>

                        </div>

                        <div class="col-md-3">

                            <input title="Document" type="file" class="form-control doc-field" id="" name="worker_document[]">

                        </div>

                        <div class="col-md-3">
                        <div class="switch_wrap">
						<p class="visible_text">Visible to Subcontractor?</p>
                        <label class="switch">

                        <input type="checkbox" class="manual-log" value="1"  name="doc_upload[]">

                        <span class="slider round" style="width: 50px;"></span>

                        </label>
						</div>

                        </div>
						</div>
						<div class="clearfix"></div>

                    </div>

                    <!-- /Multiple data -->
                  
                    @endif

                    @endif
                    

                    @if( $user->hasRole('Subcontractor') || $user->hasRole('Foreman') || $user->hasRole('Admin') || $user->hasRole('Superint') || $user->hasRole('Payroll') )

                    @if( count( $user->documents ) > 0 )

                    <div class="row current-uploaded-document">

                        <div class="col-md-12">

                            <div class="title">Currently Uploaded</div>

                        </div>

                        <?php $count = 1; ?> 

                        @foreach( $user->documents as $get )

                            <div class="col-md-6 additional-document-element">

                                <div class="form-group">

                                   <span>

                                        @if( $get->document )
                                        
                                        <span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a target="_blank" href="{{ asset('public/uploads/documents') }}/{{ $user->id }}/{{ $get->document }}">{{ strlen($get->document) > 20 ? substr($get->document,0,20)."..." : $get->document }}</a>

                                        @else

                                        <span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a target="_blank" href="{{ asset('public/uploads/documents') }}/{{ $user->id }}/{{ $get->document }}">N/A</a>

                                        @endif

                                        <a href="{{ route('admin.subcontracto.document.delete', $get->id ) }}" class="delete-document" onclick="return confirm('Are you sure you want to delete this record?');"><i class="fa fa-times delete-document"></i></a>

                                   </span>
                                   
                                </div>

                            </div>

                            <?php $count++; ?>

                        @endforeach


                    </div>

                    @endif

                        @if( count( $user->documents ) < 10 )

                        <div class="row sub-container">

                            <div class="col-md-12 add-more" style="margin-bottom: 10px;">
   
                                    <a href="javascript:void(0);" class="add-document"><i class="fa fa-plus"></i> Add more documents</a>
                               
                            </div>

                            <div class="col-md-12">

                                <input type="file" class="form-control doc-field" id="" name="document[]">

                            </div>

                        </div>

                        @endif

                    @endif

                    @if( $user->hasRole('Worker') )

                    <button type="button" class="btn btn-primary float-right worker-update-button" style="margin:15px 0;">

                        <span class="text">Update & Go Back</span>
    
                    </button>

                    @endif


                    <button type="submit" class="btn btn-primary float-right" style="margin:15px 0;margin-right: 10px;">

                        <span class="text">Update</span>
    
                    </button>

                    </form>

                </div>
				
				</div>
                
                <div class="card shadow mb-4">
            

            <div class="card-header py-3">
                
                <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
            
            </div>

            <div class="card-body">

                <form class="change-password" method="POST" action="{{ route('admin.user.password.change') }}">

                    @csrf

                    <div class="form-group">

                        <label>New Password<span class="text-danger">*</span></label>

                        <input type="hidden" value="{{ $user->id }}" name="user_id">

                        <input type="password" id="password-reset" pattern=".{6,}" title="6 characters minimum" class="form-control" name="new-password" placeholder="New Password" autocomplete="off" required>

                    </div>

                    <div class="form-group">

                        <label>Confirm New Password<span class="text-danger">*</span></label>

                        <input type="password" id="password-reset-confirm" class="form-control" name="confirm-new-password" placeholder="Confirm New Password" autocomplete="off" required>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Change Password</span>
    
                    </button>

                </form>

            </div>

            </div>

            
            

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">


    $('.worker-update-button').click(function(){

        var action = $('#edit-user').attr('action');

        action = action+'?goback=1';

        $('#edit-user').attr('action',action);

        $('#edit-user').submit();
    })

var ls = localStorage['workerIds'];

    if( ls )
    {
    
    var lsa = $.parseJSON(ls);

    console.log( lsa );

    var val = "{{ $user->id }}";

    var next = lsa.indexOf(val)+1;

    var previous = lsa.indexOf(val)-1;
    

    if(lsa[previous]  )
    {

      $('.prev-ls a').attr('href', "{{ route('admin.users.view') }}/"+lsa[previous])

   
    }

    if(lsa[next]  )
    {
      $('.next-ls a').attr('href', "{{ route('admin.users.view') }}/"+lsa[next])
    }
    }
    else
    {
      $('.prev-ls').hide();
      $('.next-ls').hide();

      $('.prev').show();
      $('.next').show();
    }

    $(document).on("keypress",".training, .osha_date, .cpr_date", function(e){
    e.preventDefault();
    return false;

  }); 

$(document).ready(function() {

    $( ".training" ).datepicker();

    $( ".osha_date" ).datepicker();

    $( ".cpr_date" ).datepicker();

    $('.manual-log-toggle').click(function(){

        var showVal = 0;
        var docId = $(this).attr('doc-id');

        if( $(this).is(':checked') )
        {
            showVal = 1;
        }

        $.ajax({
        url: "{{ route( 'worker.document.status' ) }}",
        type: "GET",
        data: { id : docId, showVal : showVal },
        success: function(data){
            
        }
        });

    })

    var preFix = $.trim( $( "#company-select option:selected" ).text() ).replace(/\s+/, "").substring(0, 4).toUpperCase();
    
    $('#ssn-id').text( preFix );

    $('#ssn-id-pre').val( preFix );

    /* Company select */

    $('#company-select').change(function(){

        var preFix = $.trim( $( "#company-select option:selected" ).text() ).replace(/\s+/, "").substring(0, 4).toUpperCase();

        if( preFix == 'SELE' )
        {
            preFix = 'COMP';
        }

        $('#ssn-id').text( preFix );

        $('#ssn-id-pre').val( preFix );

    })

    /* /Company select */

    /* Worker add note document */

    $('.add-notedoc').click(function(){

        if( $( ".additional-notedoc-element" ).length == 9 )
            return false;

        $('.worker-container').append('<div class="col-md-12 additional-notedoc-element"><div class="col-md-6"><textarea placeholder="Notes/Training" class="form-control" id="" name="notes_training[]"></textarea></div><div class="col-md-3"><input title="Document" type="file" class="form-control doc-field" id="" name="worker_document[]"></div><div class="col-md-3"><div class="switch_wrap"> <p class="visible_text">Visible to Subcontractor?</p> <label class="switch"><input type="checkbox" class="manual-log" value="1"  name="doc_upload[]"><span class="slider round" style="width: 50px;"></span></label></div></div><a href="javascript:void(0);" class="document-close"><i class="fa fa-times close-document-button"></i></a></div>');

    })

    /* Add document */

    $('.add-document').click(function(){

        if( $( ".additional-document-element" ).length == 9 )
            return false;

        $('.sub-container').append('<div class="col-md-12 additional-document-element"><input type="file" class="form-control doc-field" id="" name="document[]"><a href="javascript:void(0);" class="document-close close_sub_doc"><i class="fa fa-times close-document-button"></i></a></div>');

    })

    $(document).on("click", ".document-close", function(){

        $(this).parent().remove();

    })

    /* Check each document field for PDF */

    $('.doc-field').click(function()
    {

        if( $(this).val() != '' )
        {

            $(this).val('');
                
        }

    })

    $(document).on("change", ".doc-field", function(){

        var ext = $(this).val().split('.').pop().toLowerCase();

        if( $.inArray(ext, ["pdf"]) == -1 )
        {

            alert('Document should be only PDF!');

            $(this).val('');

            return false;

        }


    })

    /* Confirm password */

    var passwordreset = document.getElementById("password-reset")
      , confirm_passwordreset = document.getElementById("password-reset-confirm");

    function validatePasswordReset(){
      if(passwordreset.value != confirm_passwordreset.value) {
        confirm_passwordreset.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_passwordreset.setCustomValidity('');
      }
    }

    passwordreset.onchange = validatePasswordReset;
    confirm_passwordreset.onkeyup = validatePasswordReset;

    /* Confirm password */

    /* Worker document */

    $('#file_upload').click(function()
    {

        if( $(this).val() != '' )
        {

            $(this).val('');

        }

    })

    $('#file_upload').change(function()
    {

        var ext = $('#file_upload').val().split('.').pop().toLowerCase();

        if( $.inArray(ext, ["pdf"]) == -1 )
        {

            alert('Worker document should be only PDF!');

            $('#file_upload').val('');

        }

    })

    /*
     * @Function Name
     *
     *
     */

     $(document).on('change', '#role-select', function(){ 

        $('.subcontractor-field').hide();

        $('.worker-type-field').hide();

        var role = $(this);

        var roleVal = role.val();

        if( roleVal == 'worker' )
        {

            $('.subcontractor-field').show();

            $('.worker-type-field').show();

        }

        if( roleVal == 'foreman' )
        {

            $('.subcontractor-field').show();

        }

     } );

    /*
     * @Function Name
     *
     *
     */
    $('#change-profile-image').on('click', function(event) {

        // $('#update-profile-image input[name="image"]').click();

        $('#myDropzone').click();

        event.preventDefault();

    });

    // $('#update-profile-image input[name="image"]').on('change', function(event) {

    //     previewImage(this, '#profile-image');

    //     $('#remove-profile-preview').show();

    // });
    $('#remove-profile-preview').on('click', function(event) {

        $('.add-text').text('Add');
        
        $('#cropped-image').val('');

        $('#cropped-image-type').val('');

        // $('#update-profile-image input[name="image"]').val('');

        $('#profile-image').attr('src', '{{ asset("images/elements/user.png") }}');

        $(this).hide();

        event.preventDefault();

    });

    $('input[name="phone-number"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });


    $('input[name="payroll_phone"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });

    $('#edit-user').on('submit', function(event) {

        var error = false;

        $('.form-group .error-message').html('');

        var name = $('input[name="first-name"]').val();
        
        var email = $('input[name="email"]').val().toLowerCase();

        var phone_number = $('input[name="phone-number"]').val();
        
        var username = $('input[name="username"]').val();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter first name of user.');

        }

        @if( $user->hasRole('Worker') )

            if( email == '' && phone_number == '' )
            {

                error = true;

                $('#error-email').html('Please enter either an Email or Phone.');

            }

        @endif

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('First name should contain atleast one alphabetic character.');
            
        }

        if(email != '' && !email.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) {

            error = true;
            
            $('#error-email').html('Enter a valid email address of user.');
            
        }

        if(phone_number != '' && phone_number.replace(/[^0-9]/g, "").length != 10) {

            error = true;
            
            $('#error-phone-number').html('Please enter a valid phone number.');
            
        }

        if(username == '') {

            error = true;

            $('#error-username').html('Please enter username of user.');

        }

        if(username != '' && /\s/.test(username)) {

            error = true;
            
            $('#error-username').html('Username cannot contain spaces.');

        }

        if(username != '' && !/[a-zA-Z]/.test(username)) {

            error = true;

            $('#error-username').html('Username should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();

        }

    });

    $('.btn-remove-image').on('click', function(event) {

        if(!confirm('Are you sure you want to remove the image?')) {

            event.preventDefault();

        }

    });

}); 

var placeholder_img ="{{ asset('images/elements/user.png') }}";

Dropzone.options.myDropzone = {
    url: '/post',
    addRemoveLinks: true,
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png",
    disablePreview: false,

    init: function(file, done) {
        
    this.on("addedfile", function(event) {
        console.log('addedfile')
        if(this.files[0].type != 'image/png' && this.files[0].type != 'image/jpeg' && this.files[0].type != 'image/svg'){
            document.getElementsByClassName('dz-preview')[0].style.display = "none";
            alert("Please upload a image");
            this.removeFile(this.files[0]);
        }
      if (this.files[1]!=null){
        console.log('removeFile')
        this.removeFile(this.files[0]);
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
      }
      
    });
  },
    transformFile: function(file, done) {  
 
     
     
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
        // Create Dropzone reference for use in confirm button click handler
        var myDropZone = this;  
        
        // Create the image editor overlay
        var editor = document.createElement('div');
        editor.style.position = 'fixed';
        editor.style.left = '50px';
        editor.style.right = '50px';
        editor.style.top = '50px';
        editor.style.bottom = '50px';
        editor.style.zIndex = 9999;
        editor.style.width = '500px';
        editor.style.height = '500px';
        editor.className += "profile_image_cropper";
        editor.style.backgroundColor = '#000';
        document.body.appendChild(editor);  
        
        // Create confirm button at the top left of the viewport
        var buttonConfirm = document.createElement('button');
        buttonConfirm.style.position = 'absolute';
        buttonConfirm.style.left = '10px';
        buttonConfirm.style.top = '10px';
        buttonConfirm.style.zIndex = 9999;
        buttonConfirm.textContent = 'Confirm';
        editor.appendChild(buttonConfirm);

        var buttonCancel = document.createElement('button');
        buttonCancel.style.position = 'absolute';
        buttonCancel.style.left = '150px';
        buttonCancel.style.top = '10px';
        buttonCancel.style.zIndex = 9999;
        buttonCancel.textContent = 'Cancel';
        editor.appendChild(buttonCancel);

        buttonCancel.addEventListener('click', function() {  
                            // Remove the editor from the view
            document.body.removeChild(editor);  
            document.getElementById('profile-image').src = placeholder_img;
            myDropZone.removeFile(file);



        });

        
        buttonConfirm.addEventListener('click', function() {    
            
            // Get the canvas with image data from Cropper.js
            var canvas = cropper.getCroppedCanvas({
                width: 256,
                height: 256
            });    

         
           var profile_img = document.getElementById('profile-image');
           profile_img.src = canvas.toDataURL();

           var image1 = document.getElementById('cropped-image');
           var imagetype = document.getElementById('cropped-image-type');
           image1.value=canvas.toDataURL();
           imagetype.value = file.type.substr(6); 

           

          
           
            


            // Turn the canvas into a Blob (file object without a name)
            // canvas.toBlob(function(blob) { 


            //     // Create a new Dropzone file thumbnail
            //     myDropZone.createThumbnail(blob,
            //     myDropZone.options.thumbnailWidth,
            //     myDropZone.options.thumbnailHeight,
            //     myDropZone.options.thumbnailMethod,
            //     false, 
            //     function(dataURL) {
                
            //             // Update the Dropzone file thumbnail
            //             myDropZone.emit('thumbnail', file, dataURL);          
            //             // Return the file to Dropzone
            //             done(blob);      
            //         });    
            //     });   
                // Remove the editor from the view
                document.body.removeChild(editor);  

                $('.add-text').text('Change');

                $('#remove-profile-preview').show();
               
               
done();

            
            });  
            
            // Create an image node for Cropper.js
            var image = new Image();
            image.src = URL.createObjectURL(file);

            editor.appendChild(image);
    
            // Create Cropper.js
            var cropper = new Cropper(image, { aspectRatio: 1 });
            
            }
    

};

</script>

@endsection