@extends('admin.layouts.app')

@if( $userType == 'payroll' )

@section('title', 'Payroll Providers')

@else

@section('title', $userTitle)

@endif


@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">
    <style type="text/css">
        .dt-button.buttons-csv.buttons-html5 {
            position: absolute;
            right: 0;
            line-height: 15px;
            color: #fff !important;
            background: #103359;
            padding: 14px 28px;
            font-size: 18px;
            border-radius: 5px !important;
            display: inline-block;
            font-size: 15px;
            /* position: relative; */
            top: -2px;
            border-radius: 5px;
            width: auto;
        }

    </style>
    <style type="text/css">

        .namecol
        {
            color: black;
            cursor: pointer;
            font-weight: bold;
        }

        div.dataTables_wrapper div.dataTables_processing {
position: fixed;
top: 50%;
left: 55%;
width: 200px;
margin-left: -100px;
margin-top: -26px;
text-align: center;
padding: 1em 0;
background: transparent;
    font-size: 80px;
    padding: 10px;
    border-color: transparent;
    color: #0f3459;
}
     
        select[name="datatable_length"] {

        width: 70px !important;

        }

        div#datatable_info
        {

        float: left;

        }

        ul.pagination
        {

        margin-top: 14px !important;

        }


    </style>

    <link href="{{ asset('css/jquery-ui.css') }}" rel="Stylesheet" type="text/css" />


   @if( $userType == 'worker' ||  $userType == 'foreman' ||  $userType == 'safetyo' )

    <style type="text/css">

      .worker_page_wrapper .dataTables_length{

       right: 201px;	

	   }

    </style>

    @endif

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">
        
        @if( !empty( $userType ) )

        @if( $userType == 'payroll' )

        Payroll Providers

        @else

        {{ $userTitle }}

        @endif

        @endif

    </h1>

    @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

    <a href="{{ route( 'admin.users.create' ) }}?userType={{ $userType }}" class="float-right btn-create">

      Add

    </a>

    @endif

</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body worker_page_wrapper">

                  

                  <div class="custom_table_filter all-workers-custom-filter">



                    <!-- Subcontractor filter -->

                    <select id="status-filter">

                        <option disabled selected>-Select Status-</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 2 ) selected  @endif value="2">Active</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 0 ) selected  @endif value="0">Blocked</option>

                        @if( $userType == 'worker' )

                        <!--<option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 1 ) selected  @endif value="1">Pending</option>-->

                        @endif

                    </select>

                    <!-- Subcontractor filter -->

                    <!-- Company filter -->

                    @if( $userType == 'worker' )

                    <select id="company-filter">

                        <option disabled selected>-Select Company-</option>

                        @foreach( $companiesList as $company )

                          <?php $companyId = $company->id; ?>

                          <option @if( isset( $_REQUEST['company'] ) && @$_REQUEST['company'] == $companyId ) selected  @endif value="{{ $companyId }}">{{ $company->company_name }}</option>

                        @endforeach

                    </select>

                    <!-- Company filter -->

                    <!-- Title filter -->

                    <select id="title-filter">

                        <option disabled selected>-Select Title-</option>

                        @foreach( $titlesList as $title )

                          <?php $titleId = $title->id; ?>

                          <option @if( isset( $_REQUEST['title'] ) && @$_REQUEST['title'] == $titleId ) selected  @endif value="{{ $titleId }}">{{ $title->type }}</option>

                        @endforeach

                    </select>

                    @endif

                    <!-- Title filter -->

                    <!-- Reset filter -->

                    @if(
                    (
                        isset( $_GET['status'] ) ||
                        isset( $_GET['company'] ) ||
                        isset( $_GET['title'] )
                    )
                    )

                    <a href="{{ route('admin.users') }}?userType={{ $userType }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

                    @endif

                    <!-- Reset filter -->

                    @if( $userType == 'worker' ||  $userType == 'foreman'  ||  $userType == 'safetyo')


                    <form action="{{ route('download.export.workers') }}" method="GET" class='export-form'>


                        <input type='hidden' value="{{ @$_REQUEST['userType'] }}" name="userType">
                        <input type='hidden' value="{{ @$_REQUEST['title'] }}" name="title">
                        <input type='hidden' value="{{ @$_REQUEST['status'] }}" name="status">
                        <input type='hidden' value="{{ @$_REQUEST['company'] }}" name="company">
                        
                        <a class="dt-button buttons-csv buttons-html5" href="{{ url('get-user-export')}}?usertype={{ $userType }}&title={{ @$_REQUEST['title'] }}&status={{ @$_REQUEST['status'] }}&company={{ @$_REQUEST['company'] }}"><?php echo ($userType == 'worker') ? 'Export Workers' : (($userType == 'safetyo') ? 'Export Safety Officers' : 'Export Foremen '); ?></a>
                       
                        <!-- <input class="dt-button buttons-csv buttons-html5" type='submit' value=' -->
                        <?php 
                         // echo ($userType == 'worker') ? 'Export Workers' : (($userType == 'safetyo') ? 'Export Safety Officers' : 'Export Foremen ');
                        ?>
                         <!-- '> -->
            

                    </form>

                    @endif

                </div>

                @if( $userType == 'worker' )

                <table class="table table-bordered table-hover all-workers-listing-table-wrap" id="datatable">

                @else

                <table class="table table-bordered table-hover" id="datatable">

                @endif
            
                
  
                    <thead>
    
                        <tr>

                            <!--<th scope="col" class="worker-ids">Id</th>-->

                            <th scope="col">Name</th>

                            <th scope="col">SSN</th>
        
                           <th scope="col">Email</th>

                           @if( $userType == 'worker' )

                           <th scope="col">Phone</th>

                           <th scope="col">Company</th>

                           <th scope="col">SSN ID</th>

                           <th scope="col">Title</th>

                           <th scope="col">Registered On</th>

                           @endif
                            
                            <th style="text-align: center;" scope="col">Status</th>
    
                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      
      <div class="modal-body alert-modal-body">
              </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>
<input type="hidden" value="0" class="serverSideFlag">

@endsection

@section('scripts')

<script src="{{ asset('js/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
  
  <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.flash.min.js') }}"></script>
  
  

  <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
  
  <script type="text/javascript" language="javascript" src="{{ asset('js/buttons.print.min.js') }}"></script>

  <script src="{{ asset('js/jquery-ui.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/datetime-moment.js') }}"></script>

<script type="text/javascript">

    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this user?')) {
        
            event.preventDefault();

        }

    });

    /* ----------------------------------- Next/Prev ----------------------------------- */

	serialize = function(obj) {
	  var str = [];
	  for (var p in obj)
		if (obj.hasOwnProperty(p)) {
		  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	  return str.join("&");
	}
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

	$(document).on("click","a.index-link", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='Edit']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='View']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});


    var table;

    /* ----------------------------------- Next/Prev ----------------------------------- */

    $(document).on('click', '.lc-id', function(event) {

        var lcstids = [];

        $(".workers-ids-value").each(function() {
            
            lcstids.push( $(this).text() );

        });

        lcstids.shift();

        localStorage.workerIds = JSON.stringify(lcstids);

    }); 
    var i = 0;
    function initialize_database() {

        var interval = setInterval(function(){
            i += 1;
            if(i === 1){
                $(".serverSideFlag").attr('value', 1);

                clearInterval(interval);

                console.log('clearewd');
            }
        }, 5000); 

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_users_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        var serverSideFlag = $(".serverSideFlag").attr('value');
        var dats = (serverSideFlag == 0) ? true : false;
        console.log(dats); 
        table = $('#datatable').DataTable({

            stateSave: true,

          

            @if( $userType == 'worker' )

            /*dom: 'lBfrtip',

            buttons: [
                {
                    extend: 'csvHtml5',
                    text: 'Export Workers',
                    exportOptions: {
                         modifier : {
             page : 'all', // 'all', 'current'
         },
                        columns: [0,1, 2, 3, 4, 5]
                    }
                }
            ],*/

            @endif

            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No user found!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: (serverSideFlag === 0) ? true : false,

            "ajax": {

              "url": "{{ route('admin.datatable.users.get') }}",

              "data": function ( d ) {
               d.userType = "{{ $userType }}"
               d.status = "{{ @$_REQUEST['status'] }}"
               d.company = "{{ @$_REQUEST['company'] }}"
               d.title = "{{ @$_REQUEST['title'] }}"
              
            }

            },

            columns: [

               // { data: "id", name: "id", className: "workers-ids-value", searchable: false, orderable: false },

                { data: "name", name: "name", className: 'namecol', searchable: true, orderable: true },

                { data: "ssn", name: "ssn", orderable: true },
                
                { data: "email", name: "email", className: 'emailcol', searchable: true, orderable: false },

                @if( $userType == 'worker' )

                { data: "phone_number", name: "phone_number", searchable: true, orderable: true },

                { data: "company", name: "company", searchable: false, orderable: false },

                { data: "ssn", name: "ssn", visible: false, orderable: true },

                { data: "worker_type", name: "worker_type", searchable: false, orderable: false },

                { 
                    data: "created_at", name: "created_at", searchable: false, orderable: true,
                    render: function (data, type, row) {
                        var dateArr = data.split("-");
                        return dateArr[1]+"-"+dateArr[2]+"-"+dateArr[0];
                    }
                },   

                @endif
                
                { data: "status", name: "status", searchable: false, orderable: false },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $(document).on('click', 'td.namecol', function(){

            var userid = $(this).parent().find('.emailcol input').val();

            

            var link = "{{ route('admin.users.view') }}/"+userid;

            //window.location.href = link;

            $(this).parent().find('.btn-info').trigger('click');


        })

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_users_pageLength", len);

        });

        function getQueryVariable(url, variable)
        {

          var query = url.substring(1);

          var vars = query.split('&');

          for (var i=0; i<vars.length; i++)
          {

              var pair = vars[i].split('=');

              if (pair[0] == variable)
              {

                return pair[1];

              }
            
          }

             return false;

        }

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#status-filter').change(function()
        {
             var statusVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&status="+statusVal;

             if( getQueryVariable(originalUrl, 'company') )
             {

              qString += "&company="+getQueryVariable(originalUrl, 'company');

             }

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType={{ $userType }}'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#company-filter').change(function()
        {
             var companyVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&company="+companyVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType={{ $userType }}'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#title-filter').change(function()
        {
             var titleVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&title="+titleVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             if( getQueryVariable(originalUrl, 'company') )
             {

              qString += "&company="+getQueryVariable(originalUrl, 'company');

             }

             url += '?userType={{ $userType }}'+qString;

             window.location.href = url;


        })

    });
    $(document).on("click",".status_dropdown", function(){

      var status_ele = $(this);

      var selected_status = $(this).data("status");

      var current_status = status_ele.closest(".dropdown").find("button").data("status");

      var current_status_show = "";

              if(current_status=="0"){
                current_status_show = "Blocked";
              }
              else if(current_status=="1"){
                current_status_show = "Pending";
              }else {
                current_status_show = "Active";
              }

      if(selected_status!=current_status)
          $.ajax({
            "url" : "{{route('admin.users.update_status')}}",
            "method":"get",
            "data" : {"user_id": $(this).data("user_id"), "status": $(this).data("status")},
            "success": function(response){

              var status_show = "";

              if(response.status==0){
                status_show = "Blocked";
              }
              else if(response.status==1){
                status_show = "Pending";
              }else {
                status_show = "Active";
              }
              status_ele.closest(".dropdown").find("button").html(status_show);

              status_ele.closest(".dropdown").find("button").data("status",response.status);

              status_ele.closest(".dropdown").find("button").removeClass("btn-"+current_status_show);

              status_ele.closest(".dropdown").find("button").addClass("btn-"+status_show);

              $(".alert-modal-body").html("The status has been updated to "+status_show);

              $("#alertModal").modal({"show":true});

              //alert("The status has been updated to "+status_show);
            }
          })
        })
</script>

@endsection