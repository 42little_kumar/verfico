@extends('admin.layouts.app')

@section('title', 'Create')

@section('styles')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">

<link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">

<style type="text/css">
        
    i.fa.fa-times.close-document-button {
    position: absolute;
    top: 9px;
    right: 29px;
    border: 1px solid;
    padding: 5px 8px;
    border-radius: 50%;
}

.flex_wrap{
 display:flex;
 align-items:center; 
}

span#ssn-id {
    background: #f2f2f2;
    padding: 11px 12px;
    color: #000 !important;
    font-weight: bold;
    border-radius: 5px;
    margin: 0 6px 0 0;
    font-size: 14px;
}
.sub-container .col-md-12
{
    margin-bottom: 10px;
}

.add-more {
    background: #3b7642;
    color: white;
    margin-bottom: 12px;
}

.add-more a {
    color: white;
}

.col-md-12.additional-notedoc-element {
    margin: 20px 0 0;
    padding: 0;
    display: flex;
    align-items: center;
	position:relative;
}

.col-md-12.add-more a{
    display:block;
    align-items: center;
    justify-content: center;
    padding: 11px;
    background: #103359;
    font-size: 15px;
    text-transform: uppercase;
    font-weight: 600;
    border-radius: 50px;
	text-align:center;
}
.col-md-12.add-more a i{
margin-right:3px;	
}
.col-md-12.add-more{
 background:none !important;	
}

a.document-close {
    position: absolute;
    right: -15px;
    top:9px;
    color: #ff0000;
    font-weight: bold;
    font-size: 18px;
}

.col-md-12.additional-notedoc-element textarea.form-control, .small_textarea textarea.form-control{
    height: 47px !important;
}
p.visible_text {
    font-weight: bold;
    font-size: 11px;
    margin: 0 0 7px;
    width: 100%;
}
.switch_wrap .switch{
float: right;
margin-right: 55px;	
}
a.document-close.close_sub_doc {
    top: -3px;
    right: -9px;
    background: #fff;
    padding: 0;
    font-size: 20px;
}

.current-uploaded-document .title {
    background: #fff;
    color: #000;
    margin-bottom: 18px;
    padding: 0 10px;
    margin-top: 9px;
    font-size: 18px;
    font-weight: bold;
    border-bottom: 1px solid #ddd;
    padding: 10px 0 9px;
}

</style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">

        {{-- @if( !empty( $userType ) )

        Create {{ ucwords( $userType ) }}

        @endif --}}

     </h1>

    <a href="{{ route( 'admin.users' ) }}?userType={{ $userType }}" class="back-link">< Back to {{ getRoleTitle( $userType ) }}</a>
    
</div>

<form action="{{ route('admin.users.create') }}" class="crete_usr" id="create-user" method="POST" enctype="multipart/form-data">
            
    @csrf

    <input type="hidden" id="cropped-image" name="cropped-image">

    <input type="hidden" id="cropped-image-type" name="cropped-image-type">

    <div class="row">

        <div class="col-lg-4">

            <div class="card shadow mb-4">
        
                <div class="card-header py-3">
            
                    @if( $userType == 'subcontractor' )

                    <h6 class="m-0 font-weight-bold text-primary">Company Logo</h6>

                    @else

                    <h6 class="m-0 font-weight-bold text-primary">Image</h6>

                    @endif

                </div>
            
                <div class="card-body text-center" id="update-profile-image">

                    <div class="dropzone" id="myDropzone">

                    <img src="{{ asset('images/elements/user.png') }}" id="profile-image" alt="profile-image" class="img-profile rounded-circle" style="width: 150px; height: 150px;">

                    <hr>

                    <input type="file" name="image" accept="image/*" style="display:none">

                    <div class="dz-message"></div>
                    
                    <div>Drag and drop file here.</div>
                                    
                    </div>

                    <button id="change-profile-image" type="button" class="btn btn-info btn-icon-split">

                        <span class="icon text-white-50">
                        
                            <i class="far fa-image"></i>
                        
                        </span>
                        
                        <span class="text add-text">Add</span>
                    
                    </button>

                    <button id="remove-profile-preview" style="display:none" type="button" class="btn btn-danger btn-icon-split">

                        <span class="icon text-white-50">
                        
                            <i class="fas fa-times"></i>
                        
                        </span>
                        
                        <span class="text">Remove</span>
                    
                    </button>

                </div>
            
            </div>
            
        </div>

        <div class="col-lg-8">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Personal Information</h6>
                
                </div>

                <div class="card-body">

                    @if( $userType == 'subcontractor' )

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Company Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="company-name" placeholder="Company Name" value="{{ old('company-name') }}" maxlength="50" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll First Name</label>

                                <input type="text" class="form-control" name="payroll_first_name" placeholder="Payroll First Name" value="{{ old('payroll_first_name') }}" maxlength="50" >

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll Last Name</label>

                                <input type="text" class="form-control" name="payroll_last_name" placeholder="Payroll Last Name" value="{{ old('payroll_last_name') }}" maxlength="50" >

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll Email</label>

                                <input type="email" class="form-control" name="payroll_email" placeholder="Payroll Email" value="{{ old('payroll_email') }}" maxlength="50" >

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll Phone</label>

                                <input type="text" class="form-control" name="payroll_phone" placeholder="Payroll Phone" value="{{ old('payroll_phone') }}" maxlength="50" >

                            </div>

                        </div>

                    </div>

                    @endif

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}First Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="first-name" placeholder="First Name" value="{{ old('first-name') }}" maxlength="50" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}Last Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="last-name" placeholder="Last Name" value="{{ old('last-name') }}" maxlength="50" required="">

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}{{ 'Email' }}@if( $userType != 'worker' && $userType != 'safetyo' )<span class="text-danger">*</span>@endif</label>

                                <input type="email" class="form-control" name="email" placeholder="Email Address" autocomplete="off" value="{{ old('email') }}"
                                @if( $userType != 'worker' && $userType != 'safetyo') required="" @endif
                                >

                                <span id="error-email" class="error-message text-danger"></span>

                            </div>

                        </div>

                        {{--<div class="col-md-6">

                            <div class="form-group">

                                <label>Username<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" maxlength="25" required>

                                <span id="error-username" class="error-message text-danger"></span>

                            </div>

                        </div>--}}

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>{{ $fieldPrefix }}Phone</label>

                                <div class="country_code">

                                    <input type="hidden" name="user_role" value="{{ $userType }}">

                                    <input type="text" class="form-control" name="phone-number" placeholder="Phone Number" value="{{ old('phone-number') }}" >
                            
                                </div> 
                                
                                <span id="error-phone-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                        @if( $userType == 'superint' )

                        <?php $siProjects = \DB::table('project')->where('status', 'active')->where('deleted_at', NULL)->get(); ?>

                        <div class="col-md-12">

                                <div class="form-group">

                                    <label>Projects<span class="text-danger">*</span></label>

                                    <select name="si_project[]" multiple class="form-control" required="">

                                        @foreach( $siProjects as $g )

                                        <option value="{{ $g->id }}">{{ $g->job_name }}</option>

                                        @endforeach

                                    </select>
                                    
                                    <span id="error-phone-number" class="error-message text-danger"></span>

                                </div>

                            </div>

                        

                        @endif

                    </div>

                    

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Password<span class="text-danger">*</span></label>

                                <input type="password" id="password" pattern=".{6,}" title="6 characters minimum" class="form-control" name="password" placeholder="Password" required>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Confirm Password<span class="text-danger">*</span></label>

                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" onchange="check()" placeholder="Confirm Password" required>

                            </div>

                        </div>

                           @if($userType == 'foreman')
                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Classification</label>

                                
                                <select name="classification" class="form-control">

                                    <option value="">Select classfication</option>
                                    <option value="C">C</option>
                                    <option value="W">W</option>
                                    <option value="P">P</option>
                                    <option value="O">O</option>                                  

                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">

                                <div class="form-group">

                                    <label>RSC Employee Id</label>

                                    
                                    <input type="text" autocomplete="off" class="form-control" name="rsc_employee_id" placeholder="" >

                            </div>

                        </div>
                   

                    @endif

                        <div class="col-md-6" style="display: none;">

                            <div class="form-group">

                                <label>Gender</label>

                                <select name="gender" class="form-control">

                                    <option value="">Select</option>

                                    <option value="male" @if(old('gender') && old('gender') == 'male') selected @endif>Male</option>

                                    <option value="female" @if(old('gender') && old('gender') == 'female') selected @endif>Female</option>

                                    <option value="others" @if(old('gender') && old('gender') == 'others') selected @endif>Others</option>

                                </select>

                            </div>

                        </div>

                    </div>

                    @if($userType == 'foreman')
                    <div class="row">
                        <div class="col-md-12 current-uploaded-document">

                            <div class="col-md-12">

                                <div class="title">Safety Training</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>Rock Spring Safety Training received on:</label>

                            

                                 <input type="text" autocomplete="off" class="form-control training" name="training" placeholder="" >

                                 
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>OSHA 30 Training received on:</label>


                                <input type="text" autocomplete="off" class="form-control osha_date " name="osha_date" placeholder="" >

                            </div>

                        </div>

                    </div>

                    <div class="row">


                        <div class="col-md-6">

                            <div class="form-group">


                                <label>CPR Training received on:</label>


                                <input type="text" autocomplete="off" class="form-control cpr_date" name="cpr_date" placeholder="" >

                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>Other</label>

                                <input type="text" autocomplete="off" class="form-control" name="other_notes" placeholder="" >

                            </div>

                        </div>
                    </div>       

                    
                    @endif


                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group subcontractor-field" @if( $userType != 'worker' && $userType != 'safetyo' ) style="display: none;" @endif>

                                <label>Company<span class="text-danger">*</span></label>

                                <select name="general_contractor" class="form-control" id="company-select" @if( $userType == 'worker' || $userType == 'safetyo' ) required @endif>
                                    
                                    <option value="" disabled="" selected="">Select</option>

                                    @foreach( $subContractors as $subContractor )

                                    <?php $subContractorName = $subContractor->id; ?>

                                    <option  value="{{ $subContractor->id }}" @if( old('general_contractor') == $subContractorName ) selected @endif>
                                            
                                            {{ $subContractor->company_name }}

                                    </option>  

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group worker-type-field" @if( $userType != 'worker' && $userType != 'safetyo' ) style="display: none;" required="" @endif>

                                <label>Worker Type<span class="text-danger">*</span></label>

                                <select name="worker_type" class="form-control" @if( $userType == 'worker' || $userType == 'safetyo' ) required @endif>
                                    
                                    <option value="">Select</option>

                                    @foreach( $workerTypes as $workerType )

                                    <?php $workerTypeId = $workerType->id; ?>

                                    <option value="{{ $workerType->id }}" @if( old('worker_type') == $workerTypeId ) selected @endif>

                                        {{ $workerType->type }}

                                    </option>

                                    @endforeach

                                </select>
                                 
                            </div>

                        </div>

                    </div>

                    @if( $userType == 'worker' || $userType == 'superint' || $userType == 'safetyo' || $userType == 'subcontractor' )

                    <div class="row">

                        <!--<div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>Rock Spring Safety Training received on:</label>
                                  
                                 <input type="text" class="form-control training" name="training" placeholder="">
                            </div>

                        </div>-->

                        @if( $userType == 'worker' || $userType == 'safetyo' )

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>SSN ID</label>

                                
                               <div class="flex_wrap">  
                                 <span style="color: blue;" id='ssn-id'>COMP</span>							   
                                <input type="hidden" id="ssn-id-pre" value="" name="ssn_id_pre">
                                <input type="text" class="form-control" id="" maxlength="4" name="ssn_id" placeholder="SSN ID">
                                
								</div>
                            </div>

                        </div>

                        <div class="col-md-6">

                                <div class="form-group">

                                    <label>Classification</label>

                                    
                                    <select name="classification" class="form-control">

                                        <option value="">Select classfication</option>
                                        <option value="C">C</option>
                                        <option value="W">W</option>
                                        <option value="P">P</option>
                                        <option value="O">O</option>                                  

                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">

                                <div class="form-group">

                                    <label>RSC Employee Id</label>

                                    
                                    <input type="text" autocomplete="off" class="form-control" name="rsc_employee_id" placeholder="" >

                            </div>

                        </div>

                  

                        @endif
                        @if( $userType == 'superint')
                        <div class="col-md-6">

                                <div class="form-group">

                                    <label>Classification</label>

                                    
                                    <select name="classification" class="form-control">

                                        <option value="">Select classfication</option>
                                        <option value="C">C</option>
                                        <option value="W">W</option>
                                        <option value="P">P</option>
                                        <option value="O">O</option>                                  

                                </select>
                            </div>

                        </div>
                        
                        @endif
                    </div>
                                        @if($userType == 'worker' || $userType == 'safetyo' || $userType == 'superint')
                    <div class="row">
                        <div class="col-md-12 current-uploaded-document">

                            <div class="col-md-12">

                                <div class="title">Safety Training</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                           <div class="col-md-6">

                            <div class="form-group">
                
                  
                                <label>Rock Spring Safety Training received on:</label>

                            

                                 <input type="text" autocomplete="off" class="form-control training" name="training" placeholder="" >

                                 
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>OSHA 30 Training received on:</label>


                                <input type="text" autocomplete="off" class="form-control osha_date " name="osha_date" placeholder="" >

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>CPR Training received on:</label>


                                <input type="text" autocomplete="off" class="form-control cpr_date" name="cpr_date" placeholder="" >

                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">


                                <label>Other</label>

                                <input type="text" autocomplete="off" class="form-control" name="other_notes" placeholder="" >

                            </div>

                        </div>
                    </div>       

                    
                    @endif


                    @endif

                    @if( $userType == 'worker' || $userType == 'safetyo' )

                    <!-- Multiple data -->

                    <div class="row worker-container">

                        <div class="col-md-12 add-more" style="margin-bottom: 10px;">

                            <a href="javascript:void(0);" class="add-notedoc"><i class="fa fa-plus"></i> Add Notes/documents</a>

                        </div>
                        <div class="col-md-12 add_doc_first_row" style="display:flex; align-items:center; padding:0px;">
                        <div class="col-md-6 small_textarea">

                        <textarea class="form-control" placeholder="Notes/Training" name="notes_training[]" maxlength="1000"></textarea>

                        </div>

                        <div class="col-md-3">

                            <input title="Document" type="file" class="form-control doc-field" id="" name="worker_document[]">

                        </div>

                        <div class="col-md-3">
                        <div class="switch_wrap">
						  <p class="visible_text">Visible to Subcontractor?</p>
                        <label class="switch">

                        <input type="checkbox" class="manual-log" value="1"  name="doc_upload[]">

                        <span class="slider round" style="width: 50px;"></span>

                        </label>
						</div>

                        </div>
						</div>
						<div class="clearfix"></div>
						
						

                    </div>

                    <!-- /Multiple data -->

                    @endif

                    @if( $userType == 'subcontractor' ||  $userType == 'foreman' || $userType == 'admin' || $userType == 'superint' || $userType == 'payroll' )

                    <div class="row sub-container">

                        <div class="col-md-12 add-more" style="margin-bottom: 10px;">

                            <a href="javascript:void(0);" class="add-document"><i class="fa fa-plus"></i> Add Documents</a>

                        </div>

                        <div class="col-md-12">

                            <input type="file" class="form-control doc-field" id="" name="document[]">

                        </div>

                    </div>

                    @endif
                    <input type="hidden" name="user_rol" value="{{ $_GET['userType']}}">
                    <button type="submit" id="submit_usr_form" class="btn btn-primary float-right" style="margin-top:15px;">

                        <span class="text">Create</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>


<script type="text/javascript">


/*
     * @Function Name
     *
     *
     */
    $( ".training" ).datepicker();

    $( ".osha_date" ).datepicker();

    $( ".cpr_date" ).datepicker();

    $(document).on("keypress",".training, .osha_date, .cpr_date", function(e){
    e.preventDefault();
    return false;

  }); 


$(document).ready(function() {

    /* Company select */

    $('#company-select').change(function(){

        var preFix = $.trim( $( "#company-select option:selected" ).text() ).replace(/\s+/, "").substring(0, 4).toUpperCase();

        if( preFix == 'SELE' )
        {
            preFix = 'COMP';
        }

        $('#ssn-id').text( preFix );

        $('#ssn-id-pre').val( preFix );

    })

    /* /Company select */

    /* Worker add note document */

    $('.add-notedoc').click(function(){

        if( $( ".additional-notedoc-element" ).length == 9 )
            return false;

        $('.worker-container').append('<div class="col-md-12 additional-notedoc-element"><div class="col-md-6"><textarea class="form-control" placeholder="Notes/Training" id="" name="notes_training[]"></textarea></div><div class="col-md-3"><input title="Document" type="file" class="form-control doc-field" id="" name="worker_document[]"></div><div class="col-md-3"> <div class="switch_wrap"><p class="visible_text">Visible to Subcontractor?</p> <label class="switch"><input type="checkbox" class="manual-log" value="1"  name="doc_upload[]"><span class="slider round" style="width: 50px;"></span></label></div></div><a href="javascript:void(0);" class="document-close"><i class="fa fa-times close-document-button"></i></a></div>');

    })

    /* Add document */

    $('.add-document').click(function(){

        if( $( ".additional-document-element" ).length == 9 )
            return false;

        $('.sub-container').append('<div class="col-md-12 additional-document-element"><input type="file" class="form-control doc-field" id="" name="document[]"><a href="javascript:void(0);" class="document-close close_sub_doc"><i class="fa fa-times close-document-button"></i></a></div>');

    })

    $(document).on("click", ".document-close", function(){

        $(this).parent().remove();

    })

    /* Check each document field for PDF */

    $(document).on("click", ".doc-field", function(){

        if( $(this).val() != '' )
        {

            $(this).val('');
                
        }

    })

    $(document).on("change", ".doc-field", function(){

        var ext = $(this).val().split('.').pop().toLowerCase();

        if( $.inArray(ext, ["pdf"]) == -1 )
        {

            alert('Document should be PDF only');

            $(this).val('');

            return false;

        }


    })

    /* Worker document */

    $('#file_upload').click(function()
    {

        if( $(this).val() != '' )
        {

            $(this).val('');
                
        }

    })

    $('#file_upload').change(function()
    {

        var ext = $('#file_upload').val().split('.').pop().toLowerCase();

        if( $.inArray(ext, ["pdf"]) == -1 )
        {

            alert('Worker document should be PDF only');

            $('#file_upload').val('');

        }

    })

    /* Confirm password */

    var password = document.getElementById("password")
      , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

    /* Confirm password */

     /*
     * @Function Name
     *
     *
     */

     $(document).on('change', '#role-select', function(){ 

        $('.subcontractor-field').hide();

        $('.worker-type-field').hide();

        $('.worker-type-field select').attr( "disabled", "disabled" );

        $('.worker-type-field select').removeAttr( "required" );

        var role = $(this);

        var roleVal = role.val();

        if( roleVal == 'worker' )
        {

            $('.worker-type-field select').removeAttr( "disabled" );

            $('.worker-type-field select').attr( "required", "required" );

            $('.subcontractor-field').show();

            $('.worker-type-field').show();

        }

        if( roleVal == 'foreman' )
        {

            $('.subcontractor-field').show();

        }

     } );

     /*
      * @Function Name
      *
      *
      */
    $('#change-profile-image').on('click', function(event) {

        // $('#update-profile-image input[name="image"]').click();

        $('#myDropzone').click();

        event.preventDefault();

    });

    // $('#update-profile-image input[name="image"]').on('change', function(event) {

    //     previewImage(this, '#profile-image');

    //     $('#remove-profile-preview').show();

    // });

    $('#remove-profile-preview').on('click', function(event) {

        $('.add-text').text('Add');
        
        $('#cropped-image').val('');

        $('#cropped-image-type').val('');

        // $('#update-profile-image input[name="image"]').val('');

        $('#profile-image').attr('src', '{{ asset("images/elements/user.png") }}');

        $(this).hide();

        event.preventDefault();

    });

    $('input[name="phone-number"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });

    $('input[name="payroll_phone"]').inputmask({
        
        mask: [{ "mask": "(###) ###-####"}], 
        
        greedy: false, 
        
        definitions: { 
            
            '#': {
                
                validator: "[0-9]",
                
                cardinality: 1
            
            }
        
        }
        
    });

    $('#create-user').on('submit', function(event) {

        var error = false;

        $('.form-group .error-message').html('');

        var name = $('input[name="first-name"]').val();
        
        var email = $('input[name="email"]').val().toLowerCase();

        var phone_number = $('input[name="phone-number"]').val();
        
        var username = $('input[name="username"]').val();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter first name of user.');

        }

        @if( $userType == 'worker' || $userType == 'safetyo'  )

            if( email == '' && phone_number == '' )
            {

                error = true;

                $('#error-email').html('Please enter either an Email or Phone.');

            }

        @endif

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('First name should contain atleast one alphabetic character.');
            
        }

        if(email != '' && !email.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) {

            error = true;
            
            $('#error-email').html('Enter a valid email address of user.');
            
        }

        if(phone_number != '' && phone_number.replace(/[^0-9]/g, "").length != 10) {

            error = true;
            
            $('#error-phone-number').html('Please enter a valid phone number.');
            
        }

        if(username == '') {

            error = true;

            $('#error-username').html('Please enter username of user.');

        }

        if(username != '' && /\s/.test(username)) {

            error = true;
            
            $('#error-username').html('Username cannot contain spaces.');

        }

        if(username != '' && !/[a-zA-Z]/.test(username)) {

            error = true;

            $('#error-username').html('Username should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();
        }
        
       //  if(error == false){
       //      setTimeout(function(){     
       //          $(document).ready(function(){
       //              $('#submit_usr_form').attr('disabled','disabled');
       //          })
       //      }, 3000);
       // }
        
    });

});

var placeholder_img ="{{ asset('images/elements/user.png') }}";

Dropzone.options.myDropzone = {
    url: '/post',
    addRemoveLinks: true,
    maxFiles: 1,
    acceptedFiles: ".jpeg,.jpg,.png",
    disablePreview: false,

    init: function(file, done) {
        
    this.on("addedfile", function(event) {
        console.log('addedfile')
        if(this.files[0].type != 'image/png' && this.files[0].type != 'image/jpeg' && this.files[0].type != 'image/svg'){
            document.getElementsByClassName('dz-preview')[0].style.display = "none";
            alert("Please upload a image");
            this.removeFile(this.files[0]);
        }
      if (this.files[1]!=null){
        console.log('removeFile')
        this.removeFile(this.files[0]);
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
      }
      
    });
  },
    transformFile: function(file, done) {  
 
     
     
        document.getElementsByClassName('dz-preview')[0].style.display = "none";
        // Create Dropzone reference for use in confirm button click handler
        var myDropZone = this;  
        
        // Create the image editor overlay
        var editor = document.createElement('div');
        editor.style.position = 'fixed';
        editor.style.left = '50px';
        editor.style.right = '50px';
        editor.style.top = '50px';
        editor.style.bottom = '50px';
        editor.style.zIndex = 9999;
        editor.style.width = '500px';
        editor.style.height = '500px';
        editor.className += "profile_image_cropper";
        editor.style.backgroundColor = '#000';
        document.body.appendChild(editor);  
        
        // Create confirm button at the top left of the viewport
        var buttonConfirm = document.createElement('button');
        buttonConfirm.style.position = 'absolute';
        buttonConfirm.style.left = '10px';
        buttonConfirm.style.top = '10px';
        buttonConfirm.style.zIndex = 9999;
        buttonConfirm.textContent = 'Confirm';
        buttonConfirm.className += "profile_image_cropper_button_cancel";
        editor.appendChild(buttonConfirm);

        var buttonCancel = document.createElement('button');
        buttonCancel.style.position = 'absolute';
        buttonCancel.style.left = '200px';
        buttonCancel.style.top = '10px';
        buttonCancel.style.zIndex = 9999;
        buttonCancel.textContent = 'Cancel';
        buttonCancel.className += "profile_image_cropper_button_cancel";
        editor.appendChild(buttonCancel);

        buttonCancel.addEventListener('click', function() {  
                            // Remove the editor from the view
            document.body.removeChild(editor);  
            document.getElementById('profile-image').src = placeholder_img;


        });

        
        buttonConfirm.addEventListener('click', function() {    
            
            // Get the canvas with image data from Cropper.js
            var canvas = cropper.getCroppedCanvas({
                width: 256,
                height: 256
            });    

         
           var profile_img = document.getElementById('profile-image');
           profile_img.src = canvas.toDataURL();

           var image1 = document.getElementById('cropped-image');
           var imagetype = document.getElementById('cropped-image-type');
           image1.value=canvas.toDataURL();
           imagetype.value = file.type.substr(6); 

           

          
           
            


            // Turn the canvas into a Blob (file object without a name)
            canvas.toBlob(function(blob) { 


                // Create a new Dropzone file thumbnail
                myDropZone.createThumbnail(blob,
                myDropZone.options.thumbnailWidth,
                myDropZone.options.thumbnailHeight,
                myDropZone.options.thumbnailMethod,
                false, 
                function(dataURL) {
                
                        // Update the Dropzone file thumbnail
                        myDropZone.emit('thumbnail', file, dataURL);          
                        // Return the file to Dropzone
                        done(blob);      
                    });    
                });   
                // Remove the editor from the view
                document.body.removeChild(editor);

                $('.add-text').text('Change');

               $('#remove-profile-preview').show();
               


            
            });  
            
            // Create an image node for Cropper.js
            var image = new Image();
            image.src = URL.createObjectURL(file);

            editor.appendChild(image);
    
            // Create Cropper.js
            var cropper = new Cropper(image, { aspectRatio: 1 });
            
            }
    

};


</script>

@endsection