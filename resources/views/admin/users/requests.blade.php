@extends('admin.layouts.app')

@section('title', $userTitle . ' Requests')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }
		div#datatable_info {
    float: left;
}
div#datatable_paginate {
    margin-top: 14px;
}

    </style>

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection



@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">
        
        @if( !empty( $userType ) )

        {{ $userTitle }} Requests

        @endif

    </h1>
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body worker_page_wrapper">

                  <div class="custom_table_filter">

                    <!-- Subcontractor filter -->

                    {{--<select id="status-filter">

                        <option disabled selected>-Select Status-</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 2 ) selected  @endif value="2">Active</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 0 ) selected  @endif value="0">Blocked</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 1 ) selected  @endif value="1">Pending</option>

                    </select>--}}

                    <!-- Subcontractor filter -->

                    <!-- Company filter -->

                    <select id="company-filter">

                        <option disabled selected>-Select Company-</option>

                        @foreach( $companiesList as $company )

                          <?php $companyId = $company->id; ?>

                          <option @if( isset( $_REQUEST['company'] ) && @$_REQUEST['company'] == $companyId ) selected  @endif value="{{ $companyId }}">{{ $company->company_name }}</option>

                        @endforeach

                    </select>

                    <!-- Company filter -->

                    <!-- Title filter -->

                    <select id="title-filter">

                        <option disabled selected>-Select Title-</option>

                        @foreach( $titlesList as $title )

                          <?php $titleId = $title->id; ?>

                          <option @if( isset( $_REQUEST['title'] ) && @$_REQUEST['title'] == $titleId ) selected  @endif value="{{ $titleId }}">{{ $title->type }}</option>

                        @endforeach

                    </select>

                    <!-- Title filter -->

                    <!-- Reset filter -->

                    @if(
                    (
                        isset( $_GET['status'] ) ||
                        isset( $_GET['company'] ) ||
                        isset( $_GET['title'] )
                    )
                    )

                    <a href="{{ route('admin.workers.requests') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

                    @endif

                    <!-- Reset filter -->

                </div>
            
                <table class="table table-bordered table-hover" id="datatable">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Name</th>
        
                           <!--  <th scope="col">Email</th> -->

                           @if( $userType == 'worker' )

                           <th scope="col">Company</th>

                           <th scope="col">Registered on</th>

                           <th scope="col">Title</th>

                           @endif
    
                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this user?')) {
        
            event.preventDefault();

        }

    });

    /* ----------------------------------- Next/Prev ----------------------------------- */

    serialize = function(obj) {
      var str = [];
      for (var p in obj)
        if (obj.hasOwnProperty(p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      return str.join("&");
    }
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

    $(document).on("click","a.index-link", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company')+"&wr=yes";
        
        event.preventDefault();
    });
    $(document).on("click","a[title='Edit']", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company')+"&wr=yes";
        
        event.preventDefault();
    });
    $(document).on("click","a[title='View']", function(event){
        var order  = table.order();
        
        top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company')+"&wr=yes";
        
        event.preventDefault();
    });


    var table;

    /* ----------------------------------- Next/Prev ----------------------------------- */

    $(document).on('click', '.btn-action-approve', function(event) {

        if(!confirm('Are you sure you want to approve this worker?')) {
        
            event.preventDefault();

        }

    });

    $(document).on('click', '.btn-action-reject', function(event) {

        if(!confirm('Are you sure you want to reject this worker?')) {
        
            event.preventDefault();

        }

    });

    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');


        table = $('#datatable').DataTable({

            @if( $userType == 'worker' )

           

            @endif

            "bLengthChange": true,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No new worker requests!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,


            "ajax": {

              "url": "{{ route('admin.datatable.users.get.requests') }}",

              "data": function ( d ) {

               d.userType = "{{ $userType }}"
               d.status = "{{ @$_REQUEST['status'] }}"
               d.company = "{{ @$_REQUEST['company'] }}"
               d.title = "{{ @$_REQUEST['title'] }}"
              
            }

            },

            columns: [

                { data: "name", name: "first_name", searchable: true, orderable: true },
                
                // { data: "email", name: "email", searchable: true, orderable: false },

                @if( $userType == 'worker' )

                { data: "company", name: "company", searchable: false, orderable: false },

                { data: "created_at", name: "created_at", searchable: false, orderable: true },

                { data: "worker_type", name: "worker_type", searchable: false, orderable: false },

                @endif
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        function getQueryVariable(url, variable)
        {

          var query = url.substring(1);

          var vars = query.split('&');

          for (var i=0; i<vars.length; i++)
          {

              var pair = vars[i].split('=');

              if (pair[0] == variable)
              {

                return pair[1];

              }
            
          }

             return false;

        }

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#status-filter').change(function()
        {
             var statusVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&status="+statusVal;

             if( getQueryVariable(originalUrl, 'company') )
             {

              qString += "&company="+getQueryVariable(originalUrl, 'company');

             }

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType={{ $userType }}'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#company-filter').change(function()
        {
             var companyVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&company="+companyVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType={{ $userType }}'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#title-filter').change(function()
        {
             var titleVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&title="+titleVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             if( getQueryVariable(originalUrl, 'company') )
             {

              qString += "&company="+getQueryVariable(originalUrl, 'company');

             }

             url += '?userType={{ $userType }}'+qString;

             window.location.href = url;


        })
        $(document).on("click",".status_dropdown", function(){

          $.ajax({
            "url" : "{{route('admin.users.update_status')}}",
            "method":"post",
            "data" : {"user_id": $(this).data("user_id"), "status": $(this).data("status")},
            "success": function(response){

            }
          })
        })
    });

</script>

@endsection