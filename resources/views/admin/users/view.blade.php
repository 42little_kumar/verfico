@extends('admin.layouts.app')
@section('title', $user->first_name .' '. $user->last_name )
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <style>
      .approve-reject-container {
          /*position: absolute;*/
          /*top: 20px;
*/      }
      a.approve-worker {
          background: #d97171;
          color: white;
          padding: 6px 15px;
          border-radius: 5px;
      }
      a.reject-worker {
          background: #d97171;
          color: white;
          padding: 6px 15px;
          border-radius: 5px;
      }
      a.approve-worker {
    background: #6fb26b;
    color: white;
    padding: 6px 15px;
    border-radius: 5px;
}
.card-body {
    padding-bottom: 40px;
}
.icons-guide-container {
    bottom: 10px;
}
    </style>
@endsection
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><span class="project_name_view_heading">User Information <em><a href="{{ route('admin.users.edit', [ 'id' => $user->id ]) }}@if( !empty( @$_GET['from'] ) )?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?><?php if( isset( $_GET['wr'] ) ): ?>&wr=yes<?php endif; ?>@endif"><i class="fas fa-pen"></i></a></em></span></h1>
    
    
</div>


<div class="row justify-content-center trainer_view_wrapper">
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
		<?php 
		 //$next = \App\User::role('worker')->where('status', 2)->where('id', '<', $user->id)->orderBy('id','desc')->first();

         // $previous = \App\User::role('worker')->where('status', 2)->where('id', '>', $user->id)->orderBy('id','asc')->first();
		?>

        @php

           
            
            @endphp

            

            <div class="next-prev-navigation">

                <div class="prev">

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('admin.users.view', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?><?php if( isset( $_GET['wr'] ) ): ?>&wr=yes<?php endif; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('admin.users.view', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&title=<?php echo @$_GET['title']; ?>&status=<?php echo @$_GET['status']; ?>&company=<?php echo @$_GET['company']; ?><?php if( isset( $_GET['wr'] ) ): ?>&wr=yes<?php endif; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls"  style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>

  
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 text-center">
                      @if($user->image)
                      <div class="profile-img" style="background:url({{ asset('uploads/images/users/' . $user->id . '/' . $user->image) }}); background-size:100%; background-position:center;"> 
                        @if($user->image)
                        <img style="display:none;" src="" id="profile-image" alt="profile-image" class="img-profile" >
                        @else
                        <img style="display:none;" src="{{ asset('images/elements/user.png') }}" id="profile-image" alt="profile-image" class="img-profile" >
                        @endif
                      </div>
                      @else
                      <div class="profile-img" style="background:url({{ asset('images/elements/user.png') }}); background-size:100%; background-position:center;"> 
                        @if($user->image)
                        <img style="display:none;" src="" id="profile-image" alt="profile-image" class="img-profile" >
                        @else
                        <img style="display:none;" src="{{ asset('images/elements/user.png') }}" id="profile-image" alt="profile-image" class="img-profile" >
                        @endif
                      </div>
                      @endif
                    </div>
                    <div class="col-lg-9">
                      <div class="user-bio-section">
                        <div class="bio-inner-section">
                        <div class="form-group">
                            <label> <i class="far fa-user"></i> Name  </label>

                            @if( $user->hasRole('Subcontractor') )

                            <p class="businesses_view_text user-profile-name">{{ $user->company_name}}</p>

                            @else

                            <p class="businesses_view_text user-profile-name">{{ $user->first_name }} {{ $user->last_name }} {{(!empty($user->classification)) ? " - ".$user->classification : '' }}</p>


                            @endif

                            
                        </div>

                        @if( $user->hasRole('Worker') && $user->hasRole('Safetyo') )

                         <div class="form-group">
                          <label><i class="far fa-user"></i> Subcontractor </label>
                          <p class="businesses_view_text sub-contrsct-name"><span style="font-weight:normal; font-size:14px;"><b>Company:</b> {{ $user->sub_contractor_name }} </span> 

                            @if( !empty( $user->updated_by ) )

                            <?php 

                            $updateBy = \App\User::find( $user->updated_by );

                            ?>

                             <i class="stamp"> <b>Last edited on:</b>

                              {{ date( 'M d, Y', strtotime( $user->updated_at ) ) }} by 

                              @if( $updateBy->hasRole('Subcontractor') )

                              {{ $updateBy->company_name }}

                              @else

                              {{ $updateBy->name }}

                              @endif

                             </i>
                              <div class="clearfix"></div>
                            @endif
                           
                          </p>
                        </div>

                         @endif

                        <div class="bio-section" style="margin-top:6px;">
                          <div class="row">
                         
                          <div class="col-md-6">

                            @if( $user->hasRole('Subcontractor') )

                            <div class="form-group">
                              <label> <i class="far fa-user"></i> Contact Name </label>
                              <p class="businesses_view_text"><span ><i class="far fa-user"></i> </span>{{ $user->first_name }} {{ $user->last_name }}</p>
                              </div>

                            @endif

                            <div class="form-group">
                              <label> <i class="far fa-envelope"></i> Email </label>
                              <p class="businesses_view_text"><span><i class="far fa-envelope"></i> </span><?php echo empty( $user->email ) ? 'N/A' : $user->email; ?></p>
                              </div>

                            <div class="form-group">
                              <label> <i class="fas fa-mobile-alt"></i> Phone Number </label>
                                @php

                                  $phone_number = ($user->country_code ? $user->country_code . '-' : '') . $user->phone_number;

                                @endphp
                              <p class="businesses_view_text"><span> <i style="transform: rotate(95deg);" class="fa fa-phone" aria-hidden="true"></i> </span> {{ str_replace('-', '', $phone_number) != '' ? $phone_number : 'Not available'}}</p>
                            </div>

                            @if( $user->hasRole('Worker') && !empty( $user->ssn_id ) )
                              <div class="form-group">
                              <label> <i class="fas fa-mobile-alt"></i> SSN ID </label>
                                
                              <p class="businesses_view_text"><span> <i class="fa fa-id-card" aria-hidden="true"></i> </span> {{ $user->ssn_id }}</p>
                            	</div>
                            @endif

                           @if( ( $user->hasRole('Worker') || $user->hasRole('Safetyo') ) && !isset( $_REQUEST['request'] ) )

                           <div class="form-group">
                              
                                <label> <i class="far fa-user"></i> Worker Type </label>
                                <p class="businesses_view_text"> <span><i class="far fa-user"></i></span> {{ ucwords( $user->worker_type_name ) }} </p>
                                
                            </div>

                            
                           
                        
                        

                          @endif

                          @if( $user->hasRole('Worker') || $user->hasRole('Safetyo') )

                          <div class="form-group">
                              
                                <label> <i class="far fa-user"></i> Registered On </label>
                                <p class="businesses_view_text"> <span><i class="fas fa-user-clock"></i></span> {{ \Carbon\Carbon::parse($user->created_at)->tz(str_before(auth()->user()->timezone_manual, ','))->format('jS F Y, h:i A') }} </p>
                                
                            </div>

                          @endif
						  

                          

                          </div>
						  
						  
						  <div class="col-md-6">
                @if(!empty($user->other_text))
                <div class="form-group">
                  
                  <p class="businesses_view_text"> <span>RSC Employee Id: </i></span> {{ $user->rsc_employee_id }} </p>
                  
                </div>

                @endif
                @if(!empty($user->training))
                <div class="form-group">
                  
                  <p class="businesses_view_text"> <span>Rock Spring Training received: </i></span> {{ date("m/d/Y", strtotime($user->training)) }} </p>
                  
                </div>

                @endif

                @if(!empty($user->osha_date))
                <div class="form-group">
                  
                  <p class="businesses_view_text"> <span>OSHA 30 Training received: </i></span> {{ date("m/d/Y", strtotime($user->osha_date)) }} </p>
                  
                </div>

                @endif

                @if(!empty($user->cpr_date))
                <div class="form-group">
                  
                  <p class="businesses_view_text"> <span>CPR Training received: </i></span> {{ date("m/d/Y", strtotime($user->cpr_date)) }} </p>
                  
                </div>

                @endif

                @if(!empty($user->other_text))
                <div class="form-group">
                  
                  <p class="businesses_view_text"> <span>Notes: </i></span> {{ $user->other_text }} </p>
                  
                </div>

                @endif
                

						     @if( $user->hasRole('Subcontractor') )

                          @if( !empty( $user->payroll_first_name ) )

                           <div class="form-group">
                              
                                <label> <i class="far fa-user"></i> Payroll First Name </label>
                                <p class="businesses_view_text"> <span>Payroll First Name: </i></span> {{ ucwords( $user->payroll_first_name ) }} </p>
                                
                            </div>

                          @endif

                          @if( !empty( $user->payroll_last_name ) )

                           <div class="form-group">
                              
                                <label> <i class="far fa-user"></i> Payroll Last Name </label>
                                <p class="businesses_view_text"> <span>Payroll Last Name: </i></span> {{ ucwords( $user->payroll_last_name ) }} </p>
                                
                            </div>

                          @endif

                          @if( !empty( $user->payroll_email ) )

                           <div class="form-group">
                              
                                <label> <i class="far fa-user"></i> Payroll Email </label>
                                <p class="businesses_view_text"> <span>Payroll Email: </i></span> {{ ucwords( $user->payroll_email ) }} </p>
                                
                            </div>

                          @endif

                          @if( !empty( $user->payroll_phone ) )

                           <div class="form-group">
                              
                                <label> <i class="far fa-user"></i> Payroll Phone </label>
                                <p class="businesses_view_text"> <span>Payroll Phone: </i></span></span> {{ ucwords( $user->payroll_phone ) }} </p>
                                
                            </div>

                          @endif


                          @endif
						  </div>

                    
                        <div class="col-md-12">
					    <div class="user_view_wrap" style="padding:0px;">
                          @if( $user->hasRole('Subcontractor') || $user->hasRole('Foreman') || $user->hasRole('Admin') || $user->hasRole('Superint') || $user->hasRole('Payroll') )

                          <div class="col-md-12" style="padding-left:0px;">
                           

                            <div class="form-group">
                              @if( count( $user->documents ) > 0 )

                                <ul style="list-style: none; padding:0px;">

                                @foreach( $user->documents as $get )

                                <li><span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a class="download-icon" href="{{ asset('public/uploads/documents') }}/{{ $user->id }}/{{ $get->document }}" target="_blank">{{ strlen($get->document) > 20 ? substr($get->document,0,20)."..." : $get->document }}</a></li>


                                @endforeach

                                </ul>

                              @else

                                <a style="cursor: default;" class="download-icon" href="javascript:void(0);">No document uploaded </a>

                              @endif

                            </div>
                           
                          </div>

                          @endif
						  </div>
						  </div>
                         
                          </div>                      
                        </div>
                      </div>
                      </div>
                    </div>

                    @if( ( $user->hasRole('Worker') || $user->hasRole('Safetyo') ) && $user->status == 2 )
					
					<div class="col-md-12">
					  <div class="user_view_wrap">
					  <div class="currently_up_title">Currently Uploaded</div>
					   <div class="form-group more-pdf worker_doc_view_wrap">
                              @if( count( $user->workerDocuments ) > 0 )

                              @foreach( $user->workerDocuments as $get )

                                <div class="document-view row">

                                  <div class="col-md-6 form-group">
                                    <label class="doc_label">Notes/Training</label> 
                                    <span class="view_doc_text">{{ empty( $get->notes ) ? 'N/A' : $get->notes }}</span>   
                                </div>

                                <div class="col-md-3 form-group">
                                   <label class="doc_label">Document</label> 
                                   <span>

                                    @if($get->document)
                                        <span class="worker-pdf"><i class="fa fa-file-pdf" aria-hidden="true"></i> </span><a target="_blank" href="{{ asset('public/uploads/documents') }}/{{ $user->id }}/{{ $get->document }}">{{ strlen($get->document) > 20 ? substr($get->document,0,20)."..." : $get->document }}</a>

                                        @else

                                        N/A

                                        @endif

                                   </span>
                                   
                                </div>

                                <div class="col-md-3 form-group">

                                <label class="doc_label">Visible to Subcontractor?</label> 

                                <div class="switch_wrap">
								@if( $get->author == Auth::user()->id )
                                        
                                       <label class="switch">

                                        <input type="checkbox" class="manual-log manual-log-toggle" doc-id="{{ $get->id }}" value="1" @if( $get->show ) checked @endif name="doc_upload[]">

                                        <span class="slider round" style="width: 50px;"></span>

                                        </label>

                                @else

                                    <span class="uploaded-by-text">

                                        Uploaded by Subcontractor

                                    </span>

                                @endif
                                </div>
                                </div>

                                </div>

                                @endforeach
  
                              @else

                                <p class="no-worker-documents">

                                  No documents uploaded

                                </p>

                              @endif

                            </div>
					  </div>
					</div>

          

          @endif

          

                    @if( ( $user->hasRole('Worker') || $user->hasRole('Safetyo') ) && !isset( $_REQUEST['request'] ) )

                      <div class="col-lg-12" style="display:none;">
                        <div class="worker-note">
                           
                          <div class="form-group">
                              <p class="businesses_view_text"> <span>Notes/Training: </span>
                              @if( empty( $user->notes_training ) ) N/A @else {{ $user->notes_training }} @endif
                              </p>
                          </div>
                      </div>
                      </div>

                    @endif

                    @if( $user->hasRole('Worker') )

                    <div class="col-md-12">
                        <div class="approve-reject-container"  @if( $user->status != 1 ) style="display: none;" @endif>
                            <label>
                                <a class="approve-worker" href="{{ route('admin.users.approve', $user->id ) }}">Approve</a>
                                <a class="reject-worker" href="{{ route('admin.users.reject', $user->id ) }}">Reject</a>
                            
                            </label>
                            
                        </div>
                    </div>

                    @endif
                    
                </div>
    
            </div>
        </div>
    </div>
    @if( $user->hasRole('Worker') || $user->hasRole('Foreman') || $user->hasRole('Safetyo') )
    <div class="col-lg-12" @if( $user->status == 1 ) style="display: none;" @endif>
        <h6 class="logs_heading">Logs</h6>
        <div class="card shadow mb-4">
            <div class="card-body">
                <style>
          .project_view_table th{
              position:relative;
          }
          .project_view_table th.sorting_asc, th.sorting_desc, .project_view_table th.sorting_asc, th.sorting_desc, th.sorting, th.sorting{
            cursor:pointer;  
          }
          
          .project_view_table th.sorting_asc:after, th.sorting_desc:after, .project_view_table th.sorting_asc:before, th.sorting_desc:before, th.sorting:before, th.sorting:after{
            position: absolute;
          bottom: 0.9em;
          display: block;
          opacity: 0.3;  
                  
          }
          
          th.sorting_asc:before{
          right: 1em;
            content: "\2191"; 
 cursor:pointer;            
          }
          th.sorting:before{
            right: 1em;
            content: "\2191";  
            cursor:pointer;
          }
          th.sorting_asc:after{
                    right: 0.5em;
                    content: "\2193"; 
 cursor:pointer;                    
          }
          th.sorting:after{
            right: 0.5em;
                    content: "\2193";
           cursor:pointer;                  
          }
          th.sorting_desc:before{
               right: 1em;
             content: "\2191";
              cursor:pointer;
          }
          
          th.sorting_desc:after{
                right: 0.5em;
               content: "\2193"; 
 cursor:pointer;               
          }
          
        </style>
            <div class="table-responsive">
                <table class="table table-bordered table-hover project_view_table" id="datatable-worklog">
  
                    <thead>
    
                        <tr>
                            <th scope="col">Project Name</th>
                            <th scope="col">Project #</th>
                            <th scope="col">Company</th>
                            <th scope="col">Date</th>
        
                            <th style="text-align: center;" scope="col">In</th>
                            <th style="text-align: center;" scope="col">Out</th>
                            <th style="text-align: center;" scope="col">Total Hrs</th>
                            <th scrop="col">Action</th>
        
                        </tr>
  
                    </thead>
                    <tbody></tbody>
                </table>
			</div>	
                {!! getIconGuides() !!}
            </div>
        </div>
    </div>
    @endif
    @if( $user->hasRole('Subcontractor') )
    <div class="col-lg-12">
        <h6 class="logs_heading">Payrolls</h6>
        <div class="card shadow mb-4">
            <div class="card-body">
                  <div class="table-responsive">
                <table class="table table-bordered table-hover" id="datatable">
  
                    <thead>
    
                        <tr>
                            <th scope="col">Title</th>
                            
                            <th scope="col">Uploaded On</th>
                            <th scope="col">Payroll Document</th>
                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>
                    <tbody></tbody>
                </table>
				</div>
            </div>
        </div>
    </div>
    @endif

    @if( $user->hasRole(['Worker','Safetyo']) && $user->status == 2 )

     <div class="col-lg-12">
        <h6 class="logs_heading safety-strikes-head">Safety Strikes
          @if( \DB::table('safety_strikes')->whereNull('deleted_at')->where('worker_id', $user->id)->exists() )
          <span><a class="btn export-btn" href="{{ route('strike.export', $user->id) }}">Export</a></span>
          @endif
        </h6>
        <div class="card shadow mb-4">
            <div class="card-body">

         
             <div class="form-group more-pdf strike-wrap worker_doc_view_wrap strike-table-wrapper">
                              <?php 

                              $subworkers = null;

                              if( $user->hasRole('Subcontractor') )
                              {

                                  $subworkers = \App\User::where('parent', $user->id)->where('status', 2)->count();

                             
                              }


                              if( \DB::table('safety_strikes')->where('worker_id', $user->id)->exists() ): 


                                ?>

                              <?php
                              if( $user->hasRole('Subcontractor') )
                              {

                                  $subworkers = \App\User::where('parent', $user->id)->where('status', 2)->pluck('id')->toArray();

                                  $strikes = \App\Strike::whereIn('worker_id', $subworkers)->get();
                              }
                              else
                              {
                                  $strikes = \App\Strike::where('worker_id', $user->id)->get();
                              }
                              

                              ?>

                              @if( count( $strikes ) == 0 )
                            <p class="no-worker-documents">

                                  No safety strikes

                                </p>
                            @else

                          <table>

                            <th>Date</th>
                            <th>Job</th>
                            <th>Worker</th>
                            <th>Category</th>
                            <th>Violation Description</th>
                            <!--<th>Media</th>-->
                            <th>Issued By</th>
                            <th>Action</th>
                            

                            @foreach( $strikes as $g )

                              <tr>

                                <td style="width:150px;">{{ date("m-d-Y", strtotime($g->strike_date)) }}</td>
                                
                                <td style="width:200px;">

                                   @if( empty( $g->name ) )

                                    N/A

                                  @else

                                    @if( is_numeric( $g->name ) )

                                      @if( \App\Project::where('id', $g->name)->exists() )

                                        {{ \App\Project::where('id', $g->name)->first()->job_name }}

                                      @else

                                      ({{ $g->name }}) Invalid Project Id

                                      @endif

                                    @else

                                      {{ $g->name }}

                                    @endif

                                  @endif

                                </td>

                                <td style="width:200px;">
                                    
                                  @if( \DB::table('users')->where('id', $g->worker_id)->exists() )

                                    {{ \DB::table('users')->where('id', $g->worker_id)->first()->name }}

                                  @else

                                    N/A

                                  @endif

                                </td>

                                <td style="width:150px;">
                                   @if( !empty( $g->category ) )
                                                   
                                                      {{ $g->category }}
                                                   
                                                    @else

                                                    N/A
                                                    
                                                    @endif
                                  </td>

                                <td style="width:300px;">{{ $g->description }}</td>

                                <td style="width:150px;">
                                   

                                  <?php

                                  $issuedBy = 'N/A';

                                  if(\DB::Table('users')->where('id', $g->created_by)->exists())
                                  {
                                    $issuedBy = \DB::Table('users')->where('id', $g->created_by)->first()->name;
                                  }  

                                  echo $issuedBy;

                                  ?>

                                  </td>
                                <!--<td style="width:210px;">

                                  @if( !empty( $g->media ) && $g->isvideo )
                                  <video style="border-radius:5px;"  width="150" controls>
                                    <source src="<?php echo url('/'); ?>/public/uploads/strike/{{$g->media}}" type="video/mp4">
                                  </video>
                                  @endif

                                  @if( !empty( $g->media ) && !$g->isvideo )
                                 
                                    <img width="150" style="border-radius:5px;" src="<?php echo url('/'); ?>/public/uploads/strike/{{$g->media}}">
                                 
                                  @endif

                                  @if( empty( $g->media ) )
                                    <div class="no-media-text">No media uploaded</div>
                                  @endif

                                </td>-->
                
                                <td style="width:150px;">
                                  
                                  <a style="text-transform: none;" onclick="return confirm('Are you sure you want to delete this strike?')" href="{{ route('admin.strike.delete', $g->id) }}"><i  style="color: red;" class="fa fa-trash"></i></a>
                                  <a style="text-transform: none;" href="{{ route('admin.strike.edit', [$g->id, $user->id]) }}"><i class="fa fa-edit"></i></a>

                                </td>

                              </tr>

                            @endforeach

                            

                          </table>

                          @endif
  
                              <?php else: ?>

                                <p class="no-worker-documents">

                                  No safety strikes

                                </p>

                              <?php endif; ?>

                            </div>
            </div>
          </div>

          @endif

    <input type="hidden" value='0' class="inenter" name="inenter">
<input type="hidden" value='0' class="outenter" name="outenter">
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

 <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

    var ls = localStorage['workerIds'];

    if( ls )
    {
    
    var lsa = $.parseJSON(ls);

    console.log( lsa );

    var val = "{{ $user->id }}";

    var next = lsa.indexOf(val)+1;

    var previous = lsa.indexOf(val)-1;
    

    if(lsa[previous]  )
    {

      $('.prev-ls a').attr('href', "{{ route('admin.users.view') }}/"+lsa[previous])

   
    }

    if(lsa[next]  )
    {
      $('.next-ls a').attr('href', "{{ route('admin.users.view') }}/"+lsa[next])
    }
    }
    else
    {
      $('.prev-ls').hide();
      $('.next-ls').hide();

      $('.prev').show();
      $('.next').show();
    }

    /*$.ajax({
        url: "{{ route( 'worker.get.order.details' ) }}",
        type: "GET",
        data: { cl : cl, od : od , userId : {{ $user->id }} },
        success: function(data){
            console.log(data);
        }
      });*/

    $('.manual-log-toggle').click(function(){

        var showVal = 0;
        var docId = $(this).attr('doc-id');

        if( $(this).is(':checked') )
        {
            showVal = 1;
        }

        $.ajax({
        url: "{{ route( 'worker.document.status' ) }}",
        type: "GET",
        data: { id : docId, showVal : showVal },
        success: function(data){
            
        }
        });

    })

    $(document).on('click', '.btn-action-delete', function(event) {
        if(!confirm('Are you sure you want to delete this log?')) {
        
            event.preventDefault();
        }
    });
    $(document).on('click', '.approve-worker', function(event) {
        if(!confirm('Are you sure you want to approve this worker?')) {
        
            event.preventDefault();
        }
    });
    $(document).on('click', '.reject-worker', function(event) {
        if(!confirm('Are you sure you want to reject this worker?')) {
        
            event.preventDefault();
        }
    });
    /*
     * @Function Name
     *
     *
     */
    $(document).on('focus', '.date-input', function(event) {
        $(this).datepicker(
          {
          dateFormat: 'MM dd, yy' ,
            onSelect: function (dateText, inst) {
                 
                }
          }
        ).val();
    });
    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-edit', function(event) {
        var thisRow = $(this).parent().parent().parent();

        $('.inenter').val( thisRow.find('.edit-punch-in-time input').val() )
        $('.outenter').val( thisRow.find('.edit-punch-out-time input').val() )
        console.log( thisRow );
        /*-------------------------------------------- Seperated --------------------------------------------*/
        $('.btn-action-edit').show();
        $('.btn-action-update').hide();
        $( this ).hide();
        thisRow.find('.btn-action-update').show();
        $('.edit-punch-in-time').slideUp('fast');
        $('.edit-punch-out-time').slideUp('fast');
        $('.edit-date').slideUp('fast');
        thisRow.find('.edit-punch-in-time').slideDown('fast');
        thisRow.find('.edit-punch-out-time').slideDown('fast');
        //thisRow.find('.edit-date').slideDown('fast');
        /*-------------------------------------------- Seperated --------------------------------------------*/
    })
    /*
     * @Function Name
     *
     *
     */
     $(document).on('click', '.btn-action-update', function(event) {
        var thisRow = $(this).parent().parent().parent();
        var logId = $(this).attr('log-id');
        var punchIn = thisRow.find('.edit-punch-in-time input').val();
        var punchOut = thisRow.find('.edit-punch-out-time input').val();
        var dateVal = thisRow.find('.edit-date input').val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        /*-------------------------------------------- Seperated --------------------------------------------*/
        $.ajax({
            type:'POST',
            url:'{{ route("admin.log.update") }}',
            data: { 
                _token: CSRF_TOKEN,
                logId: logId, 
                punchIn: punchIn,
                punchOut: punchOut,
                oldin: $('.inenter').val(), 
                oldout: $('.outenter').val(), 
                dateVal: dateVal 
            },
            success:function( data )
            {
                var response = JSON.parse( data );
                var returnedPunchIn = response.data.punch_in_time;
                var returnedPunchOut = response.data.punch_out_time;
                var returnedDateVal = response.dateVal;
                thisRow.find('.punch-in-time').text( returnedPunchIn );
                thisRow.find('.punch-out-time').text( returnedPunchOut );
                thisRow.find('.date-text').text( returnedDateVal );
                thisRow.find('.total-hours-class').text( response.total_time );
                thisRow.find('.edit-punch-in-time').slideUp('fast');
                thisRow.find('.edit-punch-out-time').slideUp('fast');
                thisRow.find('.edit-date').slideUp('fast');
                $('.btn-action-edit').show();
                $('.btn-action-update').hide();

                if( response.data.out_type == 5 )
                {
                  var imgv = "{{ asset('public/images/admin_out.png') }}";
                  thisRow.find('.imgplaceout').html('<img width="20" class="log-icon out-image" src="'+imgv+'">')
                  thisRow.find('.out-image').attr('src', "{{ asset('public/images/admin_out.png') }}");
                }

                if( response.data.in_type == 5 )
                {
                  var imgv = "{{ asset('public/images/admin_out.png') }}";
                  thisRow.find('.imgplacein').html('<img width="20" class="log-icon in-image" src="'+imgv+'">')
                  thisRow.find('.in-image').attr('src', "{{ asset('public/images/admin_out.png') }}");
                }


            }
        })
     });
    /*
     * @Function Name
     *
     *
     */
    function initialize_database() {

      $.fn.dataTable.moment('MM-DD-YYYY');
      $.fn.dataTable.moment('h:mm a');
      
        $('#datatable-worklog').DataTable({

          @if( isset( $_REQUEST['rowcount'] ) )

            "iDisplayLength": "{{ $_REQUEST['rowcount'] }}",

            @endif

          "order": [[ 3, 'desc' ], [ 4, 'desc' ]],
            "bLengthChange": true,
            language: {
              sEmptyTable: "No data available.",
            sLengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },
            processing: true,
            serverSide: false,
            "ajax": {
              "url": "{{ route('admin.datatable.logs.get') }}",
              "data": function ( d ) {
               d.userId = {{ $user->id }}
              
            }
            },
            columns: [
                { data: "project_name", name: "project_name", searchable: true, orderable: true },
                { data: "project_number", name: "project_number", searchable: true, orderable: true },
                { data: "subcontractor", name: "subcontractor", searchable: true, orderable: true },
                { data: "date", name: "date", searchable: false, orderable: true },
                
                { data: "punch_in", name: "punch_in", searchable: true, orderable: true },
                
                { data: "punch_out", name: "punch_out", searchable: false, orderable: true },
                { data: "total_hours", name: "total_hours", searchable: false, orderable: true },
                { data: "action", name: "action", searchable: true, orderable: false },
            ],
        });
        /*-------------------------------------------- Seperator --------------------------------------------*/
        $('#datatable').DataTable({
            "bLengthChange": true,
            language: {
            sLengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },
            processing: true,
            serverSide: true,
            "ajax": {
              "url": "{{ route('admin.datatable.payroll.get') }}",
              "data": function ( d ) {
               d.subContractorId = {{ $user->id }}
              
            }
            },
            columns: [
                { data: "title", name: "title", searchable: true, orderable: true },
                { data: "uploaded_on", name: "uploaded_on", searchable: true, orderable: true },
                
                { data: "payroll_file", name: "payroll_file", searchable: true, orderable: true },
                { data: "action", name: "action", searchable: true, orderable: false },
            ],
        });
    }
    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {
        initialize_database();

        $(document).on("click",".status_dropdown", function(){

          $.ajax({
            "url" : "{{route('admin.users.update_status')}}",
            "method":"post",
            "data" : {"user_id": $(this).data("user_id"), "status": $(this).data("status")},
            "success": function(response){

            }
          })
        })
    });
</script>
@endsection