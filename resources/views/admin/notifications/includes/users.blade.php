<div class="form-group">

    <label>User(s)<span class="text-danger">*</span></label>

    <select class="form-control" name="users[]" data-live-search="true" multiple required>

        @foreach($users as $user)

        <option title="{{ $user->first_name }} {{ $user->last_name }}" value="{{ $user->id }}">{{ $user->first_name }} {{ $user->last_name }} ({{ $user->email }})</option>

        @endforeach

    </select>

</div>