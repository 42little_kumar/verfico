@extends('admin.layouts.app')

@section('title', 'Push Notification')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">

    <h1 class="h3 mb-0 text-gray-800">Push Notification</h1>

</div>

<div class="row justify-content-center">

    <div class="col-lg-10">

        <div class="card shadow mb-4">

            <div class="card-header py-3">

                <h6 class="m-0 font-weight-bold text-primary">New Notification</h6>

            </div>

            <div class="card-body">

                <form action="{{ route('admin.notifications') }}" method="POST">

                    @csrf

                    <div class=row>

                        <div class="col-12">
                            
                            <label>Mode<span class="text-danger">*</span></label>

                        </div>

                        <div class="form-group col-md-6">

                            <label for="mode-email">

                                <input class="form-control" type="radio" id="mode-email" name="mode" style="width: auto; display: inline-block;" value="email" required> Email

                            </label>

                        </div>

                        <div class="form-group col-md-6">

                            <label for="mode-push-notification">

                                <input class="form-control" type="radio" id="mode-push-notification" style="width: auto; display: inline-block;" name="mode" value="push-notification" required> Push Notification

                            </label>

                        </div>

                    </div>

                    <div class="row">
            
                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Group<span class="text-danger">*</span></label>

                                <select class="form-control" name="group" required>

                                    <option value="">Select Group</option>
                                    
                                    <option value="all">All User(s)</option>
                                    
                                    <option value="specific">Specific User(s)</option>

                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="row">
            
                        <div class="col-md-12" id="notification-users">



                        </div>

                    </div>
        
                    <div class="row">
            
                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Title<span class="text-danger">*</span></label>

                                <input class="form-control" name="title" placeholder="Notification title" maxlength="40" required>

                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Message<span class="text-danger">*</span></label>

                                <textarea class="form-control" name="message" placeholder="Notification message (max. 200 characters)" maxlength="200" rows="5" required></textarea>

                            </div>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Send</span>

                    </button>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

    $(document).ready(function(){
        
        $('input[name="mode"]').on('change', function () {

            var value = $(this).val();

            var group = $('select[name="group"] option:checked').val();

            if(group == 'specific') {

                $.ajax({

                    url: "{{ route('admin.notifications.users') }}",

                    type: 'POST',

                    dataType: 'json',

                    data: { 'mode' : value, '_token' : '{{ csrf_token() }}' },

                    success: function(response) {

                        $('#notification-users').html('');

                        if(response.status == true) {

                            $('#notification-users').html(response.data);

                            $('select').selectpicker();

                        }

                    }

                });

            }

        });

        $('select[name="group"]').on('change', function () {

            var value = $(this).val();

            var mode = $('input[name="mode"]:checked').val();

            $('#notification-users').html('');

            if(mode != undefined && value == 'specific') {

                $.ajax({

                    url: "{{ route('admin.notifications.users') }}",

                    type: 'POST',

                    dataType: 'json',

                    data: { 'mode' : mode, '_token' : '{{ csrf_token() }}' },

                    success: function(response) {

                        if(response.status == true) {

                            $('#notification-users').html(response.data);

                            $('select').selectpicker();

                        }

                    }

                });

            }

        });

    });

</script>
@endsection