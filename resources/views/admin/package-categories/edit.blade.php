@extends('admin.layouts.app')

@section('title', 'Edit Category: ' . $category->name)

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Edit Category: {{ $category->name }}</h1>

    <a href="{{ route('admin.package-categories') }}" class="back-link">< Back to Categories</a>
    
</div>

<div class="row justify-content-center">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            <div class="card-header">
            
                <h6 class="m-0 font-weight-bold text-primary">Category Information</h6>
            
            </div>

            <div class="card-body">

                <form action="{{ route('admin.package-categories.edit', [ 'id' => $category->id ]) }}" method="POST" id="edit-category">

                    @csrf

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Name<span class="text-danger">*</span></label>

                                <input type="text" name="name" placeholder="Name" maxlength="50" value="{{ $category->name }}" class="form-control" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        @if($category->parent)

                            <div class="col-md-12">

                                <div class="form-group">

                                    <label>Parent Category</label>

                                    <select class="form-control" name="parent">

                                        <option value="">-Select Parent Category-</option>

                                        @foreach($categories as $cat)

                                        <option value="{{ $cat->slug }}" @if($category->parent_category && $category->parent_category->slug == $cat->slug) selected @endif>{{ $cat->name }}</option>

                                        @endforeach

                                    </select>

                                </div>

                            </div>

                        @endif

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Description</label>

                                <textarea name="description" class="form-control" placeholder="Description">{{ $category->description }}</textarea>

                            </div>

                        </div>

                    </div>

                    <div class="row justify-content-center">

                        <div class="col-md-12">

                            <button type="submit" class="btn btn-primary float-right">

                                <span class="text">Update</span>

                            </button>

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

$(document).ready(function() {

    $('#edit-category').on('submit', function(event) {

        var error = false;

        $('#edit-category .form-group .error-message').html('');

        var name = $('#edit-category input[name="name"]').val().trim();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter name of category.');

        }

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('Category name should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();

        }

    });

});

</script>

@endsection