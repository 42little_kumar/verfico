@extends('admin.layouts.app')

@section('title', 'Package Categories')

@section('content')

@if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write package-categories'))

    @include('admin.package-categories.includes.create')

@endif

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Package Categories</h1>

    @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write package-categories'))

    <a href="javascript:void();" class="float-right btn-create" data-toggle="modal" data-target="#create-category">

        <i class="fas fa-plus-circle"></i>

    </a>

    @endif
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            <div class="card-header text-right filter-header-wrap">

                <form method="GET" id="page-filter-form" class="justify-content-between">
                   
                    <div class="fliter_wrap_left">

                        <div class="input-group">
                
                            <select class="selectpicker" name="sort-by">
                    
                                <option value="">-Sort By-</option>

                                <option value="name-ASC" @if(request()->query('sort-by') && request()->query('sort-by') == 'name-ASC') selected @endif>Name (Ascending)</option>

                                <option value="name-DESC" @if(request()->query('sort-by') && request()->query('sort-by') == 'name-DESC') selected @endif>Name (Descending)</option>                        
                    
                            </select>
                
                        </div>
                    
                        <div class="input-group">
                            
                            <input type="text" name="search" class="form-control bg-light border-0 small" value="{{ request()->query('search') }}" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                            
                        </div>
                
                        <div class="input-group" style="width:auto;">
                        
                            <div class="input-group-append">
                        
                                <button class="btn btn-primary" type="submit">
                            
                                    <i class="fas fa-search fa-sm"></i> Search

                                </button>
                        
                            </div>


                            @if(request()->query('sort-by') != '' || request()->query('search') != '')

                            <div class="reset-group">

                                <a class="btn-filter-reset" href="{{ route('admin.package-categories') }}"><i class="fas fa-sync-alt fa-sm"></i> Reset</a>

                            </div>

                            @endif
                        
                        </div>
                
                    </div>
                   
                    <div class="line_limit_wrap">
                    
                        <div class="input-group" style="width:78px;" title="Select Limit">
                    
                            <select name="limit">
                        
                                <option value="10" @if(request()->query('limit') && request()->query('limit') == 10) selected @endif>10</option>

                                <option value="25" @if(request()->query('limit') && request()->query('limit') == 25) selected @endif>25</option>

                                <option value="50" @if(request()->query('limit') && request()->query('limit') == 50) selected @endif>50</option>

                                <option value="75" @if(request()->query('limit') && request()->query('limit') == 75) selected @endif>75</option>

                                <option value="100" @if(request()->query('limit') && request()->query('limit') == 100) selected @endif>100</option>

                            </select>
                
                        </div>
                
                    </div>

                </form>
            
            </div>
            
            <div class="card-body">

                <table class="table table-bordered table-hover categories_table_wrap">
  
                    <thead>
    
                        <tr>

                            <th scope="col" width="30%">Name</th>
        
                            <th scope="col" width="30%">Description</th>

                            @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write package-categories'))
    
                            <th scope="col" width="20%" class="text-center">Action</th>

                            @endif
        
                        </tr>
  
                    </thead>
  
                    <tbody>

                        @if($categories->isEmpty())

                        <tr>

                            <td colspan="3" class="text-center">No records found.</td>

                        </tr>

                        @else

                            @foreach($categories as $category)
    
                            <tr class="@if(Session::has('status-category') && $category->id == Session::get('status-category')) focused_row  @endif">
            
                                <td><b>{{ str_limit($category->name, 75) }}</b></td>

                                <td>{{ $category->description ? str_limit($category->description, 75) : 'No description available.' }}</td>

                                @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write package-categories'))
        
                                <td>

                                    <div class="table-action">

                                        <a href="{{ route('admin.package-categories.edit', [ 'id' => $category->id ]) }}" class="btn btn-sm btn-primary btn-circle" title="Edit">
                    
                                            <i class="fas fa-pencil-alt"></i>
                    
                                        </a>

                                        <a href="{{ route('admin.package-categories.delete', [ 'id' => $category->id ]) }}" data-type="category" class="btn btn-sm btn-danger btn-circle btn-action-delete" title="Delete">
                        
                                            <i class="fas fa-trash"></i>
                    
                                        </a>

                                    </div>

                                </td>

                                @endif
        
                            </tr>

                            @endforeach

                        @endif
  
                    </tbody>

                </table>

                @if($categories->isNotEmpty())

                <div class="pagination-wrap">

                        <span class="entries float-right" style="margin-top: 7px">

                            Showing {{ $categories->firstItem() }} to {{ $categories->lastItem() }} of total {{ $categories->total() }} entries

                        </span>

                    @php
                    
                        $paginate = [];

                        if(request()->query('search')) {

                            $paginate['search'] = request()->query('search');

                        }

                        if(request()->query('limit')) {

                            $paginate['limit'] = request()->query('limit');

                        }

                        if(request()->query('sort-by')) {

                        $paginate['sort-by'] = request()->query('sort-by');

                        }

                    @endphp

                    {{ $categories->appends($paginate)->links() }}

                    <div class="clearfix"></div>

                </div>

                @endif

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

$(document).ready(function() {

    $('#create-category form').on('submit', function(event) {

        var error = false;

        $('#create-category .form-group .error-message').html('');

        var name = $('#create-category input[name="name"]').val().trim();

        if(name == '') {

            error = true;

            $('#error-name').html('Please enter name of category.');

        }

        if($.isNumeric(name)) {

            error = true;

            $('#error-name').html('Category name should contain atleast one alphabetic character.');
            
        }

        if(error) {

            event.preventDefault();

        }

    });

    $('.btn-action-delete').on('click', function(event) {

        var type = $(this).attr('data-type');

        if(!confirm('Are you sure you want to delete this ' + type + '?')) {

            event.preventDefault();

        }

    });

    @if(Session::has('open-modal'))
    
        $('a[data-target="#create-category"').click();

    @endif

});

</script>

@endsection