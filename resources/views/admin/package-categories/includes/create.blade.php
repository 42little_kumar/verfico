<div class="modal fade" id="create-category" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
        
            <div class="modal-header">
            
                <h5 class="modal-title">New Category</h5>
                
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                
                    <span aria-hidden="true">×</span>
                
                </button>
            
            </div>

            <form action="{{ route('admin.package-categories') }}" method="POST">
            
                <div class="modal-body">                

                    <div class="row">

                        <div class="col-12">
        
                            @csrf

                            <div class="form-group">

                                <label>Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="name" placeholder="Name" maxlength="50" value="{{ old('name') }}" required>

                                <span id="error-name" class="error-message text-danger"></span>

                            </div>

                            <div class="form-group">

                                <label>Description</label>

                                <textarea class="form-control" name="description" placeholder="Description">{{ old('description') }}</textarea>

                            </div>

                            {{-- <div class="form-group">

                                <label>Parent Type</label>

                                <select class="form-control" name="parent">

                                    <option value="">Select</option>

                                    @foreach(\App\Category::where('type', 'session')->whereNull('parent')->get() as $category)

                                        <option value="{{ $category->slug }}">{{ $category->name }}</option>

                                    @endforeach

                                </select>

                            </div> --}}

                        </div>

                    </div>
                
                </div>
                
                <div class="modal-footer">
                
                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Create</span>
                
                    </button>
                
                </div>

            </form>
        
        </div>
    
    </div>

</div>