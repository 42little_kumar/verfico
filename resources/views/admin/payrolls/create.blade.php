@extends('admin.layouts.app')

@section('title', 'New Payroll')

@section('styles')

<!--<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">-->

<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">New Payroll <span><a class="sample-file-button" href="{{ asset('uploads/sample/sample.xlsx') }}">Sample Report</a></span></h1>

    <a href="{{ route('admin.payrolls') }}" class="back-link">< Back to Payrolls</a>
    
</div>

<form action="{{ route('admin.payroll.save') }}" id="create-payroll" method="POST" enctype="multipart/form-data">
            
    @csrf

    <div class="row">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header py-3">

                    <h6 class="m-0 font-weight-bold text-primary">Payroll Information</h6>
                
                </div>

                <div class="card-body">

                    <div class="row">
                    

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Select Pay Period End Date<span class="text-danger">*</span></label>

                                <input type="text" class="form-control week-ending readonly" name="title" placeholder="Select Pay Period End Date" value="{{ old('title') }}" maxlength="50" autocomplete="off" required>

                                <input type="hidden" class="start-date" name="start_date">

                                <input type="hidden" class="end-date" name="end_date">

                                <span id="error-title" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Payroll File<span class="text-danger">*</span></label>

                                <input type="file" class="form-control" id="file_upload" name="payroll_file" required="" />

                                <span id="error-job-payroll-file" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                    

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Subcontractor<span class="text-danger">*</span></label>

                                <select name="subContId" class="form-control" required>

                                    <option disabled selected value="">-Select-</option>

                                    @foreach( $subConts as $get )

                                    <option value="{{ $get->id }}">{{ $get->company_name }}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                     
                    </div>

                    <div class="row">

                        <div class="col-md-12" style="display: none;">

                            <div class="form-group">

                                <span id="error-general-contractor" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-12">

                            <div class="form-group">

                                <label>Description</label>

                                <textarea name="description" class="form-control">{{ old('description') }}</textarea>

                                <span id="error-description" class="error-message text-danger"></span>

                            </div>

                        </div>

                    </div>

                     <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <input type="checkbox" class="agreement-check">

                                <span>

                                    In accordance with the provisions of the Master Subcontract Agreement entered into with Rock Spring Contracting, LLC, I/we certify that any and all of our workers who worked on Rock Spring Contracting jobs during the dates specified were properly classified as employees and were paid minimum wage and overtime (if applicable).  I/we further certified that we comply with applicable sick and safe leave laws.  I/we further certify that we have not used any lowered tiered subcontractors for labor on Rock Spring Contracting jobs for the dates specified below.  I/we further certify that we have maintained all records required for compliance under wage and hour laws.

                                </span>

                            </div>

                        </div>

                    </div>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Create Payroll</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<!--<script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>-->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

    $(".readonly").on('keydown paste', function(e){
        e.preventDefault();
    });

$(document).ready(function() {

    /*
    *
    * @Datepicker
    *
    */
    $('.week-ending').datepicker({

        dateFormat: 'MM dd, yy' ,

            /*beforeShowDay: function(date)
            {

                   var show = false;

                   if( date.getDay() == 5 ) show=true

                   return [show];

            },*/

            onSelect: function (dateText, inst) {

                var newdate = new Date(dateText);

                const monthNames = ["January", "February", "March", "April", "May", "June",
                  "July", "August", "September", "October", "November", "December"
                ];

                var endDate = newdate.setDate(newdate.getDate() - 6);

                var dd = newdate.getDate();

                var mm = monthNames[newdate.getMonth()];

                var y = newdate.getFullYear();

                endDate = mm+' '+dd+', '+y;

                $('.start-date').val(endDate);

                $('.end-date').val(dateText);

                $('.week-ending').val( endDate+' to '+dateText );
                 
                }

    })

    $('#file_upload').click(function()
    {

        if( $(this).val() != '' )
        {

            $(this).val('');
                
        }

    })

    /*
    *
    * @Agreement
    *
    */
    $('#create-payroll').on('submit', function(event) {

        if( !$('.agreement-check').is(':checked') )
        {

            alert( 'Please accept the agreement!' );

            return false;

        }


    });

    $('#file_upload').change(function()
    {

        var ext = $('#file_upload').val().split('.').pop().toLowerCase();

        if( $.inArray(ext, ["xlsx","xls"]) == -1 )
        {

            alert('File should be only xlsx or xls');

            $('#file_upload').val('');

        }

    })


});

</script>

@endsection