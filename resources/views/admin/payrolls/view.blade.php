@extends('admin.layouts.app')

@section('title', 'Payroll: ' . $payrollData->title)

@section('styles')
<link href="{{ asset('css/OverlayScrollbars.min.css') }}" rel="stylesheet"> 
<style>

tbody tr:last-child {
   font-weight:bold; color:black;
   background: white !important;
   color: inherit !important;
}

.tox-notifications-container {
    display: none !important;
}

.approve-button {
   background: #509c50;
   color: white;
   padding: 9px 10px;
   position: absolute;
   z-index: 99;
   right: 35px;
   margin-top: 8px;
   color: white !important;
   border-radius: 5px;
   min-width: 150px;
   text-align: center;
}

.payroll-excel-report-table-wrapper tr td.sheet-error {
   background: red !important;
   color: white !important;
}

.approve-button.disabled {
   background: #cac9d4 !important;
   cursor: not-allowed;
   color: white !important;
}

.approved {
   background: #cac9d4 !important;
   cursor: not-allowed;
   color: white !important;
   padding: 9px 10px;
   position: absolute;
   z-index: 99;
   right: 36px;
   margin-top: 8px;
   border-radius: 5px;
   min-width: 150px;
   text-align: center;
}
.payroll-excel-report-table-wrapper  tr td{
background:#fff;	
}

.payroll-excel-report-table-wrapper .yellow-row td
{
  background: yellow;
}

.payroll-excel-report-table-wrapper tr.logs-not-equal {
   background: #f17575;
   color: white;
}

.payroll-excel-report-table-wrapper tr td.less-hours, td.less-gross, .payroll-excel-report-table-wrapper tr td.less-rate {
   background: red !important;
   color: white;
}

.fa-times-circle {
   color: red;
}

ul {
   list-style: none;
   padding: 0px;
   margin: 0px;
   font-size: 10px;
   color: red;
}
ul li{
font-size:13px;	
}
a.report-alert {
   position: absolute;
   z-index: 999;
   right: 190px;
   margin-top: 16px;
}


			label{ text-align: left }
			button[type="submit"] {
				float: right;
				background: #404094;
				border: 0px;
				border-radius: 4px;
				width: 100%;
				color: white;
			}
			
			
/*****/
.payroll-excel-report-table-wrapper {
  position: relative;
  width:100%;
  z-index: 1;
  margin: auto;
  overflow: auto;
}

.payroll-excel-report-table-wrapper table {
  width: 100%;
  margin: auto;
  border-collapse: separate;
  border-spacing: 0;
}

.payroll-excel-report-table-wrapper table th:nth-child(9), .payroll-excel-report-table-wrapper table th:nth-child(10),
.payroll-excel-report-table-wrapper table th:nth-child(11), .payroll-excel-report-table-wrapper table th:nth-child(12),
.payroll-excel-report-table-wrapper table th:nth-child(13),
.payroll-excel-report-table-wrapper table td:nth-child(9), .payroll-excel-report-table-wrapper table td:nth-child(10), .payroll-excel-report-table-wrapper table td:nth-child(11),
.payroll-excel-report-table-wrapper table td:nth-child(12),.payroll-excel-report-table-wrapper table td:nth-child(13){
display: none !important;
}

.payroll-excel-report-data-show table th:nth-child(9), .payroll-excel-report-data-show table th:nth-child(10),
.payroll-excel-report-data-show table th:nth-child(11), .payroll-excel-report-data-show table th:nth-child(12),
.payroll-excel-report-data-show table th:nth-child(13),
.payroll-excel-report-data-show table td:nth-child(9), .payroll-excel-report-data-show table td:nth-child(10), .payroll-excel-report-data-show table td:nth-child(11),
.payroll-excel-report-data-show table td:nth-child(12), .payroll-excel-report-data-show table td:nth-child(13){
 display:table-cell !important;;
}
.payroll-excel-report-table-wrapper table th:nth-child(8){
width:119px !important;	
}
.payroll-excel-report-data-show table th:nth-child(13){
    width: 127px !important;	
}

.payroll-excel-report-table-wrapper table {
 min-width: 1950px !important;
}

.payroll-excel-report-table-wrapper.payroll-excel-report-data-show table{
min-width:2060px !important;	
}

.table-wrap {
  position: relative;
}

.payroll-excel-report-table-wrapper th,
.payroll-excel-report-table-wrapper td {

}

.payroll-excel-report-table-wrapper thead th {
  position: -webkit-sticky;
  position: sticky;
  top: 0;
}
/* safari and ios need the tfoot itself to be position:sticky also */


.payroll-excel-report-table-wrapper td:first-child, .payroll-excel-report-table-wrapper td:nth-child(2){
  position: -webkit-sticky;
  position: sticky;
  left: 0;
  z-index: 2;
  width:150px;
}
.payroll-excel-report-table-wrapper th:first-child, .payroll-excel-report-table-wrapper th:nth-child(2){
  position: -webkit-sticky;
  position: sticky;
  left: 0;
  z-index: 5;
  background: #fff;
  width:150px;	
}

.payroll-excel-report-table-wrapper td:nth-child(2), .payroll-excel-report-table-wrapper th:nth-child(2){
 left:149px;
 border-right:1.5px solid #ddd;	
}


.show-table-data {
    z-index: 555;
    right: 33px;
    top: 9px;
    transform: rotate(90deg);
    font-size: 17px;
    color: #000;
    cursor: pointer;
	position:absolute;
}
		
</style>
@endsection
@section('content')





<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><span class="project_name_view_heading">Payroll: {{ $payrollData->title }} </span></h1>
    <a href="{{ route( 'admin.payrolls' ) }}" class="back-link">< Back to Payrolls</a>
</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

      <div class="next-prev-navigation">

                <div class="prev">

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('admin.payroll.view', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&subcontractor=<?php echo @$_GET['subcontractor']; ?>&date=<?php echo @$_GET['date']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('admin.payroll.view', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&subcontractor=<?php echo @$_GET['subcontractor']; ?>&date=<?php echo @$_GET['date']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls"  style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Information</h6>
            </div>
  
            <div class="card-body">

                <div class="row">

                    <div class="col-lg-12">
                     <div class="row payroll-information-wrapper">
					 
                        <div class="form-group col-md-4">

                            <label>
                                
                                <i class="far fa-user"></i> Week of
                            
                            </label>

                            <p class="businesses_view_text"> {{ $payrollData->title }} </p>

                        </div>

                       

                        <div class="form-group col-md-4">

                            <label>
                                
                                <i class="far fa-envelope"></i> Uploaded On
                            
                            </label>

                            <p class="businesses_view_text"> {{ $payrollData->uploaded_on }} </p>

                        </div>

                        <div class="form-group col-md-4">

                            <label>
                                
                                <i class="far fa-user"></i> Company
                            
                            </label>

                            <p class="businesses_view_text"> {{ \App\User::find( $payrollData->author )->company_name }} </p>

                        </div>

                        <div class="form-group col-md-4">

                            <p class="businesses_view_text" style="color: green;"> {!! $payrollData->payroll_file_source !!} </p>

                        </div>

                        <div class="form-group col-md-4">

                            <label>
                                
                                <i class="far fa-user"></i> Payroll Status
                            
                            </label>

                            @if( $payrollData->status )
                            <p class="businesses_view_text"> Approved </p>
                            @else
                            <p class="businesses_view_text"> Pending </p>
                            @endif

                        </div>
						 <div class="form-group col-md-4">

                            <label>
                                
                                <i class="far fa-edit"></i> Description
                            
                            </label>

                            <p class="businesses_view_text"> {{ $payrollData->description }} </p>

                        </div>
                       </div>
                    </div>
                    
                </div>
    
            </div>

        </div>

        <?php


        /* ******************************************************************************************************** */

        $lw = [];

        $worklogObject = \App\WorkLog::select('worker_id')->orderBy('punch_at', 'desc');

        if( isset( $payrollData->start_date ) && isset( $payrollData->end_date ) )
        {

            $from = \Carbon\Carbon::parse( $payrollData->start_date )
                             ->startOfDay()
                             ->toDateTimeString();

            $to = \Carbon\Carbon::parse( $payrollData->end_date )
                             ->endOfDay()
                             ->toDateTimeString();

            $worklogObject = $worklogObject->whereBetween('punch_at', [$from, $to])->get();

        }

        foreach ($worklogObject as $get)
        {
          array_push($lw, $get->worker_id);
        }

        //dd( $lw );

        /* ******************************************************************************************************** */

        /* ******************************************************************************************************** */

        $workerExistIds = [];

        foreach( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->get() as $get )
        {

        	$rowData = json_decode( $get->file_data );

        	$userId = $payrollData->getSsnStatusIds( $rowData[0], $rowData[1], $payrollData->author );

        	if( $userId )
        	{

        		array_push($workerExistIds, $userId);

        	}	

        }

        $fi = array_intersect($lw, $workerExistIds);  

        $niu = $payrollData->getNotInUsers( $workerExistIds, $payrollData->author, $payrollData->start_date, $payrollData->end_date );

        /* ******************************************************************************************************** */

        ?>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Excluded Workers</h6>
            </div>
  
            <div class="card-body">

                <div class="row">

                    <div class="col-lg-12">

                      @if( count($niu) > 0)
                     
                     <ul id="custom-scroll-1" style="max-height:250px;">

                    	@foreach( $niu as $u )
                         @if(empty($u->role))
                       <?php updateRole($u->id); ?>
                        @endif
                    	<li><a href="{{ route('admin.users.view', $u->id) }}">{{ $u->first_name }} {{ $u->last_name }} ( @if( !empty($u->email) ) {{ $u->email }}, @endif  @if( !empty($u->phone_number) ) {{ $u->phone_number }}  @endif)</a></li>

                    	<hr>

                    	@endforeach

                      @else

                      <div style="margin: 0 auto; width: 220px;">No Excluded Workers Found!</div>

                      @endif

                    </div>

                	</ul>
                    
                </div>
    
            </div>

        </div>

        <?php

$workers = getPayrollErrors( $payrollData->id );

$errorsHtml = '';
$emailHeader = '';
$emailMiddle = '';
$emailFooter = '';

if( !empty( $workers ) ):

$emailHeader = 'Hello '.\App\User::find( $payrollData->author )->company_name.',<br><br>';

$emailMiddle = 'We found some issues in the Payroll data uploaded for '.$payrollData->title.'. Please see the details below:<br><br>';

foreach( $workers as $worker => $errors )
{
    $errorsHtml .= "<p><b>$worker</b></p>";
    
    foreach( $errors as $error )
    {
        $errorsHtml .= "<p><i>$error</i></p>";
    }

    $errorsHtml .= "<br>";
} 

$emailFooter = "<br>Regards,<br>
Team Verfico";

else:

$emailHeader = 'Hello '.\App\User::find( $payrollData->author )->company_name.',<br><br>';

$emailMiddle = 'We found some issues in the Payroll data uploaded for '.$payrollData->title.'. Please see the details below:<br><br>';



$emailFooter = "<br>Regards,<br>
Team Verfico";

endif;



                    ?>  
      

        <div class="modal fade" id="email-report-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title" id="exampleModalLabel">Send Report Alert</h5>
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="{{ route('admin.payroll.report.format.alert', [ 'id' => $payrollData->id ]) }}">

					<div class="form-group">
					<label for="email">Email address:</label>
					<input type="text" class="form-control" value="{{ $payrollData->subcontractor_email }}" name="email" id="email">
					</div>

					<div class="form-group">
					<label for="pwd">Subject:</label>
					<input type="text" class="form-control" name="subject" id="subject" value="Payroll Update Alert">
					</div>

					<div class="form-group">
					<label for="pwd">Message:</label>
					<textarea class="form-control" name="message">{!! $emailHeader !!}{!! $emailMiddle !!}{!! $errorsHtml !!}{!! $emailFooter !!}</textarea>
					</div>
					
					<button type="submit" class="btn btn-default">Send</button>
				</form>
				</div>
				
			  </div>
			</div>
		  </div>

        @if( !Auth::user()->hasRole('Payroll') )

       

        @if( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->count() > 1 )

        <a class="report-alert" href="javascript::void(0)" data-toggle="modal" data-target="#email-report-update">Send Report Alert To Subcontractor</a>

        @if( $payrollData->status )

        <span class="approved disabled">Approved</span>

        @else

        <a class="approve-button" data-toggle="modal" data-target="#exampleModal" href="javascript:void(0)">Approve</a>



        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form action="{{ route('payroll.report.status.change', $payrollData->id) }}" method="post">

          @csrf

          <textarea class="form-control" name="notes"></textarea>

          <div style="margin-top:10px;">
          <button type="submit" class="btn btn-primary form-control">Submit</button>
        </div>

        </form>

      </div>

    </div>
  </div>
</div>

        @endif

        @endif

        @endif
        

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary excel-file-data-heading">Excel File Data  @if( $payrollData->status )

                <div class="info-text-wrap"><i class="fa fa-info-circle" title="@if( !empty($payrollData->notes) ) {!! strip_tags($payrollData->notes) !!} @else No Notes! @endif"></i></div>

        @endif</h6>
            </div>
  
            <div class="card-body">

           
			

                <div class="row">

                    <div class="col-md-12">
                       <div id="custom-scroll" class="payroll-excel-report-table-wrapper">
					     
                        <table class="table">

                           <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>SSN</th>
                                    <th>Pay Start</th>
                                    <th>Pay End</th>
                                    <th>Chk Date</th>
                                    <th>Chk #</th>
                                    <th>Hours</th>
                                    <th>Gross <div class="show-table-data" id="show-table-data"><i class="fa fa-sort" aria-hidden="true"></i></div></th>
									
                                    <th>Fed W/H</th>
                                    <th>Soc Sec</th>
                                    <th>Med Care</th>
                                    <th>Med Care Add</th>
                                    <th>State W/H <div class="show-table-data" id="hide-table-data"><i class="fa fa-sort" aria-hidden="true"></i></div></th>
                                    <th>Net Pay</th>
                                    <th>Avg. Sal./Hour (Overtime)</th>
                                    <th>Errors</th>
                                    @if( !Auth::user()->hasRole('Payroll') )
                                    <th>Action </th>
                                    @endif
                                </tr>
                            </thead>

                            

                                

                                    @if( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->count() > 1 )

                                    <tbody>

                                        @php 

                                        $count = 0;
                                        

                                        @endphp

                                        @foreach( \App\PayrollFiles::where( 'payroll_id', $payrollData->id )->get() as $get )

                                        @php

                                        $wrongName = false;

                                        $rowData = json_decode( $get->file_data );

                                        $errors =  [];

                                        $wrongSsn = $payrollData->getSsnStatus( $rowData[1], $payrollData->author );

                                        @endphp

                                        @if( empty( $rowData[1] ) )

                                        @php 

                                        $wrongSsn = true;

                                        @endphp

                                        @endif

                                        @php
                                        $checkUser = $payrollData->getWorkerName( $rowData[0], $rowData[1], $payrollData->author );
                                        @endphp
                                        @if( $checkUser == 1  && !$wrongSsn)

                                        @php 

                                        $wrongName = true;

                                        array_push( $errors, "Worker not found for this subcontractor or Wrong SSN id." );

                                        @endphp

                                        @elseif($checkUser == 2 && !$wrongSsn)
                                         @php 

                                        $wrongName = true;

                                        array_push( $errors, "Worker name is incorrect." );

                                        @endphp

                                        @endif

                                        <tr @if( $rowData[6] <= 0 ) class="yellow-row" @endif>
                                        
                                        <td @if( ( empty(  $rowData[0] ) || $wrongName ) && !$wrongSsn  ) class="sheet-error" @endif>{{ $rowData[0] }}</td>
                                        <td style='text-align: center;' @if( empty( $rowData[1] ) || $wrongSsn ) class="sheet-error" @endif>@if( !empty($rowData[1]) ){{ makeSsnFormat( $rowData[1], $payrollData->author ) }}@else - @endif</td>
                                        <td @if( empty( $rowData[2] ) && !$wrongSsn ) class="sheet-error" @endif>@if(!empty($rowData[2])){{ @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rowData[2]))->format('M d Y') }}@endif</td>
                                        <td @if( empty( $rowData[3] ) && !$wrongSsn ) class="sheet-error" @endif>@if(!empty($rowData[3])){{ @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rowData[3]))->format('M d Y') }}@endif</td>
                                        <td @if( empty( $rowData[4] ) && !$wrongSsn ) class="sheet-error" @endif>@if(!empty($rowData[4])){{ @\Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($rowData[4]))->format('M d Y') }}@endif</td>
                                        <td @if( empty( $rowData[5] ) ) class="sheet-error" @endif>{{ $rowData[5] }}</td>

                                        

                                        @if( (!$payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) || $payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) != 'equal') && !$wrongSsn && $rowData[6] != 0 )

                                            @php

                                            array_push( $errors, "Total hours doesn't match." )

                                            @endphp

                                            <td class="less-hours sheet-error" title="Logged hours {{ $payrollData->getWorkerTotalHoursBySsn($rowData[1],$rowData[6],$rowData[2],$rowData[3],$payrollData->author) }}">{{ $rowData[6] }}</td>

                                        @else

                                            <td @if( empty( $rowData[6] ) && $rowData[6] != 0 ) class="sheet-error" @endif>{{ $rowData[6] }}</td>

                                        @endif

                                        


                                        <td @if( empty( $rowData[7] ) && $rowData[7] != 0 ) class="sheet-error" @endif>{{ $rowData[7] }}</td>
                                        <td class="hide-table-data">{{ empty( $rowData[8] ) ? 0 : $rowData[8] }}</td>
                                        <td @if( empty( $rowData[9] ) && $rowData[9] != 0 ) class="sheet-error" @endif>{{ $rowData[9] }}</td>
                                        <td @if( empty( $rowData[10] ) && $rowData[10] != 0 ) class="sheet-error" @endif>{{ $rowData[10] }}</td>
                                        <td>{{ empty( $rowData[11] ) ? 0 : $rowData[11] }}</td>
                                        <td @if( empty( $rowData[12] ) && $rowData[12] != 0 ) class="sheet-error" @endif>{{ $rowData[12] }}</td>

                                        @if( $payrollData->checkGross( $rowData[7], $rowData[13] ) )

                                            @php

                                            array_push( $errors, 'Gross pay is less than Net pay.' )

                                            @endphp

                                            <td class="less-gross sheet-error" title="Gross is less than net pay.">
                                                {{$rowData[13]}}
                                            </td>

                                        @else

                                            <td @if( empty( $rowData[13] ) && $rowData[13] != 0 ) class="sheet-error hide-table-data" @endif>
                                                {{$rowData[13]}}
                                            </td>

                                        @endif

                                        @if( ( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ) < 15 ) && !$wrongSsn && round( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ), 2) != 0 )

                                            @php

                                            array_push( $errors, 'Hourly rate is less than $15.' )

                                            @endphp

                                            <td class="less-rate sheet-error" title="Hourly rate is less then $15.">
                                                ${{ round( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ), 2) }}
                                                <span>
                                                    @if( $payrollData->ifOvertime( $rowData[6] ) && !$wrongSsn )
                                                    <img style='width: 20px;' title='Overtime' class='log-icon' src="{{ asset('public/images/time.png') }}">
                                                    @endif
                                                </span>
                                            </td>

                                        @else

                                            <td>
                                                ${{ round( $payrollData->getAvgSalary( $rowData[6], $rowData[13], $rowData[7] ), 2) }}
                                                <span>
                                                    @if( $payrollData->ifOvertime( $rowData[6] ) && !$wrongSsn )
                                                    <img style='width: 20px;' title='Overtime' class='log-icon' src="{{ asset('public/images/time.png') }}">
                                                    @endif
                                                </span>
                                            </td>

                                        @endif

                                        <td>

                                            @if( !empty( $errors ) || $wrongSsn )

                                                <span class="fa fa-times-circle"></span>

                                                <ul>

                                                @if( $wrongSsn )
                                                
                                                    @php 

                                                        $errors = [];

                                                        $errors = ['Worker not found for this subcontractor or Wrong SSN id.'];

                                                    @endphp

                                                @endif

                                                    @foreach( $errors as $error )
                                                        
                                                        <li>{{ $error }}</li>

                                                    @endforeach

                                                </ul>

                                            @else

                                                <span class="fa fa-check-circle"></span>

                                            @endif

                                        </td>

                                        @if( !Auth::user()->hasRole('Payroll') )

                                        <td>

                                        	
                                            <a style="cursor: pointer;" id="{{ $get->id }}" class="urldelete" title="It will delete this row." rowcount="{{\App\PayrollFiles::where( 'payroll_id', $payrollData->id )->count()}}" url="javascript::void(0)" attrurl="{{ route('admin.payroll.report.row.delete', [$get->id, $payrollData->id]  ) }}"><i style="color: red;" class="fa fa-trash"></i></a>
                                            

                                        </td>

                                        @endif

                                        </tr>
                                        
                                        @php 

                                        $count++;

                                        @endphp

                                        @endforeach
                                    
                                    </tbody>

                                    @else

                                    <tfoot>

                                        <tr>

                                        <td colspan="14" class="wrong-format" style="color: red;">No data or wrong format uploaded. Please use well formatted XLS OR XLSX file.</td>

                                        </tr>

                                    </tfoot>

                                    @endif

                        </table>
                        </div>

                    </div>
                    
                </div>
    
            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({selector:'textarea'});</script>

<script type="text/javascript" src="{{ asset('js/jquery.overlayScrollbars.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/OverlayScrollbars.min.js') }}"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

  $( function() {
    $( document ).tooltip();
  } );

$('.urldelete').click(function(){

  var url = $(this).attr('attrurl');

  var rowcount = $(this).attr('rowcount');



  if( rowcount == 2 )
  {
    if(confirm("If you delete the last row, the payroll will be deleted as well.")){
        

        $.ajax({
    type:'GET',

           url:url,

           

           success:function(data){

            if(data == 'empty')
            {

              location.href = '{{ route("admin.payrolls") }}';

            }

             $('#'+data).parent().parent().hide();

           }
      
    })

    }
  }
  else
  {
    $.ajax({
    type:'GET',

           url:url,

           

           success:function(data){

            if(data == 'empty')
            {

              location.href = '{{ route("admin.payrolls") }}';

            }

             $('#'+data).parent().parent().hide();
             $('.urldelete').attr('rowcount', rowcount-1);

           }
      
    })
  }


  

})

$(document).ready(function()
{
	$('#show-table-data').click(function(){
	 $('.payroll-excel-report-table-wrapper').addClass('payroll-excel-report-data-show');
     $('#show-table-data').hide();	 
	}); 
	
	$('#hide-table-data').click(function(){
	 $('.payroll-excel-report-table-wrapper').removeClass('payroll-excel-report-data-show');
     $('#show-table-data').show();	 
	});
	
    $('tbody tr:last-child td').removeClass('sheet-error');
    $('tbody tr:last-child td').removeClass('less-hours');
    $('tbody tr:last-child td').removeClass('less-rate');
    $('tbody tr:last-child td:last-child').text('');
    $('tbody tr:last-child td:first-child').next().text('');
    $('tbody tr:last-child td:last-child').prev().text('');
    $('tbody tr:last-child td:last-child').prev().prev().text('');

    if ( $("table tr td").hasClass("sheet-error") || $("table tr td").hasClass("wrong-format") )
    {
        $('.approve-button').text('Ignore & Approve');
    }
    else
    {
        $('.approve-button').removeClass('disabled');
    }
})

$('#custom-scroll').overlayScrollbars({
   
	className       : "os-theme-dark",
	scrollbars : {
		clickScrolling : true
	}
		
});
$('#custom-scroll-1').overlayScrollbars({
   
	className       : "os-theme-dark",
	scrollbars : {
		clickScrolling : true
	}
		
});

</script>

@endsection