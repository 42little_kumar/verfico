@extends('admin.layouts.app')

@section('title', 'Payroll Notifications')

@section('content')

@section('styles')

    <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>

@endsection


<div class="d-sm-flex align-items-center justify-content-between mb-4">

    <h1 class="h3 mb-0 text-gray-800">Payroll Notifications</h1>

</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

        <div class="card-body shadow mb-4 card">
  				
  			
  					
  					<form action='{{ route("admin.payroll.send.notification") }}' method='post' name='notification-form' class='notification-form'>

              @csrf

  						<div class='row'>

  							<!-- ----------- -->

  							<div class="col-md-12">

  							    <div class="form-group">

  							        <label>Select Subcontractor</label>

  							        <select class="form-control" name="nt_sbcontractor[]" multiple required="">

										@foreach( $subContractors as $subcontractor )

											@php

												if($subcontractor->payroll_email) {

													$subcontractor_email =  $subcontractor->email . ', ' . $subcontractor->payroll_email;

												} else {

													$subcontractor_email = $subcontractor->email;

												}
											
											@endphp

											<option value='{{ $subcontractor->id }}'>{{ $subcontractor->company_name }} ({{ $subcontractor_email }})</option>

										@endforeach

  							        </select>

  							    </div>

  							</div>

  							<!-- ----------- -->

							<div class="col-md-12">

  							    <div class="form-group">

  							        <label>CC Email (Optional)</label>

  							        <input type="text" class="form-control" name="nt_cc">

  							    </div>

  							</div>

  							<!-- ----------- -->

  							<div class="col-md-12">

  							    <div class="form-group">

  							        <label>Notification Text</label>

  							        <textarea class="form-control" name="nt_text" required=""></textarea>

  							    </div>

  							</div>
  							
  							<!-- ----------- -->

  							<!-- ----------- -->

  							<div class="col-md-12">


  							        <button type="submit" class="btn btn-primary float-right">

				                        <span class="text">Send Notification</span>
				    
				                    </button>


  							</div>
  							
  							<!-- ----------- -->

  						</div>

  					</form>

  				

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>

<script type="text/javascript">

    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {

        initialize_database();

    });

</script>

@endsection