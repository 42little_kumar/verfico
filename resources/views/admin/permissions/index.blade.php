@extends('admin.layouts.app')

@section('title', 'Staff Permissions')

@section('styles')

<style type="text/css">

    .permission-actions a {
    
        margin: 0 5px;
    
        color: #209af0 !important;
    
        padding: 5px;
    
        border: 1px solid #209af0 !important;
    
        text-align: center;
    
        display: inline-block;
    
        border-radius: 50%;
    
        width: 35px;
    
        height: 35px;
    
        background: #fff;

    }

    .permission-actions a:hover, .permission-actions a.active {
    
        color: #fff !important;
    
        background: #209af0 !important;

    }

</style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Staff Permissions</h1>
    
</div>

<div class="row">

    <div class="col-md-12 col-lg-3">
           <div class="open_list_mobile">
               <i class="fas fa-bars"></i> 
            </div>

        <div class="list-group list_scroll_wrap">
            <div class="inner_list_wrap"> 
            @if($roles->isEmpty())

                <a href="javascript:void();" class="list-group-item list-group-item-action text-center">No roles found.</a>

            @else

                @foreach($roles as $r)
                
                    <a href="{{ route('admin.staff.permissions', [ 'role' => $r->slug ]) }}" class="list-group-item list-group-item-action @if($role && $role->slug == $r->slug) active @endif">{{ $r->name }}</a>

                @endforeach
            
            @endif
           </div>
        </div>

    </div>

    <div class="col-md-12 col-lg-9">

        @if(!$role)

        <div class="card shadow">

            <div class="card-header text-right filter-header-wrap">

                <div class="card-body">

                    <h5 class="text-center">Select a role to view permissions.</h5>

                </div>

            </div>

        </div>

        @else
        
        <div class="table_responsive_wrapper">
        <table class="table table-bordered table-hover">

            <thead>

                <tr>

                    <th scope="col" class="text-center">Section</th>

                    @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write permissions'))
                    
                    <th scope="col" class="text-center">Action</th>

                    @endif
                    
                    <th scope="col" class="text-center" style="width: 115px !important;">Status</th>

                </tr>

            </thead>

            <tbody>

                @foreach($permissions as $key => $permission)

                <tr>

                    <td class="text-center">{{ title_case(str_replace('-', ' ', $key)) }}</td>

                    @if(auth()->user()->hasRole('Administrator') || auth()->user()->can('write permissions'))

                    <td class="text-center permission-actions">

                        <a href="{{ route('admin.staff.permissions.action', [ 'role' => $role->slug, 'section' => $key, 'permission' => $permission[0] ]) }}" @if($role->hasPermissionTo($permission[0] . ' ' . $key)) class="active" @endif>  

                            <i class="fas fa-ban"></i>

                        </a>
                        
                        <a href="{{ route('admin.staff.permissions.action', [ 'role' => $role->slug, 'section' => $key, 'permission' => $permission[1] ]) }}" @if($role->hasPermissionTo($permission[1] . ' ' . $key)) class="active" @endif>

                            <i class="far fa-eye"></i>

                        </a>
                        
                        <a href="{{ route('admin.staff.permissions.action', [ 'role' => $role->slug, 'section' => $key, 'permission' => $permission[2] ]) }}" @if($role->hasPermissionTo($permission[2] . ' ' . $key)) class="active" @endif>

                            <i class="fas fa-pencil-alt"></i>

                        </a>


                    </td>

                    @endif

                    <td class="text-center">

                        @if($role->hasPermissionTo($permission[0] . ' ' . $key))

                            {{ title_case($permission[0]) }}

                        @elseif($role->hasPermissionTo($permission[1] . ' ' . $key))

                            {{ title_case($permission[1]) }}

                        @elseif($role->hasPermissionTo($permission[2] . ' ' . $key))

                            {{ title_case($permission[2]) }}

                        @else

                            No-Access

                        @endif

                    </td>

                </tr>

                @endforeach

            </tbody>

        </table>
    </div>

        @endif                

    </div>

</div>

@endsection