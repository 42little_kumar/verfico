@extends('admin.layouts.app')

@section('title', 'Projects')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/toggle_button.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <style type="text/css">
      
      #date-filter
      {
        width: 210px;
        position: absolute;
        right: 301px;
        top: 92px;
      }


.table tr td:nth-child(4) {
    display: none !important;
}

    </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4 super-project-top-head-hold">
            
    <h1 class="h3 mb-0 text-gray-800">Projects</h1>

    @if( auth()->user()->hasRole('Superadmin') || auth()->user()->hasRole('Admin') )

    <!-- Date filter -->

    <input id="date-filter" type="text" name="started_at" required="" class="form-control datepicker_one super-project-date-select" autocomplete="off">

    <!-- Date filter -->

    <form action="{{ route('admin.project.logs.all.export') }}" method="post">

      @csrf

      <input type='hidden' name="date_from" class="date-from" value="">
      <input type='hidden' name="date_to" class="date-to" value="">
      <input style="

      position: absolute;
      right: 160px;
      top: 92px;
      border: 0px !important;
      box-shadow: none !important;

    " class="super-project-export-logs float-right btn-create" type="submit" value="Export Logs">

    </form>

    <a href="{{ route('admin.projects.create') }}" class="float-right btn-create">

       Add Project

    </a>

    @endif
    
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            

            <div class="card-body">

              <div class="custom_table_filter">
            <!-- Subcontractor filter -->

            <select id="status-filter">

              <option disabled selected>-Select Status-</option>

              @foreach( $allStatus as $status => $name )

                <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == $status ) selected  @endif value="{{ $status }}">{{ $name }}</option>

              @endforeach

            </select>

            <!-- Subcontractor filter -->

            <!-- Reset filter -->

            @if(
            (
                isset( $_GET['status'] )
            )
            )

            <a href="{{ route('admin.projects') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

            @endif

            <!-- Reset filter -->
          </div>
            
                <table class="table table-bordered table-hover" id="datatable">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Job Number</th>
        
                            <th scope="col">Job Name</th>

                            <th scope="col">Status</th>

                            <th style="display:none;" scope="col">Address</th>

                            <th scope="col">Manual Logs</th>

                            <th scope="col">Favorites</th>

                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

 <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

    /* Manual log */

    $(document).on('change', '.manual-log', function(event) {

        var projectId = $(this).attr('project_id');

        var manualLog = 0;

        if( $(this).is(':checked') )
        {
            
            manualLog = 1;

        }

        // Call ajax

        $.ajax({

              type: "POST",

              url: "{{ route('admin.project.log.status.update') }}",

              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

              data: { manual_log: manualLog, project_id: projectId },

              success: function (result) {
                   
                console.log( result );

              }
         });

    });

    /* Delete */

    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this project?')) {
        
            event.preventDefault();

        }

    });

    /* ----------------------------------- Next/Prev ----------------------------------- */

  serialize = function(obj) {
    var str = [];
    for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
    return str.join("&");
  }
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

  $(document).on("click","a.index-link", function(event){
    var order  = table.order();
    
    top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status');
    
    event.preventDefault();
  });
  $(document).on("click","a[title='Edit']", function(event){
    var order  = table.order();
    
    top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status');
    
    event.preventDefault();
  });
  $(document).on("click","a[title='View']", function(event){
    var order  = table.order();
    
    top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status');
    
    event.preventDefault();
  });


    var table;

    /* ----------------------------------- Next/Prev ----------------------------------- */

    function initialize_database() {

        var pageLength = localStorage.getItem("dataTable_projects_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

          "order": [[ 0, 'desc' ]],

          stateSave: true,

            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No projects found!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: true,

            "ajax": {

              "url": "{{ route('admin.datatable.project.get') }}",

              "data": function ( d ) {

               d.status = "{{ @$_REQUEST['status'] }}"
              
            }

            },

            columns: [

                { data: "jobnumber", name: "job_number", searchable: true, orderable: true },

                { data: "jobname", name: "job_name", searchable: true, orderable: true },

                { data: "status", name: "status", searchable: false, orderable: false },

                { data: "address", name: "address", searchable: true, orderable: false },

                { data: "manuallogs", name: "manuallogs", searchable: false, orderable: false },

                { data: "favorite", name: "favorite", searchable: false, orderable: true },

                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_projects_pageLength", len);

        });

        $('.datepicker_one').daterangepicker({
   
        },
        function(start, end, label)
        {
         
          $('.date-from').val( start.format('YYYY-MM-DD') );

          $('.date-to').val( end.format('YYYY-MM-DD') );

        });

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#status-filter').change(function()
        {
             var selectVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?status='+selectVal;

             window.location.href = url;


        })

    });

</script>

@endsection