@extends('admin.layouts.app')

@section('title', 'New Project')

@section('styles')

<!-- <link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"> -->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

 <style type="text/css">
     
    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }

    .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
    }

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #2196F3;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }

    .slider.round:before {
      border-radius: 50%;
    }
.switch {
    position: relative;
    display: inline-block;
    width: 48px;
    height: 24px;
}
.slider:before {
    position: absolute;
    content: "";
    height: 17px;
    width: 16px;
    left: 3px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
}
.allow-manul-wrap{
margin-left: 10px; 	
}
 </style>

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">New Project</h1>

    <a href="{{ route('admin.projects') }}" class="back-link">< Back to Projects</a>
    
</div>

<form action="{{ route('admin.projects.create') }}" id="create-project" method="POST" enctype="multipart/form-data">
            
    @csrf

    <div class="row">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Project Information</h6>
                
                </div>

                <div class="card-body">

                

                    <div class="row">
                         <div class="col-md-6">

                            <div class="form-group">

                                <label>Job Type<span class="text-danger">*</span></label>

                                <select name="job_type" class="form-control job-type" required="">

                                <option value="regular" selected="">Regular Job</option>
                                <option value="service">Service Job</option>

                                </select>

                            </div>

                        </div>
						
                        <div class="col-md-6 regular-job">

                            <div class="form-group">

                                <label>Job Number<span class="text-danger">*</span></label>

                                <input type="text" class="form-control " name="job_number" placeholder="Job Number" value="{{ old('job_number') }}" maxlength="50" required>

                                <span id="error-job-number" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Job Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="job_name" placeholder="Job Name" value="{{ old('job_name') }}" maxlength="50" required>

                            </div>

                        </div>

                   
 <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Superintendent<span class="text-danger">*</span></label>

                                <?php

                                $allSuperints = \App\User::role('Superint')->get();

                                ?>

                                <select name="superint" class="form-control selectpicker formen-select"  data-live-search="true" required="">

                                <option disabled="" selected="" value="">Nothinig selected</option>

                                @foreach( $allSuperints as $g )

                                <option value="{{ $g->id }}">{{ $g->first_name }} {{ $g->last_name }}</option>

                                @endforeach

                                </select>

                                <span id="error-job-number" class="error-message text-danger"></span>

                            </div>

                        </div>
                   

                    

                        <!--<div class="col-md-6">

                            <div class="form-group">

                                <label>Subcontractor<span class="text-danger"></span></label>

                                <select name="sub_contractor" class="form-control">

                                <option value="">Select</option>

                                @foreach( $subContractors as $subContractor )

                                <?php $subContractorName = $subContractor->id; ?>

                                <option  value="{{ $subContractor->id }}" @if(old('general_contractor') && old('general_contractor') == '{{ $subContractorName }}') selected @endif>
                                        
                                        {{ $subContractor->first_name }}

                                </option>  

                                @endforeach

                                </select>

                                <span id="error-general-contractor" class="error-message text-danger"></span>

                            </div>

                        </div>-->

                        <input type="hidden" value="null" name="sub_contractor">

                        <div class="col-md-6 regular-job">

                            <div class="form-group">

                                <label>General Contractor<span class="text-danger">*</span></span></label>

                                <input type="text" class="form-control " name="general_contractor_name" placeholder="John Watson" value="{{ old('general_contractor_name') }}" maxlength="50" required>

                                <span id="error-general-contractor-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 regular-job">

                            <div class="form-group">

                                <label>Foremen (Max. 3)</span></label>

                                <select class="form-control selectpicker formen-select" name="foremen[]" multiple data-live-search="true" data-max-options="3">
                                  
                                  @foreach( \App\User::role('Foreman')->orderBy('first_name', "ASC")->get() as $g )

                                  <option value="{{ $g->id }}" >{{ $g->name }}</option>

                                  @endforeach

                                </select>

                                <span id="error-general-contractor-name" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 regular-job">

                            <div class="form-group">

                                <label>Start Date</label>

                                <!-- <textarea class="form-control" name="job_address" placeholder="Job Address" required>{{ old('job_address') }}</textarea>-->

                                <input type="text" name="started_at" class="form-control datepicker_one " autocomplete="off">

                                <span id="error-started-at" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 regular-job">

                            <div class="form-group">

                                <label>Completion Date</label>

                                <!-- <textarea class="form-control" name="job_address" placeholder="Job Address" required>{{ old('job_address') }}</textarea> -->

                                <input type="text" name="completion_at" class="form-control datepicker_two " autocomplete="off">

                                <span id="error-job-completion" class="error-message text-danger"></span>

                            </div>

                        </div>

                        

                       

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Status<span class="text-danger">*</span></span></label>

                                <select name="status" class="form-control" required="">

                                <option value="active">Active</option>

                                <option value="disabled">Disabled</option>

                                </select>

                                <span id="error-status" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Street 1<span class="text-danger">*</span></label>

                                <input type="text" name="address[street_1]" value="{{ old( 'address.street_1' ) }}" placeholder="Street 1" class="form-control" required="">

                                <span id="error-job-address" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <!-- Address -->

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Street 2</label>

                                <input type="text" name="address[street_2]" value="{{ old( 'address.street_2' ) }}" placeholder="Street 2" class="form-control">

                                <span id="error-job-address" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>City<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="address[city]" value="{{ old( 'address.city' ) }}" placeholder="City"  maxlength="50" required>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>State<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="address[state]" value="{{ old( 'address.state' ) }}" placeholder="State"  maxlength="50" required>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Zipcode<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="address[zipcode]" value="{{ old( 'address.zipcode' ) }}" placeholder="Zipcode"  maxlength="50" required>

                            </div>

                        </div>

                        <div class="col-md-6 regular-job">

                            <div class="form-group">

                                <label>Job Description</label>

                                <textarea class="form-control" name="job_description" style="height:44px !important;" placeholder="Job Description">{{ old('job_description') }}</textarea>

                                <span id="error-job-description" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <!-- Address -->

                        <div class="allow-manul-wrap regular-job">


                                <label class="switch" style="margin-bottom:0px;">

                                  <input type="checkbox" value="1" checked="" name="manual_log">

                                  <span class="slider round"></span>

                                </label>

                                <span style="position: relative; top: 0px; left: 10px;">Allow for Manual Check In</span>

                        </div>

                   </div>
                   <br/>
                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Create Project</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>

@endsection

@section('scripts')

<!-- <script type="text/javascript" src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script> -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script type="text/javascript">

$(document).ready(function() {

  $('.job-type').change(function(){

    if( $(this).val() == 'service' )
    {

      $('.regular-job').hide();
      $('.regular-job').find('input').attr('disabled', 'disabled');
      $('.regular-job').find('select').attr('disabled', 'disabled');

    }
    else
    {

      $('.regular-job').show();
      $('.regular-job').find('input').removeAttr('disabled');
      $('.regular-job').find('select').removeAttr('disabled');

    }

  })

    $('#create-project').on('submit', function(event) {

  

    });

    /*
     * @Function Name
     *
     *
     */
    $( ".datepicker_one" ).datepicker({
   
      minDate: 0,
            
      onSelect: function(selectedDate) {

        $(".datepicker_two").datepicker('destroy');

        $(".datepicker_two").val('');

        $(".datepicker_two").datepicker(
            {
                minDate: selectedDate
            }
        )
      },

    });


});

</script>

@endsection