@extends('admin.layouts.app')

@section('title', 'Project: ' . $projectData->job_number)

@section('styles')

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('common/wickedpicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection



@section('content')


<style>
.worker_page_wrapper .dataTables_length {
    position: absolute;
    right: 131px;
}
div#datatable-worklog_info {
    float: left;
    margin-top: 11px;
}
.card-body {
    padding-bottom: 40px;
}
.icons-guide-container {
    bottom: 10px;
}
</style>


<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><span class="project_name_view_heading">Project: {{ $projectData->job_number }}  <em><a href="{{ route('admin.project.edit', $projectData->id) }}@if( !empty( @$_GET['from'] ) )?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>@endif"><i class="fas fa-pen"></i></a></em></span>

      <?php 

          if( DB::table('fav_payroll')->where('user_id', Auth::user()->id)->where('payroll_id', $projectData->id)->exists() )
          {
            $fl = "<a style='left: 21px; position: relative;' href='".route('admin.payrolls.fav', [$projectData->id, Auth::user()->id])."'><i class='fa fa-star'></i></a>"; 
          }
          else
          {
            $fl = "<a style='left: 21px; position: relative;' href='".route('admin.payrolls.fav', [$projectData->id, Auth::user()->id])."'><i class='fa fa-star-o' aria-hidden='true'></i></a>";
          }

        ?>

        {!! $fl !!}

    </h1>
    <a href="{{ route( 'admin.projects' ) }}" class="back-link">< Back to Projects</a>
</div>

<div class="project-tab-wrap">
	<div class="tab-inner-wrap">
	    <span id="info-tab">Information</span>
	    <span id="logs-tab">Logs</span>
	    <span id="manpower-tab">Manpower schedule</span> 
		<div class="clearfix"></div> 
	</div>
</div>

<div class="row justify-content-center trainer_view_wrapper projects-view-page-wrapper">

    <div class="col-lg-12 mb-4" id="project-info-data">

        <div class="card shadow mb-4">
    

            <div class="next-prev-navigation super-projecr-next-prev-navigation">

                <div class="prev">

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('admin.project.view', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('admin.project.view', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls"  style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>
  
            <div class="card-body">

                <div class="row">

                    <div class="col-md-9">

                        <div class="form-group">

                            <label>
                                
                                <i class="far fa-user"></i> Job Name
                            
                            </label>

                            <p class="businesses_view_text">{{ $projectData->job_name }}</p>

                        </div>

                        @if( !empty( $projectData->general_contractor_name ) )

                        <div class="form-group">

                            <label>
                                
                                <i class="far fa-user"></i> General Contractor
                            
                            </label>

                            <p class="businesses_view_text">{{ $projectData->general_contractor_name }}</p>

                        </div>

                        @endif

                        @if( !empty( $projectData->foremen ) )

                        <div class="form-group">

                            <label>
                                
                                <i class="far fa-user"></i> Foremen
                            
                            </label>

                            <?php 

                            $formn = explode(',', $projectData->foremen); 

                            $formenNames = [];

                            foreach( $formn as $get )
                            {

                              $fdata = \App\User::find($get);

                              array_push($formenNames, @$fdata->first_name.' '.@$fdata->last_name);

                            }

                            ?>

                            <p class="businesses_view_text">{{ implode(', ', $formenNames) }}</p>

                        </div>

                        @endif

                        <div class="form-group">

                            <label>
                                
                                <i class="far fa-user"></i> Superintendent
                            
                            </label>

                            <p class="businesses_view_text">{{ ucfirst( @\DB::table('users')->where('id', $projectData->superint)->first()->first_name ) }} {{ ucfirst( @\DB::table('users')->where('id', $projectData->superint)->first()->last_name ) }}</p>

                        </div>

                        <div class="form-group">

                            <label>
                                
                                <i class="fas fa-map-marker-alt"></i> Job Address
                            
                            </label>

                            <p class="businesses_view_text">{{ $projectData->job_address }}</p>

                        </div>

                        <div class="form-group">

                            <label>
                                
                                <i class="fas fa-eye"></i> Status
                            
                            </label>

                            <p class="businesses_view_text">{{ ucfirst( str_replace( '_', ' ', $projectData->status ) ) }}</p>

                        </div>

                        <div class="form-group">

                            <label>
                                
                                <i class="fas fa-map-marker-alt"></i> Manual logs
                            
                            </label>

                            <p class="businesses_view_text">
                              
                              <?php echo ( $projectData->manual_log == 1 ) ? 'Allowed' : 'Not Allowed'; ?>

                            </p>

                        </div>

                    </div>

                    <div class="col-md-3" style="text-align : center;align-items: center;display: flex;flex-direction: column;">

                       @if( $projectData->job_type == 'regular' )

                        <div class="bar_code_holder">

                       <!-- Show The Qrcode -->

                      

                        @if( !empty( $projectData->qrcode ) || $projectData->qrcode != null )

                            <img src="data:image/png;base64, {{ $projectData->qrcode  }} ">

                        @endif

                        

                        <!-- /Show The Qrcode -->
            
            <form action="{{ route('admin.project.print.qr', 'Job Number: '.$projectData->job_number) }}" target="_blank" method="post">

                            {{ csrf_field() }}

                            <input type="hidden" value="{{ $projectData->qrcode }}" name="imageURL">

                            <input type="hidden" value="{{ $projectData->job_name }}" name="projectName">

                            <input type="hidden" value="{{ $projectData->job_number }}" name="projectNumber">

                            <input type="hidden" value="{{ $projectData->job_address_json }}" name="projectAddress">
                            <input style="background: none; border: 0px; font-weight: bold;" type="submit" value="Print">

                        </form>

                        </div>

                        @endif

                        <!-- Print QR -->

                        

                        <!-- Print QR -->

                    </div>
                    
                </div>
    
            </div>

        </div>

    </div>

    <div class="col-lg-12" id="logs-data-wrap" style="display:none;">
       
        <div class="card shadow mb-4">

            <div class="card-body worker_page_wrapper">

                <div class="custom_table_filter">
                 

                  <!-- Date filter -->

                  <input id="date-filter" type="text" value="{{ @$_REQUEST['date'] }}" name="started_at" required="" class="form-control datepicker_one" autocomplete="off">

                  <!-- Date filter -->

                  <!-- Reset filter -->

                  @if(
                  (
                      isset( $_GET['date'] )
                  )
                  )

                  <a href="{{ route('admin.project.view', $projectData->id) }}#logs" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

                  @endif

                  <!-- Reset filter -->
                </div>
        
        <style>
          .project_view_table th{
			  position:relative;
		  }
		  .project_view_table th.sorting_asc, th.sorting_desc, .project_view_table th.sorting_asc, th.sorting_desc, th.sorting, th.sorting{
			cursor:pointer;  
		  }
		  
          .project_view_table th.sorting_asc:after, th.sorting_desc:after, .project_view_table th.sorting_asc:before, th.sorting_desc:before, th.sorting:before, th.sorting:after{
            position: absolute;
          bottom: 0.9em;
          display: block;
          opacity: 0.3;  
        		  
          }
		  
          th.sorting_asc:before{
          right: 1em;
            content: "\2191"; 
 cursor:pointer;			
          }
		  th.sorting:before{
			right: 1em;
            content: "\2191";  
			cursor:pointer;
		  }
          th.sorting_asc:after{
                    right: 0.5em;
                    content: "\2193"; 
 cursor:pointer;					
          }
		  th.sorting:after{
			right: 0.5em;
                    content: "\2193";
           cursor:pointer;					
		  }
		  th.sorting_desc:before{
               right: 1em;
             content: "\2191";
			  cursor:pointer;
		  }
		  
		  th.sorting_desc:after{
			    right: 0.5em;
               content: "\2193"; 
 cursor:pointer;			   
		  }
		  .project_view_table .punch-in-time, .project_view_table .punch-out-time{
			 display:table;
             margin:0 auto;
            width:100%;
            text-align:center;			
		  }
		  
		  .project_view_table td:nth-child(5), .project_view_table td:nth-child(6){
		     text-align:center;	  
		  } 
		  
        </style>
            
                <table class="table table-bordered table-hover project_view_table" id="datatable-worklog">
    
                    <thead>
    
                        <tr>

                            <th scope="col">Worker Name</th>

                            <th scope="col">Worker Name</th>

                            <th scope="col">Classification</th>

                            <th scope="col">Company</th>

                            <th scope="col">Title</th>

                            <th scope="col">Date</th>
        
                            <th scope="col" style="text-align:center;">In</th>

                            <th scope="col" style="text-align:center;">Out</th>

                            <th scope="col" style="text-align:center;">Total Hrs</th>

                            <th scope="col">Action</th>
        
                        </tr>
    
                    </thead>

                    <tbody></tbody>

                </table>

                {!! getIconGuides() !!}

            </div>

        </div>

    </div>

</div>
<div class="clearfix"></div>

<div class="manpower-schedule-wrap" id="manpower-schedule-data" style="display:none;">


<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    

</div>
<div class="clearfix"></div>
<?php

          $dt = date('m-d-Y');

          if( isset( $_GET['d'] ) )
          {
            $dt = $_GET['d'];
          }

          $timestamp = strtotime($dt);

          $input  = $dt;

        $format = 'm-d-Y';

        $c = \Carbon\Carbon::createFromFormat($format, $input);

          $d = $c->format('l');
          $da = $c->format('d');
          $m = $c->format('F');
          $y = $c->format('Y');


          $dtv = \Carbon\Carbon::createFromFormat($format, $dt)->format('m/d/Y');

          ?>

<div class="row superadmin-manpower-schedule-wrap">
   
    <div class="col-lg-12">

      <div class="manpower-next-pre-wrap"> 
      <div class="row manpower-top-btn-wrap">
      <div class="col-3">
       <div class="manpower-next-pre-button-wrap">
                <a href="{{ Request::url() }}?d={{$c->subDays(1)->format('m-d-Y')}}#manpower" class="btn">Previous</a>
        </div>
      </div>
      <div class="col-6">
        <div class="manpower-date-wrap ">
          <strong>{{ $d }}</strong>
        <p>{{ $da }} {{ $m }}, {{ $y }}</p>
        <input class="form-control job-date" name="job_date" type="text" id="datetype" placeholder="Select date" value="{{ $dtv }}" autocomplete="off" style="background:white;"/ />
        </div>
        </div>
        <div class="col-3">
         <div class="manpower-next-pre-button-wrap" style="text-align:right;">
                <a href="{{ Request::url() }}?d={{$c->addDays(2)->format('m-d-Y')}}#manpower" class="btn">Next</a>
        </div>
        </div>
      </div>
      </div>
<div class="clearfix"></div>
    <div class="row">

       
      @if( $md == NULL )

       <p class="no-data-found">No manpower schedule found.</p>

      @else

   @php

              $mds1 = json_decode( $md->emp_data );
           

            @endphp

<?php

 $date_now = date("m-d-Y"); // this format is string comparable

 $todayDateGreater = false;

if ( $date_now  >= @$_REQUEST['d'] ) {
    
  $todayDateGreater = true;

}

//if( !isset( $_REQUEST['d'] ) )
//$todayDateGreater = false;


?>
<!-- ------------------------------------------- Log Data ----------------------------------------- -->



<div class="card card-body shadow col-lg-12" @if( !$todayDateGreater ) style='display: none;' @endif>
 

            <div class="man-power-project-name">
           

        
               <h3>
                @foreach( $p as $g )

                @if( $md->project_id == $g->id )

                Project Name: {{ $g->job_name }}

                @endif

                @endforeach
              </h3>
        <!--<span>Job Opens: {{ str_replace(' : ', ':', $md->job_time) }}</span>-->

            
           
          </div>
      
         <div class="manpower-view-table-holder">

          <ul class="log-list-flex">   
          <li>
            <b>WORKERS</b>
            <b>WORK TYPE</b>
            <b>IN TIME</b>
            <b>OUT TIME</b>
            
          </li>
          </ul>
        </div>

        <?php

        $url = URL::to('/').'/api/v1/getscheduleforadmin?date='.$dt.'&project_id='.$md->project_id.'&classification=show';;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        $allLogs = json_decode($output);


        ?>
        
        <div>

          

            @foreach( $allLogs->data as $g )

            <ul class='log-list-flex'>

            <li @if( !isset( $g->occupied ) ) class="loggreen" @endif @if( empty( $g->punch_in ) && empty( $g->punch_out ) ) class="logred" @endif>
            <div class='log-worker-name'>

              <?php

              $sn = $g->sub_name;

              if( empty( $sn ) )
                $sn = 'Foreman';


              ?>

            <span> @if( !empty( $g->worker_name ) ) {{ ucfirst( $g->worker_name ) }} ({{ $sn }}) @else {{ ucfirst( $g->worker_type ) }} ({{ $sn }}) @endif </span>
            </div>

            <div>
            <span class='in-time-text'> {{ $g->work_type }} </span>
            </div>

            <div>
            <span class='in-time-text'>@if( !empty( $g->punch_in ) ) {{ gt( $g->punch_in ) }} @else - @endif</span>
            </div>
            <div>
            <span class='out-time-text'>@if( !empty( $g->punch_out ) ) {{ gt( $g->punch_out ) }} @else - @endif</span>
            </div>  
            </li>

            </ul>  

            @endforeach

        </div>

         
         
</div>

<!-- ------------------------------------------- Log Data ----------------------------------------- -->


<!-- ------------------------------------------- All Data ----------------------------------------- -->

   
       <div class="card card-body shadow col-lg-12" @if( $todayDateGreater ) style='display: none;' @endif>
 

            <div class="man-power-project-name">
           

        
               <h3>
                @foreach( $p as $g )

                @if( $md->project_id == $g->id )

                Project Name: {{ $g->job_name }}

                @endif

                @endforeach
              </h3>
        <span>Job Opens: {{ str_replace(' : ', ':', $md->job_time) }} ( {{ $md->shifttype }} Hours )</span>

            
           
          </div>

          

      
         <div class="manpower-view-table-holder">
            <ul class="view-power-list-wrap">
        <li style="border:0px;">
          <b class="work-name-td">Subcontractor Name</b>
         <b class="work-name-td">Worker Name/Title</b>
        <b class="work-no-td">No. of Workers</b>
         
          <b class="work-type-td">Work Type</b>
       
       
          <b class="work-notes-td">Notes</b>

         
        </li>
      </ul>
           

  
            <?php

            $mds2 = [];

            if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
             

            }

            ?>

            <?php

            $dp = [];
            $dp2 = [];

            ?>

  
    <ul class="view-power-list-wrap" >
         
          
          @foreach( $mds1->sub as $k => $g )

          <li>

          

          
             <span class="work-name-td">
        {{ \DB::table('users')->where('id', $mds1->sub[$k])->first()->company_name }}
      </span>

            @if( $mds1->sub[$k] == 624 )


                <!-- Rosa case shift 1 start -->
                    
                <span class="work-name-td"> 
                @foreach( $w as $g )

                @if( $mds1->worker[$k] == $g->id )

                {{ @$g->first_name }} {{ @$g->last_name }} {{!empty($g->classification) ? " - ".$g->classification:''}}


                @endif

                @endforeach
                </span> 
                <span class="work-name-td">-</span>
                <!-- Rosa case shift 1 end -->
     

            @else
        
           
      
        <span class="work-name-td">
            

              @php

              $awt = (array)$mds1->a_workertype_name;
              $awtn = (array)$mds1->a_workertype_number;
              $sid = $mds1->sub[$k];

              @endphp

              <?php

              if( count( $awt[$sid] ) > 1 )
              {

                 if( isset( $dp[$sid] ) )
                {
                  $awt = $awt[$sid][$dp[$sid]];
                  $awtn = $awtn[$sid][$dp[$sid]];

                  $dp[$sid] = $dp[$sid]+1;
                }
                else
                {
                  $awt = $awt[$sid][0];
                  $awtn = $awtn[$sid][0];

                  $dp[$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[$sid][0];
                $awtn = $awtn[$sid][0];

              }

              ?>

             {!! ( $awt == 'null' ) ? '-</span>' : '' !!}
              
      
      @foreach( \DB::table('worker_types')->get() as $g )

      @if( $awt == $g->id )

      <!-- shift 1 type start -->

      {{ $g->type }}


      <!-- shift 1 type start -->

      </span>

      

      @endif
    
  
      @endforeach
      
      


              <!-- shift 1 no start -->
             <span class="work-no-td">
             {{ empty( $awtn ) ? '-' : $awtn }}
            </span> 
              <!-- shift 1 no end -->

            @endif
      
         
              
                <!--@foreach( \DB::table('work_types')->get() as $g )



                @if( $mds1->jobtype[$k] == $g->type )

               
                <span class="work-type-td">  
                {{ $g->type }}
               </span>
          


                @endif

                    @endforeach-->

                    <!-- shift 1 job type start -->
                <span class="work-type-td">  
                {{  empty( $mds1->jobtype[$k] ) ? '-': $mds1->jobtype[$k] }}                
               </span>
                <!-- shift 1 job type end -->
                
               

             <span class="work-notes-td">
               <!-- shift 1 note start -->

               {{ empty( $mds1->note[$k] ) ? '-' : $mds1->note[$k] }}

               <!-- shift 1 note end -->
            
            </span>   

           
            @if( $k != 0 )
            
            @endif


            

            </li>

          @endforeach

        

      </ul>
  


 @if( !empty( $mds2 ) )
</div>
<div class="project-sift-inner-repeat project-sift-2-wrap"> 

 <h4 class="project-sift-2">2nd Shift</h4>
 <div class="manpower-view-table-holder" > 
    <div class="manpower-view-table-holder">
    <div class="man-power-project-name" style="margin-top:0px;">
      <span>Job Opens: {{ str_replace(' : ', ':', $md->job_2_time) }} ( {{ $md->shifttype2 }} Hours )</span>
    </div>
       <ul class="view-power-list-wrap" style="margin:20px 0 0; padding-bottom:0px;">
        <li style="border:0px;">
          <b class="work-name-td">Subcontractor Name</b>
          <b class="work-name-td">Worker Name/Title</b>
        <b class="work-no-td">No. of Workers</b>
       
          <b class="work-type-td">Work Type</b>
         
         
          <b class="work-notes-td">Notes</b>

         
        </li>
      </ul>
      
<ul class="view-power-list-wrap" >

        
           

              @foreach( $mds2->sub as $k => $g )

              <li>

             
              <span class="work-name-td">
        {{ \DB::table('users')->where('id', $mds2->sub[$k])->first()->company_name }}
      </span>
               
            
            
            @if( $mds2->sub[$k] == 624 )

            
              
                 <span class="work-name-td"> 
                @foreach( $w as $g )

                @if( $mds2->worker[$k] == $g->id )

                <!-- shift 2 name start -->

                {{ @$g->first_name }} {{ @$g->last_name }} {{!empty($g->classification) ? " - ".$g->classification:''}}


                <!-- shift 2 name end -->


                @endif

                  

                @endforeach
               </span>
              <span class="work-name-td">-</span>
            @else

          
            <span class="work-name-td">
              @php

              $awt = (array)$mds2->a_workertype_name;
              $awtn = (array)$mds2->a_workertype_number;
              $sid = $mds2->sub[$k];

              @endphp

              <?php

              if( count( $awt[$sid] ) > 1 )
              {

                 if( isset( $dp2[$sid] ) )
                {
                  $awt = $awt[$sid][$dp2[$sid]];
                  $awtn = $awtn[$sid][$dp2[$sid]];

                  $dp2[$sid] = $dp2[$sid]+1;
                }
                else
                {
                  $awt = $awt[$sid][0];
                  $awtn = $awtn[$sid][0];

                  $dp2[$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[$sid][0];
                $awtn = $awtn[$sid][0];

              }

              ?>

              {!! ( $awt == 'null' ) ? '-</span>' : '' !!}

      @foreach( \DB::table('worker_types')->get() as $g )

      @if( $awt == $g->id )

      {{ $g->type }}
      </span>
      @endif
      
    
      @endforeach

      <!-- shift 2 no start -->
      <span class="work-no-td">
          {{ empty( $awtn ) ? '-' : $awtn }}
        </span>
          <!-- shift 2 no end -->
              


            @endif
      
              

                <!--@foreach( \DB::table('work_types')->get() as $g )

                 @if( $mds2->jobtype[$k] == $g->type )

                  <span class="work-type-td"> 
                 {{ $g->type }}
                 </span>
                 

                 @endif


                    @endforeach-->

                     <span class="work-type-td"> 
                 {{  empty( $mds2->jobtype[$k] ) ? '-': $mds2->jobtype[$k] }}
                 </span>
              


               
            
            <!-- shift 2 note start -->
            <span class="work-notes-td">
            {{ empty( $mds2->note[$k] ) ? '-' : $mds2->note[$k] }}
            </span>

         <!-- shift 2 note end -->
         
 
          
            @if( $k != 0 )
            
            @endif


            

          </li>

          @endforeach

            @else

            
          </ul>
           


 @endif

 @endif
       

       <!-- ------------------------------------------- All Data ----------------------------------------- -->  
</div>


</div>
</div>

</div>
</div>

<input type="hidden" value='0' class="inenter" name="inenter">
<input type="hidden" value='0' class="outenter" name="outenter">


</div>

    </div>

</div>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

 <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

  $('.job-date').datepicker({

    onSelect: function (dateText, inst) {

         var d = $(this).datepicker('getDate').getDate();
         var m = $(this).datepicker('getDate').getMonth()+1;
         var y = $(this).datepicker('getDate').getFullYear();

         location.href =  "{{ Request::url() }}?d="+m+"-"+d+"-"+y+"#manpower";


      }
    });

  function initialize_database_1() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_users_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

          "order": [[ 1, 'desc' ]],

            stateSave: true,


            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No resources found!',

            searchPlaceholder: "Search...",
              sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,


            "ajax": {

              "url": "{{ route('subcontractor.manpower.get.p') }}/{{$projectData->id}}",

              "data": function ( d ) {

               d.userType = ''
              
            }

            },

            columns: [

               // { data: "id", name: "id", className: "workers-ids-value", searchable: false, orderable: false },

                { data: "project", name: "project", searchable: true, orderable: true },
                
                { data: "job_date", name: "job_date", searchable: true, orderable: true },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database_1();
		
		// tabs js 
		$('.tab-inner-wrap span:first-child').addClass('active');
		
		$('.tab-inner-wrap span').click(function(){
		  $('.tab-inner-wrap span').removeClass('active');
          $(this).addClass('active');		  
		});
		
		$('#info-tab').click(function(){

      window.location.hash = '#information';

		  $('#project-info-data').show();
		  $('#logs-data-wrap').hide();
		  $('#manpower-schedule-data').hide();
		});
		
		$('#logs-tab').click(function(){

      window.location.hash = '#logs';

		  $('#project-info-data').hide();
		  $('#logs-data-wrap').show();
		  $('#manpower-schedule-data').hide();
		});
		
		$('#manpower-tab').click(function(){
      

      window.location.hash = '#manpower';


		  $('#project-info-data').hide();
		  $('#logs-data-wrap').hide();
		  $('#manpower-schedule-data').show();
		});

    var hashVal = window.location.hash;

    
    if( hashVal == '#manpower' )
    {

      $('#manpower-tab').trigger('click');

    }

    if( hashVal == '#logs' )
    {

      $('#logs-tab').trigger('click');

    }

    });

/*
     * @Function Name
     *
     *
     */
    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');
    	$.fn.dataTable.moment('h:mm a');

        $('#datatable-worklog').DataTable({

          "order": [[ 5, 'desc' ], [ 6, 'desc' ]],

dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csvHtml5',
          text: 'Export',
          exportOptions: {
            modifier : {
             page : 'all', // 'all', 'current'
         },
            columns: [1, 2,3,4,5,6,7,8]

          }
                }
      ],
            "bLengthChange": true,

            language: {

            sEmptyTable: "No data available.",

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            searchPlaceholder: "Search...",
             sProcessing: "<span>Large logs can take a bit time initialy to load. Please wait.....</span><br><i class='fas fa-spinner fa-spin'></i>",
             },
             "columnDefs": [
            {
                "targets": [ 1,2 ],
                "visible": false,
                "searchable": false
            }],

            processing: true,

            serverSide: false,

            "ajax": {

              "url": "{{ route('admin.datatable.project.logs.get') }}",

              "data": function ( d ) {

               d.projectId = '{{ $projectData->id }}',
               d.log_date = "{{ @$_REQUEST['date'] }}"
              
            }

            },

            columns: [

                { data: "worker_name", name: "worker_name", searchable: true, orderable: true },

                { data: "worker_name_without_classification", name: "worker_name_without_classification", searchable: true, orderable: true },

                { data: "classification", name: "classification", searchable: false, orderable: false },

                { data: "subcontractor", name: "subcontractor", searchable: true, orderable: true },

                { data: "title", name: "title", searchable: true, orderable: false },

                { data: "date", name: "date", searchable: false, orderable: true },
                
                { data: "punch_in", name: "punch_in", searchable: true, orderable: true },
                
                { data: "punch_out", name: "punch_out", searchable: false, orderable: true },

                { data: "total_hours", name: "total_hours", searchable: false, orderable: true },

                { data: "action", name: "action", searchable: true, orderable: false },

            ],

        });

    }

    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {

    /*
     * @Function Name
     *
     *
     */
     

    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this log?')) {
        
            event.preventDefault();

        }

    });

    /*
     * @Function Name
     *
     *
     */
    $(document).on('focus', '.date-input', function(event) {

        $(this).datepicker(

          {

          dateFormat: 'MM dd, yy' ,

            onSelect: function (dateText, inst) {
                 
                }

          }

        ).val();

    });

    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-edit', function(event) {

      

        var thisRow = $(this).parent().parent().parent();

        $('.inenter').val( thisRow.find('.edit-punch-in-time input').val() )
        $('.outenter').val( thisRow.find('.edit-punch-out-time input').val() )

        console.log( thisRow );

        /*-------------------------------------------- Seperated --------------------------------------------*/

        $('.btn-action-edit').show();

        $('.btn-action-update').hide();

        $( this ).hide();

        thisRow.find('.btn-action-update').show();

        $('.edit-punch-in-time').slideUp('fast');

        $('.edit-punch-out-time').slideUp('fast');

        $('.edit-date').slideUp('fast');

        thisRow.find('.edit-punch-in-time').slideDown('fast');

        thisRow.find('.edit-punch-out-time').slideDown('fast');



        //thisRow.find('.edit-date').slideDown('fast');

        /*-------------------------------------------- Seperated --------------------------------------------*/

    })

    /*
     * @Function Name
     *
     *
     */
     $(document).on('click', '.btn-action-update', function(event) {

        var thisRow = $(this).parent().parent().parent();

        var logId = $(this).attr('log-id');

        var punchIn = thisRow.find('.edit-punch-in-time input').val();

        var punchOut = thisRow.find('.edit-punch-out-time input').val();

        var dateVal = thisRow.find('.edit-date input').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        /*-------------------------------------------- Seperated --------------------------------------------*/

        $.ajax({

            type:'POST',

            url:'{{ route("admin.log.update") }}',

            data: { 

                _token: CSRF_TOKEN,

                logId: logId, 

                punchIn: punchIn,

                punchOut: punchOut, 

                oldin: $('.inenter').val(), 

                oldout: $('.outenter').val(), 

                dateVal: dateVal 

            },

            success:function( data )
            {

                var response = JSON.parse( data );

                var returnedPunchIn = response.data.punch_in_time;

                var returnedPunchOut = response.data.punch_out_time;

                var returnedDateVal = response.dateVal;

                thisRow.find('.punch-in-time').text( returnedPunchIn );

                thisRow.find('.punch-out-time').text( returnedPunchOut );

                thisRow.find('.date-text').text( returnedDateVal );

                thisRow.find('.total-hours-class').text( response.total_time );

                thisRow.find('.edit-punch-in-time').slideUp('fast');

                thisRow.find('.edit-punch-out-time').slideUp('fast');

                thisRow.find('.edit-date').slideUp('fast');

                if( response.data.out_type == 5 )
                {
                  var imgv = "{{ asset('public/images/admin_out.png') }}";
                  thisRow.find('.imgplaceout').html('<img width="20" class="log-icon out-image" src="'+imgv+'">')
                  thisRow.find('.out-image').attr('src', "{{ asset('public/images/admin_out.png') }}");
                }

                if( response.data.in_type == 5 )
                {
                  var imgv = "{{ asset('public/images/admin_out.png') }}";
                  thisRow.find('.imgplacein').html('<img width="20" class="log-icon in-image" src="'+imgv+'">')
                  thisRow.find('.in-image').attr('src', "{{ asset('public/images/admin_out.png') }}");
                }

                $('.btn-action-edit').show();

                $('.btn-action-update').hide();

            }

        })

     });

        initialize_database();

        /*
         * @Function Name
         *
         *
         */
        $( ".datepicker_one" ).daterangepicker();

        /*
        * @Date Filter
        *
        *
        */
        $('#date-filter').change(function()
        {
             var dateVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?date='+dateVal+'#logs';

             window.location.href = url;


        })

       

    });

</script>

@endsection