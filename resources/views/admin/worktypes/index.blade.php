@extends('admin.layouts.app')

@section('title', 'Work Types')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style>

        div.dataTables_wrapper div.dataTables_length select {
            width: 50px;
            display: inline-block;
        }

    </style>
   
@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">
        
        Work Types

    </h1>

    <a href="" class="float-right btn-create" data-toggle="modal" data-target="#addType">

      Add

    </a>
</div>

<div class="row">

    <div class="col-lg-12">

        <div class="card shadow mb-4">

            <div class="card-body worker_page_wrapper">

                <div class="custom_table_filter">

                </div>
            
                <table class="table table-bordered table-hover" id="datatable-workertypes">
  
                    <thead>
    
                        <tr>

                            <th scope="col">Type</th>

                            <th scope="col">Action</th>
        
                        </tr>
  
                    </thead>

                    <tbody></tbody>

                </table>

            </div>

        </div>

    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="addType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Work Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form action="{{ route('admin.work.type.add') }}" method="post">

        @csrf

        <div class="form-group">

            <input type="text" class="form-control" placeholder="Type Name" name="type_name" required="">

        </div>

        <div class="form-group" style="margin: 0px;">

            <button type="submit"  class="btn btn-primary">Add</button>

        </div>

        </form>

      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Update</button>-->
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this type?')) {
        
            event.preventDefault();

        }

    });

    $('body').on('click', function(e)
    {


            if($(e.target).closest('.active-row').length == 0)
            {

                $('.btn-action-edit').show();

                $('.btn-action-delete').show();

                $('.btn-action-close').hide();

                $('.btn-action-update').hide();

                $('.edit-type').slideUp('fast');

            }

    });

    $(document).on('click', '.btn-action-close', function(event) {

        $('.btn-action-edit').show();

        $('.btn-action-delete').show();

        $('.btn-action-close').hide();

        $('.btn-action-update').hide();

        $('.edit-type').slideUp('fast');

    })

    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-edit', function(event) {

      $('table tbody tr').removeClass('active-row');

        var thisRow = $(this).parent().parent().parent();

        thisRow.addClass('active-row');

        console.log( thisRow );

        /*-------------------------------------------- Seperated --------------------------------------------*/

        $('.btn-action-edit').show();

        $('.btn-action-delete').show();

        $('.btn-action-close').hide();

        $('.btn-action-update').hide();

        $( this ).hide();

        thisRow.find('.btn-action-update').show();

        thisRow.find('.btn-action-close').show();

        thisRow.find('.btn-action-delete').hide();

        $('.edit-type').slideUp('fast');

        thisRow.find('.edit-type').slideDown('fast');

        /*-------------------------------------------- Seperated --------------------------------------------*/

    })

    /*
     * @Function Name
     *
     *
     */
     $(document).on('click', '.btn-action-update', function(event) {

        var thisRow = $(this).parent().parent().parent();

        var typeId = $(this).attr('element-id');

        var typeVal = thisRow.find('.edit-type input').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        /*-------------------------------------------- Seperated --------------------------------------------*/

        $.ajax({

            type:'POST',

            url:'{{ route("admin.work.type.update") }}',

            data: { 

                _token: CSRF_TOKEN,

                id: typeId, 

                type: typeVal,

            },

            success:function( data )
            {

                var response = JSON.parse( data );

                if( response.success )
                {

                  var typeVal = response.data.type;

                  thisRow.find('.type-text').text( typeVal );

                  thisRow.find('.edit-type').slideUp('fast');

                  $('.btn-action-edit').show();

                  $('.btn-action-update').hide();

                  $('.btn-action-delete').show();

                  $('.btn-action-close').hide();

                  toastr.success("Type updated");

                  setInterval(function () {
                      location.reload();

                  },1000); 

                }
                else
                {

                  toastr.error( response.message );

                }

                

            },
            error: function(XMLHttpRequest, textStatus, errorThrown)
            { 

                toastr.success("Some error.");

            }  

        })

     });

    /*
     * @Function Name
     *
     *
     */
    function initialize_database() {

        $('#datatable-workertypes').DataTable({

            stateSave: true,

            "bLengthChange": true,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,

            "ajax": {

              "url": "{{ route('admin.datatable.work.types.get') }}",

              "data": function ( d ) {

               d.projectId = ''
              
            }

            },

            columns: [

                { data: "type", name: "type", searchable: true, orderable: true },

                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {

        initialize_database();

    });
  
</script>

@endsection