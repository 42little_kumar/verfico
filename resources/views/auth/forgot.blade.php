@extends('auth.layouts.app')

@section('content')
<div class="container">
<div class="login_holder_wrapper">
<div class="row justify-content-center login_form_row_wrap">

    <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
            
            <div class="card-body p-0">
            
                <div class="row">
                
                    <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                
                    <div class="col-lg-6">
                
                        <div class="p-5">
                            <div class="text-center">
                    
                                <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                    
                                <p class="mb-4">We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!</p>
                    
                            </div>

                            @if(Session::has('success'))

                                <div class="card" style="border-radius: 0; margin-bottom: 7px; border: 0px; color: green; padding: 0;">

                                    <div class="card-body">
                                    
                                        <p style="margin: 0;">{{ Session::get('success') }}</p>

                                    </div>

                                </div>

                            @endif

                            @if(Session::has('error'))

                                <p class="error_message">{{ Session::get('error') }}</p>
                            
                            @endif
                    
                            <form class="user" method="POST" action="{{ route('forgot.password') }}">

                                @csrf
                    
                                <div class="form-group">
                        
                                    <input type="text" name="email" class="form-control form-control-user" placeholder="Enter Email" required>
                    
                                </div>
                    
                                <button type="submit" class="btn btn-primary btn-user btn-block">Send Reset Password Link</button>
                    
                            </form>
                    
                            <br>
                    
                            <div class="text-center">
                    
                                <a class="small" href="{{ route('login') }}">Already have an account? Login!</a>
                    
                            </div>
                
                        </div>
                
                    </div>
            
                </div>
            
            </div>
        
        </div>

    </div>

</div>
</div>
</div>

@endsection