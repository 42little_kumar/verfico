@extends('auth.layouts.app')

@section('content')

<div class="login_holder_wrapper">

<div class="row justify-content-center login_form_row_wrap">

    <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
            
            <div class="card-body p-0">
            
                <div class="row">
                
                    <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                
                    <div class="col-lg-6">
                
                        <div class="p-5">
                    
                            <div class="text-center">
                    
                                <h1 class="h4 text-gray-900 mb-2">Reset Password</h1>
                    
                            </div>

                            @if(Session::has('success'))

                                <div class="card" style="border-radius: 0; margin-bottom: 7px; border: 0px; color: green; padding: 0;">

                                    <div class="card-body">
                                    
                                        <p style="margin: 0;">{{ Session::get('success') }}</p>

                                    </div>

                                </div>

                            @endif

                            @if(Session::has('error'))

                                <p class="error_message">{!! Session::get('error') !!}</p>
                            
                            @endif

                            <hr>
                    
                            <form class="user" method="POST" action="{{ route('reset.password') }}">

                                @csrf

                                <input type="hidden" name="token" value="{{ $token->token }}">
                    
                                <div class="form-group">
                        
                                    <input type="email" name="email" class="form-control form-control-user" placeholder="Enter Email Address" required="">
                    
                                </div>

                                <div class="form-group">
                        
                                    <input type="password" pattern=".{6,}" title="6 characters minimum" id="password" name="password" class="form-control form-control-user" placeholder="New Password" required="">
                    
                                </div>

                                <div class="form-group">
                        
                                    <input type="password" name="confirm-password" id="confirm_password" class="form-control form-control-user" placeholder="Confirm New Password" required="">
                    
                                </div>
                    
                                <button type="submit" class="btn btn-primary btn-user btn-block">Reset Password</button>
                    
                            </form>
                    
                            <hr>
                    
                            <div class="text-center">
                    
                                <a class="small" href="{{ route('login') }}">Already have an account? Login!</a>
                    
                            </div>
                
                        </div>
                
                    </div>
            
                </div>
            
            </div>
        
        </div>

    </div>

</div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">
    
    var password = document.getElementById("password")
      , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
      if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
      } else {
        confirm_password.setCustomValidity('');
      }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

</script>

@endsection