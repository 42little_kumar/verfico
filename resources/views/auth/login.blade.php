@extends('auth.layouts.app')

@section('content')

<div class="container">
<div class="login_holder_wrapper">
<div class="row justify-content-center login_form_row_wrap">

    <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
    
            <div class="card-body p-0">
               
                <div class="row">
              
                    <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              
                    <div class="col-lg-6">
              
                        <div class="p-5">
                    
                            <div class="text-center">
                    
                                <h1 class="h4 text-gray-900 mb-4">Login</h1>
                  
                            </div>

                            @if(Session::has('success'))

                                <div class="card" style="border-radius: 0; margin-bottom: 7px; border: 0px; color: green; padding: 0;">

                                    <div class="card-body">
                                    
                                        <p style="margin: 0;">{{ Session::get('success') }}</p>

                                    </div>

                                </div>

                            @endif

                            @if(Session::has('error'))

                                <p class="error_message">{!! Session::get('error') !!}</p>
                            
                            @endif
                  
                            <form class="user" method="POST" action="{{ route('login') }}">

                                @csrf

                                <input type="hidden" name="timezone" value="">
                    
                                <div class="form-group">
                      
                                    <input type="text" class="form-control form-control-user" name="login" placeholder="Enter Email Address" value="{{ old('login') }}" autocomplete="off" required="">

                                    @if ($errors->has('login'))
                                    
                                        <p class="error_message">{{ $errors->first('login') }}</p>

                                    @endif
                    
                                </div>
                    
                                <div class="form-group">
                            
                                    <input type="password" class="form-control form-control-user" name="password" placeholder="Password" required="">

                                    @if ($errors->has('password'))
                                    
                                        <p class="error_message">{{ $errors->first('password') }}</p>

                                    @endif
                    
                                </div>
                    
                                <div class="form-group">
                    
                                    <div class="custom-control custom-checkbox small">
                        
                                        <input type="checkbox" class="custom-control-input" name="remember" id="rememberMeCheck">
                            
                                        <label class="custom-control-label" for="rememberMeCheck">Remember Me</label>
                            
                                    </div>
                    
                                </div>
                    
                                <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
                  
                            </form>
                  
                            <br>
                  
                            <div class="text-center">
                  
                                <a class="small" href="{{ route('forgot.password') }}">Forgot Password?</a>
                  
                            </div>
                
                        </div>
              
                    </div>
            
                </div>
          
            </div>
        
        </div>

    </div>

</div>
</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/momentjs/moment-with-locales.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/momentjs/moment-timezone-with-data.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('input[name="timezone"]').val(moment.tz.guess());

    });

</script>

@endsection