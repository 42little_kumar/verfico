@component('mail::message')

<h1 style="font-size:20px; margin-bottom:10px;">Hello,</h1>
<?php
if( $data['userid'] )
{
	echo "This is to inform you that you are linked with the following manpower schedule";
}
else
{
	echo $message;
}
?>

<?php
$showtime1 = false;
					$showWorkerTime1 = false;


foreach( $data[0] as $manpowerArray )
                {
                	

                    foreach( $manpowerArray as $k => $v )
                    {
                    	if( str_replace(' ', '_', $data['contractor'] ) == $k ):

                    		$showtime1 = true;
                        
                    	endif;

                    	if( (@$data['userid1'] == true) && isset($data['parent']) && str_replace(' ', '_', $data['parent'] ) == $k )
                    		{
                    			$showWorkerTime1 = true;
                    		}
                    	

             			

                    }

                }

                ?>

<p style="margin-bottom:0px;"><b>Project Name</b> {{ $data['project_name'] }}</p>
<p style="margin-bottom:0px;"><b>Date</b> {{ $data['date'] }}</p>

<?php

if( $showWorkerTime1 )
{
	echo "<b>First Shift Time </b> ".str_replace(' : ', ':', $data['shift_1_time']);
}



 if( $showtime1 )
 {
?>



<p ><b>First Shift Time </b> {{ str_replace(' : ', ':', $data['shift_1_time'] ) }}</p>

<?php
 }


foreach( $data[0] as $manpowerArray )
                {

                    foreach( $manpowerArray as $k => $v )
                    {
                    	if( str_replace(' ', '_', $data['contractor'] ) == $k ):


                        if( $k == 'Rock_Spring_Contracting' )
                        {
                            echo '<h2 class="e-sub-name" style="background:#f2f2f2; padding:10px; margin-bottom:5px;">'.str_replace('_', ' ', $k).'</h2>';
                            echo '<p class="e-worker-name" style="margin-bottom:4px;"><b>Name</b> '.str_replace('_', ' ', $v['worker']).'</p>';
                            echo '<p class="e-work-type" style="margin-bottom:4px;"><b>Work Type</b> '.$v['work_type'].'</p>';
                            echo '<p class="e-note" style="margin-bottom:4px;"><b>Note</b> '.$v['note'].'</p>';
                        }
                        else
                        {
                            echo '<h2 class="e-sub-name" style="background:#f2f2f2; padding:10px; margin-bottom:5px;">'.str_replace('_', ' ', $k).'</h2>';
                            echo '<p class="e-worker-type-number" style="margin-bottom:4px;"><b>Worker title</b> '.str_replace('_', ' ', $v['worker_type']).'</p>';
                            echo '<p class="e-worker-type-number" style="margin-bottom:4px;"><b>No. of workers</b> '.$v['worker_number'].'</p>';
                            echo '<p class="e-work-type" style="margin-bottom:4px;"><b>Work Type</b> '.$v['work_type'].'</p>';
                            echo '<p class="e-note" style="margin-bottom:4px;"><b>Note</b> '.$v['note'].'</p>';
                        }
                        
                    	endif;
                    }

                }

if( isset( $data[1] ) )
{
	

	$showtime2 = false;
					$showWorkerTime2 = false;
	
	foreach( $data[1] as $manpowerArray )
                {

                	

                    foreach( $manpowerArray as $k => $v )
                    {

                    	if( str_replace(' ', '_', $data['contractor'] ) == $k ):

                        $showtime2 = true;
                        
                        endif;

                        if( (@$data['userid2'] == true) && isset($data['parent']) && str_replace(' ', '_', $data['parent'] ) == $k )
                    		{
                    			$showWorkerTime2 = true;
                    		}


                    }

                }

         if( $showWorkerTime2 )
{
	echo "<br><b>Second Shift Time </b> ".str_replace( ' : ', ':', $data['shift_2_time']);
}

     if( $showtime2 )
     {
     	echo '';
	echo '<div style="background: #f9f9f9; padding: 12px; margin: 6px 0 20px; border-radius: 10px;"><h1 style="font-size:18px;">Second Shift</h1>';

	echo "<p style='margin-bottom:0px;'><b>Second Shift Time </b> ".str_replace(' : ', ':', $data['shift_2_time'])."</p></div>";
     }

	foreach( $data[1] as $manpowerArray )
                {

                    foreach( $manpowerArray as $k => $v )
                    {

                    	if( str_replace(' ', '_', $data['contractor'] ) == $k ):

                        if( $k == 'Rock_Spring_Contracting' )
                        {
                            echo '<h2 class="e-sub-name" style="background:#f2f2f2; padding:10px; margin-bottom:5px;">'.str_replace('_', ' ', $k).'</h2>';
                            echo '<p class="e-worker-name" style="margin-bottom:4px;"><b>Name</b> '.str_replace('_', ' ', $v['worker']).'</p>';
                            echo '<p class="e-work-type" style="margin-bottom:4px;"><b>Work Type</b> '.$v['work_type'].'</p>';
                            echo '<p class="e-note" style="margin-bottom:4px;"><b>Note</b> '.$v['note'].'</p>';
                        }
                        else
                        {
                            echo '<h2 class="e-sub-name" style="background:#f2f2f2; padding:10px; margin-bottom:5px;">'.str_replace('_', ' ', $k).'</h2>';
                            echo '<p class="e-worker-type-number" style="margin-bottom:4px;"><b>Worker title</b> '.str_replace('_', ' ', $v['worker_type']).'</p>';
                            echo '<p class="e-worker-type-number" style="margin-bottom:4px;"><b>No. of workers</b> '.$v['worker_number'].'</p>';
                            echo '<p class="e-work-type"style="margin-bottom:4px;"><b>Work Type</b> '.$v['work_type'].'</p>';
                            echo '<p class="e-note" style="margin-bottom:4px;"><b>Note</b> '.$v['note'].'</p>';
                        }
                        
                        endif;

                    }

                }

}

               
?>
<br/>
{!! $data['regards'] !!}
@endcomponent
