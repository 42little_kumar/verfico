@component('mail::message')
Hello Admin,
<br>

A new worker registration has been performed from the app with name {{ $user->first_name }} {{ $user->last_name }}.<br><br>

Regards,<br>
Team {{ config('app.name') }}
@endcomponent
