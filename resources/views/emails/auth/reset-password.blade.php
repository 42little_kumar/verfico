@component('mail::message')
Hello {{ $reset->user->first_name }},
<br>

We have received a request to reset your account password. Click on the button below to change your account password.

@component('mail::button', [ 'url' => route('reset.password', [ 'token' => $reset->token ]) ])
Click Here
@endcomponent

If you haven't requested for password reset, kindly ignore this email.

Regards,<br>
Team {{ env('APP_NAME') }}
@endcomponent
