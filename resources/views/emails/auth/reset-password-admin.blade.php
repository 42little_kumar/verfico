@component('mail::message')
Hello Admin,
<br>

{{ $user->first_name }} {{ $user->last_name }} associated with {{ $user->phone_number }} has requested you to reset the password. Please reset the password from Admin Panel.

Regards,<br>
Team {{ env('APP_NAME') }}
@endcomponent
