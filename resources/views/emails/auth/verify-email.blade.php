@component('mail::message')
Hello {{ $user->first_name }},
<br>
Your account is successfully created. In order to use your account, you need to activate the account by clicking the button below.

@component('mail::button', [ 'url' => route('verify', [ 'email' => $user->email, 'token' => $user->password ]) ])
Click Here
@endcomponent

Regards,<br>
Team {{ env('APP_NAME') }}
@endcomponent
