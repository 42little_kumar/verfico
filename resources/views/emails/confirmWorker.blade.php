@component('mail::message')
Hello {{ $user->first_name }},
<br>

@if( $user->user_role == 'worker' )

Your new account has been created and here are the details:

@endif

@if( $user->user_role == 'foreman' )

Your new Verfico Foreman account has been created and here are the details.

@endif

@if( $user->user_role == 'payroll' )

Your new Verfico Payroll provider account has been created and here are the details.

@endif

@if( $user->user_role == 'subcontractor' )

Your new Verfico Subcontractor account has been created. Your account will allow you to track your employee job logs, upload payroll reports and more. Here are your account details:

@endif

@if( $user->user_role == 'admin' )

Your new Verfico Administrator account has been created and here are the details.

@endif

{!! $user->name !!}
Email: {{ $user->email }}
<br>{!! $user->phone_number !!}
Password: {{ $simplePassword }}
{!! $workerCompanyField !!}
@if( $user->user_role == 'subcontractor' || $user->user_role == 'admin'  || $user->user_role == 'payroll' )
Please visit https://rockspringcontracting.verficoprotect.com/login to login.
@endif

Regards,<br>
Team {{ config('app.name') }}
@endcomponent
