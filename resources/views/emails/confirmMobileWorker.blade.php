@component('mail::message')
Hello {{ $user->first_name }} {{ $user->last_name }},
<br>

Your request to join Verfico has been received. We'll verify your details and our team will get back to you soon.
Once you're verified, you can login and start using our mobile app.

Regards,<br>
Team {{ config('app.name') }}
@endcomponent
