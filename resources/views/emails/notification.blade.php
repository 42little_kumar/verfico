@component('mail::message')
Hello {{ $user->first_name }},
<br>
{{ $message }}

Regards,<br>
Team {{ config('app.name') }}
@endcomponent
