@component('mail::message')

<h1 style="font-size:20px; margin-bottom:10px;">Hello,</h1>
<?php
echo $message;
?>

<p>Project Name: {{ $data['project_name'] }}</p>
<p>Date: {{ $data['date'] }}</p>
<p>Shift: {{ $data['shift'] }}</p>
<p>Shift Time: {{ $data['shift_time'] }}</p>

<br/>
{!! $data['regards'] !!}
@endcomponent
