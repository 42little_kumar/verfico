@component('mail::message')
Hello {{ $user->first_name }},
<br>

We are pleased to inform you that your request to join Verfico has been approved.

{!! $user->name !!}
Email: {{ $user->email }}
<br>{!! $user->phone_number !!}
Password: {!! $simplePassword !!}
<br>
{{ $workerCompanyField }}
<br>

Regards,<br>
Team {{ config('app.name') }}
@endcomponent
