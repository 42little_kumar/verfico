@component('mail::message')
Hello Admin,

<br>

{{ $user->first_name }} with user id "{{ $user->id }}" has requested for password change/reset.

Regards,<br>
Team {{ config('app.name') }}
@endcomponent
