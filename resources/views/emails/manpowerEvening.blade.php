@component('mail::message')

<h1 style="font-size:20px; margin-bottom:10px;">Hello,</h1>

{{ $message }}
{!! $data !!}

<br/>
<p>Regards,<br>Team {{ config('app.name') }}</p>
@endcomponent
