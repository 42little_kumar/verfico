@extends('frontend.layouts.app')

@section('title', 'Contact Us')

@section('content')

@section('styles')



@endsection


<div class="d-sm-flex align-items-center justify-content-between mb-4">

    <h1 class="h3 mb-0 text-gray-800" style='margin: 0 auto;'>Contact Us</h1>

</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

        <div class="card-body shadow mb-4">
  				
                    
                      </p>
                        If you have any questions or suggestions, do not hesitate to contact us at info@rockspringcontracting.com.
                      </p>


        </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>

<script type="text/javascript">


</script>

@endsection