@extends('frontend.layouts.app')

@section('title', '404')

@section('styles')
  
    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">


    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
   
   <style>
   #datatable_wrapper {
    overflow-x: visible !important;
     }
     .dropdown-menu hr:first-child{
	  display:none;	  
	 }
	 
	
footer.sticky-footer .copyright{
color:#000;	
}
   </style>

@section('content')

<main class="resource-front-page-wrap" style="min-height:85vh;">
<div class="container">
 <div class="resource-page-wrap">

<div class="row">

    <div class="col-lg-12">
		<div class="resource-innter-text">
						
         	
			<h1><span class="rcrs-name-wrap">404 Page</span>
				
			</h1>

			

        
       </div>
    </div>

</div>
</div>

</div>
</main>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

   

</script>

@endsection