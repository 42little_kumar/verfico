@extends('frontend.layouts.app')

@if( $r )
@section('title', $r->name)
@else
@section('title', '404')
@endif

@section('styles')
  
    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">


    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
   
   <style>
   #datatable_wrapper {
    overflow-x: visible !important;
     }
     .dropdown-menu hr:first-child{
	  display:none;	  
	 }
	 
	
footer.sticky-footer .copyright{
color:#000;	
}
   </style>

@section('content')

<main class="resource-front-page-wrap" style="min-height:85vh;">
<div class="container">
 <div class="resource-page-wrap">

<div class="row">

    <div class="col-lg-12">
		<div class="resource-innter-text">
						
          <?php

          function getBetween($string, $start = "", $end = ""){
    if (strpos($string, $start)) { // required if $start not exist in $string
        $startCharCount = strpos($string, $start) + strlen($start);
        $firstSubStr = substr($string, $startCharCount, strlen($string));
        $endCharCount = strpos($firstSubStr, $end);
        if ($endCharCount == 0) {
            $endCharCount = strlen($firstSubStr);
        }
        $toreplace = substr($firstSubStr, 0, $endCharCount);

        return str_replace('--'.$toreplace.'--', '', $string);
    } else {
        return $string;
    }
}

          ?>
			 	  	
			<h1><span class="rcrs-name-wrap">{{ @$r->name }}</span>
				<span class="back-btn-text"> <a href="{{ route('f.resources') }}">< Back</a> </span>
			</h1>

			

        <div class="">

          @if( $r )

          <p>{!! $r->description !!}</p>
          
		  <ul class="resource-listing-wrap">
		  
          @if( !empty( $r->file_1 ) )
			  <li><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $r->file_1 }}"><i class="fa fa-file-o" aria-hidden="true"></i> {{ getBetween($r->file_1,"--","--") }}
		  </li>
		  @endif
		  
          @if( !empty( $r->file_2 ) )<li><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $r->file_2 }}"><i class="fa fa-file-o" aria-hidden="true"></i> {{ getBetween($r->file_2,"--","--") }}</a></li>
		  @endif
		  
          @if( !empty( $r->file_3 ) )
			  <li><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $r->file_3 }}"><i class="fa fa-file-o" aria-hidden="true"></i> {{ getBetween($r->file_3,"--","--") }}</a></li>
		  @endif
		  
          @if( !empty( $r->file_4 ) )<li><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $r->file_4 }}"><i class="fa fa-file-o" aria-hidden="true"></i> {{ getBetween($r->file_4,"--","--") }}</a></li>@endif
          @if( !empty( $r->file_5 ) )<li><a target="_blacnk" href="{{ asset('public/uploads/resources') }}/{{ $r->file_5 }}"><i class="fa fa-file-o" aria-hidden="true"></i> {{ getBetween($r->file_5,"--","--") }}</a></li>@endif

          @else

          Resource has been deleted!

          @endif
          </ul>
        </div>
       </div>
    </div>

</div>
</div>

</div>
</main>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

   

</script>

@endsection