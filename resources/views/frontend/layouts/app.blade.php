<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Title -->
        <title>{{ env('APP_NAME') }} | @yield('title')</title>

        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/font-awesome/css/all.min.css') }}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/sb-admin-2.min.css') }}">


        <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png" sizes="16x16">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

        <!-- Toastr -->
        <link rel="stylesheet" href="{{ asset('vendor/toastr/toastr.min.css') }}">

        <!-- Custom styles for all page -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/gif" sizes="16x16">

        <!--DropzoneJS CSS-->
        <link href="https://unpkg.com/dropzone/dist/dropzone.css" rel="stylesheet"/>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

        <!--CropperJS Css-->
        <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/> 

        <!--Ediro -->
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

        <!-- Custom styles for current page -->

        <style>
            .sidebar-brand-text.mx-3 {
                width: 191px;
                margin: 0 auto !important;
            }

        </style>

        @yield('styles')

    </head>

    <body id="page-top">

        <div id="wrapper">

            @include('frontend.layouts.sidebar')

            <div id="content-wrapper" class="d-flex flex-column">

                <div id="content">

                    @include('frontend.layouts.header')

                    <div class="container-fluid">

                        @yield('content')

                    </div>

                </div>

                @include('frontend.layouts.footer')

            </div>

        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('js/jquery.min.js') }}"></script>

        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- Theme script -->
        <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

        <!-- Toastr -->
       <script src="{{ asset('vendor/toastr/toastr.min.js') }}"></script>

       <!-- DropzoneJS -->
       <script src="https://unpkg.com/dropzone"></script>

       <!-- CropperJS -->
       <script src="https://unpkg.com/cropperjs"></script> 

        <!-- Toastr alert script -->
        <script type="text/javascript">
  
            @if(Session::has('success'))
                  
            toastr.success("{{ Session::get('success') }}");
            
            @endif

            @if(Session::has('info'))
            
            toastr.info("{{ Session::get('info') }}");
  
            @endif

            @if(Session::has('warning'))

            toastr.warning("{{ Session::get('warning') }}");

            @endif

            @if(Session::has('error'))

            toastr.error("{{ Session::get('error') }}");

            @endif
        
        </script>

        <!-- Custom scripts for all pages -->
        <script src="{{ asset('js/script.js') }}"></script>

        <!-- Custom scripts for current page -->
        @yield('scripts')
        
    </body>

</html>
