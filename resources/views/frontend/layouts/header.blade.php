<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow resource-front-end-header">

    <div class="sidebar-brand-text mx-3">

        <img src="{{ asset('images/elements/blue-logo.png') }}" alt="logo" style="width: 100%">

    </div>

    <a href="{{ url('/logout') }}"> Logout </a>

</nav>