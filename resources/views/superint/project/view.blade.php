@extends('admin.layouts.app')

@section('title', 'Project: ' . $projectData->job_number)

@section('styles')

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('common/wickedpicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection



@section('content')


<style>
.worker_page_wrapper .dataTables_length {
    position: absolute;
    right: 131px;
}
div#datatable-worklog_info {
    float: left;
    margin-top: 11px;
}
.card-body {
    padding-bottom: 40px;
}
.icons-guide-container {
    bottom: 10px;
}

select[name="datatable_length"] {

            width: 70px !important;

        }
    div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
  right:187px;  
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}

.wickedpicker {
    z-index: 9999;
}
#sidebarToggle{
display:none !important;  
}
.sidebar{
display:none !important;  
}
.fixed-logo {
    position: absolute;
    top: 0;
    width: 154px;
    left: 0;
    height: 4.375rem;
    display: flex;
    align-items: center;
    padding: 7px;
}
.topbar #sidebarToggleTop{
display:none !important;	
}
</style>

<div class="fixed-logo">
<img src="{{ asset('images/elements/blue-logo.png') }}" alt="logo" style="width: 100%">
</div>

<div class="container manprowe-page-wrap manpower-project-pages-wrapper">
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><span class="project_name_view_heading">Project: {{ $projectData->job_number }}  <em><a href="{{ route('superint.project.edit', $projectData->id) }}@if( !empty( @$_GET['from'] ) )?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>@endif"><i class="fas fa-pen"></i></a></em></span>

      
    </h1>
    <a href="{{ route( 'superint.projects' ) }}" class="back-link">< Back to Projects</a>
</div>

<div class="row justify-content-center trainer_view_wrapper">

    <div class="col-lg-12 mb-4">

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Information</h6>
            </div>

            <div class="next-prev-navigation super-projecr-next-prev-navigation" style="display: none;">

                <div class="prev">

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('admin.project.view', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('admin.project.view', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls"  style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>
  
            <div class="card-body">

                <div class="row">

                    <div class="col-md-9">

                        <div class="form-group">

                            <label>
                                
                                <i class="far fa-user"></i> Job Name
                            
                            </label>

                            <p class="businesses_view_text">{{ $projectData->job_name }}</p>

                        </div>

                        @if( !empty( $projectData->general_contractor_name ) )

                        <div class="form-group">

                            <label>
                                
                                <i class="far fa-user"></i> General Contractor
                            
                            </label>

                            <p class="businesses_view_text">{{ $projectData->general_contractor_name }}</p>

                        </div>

                        @endif

                        <div class="form-group">

                            <label>
                                
                                <i class="far fa-user"></i> Superintendent
                            
                            </label>

                            <p class="businesses_view_text">{{ ucfirst( @\DB::table('users')->where('id', $projectData->superint)->first()->first_name ) }} {{ ucfirst( @\DB::table('users')->where('id', $projectData->superint)->first()->last_name ) }}</p>

                        </div>

                        <div class="form-group">

                            <label>
                                
                                <i class="fas fa-map-marker-alt"></i> Job Address
                            
                            </label>

                            <p class="businesses_view_text">{{ $projectData->job_address }}</p>

                        </div>

                        <div class="form-group">

                            <label>
                                
                                <i class="fas fa-eye"></i> Status
                            
                            </label>

                            <p class="businesses_view_text">{{ ucfirst( str_replace( '_', ' ', $projectData->status ) ) }}</p>

                        </div>

                    </div>

                    <div class="col-md-3" style="text-align : center;align-items: center;display: flex;flex-direction: column;">

                       @if( $projectData->job_type == 'regular' )

                        <div class="bar_code_holder">

                       <!-- Show The Qrcode -->

                      

                        @if( !empty( $projectData->qrcode ) || $projectData->qrcode != null )

                            <img src="data:image/png;base64, {{ $projectData->qrcode  }} ">

                        @endif

                        

                        <!-- /Show The Qrcode -->
            
            <form action="{{ route('admin.project.print.qr', 'Job Number: '.$projectData->job_number) }}" target="_blank" method="post">

                            {{ csrf_field() }}

                            <input type="hidden" value="{{ $projectData->qrcode }}" name="imageURL">

                            <input type="hidden" value="{{ $projectData->job_name }}" name="projectName">

                            <input type="hidden" value="{{ $projectData->job_number }}" name="projectNumber">

                            <input type="hidden" value="{{ $projectData->job_address_json }}" name="projectAddress">
                            <input style="background: none; border: 0px; font-weight: bold;" type="submit" value="Print">

                        </form>

                        </div>

                        @endif

                        <!-- Print QR -->

                        

                        <!-- Print QR -->

                    </div>
                    
                </div>
    
            </div>

        </div>

    </div>
</div>


@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

 <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

  $('.job-date').datepicker({

    onSelect: function (dateText, inst) {

         var d = $(this).datepicker('getDate').getDate();
         var m = $(this).datepicker('getDate').getMonth()+1;
         var y = $(this).datepicker('getDate').getFullYear();

         location.href =  "{{ Request::url() }}?d="+m+"-"+d+"-"+y;


      }
    });

  function initialize_database_1() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_users_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

          "order": [[ 1, 'desc' ]],

            stateSave: true,


            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No resources found!',

            searchPlaceholder: "Search...",
              sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,


            "ajax": {

              "url": "{{ route('subcontractor.manpower.get.p') }}/{{$projectData->id}}",

              "data": function ( d ) {

               d.userType = ''
              
            }

            },

            columns: [

               // { data: "id", name: "id", className: "workers-ids-value", searchable: false, orderable: false },

                { data: "project", name: "project", searchable: true, orderable: true },
                
                { data: "job_date", name: "job_date", searchable: true, orderable: true },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database_1();

    });

/*
     * @Function Name
     *
     *
     */
    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');
    	$.fn.dataTable.moment('h:mm a');

        $('#datatable-worklog').DataTable({

          "order": [[ 3, 'desc' ], [ 4, 'desc' ]],
dom: 'lBfrtip',
      buttons: [
        {
          extend: 'csvHtml5',
          text: 'Export',
          exportOptions: {
            modifier : {
             page : 'all', // 'all', 'current'
         },
            columns: [0,1, 2,3,4,5,6]
          }
                }
      ],
            "bLengthChange": true,

            language: {

            sEmptyTable: "No data available.",

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,

            "ajax": {

              "url": "{{ route('admin.datatable.project.logs.get') }}",

              "data": function ( d ) {

               d.projectId = '{{ $projectData->id }}',
               d.log_date = "{{ @$_REQUEST['date'] }}"
              
            }

            },

            columns: [

                { data: "worker_name", name: "worker_name", searchable: true, orderable: true },

                { data: "subcontractor", name: "subcontractor", searchable: true, orderable: true },

                { data: "title", name: "title", searchable: true, orderable: false },

                { data: "date", name: "date", searchable: false, orderable: true },
                
                { data: "punch_in", name: "punch_in", searchable: true, orderable: true },
                
                { data: "punch_out", name: "punch_out", searchable: false, orderable: true },

                { data: "total_hours", name: "total_hours", searchable: false, orderable: true },

                { data: "action", name: "action", searchable: true, orderable: false },

            ],

        });

    }

    /*
     * @Function Name
     *
     *
     */
    $(document).ready(function() {

    /*
     * @Function Name
     *
     *
     */
     

    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this log?')) {
        
            event.preventDefault();

        }

    });

    /*
     * @Function Name
     *
     *
     */
    $(document).on('focus', '.date-input', function(event) {

        $(this).datepicker(

          {

          dateFormat: 'MM dd, yy' ,

            onSelect: function (dateText, inst) {
                 
                }

          }

        ).val();

    });

    /*
     * @Function Name
     *
     *
     */
    $(document).on('click', '.btn-action-edit', function(event) {

      

        var thisRow = $(this).parent().parent().parent();

        $('.inenter').val( thisRow.find('.edit-punch-in-time input').val() )
        $('.outenter').val( thisRow.find('.edit-punch-out-time input').val() )

        console.log( thisRow );

        /*-------------------------------------------- Seperated --------------------------------------------*/

        $('.btn-action-edit').show();

        $('.btn-action-update').hide();

        $( this ).hide();

        thisRow.find('.btn-action-update').show();

        $('.edit-punch-in-time').slideUp('fast');

        $('.edit-punch-out-time').slideUp('fast');

        $('.edit-date').slideUp('fast');

        thisRow.find('.edit-punch-in-time').slideDown('fast');

        thisRow.find('.edit-punch-out-time').slideDown('fast');



        //thisRow.find('.edit-date').slideDown('fast');

        /*-------------------------------------------- Seperated --------------------------------------------*/

    })

    /*
     * @Function Name
     *
     *
     */
     $(document).on('click', '.btn-action-update', function(event) {

        var thisRow = $(this).parent().parent().parent();

        var logId = $(this).attr('log-id');

        var punchIn = thisRow.find('.edit-punch-in-time input').val();

        var punchOut = thisRow.find('.edit-punch-out-time input').val();

        var dateVal = thisRow.find('.edit-date input').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        /*-------------------------------------------- Seperated --------------------------------------------*/

        $.ajax({

            type:'POST',

            url:'{{ route("admin.log.update") }}',

            data: { 

                _token: CSRF_TOKEN,

                logId: logId, 

                punchIn: punchIn,

                punchOut: punchOut, 

                oldin: $('.inenter').val(), 

                oldout: $('.outenter').val(), 

                dateVal: dateVal 

            },

            success:function( data )
            {

                var response = JSON.parse( data );

                var returnedPunchIn = response.data.punch_in_time;

                var returnedPunchOut = response.data.punch_out_time;

                var returnedDateVal = response.dateVal;

                thisRow.find('.punch-in-time').text( returnedPunchIn );

                thisRow.find('.punch-out-time').text( returnedPunchOut );

                thisRow.find('.date-text').text( returnedDateVal );

                thisRow.find('.total-hours-class').text( response.total_time );

                thisRow.find('.edit-punch-in-time').slideUp('fast');

                thisRow.find('.edit-punch-out-time').slideUp('fast');

                thisRow.find('.edit-date').slideUp('fast');

                if( response.data.out_type == 5 )
                {
                  var imgv = "{{ asset('public/images/admin_out.png') }}";
                  thisRow.find('.imgplaceout').html('<img width="20" class="log-icon out-image" src="'+imgv+'">')
                  thisRow.find('.out-image').attr('src', "{{ asset('public/images/admin_out.png') }}");
                }

                if( response.data.in_type == 5 )
                {
                  var imgv = "{{ asset('public/images/admin_out.png') }}";
                  thisRow.find('.imgplacein').html('<img width="20" class="log-icon in-image" src="'+imgv+'">')
                  thisRow.find('.in-image').attr('src', "{{ asset('public/images/admin_out.png') }}");
                }

                $('.btn-action-edit').show();

                $('.btn-action-update').hide();

            }

        })

     });

        initialize_database();

        /*
         * @Function Name
         *
         *
         */
        $( ".datepicker_one" ).daterangepicker();

        /*
        * @Date Filter
        *
        *
        */
        $('#date-filter').change(function()
        {
             var dateVal = $( this ).val();

             var url = window.location.href;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             url += '?date='+dateVal;

             window.location.href = url;


        })

       

    });

</script>

@endsection