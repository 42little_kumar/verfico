@extends('admin.layouts.app')

@section('title', 'Edit Project')

@section('styles')

<!-- <link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css"> -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<style type="text/css">
    
    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }

    .switch input { 
      opacity: 0;
      width: 0;
      height: 0;
    }

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #2196F3;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
      border-radius: 34px;
    }

    .slider.round:before {
      border-radius: 50%;
    }
   .switch {
    position: relative;
    display: inline-block;
    width: 48px;
    height: 24px;
}
.slider:before {
    position: absolute;
    content: "";
    height: 17px;
    width: 16px;
    left: 3px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
}
.allow-manul-wrap{
margin-left: 10px;
}

select[name="datatable_length"] {

            width: 70px !important;

        }
    div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
  right:187px;  
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}

.wickedpicker {
    z-index: 9999;
}
#sidebarToggle{
display:none !important;  
}
.sidebar{
display:none !important;  
}
.fixed-logo {
    position: absolute;
    top: 0;
    width: 154px;
    left: 0;
    height: 4.375rem;
    display: flex;
    align-items: center;
    padding: 7px;
}
.topbar #sidebarToggleTop{
display:none !important;	
}
</style>

@endsection

@section('content')
<div class="fixed-logo">
<img src="{{ asset('images/elements/blue-logo.png') }}" alt="logo" style="width: 100%">
</div>

<div class="container manprowe-page-wrap manpower-project-pages-wrapper">
<div class="d-sm-flex align-items-center justify-content-between mb-4">
            
    <h1 class="h3 mb-0 text-gray-800">Edit Project

        

    </h1>

    <a href="{{ route('superint.projects') }}" class="back-link">< Back to Projects</a>
    
</div>

<form action="{{ route('superint.project.edit', [ 'id' => $projectData->id ]) }}" id="update-project" method="POST" enctype="multipart/form-data">
            
    @csrf

    <div class="row">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header py-3">
                    
                    <h6 class="m-0 font-weight-bold text-primary">Project Information</h6>
                
                </div>

                <div class="next-prev-navigation" style="display: none;">

                <div class="prev">

                @if( isset( $previous->id ) )
                
                   <a href="{{ route('admin.project.edit', [ 'id' => $previous->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Previous</a>
                
                @endif

                </div>

                <div class="next" >

                @if( isset( $next->id ) )
                
                    <a href="{{ route('admin.project.edit', [ 'id' => $next->id ]) }}?column=<?php echo @$_GET['column']; ?>&dir=<?php echo @$_GET['dir']; ?>&from=<?php echo @$_GET['from']; ?>&status=<?php echo @$_GET['status']; ?>">Next</a>
                
                @endif

                </div>

                <div class="prev-ls" style="display:none;">

                @if( isset( $previous->id ) )
                
                   <a href="">Previous</a>
                
                @endif

                </div>

                <div class="next-ls"  style="display:none;">

                @if( isset( $next->id ) )
                
                    <a href="">Next</a>
                
                @endif

                </div>

            </div>

                <div class="card-body">

                    <input type="hidden" name="job_type" value="service">
                  <input type="hidden" name="superint" value="{{ Auth::user()->id }}">

                    <div class="row">
                        
                       

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Job Name<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" name="job_name" placeholder="Job Name" value="{{ $projectData->job_name }}" maxlength="50" required="">

                            </div>

                        </div>
                        
						

                        <input type="hidden" value="null" name="sub_contractor">

                        

                        

                        <div class="col-md-6">

                            <div class="form-group">

                                <label>Status<span class="text-danger">*</span></label>

                                <select name="status" class="form-control" required="">

                                <option value="">Select</option>

                                <?php $statusValue = $projectData->status; ?>

                                <option @if( $statusValue== 'active' ) selected=''  @endif value="active">Active</option>

                                <option  @if( $statusValue == 'disabled' ) selected=''  @endif value="disabled">Disabled</option>

                                <!--<option  @if( $statusValue == 'completed' ) selected=''  @endif value="completed">Completed</option>

                                <option  @if( $statusValue == 'to_be_started' ) selected=''  @endif value="to_be_started">To be started</option>

                                <option  @if( $statusValue == 'hold' ) selected=''  @endif value="hold">On hold</option>-->

                                </select>

                                <span id="error-status" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <!-- Address -->

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Street 1<span class="text-danger">*</span></label>

                                <input type="text" name="address[street_1]" value="{{ @$address->street_1 }}" placeholder="Street 1" class="form-control" required="">

                                <span id="error-job-address" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Street 2</label>

                                <input type="text" name="address[street_2]" value="{{ @$address->street_2 }}" placeholder="Street 2" class="form-control">

                                <span id="error-job-address" class="error-message text-danger"></span>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>City<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" value="{{ @$address->city }}" name="address[city]" placeholder="City"  maxlength="50" required>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>State<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" value="{{ @$address->state }}" name="address[state]" placeholder="State"  maxlength="50" required>

                            </div>

                        </div>

                        <div class="col-md-6 service-job">

                            <div class="form-group">

                                <label>Zipcode<span class="text-danger">*</span></label>

                                <input type="text" class="form-control" value="{{ @$address->zipcode }}" name="address[zipcode]" placeholder="Zipcode"  maxlength="50" required>

                            </div>

                        </div>

                       

                        <!-- Address -->

                        

                    </div>
					<br/>

                    <button type="submit" class="btn btn-primary float-right">

                        <span class="text">Update Project</span>
    
                    </button>

                </div>

            </div>

        </div>

    </div>

</form>
</div>
@endsection

@section('scripts')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

$(document).ready(function() {

    @if( $projectData->job_type == 'service' ) 

    $('.regular-job').hide();
      $('.regular-job').find('input').attr('disabled', 'disabled');
      $('.regular-job').find('select').attr('disabled', 'disabled');

    @else

        $('.regular-job').show();
      $('.regular-job').find('input').removeAttr('disabled');
      $('.regular-job').find('select').removeAttr('disabled');

    @endif

    $('.job-type').change(function(){

    if( $(this).val() == 'service' )
    {

      $('.regular-job').hide();
      $('.regular-job').find('input').attr('disabled', 'disabled');
      $('.regular-job').find('select').attr('disabled', 'disabled');

    }
    else
    {

      $('.regular-job').show();
      $('.regular-job').find('input').removeAttr('disabled');
      $('.regular-job').find('select').removeAttr('disabled');

    }

  })

    $('#create-project').on('submit', function(event) {

  

    });

    /*
     * @Function Name
     *
     *
     */
    /*
     * @Function Name
     *
     *
     */
    $( ".datepicker_one" ).datepicker({
   
      minDate: 0,
            
      onSelect: function(selectedDate) {

        $(".datepicker_two").datepicker('destroy');

        $(".datepicker_two").val('');

        $(".datepicker_two").datepicker(
            {
                minDate: selectedDate
            }
        )
      },

    });

});

</script>

@endsection