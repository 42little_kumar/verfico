@extends('admin.layouts.app')

@section('title', 'Edit')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('common/wickedpicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.min.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }
		div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
	right:187px;	
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}

.wickedpicker {
    z-index: 9999;
}
#sidebarToggle{
display:none !important; 	
}
.sidebar{
display:none !important;	
}
.fixed-logo {
    position: absolute;
    top: 0;
    width: 154px;
    left: 0;
    height: 4.375rem;
    display: flex;
    align-items: center;
    padding: 7px;
}
.topbar #sidebarToggleTop{
display:none !important;	
}
    </style>

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection

@section('content')
<div class="fixed-logo">
<img src="{{ asset('images/elements/blue-logo.png') }}" alt="logo" style="width: 100%">
</div>


<div class="container manprowe-page-wrap">
<div class="d-sm-flex align-items-center justify-content-between mb-4">         
    <h1 class="h3 mb-0 text-gray-800"><strong>{{ \DB::table('project')->where('id', $_REQUEST['pr'])->first()->job_name }}</strong></h1>    
</div>

<div class="row">
        <div class="col-lg-2">
	   <div class="manpower-project-list-wrap">
	     <h3>Projects <i class="fa fa-tasks" aria-hidden="true"></i></h3>

		 <ul>
		  @foreach( $p as $g )

                  <li><a projectId="{{ $g->id }}" class="project-select-li @if( $_REQUEST['pr'] == $g->id ) active @endif"  href="javascript:void();"><span>{{ $g->job_name }}</span>  <div class="project-list-icons"><i class="fa fa-calendar-check-o @if( ifShd( $g->id, $_REQUEST['d'] ) ) active @endif" aria-hidden="true"></i> <i class="fa fa-envelope-o @if( ifEmail( $g->id, $_REQUEST['d'] ) ) active @endif" aria-hidden="true"></i></div></a></li>

      @endforeach
		 </ul>
	   </div>
	</div>


	
    <div class="col-lg-10">

      <form action="{{ route('manpower.update'), $mid }}" method="post">

        @csrf

        <input type="hidden" name="project_id" value="{{ $_REQUEST['pr'] }}">

        <input type="hidden" value="{{ $mid }}" name='id'>
		 <div class="schedule-project-row">
         <div class="row">
		 <div class="col-md-12 emails-buttons-flex-wrap">
               <div class="copy-old-section-wrap schedule-email-dropdown-wrap">
					<a class="btn copy-btn copy-old-btn">Schedule Email  <i class="fa fa-angle-down" aria-hidden="true"></i></a>
				   <div class="manpower-top-btn-wrap copy-button-wrap">
						
						<a href='javascript::void(0)' data-toggle="modal" data-target="#schedule-preview-modal" class="btn send-email-href">Email All Schedules</a>
						<a href='javascript::void(0)' data-toggle="modal" data-target="#schedule-preview-modal1" class="btn send-email-href">Email Current Schedule</a>
				   </div>
		   
				</div>
		 	<div class="send-all-email-pre-wrap"> 

        {{--<a href='{{ route("schedule.emails.all") }}?d={{ $_REQUEST["d"] }}&pr={{ $_REQUEST["pr"] }}' onclick="return confirm('Are you sure you want to send email to all schedules for tomorrow?')" class="send-email-href">Email all schedules</a>--}}

       



        <a href='{{ route("schedule.clear", [$md->id, $_REQUEST["d"], $_REQUEST["pr"]] ) }}' onclick="return confirm('Are you sure you want to clear the schedule? This step could not be reversed.')" class="send-email-href">Clear Schedule</a>

      </div>	

				  {{--<div class="send-email-pre-wrap"> 

                      <a href='{{ route("schedule.emails") }}?d={{ $_REQUEST["d"] }}&pr={{ $_REQUEST["pr"] }}' onclick="return confirm('Are you sure you want to send schedule email?')" class="send-email-href">Send Schedule Email</a>

                   </div>--}}
          
      		  
		</div>
		 </div>
		 </div>

     <?php

     $dt = date('m-d-Y');

          if( isset( $_GET['d'] ) )
          {
            $dt = $_GET['d'];
          }

          ?>

     <div class="modal fade custom-modal" id="schedule-preview-modal" tabindex="-1" role="dialog" aria-labelledby="schedule-preview-modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <!---loader html--->
                      <div class="email-submit-loader-main">
                        <div class="liader-inner">
                          <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                        </div>
                      </div>
                 <!---loader html--->
              <div class="modal-header">
              <h5 class="modal-title" > EMAIL PREVIEWS 

               

              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              </div>
              <div class="modal-body">
              <div class='modal-data project-select-form'>

           

             <div class='schedule-subs'>

              <select class='form-control select-sub-preview'>

                {!! getScheduleSubsPreview( $dt ) !!}

              </select>

              <div class='show-preview-container'>
              </div>

             </div>

            </div>
              </div>
              <div class="modal-footer">

                 <a id="send-email-btn" href='{{ route("schedule.preview.emails.send") }}?d={{ $_REQUEST["d"] }}&pr={{ $_REQUEST["pr"] }}' onclick="return confirm('Are you sure you want to send schedule emails?')" class="send-email-href btn btn-primary">Send Emails</a>

             
              </div>
            </div>
            </div>
          </div>

          <div class="modal fade custom-modal" id="schedule-preview-modal1" tabindex="-1" role="dialog" aria-labelledby="schedule-preview-modal1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <!---loader html--->
                      <div class="email-submit-loader-main">
                        <div class="liader-inner">
                          <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                        </div>
                      </div>
                 <!---loader html--->
              <div class="modal-header">
              <h5 class="modal-title" > EMAIL PREVIEWS 

               

              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              </div>
              <div class="modal-body">
              <div class='modal-data project-select-form'>

           

             <div class='schedule-subs'>

              <select class='form-control select-sub-preview-current'>

                {!! getScheduleSubsPreviewCurrent( $dt, $md->id ) !!}

              </select>

              <div class='show-preview-container-current'>
              </div>

             </div>

            </div>
              </div>
              <div class="modal-footer">

                 <a id="send-email-btn" href='{{ route("schedule.preview.emails.send") }}?d={{ $_REQUEST["d"] }}&pr={{ $_REQUEST["pr"] }}&tosingle={{ $md->id }}' onclick="return confirm('Are you sure you want to send schedule emails?')" class="send-email-href btn btn-primary">Send Email To Current Schedule</a>

             
              </div>
            </div>
            </div>
          </div>

     <?php

          

          $timestamp = strtotime($dt);

          $input  = $dt;

        $format = 'm-d-Y';

        $c = \Carbon\Carbon::createFromFormat($format, $input);

          $d = $c->format('l');
          $da = $c->format('d');
          $m = $c->format('F');
          $y = $c->format('Y');


          $dtv = \Carbon\Carbon::createFromFormat($format, $dt)->format('m/d/Y');

          ?>

		 		   <div class="manpower-next-pre-wrap"> 
			<div class="row manpower-top-btn-wrap">
			<div class="col-3">
			 <div class="manpower-next-pre-button-wrap">
               <a href="{{ route('manpower.dl') }}?d={{$c->subDays(1)->format('m-d-Y')}}&p=1&pr={{ $_REQUEST['pr'] }}" class="btn">Previous</a>
			  </div>
			</div>
			<div class="col-6">
			  <div class="manpower-date-wrap ">
			    <strong>{{ $d }}</strong>
        <p>{{ $da }} {{ $m }}, {{ $y }}</p>
        
        <input class="form-control job-date" name="job_date" type="text" id="datetype" placeholder="Select date" value="{{ $dtv }}" autocomplete="off" style="background:white;"/ /> 
				</div>
			  </div>
			  <div class="col-3">
			   <div class="manpower-next-pre-button-wrap" style="text-align:right;">
                <a href="{{ route('manpower.create') }}?d={{$c->addDays(2)->format('m-d-Y')}}&pr={{ $_REQUEST['pr'] }}" class="btn">Next</a>
			  </div>
			  </div>
			</div>
			</div>

    
			
			<div class="manpower-filter-wrap">
                    <div class="row">
						
						
						<div class="col-md-6">
						<div class="row" style="justify-content: flex-start;">
						    <div class="col-md-4">
													  <?php

										$t = str_replace( ' : ', ':', $md->job_time );

										$t = date("G:i", strtotime($t));




										?>
							<input class="form-control job-time" name="job_time" value="{{ $md->job_time }}" type="text" required="" autocomplete="off" />
                          </div>
						      <div class="col-md-4">
              
              <select name="shifttype" required="">

                <option @if( $md->shifttype == 8 ) selected @endif value="8">8 Hours</option>
                <option @if( $md->shifttype == 10 ) selected @endif value="10">10 Hours</option>
                <option @if( $md->shifttype == 12 ) selected @endif value="12">12 Hours</option>

              </select>

            </div>
						  </div>
						</div>

        

						<div class="col-md-3 sift-flex-wrap">
						
							  <div class="second-sift-wrap">
							    <b class="second-sift-heading">Schedule </b> 
                  
                                   
								 <label><input name="isshift" @if( !empty( $md->emp_data_2 ) ) checked @endif  class="sc" type="checkbox" /> 2<sup>nd</sup> Shift</label>
							      <label class="close-job-label"><input @if( $md->closed ) checked @endif name="closed"  class="closedclass" type="checkbox" /> Special Notes</label>
							 
                                     <textarea @if( !$md->closed ) style="display: none;" @endif class="specialnotes" name="special_notes" placeholder="Special Notes">{{ $md->special_notes }}</textarea>
							 </div>
							
						</div>
					</div>   
                    

                    @if(
                    (
                        isset( $_GET['status'] ) ||
                        isset( $_GET['title'] )
                    )
                    )

                    <a style="display:none;" href="{{ route('subcontractor.workers') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

                    @endif

                    <!-- Reset filter -->

                </div>
			
        <div class="card shadow mb-4">
            <div class="card-body worker_page_wrapper">
			
                  
				
            <div class="step-one-hidden" style="display: none;">
                  <select name="shift1[sub][]" class="form-control get-subcontractors s1-s" data-live-search="true">

                    <option selected="" disabled="">-Select Subcontractor-</option>
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>

            <div class="step-one-hidden-2" style="display: none;">
                  <select name="shift2[sub][]" class="form-control get-subcontractors-2 s2-s" data-live-search="true">

                    <option selected="" disabled="">-Select Subcontractor-</option>
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>


            <div class="step-one-hidden gww" style="display:none;">
                  <select name="shift1[worker][]" class="form-control get-workers" data-live-search="true">

                    <option selected="" disabled="" value="">-Select Worker-</option>
      
                @foreach( $w as $g )

                  <option value="{{ $g->id }}">{{ $g->first_name }} {{ $g->last_name }} {{!empty($g->classification) ? " - ".$g->classification:''}}</option>

                @endforeach
              </select> 
            </div>

            <div class="step-one-hidden gww2" style="display:none;">
                  <select name="shift2[worker][]" class="form-control get-workerss get-workers-2" data-live-search="true">

                    <option selected="" disabled="" value="">-Select Worker-</option>
      
                @foreach( $w as $g )

                  <option value="{{ $g->id }}">{{ $g->first_name }} {{ $g->last_name }} {{!empty($g->classification) ? " - ".$g->classification:''}}</option>

                @endforeach
              </select> 
            </div>

            @php

              $mds1 = json_decode( $md->emp_data );
              //array_shift( $mds1->sub );
              //array_shift( $mds1->worker );

            @endphp

  
            <?php

            $mds2 = [];

            if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
              //array_shift( $mds2->sub );
              //array_shift( $mds2->worker );

            }

            ?>



            <?php

            $dp = [];
            $dp2 = [];

            ?>


				<div class="manpower-text-body-wrap ms1">
				     <div class="power-row-repeat manpower-table-heading-row">
				     <div class="step-one"><strong>Subcontractor</strong></div>
				     <div class="step-one two rw"><strong>worker Name/ Title</strong></div>		
				     <div class="step-one for-label-wrap"><strong>&nbsp;</strong></div>
				     <div class="step-one four"><strong>work type</strong></div>
				     <div class="step-one four"><strong>Notes</strong></div>
				     <div class="step-one add-power-row-wrap"><strong>action</strong></div>
				   </div>
				   
					
					@foreach( $mds1->sub as $k => $g )

                <div class="power-row-repeat">
              <div class="step-one">
                  <select name="shift1[sub][]" class="form-control get-subcontractors s1-s" data-live-search="true">

                    <option selected="" disabled="">-Select Subcontractor-</option>
      
                @foreach( $s as $g )

                  <option @if( $mds1->sub[$k] == $g->id ) selected @endif value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>

            @if( $mds1->sub[$k] == 624 )

             <div class="step-one two rw">
                  <select name="shift1[worker][]" class="form-control get-workers" data-live-search="true">

                    <option selected="" disabled="" value="">-Select Worker-</option>
      
                @foreach( $w as $g )

                  <option @if(isset($mds1->worker[$k]) && $mds1->worker[$k] == $g->id ) selected @endif value="{{ $g->id }}">{{ $g->first_name }} {{ $g->last_name }} {{!empty($g->classification) ? " - ".$g->classification:''}}</option>

                @endforeach
              </select> 
            </div>

            @else

            <div class="step-one two rw">

              @php

              $awt = (array)@$mds1->a_workertype_name;
              $awtn = (array)@$mds1->a_workertype_number;
              @$sid = $mds1->sub[$k];

              @endphp

              <?php

              if( count( $awt[@$sid] ) > 1 )
              {

                 if( isset( $dp[@$sid] ) )
                {
                  $awt = $awt[@$sid][$dp[@$sid]];
                  $awtn = $awtn[@$sid][$dp[@$sid]];

                  $dp[@$sid] = $dp[@$sid]+1;
                }
                else
                {
                  $awt = $awt[@$sid][0];
                  $awtn = $awtn[@$sid][0];

                  $dp[@$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[@$sid][0];
                $awtn = $awtn[@$sid][0];

              }

              ?>

              <input type='hidden' name='shift1[worker][]' value='2222222'>
              <select class='form-control small-i-w wt-s1' name='shift1[a_workertype_name][{{@$sid}}][]' data-live-search="true">

                <option selected="" value="null">-Title-</option>
      
      @foreach( \DB::table('worker_types')->get() as $g )

      <option @if( $awt == $g->id ) selected @endif value='{{ $g->id }}'>{{ $g->type }}</option>

      @endforeach

      
      </select>
              <input class='form-control small-i-n wnumber1' subid='{{@$sid}}' value='{{ $awtn }}' placeholder='No.' onkeydown="javascript: return event.keyCode == 69 ? false : true" type='number' onkeypress='return isNumber(event)' min='1' name='shift1[a_workertype_number][{{@$sid}}][]'>

            </div>

            @endif

             <div class="step-one for-label-wrap">
                  for
            </div>
             <div class="step-one four">
                  <select name="shift1[jobtype][]" class="form-control jobt1" data-live-search="true">

                    <option value="">-Select Work Type-</option>;

                @foreach( \DB::table('work_types')->get() as $g )

                      <option @if( $mds1->jobtype[$k] == $g->type ) selected  @endif>{{ $g->type }}</option>

                    @endforeach

              </select> 
            </div>
            <div class="step-one five">
                 <input name="shift1[note][]" class="form-control s1note" subid='{{@$sid}}' value="{{ $mds1->note[$k] }}" type="text" placeholder="Notes" />
            </div>
            <div class="step-one add-power-row-wrap add-power-row-wrap-button">
                 <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a>
            </div>

            <?php

            $delsts1 = 'none';

             if( count( $mds1->sub ) > 1 ):
            
            $delsts1 = 'block';

            endif; ?>

            <div class="step-one add-power-row-wrap delete-wrap" style="display: {{$delsts1}}"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div>

            </div>

          @endforeach


        </div>

        

    </div>

</div>


<div class="card shadow mb-4 card-body worker_page_wrapper second-shift"  @if( empty( $md->emp_data_2 ) ) style="display: none;" @endif>
<h2 class="second-sift-heading-text">2nd Shift</h2>

<?php


$t2 = str_replace( ' : ', ':', $md->job_2_time );

if( empty( $md->job_2_time ) )
{

  $t2 = "6:00 AM";

}



$t2 = date("G:i", strtotime($t2));




?>
<div class="second-shift-job-time-wrap">
<div class="manpower-filter-wrap">
<div class="row">

<div class="col-md-6">
 <div class="row" style="justify-content:flex-start;">
    <div class="col-md-4">
	  <input class="form-control job-2-time" name="job_2_time" value="{{ $md->job_2_time }}" type="text" placeholder="Job Opens" autocomplete="off" />
	</div>
	<div class="col-md-4">
	       <select name="shifttype2" required="">

                <option @if( $md->shifttype2 == 8 ) selected @endif value="8">8 Hours</option>
                <option @if( $md->shifttype2 == 10 ) selected @endif value="10">10 Hours</option>
                <option @if( $md->shifttype2 == 12 ) selected @endif value="12">12 Hours</option>

              </select>
	</div>
 </div>

</div>

</div>
</div>
</div>
<div class="manpower-text-body-wrap shift-2 ms2" style="margin-top:0px;">
					<div class="power-row-repeat manpower-table-heading-row">
				     <div class="step-one"><strong>Subcontractor</strong></div>
				     <div class="step-one two rw"><strong>worker Name/ Title</strong></div>		
				     <div class="step-one for-label-wrap"><strong>&nbsp;</strong></div>
				     <div class="step-one four"><strong>work type</strong></div>
				     <div class="step-one four"><strong>Notes</strong></div>
				     <div class="step-one add-power-row-wrap"><strong>action</strong></div>
				   </div>
				   
            <!--<div class="power-row-repeat">
              <div class="step-one">
                  <select name="shift2[sub][]" class="form-control get-subcontractors-2">
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>
             <div class="step-one two rw">
                  <select data-live-search="false" class="form-control">
                <option>Job</option>
              </select> 
            </div>
             <div class="step-one for-label-wrap">
                  for
            </div>
             <div class="step-one four">
                  <select name="shift2[jobtype][]" class="form-control">
                <option>Cleaning</option>
                <option>Washing</option>
                <option>Decorating</option>
              </select> 
            </div>
            <div class="step-one five">
                 <input name="shift2[note][]" class="form-control" type="text" placeholder="Notes" />
            </div>
            <div class="step-one add-power-row-wrap add-power-row-wrap-button-2">
                 <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a>
            </div>

            </div>-->
          
            @if( !empty( $mds2 ) )

              @foreach( $mds2->sub as $k => $g )

                <div class="power-row-repeat">
              <div class="step-one">
                  <select name="shift2[sub][]" class="form-control get-subcontractors-2 s2-s" data-live-search="true">

                    <option selected="" disabled="">-Select Subcontractor-</option>

                  @foreach( $s as $g )

                  <option @if( $mds2->sub[$k] == $g->id ) selected @endif value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>

            @if( $mds2->sub[$k] == 624 )

             <div class="step-one two rw">
                  <select name="shift2[worker][]" class="form-control get-workerss get-workers-2" data-live-search="true">
      
                @foreach( $w as $g )

                  <option @if( $mds2->worker[$k] == $g->id ) selected @endif value="{{ $g->id }}">{{ $g->first_name }} {{ $g->last_name }} {{!empty($g->classification) ? " - ".$g->classification:''}}</option>

                @endforeach
              </select> 
            </div>

            @else

            <div class="step-one two rw">

              @php

              $awt = (array)$mds2->a_workertype_name;
              $awtn = (array)$mds2->a_workertype_number;
              @$sid = $mds2->sub[$k];

              @endphp

              <?php

              if( count( $awt[@$sid] ) > 1 )
              {

                 if( isset( $dp2[@$sid] ) )
                {
                  $awt = $awt[@$sid][$dp2[@$sid]];
                  $awtn = $awtn[@$sid][$dp2[@$sid]];

                  $dp2[@$sid] = $dp2[@$sid]+1;
                }
                else
                {
                  $awt = $awt[@$sid][0];
                  $awtn = $awtn[@$sid][0];

                  $dp2[@$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[@$sid][0];
                $awtn = $awtn[@$sid][0];

              }

              ?>

              <input type='hidden' name='shift2[worker][]' value='2222222'>

              <select class='form-control small-i-w wt-s2' name='shift2[a_workertype_name][{{@$sid}}][]' data-live-search="true">

                <option selected="" value="null">-Title-</option>
      
      @foreach( \DB::table('worker_types')->get() as $g )

      <option @if( $awt == $g->id ) selected @endif value='{{ $g->id }}'>{{ $g->type }}</option>

      @endforeach

      
      </select>

              <input class='form-control small-i-n wnumber2' subid='{{@$sid}}' value='{{ $awtn }}' placeholder='No.' onkeydown="javascript: return event.keyCode == 69 ? false : true" type='number' onkeypress='return isNumber(event)' min='1' name='shift2[a_workertype_number][{{@$sid}}][]'>

            </div>

            @endif

             <div class="step-one for-label-wrap">
                  for
            </div>
             <div class="step-one four">
                  <select name="shift2[jobtype][]" class="form-control jobt2" data-live-search="true">

                    <option value="">-Select Work Type -</option>

                @foreach( \DB::table('work_types')->get() as $g )

                      <option @if( $mds2->jobtype[$k] == $g->type ) selected  @endif>{{ $g->type }}</option>

                    @endforeach
              </select> 
            </div>
            <div class="step-one five">
                 <input name="shift2[note][]" class="form-control s2note" value="{{ $mds2->note[$k] }}" type="text" placeholder="Notes" />
            </div>
            <div class="step-one add-power-row-wrap add-power-row-wrap-button-2">
                 <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a>
            </div>

            <?php

            $delsts2 = 'none';

             if( count( $mds2->sub ) > 1 ):
            
            $delsts2 = 'block';

            endif; ?>

       
           
            <div class="step-one add-power-row-wrap delete-wrap-2 " style="display: {{$delsts2}}"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div>
         

            </div>

          @endforeach

            @else

            <div class="power-row-repeat">
              <div class="step-one">
                  <select name="shift2[sub][]" class="form-control get-subcontractors-2 s2-s" data-live-search="true">

                    <option selected="" disabled="">-Select Subcontractor-</option>
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>
             <div class="step-one two rw">
                  <select data-live-search="false" class="form-control s2jobfield" data-live-search="true">
               
              </select> 
            </div>
             <div class="step-one for-label-wrap">
                  for
            </div>
             <div class="step-one four">
                  <select name="shift2[jobtype][]" class="form-control jobt2" data-live-search="true">
               
                    <option value="">- Select Work Type -</option>

                @foreach( \DB::table('work_types')->get() as $g )

                      <option >{{ $g->type }}</option>

                    @endforeach

              </select> 
            </div>
            <div class="step-one five">
                 <input name="shift2[note][]" class="form-control s2note" type="text" placeholder="Notes" />
            </div>
            <div class="step-one add-power-row-wrap add-power-row-wrap-button-2">
                 <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a>
            </div>
            <div class="step-one add-power-row-wrap delete-wrap-2 delete1-ini" style="display: none;"> 
          <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> 
        </div>

            </div>

            @endif

        </div>

</div>
<div class="man-power-button-wrapper"> 
<input class="btn" type="submit" value="Update">
</div>

<div class="email-userids">
  </div>

  <div class="email-delids">
  </div>

</form>
</div>
</div>
@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>

<script src="{{ asset('js/jquery.timepicker.min.js') }}"></script>

<script src="{{ asset('common/wickedpicker.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

  $('.closedclass').click(function(){

    if( $(this).is(':checked') )
    {

      $('.specialnotes').show()

    }
    else
    {
      $('.specialnotes').hide()

    }

  })

//loader js//
$(document).ready(function(){
  $('#send-email-btn').click(function(){
    $('.email-submit-loader-main').addClass('show-loader')
  }); 
});

$(document).on('change', '.select-sub-preview', function(event){

  event.stopImmediatePropagation();

  

  $('.show-preview-container').html('');

  $.ajax({

           type:'GET',

           url:'{{ route("get.the.email.preview") }}',

           data:{subid:$(this).val(), date:'{{$dt}}'},

           success:function(data){

              $('.show-preview-container').html('<div class="show-preview">'+data+'</div>'); 

           }

        });

})

$(document).on('change', '.select-sub-preview-current', function(event){

  event.stopImmediatePropagation();

  

  $('.show-preview-container-current').html('');

  $.ajax({

           type:'GET',

           url:'{{ route("get.the.email.preview") }}',

           data:{subid:$(this).val(), date:'{{$dt}}', tosingle:'yes', pr:'{{ $_REQUEST["pr"] }}'},

           success:function(data){

              $('.show-preview-container-current').html('<div class="show-preview">'+data+'</div>'); 

           }

        });

})

$(document).on('change', '.s1-s', function(event){


    var thisId = $(this).val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');
  

})

  $(document).on('change', '.s2-s', function(event){

    var thisId = $(this).val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');

  })

  $(document).on('click', '.delete-wrap a', function(event){

    var thisId = $(this).parent().parent().find('.s1-s select').val();
    var thisIdw = $(this).parent().parent().find('.get-workers select').val();


    $('.email-delids').append('<input type="hidden" name="emaildels[]" value="'+ thisId +':1">');
    //$('.email-delids').append('<input type="hidden" name="emaildels[]" value="'+ thisIdw +':1">');
    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');

  })

    $(document).on('change', '.wt-s1', function(event){

    var thisId = $(this).parent().parent().parent().find('.s1-s select').val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');

  })

    $(document).on('change', '.wt-s2', function(event){

    var thisId = $(this).parent().parent().parent().find('.s2-s select').val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');

  })

    $(document).on('change', '.wnumber1', function(event){

    var thisId = $(this).parent().parent().parent().find('.s1-s select').val();
    var thisId = $(this).attr('subid');

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');

  })

    $(document).on('click', '.delete-wrap-2 a', function(event){

    var thisId = $(this).parent().parent().find('.s2-s select').val();
    var thisIdw = $(this).parent().parent().find('.get-workers-2 select').val();



    $('.email-delids').append('<input type="hidden" name="emaildels[]" value="'+ thisId +':2">');
    //$('.email-delids').append('<input type="hidden" name="emaildels[]" value="'+ thisIdw +':2">');

  })

    $(document).on('change', '.wnumber2', function(event){

    //var thisId = $(this).parent().parent().parent().find('.s2-s select').val();
    var thisId = $(this).attr('subid');

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');

  })

    $(document).on('change', '.jobt1', function(event){

    var thisId = $(this).parent().parent().parent().find('.s1-s select').val();
    var thisIdw = $(this).parent().parent().parent().find('.get-workers select').val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');
    //$('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisIdw +'">');

  })

    $(document).on('change', '.s1note', function(event){

    //var thisId = $(this).parent().parent().parent().find('.s1-s select').val();
    var thisId = $(this).attr('subid');
    var thisIdw = $(this).parent().parent().parent().find('.get-workers select').val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');
    //$('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisIdw +'">');

  })

    $(document).on('change', '.s2note', function(event){

    var thisId = $(this).parent().parent().parent().find('.s2-s select').val();
    var thisIdw = $(this).parent().parent().parent().find('.get-workers-2 select').val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');
    //$('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisIdw +'">');

  })


    $(document).on('change', '.job-time', function(event){

    $('.email-userids').append('<input type="hidden" name="allUpdate[]" value="1">');

  })

    $(document).on('change', '.job-2-time', function(event){

    $('.email-userids').append('<input type="hidden" name="allUpdate[]" value="1">');

  })


    $(document).on('change', '.jobt2', function(event){

    var thisId = $(this).parent().parent().parent().find('.s2-s select').val();
    var thisIdw = $(this).parent().parent().parent().find('.get-workers-2 select').val();

    $('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisId +'">');
    //$('.email-userids').append('<input type="hidden" name="emailusers[]" value="'+ thisIdw +'">');

  })

    $(document).on('change', '.get-workers', function(event){



    //$('.email-userids').append('<input type="hidden" name="emailusers[]" value="624">');

  })

    $(document).on('change', '.get-workers-2', function(event){



    //$('.email-userids').append('<input type="hidden" name="emailusers[]" value="624">');

  })

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

  $('select[name="project_id"]').change(function(){
    location.href =  "{{ route('manpower.create') }}?d={{ $_REQUEST['d'] }}&pr="+$(this).val();
  })

  $('.project-select-li').click( function() {


    $('.project_id').val($(this).attr('projectId'));
    location.href =  "{{ route('manpower.create') }}?d={{ $_REQUEST['d'] }}&pr="+$(this).attr('projectId');

  } )

  function addParam(currentUrl,key,val) {
    var url = new URL(currentUrl);
    url.searchParams.set(key, val);
    return url.href; 
}

  $("#datetype").on("keypress", function(e){
   
        e.preventDefault();
    
});

var currentSub = '';
var currentSub2 = '';
var arraySub = [];

$(document).on('change', '.s1-s', function(event){


    
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
      //alert( $(this).val() ); 
      currentSub = $(this).val();

      }
    }
    
  

})

$(document).on('change', '.s2-s', function(event){


    
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
      //alert( $(this).val() ); 
      currentSub2 = $(this).val();

      }
    }
    
  

})

/* ------------------------------------------------------------------------------*/


// For shift 1
$(document).on('change', '.jobt1', function(e){

  e.stopImmediatePropagation();
  checkValidaion($(this));

})

$(document).on('change', '.wt-s2', function(e){

  e.stopImmediatePropagation();

 //var thisworkval = $(this).parent().parent().parent().find('.jobt2 select').val('').change();


})

$(document).on('change', '.wt-s1', function(e){

 e.stopImmediatePropagation();

 //var thisworkval = $(this).parent().parent().parent().find('.jobt1 select').val('').change();

})



// For shift 2
$(document).on('change', '.jobt2', function(e){

  e.stopImmediatePropagation();
  checkValidaion2($(this));

})


function checkValidaion2(thisval)
{
   
   var thisId = thisval.parent().parent().parent().find('.s2-s select').val();
   var thisIdw = thisval.parent().parent().parent().find('.wt-s2 select').val();
   var worktype = thisval.parent().parent().parent().find('.jobt2 select').val();

   //alert( thisval.val() );
   //alert( thisId );
   //alert( thisIdw );

   var sub = false;
   var worker = false;
   var work = false;


   $('.power-row-repeat').not(thisval.parent().parent().parent()).each(function(){

    if( $(this).find('.s2-s select').val() == thisId )
    {
      sub = true;
    }

    //

    if( $(this).find('.wt-s2 select').val() == thisIdw )
    {
      worker = true;
    }

    //

    if( $(this).find('.jobt2 select').val() != '' && $(this).find('.s2-s select').val() != '624' && $(this).find('.jobt2 select').val() == worktype )
    {
      work = true;
    }

   })

   //

   if( sub && worker && work )
    {
      //alert('This schedule already selected');
      //thisval.val('').change();
    }
   

}

function checkValidaion(thisval)
{
   
   var thisId = thisval.parent().parent().parent().find('.s1-s select').val();
   var thisIdw = thisval.parent().parent().parent().find('.wt-s1 select').val();
   var worktype = thisval.parent().parent().parent().find('.jobt1 select').val();

   //alert( thisval.val() );
   //alert( thisId );
   //alert( thisIdw );

   var sub = false;
   var worker = false;
   var work = false;


   $('.power-row-repeat').not(thisval.parent().parent().parent()).each(function(){

    if( $(this).find('.s1-s select').val() == thisId )
    {
      sub = true;
    }

    //

    if( $(this).find('.wt-s1 select').val() == thisIdw )
    {
      worker = true;
    }

    //

    if( $(this).find('.jobt1 select').val() != '' != '' && $(this).find('.s1-s select').val() != '624' && $(this).find('.jobt1 select').val() == worktype )
    {
      work = true;
    }

   })

   //

   if( sub && worker && work )
    {
      //alert('This schedule already selected');
      //thisval.val('').change();
    }
   

}

/* ------------------------------------------------------------------------------*/

/* ------------------------------------------------------------------------------*/

$(document).on('change', '.get-workers-2', function(event){

  arraySub = [];
  workerArray = [];

  var t = $(this);
  var wval = t.val();
  
  event.stopImmediatePropagation();
  var tv = t.val();
  var isub = false;

  $('.s2-s').each(function(e){
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
        arraySub.push( $(this).val() )
      }
    }
  })


  /* ------------ */  
var basketItems = arraySub;
var returnObj = {};

$.each(basketItems, function(key,value) {

var numOccr = $.grep(basketItems, function (elem) {
    return elem === value;
}).length;
    returnObj[value] = numOccr
});
  /* ------------ */

    $('.get-workers-2').not($(this)).each(function(e){
      if( $(this).val() != null )
      {
        if( $(this).val() != '' )
        {
          workerArray.push( $(this).val() )
        }
      }
    })



    if( returnObj[624] > 1)
    {
      if( workerArray.includes( wval ) )
      {
        alert('This value already selected');
        $(this).val('');
        $(this).selectpicker('refresh')
      }
    }

})

$(document).on('change', '.get-workers', function(event){

  arraySub = [];
  workerArray = [];

  var t = $(this);
  var wval = t.val();
  
  event.stopImmediatePropagation();
  var tv = t.val();
  var isub = false;

  $('.s1-s').each(function(e){
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
        arraySub.push( $(this).val() )
      }
    }
  })


  /* ------------ */  
var basketItems = arraySub;
var returnObj = {};

$.each(basketItems, function(key,value) {

var numOccr = $.grep(basketItems, function (elem) {
    return elem === value;
}).length;
    returnObj[value] = numOccr
});
  /* ------------ */

    $('.get-workers').not($(this)).each(function(e){
      if( $(this).val() != null )
      {
        if( $(this).val() != '' )
        {
          workerArray.push( $(this).val() )
        }
      }
    })



    if( returnObj[624] > 1)
    {
      if( workerArray.includes( wval ) )
      {
        alert('This value already selected');
        $(this).val('');
        $(this).selectpicker('refresh')
      }
    }

  
})

/* ------------------------------------------------------------------------------*/

/* ------------------------------------------------------------------------------*/

$(document).on('change', '.wt-s1-DEPRECATED', function(event){

  arraySub = [];
  workerArray = [];

  var t = $(this);
  var wval = t.val();
  
  event.stopImmediatePropagation();
  var tv = t.val();
  var isub = false;

  $('.s1-s').each(function(e){
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
        arraySub.push( $(this).val() )
      }
    }
  })


  /* ------------ */  
var basketItems = arraySub;
var returnObj = {};

$.each(basketItems, function(key,value) {

var numOccr = $.grep(basketItems, function (elem) {
    return elem === value;
}).length;
    returnObj[value] = numOccr
});
  /* ------------ */

    $('.wt-s1').not($(this)).each(function(e){
      if( $(this).val() != null )
      {
        if( $(this).val() != '' )
        {
          workerArray.push( $(this).val() )
        }
      }
    })


    console.log( workerArray )

    if( returnObj[currentSub] > 1)
    {
      if( workerArray.includes( wval ) )
      {
        alert('This value already selected');
        $(this).val('');
        $(this).selectpicker('refresh')
      }
    }

  
})

/* ------------------------------------------------------------------------------*/

/* ------------------------------------------------------------------------------*/



/* ------------------------------------------------------------------------------*/

/* ------------------------------------------------------------------------------*/

$(document).on('change', '.wt-s2-DEPRECATED', function(event){

  arraySub = [];
  workerArray = [];

  var t = $(this);
  var wval = t.val();
  
  event.stopImmediatePropagation();
  var tv = t.val();
  var isub = false;

  $('.s2-s').each(function(e){
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
        arraySub.push( $(this).val() )
      }
    }
  })

  console.log( arraySub );

  /* ------------ */  
var basketItems = arraySub;
var returnObj = {};

$.each(basketItems, function(key,value) {

var numOccr = $.grep(basketItems, function (elem) {
    return elem === value;
}).length;
    returnObj[value] = numOccr
});
  /* ------------ */

    $('.wt-s2').not($(this)).each(function(e){
      if( $(this).val() != null )
      {
        if( $(this).val() != '' )
        {
          workerArray.push( $(this).val() )
        }
      }
    })


    if( returnObj[currentSub2] > 1)
    {
      if( workerArray.includes( wval ) )
      {
        alert('This value already selected');
        $(this).val('');
        $(this).selectpicker('refresh')
      }
    }

  
})

/* ------------------------------------------------------------------------------*/




var options = { now: "{{ $t }}",twentyFour: false ,title: 'Select Time' };
var opt = { now: "{{ $t2 }}",twentyFour: false ,title: 'Select Time' };


@if( !isset($md->job_2_time) )

  var options2 = { title: 'Select Time' };
@endif


$('.job-time').on('focus',function(){
  $('.job-time').wickedpicker(options);
})

$('.job-2-time').on('focus',function(){
  $('.job-2-time').wickedpicker(opt);
})

//timepickers.wickedpicker('setTime', 0, "14:00");

var wrd = $('.gww').html()
var wrd2 = $('.gww2').html()

  $('.sc').click(function(){

    var t = $(this)

    if( t.is(':checked') == true )
    {

      $('.second-shift').show()
      $('.s2jobfield').attr('required','required')
      $('.job-2-time').attr('required','required')

    }
    else
    {
      $('.second-shift').hide()
      $('.s2jobfield').removeAttr('required')
      $('.job-2-time').removeAttr('required')
    }
  })



 $(document).on('change','.get-subcontractors', function(event){
    
    event.stopPropagation();
    var t = $(this);

    var sv = t.val();

    if( sv == 624 )
    {
      
      t.closest('.power-row-repeat').find('.rw').html(wrd);
      t.closest('.power-row-repeat').find('.rw').find('select').attr('required', 'required');
    }
    else
    {
      t.closest('.power-row-repeat').find('.rw').html('');

      var hd = "<input class='form-control' type='hidden' name='shift1[worker][]' value='2222222'>";

      var s1wtm = '<option selected="" value="null">-Title-</option>';

      var s1wts = "<select class='form-control small-i-w wt-s1' name='shift1[a_workertype_name]["+sv+"][]' data-live-search='true'>"
      
      @foreach( \DB::table('worker_types')->get() as $g )

      s1wtm += "<option value='{{ $g->id }}'>{{ $g->type }}</option>";

      @endforeach

      
      var s1wte = '</select>';

      //var s1 = "<input type='hidden' name='shift1[worker][]' value='2222222'><input class='form-control' placeholder='Worker Type' type='text' name='shift1[a_workertype_name]["+sv+"]'>";
      var s2 = "<input class='form-control small-i-n wnumber1' subid="+sv+" placeholder='No.' onkeydown='javascript: return event.keyCode == 69 ? false : true' type='number' liveChange='validateNumberInput' min='1' name='shift1[a_workertype_number]["+sv+"][]'>";

      t.closest('.power-row-repeat').find('.rw').html(hd+s1wts+s1wtm+s1wte+s2);

    }

    $('select').selectpicker('refresh');

  })

 $(document).on('change','.get-subcontractors-2', function(event){
    
    event.stopPropagation();
    var t = $(this);

    var sv = t.val();

    if( sv == 624 )
    {
      
      t.closest('.power-row-repeat').find('.rw').html(wrd2);
      t.closest('.power-row-repeat').find('.rw').find('select').attr('required', 'required');
    }
    else
    {
      t.closest('.power-row-repeat').find('.rw').html('');

      var hd = "<input class='form-control' type='hidden' name='shift2[worker][]' value='2222222'>";

      var s1wtm = '<option selected="" value="null">-Title-</option>';

      var s1wts = "<select class='form-control small-i-w wt-s2' name='shift2[a_workertype_name]["+sv+"][]' data-live-search='true'>"
      
      @foreach( \DB::table('worker_types')->get() as $g )

      s1wtm += "<option value='{{ $g->id }}'>{{ $g->type }}</option>";

      @endforeach

      
      var s1wte = '</select>';

      //var s1 = "<input type='hidden' name='shift2[worker][]' value='2222222'><input class='form-control' placeholder='Worker Type' type='text' name='shift2[a_workertype_name]["+sv+"]'>";
      var s2 = "<input  class='form-control small-i-n wnumber2' subid="+sv+" placeholder='No.' onkeydown='javascript: return event.keyCode == 69 ? false : true' type='number' liveChange='validateNumberInput' min='1' name='shift2[a_workertype_number]["+sv+"][]'>";

      t.closest('.power-row-repeat').find('.rw').html(hd+s1wts+s1wtm+s1wte+s2);


    }

    $('select').selectpicker('refresh');

  })

var s1 = $('.step-one-hidden').html();
  $(document).on('click','.add-power-row-wrap-button', function()
  {

    $('.ms1').find('.power-row-repeat:nth-child(2)').find('.delete-wrap').show();

    var wt1wts = '<select class="form-control jobt1" name="shift1[jobtype][]" data-live-search="true">';

      var wt1wtm = '<option value="">-Select Work Type-</option>';



      @foreach( \DB::table('work_types')->get() as $g )

      wt1wtm += "<option>{{ $g->type }}</option>";

      @endforeach

      var wt1wte = '</select>'

      wt1 = wt1wts+wt1wtm+wt1wte;

    $(this).parent().after('<div class="power-row-repeat"> <div class="step-one"> '+s1+' </div> <div class="step-one two rw"> <select data-live-search="true" class="form-control" required="required"> </select> </div> <div class="step-one for-label-wrap"> for </div> <div class="step-one four"> '+wt1+' </div> <div class="step-one five"> <input class="form-control s1note" type="text" name="shift1[note][]" placeholder="Notes" /> </div> <div class="step-one add-power-row-wrap add-power-row-wrap-button"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a> </div> <div class="step-one add-power-row-wrap delete-wrap"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div> </div>')

    $('select').selectpicker('refresh');
  })

  $(document).on('click','.delete-wrap', function()
  {
    if( $(this).parent().parent().children().length == 3 )
    {
      $(this).parent().parent().find('.power-row-repeat:nth-child(2)').find('.delete-wrap').hide();
    }



    $(this).parent().remove();


    if( $('.ms1').children().length == 2 )
    {
      $('.ms1').find('.power-row-repeat:nth-child(2)').find('.delete-wrap').hide();
    }
  })


var ss1 = $('.step-one-hidden-2').html();
$(document).on('click','.add-power-row-wrap-button-2', function()
  {

    $('.ms2').find('.power-row-repeat:nth-child(2)').find('.delete-wrap-2').show();

    var wt2wts = '<select class="form-control jobt2" name="shift2[jobtype][]" data-live-search="true">';

      var wt2wtm = '<option value="">-Select Work Type -</option>';

      @foreach( \DB::table('work_types')->get() as $g )

      wt2wtm += "<option>{{ $g->type }}</option>";

      @endforeach

      var wt2wte = '</select>'

      wt2 = wt2wts+wt2wtm+wt2wte;

    $(this).parent().after('<div class="power-row-repeat"> <div class="step-one"> '+ss1+' </div> <div class="step-one two rw"> <select data-live-search="true" class="form-control" required="required">  </select> </div> <div class="step-one for-label-wrap"> for </div> <div class="step-one four"> '+wt2+' </div> <div class="step-one five"> <input class="form-control s2note" type="text" name="shift2[note][]" placeholder="Notes" /> </div> <div class="step-one add-power-row-wrap add-power-row-wrap-button-2"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a> </div> <div class="step-one add-power-row-wrap delete-wrap-2"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div> </div>')

    $('select').selectpicker('refresh');
  })


  $(document).on('click','.delete-wrap-2', function()
  {
    if( $(this).parent().parent().children().length == 3 )
    {
      $(this).parent().parent().find('.power-row-repeat:nth-child(2)').find('.delete-wrap-2').hide();
    }



    $(this).parent().remove();


    if( $('.ms2').children().length == 2 )
    {
      $('.ms2').find('.power-row-repeat:nth-child(2)').find('.delete-wrap-2').hide();
    }
  })


  $('.job-date').datepicker({

    onSelect: function (dateText, inst) {

         var d = $(this).datepicker('getDate').getDate();
         var m = $(this).datepicker('getDate').getMonth()+1;
         var y = $(this).datepicker('getDate').getFullYear();

         location.href =  "{{ route('manpower.create') }}?d="+m+"-"+d+"-"+y+"&pr={{ $_REQUEST['pr'] }}";


      }
    });


    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this user?')) {
        
            event.preventDefault();

        }

    });


    serialize = function(obj) {
	  var str = [];
	  for (var p in obj)
		if (obj.hasOwnProperty(p)) {
		  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	  return str.join("&");
	}
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

	$(document).on("click","a.index-link", function(event){
        
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='Edit']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='View']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});


    var table;

    $(document).on('click', '.lc-id', function(event) {

        var lcstids = [];

        $(".workers-ids-value").each(function() {
            
            lcstids.push( $(this).text() );

        });

        lcstids.shift();

        localStorage.workerIds = JSON.stringify(lcstids);

    });

    $(document).on('click', '.lc-id', function(event) {

var lcstids = [];

$(".workers-ids-value").each(function() {
    
    lcstids.push( $(this).text() );

});

lcstids.shift();

localStorage.workerIds = JSON.stringify(lcstids);

});

    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_sub_workers_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

            stateSave: true,

            dom: 'lBfrtip',

            buttons: [
                {
                    extend: 'csvHtml5',
                    text: 'Export Workers',
                    exportOptions: {
                       modifier : {
             page : 'all', // 'all', 'current'
         },
                        columns: [1, 2, 3, 4, 5]
                    }
                }
            ],
        
            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No workers found!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,

            "ajax": {

              "url": "{{ route('subcontractor.datatable.workers.get') }}",

              "data": function ( d ) {

               d.status = "{{ @$_REQUEST['status'] }}"
               d.title = "{{ @$_REQUEST['title'] }}"
              
            }

            },
            
            columns: [

                { data: "name", name: "first_name", searchable: true, orderable: true },

                { data: "email", name: "email", searchable: true, orderable: false },

                { data: "phone_number", name: "phone_number", searchable: false, orderable: true },

                { data: "worker_type", name: "worker_type", searchable: false, orderable: false },
                
                { data: "ssn", name: "ssn", visible: false, orderable: true },

                { data: "created_at", name: "created_at", searchable: false, orderable: true }, 
                
                { data: "status", name: "status", searchable: false, orderable: false },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_sub_workers_pageLength", len);

        });

        function getQueryVariable(url, variable)
        {

          var query = url.substring(1);

          var vars = query.split('&');

          for (var i=0; i<vars.length; i++)
          {

              var pair = vars[i].split('=');

              if (pair[0] == variable)
              {

                return pair[1];

              }
            
          }

             return false;

        }

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#status-filter').change(function()
        {
             var statusVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&status="+statusVal;

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#title-filter').change(function()
        {
             var titleVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&title="+titleVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

    });

</script>

@endsection