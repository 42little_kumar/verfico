@extends('admin.layouts.app')

@section('title', 'Logs')

@section('styles')



    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('common/wickedpicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }
		div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
	right:187px;	
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}
#sidebarToggle{
display:none !important;	
}
.sidebar{
display:none !important;	
}
.fixed-logo {
    position: absolute;
    top: 0;
    width: 154px;
    left: 0;
    height: 4.375rem;
    display: flex;
    align-items: center;
    padding: 7px;
}
.topbar #sidebarToggleTop{
display:none !important;	
}

    </style>

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection

@section('content')
<div class="fixed-logo">
<img src="{{ asset('images/elements/blue-logo.png') }}" alt="logo" style="width: 100%">
</div>

<div class="container manprowe-page-wrap">


        

<div class="row">
  
	  
          <div class="col-lg-2">
		  
		  
		  
	   <div class="manpower-project-list-wrap">
	     <h3>Projects <i class="fa fa-tasks" aria-hidden="true"></i></h3>
		 <ul>
		  @foreach( $p as $g )

                  <li><a projectId="{{ $g->id }}" class="project-select-li @if( $_REQUEST['pr'] == $g->id ) active @endif"  href="javascript:void();"><span>{{ $g->job_name }}</span>  <div class="project-list-icons"><i class="fa fa-calendar-check-o @if( ifShd( $g->id, $_REQUEST['d'] ) ) active @endif" aria-hidden="true"></i> <i class="fa fa-envelope-o @if( ifEmail( $g->id, $_REQUEST['d'] ) ) active @endif" aria-hidden="true"></i></div></a></li>

      @endforeach
		 </ul>
	   </div>
	</div>
    <div class="col-lg-10" style="position:relative;">
       <div class="send-all-email-pre-wrap logs-position-wrap"> 

      {{--<a href='{{ route("schedule.emails.all") }}?d={{ $_REQUEST["d"] }}&pr={{ $_REQUEST["pr"] }}' onclick="return confirm('Are you sure you want to send email to all schedules for tomorrow?')" class="send-email-href">Email all schedules</a>--}}
      
      </div>
	   
	   <div class="d-sm-flex align-items-center justify-content-between mb-4 manpower-top-btn-wrap">
	    <h1 class="h3 mb-0 text-gray-800" style="text-align:left;">Manpower Logs</h1> 
      
        </div>
		
		  
	  
		 <div class="schedule-project-row">
		<div class="row">

      <?php $p = \DB::table('project')->where( 'deleted_at', NULL )->where('superint', Auth::user()->id)->get(); ?>

      <div class="col-md-12" style="padding-bottom: 20px; display:none;" >

              <select name="project_id" required="" data-live-search='true'>

                @foreach( $p as $g )

                  <option @if( $_REQUEST['pr'] == $g->id ) selected @endif value="{{ $g->id }}">{{ $g->job_name }}</option>

                @endforeach
                 
              </select> 

            </div>

		 <div class="col-md-3">

     

      <h2 style="margin:0px; font-weight:bold; font-size:18px; color:#000;"><b>Project</b>: {{ \DB::table('project')->where('id', $_REQUEST['pr'])->first()->job_name }}</h2>

     

		  
		 </div>
		</div>
		</div>

    <?php

          $dt = date('m-d-Y');

          if( isset( $_GET['d'] ) )
          {
            $dt = $_GET['d'];
          }

          $timestamp = strtotime($dt);

          $input  = $dt;
          
        $format = 'm-d-Y';

        $c = \Carbon\Carbon::createFromFormat($format, $input);

          $d = $c->format('l');
          $da = $c->format('d');
          $m = $c->format('F');
          $y = $c->format('Y');

          $dtv = \Carbon\Carbon::createFromFormat($format, $dt)->format('m/d/Y');

?>

		<div class="manpower-next-pre-wrap">
			   <div class="row manpower-top-btn-wrap">
				<div class="col-3">
				 <div class="manpower-next-pre-button-wrap">
					<a href="{{ route('manpower.dl') }}?d={{ $c->subDays(1)->format('m-d-Y')}}&pr={{ $_REQUEST['pr'] }}" class="btn">Previous</a>
				  </div>
				</div>
				
				<div class="col-6">
				  <div class="manpower-date-wrap ">
					<strong>{{ $d }}</strong>
        <p>{{ $da }} {{ $m }}, {{ $y }}</p>
        <input class="form-control job-date" name="job_date" type="text" id="datetype" placeholder="Select date" value="{{ $dtv }}" autocomplete="off" style="background:white;"/ />
					</div>
				  </div>
				  
				  <div class="col-3">
				   <div class="manpower-next-pre-button-wrap" style="text-align:right;">
					<a href="{{ route('manpower.dl') }}?d={{ $c->addDays(2)->format('m-d-Y')}}&pr={{ $_REQUEST['pr'] }}" class="btn">Next</a>
          
				  </div>
				  </div>
				</div>
		</div>

    

    @if( empty( $md ) )
    
      <p class="no-data-found">No manpower schedule found</p>
      
    @else

    

      <form action="{{ route('manpower.update'), $mid }}" method="post">

        @csrf

        <input type="hidden" value="{{ $mid }}" name='id'>

        <div class="card shadow mb-4">
            <div class="card-body worker_page_wrapper">
			
                  <div class="manpower-filter-wrap" style="padding:0px; background:none; border-radius:0px;">
                    <div class="row">
						<div class="col-md-3">

						
						<div class="col-md-3">
						 
						</div>
					</div>   
                    
					
					
					
                          
                    <!-- Status filter -->

                    <!--select id="status-filter">

                        <option disabled selected>-Select Status-</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 2 ) selected  @endif value="2">Active</option>

                        <option @if( isset( $_REQUEST['status'] ) && @$_REQUEST['status'] == 0 ) selected  @endif value="0">Blocked</option>

                    </select-->

                    <!-- Status filter -->

                    <!-- Title filter -->

                    <!-- Title filter -->

                    <!-- Reset filter -->

                    @if(
                    (
                        isset( $_GET['status'] ) ||
                        isset( $_GET['title'] )
                    )
                    )

                    <a style="display:none;" href="{{ route('subcontractor.workers') }}" class="red_btn black_btn reset-filter user-reset-filter pet-user-filter" style="font-weight: 500"><i class="fas fa-sync-alt"></i>  Reset</a>

                    @endif

                    <!-- Reset filter -->

                </div>
				
            <div class="step-one-hidden" style="display: none;">
                  <select name="shift1[sub][]" class="form-control get-subcontractors">
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>

            <div class="step-one-hidden-2" style="display: none;">
                  <select name="shift2[sub][]" class="form-control get-subcontractors-2">
      
                @foreach( $s as $g )

                  <option value="{{ $g->id }}">{{ $g->company_name }}</option>

                @endforeach
              </select> 
            </div>


            <div class="step-one-hidden gww" style="display:none;">
                  <select name="shift1[worker][]" class="form-control get-workers">
      
                @foreach( $w as $g )

                  <option value="{{ $g->id }}">{{ $g->first_name }}</option>

                @endforeach
              </select> 
            </div>

            <div class="step-one-hidden gww2" style="display:none;">
                  <select name="shift2[worker][]" class="form-control get-workers">
      
                @foreach( $w as $g )

                  <option value="{{ $g->id }}">{{ $g->first_name }}</option>

                @endforeach
              </select> 
            </div>

            @php

              $mds1 = json_decode( $md->emp_data );
              //array_shift( $mds1->sub );
              //array_shift( $mds1->worker );

            @endphp

  
            <?php

            $mds2 = [];

            if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
              //array_shift( $mds2->sub );
              //array_shift( $mds2->worker );

            }

            ?>

            <?php

            $dp = [];
            $dp2 = [];

            ?>

				<div class="manpower-text-body-wrap" style="margin:0px; padding:0px; background:#fff; border-radius:0px;">
				   <div class="step-one two rw">

            <span class="project_name">
             
            
            

            </span>

			    <ul class="log-list-flex">   
					<li>
					  <b>WORKERS</b>
            <b>WORK TYPE</b>
					  <b>IN TIME</b>
					  <b>OUT TIME</b>
            
					  
					</li>
			    </ul>
				</div>

        <?php

        $url = URL::to('/').'/api/v1/getscheduleforadmin?date='.$_REQUEST["d"].'&project_id='.$_REQUEST["pr"];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        $allLogs = json_decode($output);

        //echo '<pre>';

        //print_r( $allLogs );

        ?>
				
        <div>

          

            @foreach( $allLogs->data as $g )

            <ul class='log-list-flex'>

            <li @if( !isset( $g->occupied ) ) class="loggreen" @endif @if( empty( $g->punch_in ) && empty( $g->punch_out ) ) class="logred" @endif >
            <div class='log-worker-name'>
            <span>{{ $g->sub_name }} @if( !empty( $g->worker_name ) ) {{ ucfirst( $g->worker_name ) }} @else {{ ucfirst( $g->worker_type ) }} @endif</span>
            
            </div>

            <div>
            <span class='in-time-text'> {{ $g->work_type }} </span>
            </div>  

            <div>
            <span class='in-time-text'>@if( !empty( $g->punch_in ) ) {{ gt( $g->punch_in ) }} @else - @endif</span>
            </div>
            <div>
            <span class='out-time-text'>@if( !empty( $g->punch_out ) ) {{ gt( $g->punch_out ) }} @else - @endif</span>
            </div>  
            
            </li>

            </ul>  

            @endforeach

        </div>

        

    </div>

</div>
</div>
</div>


</form>

@endif
</div>
</div>

@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>  

<script src="{{ asset('common/wickedpicker.min.js') }}"></script>  

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

  $('select[name="project_id"]').change(function(){
    location.href =  "{{ route('manpower.dl') }}?d={{ $_REQUEST['d'] }}&pr="+$(this).val();
  })

  $('.project-select-li').click( function() {

    location.href =  "{{ route('manpower.dl') }}?d={{ $_REQUEST['d'] }}&pr="+$(this).attr('projectId');

  } )

var wrd = $('.gww').html()
var wrd2 = $('.gww2').html()

  $('.sc').click(function(){

    var t = $(this)

    if( t.is(':checked') == true )
    {

      $('.second-shift').show()
      $('.s2jobfield').attr('required','required')

    }
    else
    {
      $('.second-shift').hide()
      $('.s2jobfield').removeAttr('required')
    }
  })



 $(document).on('change','.get-subcontractors', function(event){
    
    event.stopPropagation();
    var t = $(this);

    var sv = t.val();

    if( sv == 624 )
    {
      
      t.closest('.power-row-repeat').find('.rw').html(wrd);
    }
    else
    {
      t.closest('.power-row-repeat').find('.rw').html('');

      var s1 = "<input type='hidden' name='shift1[worker][]' value='2222222'><input class='form-control' placeholder='Worker Type' type='text' name='shift1[a_workertype_name]["+sv+"]'>";
      var s2 = "<input class='form-control' placeholder='No.' type='text' name='shift1[a_workertype_number]["+sv+"]'>";

      t.closest('.power-row-repeat').find('.rw').html(s1+s2);


    }


  })

 $(document).on('change','.get-subcontractors-2', function(event){
    
    event.stopPropagation();
    var t = $(this);

    var sv = t.val();

    if( sv == 624 )
    {
      
      t.closest('.power-row-repeat').find('.rw').html(wrd2);
    }
    else
    {
      t.closest('.power-row-repeat').find('.rw').html('');

      var s1 = "<input type='hidden' name='shift2[worker][]' value='2222222'><input class='form-control' placeholder='Worker Type' type='text' name='shift2[a_workertype_name]["+sv+"]'>";
      var s2 = "<input class='form-control' placeholder='No.' type='text' name='shift2[a_workertype_number]["+sv+"]'>";

      t.closest('.power-row-repeat').find('.rw').html(s1+s2);


    }


  })

var s1 = $('.step-one-hidden').html();
  $(document).on('click','.add-power-row-wrap-button', function()
  {

    

    $(this).closest('.manpower-text-body-wrap').append('<div class="power-row-repeat"> <div class="step-one"> '+s1+' </div> <div class="step-one two rw"> <select data-live-search="true" class="form-control" required="required"> </select> </div> <div class="step-one for-label-wrap"> for </div> <div class="step-one four"> <select class="form-control" name="shift1[jobtype][]"> <option>Cleaning</option><option>Washing</option><option>Decorating</option> </select> </div> <div class="step-one five"> <input class="form-control" type="text" name="shift1[note][]" placeholder="Notes" /> </div> <div class="step-one add-power-row-wrap add-power-row-wrap-button"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a> </div> <div class="step-one add-power-row-wrap delete-wrap"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div> </div>')
  })

  $(document).on('click','.delete-wrap', function()
  {
    $(this).parent().remove();
  })


var ss1 = $('.step-one-hidden-2').html();
$(document).on('click','.add-power-row-wrap-button-2', function()
  {

    

    $(this).closest('.manpower-text-body-wrap').append('<div class="power-row-repeat"> <div class="step-one"> '+ss1+' </div> <div class="step-one two rw"> <select data-live-search="true" class="form-control" required="required">  </select> </div> <div class="step-one for-label-wrap"> for </div> <div class="step-one four"> <select class="form-control" name="shift2[jobtype][]"> <option>Cleaning</option><option>Washing</option><option>Decorating</option> </select> </div> <div class="step-one five"> <input class="form-control" type="text" name="shift2[note][]" placeholder="Notes" /> </div> <div class="step-one add-power-row-wrap add-power-row-wrap-button-2"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-plus add-row"></i></a> </div> <div class="step-one add-power-row-wrap delete-wrap-2"> <a class="add-power-btn" href="javascript:void();"><i class="fa fa-minus delete-row"></i></a> </div> </div>')
  })


  $(document).on('click','.delete-wrap-2', function()
  {
    $(this).parent().remove();
  })


  $('.job-date').datepicker({

    onSelect: function (dateText, inst) {

         var d = $(this).datepicker('getDate').getDate();
         var m = $(this).datepicker('getDate').getMonth()+1;
         var y = $(this).datepicker('getDate').getFullYear();

         location.href =  "{{ route('manpower.create') }}?d="+m+"-"+d+"-"+y+"&pr={{ $_REQUEST['pr'] }}";


      }
    });


    $(document).on('click', '.btn-action-delete', function(event) {

        if(!confirm('Are you sure you want to delete this user?')) {
        
            event.preventDefault();

        }

    });


    serialize = function(obj) {
	  var str = [];
	  for (var p in obj)
		if (obj.hasOwnProperty(p)) {
		  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	  return str.join("&");
	}
    
    $.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

	$(document).on("click","a.index-link", function(event){
        
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='Edit']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});
	$(document).on("click","a[title='View']", function(event){
		var order  = table.order();
		
		top.location.href = $(this).attr("href")+"?column="+order[0][0]+"&dir="+order[0][1]+"&from=workers&title="+$.urlParam('title')+"&status="+$.urlParam('status')+"&company="+$.urlParam('company');
		
		event.preventDefault();
	});


    var table;

    $(document).on('click', '.lc-id', function(event) {

        var lcstids = [];

        $(".workers-ids-value").each(function() {
            
            lcstids.push( $(this).text() );

        });

        lcstids.shift();

        localStorage.workerIds = JSON.stringify(lcstids);

    });

    $(document).on('click', '.lc-id', function(event) {

var lcstids = [];

$(".workers-ids-value").each(function() {
    
    lcstids.push( $(this).text() );

});

lcstids.shift();

localStorage.workerIds = JSON.stringify(lcstids);

});

    function initialize_database() {

        $.fn.dataTable.moment('MM-DD-YYYY');

        var pageLength = localStorage.getItem("dataTable_sub_workers_pageLength");

        if(pageLength == null) {

            pageLength = 10;

        }

        table = $('#datatable').DataTable({

            stateSave: true,

            dom: 'lBfrtip',

            buttons: [
                {
                    extend: 'csvHtml5',
                    text: 'Export Workers',
                    exportOptions: {
                       modifier : {
             page : 'all', // 'all', 'current'
         },
                        columns: [1, 2, 3, 4, 5]
                    }
                }
            ],
        
            "bLengthChange": true,

            pageLength: pageLength,

            language: {

            sLengthMenu: "_MENU_",

            search: "_INPUT_",

            emptyTable: 'No workers found!',

            searchPlaceholder: "Search...",
             sProcessing: "<i class='fas fa-spinner fa-spin'></i>",
             },

            processing: true,

            serverSide: false,

            "ajax": {

              "url": "{{ route('subcontractor.datatable.workers.get') }}",

              "data": function ( d ) {

               d.status = "{{ @$_REQUEST['status'] }}"
               d.title = "{{ @$_REQUEST['title'] }}"
              
            }

            },
            
            columns: [

                { data: "name", name: "first_name", searchable: true, orderable: true },

                { data: "email", name: "email", searchable: true, orderable: false },

                { data: "phone_number", name: "phone_number", searchable: false, orderable: true },

                { data: "worker_type", name: "worker_type", searchable: false, orderable: false },
                
                { data: "ssn", name: "ssn", visible: false, orderable: true },

                { data: "created_at", name: "created_at", searchable: false, orderable: true }, 
                
                { data: "status", name: "status", searchable: false, orderable: false },
                
                { data: "action", name: "action", searchable: false, orderable: false },

            ],

        });

    }

    $(document).ready(function() {

        initialize_database();

        $('#datatable').on('length.dt', function (e, settings, len) {

            localStorage.setItem("dataTable_sub_workers_pageLength", len);

        });

        function getQueryVariable(url, variable)
        {

          var query = url.substring(1);

          var vars = query.split('&');

          for (var i=0; i<vars.length; i++)
          {

              var pair = vars[i].split('=');

              if (pair[0] == variable)
              {

                return pair[1];

              }
            
          }

             return false;

        }

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#status-filter').change(function()
        {
             var statusVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&status="+statusVal;

             if( getQueryVariable(originalUrl, 'title') )
             {

              qString += "&title="+getQueryVariable(originalUrl, 'title');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

        /*
        * @Subcontractor Filter
        *
        *
        */
        $('#title-filter').change(function()
        {
             var titleVal = $( this ).val();

             var url = window.location.href;

             var originalUrl = url;

             var a = url.indexOf("?");

             var b =  url.substring(a);

             var c = url.replace(b,"");

             url = c;

             var qString = "&title="+titleVal;

             if( getQueryVariable(originalUrl, 'status') )
             {

              qString += "&status="+getQueryVariable(originalUrl, 'status');

             }

             url += '?userType=worker'+qString;

             window.location.href = url;


        })

    });

</script>

@endsection