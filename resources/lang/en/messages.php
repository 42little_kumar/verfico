<?php 
return[
    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'LOGIN_VALID'=>'Please enter valid login details.',
    'LOGIN_SUCCESS'=>'User has been succussfully login.',
    'UNAUTH'=>'Please check your credentials.',
    'UNAUTH_ROLE'=>"You're not authorized to login to the app.",
    'NUMBER_NOT_EXISTS'=>'Email or Mobile Number does not exists.',
    'EMAIL_NOT_EXISTS'=>'Email or Mobile Number does not exists.',
    'CHANGE_PASS_REQ'=>'Change password request has been sent to the admin',
    'EXECUTION_ERROR'=>'Some technical error occurred.',
    'CHANGE_PASS_SUCCESS'=>'Password changed successfully.',
    'REGISTER_SUCCESS'=>'Your account details has been sent for verification. Once verified you can login successfully.',
    'APPROVE'=>'Your account has been registered but not verified. Please wait for some time.',
    'BLOCK_ACCOUNT'=>'Your account has been blocked by the admin. Please contact us at verfico.rockspringcontracting@gmail.com',
    'OFFLINE_MARKED'=>'Your offline punch successfully marked.',
    'PUNCH_SUCCESS'=>'Your punch successfully marked.',
    'PUNCH_LIST'=>'List of punch marked.',
    'PUNCH_UPDATE'=>"Record successfully update.",
    'WORKER_LIST'=>"List Of Workers.",
    'ACCOUNT_APPROVED'=>"Account has been approved.",
    'ACCOUNT_REJECTED'=>"Account has been rejected.",
    'RECORD_UPDATE'=>"Record successfully update.",
    'RECORD_DELETE'=>"Record successfully deleted.",
    'PUNCH_STATUS'=>"Punch status successfully fetch.",
    'PROJECT_LIST'=>"List of projects.",
    'PROJECT_DELETED'=>"Project not found or has been deleted.",
    'FETCH'=>"Records successfully fetch.",
    'IS_BLOCKED'=>"Your account has been blocked by the admin. Please contact us at verfico.rockspringcontracting@gmail.com",
    'IS_DELETED'=>"Your account has been deleted by the admin. Please contact us at verfico.rockspringcontracting@gmail.com",
    'USER_EXISTS'=>"User exists.",
    'CHECK_RESET_EMAIL' => "Please check your registered email address to reset your password.",
    'ACTION_ALREADY_PERFORMED' => "This action has already been performed on the request.",
    'UNCORRECT_EMAIL_FORMAT' => "Please enter valid email address.",
    'UNCORRECT_PHONE_NUMBER_FORMAT' => "Please enter a valid 10 digits phone number.",
    'SAFETY_CREATED' => "Safety request has been created successfully.",
    'SAFETY_VIEW' => "Listing for the safety requests.",
    'TO_ADMIN_SENT' => "We have sent your request to the Admin. Our team will get back to you soon."
    
];