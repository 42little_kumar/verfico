<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'LOGIN_VALID'=>'Ingrese detalles de inicio de sesión válidos.',
    'LOGIN_SUCCESS'=>'La usuario ha sido exitosamente iniciar sesión.',
    'UNAUTH'=>'No autorizado.',
    'UNAUTH_ROLE'=>'No tiene autorización para iniciar sesión en la aplicación.',
    'NUMBER_NOT_EXISTS'=>'El número de móvil no existe.',
    'EMAIL_NOT_EXISTS'=>'Email does not exists.',
    'CHANGE_PASS_REQ'=>'La solicitud de cambio de contraseña ha sido enviada al administrador.',
    'EXECUTION_ERROR'=>'Se produjo un error técnico.',
    'CHANGE_PASS_SUCCESS'=>'La contraseña ha cambiado exitosamente.',
    'REGISTER_SUCCESS'=>'El usuario se registró con éxito.',
    'APPROVE'=>'Tu cuenta no ha sido aprobada.',
    'BLOCK_ACCOUNT'=>'Su cuenta ha sido bloqueada por el administrador. Por favor contactenos a verfico.rockspringcontracting@gmail.com',
    'OFFLINE_MARKED'=>'Su golpe fuera de línea marcado con éxito.',
    'PUNCH_SUCCESS'=>'Su golpe marcado con éxito.',
    'PUNCH_LIST'=>'Lista de golpes marcados.',
    'PUNCH_UPDATE'=>"Registro actualizado con éxito.",
    'WORKER_LIST'=>"Lista de trabajadoras.",
    'ACCOUNT_APPROVED'=>"Cuenta aprobada.",
    'ACCOUNT_REJECTED'=>"Account has been rejected.",
    'RECORD_UPDATE'=>"Actualización de registro exitosa.",
    'RECORD_DELETE'=>"Registro eliminado con éxito.",
    'PUNCH_STATUS'=>"El estado de perforación se recupera correctamente.",
    'PROJECT_DELETED'=>"Project not found or has been deleted.",
    'PROJECT_LIST'=>"List of projects.",
    'IS_BLOCKED'=>"Su cuenta ha sido bloqueada por el administrador. Por favor contactenos a verfico.rockspringcontracting@gmail.com",
    'IS_DELETED'=>"Su cuenta ha sido eliminada por el administrador. Por favor contactenos a verfico.rockspringcontracting@gmail.com",
    'USER_EXISTS'=>"User exists.",
    'CHECK_RESET_EMAIL' => "Verifique su dirección de correo electrónico registrada para restablecer su contraseña.",
    'ACTION_ALREADY_PERFORMED' => "Esta acción ya se realizó en la solicitud.",
    'UNCORRECT_EMAIL_FORMAT' => "Por favor ingresa un correo eléctronico válido.",
    'UNCORRECT_PHONE_NUMBER_FORMAT' => "Por favor ingresa los 10 digitos de número teléfonico.",
    'SAFETY_CREATED' => "La solicitud de seguridad se ha creado correctamente.",
    'SAFETY_VIEW' => "Listado para las solicitudes de seguridad.",
    'TO_ADMIN_SENT' => "Hemos enviado su solicitud al administrador. Nuestro equipo se pondrá en contacto con usted pronto."
];
 