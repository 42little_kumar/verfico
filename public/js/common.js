$('body').on('change','.custom-select', function(){


var url = window.location.href;    
url = updateQueryStringParameter(url, 'rowcount', $(this).val())

window.location.href = url;

})


function updateQueryStringParameter(uri, key, value) {
   var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
   var separator = uri.indexOf('?') !== -1 ? "&" : "?";
   if (uri.match(re)) {
     return uri.replace(re, '$1' + key + "=" + value + '$2');
   }
   else {
     return uri + separator + key + "=" + value;
   }
 }