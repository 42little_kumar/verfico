function previewImage(input, element) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {

            $(element).attr('src', e.target.result);

        }

        reader.readAsDataURL(input.files[0]);

    }

}

function parseAmount(string) {

    var value = string.trim().replace(/[^0-9.]/g,'');

    if(value != '' && value != '.') {

        var amount = value.substr(0,9);

        if(amount.indexOf('.') != amount.lastIndexOf('.')) {

            amount = parseFloat(amount).toString();

        }

        if(amount.indexOf('.') > 0) {

            var arr = amount.split('.');

            if(arr[1] != '') {

                arr[1] = arr[1].substr(0,2);

                amount = arr[0] + '.' + arr[1];

            }

        }

        return amount;

    } else {

        return false;

    }

}

$(document).ready(function() {
    $('.manpower-project-list-wrap ul li a').on('click', function() {
	 $('.manpower-project-list-wrap ul li a').removeClass('active');
     $(this).addClass('active');	 
	});
	
    $('select').selectpicker();

    $('#page-filter-form select[name="limit"], #page-filter-form select[name="type"], #page-filter-form select[name="business"], #page-filter-form select[name="status"], #page-filter-form select[name="role"], #page-filter-form select[name="sort-by"]').on('change', function(event) {

        $('#page-filter-form').submit();

        event.preventDefault();

    });

    $('table').addClass('table-responsive');
    
    $('.modal-dialog').addClass('modal-dialog-centered');
    
    $('#sidebarToggleTop').click(function(){
    
        $('.sidebar').toggleClass('open_nav');
    
    });


    $('.open_list_mobile').on('click', function() {
   
       $('.open_list_mobile i').toggleClass('fa-times');
       $('.list_scroll_wrap').toggleClass('list_scroll_wrapshow');  
    });

    $(document).click(function(event) {
  if (!$(event.target).closest(".list_scroll_wrap,.open_list_mobile").length) {
    $("body").find(".list_scroll_wrap").removeClass("list_scroll_wrapshow");
  }
});

});