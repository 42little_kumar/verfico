@extends('admin.layouts.app')

@section('title', 'Edit')

@section('styles')

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('common/wickedpicker.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/datatables/datatables.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.min.css') }}">

    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <style type="text/css">

        select[name="datatable_length"] {

            width: 70px !important;

        }
    div#datatable_info {
    float: left;
}
         div#datatable_paginate {
    margin-top: 12px;
}
 .worker_page_wrapper .dataTables_length{
  right:187px;  
 }

table tr td:nth-child(5) {
    display: none;
}
table tr th:nth-child(5) {
    display: none;
}

.wickedpicker {
    z-index: 9999;
}
footer{
display:none;	
}

    </style>

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />

@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">         
    <h1 class="h3 mb-0 text-gray-800">Manpower Requests</h1>    
</div>


<div class="row">

   
       <div class="card shadow col-lg-12">
 

            <div class="view-man-pawer-top-detail">
           

        
               <b class="project-name-wrap">
                @foreach( $p as $g )

                @if( $md->project_id == $g->id )

                Project Name: {{ $g->job_name }}

                @endif

                @endforeach
              </b>
			  <p class="project-date-time">Date: {{date( 'm/d/Y', strtotime( $md->job_date ) )}}</p>
			  <p class="project-date-time">Time: {{ $md->job_time }}</p>

            
           
          </div>   

            <ul class="view-power-list-wrap" style="margin:0px;">
			  <li>
			    <b>Worker Name</b>
				<b>No. of Workers</b>
			    <b>Work Type</b>
			    <b>Notes</b>
			  </li>
			</ul>
            @php

              $mds1 = json_decode( $md->emp_data );
           

            @endphp

  
            <?php

            $mds2 = [];

            if( !empty( $md->emp_data_2 ) )
            {

              $mds2 = json_decode( $md->emp_data_2 );
             

            }

            ?>

            <?php

            $dp = [];
            $dp2 = [];

            ?>

  
			     <ul class="view-power-list-wrap">
	       
          
          @foreach( $mds1->sub as $k => $g )

          <li>

          @if( $mds1->sub[$k] == Auth::user()->id )

          
             

            @if( $mds1->sub[$k] == 624 )


                <!-- Rosa case shift 1 start -->
                    
                <span> 
                @foreach( $w as $g )

                @if( $mds1->worker[$k] == $g->id )

                {{ $g->first_name }} {{ $g->last_name }}

                @endif

                @endforeach
                </span> 
				
				 <span>&nbsp;</span>
                <!-- Rosa case shift 1 end -->
     

            @else
				
           
			
		    <span>
            

              @php

              $awt = (array)$mds1->a_workertype_name;
              $awtn = (array)$mds1->a_workertype_number;
              $sid = $mds1->sub[$k];

              @endphp

              <?php

              if( count( $awt[$sid] ) > 1 )
              {

                 if( isset( $dp[$sid] ) )
                {
                  $awt = $awt[$sid][$dp[$sid]];
                  $awtn = $awtn[$sid][$dp[$sid]];

                  $dp[$sid] = $dp[$sid]+1;
                }
                else
                {
                  $awt = $awt[$sid][0];
                  $awtn = $awtn[$sid][0];

                  $dp[$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[$sid][0];
                $awtn = $awtn[$sid][0];

              }

              ?>

             
              
      
      @foreach( \DB::table('worker_types')->get() as $g )

      @if( $awt == $g->id )

      <!-- shift 1 type start -->

      {{ $g->type }}

      <!-- shift 1 type start -->

      @endif
    </span>
	
      @endforeach
	    
	    


              <!-- shift 1 no start -->
             <span>
             {{ $awtn }}
            </span> 
              <!-- shift 1 no end -->

            @endif
			
			   
              
                @foreach( \DB::table('work_types')->get() as $g )



                @if( $mds1->jobtype[$k] == $g->type )

                <!-- shift 1 job type start -->
                <span>  
                {{ $g->type }}
               </span>
                <!-- shift 1 job type end -->


                @endif

                    @endforeach
                
	

             <span>
               <!-- shift 1 note start -->

               {{ $mds1->note[$k] }}

               <!-- shift 1 note end -->
            
            </span>   
            @if( $k != 0 )
            
            @endif


            @endif

            </li>

          @endforeach

        

      </ul>
  
  

 @if( !empty( $mds2 ) )

</div>

<div class="col-lg-12">
<br>
 <h1 class="h3 mb-0 text-gray-800">Shift 2</h1>    
   <br>
</div>

<div class="card shadow col-lg-12"> 
   
       <ul class="view-power-list-wrap" style="margin:20px 0 0;">
			  <li>
			    <b>Worker Name</b>
				<b>No. of Workers</b>
			    <b>Work Type</b>
			    <b>Notes</b>
			  </li>
			</ul>
			
<ul class="view-power-list-wrap">

        
           

              @foreach( $mds2->sub as $k => $g )

              <li>

              @if( $mds2->sub[$k] == Auth::user()->id )
              
               
            
            
            @if( $mds2->sub[$k] == 624 )

            
              
      
                @foreach( $w as $g )

                @if( $mds2->worker[$k] == $g->id )

                <!-- shift 2 name start -->

                {{ $g->first_name }} {{ $g->last_name }}

                <!-- shift 2 name end -->


                @endif

                  

                @endforeach
          
            

            @else

          

              @php

              $awt = (array)$mds2->a_workertype_name;
              $awtn = (array)$mds2->a_workertype_number;
              $sid = $mds2->sub[$k];

              @endphp

              <?php

              if( count( $awt[$sid] ) > 1 )
              {

                 if( isset( $dp2[$sid] ) )
                {
                  $awt = $awt[$sid][$dp2[$sid]];
                  $awtn = $awtn[$sid][$dp2[$sid]];

                  $dp2[$sid] = $dp2[$sid]+1;
                }
                else
                {
                  $awt = $awt[$sid][0];
                  $awtn = $awtn[$sid][0];

                  $dp2[$sid] = 1;

                }

              }
              else
              {

                $awt = $awt[$sid][0];
                $awtn = $awtn[$sid][0];

              }

              ?>

      @foreach( \DB::table('worker_types')->get() as $g )

      @if( $awt == $g->id )

      {{ $g->type }}

      @endif

      @endforeach

      <!-- shift 2 no start -->
	  
	        :{{ $awtn }}

          <!-- shift 2 no end -->
              


            @endif
			
                <span> For</span> 

                @foreach( \DB::table('work_types')->get() as $g )

                 @if( $mds2->jobtype[$k] == $g->type )

                 <!-- shift 2 type start -->

                 {{ $g->type }}

                 <!-- shift 2 type end -->

                 @endif


                    @endforeach
              


           
            <!-- shift 2 note start -->

            {{ $mds2->note[$k] }}

            <!-- shift 2 note end --> 
         
 
          
            @if( $k != 0 )
            
            @endif


            @endif

          </li>

          @endforeach

            @else

            
          </ul>
           


 @endif

</div>
</div>


@endsection

@section('scripts')

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') }}"></script>

<script src="{{ asset('js/daterangepicker.js') }}"></script>

<script src="{{ asset('js/jquery.timepicker.min.js') }}"></script>

<script src="{{ asset('common/wickedpicker.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/jquery-datatables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.20/sorting/datetime-moment.js"></script>

<script type="text/javascript">

  $("#datetype").on("keypress", function(e){
   
        e.preventDefault();
    
});

var currentSub = '';
var currentSub2 = '';
var arraySub = [];

$(document).on('change', '.s1-s', function(event){


    
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
      //alert( $(this).val() ); 
      currentSub = $(this).val();

      }
    }
    
  

})

$(document).on('change', '.s2-s', function(event){


    
    if( $(this).val() != null )
    {
      if( $(this).val() != '' )
      {
      //alert( $(this).val() ); 
      currentSub2 = $(this).val();

      }
    }
    
  

})



</script>

@endsection