<?php

/*
|--------------------------------------------------------------------------
| Subcontractor Routes
|--------------------------------------------------------------------------
|
|
*/

Route::group([ 'prefix' => 'superint', 'middleware' => [ 'auth', 'superadmin', 'prevent-back-history' ] ], function() {

Route::get('/manpower', [

    'uses' => 'Superint\ManpowerController@index',

    'as' => 'manpower'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/add', [

    'uses' => 'Superint\ManpowerController@create',

    'as' => 'manpower.create'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/delete/{id}', [

    'uses' => 'Superint\ManpowerController@destroy',

    'as' => 'manpower.delete'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/get', [

    'uses' => 'Superint\ManpowerController@get',

    'as' => 'manpower.get'

])->middleware('role_or_permission:Superint');

Route::post('/manpower/store', [

    'uses' => 'Superint\ManpowerController@store',

    'as' => 'manpower.save'

])->middleware('role_or_permission:Superint');

Route::post('/manpower/update', [

    'uses' => 'Superint\ManpowerController@update',

    'as' => 'manpower.update'

])->middleware('role_or_permission:Superint');

Route::post('/manpower/copy_previous', [

    'uses' => 'Superint\ManpowerController@copyPrevious',

    'as' => 'manpower.copy'

])->middleware('role_or_permission:Superint');

Route::post('/manpower/copy_previous_by_date', [

    'uses' => 'Superint\ManpowerController@copyPreviousByDate',

    'as' => 'manpower.copy.by.date'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/edit/{id}', [

    'uses' => 'Superint\ManpowerController@edit',

    'as' => 'manpower.edit'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/copy/edit/{id}', [

    'uses' => 'Superint\ManpowerController@copyEdit',

    'as' => 'manpower.copy.edit'

])->middleware('role_or_permission:Superint');

Route::get('/manpower-logs/{id}', [

    'uses' => 'Superint\ManpowerController@logs',

    'as' => 'manpower.logs'

])->middleware('role_or_permission:Superint');

Route::get('/manpower-logs-d', [

    'uses' => 'Superint\ManpowerController@logsd',

    'as' => 'manpower.dl'

])->middleware('role_or_permission:Superint');



Route::group([ 'prefix' => 'projects' ], function() {

        Route::get('/', [

            'uses' => 'Superint\ProjectController@index',

            'as' => 'superint.projects'

        ])->middleware('role_or_permission:Superint');

        Route::get('/get', [

            'uses' => 'Superint\ProjectController@getProjects',

            'as' => 'superint.datatable.project.get'

        ])->middleware('role_or_permission:Superint');

        Route::get('/create', [

            'uses' => 'Superint\ProjectController@create',

            'as' => 'superint.projects.create'

        ])->middleware('role_or_permission:Superint');

        Route::post('/create', [

            'uses' => 'Superint\ProjectController@store'

        ])->middleware('role_or_permission:Superint');

        Route::get('/delete/{id}', [

            'uses' => 'Superint\ProjectController@destroy',

            'as' => 'superint.project.delete'

        ])->middleware('role_or_permission:Superint');

        Route::get('/edit/{id}', [

            'uses' => 'Superint\ProjectController@edit',

            'as' => 'superint.project.edit'

        ])->middleware('role_or_permission:Superint');

        Route::post('/edit/{id}', [

            'uses' => 'Superint\ProjectController@update'

        ])->middleware('role_or_permission:Superint');

        Route::get('/view/{id}', [

            'uses' => 'Superint\ProjectController@show',

            'as' => 'superint.project.view'

        ])->middleware('role_or_permission:Superint');

      
    });

/*-------------------------------------------- Logs --------------------------------------------*/
    

/*-------------------------------------------- Logs --------------------------------------------*/

/*-------------------------------------------- Workers --------------------------------------------*/



/*-------------------------------------------- Workers --------------------------------------------*/

/*-------------------------------------------- Projects --------------------------------------------*/
    


/*-------------------------------------------- Projects --------------------------------------------*/

/*-------------------------------------------- Payrolls --------------------------------------------*/
    


/*-------------------------------------------- Payrolls --------------------------------------------*/

});



