<?php

/*
|--------------------------------------------------------------------------
| Subcontractor Routes
|--------------------------------------------------------------------------
|
|
*/

Route::group([ 'prefix' => 'subcontractor', 'middleware' => [ 'auth', 'superadmin', 'prevent-back-history' ] ], function() {

Route::get('/dashboard', [

    'uses' => 'Subcontractor\DashboardController@index',

    'as' => 'subcontractor.dashboard'

])->middleware('role_or_permission:Subcontractor');

Route::get('/documents', [

    'uses' => 'Subcontractor\UserController@showDocuments',

    'as' => 'subcontractor.documents'

]);

/*-------------------------------------------- Logs --------------------------------------------*/
    
Route::group([ 'prefix' => 'logs' ], function() {

    Route::get('/get', [

        'uses' => 'Subcontractor\LogController@getUserLogs',

        'as' => 'subcontractor.datatable.logs.get'

    ])->middleware('role_or_permission:Subcontractor');

});

Route::group([ 'prefix' => 'subcontractor/logs' ], function() {

    Route::get('/project/get', [

        'uses' => 'Subcontractor\LogController@getProjectLogs',

        'as' => 'subcontractor.datatable.projects.logs.get'

    ])->middleware('role_or_permission:Subcontractor');

});

/*-------------------------------------------- Logs --------------------------------------------*/

/*-------------------------------------------- Workers --------------------------------------------*/

Route::group([ 'prefix' => 'workers' ], function() {

    Route::get('/', [

        'uses' => 'Subcontractor\UserController@index',

        'as' => 'subcontractor.workers'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/get', [

        'uses' => 'Subcontractor\UserController@getUsers',

        'as' => 'subcontractor.datatable.workers.get'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/delete/{id}', [

        'uses' => 'Subcontractor\UserController@destroy',

        'as' => 'subcontractor.workers.delete'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/view/{id?}', [

        'uses' => 'Subcontractor\UserController@show',

        'as' => 'subcontractor.workers.view'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/edit/{id?}', [

        'uses' => 'Subcontractor\UserController@edit',

        'as' => 'subcontractor.workers.edit'

    ])->middleware('role_or_permission:Subcontractor');

    Route::post('/update/{id}', [

            'uses' => 'Subcontractor\UserController@update',

            'as' => 'subcontractor.workers.update'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/create', [

        'uses' => 'Subcontractor\UserController@create',

        'as' => 'subcontractor.workers.create'

    ])->middleware('role_or_permission:Subcontractor');

    Route::post('/create', [

        'uses' => 'Subcontractor\UserController@store'

    ])->middleware('role_or_permission:Subcontractor');

});

/*-------------------------------------------- Workers --------------------------------------------*/

/*-------------------------------------------- Projects --------------------------------------------*/
    
Route::group([ 'prefix' => 'projects' ], function() {

    Route::get('/', [

        'uses' => 'Subcontractor\ProjectController@index',

        'as' => 'subcontractor.projects'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/fav/list', [

        'uses' => 'Subcontractor\ProjectController@favindex',

        'as' => 'subcontractor.projects.get.fav'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/get', [

        'uses' => 'Subcontractor\ProjectController@getProjects',

        'as' => 'subcontractor.datatable.project.get'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/get/datatable/fav/list', [

        'uses' => 'Subcontractor\ProjectController@getProjectsFav',

        'as' => 'subcontractor.datatable.project.get.fav'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/create', [

        'uses' => 'Subcontractor\ProjectController@create',

        'as' => 'subcontractor.projects.create'

    ])->middleware('role_or_permission:Subcontractor');

    Route::post('/create', [

        'uses' => 'Subcontractor\ProjectController@store'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/delete/{id}', [

        'uses' => 'Subcontractor\ProjectController@destroy',

        'as' => 'subcontractor.project.delete'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/edit/{id}', [

        'uses' => 'Subcontractor\ProjectController@edit',

        'as' => 'subcontractor.project.edit'

    ])->middleware('role_or_permission:Subcontractor');

    Route::post('/edit/{id}', [

        'uses' => 'Subcontractor\ProjectController@update'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/view/{id}', [

        'uses' => 'Subcontractor\ProjectController@show',

        'as' => 'subcontractor.project.view'

    ])->middleware('role_or_permission:Subcontractor');

});

/*-------------------------------------------- Projects --------------------------------------------*/

/*-------------------------------------------- Payrolls --------------------------------------------*/
    
Route::group([ 'prefix' => 'payrolls' ], function() {

    Route::get('/', [

        'uses' => 'Subcontractor\PayrollsController@index',

        'as' => 'subcontractor.payrolls'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/download/file/{file_source}/{author}', [

        'uses' => 'Subcontractor\PayrollsController@downloadFiles',

        'as' => 'common.datatable.payroll.download.file'

    ])->middleware('role_or_permission:Subcontractor|Superadmin');

    Route::get('/get', [

        'uses' => 'Subcontractor\PayrollsController@getPayrolls',

        'as' => 'subcontractor.datatable.payroll.get'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/create', [

        'uses' => 'Subcontractor\PayrollsController@create',

        'as' => 'subcontractor.payroll.create'

    ])->middleware('role_or_permission:Subcontractor');

    Route::post('/create', [

        'uses' => 'Subcontractor\PayrollsController@store'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/delete/{id}', [

        'uses' => 'Subcontractor\PayrollsController@destroy',

        'as' => 'subcontractor.payroll.delete'

    ])->middleware('role_or_permission:Subcontractor');

    /*Route::get('/edit/{id}', [

        'uses' => 'Subcontractor\PayrollsController@edit',

        'as' => 'subcontractor.payroll.edit'

    ])->middleware('role_or_permission:Subcontractor');*/

    Route::post('/edit/{id}', [

        'uses' => 'Subcontractor\PayrollsController@update'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/view/{id}', [

        'uses' => 'Subcontractor\PayrollsController@show',

        'as' => 'subcontractor.payroll.view'

    ])->middleware('role_or_permission:Subcontractor');

});

Route::group([ 'prefix' => 'manpower/requests' ], function() {

    Route::get('/', [

        'uses' => 'Subcontractor\ManpowerController@index',

        'as' => 'subcontractor.manpower.requests'

    ])->middleware('role_or_permission:Subcontractor');

    Route::get('/manpower/add', [

    'uses' => 'Superint\ManpowerController@create',

    'as' => 'manpower.create'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/delete/{id}', [

    'uses' => 'Superint\ManpowerController@destroy',

    'as' => 'manpower.delete'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/get', [

    'uses' => 'Subcontractor\ManpowerController@get',

    'as' => 'subcontractor.manpower.get'

])->middleware('role_or_permission:Subcontractor|Superadmin');

Route::get('/manpower/get/{id?}', [

    'uses' => 'Subcontractor\ManpowerController@getp',

    'as' => 'subcontractor.manpower.get.p'

])->middleware('role_or_permission:Subcontractor|Superadmin');

Route::post('/manpower/store', [

    'uses' => 'Superint\ManpowerController@store',

    'as' => 'manpower.save'

])->middleware('role_or_permission:Superint');

Route::post('/manpower/update', [

    'uses' => 'Superint\ManpowerController@update',

    'as' => 'manpower.update'

])->middleware('role_or_permission:Superint');

Route::post('/manpower/copy_previous', [

    'uses' => 'Superint\ManpowerController@copyPrevious',

    'as' => 'manpower.copy'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/edit/{id}', [

    'uses' => 'Superint\ManpowerController@edit',

    'as' => 'manpower.edit'

])->middleware('role_or_permission:Superint');

Route::get('/manpower/copy/edit/{id}', [

    'uses' => 'Superint\ManpowerController@copyEdit',

    'as' => 'manpower.copy.edit'

])->middleware('role_or_permission:Superint');

Route::get('/manpower-logs/{id}', [

    'uses' => 'Subcontractor\ManpowerController@logs',

    'as' => 'subcontractor.manpower.logs'

])->middleware('role_or_permission:Subcontractor');
Route::get('/manpower-view/{id}', [

    'uses' => 'Subcontractor\ManpowerController@logs',

    'as' => 'subcontractor.manpower.view'

])->middleware('role_or_permission:Subcontractor|Superadmin');

Route::get('/manpower-view-d', [

    'uses' => 'Subcontractor\ManpowerController@logsd',

    'as' => 'subcontractor.manpower.view.d'

])->middleware('role_or_permission:Subcontractor|Superadmin');

});



/*-------------------------------------------- Payrolls --------------------------------------------*/

});



