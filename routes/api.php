<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
[

'prefix' => 'v1',
'namespace' => 'Api',
'middleware' =>'localization' // set locale lang

],
function () {

    Route::post('user/forgot_password', 'UserController@forgotPassword');
   
    Route::post('user/login', 'UserController@login');

    Route::post('user/register', 'UserController@register');

    Route::get('companies/get', 'CompanyController@getCompanies');

    Route::get('user/check', 'UserController@checkUser');

    Route::get('user/get/timezone', 'UserController@getTimezone');

    // Cron monitor
    Route::get('cron/monitor', 'CronMonitorController@cronMonitor');

    Route::get('test/notification', 'TestController@testNotification');

    Route::get('cron/punch_notification', 'PunchNotificationCron@sendTimeOutReminder');

    Route::get('workers/type/get', 'WorkerController@getWorkers');


    Route::get('workers/request/count', 'WorkerController@getWorkersRequestCount');

    /* Safety Strikes */

    Route::post('safety/strike/create', 'SafetyController@create');
    Route::post('safety/strike/update', 'SafetyController@update');
    Route::post('safety/strike/delete', 'SafetyController@destroy');
    Route::get('safety/strike/list', 'SafetyController@list');
    Route::get('safety/strike/worker/list', 'SafetyController@workersList');
    Route::get('safety/strike/category/list', 'SafetyController@getCategories');

    /* Safety Strikes */

    Route::get('/getschedule', 'PunchController@getSchedule');
    
    Route::get('/getscheduleforadmin', 'PunchController@getscheduleforadmin');

    Route::group(['middleware' => 'auth:api'], function(){

        Route::group([ 'prefix' => 'worker' ], function() {
            Route::get('/', 'WorkerController@filterWorkers');
            Route::get('/pending', 'WorkerController@pendingApproval');
            Route::post('/update', 'WorkerController@update');
        });

        Route::get('user/announcements', 'WorkerController@getAnnce');

        Route::group([ 'prefix' => 'user' ], function() {
                Route::get('/profile', 'ProfileController@index');
                Route::get('/update/language', 'ProfileController@updateLanguage');
                Route::post('/profile', 'ProfileController@update');
                Route::post('/change-password', 'UserController@changePassword');
        });
        Route::group([ 'prefix' => 'project' ], function() {
            // Calling Product controller from Api directory
            Route::get('/', 'ProjectController@index');
            Route::post('/qrcode/get', 'ProjectController@getQRcode');


            
            Route::get('/logs/{id}', 'WorkerController@logDates');
            //project workers sections
            Route::get('/worker/{id}', 'WorkerController@index');

            Route::post('/favourite', 'FavouriteController@create');
            Route::get('/favourite', 'FavouriteController@index');

            Route::get('/filter', 'ProjectApiController@projectFilter');
            Route::get('/safety-filter', 'ProjectApiController@safetyProjectFilter');
            Route::get('/punch', 'PunchController@index');
            Route::post('/punch_offline', 'PunchController@store');
            Route::post('/update_out_in', 'PunchController@updateWorkerStatus');
            Route::post('/punch_out', 'PunchController@update');
            Route::post('/punch_in', 'PunchController@create');
            Route::get('/punch/status', 'PunchController@status');
            Route::delete('/punch/{id}', 'PunchController@destroy');

            

        });



    });

});
