<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Get SSN ID with more than 12 digits

Route::any('/get-user-export', 'Admin\UserController@downloadExportWorkers');


Route::get('/checkSSN', 'Admin\UserController@checkSSN');

Route::get('/checkTimeGap', 'Admin\UserController@checkTimeGap');

//Privacy page

Route::get('/privacy-policy', 'Frontend\PageController@privacy');

//Contact page

Route::get('/contact-us', 'Frontend\PageController@contactUs');

Route::get('/duplicate_logs', 'Admin\CronController@checkDuplicateLogs');

Route::get('/send/report/notification/cron', 'Admin\ReportNotificationCron@sendReportNotificationCron');
//Route::get('/send/manpower/notification/cron', 'Admin\ManpowerCron@sendManpowerDayBefore');

Route::get('/send/manpower/notification/cron', 'Admin\ManpowerCron@sendManpowerDayBefore')->name('schedule.emails');

/* Log Reports */

Route::get('/log/report/show', 'Admin\LogreportController@logReports')->name('admin.logreports');
Route::get('/log/report/get', 'Admin\LogreportController@getReport')->name('admin.report.get');
Route::get('/log/report/job/get', 'Admin\LogreportController@getJobReport')->name('admin.report.job.get');
Route::post('/log/report/generate', 'Admin\LogreportController@generate')->name('admin.report.generate');
Route::get('/admin/strike/delete/{id}', 'Admin\LogreportController@strikeDelete')->name('admin.strike.delete');
Route::get('/admin/strike/edit/{id}/{worker_id}', 'Admin\LogreportController@edit')->name('admin.strike.edit');
Route::post('/admin/strike/update/{id}', 'Admin\LogreportController@update')->name('admin.strike.update');
Route::get('/admin/strike/export/{id}', 'Admin\LogreportController@exportStrike')->name('strike.export');

/* Log Reports */

Route::get('/send/manpower/notification/cron/send', 'Admin\ManpowerCron@sendPreviewEmails')->name('schedule.preview.emails.send');

Route::get('/send/manpower/notification/mass/cron', 'Admin\ManpowerCron@sendManpowerDayBeforeToAll')->name('schedule.emails.all');
Route::get('/send/manpower/email/evening', 'Admin\ManpowerCron@sendEmailEvening')->name('schedule.emails.evening');
Route::get('/send/manpower/email/evening/show-preview', 'Admin\ManpowerCron@sendEmailEveningShowPreview')->name('schedule.emails.evening.show.preview');

Route::get('/get/manpower/email/preview', 'Admin\ManpowerCron@generatePreview')->name('get.the.email.preview');

Route::get('/get/merger/logs', 'Admin\AnnouncementController@getMergerLogs')->name('get.merger.logs');

Route::get('/download/export/workers', 'Admin\UserController@downloadExportWorkers')->name('download.export.workers');
Route::get('/script/update_log_sub', 'Admin\WorkerController@createParentOfLogs')->name('script.update.log.sub');
Route::get('/schedule/clear/{id}/{d}/{pr}', 'Superint\ManpowerController@clear')->name('schedule.clear');

Route::get('/', function () {

    return redirect( route('login') );

})->name('home');

Route::get('/login_app', [

    'uses' => 'AuthenticationController@loginApp',

    'as' => 'login_app'

]);

Route::get('/logout', [

    'uses' => 'AuthenticationController@logout',

    'as' => 'logout'

]);



Route::group([ 'middleware' => [ 'guest' ] ], function() {

    Route::group([ 'prefix' => 'register' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@register',

            'as' => 'register'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@registeration',

        ]);

    });

    Route::group([ 'prefix' => 'verify' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@verify',

            'as' => 'verify'

        ]);

        Route::get('/resend', [

            'uses' => 'AuthenticationController@resend_verify_email',

            'as' => 'verify.email.resend'

        ]);

    });

    Route::group([ 'prefix' => 'login' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@login',

            'as' => 'login'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@authenticate',

        ]);

    });

    Route::group([ 'prefix' => 'social-authentication' ], function() {

        Route::get('/{provider}', [

            'uses' => 'AuthenticationController@social_authentication',
        
            'as' => 'social.authentication'
        
        ]);
        
        Route::get('/{provider}/callback', [
        
            'uses' => 'AuthenticationController@social_authentication_callback',
        
            'as' => 'social.authentication.callback'
        
        ]);

    });

    Route::group([ 'prefix' => 'forgot-password' ], function(){

        Route::get('/', [

            'uses' => 'AuthenticationController@forgot_password',

            'as' => 'forgot.password'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@forgot_password_mail'

        ]);

    });

    Route::group([ 'prefix' => 'reset-password' ], function() {

        Route::get('/', [

            'uses' => 'AuthenticationController@reset_password',

            'as' => 'reset.password'

        ]);

        Route::post('/', [

            'uses' => 'AuthenticationController@reset_password_mail',

        ]);

    });

});

Route::group([ 'middleware' => [ 'auth', 'superadmin', 'prevent-back-history' ] ], function() {

Route::get('/resources', 'Frontend\ResourceController@resource')->name('f.resources');
Route::get('/resources/view/{id}', [

            'uses' => 'Frontend\ResourceController@view',

            'as' => 'f.resource.view'

        ]);


});



Route::group([ 'prefix' => 'superadmin', 'middleware' => [ 'auth', 'superadmin', 'prevent-back-history' ] ], function() {

    Route::get('/resources', 'Frontend\PageController@resource');
/*Route::get('/resources/view/{id}', [

            'uses' => 'Frontend\ResourceController@view',

            'as' => 'f.resource.view'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll|Superint|Subcontractor');*/

    Route::get('/dashboard', [

        'uses' => 'Admin\DashboardController@index',

        'as' => 'admin.dashboard'

    ])->middleware('role_or_permission:Superadmin|Admin|Payroll|Superint|Subcontractor');

    Route::get('/subcontractor/document/delete/{id}', [

        'uses' => 'Admin\UserController@deleteDocument',

        'as' => 'admin.subcontracto.document.delete'

    ]);
    

    Route::get('/worker/document/change/status', [

        'uses' => 'Admin\UserController@changeStatusWorker',

        'as' => 'worker.document.status'

    ]);

    

    Route::get('/worker/get/order/details', [

        'uses' => 'Admin\UserController@getWorkerOrderDetails',

        'as' => 'worker.get.order.details'

    ]);

    Route::get('/worker/document/delete/{id}', [

        'uses' => 'Admin\UserController@deleteWorkerDocument',

        'as' => 'admin.worker.document.delete'

    ]);

    // Log Merger


    Route::group([ 'prefix' => 'log-merger' ], function() {

        Route::get('/', [

            'uses' => 'Admin\AnnouncementController@logMerger',

            'as' => 'admin.log.merger'

        ]);

        Route::post('/save', [

            'uses' => 'Admin\AnnouncementController@logMergerSave',

            'as' => 'admin.log.merger.save'

        ]);

    });

    // Announcement

    Route::group([ 'prefix' => 'announcement' ], function() {

        Route::get('/create', [

            'uses' => 'Admin\AnnouncementController@index',

            'as' => 'admin.announcement.create'

        ]);

        Route::post('/save', [

            'uses' => 'Admin\AnnouncementController@save',

            'as' => 'admin.announcement.save'

        ]);

    });

    // Announcement

    Route::group([ 'prefix' => 'resource-content' ], function() {

        Route::get('/create', [

            'uses' => 'Admin\ResourceController@contentIndex',

            'as' => 'admin.resource.content.create'

        ]);

        Route::post('/save', [

            'uses' => 'Admin\ResourceController@contentSave',

            'as' => 'admin.resource.content.save'

        ]);

        Route::get('/save/{resource_id}/{file}', [

            'uses' => 'Admin\ResourceController@fileDelete',

            'as' => 'resource.delete'

        ]);

    });

    // Pages

    Route::group([ 'prefix' => 'pages' ], function() {

        Route::get('/billboard', [

            'uses' => 'Admin\PagesController@index',

            'as' => 'admin.pages.billboard'

        ]);

        Route::post('/add_update', [

            'uses' => 'Admin\PagesController@update',

            'as' => 'admin.pages.billboard.update'

        ])->middleware('role_or_permission:Superadmin|Admin');


    });    

    Route::group([ 'prefix' => 'profile' ], function() {

        Route::get('/', [

            'uses' => 'Admin\ProfileController@index',

            'as' => 'admin.profile'

        ]);

        Route::post('/', [

            'uses' => 'Admin\ProfileController@update',

        ]);

        Route::post('/image', [

            'uses' => 'Admin\ProfileController@image',

            'as' => 'admin.profile.image'

        ]);

        Route::post('/password', [

            'uses' => 'Admin\ProfileController@password',

            'as' => 'admin.profile.password'

        ]);

        Route::post('/user/password', [

            'uses' => 'Admin\ProfileController@userPasswordChange',

            'as' => 'admin.user.password.change'

        ]);

    });

    Route::group([ 'prefix' => 'workers' ], function() {

        Route::get('/request', [

            'uses' => 'Admin\UserController@workerRequests',

            'as' => 'admin.workers.requests'

        ])->middleware('role_or_permission:Superadmin|Admin');

    });

    /* Worker types */

    Route::group([ 'prefix' => 'workers/types' ], function() {

        Route::get('/', [

            'uses' => 'Admin\WorkerController@index',

            'as' => 'admin.workers.types'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/get', [

            'uses' => 'Admin\WorkerController@getData',

            'as' => 'admin.datatable.workers.types.get'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/update', [

            'uses' => 'Admin\WorkerController@update',

            'as' => 'admin.worker.type.update'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/add', [

            'uses' => 'Admin\WorkerController@add',

            'as' => 'admin.worker.type.add'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\WorkerController@destroy',

            'as' => 'admin.worker.type.delete'

        ])->middleware('role_or_permission:Superadmin|Admin');

    });

    /* Worker types */

    /* Work types */

    Route::group([ 'prefix' => 'work/types' ], function() {

        Route::get('/', [

            'uses' => 'Admin\WorkController@index',

            'as' => 'admin.work.types'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/get', [

            'uses' => 'Admin\WorkController@getData',

            'as' => 'admin.datatable.work.types.get'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/update', [

            'uses' => 'Admin\WorkController@update',

            'as' => 'admin.work.type.update'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/add', [

            'uses' => 'Admin\WorkController@add',

            'as' => 'admin.work.type.add'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\WorkController@destroy',

            'as' => 'admin.work.type.delete'

        ])->middleware('role_or_permission:Superadmin|Admin');

    });

    /* Work types */

    Route::group([ 'prefix' => 'workers' ], function() {

        Route::get('/export/{subid?}', [

            'uses' => 'Admin\UserController@workerExport',

            'as' => 'admin.workers.export'

        ])->middleware('role_or_permission:Superadmin|Admin|Subcontractor');

    });

    Route::group([ 'prefix' => 'project' ], function() {

        Route::get('/logs/export/{projectid?}', [

            'uses' => 'Admin\LogController@workerProjectLogs',

            'as' => 'admin.project.logs.export'

        ])->middleware('role_or_permission:Superadmin|Admin|Subcontractor');


        Route::post('/logs/export/{projectid?}', [

            'uses' => 'Admin\LogController@exportProjectLogs',

            'as' => 'admin.project.logs.all.export'

        ])->middleware('role_or_permission:Superadmin|Admin|Subcontractor');

        Route::post('/log/status/update', [

            'uses' => 'Admin\LogController@projectLogStatusUpdate',

            'as' => 'admin.project.log.status.update'

        ])->middleware('role_or_permission:Superadmin|Admin');

    });

    Route::group([ 'prefix' => 'settings' ], function() {

        Route::get('/', [

            'uses' => 'Admin\SettingController@index',

            'as' => 'admin.settings'

        ]);

        Route::post('/', [

            'uses' => 'Admin\SettingController@update'

        ]);

    });


    /* Resources */

    Route::group([ 'prefix' => 'resources' ], function() {

        Route::get('/', [

            'uses' => 'Admin\ResourceController@index',

            'as' => 'admin.resource'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/get', [

            'uses' => 'Admin\ResourceController@getResources',

            'as' => 'admin.datatable.resource.get'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/create', [

            'uses' => 'Admin\ResourceController@create',

            'as' => 'admin.resource.create'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/save', [

            'uses' => 'Admin\ResourceController@store',

            'as' => 'admin.resource.save'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/edit/{id}', [

            'uses' => 'Admin\ResourceController@edit',

            'as' => 'admin.resource.edit'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/update', [

            'uses' => 'Admin\ResourceController@update',

            'as' => 'admin.resource.update'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/remove/{id}', [

            'uses' => 'Admin\ResourceController@destroy',

            'as' => 'admin.resource.remove'

        ])->middleware('role_or_permission:Superadmin|Admin');

        /*Route::post('/create', [

            'uses' => 'Admin\UserController@store'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/edit/{id}', [

            'uses' => 'Admin\UserController@edit',

            'as' => 'admin.users.edit'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/remove/{id}', [

            'uses' => 'Admin\UserController@remove',

            'as' => 'admin.users.remove'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/edit/{id?}', [

            'uses' => 'Admin\UserController@update'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\UserController@destroy',

            'as' => 'admin.users.delete'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/approve/{id}', [

            'uses' => 'Admin\UserController@approve',

            'as' => 'admin.users.approve'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/reject/{id}', [

            'uses' => 'Admin\UserController@reject',

            'as' => 'admin.users.reject'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/view/{id?}', [

            'uses' => 'Admin\UserController@show',

            'as' => 'admin.users.view'

        ])->middleware('role_or_permission:Superadmin|Admin');*/

    });

    /* Resources */


    Route::group([ 'prefix' => 'users' ], function() {

        Route::get('/', [

            'uses' => 'Admin\UserController@index',

            'as' => 'admin.users'

        ])->middleware('role_or_permission:Superadmin|Admin');


        Route::get('/get', [

            'uses' => 'Admin\UserController@getUsers',

            'as' => 'admin.datatable.users.get'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/get/requests', [

            'uses' => 'Admin\UserController@getRequests',

            'as' => 'admin.datatable.users.get.requests'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/create', [

            'uses' => 'Admin\UserController@create',

            'as' => 'admin.users.create'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/create', [

            'uses' => 'Admin\UserController@store'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/edit/{id}', [

            'uses' => 'Admin\UserController@edit',

            'as' => 'admin.users.edit'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/remove/{id}', [

            'uses' => 'Admin\UserController@remove',

            'as' => 'admin.users.remove'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/edit/{id?}', [

            'uses' => 'Admin\UserController@update'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\UserController@destroy',

            'as' => 'admin.users.delete'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/approve/{id}', [

            'uses' => 'Admin\UserController@approve',

            'as' => 'admin.users.approve'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/reject/{id}', [

            'uses' => 'Admin\UserController@reject',

            'as' => 'admin.users.reject'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/view/{id?}', [

            'uses' => 'Admin\UserController@show',

            'as' => 'admin.users.view'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::any('/update_status/', [

            'uses' => 'Admin\UserController@updateStatus',

            'as' => 'admin.users.update_status'

        ])->middleware('role_or_permission:Superadmin|Admin');

    });

    Route::group([ 'prefix' => 'projects' ], function() {

        Route::get('/', [

            'uses' => 'Admin\ProjectController@index',

            'as' => 'admin.projects'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/fav/list', [

            'uses' => 'Admin\ProjectController@favindex',

            'as' => 'admin.projects.get.fav'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/project/{qrcode}', [

            'uses' => 'Admin\ProjectController@printQR',

            'as' => 'admin.project.print.qr'

        ])->middleware('role_or_permission:Superadmin|Admin|Subcontractor');

        Route::get('/get', [

            'uses' => 'Admin\ProjectController@getProjects',

            'as' => 'admin.datatable.project.get'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/get/list/fav/datatable', [

            'uses' => 'Admin\ProjectController@getFavProjects',

            'as' => 'admin.datatable.project.get.fav'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/create', [

            'uses' => 'Admin\ProjectController@create',

            'as' => 'admin.projects.create'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/create', [

            'uses' => 'Admin\ProjectController@store'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\ProjectController@destroy',

            'as' => 'admin.project.delete'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/edit/{id}', [

            'uses' => 'Admin\ProjectController@edit',

            'as' => 'admin.project.edit'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/edit/{id}', [

            'uses' => 'Admin\ProjectController@update'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/view/{id}', [

            'uses' => 'Admin\ProjectController@show',

            'as' => 'admin.project.view'

        ])->middleware('role_or_permission:Superadmin|Admin');

        /*

        Route::get('/remove/{id}', [

            'uses' => 'Admin\UserController@remove',

            'as' => 'admin.users.remove'

        ])->middleware('role_or_permission:Superadmin|Admin');

       */

    });

    Route::group([ 'prefix' => 'logs' ], function() {


        Route::get('/get', [

            'uses' => 'Admin\LogController@getUserLogs',

            'as' => 'admin.datatable.logs.get'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/get/project/logs', [

            'uses' => 'Admin\LogController@getProjectLogs',

            'as' => 'admin.datatable.project.logs.get'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/create/{userId}', [

            'uses' => 'Admin\LogController@create',

            'as' => 'admin.log.create'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/save', [

            'uses' => 'Admin\LogController@save',

            'as' => 'admin.log.save'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\LogController@destroy',

            'as' => 'admin.log.delete'

        ])->middleware('role_or_permission:Superadmin|Admin');

        Route::post('/update', [

            'uses' => 'Admin\LogController@update',

            'as' => 'admin.log.update'

        ])->middleware('role_or_permission:Superadmin|Admin');

        
    });

    Route::group([ 'prefix' => 'trainers' ], function() {

        Route::get('/', [

            'uses' => 'Admin\TrainerController@index',

            'as' => 'admin.trainers'

        ])->middleware('role_or_permission:Superadmin|Admin|read trainers|write trainers');

        Route::get('/create', [

            'uses' => 'Admin\TrainerController@create',

            'as' => 'admin.trainers.create'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

        Route::post('/create', [

            'uses' => 'Admin\TrainerController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

        Route::get('/edit/{id}', [

            'uses' => 'Admin\TrainerController@edit',

            'as' => 'admin.trainers.edit'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

        Route::get('/remove/{id}', [

            'uses' => 'Admin\TrainerController@remove',

            'as' => 'admin.trainers.remove'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

        Route::post('/edit/{id}', [

            'uses' => 'Admin\TrainerController@update'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\TrainerController@destroy',

            'as' => 'admin.trainers.delete'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

        Route::get('/view/{id}', [

            'uses' => 'Admin\TrainerController@show',

            'as' => 'admin.trainers.view'

        ])->middleware('role_or_permission:Superadmin|Admin|read trainers|write trainers');

        Route::get('/availability/{id}', [

            'uses' => 'Admin\TrainerController@availability',

            'as' => 'admin.trainers.availability'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

        Route::post('/availability/{id}', [

            'uses' => 'Admin\TrainerController@update_availability'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainers');

    });

    Route::group([ 'prefix' => 'trainer-requests' ], function() {

        Route::get('/', [

            'uses' => 'Admin\TrainerController@requests',

            'as' => 'admin.trainer-requests'

        ])->middleware('role_or_permission:Superadmin|Admin|read trainer-requests|write trainer-requests');

        Route::get('/{id}/{action}', [

            'uses' => 'Admin\TrainerController@request_action',

            'as' => 'admin.trainer-requests.action'

        ])->middleware('role_or_permission:Superadmin|Admin|read trainer-requests|write trainer-requests');

    });

    Route::group([ 'prefix' => 'trainer-categories' ], function() {

        Route::get('/', [

            'uses' => 'Admin\TrainerCategoryController@index',

            'as' => 'admin.trainer-categories'

        ])->middleware('role_or_permission:Superadmin|Admin|read trainer-categories|write trainer-categories');

        Route::post('/', [

            'uses' => 'Admin\TrainerCategoryController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write trainer-categories');

        Route::group([ 'prefix' => '{id}', 'middleware' => 'role_or_permission:Superadmin|Admin|write trainer-categories' ], function() {

            Route::get('/edit', [

                'uses' => 'Admin\TrainerCategoryController@edit',
    
                'as' => 'admin.trainer-categories.edit'
    
            ]);

            Route::post('/edit', [

                'uses' => 'Admin\TrainerCategoryController@update'
    
            ]);

            Route::get('/delete', [

                'uses' => 'Admin\TrainerCategoryController@destroy',
    
                'as' => 'admin.trainer-categories.delete'
    
            ]);

        });

    });

    Route::group([ 'prefix' => 'surveys' ], function() {

        Route::get('/', [

            'uses' => 'Admin\SurveyController@index',

            'as' => 'admin.surveys'

        ])->middleware('role_or_permission:Superadmin|Admin|read surveys|write surveys');

        Route::get('/create', [

            'uses' => 'Admin\SurveyController@create',

            'as' => 'admin.surveys.create'

        ])->middleware('role_or_permission:Superadmin|Admin|write surveys');

        Route::post('/create', [

            'uses' => 'Admin\SurveyController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write surveys');

        Route::get('/edit/{id}', [

            'uses' => 'Admin\SurveyController@edit',

            'as' => 'admin.surveys.edit'

        ])->middleware('role_or_permission:Superadmin|Admin|write surveys');

        Route::post('/edit/{id}', [

            'uses' => 'Admin\SurveyController@update'

        ])->middleware('role_or_permission:Superadmin|Admin|write surveys');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\SurveyController@destroy',

            'as' => 'admin.surveys.delete'

        ])->middleware('role_or_permission:Superadmin|Admin|write surveys');

        Route::get('/view/{id}', [

            'uses' => 'Admin\SurveyController@show',

            'as' => 'admin.surveys.view'

        ])->middleware('role_or_permission:Superadmin|Admin|read surveys|write surveys');

    });

    Route::group([ 'prefix' => 'staff' ], function() {

        Route::group([ 'prefix' => 'permissions' ], function() {

            Route::get('/{role?}', [

                'uses' => 'Admin\PermissionController@index',
    
                'as' => 'admin.staff.permissions'
    
            ])->middleware('role_or_permission:Superadmin|Admin|read permissions|write permissions');

            Route::get('/{role}/{section}/{permission}', [

                'uses' => 'Admin\PermissionController@permissions',
    
                'as' => 'admin.staff.permissions.action'
                
            ])->middleware('role_or_permission:Superadmin|Admin|write permissions');

        });

        Route::group([ 'prefix' => 'roles' ], function() {
            
            Route::get('/', [

                'uses' => 'Admin\RoleController@index',
    
                'as' => 'admin.staff.roles'
    
            ])->middleware('role_or_permissioadmin.users.createn:Superadmin|read roles|write roles');

            Route::post('/', [

                'uses' => 'Admin\RoleController@store'
    
            ])->middleware('role_or_permission:Superadmin|Admin|write roles');

            Route::group([ 'prefix' => '{id}', 'middleware' => 'role_or_permission:Superadmin|Admin|write roles' ], function() {

                Route::get('/edit', [

                    'uses' => 'Admin\RoleController@edit',
        
                    'as' => 'admin.staff.roles.edit'
        
                ]);

                Route::post('/edit', [

                    'uses' => 'Admin\RoleController@update'
        
                ]);

                Route::get('/delete', [

                    'uses' => 'Admin\RoleController@destroy',
        
                    'as' => 'admin.staff.roles.delete'
        
                ]);

            });

        });

        Route::group([ 'prefix' => 'members' ], function () {

            Route::get('/', [

                'uses' => 'Admin\MemberController@index',

                'as' => 'admin.staff.members'

            ])->middleware('role_or_permission:Superadmin|Admin|read members|write members');

            Route::get('/create', [

                'uses' => 'Admin\MemberController@create',

                'as' => 'admin.staff.members.cadmin.users.createreate'

            ])->middleware('role_or_permission:Superadmin|Admin|write members');

            Route::post('/create', [

                'uses' => 'Admin\MemberController@store'

            ])->middleware('role_or_permission:Superadmin|Admin|write members');

            Route::get('/edit/{id}', [

                'uses' => 'Admin\MemberController@edit',

                'as' => 'admin.staff.members.edit'

            ])->middleware('role_or_permission:Superadmin|Admin|write members');

            Route::post('/edit/{id}', [

                'uses' => 'Admin\MemberController@update'

            ])->middleware('role_or_permission:Superadmin|Admin|write members');


            Route::get('/remove/{id}', [

                'uses' => 'Admin\MemberController@remove',
    
                'as' => 'admin.staff.members.remove'
    
            ])->middleware('role_or_permission:Superadmin|Admin|write members');

            Route::get('/delete/{id}', [

                'uses' => 'Admin\MemberController@destroy',
    
                'as' => 'admin.staff.members.delete'
    
            ])->middleware('role_or_permission:Superadmin|Admin|write members');

        });

    });

    Route::group([ 'prefix' => 'session-categories' ], function() {

        Route::get('/', [

            'uses' => 'Admin\SessionCategoryController@index',

            'as' => 'admin.session-categories'

        ])->middleware('role_or_permission:Superadmin|Admin|read session-categories|write session-categories');

        Route::post('/', [

            'uses' => 'Admin\SessionCategoryController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write session-categories');

        Route::group([ 'prefix' => '{id}', 'middleware' => 'role_or_permission:Superadmin|Admin|write session-categories' ], function() {

            Route::get('/edit', [

                'uses' => 'Admin\SessionCategoryController@edit',
    
                'as' => 'admin.session-categories.edit'
    
            ]);

            Route::post('/edit', [

                'uses' => 'Admin\SessionCategoryController@update'
    
            ]);

            Route::get('/delete', [

                'uses' => 'Admin\SessionCategoryController@destroy',
    
                'as' => 'admin.session-categories.delete'
    
            ]);

        });

    });

    Route::group([ 'prefix' => 'sessions' ], function() {

        Route::get('/', [

            'uses' => 'Admin\SessionController@index',

            'as' => 'admin.sessions'

        ])->middleware('role_or_permission:Superadmin|Admin|read sessions|write sessions');

        Route::get('/create', [

            'uses' => 'Admin\SessionController@create',

            'as' => 'admin.sessions.create'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::post('/create', [

            'uses' => 'Admin\SessionControlleradmin.users.create@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::get('/view/{id}', [

            'uses' => 'Admin\SessionController@show',

            'as' => 'admin.sessions.view'

        ])->middleware('role_or_permission:Superadmin|Admin|read sessions|write sessions');
        
        Route::get('/edit/{id}', [

            'uses' => 'Admin\SessionController@edit',

            'as' => 'admin.sessions.edit'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::post('/edit/{id}', [

            'uses' => 'Admin\SessionController@update'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\SessionController@destroy',

            'as' => 'admin.sessions.delete'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::get('/availability/{id}', [

            'uses' => 'Admin\SessionController@availability',

            'as' => 'admin.sessions.availability'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::get('/availability/{id}/slot', [

            'uses' => 'Admin\SessionController@availability_slot',

            'as' => 'admin.sessions.availability.slot'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::post('/availability/{id}/slot', [

            'uses' => 'Admin\SessionController@update_availability_slot',

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::get('/booking/{id}', [

            'uses' => 'Admin\SessionController@booking',

            'as' => 'admin.sessions.booking'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::get('/booking/{id}/slots', [

            'uses' => 'Admin\SessionController@booking_slots',

            'as' => 'admin.sessions.booking.slots'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::get('/booking/{id}/slot', [

            'uses' => 'Admin\SessionController@booking_slot',

            'as' => 'admin.sessions.booking.slot'

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

        Route::post('/booking/{id}/slot', [

            'uses' => 'Admin\SessionController@update_booking_slot',

        ])->middleware('role_or_permission:Superadmin|Admin|write sessions');

    });

    Route::group([ 'prefix' => 'package-categories' ], function() {

        Route::get('/', [

            'uses' => 'Admin\PackageCategoryController@index',

            'as' => 'admin.package-categories'

        ])->middleware('role_or_permission:Superadmin|Admin|read package-categories|write package-categories');

        Route::post('/', [

            'uses' => 'Admin\PackageCategoryController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write package-categories');

        Route::group([ 'prefix' => '{id}', 'middleware' => 'role_or_permission:Superadmin|Admin|write package-categories' ], function() {

            Route::get('/edit', [

                'uses' => 'Admin\PackageCategoryController@edit',
    
                'as' => 'admin.package-categories.edit'
    
            ]);

            Route::post('/edit', [

                'uses' => 'Admin\PackageCategoryController@update'
    
            ]);

            Route::get('/delete', [

                'uses' => 'Admin\PackageCategoryController@destroy',
    
                'as' => 'admin.package-categories.delete'
    
            ]);

        });

    });

    Route::group([ 'prefix' => 'packages' ], function() {

        Route::get('/', [

            'uses' => 'Admin\PackageController@index',

            'as' => 'admin.packages'

        ])->middleware('role_or_permission:Superadmin|Admin|read packages|write packages');

        Route::get('/create', [

            'uses' => 'Admin\PackageController@create',

            'as' => 'admin.packages.create'

        ])->middleware('role_or_permission:Superadmin|Admin|write packages');

        Route::post('/create', [

            'uses' => 'Admin\PackageController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write packages');

        Route::get('/edit/{id}', [

            'uses' => 'Admin\PackageController@edit',

            'as' => 'admin.packages.edit'

        ])->middleware('role_or_permission:Superadmin|Admin|write packages');

        Route::post('/edit/{id}', [

            'uses' => 'Admin\PackageController@update'

        ])->middleware('role_or_permission:Superadmin|Admin|write packages');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\PackageController@destroy',

            'as' => 'admin.packages.delete'

        ])->middleware('role_or_permission:Superadmin|Admin|write packages');

    });

    Route::get('/generate-coupon-code', [

        'uses' => 'Admin\CouponController@generate_code',

        'as' => 'admin.coupons.generate'

    ])->middleware('role_or_permission:Superadmin|Admin|write coupons');

    Route::group([ 'prefix' => 'coupons' ], function() {

        Route::get('/', [

            'uses' => 'Admin\CouponController@index',

            'as' => 'admin.coupons'

        ])->middleware('role_or_permission:Superadmin|Admin|read coupons|write coupons');

        Route::post('/', [

            'uses' => 'Admin\CouponController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write coupons');

        Route::group([ 'prefix' => '{id}', 'middleware' => 'role_or_permission:Superadmin|Admin|write coupons' ], function() {

            Route::get('/view', [

                'uses' => 'Admin\CouponController@show',
    
                'as' => 'admin.coupons.view'
    
            ]);
            
            Route::get('/edit', [

                'uses' => 'Admin\CouponController@edit',
    
                'as' => 'admin.coupons.edit'
    
            ]);

            Route::post('/edit', [

                'uses' => 'Admin\CouponController@update'
    
            ]);

            Route::get('/delete', [

                'uses' => 'Admin\CouponController@destroy',
    
                'as' => 'admin.coupons.delete'
    
            ]);

        });

    });

    Route::group([ 'prefix' => 'products' ], function() {

        Route::get('/', [

            'uses' => 'Admin\ProductController@index',

            'as' => 'admin.products'

        ])->middleware('role_or_permission:Superadmin|Admin|read products|write products');

        Route::get('/create', [

            'uses' => 'Admin\ProductController@create',

            'as' => 'admin.products.create'

        ])->middleware('role_or_permission:Superadmin|Admin|write products');

        Route::post('/create', [

            'uses' => 'Admin\ProductController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|write products');

        Route::get('/view/{id}', [

            'uses' => 'Admin\ProductController@show',

            'as' => 'admin.products.view'

        ])->middleware('role_or_permission:Superadmin|Admin|write products');
        
        Route::get('/edit/{id}', [

            'uses' => 'Admin\ProductController@edit',

            'as' => 'admin.products.edit'

        ])->middleware('role_or_permission:Superadmin|Admin|write products');

        Route::post('/edit/{id}', [

            'uses' => 'Admin\ProductController@update'

        ])->middleware('role_or_permission:Superadmin|Admin|write products');

        Route::get('/view/{id}', [

            'uses' => 'Admin\ProductController@show',

            'as' => 'admin.products.view'

        ])->middleware('role_or_permission:Superadmin|Admin|read products|write products');

        Route::get('/delete/{id}', [

            'uses' => 'Admin\ProductController@destroy',

            'as' => 'admin.products.delete'

        ])->middleware('role_or_permission:Superadmin|Admin|write products');


    });

    Route::group([ 'prefix' => 'notifications' ], function() {

        Route::get('/', [

            'uses' => 'Admin\NotificationController@index',

            'as' => 'admin.notifications'

        ])->middleware('role_or_permission:Superadmin|Admin|read notifications|write notifications');

        Route::post('/', [

            'uses' => 'Admin\NotificationController@store'

        ])->middleware('role_or_permission:Superadmin|Admin|read notifications|write notifications');

        Route::post('/users', [

            'uses' => 'Admin\NotificationController@users',

            'as' => 'admin.notifications.users'

        ])->middleware('role_or_permission:Superadmin|Admin|read notifications|write notifications');

        Route::get('/payroll', [

            'uses' => 'Admin\PayrollsController@notificationsForm',

            'as' => 'admin.payroll.notifications'

        ])->middleware('role_or_permission:Superadmin|Admin');

    });

    /*-------------------------------------------- Payrolls --------------------------------------------*/
        
    Route::group([ 'prefix' => 'payrolls' ], function() {

        Route::get('/', [

            'uses' => 'Admin\PayrollsController@index',

            'as' => 'admin.payrolls'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

        Route::get('/fav/{pi?}/{ui?}', [

            'uses' => 'Admin\PayrollsController@payrollFav',

            'as' => 'admin.payrolls.fav'

        ])->middleware('role_or_permission:Superadmin|Admin|Subcontractor|Payroll');

        Route::get('/report/format/alert/{id}', [

            'uses' => 'Admin\PayrollsController@reportFormatAlert',

            'as' => 'admin.payroll.report.format.alert'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');


        Route::get('/report/format/alert/{payrollFileId}/{rowId}', [

            'uses' => 'Admin\PayrollsController@reportRowDelete',

            'as' => 'admin.payroll.report.row.delete'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

        Route::post('/payroll/report/status/change/{id}', [

            'uses' => 'Admin\PayrollsController@reportStatusChange',

            'as' => 'payroll.report.status.change'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

        Route::get('/get', [

            'uses' => 'Admin\PayrollsController@getPayrolls',

            'as' => 'admin.datatable.payroll.get'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

        Route::get('/view/{id}', [

            'uses' => 'Admin\PayrollsController@show',

            'as' => 'admin.payroll.view'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

        Route::get('/create', [

            'uses' => 'Admin\PayrollsController@create',
    
            'as' => 'admin.payroll.create'
    
        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

        /*Route::get('/edit/{id}', [

            'uses' => 'Admin\PayrollsController@edit',
    
            'as' => 'admin.payroll.edit'
    
        ])->middleware('role_or_permission:Superadmin');*/

        Route::get('/delete/{id}', [

            'uses' => 'Admin\PayrollsController@delete',
    
            'as' => 'admin.payroll.delete'
    
        ])->middleware('role_or_permission:Superadmin|Subcontractor|Payroll');

        Route::post('/edit/{id}', [

            'uses' => 'Admin\PayrollsController@update',
    
            'as' => 'admin.payroll.update'
    
        ])->middleware('role_or_permission:Superadmin|Payroll');

        Route::post('/save', [

            'uses' => 'Admin\PayrollsController@store',
    
            'as' => 'admin.payroll.save'
    
        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

        Route::post('/payroll/notification/send', [

            'uses' => 'Admin\PayrollsController@notificationsSend',

            'as' => 'admin.payroll.send.notification'

        ])->middleware('role_or_permission:Superadmin|Admin|Payroll');

    });

    /*-------------------------------------------- Payrolls --------------------------------------------*/

    Route::group([ 'prefix' => 'manpower/requests' ], function() {

   
Route::get('superadmin/manpower-view/{id}', [

    'uses' => 'Subcontractor\ManpowerController@logsp',

    'as' => 'superadmin.manpower.view'

])->middleware('role_or_permission:Subcontractor|Superadmin');

});

});



Route::group([ 'prefix' => 'trainer-panel', 'middleware' => [ 'auth', 'prevent-back-history' ] ], function() {

    Route::get('/dashboard', [

        'uses' => 'Trainer\DashboardController@index',

        'as' => 'trainer-panel.dashboard'

    ]);
    
});